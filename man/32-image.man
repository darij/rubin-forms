= Изображения

[init]

	use lib "lib";
	use common::sense;
	use R::App;

	
	$app->man->conf;
	
[test]

== Загрузка изображений и сохранение

	my $img = $app->image->load("html/img/testdata/orig.gif")->save("var/test-image/x.gif");
	
	my $img2 = $app->image->load("var/test-image/x.gif");
	
	$img->width			## $img2->width
	$img->height		## $img2->height
	
	