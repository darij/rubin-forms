= Сервер разработки

== Инициализация

[init]
	
	use lib "lib";
	use common::sense;
	use R::App;
	
	$app->ini->{service}{connect}{basename} = "rubin-forms-test";
	my $PRJ_CLEARER = $app->man->project("10-dev", include=>["user.yml"], log=>0, logsqlinfo=>0);
	
[test]

== Введение

`al dev` запускает прокси-сервер, который поднимает указанные в его конфигурации сервера и перезагружает их при изменении их файлов.

== Пример

@@10-dev//etc/user.yml
	# etc project file
	service:
		dev: !!perl/hash:INI_FILE_NODE
			port: 9001
			std: 0
			projects:
				"my1.do":
					dir: "."
					init: 'server { $app->project->path } port=>9002'
					run: 'sleep 1000'
					proxy: "localhost:9002"
				"my2.do":
					dir: "."
					watch:
						- lib
					init: 'require "my2.pm"'
					run: 'sleep 1000'
					proxy: ":9003"
@@10-dev/lib/my2.pm

	use R::App;
	server { [200, ["Content-Type" => "text/plain"], 456] } port=>9003;
	1;
	
[test]

В файле конфигурации фреймворка укажем:

	my $port_dev = 9001;
	my $port_my1 = 9002;
	my $port_my2 = 9003;

	my $dev = $app->dev->run;

	my $dev_sv = $dev->sv;

К ответу в `text/html` прибавляется websocket-ауторелоадер на javascript:

	$app->url(":$port_dev")->header(Host => "my1.do")->read	#~ var/.miu/root/10-dev<script>

А к остальным, нет:

	$app->url(":$port_dev")->header(Host => "my2.do")->read	#~ ^456$

Меняем `my2.pm` и любуемся новым выводом.

	$app->file("lib/my2.pm")->write("use R::App; server { 789 } port=>$port_my2; 1;");

	$app->url(":$port_dev")->header(Host => "my2.do")->read	#~ ^789

	undef $dev_sv;
