= Mime

[init]
	
	use common::sense;
	use lib "lib";
	use R::App;

	
[test]

== Классы
	
Классы имеют интерфейсы (методы `to` и `from`).
	
Получаем объект по типу:
	
	$app->mime->Get("text/json; charset=utf-8")->to({x=>{y=>20}})	# {"x":{"y":20}}
	
По расширению:
	
	$app->mime->Get("json")->from('{"x":{"y":20}}')	## {x=>{y=>20}}
	
Через геттер. Геттеры формируются из расширений:
	
	$app->mime->urlencoded->from('x=10&x=20&y=%20')	## {x=>bless([10,20], "R::Mime::Array"), y=>" "}
	
	
Классы описаны в `R::Mime`.

== Получение информации

Все зарегистрированные классы:

	join ",", app->mime->Regs		#~ R::Yaml
	
Типы:
	
	join ",", app->mime->Types		#~ text/json
	
Расширения:

	join ",", app->mime->Exts		#~ txt
	
=== Для одного mime
	
Все типы которые есть у mime по расширению или типу:

	join ",", app->mime->Alias("txt")			#~ text/plain
	join ",", app->mime->Alias("text/plain")	#~ text/plain
	
Первый тип:
	
	app->mime->Alias("txt")						# text/plain
	
Все расширения:
	
	join ",", app->mime->Ext("txt")				#~ txt
	join ",", app->mime->Ext("text/plain")		#~ txt

Первое расширение:
	
	app->mime->Ext("txt")				# txt
	app->mime->Ext("text/plain")		# txt
	
	
== Классы mime

=== json

	$app->mime->json->from('{"x":{"y":20}}')	## {x=>{y=>20}}
	$app->mime->json->to({x=>{y=>20}})			# {"x":{"y":20}}
	
=== yaml

	$app->mime->yaml->from("x:\n  'y': 20")		## {x=>{y=>20}}
	$app->mime->yaml->to({x=>{y=>20}})			# x: \n  "y": 20\n

=== urlencoded

	$app->mime->urlencoded->from('x=10&x=20&y=%20')	## {x=>bless([10,20], "R::Mime::Array"), y=>" "}
	$app->mime->urlencoded->to({x=>bless([10,20], "R::Mime::Array"), y=>" "})	# x=10&x=20&y=%20

=== httpheaders

	my $headers = 'content-type: text/html;
		  charset=windows-1251
	Allow: GET, HEAD
	Content-Length: 356
	ALLOW: GET, OPTIONS
	Content-Length:   1984
	';

	my $perldata = {
		'Content-Type' => 'text/html; charset=windows-1251',
		Allow => 'GET,HEAD,OPTIONS',
		'Content-Length' => 1984,
	};
	$app->mime->httpheaders->from($headers) ## $perldata
	

	my $string = 'Allow: GET,HEAD,OPTIONS
	Content-Length: 1984
	Content-Type: text/html; charset=windows-1251
	';
	$app->mime->httpheaders->to($perldata)	## $string

	
	