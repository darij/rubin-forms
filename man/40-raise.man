= Трейсбэк

[init]

	use common::sense;
	use lib "lib";
	use R::App;


	# добавим логирование
	$app->man->conf;
	
[test]

== Приёмы отладки

Получить трейсбэк в виде строки:
	
	$app->raise->trace;			#~ app.raise.trace
	
== Передача далее

`R::Raise` заменяет `$SIG{__DIE__}` на свой обработчик и если получена строка, то к ней прикрепляется трейсбэк.

	sub A1 { die 123; }
	sub A2 { A1() }
	sub A3 { A2() }
	sub A4 { 
		eval { A3() }; 
		$@ #~ 123
		#ref $@ 	# R::Raise
		#msg1 ":on_red white", "A3:", ":reset", $@;
		die if $@;
	}
	sub A5 { A4() }
	sub A6 { A5() }
	
	eval { A6() };
	$@ #~ 123
	#ref $@ # R::Raise
	#msg1 ":on_red white", "A6:", ":reset", $@;
	
== R::Raise::Quit

Начатые в волокне AnyEvent-события должны быть завершены, а их память - очищена. Иначе, .