= Coro

[init]

	use common::sense;
	use lib "lib";
	use R::App;

	
	use Coro;
	
[test]

== Каналы

	use Coro::Channel;
	
	my $channel = new Coro::Channel;
	
	my $x = async {
		$channel->get	# 10
		cede;
		$channel->get	# 30
	};
	
	my $y = async {
		$channel->get	# 20
	};
	
	$channel->put(10);
	$channel->put(20);
	$channel->put(30);
	
	$x->join;
	$y->join;
	

== Переменные текущего волокна

	use Coro::Specific;
	
	my $ref1 = new Coro::Specific;
	$$ref1 = 5;
	
	my $ref2 = new Coro::Specific;
	$$ref2 = 6;
	
	my $ref3 = new Coro::Specific;
	$$ref3 = {x=>7};
	
	$Coro::current->{_specific}		## [5, 6, {x=>7}]
	
	$Coro::current->{mydata} = {y => 123};
	
	my $x = async {
		my $ref4 = new Coro::Specific;
		$$ref4 = 4;
	
		$Coro::current->{_specific}		## [undef, undef, undef, 4]
		$Coro::main->{_specific}		## [5, 6, {x=>7}]
		$$ref2							## undef
		$$ref2 = 7;
		$$ref2							# 7
		
		$Coro::current->{_specific}		## [undef, 7, undef, 4]
		
		$Coro::main->{mydata}			## {y => 123}
	};
	
	$x->join;
	
	$Coro::current->{_specific}		## [5, 6, {x=>7}]
	

== call
	
	my $sigint = sub { exit; };

	my $x = async {
		$SIG{INT} = $sigint;
		$Coro::current->{desc} . 12	# call-async12
		cede;
		$Coro::current->{desc} . 24	# call-async24
		20
	};
	
	$x->{desc} = "call-async";
	
	cede;
	
	$SIG{INT}				##== $sigint
	
	
`call`:
	
	$x->call(sub {
		$SIG{INT}+0				##== $sigint
		$Coro::current->{desc}	# [main::]
	});
	
и `eval` выполняются в контексте того волокна из которого вызваны:
	
	$x->eval('$::VAL = $Coro::current->{desc}');
	
	$::VAL # [main::]
	
	$x->join	# 20

== async_pool
	
	Coro::async_pool {
		1 # 1
		cede;
		2 # 2
	};
	
	cede;
	cede;

== Повторное использование

	my $x = async {
		123
	};
	
	$x->join 	# 123
	
	$x->ready;
	$x->join	# 123

== async vs Coro->new

	my @async = async {
		123
	};
	push @async, async {
		456
	};
	join ",", map { $_->join } @async		# 123,456
	
	my @async = new Coro sub {
		123
	};
	push @async, new Coro sub {
		456
	};
	
	$_->ready for @async;
	
	join ",", map { $_->join } @async		# 123,456
	

== Дефолтные волокна

Idle процесс может называться по-разному в зависимости от выбранной AnyEvent::Model, например, `[EV idle thread]`.

	$Coro::idle->{desc}			#~ idle
	$Coro::main->{desc}			# [main::]
	$Coro::current->{desc}		# [main::]
	my $ps = async {
		$Coro::current->{desc}		## undef
	};
	$ps->join;

== Деструктор

	my @des;
	my $ps = new Coro sub {
	};
	$ps->on_destroy(sub {
		push @des, "des1"
	});
	$ps->on_destroy(sub {
		push @des, "des2"
	});
	$ps->ready;
	$ps->join;
	
	join ",", @des	# des2,des1
	
== eval

Перехват исключений.

	my @ps;
	my @des;
	push @ps, async {
		eval {
			push @des, "des1";
			cede;
			push @des, "des2";
			die 123;
		};
		push @des, "des3";
		$@	#~ 123
	};
	push @ps, async {
		push @des, "des4";
	};
	
	$_->join for @ps;
	
	join ",", @des		# des1,des4,des2,des3
	
То же, но внутри. Волокно с die валится и захватывает с собой программу.


== Время в волокнах

	use Coro::AnyEvent;
	
	msg1 "was_enabled", Coro::State::enable_times 1;
	msg1 "main times:", $Coro::current->times;
	
	my $tus = 1000_000;
	my $real_1;
	my $real_2;
	
	my @async;
	push @async, async {
		msg1 "a1 times:", $Coro::current->times;
		Coro::AnyEvent::sleep 1;
		msg1 "a1 times after sleep:", $Coro::current->times;
		my @r;
		push @r, rand for 1..$tus;
		msg1 "a1 times end:", ($real_2, undef) = $Coro::current->times;
	};
	
	push @async, async {
		msg1 "a2 times:", scalar $Coro::current->times, $Coro::current->times;
		Coro::AnyEvent::sleep 1;
		msg1 "a2 times after sleep:", $Coro::current->times;
		my @r;
		push @r, rand for 1..$tus;
		msg1 "a2 times end:", ($real_2, undef) = $Coro::current->times;
	};
	
	msg1 "main times end:", my ($real_m, undef) = $Coro::current->times;
	
	$_->join for @async;
	
	msg1 "main times after async:", $Coro::current->times;

	#$real_1+$real_2 ## $real_m
	
== Переключение данных


Можем ли мы при переключении на иное волокно менять какие-то данные?
	
	# use Coro::LocalScalar;
	
	# my $appval;
	
	# Coro::LocalScalar->new->localize($appval);
	
	# $appval = 10;
	# $appval # 10
	
	# my @ps;
	# push @ps, async {
		# 0+$appval # 0
		# $appval = 20;
		# cede;
		# $appval # 20
	# };
	
	# push @ps, async {
		# 0+0+$appval # 0
		# $appval = 30;
		# cede;
		# $appval # 30
	# };
	
	# 0+0+0+$appval # 10
	
	# $_->join for @ps;
	
	# 0+0+0+0+$appval # 10

Переменная в новом волокне будет undef.

	# use Coro::Localize;	
	# our $alias;
	
	# $alias = 10;
	# $alias # 10
	
	# my @ps = ();
	# push @ps, async {
		# corolocal $alias;
		# 0+$alias # 0
		# $alias = 20;
		# cede;
		# $alias # 20
	# };
	
	# push @ps, async {
		# corolocal $alias;
		# 0+0+$alias # 0
		# $alias = 30;
		# cede;
		# $alias # 30
	# };
	
	# 0+0+0+$alias # 10
	
	# $_->join for @ps;
	
	# 0+0+0+0+$alias # 10
	

Ну и просто:

	my $appval = my $zval = 10;
	$appval # 10
	Coro::on_enter { $appval = $zval };
	
	my @ps;
	push @ps, async {
		my $xval = 55;
		Coro::on_enter { $appval = $xval };
	
		0+$appval # 55

		cede;
		0-$appval # -55
	};
	
	push @ps, async {
		my $yval = 66;
		Coro::on_enter { $appval = $yval };
		
		0+0+$appval # 66

		cede;
		0-0-$appval # -66
	};
	
	0+0+0+$appval # 10
	
	$_->join for @ps;
	
	0+0+0+0+$appval # 10
	
	
	
# Ещё одна реализация.
# Однако нам надо реализовать что-то вроде `$app->connect`.

# == Главное волокно

# Главное волокно, это и есть `app->coro`:

	# $app->coro->name		# main
	
# Текущее волокно:

	# $app->coro->me			## $app->coro
	
# == Переключения
	
	# my $x = 10;
	
	# my $koni = $app->coro->new("koni-1" => sub {
		# $x = 6;
		# #Coro::AnyEvent::sleep( 1 );
		# cede;
		# $x;			# 11
		# $x = 5;
	# });
	
	# $koni->length			# 1
	# $koni->active->length	# 0
	# $koni->passive->length	# 1
	
	
# Запустим волокно:
	
	# $koni->run;
	
	# $koni->active->length	# 1
	# $koni->passive->length	# 0

# `async` добавляет волокно в коллекцию и сразу же запускает его.
	
	# $koni = $koni->async("koni-2" => sub {
		# $x;			# 6
		# $x = 11;
	# });
	
	# $x;			# 10
	
# Подождём завершения всех волокон в коллекции:
	
	# $koni->wait;
	
	# $x;			# 5

	# $koni->active->length	# 0
	# $koni->passive->length	# 2
	

== Исключения
	

`Coro` перекрывает в каждом порождённом волокне стандартные __DIE__ и __WARN__. Заменим их на `$app->raise`.

	my $x = 10;

	die "123";			#@ ~ (?s)error.*123
	
	my $sinus = async {
		die "456";			#@ !~ error
		$app->raise->setdie;
		die "789";			#@ ~ (?s)error.*789
	};
	
	$sinus->join;


== Межпроцессорные сигналы

Проверяем, что сигналы прерывают `Coro`.

	$app->process->new("test-coro" => "
		
	");

	# $app->coro;
	
	# Coro::AnyEvent::sleep(1);
	
	# my $process = $app->process->new("test-signal-with-coro", '

		# #$app->ini( $app->ini->parse("etc/unicorn.ini") );
		# $app->coro;
		# Coro::AnyEvent::sleep(1);
	
		# $app->coro->async(sub {
			# Coro::AnyEvent::sleep(3);
		# })->desc("sleeepper");
		# $app->coro->run;
		
	# ')->fg->start->done;
	
	# sleep 1;
	
	# $process->exists;			# 1
	
	# $process->stop->exists;		## undef


== condvar и signal

	$app->coro;

	use AnyEvent;
	my $cv = AnyEvent->condvar;
	$cv->send(1);
	
	$cv->recv;		# 1
	$cv->recv;		# 1
	
	my $signal = Coro::Signal->new;
	
	$signal->send;
	$signal->wait;	## undef
	
	my $signal = Coro::Signal->new;
	
	my @ps;
	push @ps, async {
		1 # 1
		$signal->wait;
		2 # 2
	};
	
	push @ps, async {
		3 # 3
		$signal->wait;
		4 # 4
	};
	
	require "Coro/Timer.pm";
	
	push @ps, async {
		Coro::Timer::sleep(0.1);
		$signal->send;
		$signal->send;
	};
	
	$_->join for @ps;
	
	
	#== AnyEvent server

	# my $server = $app->processServer->new()->make;
	# my $sd = $server->{sd};

	# $sd = $sh->fh if Isa $sd, "Coro::Handle";
	
	# AE::io();

	
== Завершение процессов
	
	#$app->coro->run;
	
== Connect и Coro

== Memcached

	# use AnyEvent::Memcached;
	
	# my $cv = AE::cv;
	
	# my $m = AnyEvent::Memcached->new(
		# servers => ["127.0.0.1:11211"],
		# debug   => 0,
		# compress_threshold => 10000,
		# namespace => 'test-namespace:',
	 
		# # May use another hashing algo:
		# #hasher  => 'AnyEvent::Memcached::Hash::WithNext',
	 
		# cv      => $cv,
	# );
	
	# $m->set(123, 123, cb => sub {
		# shift or warn "Set failed: @_";
		# 123 # 123
	# });
	
	# msg1 "set 123";
	
	# $m->set(456, 456, cb => sub {
		# shift or warn "Set failed: @_";
		# 456 # 456
	# });
	
	# msg1 "set 456";
	
	# $m->get(123, cb => sub {
		# $_[1] and warn "Set failed: $_[1]";
		# shift # 123
	# });
	
	# msg1 "get 123";
	
	# $m->get(456, cb => sub {
		# $_[1] and warn "Set failed: $_[1]";
		# shift # 456
	# });
	
	# msg1 "get 123";
	
	# $cv->recv;
	
	
	# use Coro;
	
	# my $m = AnyEvent::Memcached->new(
		# servers => ["127.0.0.1:11211"],
		# debug   => 0,
		# compress_threshold => 10000,
		# namespace => 'test-namespace:',
	# );
	
	# $m->set(123, 123, cb => Coro::rouse_cb);
	# my ($rc, $err) = Coro::rouse_wait;
	# $rc # 1
	
	# $m->set(456, 456, cb => Coro::rouse_cb);
	# my ($rc, $err) = Coro::rouse_wait;
	# $rc # 1
	
	# $m->get(123, cb => Coro::rouse_cb);
	# my ($rc, $err) = Coro::rouse_wait;
	# $rc # 123
	
	# $m->get(456, cb => Coro::rouse_cb);
	# my ($rc, $err) = Coro::rouse_wait;
	# $rc # 456
	
	# use Cache::Memcached::AnyEvent;
	
	# my $m = Cache::Memcached::AnyEvent->new({
		# servers => [ '127.0.0.1:11211' ],
		# auto_reconnect => 5,
		# compress_threshold => 10_000,
		# namespace => 'myapp.',
	# });
	# my $sv = guard { $m->disconnect if $m };
 
	# $m->add(123, 123, cb => sub {
		# shift or warn "Set failed: @_";
		# 123 # 123
	# });
	
	# msg1 "set 123";
	
	# $m->add(456, 456, cb => sub {
		# shift or warn "Set failed: @_";
		# 456 # 456
	# });
	
	# undef $sv;
	
== Многоядерность
	
Включаем по умолчанию:
	
	# use Coro::Multicore;
	
	# my $global = 10;
	
	# my @async = ();
	
	# push @async, async {
		# $global = 20;
		# cede;
		# $global # 30
		# $global = 40;
	# };
	
	# push @async, async {
		# $global = 30;
		# cede;
		# $global # 40
	# };
	
	# $_->join for @async;
	
	