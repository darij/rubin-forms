= Процессы

[init]

	use common::sense;
	use lib "lib";
	use R::App;

	
[test]
	
== Процесс
	
Текущий процесс:
	
	$app->process->pid			 	## $$
	$app->process->name			 	# 33-process.t
	
`app->process` - коллекция, она имеет длину:
	
	$app->process->length			# 1
	
Получить все дочерние процессы:
	
	$app->process->child->length	# 0
	
Получить все процессы в системе:

	$app->process->ps->length		#> 1
	
Породить новые процессы:
	
	my $doroty = $app->process->new(doroty => 'sleep 10')->init("require R::App");
	
	$app->process->child->length 			# 0
	$doroty->length;						# 1
	my $doroties = $doroty->dup(2)->waitrun;
		
`dup` создал коллекцию из 2-х элементов:
	
	$doroties->length;					# 2
	$doroties->name						# doroty-1
	$doroties->eq(1)->name				# doroty-2
	
	$doroty->name						# doroty
	$doroty->length;					# 1
	$app->process->child->length 		# 2
	
`waitrun` закончит ожидание до `sleep 10`, поэтому можно инициализировать процесс. Например, в инициаторе должны подгрузиться тяжёлые библиотеки.

	
	
Название процесса всегда с ним:
	
	join ",", sort $app->process->child->map(sub {$_->name}) # doroty-1,doroty-2
	
Процессы можно получить по имени:

	$app->process("doroty-1")->info->status		# S
	
Активными называются выполняющиеся процессы:
	
	$doroties->active->length				# 2
	
Можно послать процессам сигнал и ожидать их завершения:
	
	$doroties->kill("INT")->wait(5);
	
`wait` ожидает завершения процесса, а если он за указанный таймаут (5) не завершится, то убивает его 9-м сигналом.
	
	$doroties->active->length				# 0
	$doroties->passive->length				# 2
	$doroties->length						# 2	
	$app->process->child->length 			# 0
	
	
=== Запуск процессов nodejs

	app->process("node-test" => "#!node	console.log(123)")->std->waitrun->output;		#~ 123\n	
	
=== Деструкторы

Деструктор запускается при завершении процесса.

	# $app->process->new(first => 'sleep 1')->destroy(sub {
		# $app->child->length # 0
	# })->bg->run;
	
=== Восстановление упавших процессов

Если нужно чтобы процесс самовосстанавливался при падении:

	my $immortal = $app->process->new("immortal-manager" => '
	$app->process->new(
		"deathless-first" => "sleep 10",
		"deathless-second" => "sleep 10",
	)->run->immortal;
	')->waitrun;
	
	sleep 1;
	
	$immortal->child->length				# 2
	
Пусть процессы мгновенно погибнут:
	
	$immortal->child->kill("KILL");
	
	sleep 2;
	
	$immortal->child->length		# 2
	
	$immortal->kill("INT");
	
	sleep 2;
	
	$immortal->child->length		# 0
	$immortal->active->length		# 0
	
== pipe-ы

Потоки ввода и вывода, как и другие файловые дескрипторы можно переопределить:

`rrrumiu` перенаправляет **STDERR** в **STDOUT**. Восстанавливаем:

	open STDERR, ">&", our $__Miu__STDERR	# 1

Теперь укажем, что мы в новом процессе будем переопределять потоки вывода и ошибок.
	
	my $ps = $app->process->new("pipe-test" => '
			print "happy!\n";
			print STDERR "hi!\n";
			print STDERR "low-level!\n" x 10**3;
			print "outer!\n123";
			print STDERR "besier!\n";
			print "dist!\n";
	')->stdout->stderr->waitrun;
	
Ну и получим данные. Потоки неблокирующие, они считывают всё, что попало в потоки вывода на момент считывания.
	
	$ps->err	#~ besier!
	$ps->out	#~ happy!\nouter!\n123dist!\n
	
Используйте `->stderr` для переопределения потока ошибок до `->run` и `->err` для чтения данных. Поток неблокирующий.
	

