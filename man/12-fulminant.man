= Fulminant

== Инициализация

Подключаем наш фреймворк:

[init]
	
	use lib "lib";
	use common::sense;
	use R::App;
	
	my $PRJ_CLEARER = $app->man->project("12-fulminant", log=>0, logsqlinfo=>0);
	
[test]


== Введение

`R::Fulminant` - сервер на волокнах.

== Keep-Alive соединение

`Fulminant` предоставляет удобный интерфейс для запуска приложений.

	my $BIG = "123\n" x 10000;

	my $port = 3194;
	my $router = $app->router->new;
	
	package MyController {
		use common::sense;
		use R::App;
		
		sub GET($$) { $router->add("GET $_[0]" => $_[1]) }
		sub PUT($$) { $router->add("PUT $_[0]" => $_[1]) }
		
		GET "/sleep-:time" => sub {
			sleep $_[0]->attr->time;
			"sleepped"
		};
	
		my $name = "memorial";
		GET "/name" => sub {
			$name
		};
		
		PUT "/name" => sub {
			$name = $_[0]->data->name;
			"name puts $name"
		};
		
		GET "/hello" => sub {
			my ($q) = @_;
			"привет ".$q->cookie("world")."!"
		};
		
		GET "/big" => sub {
			$BIG
		};
		
		GET "/511" => sub {
			die [511];
		};
		
		GET "/err" => sub {
			die [500, "ошибка, знаете-ли"];
		};
		
		GET "/err0" => sub {
			die 123;
		};
	}
	
Запускаем сервер и получаем охранное значение (`$sv`), как только охранное значение будет "обнулено" тут же сервер остановится.

	#msg1 $router;

	my $fulminant = server {
		my ($x, $slugs) = $router->get(join " ", $_->via, $_->path);
		$_->{_attr} = $slugs;
		$x? $x->($_): [404] 
	} port => $port;
	my $sv = $fulminant->sv;
	
Следующий запрос начнёт выполняться
	
	use R::Coro;
	
	#$app->coro->ps->say;
	#$app->coro->lis;
	
	my $async = async {
		my $kiss = $app->url->timeout(1)->keep;
	
		$kiss->alive("http://localhost:$port/511")->res->code	# 511
	
		$kiss->alive("http://localhost:$port/big")->read		## $BIG
		
		$kiss->alive("http://localhost:$port/hello")->cookie(world=>"Мир")->read		# привет Мир!
	};

	cede;
	
и передаст управление на:
	
	my $less = $app->url->timeout(1)->keep;
	
	$less->alive("http://localhost:$port/err")->read		#~ ошибка, знаете-ли
	
	#$app->coro->ps->say;
	#$app->coro->lis;
	
	my $keep = $app->url->timeout(1)->keep;
	
	$keep->alive("http://localhost:$port/name")->read		# memorial
	
	$less->alive("http://localhost:$port/err0")->read =~ 123		## $app->ini->{dev}? 1: ""
	
	$keep->alive("http://localhost:$port/name")->put(name => "six")->read	# name puts six
	
`keep` клонирует `jar`.
	
	$less->alive("http://localhost:$port/hello")->read		# привет Мир!
	
	$keep->alive("http://localhost:$port/name")->read		# six
	
	$keep->alive("http://localhost:$port/xxx")->read		# <center>404 Not Found<hr>fulminant/12-fulminant</center>
	
	$keep->alive("http://localhost:$port/err")->read		#~ ошибка, знаете-ли
	
	$less->alive("http://localhost:$port/big")->read		## $BIG
	
	$async->join;
	
Теперь оттестируем таймаут:

	$fulminant->{timeout} = 0.25;
	
	$app->url("http://localhost:$port/sleep-10")->read		# Gateway Timeout 0.25s
	
	undef $sv;
