= Метаинформация о структуре модели

== Инициализация

Подключаем наш фреймворк:

[init]

	use common::sense;
	use lib "lib";
	use R::App;


Зададим конфигурацию:

	my $PRJ_CLEARER = $app->man->project("03-orm-meta", log=>0, logsqlinfo=>0);

[test]

== Введение

Объект с метаинформцией задаётся в виде сервиса (обычно это `app->meta`). Так же задаётся сервис методы которого соответствуют таблицам (`app->model->table`).

== Метаинформация

	ref $app->meta		# R::Model::Metafieldset
	
== Создание и удаление полей "на лету"

	$app->meta->fieldset("filmmaker")->
		col(name => "varchar(255)")->
		m2m("serial")->
	end;
	
	$app->meta->fieldset("serial")->
		col(name => "varchar(255)")->
	end;
	
Были созданы таблицы:

	join ", ", map { $_->name } @{ $app->meta->fields }		# _migrate, filmmaker, serial, serialFilmmaker

`_migrate` - таблица для миграций, о ней - позже.
	
Таблицу `serialFilmmaker` создала связь `m2m("serial")`. Она так же создала филд `filmmaker.serials`:
	
	join ", ", sort keys %{$app->meta->exists("filmmaker")->field}	# id, name, serialFilmmakers, serials
	
`serials` - это наше поле m2m:
	
	ref $app->meta->exists("filmmaker")->field("serials")	# R::Model::Field::M2m
	
А `serialFilmmakers`  - это обратное поле на поле `serialFilmmaker.filmmaker`.
	
	ref $app->meta->exists("filmmaker")->field("serialFilmmakers")	# R::Model::Field::Back
	
Так же дело обстоит и в филдсете `serial`:

	join ", ", sort keys %{$app->meta->exists("serial")->field}		# filmmakers, id, name, serialFilmmakers
	
`filmmakers` - это связь m2m на фиелдсет `filmmaker`.
	
	join ", ", sort keys %{$app->meta->exists("serialFilmmaker")->field}		# filmmaker, id, serial
	
Сносим поле m2m. Исчезнут все связи порождённые им и филдсет `serialFilmmaker`.

	$app->meta->exists("filmmaker")->field("serials")->delete;
	
Был удалён филдсет для связи и все связанные с ним связи:
	
	join ", ", map { $_->name } @{ $app->meta->fields }					# _migrate, filmmaker, serial
	join ", ", sort keys %{$app->meta->exists("serial")->field}			# id, name
	join ", ", sort keys %{$app->meta->exists("filmmaker")->field}		# id, name
	
Теперь воссоздадим столбец:

	$app->meta->fieldset("filmmaker")->m2m("serial");
	
	join ", ", map { $_->name } @{ $app->meta->fields }					# _migrate, filmmaker, serial, serialFilmmaker
	join ", ", sort keys %{$app->meta->exists("serial")->field}			# filmmakers, id, name, serialFilmmakers
	join ", ", sort keys %{$app->meta->exists("filmmaker")->field}		# id, name, serialFilmmakers, serials
	
	# app->make->gosub("models");
	# app->make->gosub("show", msg "filmmaker");
	# app->make->gosub("show", msg "serial");
	# app->make->gosub("show", msg "serialFilmmaker");
	