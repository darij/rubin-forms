= Вспомогательные функции

== Инициализация

[init]

	use common::sense;
	use lib "lib";
	use R::App;

	
[test]

== like

Хелпер для превращения глоба в регулярку:

* ** и **/ - несколько символов включая /
* * - несколько символов исключая /
* ? - один символ
* {...} - 0+
* (...) - 1+
* [...] - 0|1
* |...| - один из указанных символов, - - символьный интервал
* ||...|| - кроме указанных символов
* , или ; - или
* \ - экранировать следующий символ, а так же - группы символов
* # - цифра
* <name:...> - именованная группа
* <...> или <:...>- скобки

	'' =~ $app->perl->like('*');						# 1
	'.png' =~ $app->perl->like('*.png');				# 1
	'a/1.png' =~ $app->perl->like('*a/*.png');		  	# 1
	'a/x/1.png' =~ $app->perl->like('*a/*.png');		# 
	'a/x/m/1.png' =~ $app->perl->like('*a/**/*.png');	# 1
	'a/1.png' =~ $app->perl->like('*a/**/*.png');		# 1
	'a1.png' =~ $app->perl->like('a/**/1.png');			# 
	
	'a1.png' =~ $app->perl->like('a?.png');			 	# 1
	'a12.png' =~ $app->perl->like('a?.png');			# 
	
	'a12.png' =~ $app->perl->like('a{#}.png');		 	# 1
	'a.png' =~ $app->perl->like('a{#}.png');		 	# 1
	
	'a.png' =~ $app->perl->like('a[##].png');		 	# 1
	'a1.png' =~ $app->perl->like('a[##].png');			# 
	'a12.png' =~ $app->perl->like('a[##].png');			# 1 
	
	'a12.png' =~ $app->perl->like('a(##).png');			# 1 
	'a.png' =~ $app->perl->like('a(##).png');			#  
	'a123.png' =~ $app->perl->like('a(##).png');		#  
	'a1234.png' =~ $app->perl->like('a(\d\d).png');		# 1
	
	'ab.png' =~ $app->perl->like('a|a-z_|.png');		# 1
	'a9.png' =~ $app->perl->like('a|a-z_|.png');		# 
	
	'ab.png' =~ $app->perl->like('a||a-z_||.png');		# 
	'a9.png' =~ $app->perl->like('a||a-z_||.png');		# 1
	
	'abccda.png' =~ $app->perl->like('{abc;cda}.png');	# 1
	
	'abc.png' =~ $app->perl->like('<:abc,cda>.png');	# 1
	'cda.png' =~ $app->perl->like('<abc,cda>.png');		# 1
	'abc.png' =~ $app->perl->like('<name:abc,cda>.png') && $+{name};	# abc
	
	'{}.png' =~ $app->perl->like('\{\}.png');			# 1

Рекурсия позволяет распознавать вложенные скобки:

	'((123)(2))' =~ $app->perl->like('<bra:\(((||()||),&bra)\)>');			# 1
	'((123)(2))' =~ $app->perl->like('<bra:\(((||()||),&{bra})\)>');		# 1
	
Регулярка `like` работает от начала и до конца строки:

	'1' =~ $app->perl->like('1');			# 1
	'12' =~ $app->perl->like('1');			# 
	

