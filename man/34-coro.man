= Волокна

[init]

	use common::sense;
	use lib "lib";
	use R::App;

	use R::Coro;
	
	
[test]
	
== Волокно
	
=== Стандартные волокна
	
Главное волокно:

	$app->coro->main->name		# [main::]
	
Текущее волокно:
	
	$app->coro->me->part	##== $app->coro->main->part
	$app->coro->me->name	# [main::]
	
Idle волокно:

	$app->coro->idle->name	#~ idle
	
Его название зависит от текущей виртуальной машины: `[EV idle thread]` или `[AnyEvent idle thread]` или `[AnyEvent idle process]`.
	
Шедулер внеблочных суброутин:

	$app->coro->one($Coro::unblock_scheduler)->name		# [unblock_sub scheduler]
	
Менеджер:

	$app->coro->one($Coro::manager)->name		# [coro manager]
	
=== Все волокна

При старте 

	$app->coro->ps->length	# 4
	join ",", sort $app->coro->ps->map(sub {$_->name})	#~ \[main::\]
	join ",", sort $app->coro->ps->map(sub {$_->name})	#~ idle
	
`app->coro` - коллекция, она имеет длину:
	
	$app->coro->length		# 0
	
	
=== Порождение волокон

Создаём:

	my $fiber = $app->coro("fiber" => sub {
		$app->coro->me->name
	});
	
На основе `fiber` создаём два других с именами `fiber-1` и `fiber-2`.
	
	my $fibers = $fiber->dup(2);

	$fibers->active->length			# 0
	$fibers->passive->length		# 2
	
И помещаем в очередь:
	
	$fibers->ready;
	
	$fibers->active->length			# 2
	$fibers->passive->length		# 0

Ожидаем завершения и получаем результаты выполнения:
	
	join ",", $fibers->join			# fiber-1,fiber-2
	
Из завершонных волокон мы можем получать результаты сколько угодно:
	
	join ",", $fibers->join			# fiber-1,fiber-2
	
В данном случае волокна запустятся только в `join`.

Можно получить волокно по имени:
	
	$app->coro("fiber-1")->length		# 1
	
Перезапускаем, так как уже отработанные волокна нельзя использовать повторно:
	
	join ",", sort $fibers->restart->join	# fiber-1,fiber-2
		
	$fibers->length					# 2
	$fibers->active->length			# 0
	$fibers->passive->length		# 2
	
=== Анонимные сопрограммы

Анонимным волокнам назначаются имена "[async-1], [async-2]..." в зависимости от внутреннего счётчика.

	use R::Coro;

	async {
		me->name #~ ^\[async-\d+\]$
	};
	
При завершении программы активные сопрограммы джойнятся, благодаря чему пока они не завершены выхода не происходит.

Она возвращает очередь из одного элемента - себя.

	my $async = async {
		return 10, 20, 30;
	};

	$async->join	# 3

=== Именованные сопрограммы

	use R::Coro;
	
	coro "mikl" => sub {
		me->name	# mikl
	};
	
	
=== enter и leave
	
`use R::Coro` импортирует `async` аналогичный `$app->coro(name => sub)->ready`. Так же на `END` ставиться обработчик, делающий `join` для всех порождённых волокон.

	use R::Coro;

	my $counter = 0;
	
	
	coro "enter-leave-1" => sub {
		enter {
			msg "enter";
			$counter .= "+";
		};
		
		leave {
			msg "leave";
			$counter .= "-";
		};
	
		me ## $app->coro->me
		
`enter` срабатывает только один раз.

		$counter # 0+
		
		cede;
		
		$counter # 0+-+
	};
	
	coro "enter-leave-2" => sub {
		$counter	# 0+-
	};
	
	coro "enter-leave-3" => sub {
		$counter	# 0+-
	};
	
	
	$app->coro("enter-leave-1", "enter-leave-2", "enter-leave-3")->join;
	
	
=== kill
	
Вновь запускаем и киляем один:
	
	my $coronal = $app->coro("coronal"=>sub {sleep .1; $app->coro->me->name});
	
	my $coronals = $coronal->dup(2)->ready;
	
	#$coronals->part->cancel("terminate");
	$coronals->eq(0)->kill("terminate");
	
	join ",", $coronals->join	# terminate,coronal-2
	
	my $a = async {
		sleep 10;
	};
	
	$a->kill;
	join ", ", $a->join	# 
	
=== timer

Киляем по таймеру:

	my $sleep = $app->coro("sleep"=>sub {
		sleep 3;
		$app->coro->me->name
	});
	my $sleeps = $sleep->dup(2)->ready;

	my $timer = AE::timer 1, 0, sub {
		"timer"	# timer
		msg1 $app->coro->me->name, $sleeps->eq(0)->name;
		$sleeps->eq(0)->throw("terminate");
		"after kill"	# after kill
	};
	
	join ",", $sleeps->join	# sleep-2
	
	undef $timer;
	
=== ps
	
Получить все волокна:
	
	join ",", $app->coro->ps->map(sub { $_->name }) #~ [main::]
	
=== pool

Волокна в пуле (вызванные через async_pool).

	ref $app->coro->psasync	# R::Coro

	#$app->coro->async_pool->map(sub {$_->name}) #~ [main::]
	
=== Запустить код в волокне

	
	
=== Восстанавливаемые волокна

Волокна восстанавливаются, пока их не убъют `kill`.

	my $count = 0;
	
	my $coros = $app->coro("deathless" => sub { $count .= "," . $app->coro->me->name })->dup(3)->immortal->ready;
	
	$coros->active->length		# 3
	
	#$app->coro->ps->say;
	#$app->coro->lis;
	
После `cede` управление передаться на **deathless-1**, после него - на **deathless-2** и вернётся только после **deathless-3**.
	
	$count # 0
	
Один куда-то теряется:
	
	Coro::cede();
	
	$count # 0
	
	Coro::cede();
	$count						# 0,deathless-1,deathless-2,deathless-3

	Coro::cede();
	$count						# 0,deathless-1,deathless-2,deathless-3,deathless-1,deathless-2,deathless-3
	
	$coros->active->length		# 3
	
	$coros->kill;
	
	$coros->active->length		# 0
	
	
=== Приоритет
	
	
