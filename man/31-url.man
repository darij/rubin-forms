= Урлы

Коллеции урлов позволяют скачать с интернета необходимые страницы. Поддерживаются различные протоколы, метода и куки. Полученные страницы могут быть отпарсены, из них извлечены url-ы и  

Для работы с урлами используется `$app->url("url1", "url2"...)`.

[init]

	use lib "lib";
	use common::sense;
	use R::App;

	
	$app->man->conf;
	
[test]

== Загрузка страниц

У `app->url` нет корня, поэтому если не указан протокол подставляется `http://`

	$app->url("example.edu")->read		#~ <h1>Example Domain</h1>
	$app->url("no.domen")->read		#~ Can't connect to no.domen:80
	
Загрузить все страницы в коллекции можно через `download`, а получить `HTTP::Response` через `res`:
	
	$app->url("example.edu", "no.domen")->method("head")->eq(1)->res->status_line	#~ 500 Can't connect to no.domen:80
	
`download` вызывается в `read` и `res` автоматически, если его не было раньше в цепочке вызовов коллекции.

Получить по url сразу `R::Htmlquery` (аналог `jQuery`) можно методом `q`:

	$app->url("example.edu")->q->find('[href$="example"]')->text			# More information...

== Части урла

	$app->url(":9013")->url						# http://localhost:9013/
	$app->url("http://:9013/")->url				# http://localhost:9013/
	$app->url(":9010")->domain(":9013")->url	# http://localhost:9013/
	$app->url("meta.ua")->url					# http://meta.ua/
	$app->url("meta.ua")->scheme				# http
	$app->url("meta.ua")->authority				# meta.ua
	$app->url('user@meta.ua')->authority		# user@meta.ua
	$app->url('user:pass@meta.ua')->authority	# user:pass@meta.ua
	$app->url('user:pass@meta.ua:2/path/?1')->authority	# user:pass@meta.ua:2
	$app->url("meta.ua")->userinfo				## undef
	$app->url('user@meta.ua')->userinfo			# user
	$app->url('user:pass@meta.ua')->userinfo	# user:pass
	$app->url("meta.ua")->login					## undef
	$app->url('user@meta.ua')->login			# user
	$app->url('user:pass@meta.ua')->login		# user
	$app->url("meta.ua")->passwd				## undef
	$app->url('user@meta.ua')->passwd			## undef
	$app->url('user:pass@meta.ua')->passwd		# pass
	$app->url("meta.ua")->opaque				# //meta.ua/
	$app->url("meta.ua")->host					# meta.ua
	$app->url("meta.ua")->port					# 80
	$app->url("meta.ua:90")->port				# 90
	$app->url("meta.ua")->_port					## undef
	$app->url("meta.ua:90")->_port				# 90
	$app->url("meta.ua")->path					# /
	$app->url("meta.ua")->query					## undef
	$app->url("meta.ua?x=10&y=6")->query		# x=10&y=6
	$app->url("meta.ua")->param->Size			# 0
	$app->url("meta.ua?x=10&x=8&y=6")->param->All				## {x=>bless([10, 8], "R::Mime::Array"), y=>6}
	$app->url("meta.ua")->param(x=>10, x=>8, y=>6)->param->x	# 10
	[$app->url("meta.ua")->param(x=>10, x=>8, y=>6)->param->x]	## [10, 8]
	$app->url("meta.ua")->fragment				## undef
	$app->url("meta.ua#xyz")->fragment			# xyz
	
Если начинается с `/`, то считается uri localhost-а:
	
	$app->url("/meta.ua")->url				# http://localhost/meta.ua
	$app->url("?meta.ua")->url				# http://localhost/?meta.ua
	$app->url("#meta.ua")->url				# http://localhost/#meta.ua

Устанавливаются все:
	
	my $a = $app->url("meta.ua", "ya.ru");
	my $x = $a->scheme("ftp")->login("admin")->passwd("querty")->port(346)->path("xyz")->param(x=>1, x=>2)->fragment("123");
	
	$x->eq(0)->url			# ftp://admin:querty@meta.ua:346/xyz?x=1&x=2#123
	$x->eq(1)->url			# ftp://admin:querty@ya.ru:346/xyz?x=1&x=2#123
	
Каждый раз при изменении коллекция копируется:

	$a->eq(0)->url			# http://meta.ua/
	$a->eq(1)->url			# http://ya.ru/


=== Методы считывания

$app->url()->get

	
== Зеркалирование сайта

	
