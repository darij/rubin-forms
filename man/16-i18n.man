= i18n - локализация

== Инициализация

Подключаем наш фреймворк:

[init]
	
	use lib "lib";
	use common::sense;
	use R::App;
	
	my $PRJ_CD = $app->man->project("my-i18n-prj", nodatabase=>1);
	
[test]


== perl

Для использования установите `xgettext` и `Locale::TextDomain`.

Проверить версию `xgettext` можно командой: `gettext -V`.

@@my-i18n-prj/lib/Dex.pm
	package Dex;
	use common::sense;
	use R::App;
	#use Locale::TextDomain qw/ru/;
	
	# проверочный метод
	sub hello {
		my ($self) = @_;
		join ", ", 
			__"hello", 
			__n("man", "men", 0),
			__n("man", "men", 1),
			__n("man", "men", 2),
			__x("This is the {color} {thing}.", thing => __"apple", color => __"red"),
			__p("File|", "Open"),
			__p("Preferences|Advanced|Policy", "Open"),
			__px("Verb, to view", "View {file}", file => "fff");
	}
	
	1;
[test]

	my $ru = $app->file("i18n/ru.po");
	my $fr = $app->file("i18n/fr.po");
	my $po = $app->file("var/messages.pot");
	
`al mkmsg` создаёт локали.

	print $app->tty->run("al mkmsg ru");
	print $app->tty->run("al mkmsg fr");
	
	$ru->exists	# 1
	$ru->read	#~ msgid "hello"
	
	$ru->replace(sub {
		s/(msgid "hello"\nmsgstr )""/$1"приветище!"/;
	});

`al msg` обновляет.

	print $app->tty->run("al msg");
	
	$ru->read	#~ msgstr "приветище!"
	
	$app->use("Dex")->hello		# приветище!, man, man, men, This is the red apple., Open, Open, View fff
	
	