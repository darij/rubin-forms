= Коннект к базе

Объект коннект представляет собой низкий уровень доступа к базе. Он используется ORM.

== Инициализация

Подключаем наш фреймворк:

[init]

	use common::sense;
	use lib "lib";
	use R::App;


Зададим конфигурацию:

	$app->man->configure(log=>0);

[test]


== Информация о структуре базы

== SQL-запросы

* do(sql) - выполняет модифицирующий запрос

	$app->connect->do("create table N(id int primary key auto_increment, x int)");		# 0

* add - добавляет строку

	$app->connect->add("N" => {x => 66})	##== $app->connect
	
* append - добавляет строку и возвращает её `pk`
	
	$app->connect->append("N" => {x => 69})	# 2
	
* update - апдейт-запрос
	
	$app->connect->update("N" => {x => 63} => { x=>66, id=>1 })->last_count	# 1
	$app->connect->update("N" => {x => 67} => 1)->effect	# 1
	
	
* query - запрашивает строку в виде массива, а в скалярном контексте один колумн

	$app->connect->query("N" => "x" => {id => 1})		## 67
	join ", ", $app->connect->query("N" => "*" => 1)	# 1, 67

* query_ref - запрашивает строку в виде хеша
	
	$app->connect->query_ref("N" => "*" => {id => 1})		## {id=>1, x=>67}
	
* query_all - запрашивает все строки
	
	$app->connect->query_all("N" => [qw/id x/] => {id => [1..2]})	## [{id=>1, x=>67}, {id=>2, x=>69}]
	$app->connect->query_all("N" => "id, x" => "id in (1,2)")		## [{id=>1, x=>67}, {id=>2, x=>69}]

* query_col - запрашивает один колумн
	
	$app->connect->query_col("N" => "x" => {id => [1..2]})	## [67, 69]

* query_array - запрашивает строки в виде массивов
	
	$app->connect->query_array("N" => "*")		## [[1,67], [2,69]]
	my @A = $app->connect->query_array("N" => "*");
	\@A ## [[1,67], [2,69]]

	
== Транзакции

	my $c = $app->connect;

	$c->do("create table A(id int)");		# 0

Транзакция создаётся и все sql-запросы отправляются на транзакцию.
При этом только когда произойдёт `commit` транзакция запишет всё, что у неё накопилось.

	my $guard_transaction = $app->connect->begin;
	$c->_dbh->{AutoCommit};  # 
	$c->add("A", {id=>10});
	$c->add("A", {id=>20});
	$c->commit;
	undef $guard_transaction;
	
	$c->_dbh->{AutoCommit}	# 1
	
	$app->connect->query_col("A", "id")	## [10, 20]
	
Если не было `commit`, то произойдёт автоматом `rollback` на деструкторе.

	my $guard_transaction = $app->connect->begin;
	$c->_dbh->{AutoCommit};  # 
	$c->add("A", {id=>30});
	$c->add("A", {id=>40});
	undef $guard_transaction;
	
	$c->_dbh->{AutoCommit}	# 1
	
	$app->connect->query_col("A", "id")	## [10, 20]
	$c->add("A", {id=>40});
	$app->connect->query_col("A", "id")	## [10, 20, 40]
	

	
== Acc (TODO)

Данный интерфейс предназначен для выполнения коллекции запросов DDL и SQL вперемешку.
Он имеет следующие операции:

1. show - показать SQL или DDL запрос
2. save (store, sync) - сохранить
3. load - загрузить. Загружает данные при SQL или выводит `show create` для таблиц и отображений, а для  при DDL
4. rm (erase, drop) - удаляет сущность
5. val - возвращает/ устанавливает значения

$app->connect->acc(":test_base")->save->use->show		# CREATE DATABASE test_base
$app->connect->acc("ex.", "a int?")->save->show		# CREATE TABLE ex()
	
	
