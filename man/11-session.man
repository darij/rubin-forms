= Сессии

== Инициализация

[init]
	
	use common::sense;
	use R::App;


	$app->man->conf;
	
[test]

== Сессии
	
	my $id = 10;
	
	$app->session(id=>$id)->_clear;
	$app->session(id=>$id)->counter				## undef
	$app->session(id=>$id)->counter({x=>123});
	$app->session(id=>$id)->counter				## {x=>123}
	
== В сервере
	
Кука сессии устанавливаются автоматически:
	
	my @paths;
	
	my $sv = server { 
		push @paths, $_->path; 
		$_->session->counter($_->session->counter + 1)->id 
	} port=>9002;
	$sv = $sv->sv;
		
	my $url = $app->url(":9002");
	
	my $session_id = $url->read;

	$url->cookie("sess")		## $session_id
	
	my $url2 = $url->path("/123");
	
	$url2->cookie("sess")		## $session_id
	$url2->read					## $session_id
	$url2->cookie("sess")		## $session_id
	
	my $session_id2 = $app->url(":9002/456")->read;
	$session_id2 ##ne $session_id

	undef $sv;
	
	$app->session(%{$app->session}, id=>$session_id)->counter		# 2
	$app->session(%{$app->session}, id=>$session_id2)->counter		# 1
	
	join ", ", @paths		# /, /123, /456
	