#!/usr/bin/env perl
# сгенерировано miu

use utf8;
use open qw/:std :utf8/;

BEGIN {
	select(STDERR);	$| = 1;
	select(STDOUT); $| = 1; # default
	close STDERR; open(STDERR, ">&STDOUT");
}

use Test::More tests => 131;
print "= Файлы" . "\n";
use lib "lib";
use common::sense;
use R::App;


print "== Пути" . "\n";
my $file = $app->file("root/mmd/dir1/dir2/name.ext1.ext2.ext3");

::is( scalar($file->ext), "ext3", "\$file->ext;					# ext3" );
::is( scalar($file->exts), "ext1.ext2.ext3", "\$file->exts;				# ext1.ext2.ext3" );
::is( scalar($file->name), "name.ext1.ext2", "\$file->name;				# name.ext1.ext2" );
::is( scalar($file->nik), "name", "\$file->nik;					# name" );
::is( scalar($file->file), "name.ext1.ext2.ext3", "\$file->file;				# name.ext1.ext2.ext3" );
::is( scalar($file->dir), "root/mmd/dir1/dir2", "\$file->dir;					# root/mmd/dir1/dir2" );
::is( scalar($file->subdir("root/mmd")->path), "dir1/dir2/name.ext1.ext2.ext3", "\$file->subdir(\"root/mmd\")->path;				# dir1/dir2/name.ext1.ext2.ext3" );
::is( scalar($file->subdir("root/mmd" => "rss/mmx")->path), "rss/mmx/dir1/dir2/name.ext1.ext2.ext3", "\$file->subdir(\"root/mmd\" => \"rss/mmx\")->path;	# rss/mmx/dir1/dir2/name.ext1.ext2.ext3" );

::is( scalar($file->ext("ee")->path), "root/mmd/dir1/dir2/name.ext1.ext2.ee", "\$file->ext(\"ee\")->path;					# root/mmd/dir1/dir2/name.ext1.ext2.ee" );
::is( scalar($file->exts("ee")->path), "root/mmd/dir1/dir2/name.ee", "\$file->exts(\"ee\")->path;				# root/mmd/dir1/dir2/name.ee" );
::is( scalar($file->name("nn")->path), "root/mmd/dir1/dir2/nn.ext3", "\$file->name(\"nn\")->path;				# root/mmd/dir1/dir2/nn.ext3" );
::is( scalar($file->nik("nik")->path), "root/mmd/dir1/dir2/nik.ext1.ext2.ext3", "\$file->nik(\"nik\")->path;				# root/mmd/dir1/dir2/nik.ext1.ext2.ext3" );
::is( scalar($file->dir("dir")->path), "dir/name.ext1.ext2.ext3", "\$file->dir(\"dir\")->path;				# dir/name.ext1.ext2.ext3" );

my $file2 = $app->file("xxx/mmd");
::is( scalar($file2->nik), "mmd", "\$file2->nik;					# mmd" );
::is( scalar($file2->name), "mmd", "\$file2->name;					# mmd" );
::is( scalar($file2->file), "mmd", "\$file2->file;					# mmd" );
::is( scalar($file2->dir), "xxx", "\$file2->dir;					# xxx" );
::is( scalar($file2->ext), "", "\$file2->ext;					#" );
::is( scalar($file2->exts), "", "\$file2->exts;					#" );

::is( scalar($file2->file("mmx")->path), "xxx/mmx", "\$file2->file(\"mmx\")->path;		# xxx/mmx" );
::is( scalar($file2->nik("mmx")->path), "xxx/mmx", "\$file2->nik(\"mmx\")->path;		# xxx/mmx" );
::is( scalar($file2->name("mmx")->path), "xxx/mmx", "\$file2->name(\"mmx\")->path;		# xxx/mmx" );
::is( scalar($file2->ext("wxt")->path), "xxx/mmd.wxt", "\$file2->ext(\"wxt\")->path;		# xxx/mmd.wxt" );
::is( scalar($file2->exts("wxt")->path), "xxx/mmd.wxt", "\$file2->exts(\"wxt\")->path;		# xxx/mmd.wxt" );

my $file3 = $app->file("mmd");
::is( scalar($file3->nik), "mmd", "\$file3->nik;					# mmd" );
::is( scalar($file3->name), "mmd", "\$file3->name;					# mmd" );
::is( scalar($file3->file), "mmd", "\$file3->file;					# mmd" );
::is( scalar($file3->dir), "", "\$file3->dir;					#" );
::is( scalar($file3->ext), "", "\$file3->ext;					#" );
::is( scalar($file3->exts), "", "\$file3->exts;					#" );

::is( scalar($file3->dir("dir")->path), "dir/mmd", "\$file3->dir(\"dir\")->path;		# dir/mmd" );
::is( scalar($file3->file("mmx")->path), "mmx", "\$file3->file(\"mmx\")->path;		# mmx" );
::is( scalar($file3->nik("mmx")->path), "mmx", "\$file3->nik(\"mmx\")->path;		# mmx" );
::is( scalar($file3->name("mmx")->path), "mmx", "\$file3->name(\"mmx\")->path;		# mmx" );
::is( scalar($file3->ext("wxt")->path), "mmd.wxt", "\$file3->ext(\"wxt\")->path;		# mmd.wxt" );
::is( scalar($file3->exts("wxt")->path), "mmd.wxt", "\$file3->exts(\"wxt\")->path;		# mmd.wxt" );

print "=== sub" . "\n";
my $file1 = $app->file("mm1", "mm2/");
my $file2 = $file1->sub("uni.ext");

::is( scalar($file2->path(0)), "mm1/uni.ext", "\$file2->path(0);		# mm1/uni.ext" );
::is( scalar($file2->path(1)), "mm2/uni.ext", "\$file2->path(1);		# mm2/uni.ext" );

::is( scalar($file1->path(0)), "mm1", "\$file1->path(0);		# mm1" );

print "=== pwd и cwd" . "\n";
::like( scalar($app->file->pwd), qr{rubin-forms}, "\$app->file->pwd;		#~ rubin-forms" );

print "=== abs" . "\n";
::is( scalar($app->file("/./abc/./x")->abs->path), "/abc/x", "\$app->file(\"/./abc/./x\")->abs->path;			# /abc/x" );
::is( scalar($app->file("/./abc/../x")->abs->path), "/x", "\$app->file(\"/./abc/../x\")->abs->path;			# /x" );
::is( scalar($app->file("/abc/y/../../x/..")->abs->path), "/", "\$app->file(\"/abc/y/../../x/..\")->abs->path;		# /" );
::is_deeply( scalar($app->file(".")->abs->path), scalar($app->file->pwd), "\$app->file(\".\")->abs->path;						## \$app->file->pwd" );
::is( scalar($app->file("/")->abs->path), "/", "\$app->file(\"/\")->abs->path;						# /" );
::is_deeply( scalar($app->file("x/y")->abs->path), scalar($app->file->pwd . "/x/y"), "\$app->file(\"x/y\")->abs->path;					## \$app->file->pwd . \"/x/y\"" );

::is( scalar($app->file("x/y")->abs("/a")->path), "/a/x/y", "\$app->file(\"x/y\")->abs(\"/a\")->path;				# /a/x/y" );
::is( scalar($app->file("x/y")->abs("a")->path), "a/x/y", "\$app->file(\"x/y\")->abs(\"a\")->path;				# a/x/y" );
::is( scalar($app->file("x/y")->abs("")->path), "x/y", "\$app->file(\"x/y\")->abs(\"\")->path;				# x/y" );

print "=== norm" . "\n";
eval { $app->file("../abc/./x")->norm->path }; ::like( scalar($@), qr{выход за пределы рутовой директории}, "\$app->file(\"../abc/./x\")->norm->path;			#\@ ~ выход за пределы рутовой директории" );
::is( scalar($app->file("./abc/../x")->norm->path), "x", "\$app->file(\"./abc/../x\")->norm->path;			# x" );
::is( scalar($app->file("./abc/./x")->norm->path), "abc/x", "\$app->file(\"./abc/./x\")->norm->path;			# abc/x" );
::is( scalar($app->file("/abc/y/../../x/..")->norm->path), "/", "\$app->file(\"/abc/y/../../x/..\")->norm->path;	# /" );
::is( scalar($app->file("./abc/y/../../x/..")->norm->path), ".", "\$app->file(\"./abc/y/../../x/..\")->norm->path;	# ." );
::is( scalar($app->file(".")->norm->path), ".", "\$app->file(\".\")->norm->path;					# ." );
::is( scalar($app->file("/")->norm->path), "/", "\$app->file(\"/\")->norm->path;					# /" );
::is( scalar($app->file("x/y")->norm->path), "x/y", "\$app->file(\"x/y\")->norm->path;					# x/y" );

print "=== root" . "\n";
::is( scalar($app->file("/x/y")->root("/x")->path), "y", "\$app->file(\"/x/y\")->root(\"/x\")->path;					# y" );
::is( scalar($app->file("/a/y")->root("/x")->path), "/a/y", "\$app->file(\"/a/y\")->root(\"/x\")->path;					# /a/y" );
::is_deeply( scalar($app->file("x/y")->root("/x")->path), scalar($app->file->pwd . "/x/y"), "\$app->file(\"x/y\")->root(\"/x\")->path;					## \$app->file->pwd . \"/x/y\"" );

print "=== as_dir" . "\n";
::is( scalar($app->file("/x/y")->as_dir->path), "/x/y/", "\$app->file(\"/x/y\")->as_dir->path;				# /x/y/" );
::is( scalar($app->file("/x/y/")->as_dir->path), "/x/y/", "\$app->file(\"/x/y/\")->as_dir->path;				# /x/y/" );

print "=== as_file" . "\n";
::is( scalar($app->file("/x/y")->as_file->path), "/x/y", "\$app->file(\"/x/y\")->as_file->path;				# /x/y" );
::is( scalar($app->file("/x/y/")->as_file->path), "/x/y", "\$app->file(\"/x/y/\")->as_file->path;				# /x/y" );
::is_deeply( scalar($app->file("/")->as_file->path), scalar(""), "\$app->file(\"/\")->as_file->path;					## \"\"" );
::is( scalar($app->file("/")->as_file(".")->path), ".", "\$app->file(\"/\")->as_file(\".\")->path;			# ." );

print "== Изменения" . "\n";
print "=== watch" . "\n";
my $file = $app->file("var/man-file")->rm->mkdir;

$file->sub("1")->write("1");
$file->sub("2")->write("2");

my ($changed, $created, $deleted) = $file->watch;

::is( scalar($changed->length), "0", "\$changed->length;		# 0" );
::is( scalar($created->length), "3", "\$created->length;		# 3" );
::is( scalar($deleted->length), "0", "\$deleted->length;		# 0" );

my ($changed, $created, $deleted) = $file->watch;

::is( scalar($changed->length), "0", "\$changed->length;		# 0" );
::is( scalar($created->length), "0", "\$created->length;		# 0" );
::is( scalar($deleted->length), "0", "\$deleted->length;		# 0" );

sleep .1;

$file->sub("3")->write("3");

my ($changed, $created, $deleted) = $file->watch;

::is( scalar($changed->join), "var/man-file", "\$changed->join;			# var/man-file" );
::is( scalar($created->join), "var/man-file/3", "\$created->join;			# var/man-file/3" );
::is( scalar($deleted->length), "0", "\$deleted->length;		# 0" );

::is( scalar($file->length), "1", "\$file->length;			# 1" );

sleep 1;

$file->sub("1")->write("1");
$file->sub("3")->write("3");
$file->sub("2")->rm;

my ($changed, $created, $deleted) = $file->watch;

::like( scalar($changed->join), qr{1}, "\$changed->join;			#~ 1" );
::like( scalar($changed->join), qr{3}, "\$changed->join;			#~ 3" );
::is( scalar($changed->length), "3", "\$changed->length;		# 3" );

::is( scalar($created->length), "0", "\$created->length;			# 0" );
::is( scalar($deleted->join), "var/man-file/2", "\$deleted->join;			# var/man-file/2" );

my $watch = $file->watch;

::is( scalar($watch->changed->length), "0", "\$watch->changed->length;		# 0" );
::is( scalar($watch->created->length), "0", "\$watch->created->length;		# 0" );
::is( scalar($watch->deleted->length), "0", "\$watch->deleted->length;		# 0" );

$file->rm;

print "=== cmpmtime" . "\n";
my $file1 = $app->file("var/man-file1")->rm->mkdir;
my $file2 = $app->file("var/man-file2")->rm->mkdir;

$file2->sub("1")->write("123");
$file2->sub("2")->write("123");
$file2->sub("4")->write("123");

$file1->sub("1")->write("123");
$file1->sub("2")->write("123");
$file1->sub("3")->write("123");

my ($changed, $created, $deleted) = $file1->cmpmtime( $file2 );

::is( scalar($changed->join), "", "\$changed->join;			#" );
::is( scalar($created->join), "var/man-file2/4", "\$created->join;			# var/man-file2/4" );
::is( scalar($deleted->join), "var/man-file2/3", "\$deleted->join;			# var/man-file2/3" );

sleep 1;

$file2->sub("1")->write("abc");
$file1->sub("2")->write("abc");

my ($changed, $created, $deleted) = $file1->cmpmtime( $file2 );

::is( scalar($changed->join), "var/man-file2/1", "\$changed->join;			# var/man-file2/1" );
::is( scalar($created->join), "var/man-file2/4", "\$created->join;			# var/man-file2/4" );
::is( scalar($deleted->join), "var/man-file2/3", "\$deleted->join;			# var/man-file2/3" );

$file1->rm;
$file2->rm;


print "== Поиск" . "\n";
print "=== glob" . "\n";
my $file1 = $app->file("*process*", "*model*")->dir("man");
my $file2 = $file1->glob;

::is( scalar($file1->length), "2", "\$file1->length;			# 2" );
::cmp_ok( scalar($file2->length), '>', "2", "\$file2->length;			#> 2" );
::like( scalar($file2->join(", ")), qr{33-process.man}, "\$file2->join(\", \");		#~ 33-process.man" );

::cmp_ok( scalar($file1->glob("**.man")->length), '>', "2", "\$file1->glob(\"**.man\")->length;				#> 2" );
::cmp_ok( scalar($file1->glob(qr/\.man$/)->length), '>', "2", "\$file1->glob(qr/\\.man\$/)->length;			#> 2" );
::cmp_ok( scalar($file1->glob("-s")->length), '>', "2", "\$file1->glob(\"-s\")->length;						#> 2" );
::cmp_ok( scalar($file1->glob(sub {-f $_})->length), '>', "2", "\$file1->glob(sub {-f \$_})->length;		#> 2" );

::is( scalar($app->file("*", "-f")->glob->eq(-1)->path), "-f", "\$app->file(\"*\", \"-f\")->glob->eq(-1)->path			# -f" );

print "=== find" . "\n";
my $glob = $app->file("man")->find("**.<human,man>")->length;
my $regexp = $app->file("man")->find(qr/\.(hu)?man$/)->length;
my $code = $app->file("man")->find(sub { /\.(hu)?man$/ })->length;
my $test = $app->file("man")->find("-f")->length;

::is_deeply( scalar($glob), scalar($regexp), "\$glob;				## \$regexp" );
::is_deeply( scalar($glob), scalar($code), "\$glob;				## \$code" );
::is_deeply( scalar($glob), scalar($test), "\$glob;				## \$test" );

print "=== grep" . "\n";
my $f = $app->file("man/*.man")->glob;

::is_deeply( scalar($f->grep("**.man")->length), scalar($f->length), "\$f->grep(\"**.man\")->length;				## \$f->length" );
::is_deeply( scalar($f->grep(qr/\.man$/)->length), scalar($f->length), "\$f->grep(qr/\\.man\$/)->length;			## \$f->length" );
::is_deeply( scalar($f->grep("-s")->length), scalar($f->length), "\$f->grep(\"-s\")->length;					## \$f->length" );
::is_deeply( scalar($f->grep(sub {-f $_}, "-s")->length), scalar($f->length), "\$f->grep(sub {-f \$_}, \"-s\")->length;	## \$f->length" );

print "=== filter" . "\n";
my $f = $app->file("man/*")->glob;

::is_deeply( scalar($f->filter(sub { $_->size })->length), scalar($f->length), "\$f->filter(sub { \$_->size })->length	## \$f->length" );


print "=== reverse" . "\n";
::is( scalar($app->file("1", "2")->reverse->path), "2", "\$app->file(\"1\", \"2\")->reverse->path;	# 2" );

print "=== sort" . "\n";
::is( scalar($app->file("1", "3", "2")->sort->join), "1,2,3", "\$app->file(\"1\", \"3\", \"2\")->sort->join;	# 1,2,3" );
::is( scalar($app->file("1", "3", "2")->sort(sub { $b->path cmp $a->path })->join), "3,2,1", "\$app->file(\"1\", \"3\", \"2\")->sort(sub { \$b->path cmp \$a->path })->join;	# 3,2,1" );

print "=== asc и desc" . "\n";
::is( scalar($app->file("10", "3", "2")->ascending(sub { $_->path })->join), "10,2,3", "\$app->file(\"10\", \"3\", \"2\")->ascending(sub { \$_->path })->join;	# 10,2,3" );
::is( scalar($app->file("10", "3", "2")->descending(sub { $_->path })->join), "3,2,10", "\$app->file(\"10\", \"3\", \"2\")->descending(sub { \$_->path })->join;	# 3,2,10" );

::is( scalar($app->file("10", "3", "2")->asc(sub { $_->path })->join), "2,3,10", "\$app->file(\"10\", \"3\", \"2\")->asc(sub { \$_->path })->join;	# 2,3,10" );
::is( scalar($app->file("10", "3", "2")->desc(sub { $_->path })->join), "10,3,2", "\$app->file(\"10\", \"3\", \"2\")->desc(sub { \$_->path })->join;	# 10,3,2" );

print "=== env" . "\n";
::like( scalar($app->file->env("PATH")->sub("/ls")->grep("-f")->path), qr{/bin/ls}, "\$app->file->env(\"PATH\")->sub(\"/ls\")->grep(\"-f\")->path		#~ /bin/ls" );
::like( scalar($app->file->env->sub("/ls")->grep("-f")->path), qr{/bin/ls}, "\$app->file->env->sub(\"/ls\")->grep(\"-f\")->path				#~ /bin/ls" );


print "== Ввод-вывод" . "\n";
print "=== replace" . "\n";
my $file = $app->file("var/man1")->write("123")->replace(sub {
	my $self = shift;
::is( scalar(ref $self), "R::File", "	ref \$self;			# R::File" );
	s/2/5/;
});

::is( scalar($file->read), "153", "\$file->read;			# 153" );

$file = $file->from($file, $app->file("var/man2")->write("123"))->replace(sub {
	my ($self, $idx) = @_;
	s/1/$idx . $self->file/e;
});

::is( scalar($file->read), "0man153", "\$file->read;			# 0man153" );
::is( scalar($file->eq(1)->read), "1man223", "\$file->eq(1)->read;		# 1man223" );

print "=== readln" . "\n";
my $f = $app->file("var/man-file")->write("123/4\n6/890");

::is( scalar(my @lines = $f->sep("/")->readln), "3", "my \@lines = \$f->sep(\"/\")->readln;	# 3" );

::is( scalar($lines[0]), "123", "\$lines[0]		# 123" );
::is( scalar($lines[1]), "4\n6", "\$lines[1]		# 4\n6" );
::is( scalar($lines[2]), "890", "\$lines[2]		# 890" );

::is( scalar(my @lines = $f->chop(0)->readln("/")), "3", "my \@lines = \$f->chop(0)->readln(\"/\");	# 3" );

::is( scalar($lines[0]), "123/", "\$lines[0]		# 123/" );

::is( scalar(my @lines = $f->readln("/", 0)), "3", "my \@lines = \$f->readln(\"/\", 0);	# 3" );

::is( scalar($lines[0]), "123/", "\$lines[0]		# 123/" );

::is( scalar(my @lines = $f->readln), "2", "my \@lines = \$f->readln;		# 2" );

::is( scalar($lines[0]), "123/4", "\$lines[0]		# 123/4" );
::is( scalar($lines[1]), "6/890", "\$lines[1]		# 6/890" );


print "=== Строка в памяти" . "\n";
my $s = "строка в памяти";
::is( scalar($app->file(\$s)->read), "строка в памяти", "\$app->file(\\\$s)->read			# строка в памяти" );


