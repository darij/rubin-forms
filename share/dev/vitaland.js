'use strict'

class App$ {
	constructor() {
		this.form_href = {}
		this.ini_href = null // устанавливается при загрузке
	}
	ini() { return this.ini_href }
	cookies() { return new window.Cookies() }
	form() { return this.form_href }	// хеш с формами
}

App$.prototype.emitti = {
	on: function on(event, fn) {
		
		return fn
	}
}

var app$ = new App$()

var CODE$ = []

var DATA = {}

var ARG$

var _Arr = Array.prototype
var _Obj = Object.prototype
var _own = _Obj.hasOwnProperty

function _extend(child, parent, methods) {
	function F() { this.constructor = child }
	F.prototype = parent.prototype
	var pro = child.prototype = new F()
	child.SUPER = parent.prototype
	if(methods)
		for(var i in methods) pro[i] = methods[i]
	return child
}

var _mixin_counter = 0
function _mixin(extend, ...classes) {
	var cls_name = "_Mixin" + (_mixin_counter++)
	var cls
	eval("cls = class " + cls_name + " extends " + extend.name + " {}")
	for(let mix of classes) {
		for(let k of Object.getOwnPropertyNames(mix.prototype)) 
			if(k !== 'constructor') cls.prototype[k] = mix.prototype[k]
	}
	return cls
}

function Isa(obj, ...classes) {
	for(let cls of classes) {
		if(obj instanceof cls) return true
	}
	return false
}

function _Html() { this.push.apply(this, arguments) }
_extend(_Html, Array, {
	render: function() { return this.join("") }
})

/*
_Obj._apply = function(name, args) {
	return this[name].apply(this, args)
}

_Obj._assign = function() {
	for(var i in this) if(_own.call(this, i)) delete this[i]
	for(var i=0, n=arguments.length; i<n; i+=2) this[arguments[i]] = arguments[i+1]
	this
}
*/

function defined(a) {
	return a != null
}

function keys(a) {
	var r=[]
	for(var i in a) if(_own.call(a, i)) r.push(i)
	return r
}

function values(a) {
	var r=[]
	for(var i in a) if(_own.call(a, i)) r.push(a[i])
	return r
}

function _escape(s) {
	if(typeof s == 'object' && 'render' in s) return s.render();
	return String(s).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/\"/g, '&quot;').replace(/\'/g, '&#39;');
}

function _range(a, b) { var A=new Array(b-a); for(var i=0,n=b-a; i<n; i++) A[i]=a+i; return A }

function _hash2array(h) { var a=[], i; for(i in h) a.push(i, h[i]); return a }

function _pairmap(self, data, cb, from) {
	var A=[], a=data.a, b=data.b
	for(var i=0, n=from.length; i<n; i+=2) { data.a=from[i]; data.b=from[i+1]; A.push(cb.call(self)) }
	data.a=a
	return A
}

function _map(self, data, cb, from) {
	var A=[], a=data.a
	for(var i=0, n=from.length; i<n; i++) { data.a=from[i]; A.push(cb.call(self)) }
	data.a=a
	return A
}

function _grep(self, data, cb, from) {
	var A=[], a=data.a
	for(var i=0, n=from.length; i<n; i++) { data.a=from[i]; if(cb.call(self)) A.push(from[i]) }
	data.a=a
	return A
}

function _reduce(self, data, cb, from) {
	var a=data.a, b=data.b
	data.a = from[0]
	for(var i=1, n=from.length; i<n; i++) { data.b=from[i]; data.a=cb.call(self) }
	var A=data.a
	data.a=a; data.b=b
	return A
}

function _sort(self, data, cb, from) {
	var a = data.a, b = data.b
	from.sort(function(a,b) { data.a=a; data.b=b; return cb.call(self) })
	data.a = a; data.b = b
	return from
}

function join() { var a=_Arr.slice.call(arguments); return a.join(a.shift()) }
function split(a, s) { return s.split(a) }

function _push() { var a=_Arr.slice.call(arguments); return a.push.apply(a.shift(), a) }
function _unshift() { var a=_Arr.slice.call(arguments); return a.unshift.apply(a.shift(), a) }
function _splice() { var a=_Arr.slice.call(arguments); return a.splice.apply(a.shift(), a) }
function _pop(a) { return a.pop() }
function _shift(a) { return a.shift() }


if(!RegExp.escape)
	RegExp.escape = function(s) {
		return s.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
	};

var app_OUTPUT = [];


