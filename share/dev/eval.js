// сервер для выполнения файлов js

var http = require('http')
var url = require('url')
var fs = require('fs')

function say() { console.log(...arguments); return arguments[arguments.length-1]; }


var server = http.createServer(function(q, r) {
	say("node-request", q.method)
	if (q.method == 'POST') {
		let __param__ = [];
		q.on('data', data => { __param__.push(data) })
		q.on('end', () => on_end(r, () => {
				let x = JSON.parse(__param__.join(""))
				let res = eval(x.code)
				return { res: res }
			})
		)
	}
	else {
		on_end(r, function() {
			var u = url.parse(q.url, true)
			var path = u.query.path
			var res = require(path)
			return { res: res }
		})
	}
})

server.listen(__port__, (err) => {
    if (err) {
        return say('something bad happened', err)
    }
    say("Node evaler is listening on " + __port__)
})


function on_end(r, fn) {
	try {
		_send(r, fn())
	} catch(e) {
		_send(r, { error: e.message, stack: ''+e.stack })
	}
}

function _send(r, x) {
	//say("_send:", x, typeof x)
	var json = JSON.stringify(x, (k, v) => {		
		var x= v instanceof RegExp? v.toString(): 
		typeof v === 'function' || v instanceof Function? "[Function: "+v.name+"]":
		typeof v === 'object' && !'toJSON' in v? "[Object: "+v.toString()+"]":
		v === undefined? "[Undefined]":
		typeof v === 'number' && isNaN(v)? "[NaN]":
		Infinity === v? "[Infinity]":
		-Infinity === v? "[-Infinity]":
		v;
		
		//say(k, v, x, typeof v)
		return x
	})
	if(typeof json !== "string") json = JSON.stringify({ error: ''+x })
	if(typeof json !== "string") json = JSON.stringify({ error: "JSON pack error!" })
	//say("_send-json:", json, typeof json)
	r.end(json, 'utf-8')
}