// сервер для компилляции coffee в js

var coffeescript = require("iced-coffee-script/lib/coffee-script");
var http = require('http')
var url = require('url')
var fs = require('fs')
var server = http.createServer(function(q, r) {

	try {
		//r.setHeader('Content-Type', 'application/json')

		
		var u = url.parse(q.url, true)
		var path = u.query.path
		var out = u.query.out || path.replace(/\.\w+$/, '.js')
		
		var code = fs.readFileSync(path, "utf8")
					
		//{js, v3SourceMap, sourceMap}
		result = coffeescript.compile(code, {
			filename: path,
			//transpile: opt,
			sourceMap: true,
			inlineMap: false,
			bare: false,
			header: true,
			literate: /litcoffee$/.test(path)
		})
		
		fs.writeFileSync(out, result.js)
		fs.writeFileSync(out.replace(/\.\w+$/, '.js.map'), JSON.stringify(result.sourceMap))
		
		r.end('ok')
	} catch(e) {
		console.log(''+e)
		r.end('err')
	}
})

server.listen(port, (err) => {
    if (err) {
        return console.log('something bad happened', err)
    }
    console.log("CoffeeScript Compiler is listening on " + port)
})
