export PATH=.:/usr/local/bin:/usr/local/sbin:/bin:/sbin:/usr/sbin:~/bin:$PATH

#export PATH=.:/bin:/sbin:/usr/sbin:/usr:/usr/local/bin:/usr/local/sbin:$PATH:/cygdrive/c/lang/Julia-0.3.11/bin:/cygdrive/c/lang/ScriptBasic/bin

export LANG=ru_RU.UTF-8
export EDITOR=mcedit
export PAGER=less


# для nodejs
export FORCE_COLOR=true

alias ls='ls --color'

parse_git_branch() {
    git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/ \1/'
}
export PS1="\[\e[30m\]\u@\[\e[32m\]\h \[\e[34m\]\w\[\e[31m\]\$(parse_git_branch )\[\e[00m\]\n$ "

 