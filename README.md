# Приложение
## Меню
1. [Приложение](#Приложение)
2. [Инициализация](#Инициализация)
3. [Установка](#Установка)
4. [Использование](#Использование)
5. [Быстрый старт](#Быстрый старт)
6. [Структура проекта](#Структура проекта)
7. [Принципы](#Принципы)
8. [Методология ХР](#Методология ХР)
9. [Идеология `rubin-forms`](#Идеология `rubin-forms`)
10. [Сервисы](#Сервисы)
11. [Обзор функциональности](#Обзор функциональности)
12. [R::App](#R::App)
13. [R::Project](#R::Project)
14. [R::Framework](#R::Framework)
15. [R::Ini](#R::Ini)
16. [R::File](#R::File)
17. [Настройка сервисов](#Настройка сервисов)
18. [Сервисы в отдельном процессе](#Сервисы в отдельном процессе)
19. [Утилиты](#Утилиты)
20. [HTTP-сервер](#HTTP-сервер)
21. [Типы](#Типы)
22. [Документация](#Документация)

## Инициализация



```perl
use common::sense;
use R::App;

$app->man->conf;

#my $act = $app->act(inc => ["var/.miu/root/act"]);


```

## Установка

```sh
$ git clone git@bitbucket.org:darij/rubin-forms.git
$ cd rubin-forms
$ cpm install -g
$ sudo perl rubin-forms mkage /bin
```

Если вы хотите установить в домашнюю директорию, то вместо последней команды:

```sh
$ perl rubin-forms mkage ~/bin
```

и убедитесь, что каталог `~/bin` прописан в `$PATH`.


## Использование

После установки у Вас появится команда `al`. Это утилита вроде `make` или `rake`. Посмотреть список команд:

```sh
$ al help
```

Посмотреть команды соответствующие маске (маска - регулярка `perl`):

```sh
$ al help маска
```

Расширенная информация о команде:

```sh
$ al -help команда
```

## Быстрый старт

Новый проект создаётся командой:

```sh
$ al mk Мой_проект.project
```

Появится папка `Мой_проект`.

### Структура проекта

```
| etc/ - папка с конфигурационными файлами
|	Мой_проект.yml - конфигурационный файл
|	user.yml - должен быть вне системы контроля версий
| lib/ - библиотеки perl (могут и не понадобится)
| html/ - статика
|   sprite/ - спрайты. `al sprite` перегенерирует спрайты
|     sprite1/
|       картинка1.png
|       картинка2.jpg
|   sign/ - шрифты. Загружаются автоматически
|     шрифт1/
|       шрифт1.css - в шаблонах `&какой-то-символ;` заменяется на `<span class='какой-то-символ'>&nbsp;</span>`
|       шрифт1.oft
| model/ - модели
|   User.pm
| make/ - задания
|   модуль_с_заданиями1.pm
| snippet/ - сниппеты
|   я_сниппет1/ - `al mk Имя.я_сниппет1`
|   я_сниппет2.pl - `al mk Имя.я_сниппет2`
| view/ - шаблоны на языке Lukull (вместо контроллеров используются формы, находящиеся так же в шаблонах)
|   layout.html
|   menu.html
|   index.html
```

## Принципы

Принципы на которых строится фреймворк:

1. Всё сразу сделать нельзя - начни с малого.
1. Деньги, заработанные сегодня, стоят дороже, чем деньги, заработанные завтра.
1. KISS - упрощай.
1. YAGNI - только то что нужно сейчас.
1. WIB - Чем хуже, тем лучше (Worse is better, New Jersey style)
1. DRY - каждая часть знания должна иметь единственное, непротиворечивое и авторитетное представление в рамках системы

### Методология ХР

Для разработки была выбрана методология ХР.

1. Короткий цикл обратной связи (Fine-scale feedback)
* Разработка через тестирование (Test-driven development) TDD
* Игра в планирование (Planning game) - пожелания заказчика записывается на карточки (customer stories) и приблизительный план работы на итерацию (производство следующей версии продукта)
* Заказчик всегда рядом (Whole team, Onsite customer) - заказчик, это пользователь, а не директор
* Парное программирование (Pair programming)
1. Непрерывный, а не пакетный процесс
* Непрерывная интеграция (Continuous integration)
* Рефакторинг (Design improvement, Refactoring)
* Частые небольшие релизы (Small releases) - деньги, заработанные сегодня, стоят дороже, чем деньги, заработанные завтра
1. Понимание, разделяемое всеми
* Простота (Simple design) - использовать наиболее простой дизайн
* Метафора системы - как устроена система
* Коллективное владение кодом (Collective code ownership) или выбранными шаблонами проектирования (Collective patterns ownership)
* Стандарт кодирования (Coding standard or Coding conventions)
1. Социальная защищённость программиста (Programmer welfare):
* 40-часовая рабочая неделя (Sustainable pace, Forty-hour week)

### Идеология `rubin-forms`

Приложение состоит из модели данных (ORM) и шаблонов. В шаблонах находятся формы, осуществляющие просмотр данных их валидацию и сохранение. Формы состоят из полей ввода. Формы могут работать через перезагрузку страницы, ajax и веб-сокеты.

Шаблоны написаны на языке Lukull.

Методы классов начинающиеся на `sub` транслируются в код `perl` и работают на сервере, а на `def` - в код Javascript и соответственно работают в браузере. Иерархия классов одинакова как для серверной, так и для клиентской части.

## Сервисы

`use R::App;` добавляет в пакет переменную `$app` и значением `bless {}, R::App`.
Любой метод у `$app` без параметров вернёт сервис, создав его, если сервиса нет.

Сервисом является объект с настройками указанными в `etc/имя-пректа.yml` в секции `services:`.

Если конфигурации для данного метода нет, то имя метода преобразуется в путь и имя класса. Например, `$app->addService` создаст экземпляр класса `Add::Service->new` подгрузив его по пути `lib/Add/Service.pm` в проекте или фреймворке.

Если вызывать метод с параметрами, то будет создан экземпляр класса сервиса с указанными параметрами.

## Обзор функциональности

### R::App

В проекте доступна суперглобальная переменная `$app`. Её же возвращает константа `app`:


```perl
ref $app            # R::App
ref app             ## ref $app
app                 ##== $app

```

Метод вызванный у `$app` впервые подгрузит класс `R::Метод` из соответствующего файла `R/Метод.pm` в `@INC`, создаст его объект (через конструктор `new`), а затем будет всегда его возвращаеть.


```perl
$app->raise						##== $app->raise
ref $app->raise					# R::Raise
exists $INC{"R/Raise.pm"}		# 1

```

Если передать параметры, то это будет эквивалентно вызову конструктора:


```perl
$app->raise->new(1)				##!= $app->raise
$app->raise(1)					##!= $app->raise


```

Параметры для любого сервиса можно добавить в ini-файле. См. [Настройка сервисов](#Настройка сервисов)

### R::Project


```perl
$app->project->name			# rubin-forms
$app->project->root			#~ /
$app->project->yes			# 1

```

### R::Framework


```perl
$app->framework->name			# rubin-forms
$app->framework->root			#~ /


```

### R::Ini

Подгружает файл `<папка проекта>/<имя папки проекта>.ini` или `<папка проекта>/etc/<имя папки проекта>.ini`, парсит и возвращает хэш.
Вместо ini-файлов можно использовать yml-файлы.


```perl
$app->ini->{project}{company}		# Emerald Labs

```

### R::File

Для создания файлов можно применять `$app->file`


```perl
$app->file("/")->isdir		# 1

```

## Настройка сервисов

В ini-файле есть секция `[service]`. В этой секции описываются сервисы доступные через `app->сервис` с параметрами.


```perl
$app->myservice1	#@ ~ Can't locate R/Myservice1.pm in \@INC

$app->ini->{service}{myservice2} = "~raise(msg=>123, who=>456)";

ref $app->myservice2		# R::Raise
$app->myservice2->{msg}		# 123
$app->myservice2->{who}		# 456


```

## Сервисы в отдельном процессе

Сервис может быть запущен в отдельном процессе. Для него нужно создать сервис-заглушку на основе `R::Rpc`:


```perl
$app->ini->{service}{xfile} = {
	'~class' => "R::Rpc",
	service => 'file',
	port => 9043,
	name => 'xfile',
	workers => 3,
};

my $kill_ps = R::guard {
	# Останавливаем процесс:
	$app->xfile->{ps}->exists	# 1
	$app->xfile->{ps}->kill("INT")->wait;
	$app->xfile->{ps}->exists   ## undef
};

$app->xfile->add("man")->length 		# 1

```

`add("man")` выполнится в отдельном процессе, однако будет возвращён объект файл и `length` выполнится уже в главном процессе.


```perl
$app->xfile->encode("cp1251")->encode	# cp1251

```

Все ошибки передаются в текущий процесс и выполняются.


```perl
$app->xfile->__no_method	#@ ~ __no_method

undef $kill_ps;

```

## Утилиты

### HTTP-сервер

Создаём сервер и получаем охранное значение.


```perl
my $s = server { "hi!" . $_->path } port => 9001;
my $sv = $s->sv;

$app->url("localhost:9001")->read			# hi!/
$app->url("localhost:9001/xxx")->read		# hi!/xxx

```

Как только `$sv` c охранным значением выйдет из области видимости - сервер будет остановлен.


```perl
undef $sv;

```

### Типы


```perl
Is undef, "Str" # 

$_ = -10;
Is "Int"	# 1

Is [1,2, undef], "[Int Undef]"	# 1
Is [1,2], "[Int Undef]"			# 1
Is [], "[Int Undef]"			# 1
Is [1,2,[]], "[Int Undef]"		# 

IsErr [1,2,[]], "[Int Undef]"	#  значение `[ 1, 2, [] ]` не типа [Int Undef]

subenum ImportantEnum => [1,2,3];
subenum UnimportantEnum => [4,5,6];

Is 1, "ImportantEnum"			# 1
Is 2, "ImportantEnum"			# 1
Is 3, "ImportantEnum"			# 1
Is 4, "ImportantEnum"			#
Is 4, "UnimportantEnum"			# 1

```

Рекурсивные типы:


```perl
subtype Nix => "ARRAY[Nix] Num" => "use Nix! %dump is not Nix!";

Is [1, [3, [4, []], [5]], 2], "Nix"		# 1

IsErr {a=>1}, "Nix"			# use Nix! { 'a' => 1 } is not Nix!

```

В пустом контексте IsErr выбросит исключение.


```perl
eval {
	IsErr {a=>1}, "Nix"
};
$@		#~ use Nix! \{ 'a' => 1 \} is not Nix!

```

Регулярки:


```perl
subtype Email => qr/@/;

Is "a\@a.a", "Email"		# 1
Is "a.a.a", "Email"			# 

```

Функции в типах:


```perl
subtype Ten => "min(10)";

Is 20, "Ten"			# 1
Is 5, "Ten"				#

subtype TenMLen => "maxlen(10)";

Is [1..20], "TenMLen"			# 
Is [1..10], "TenMLen"			# 1
Is +{1..20}, "TenMLen"			# 1
Is +{1..21}, "TenMLen"			# 

Is "1234567890", "TenMLen"		# 1
Is "1234567890-", "TenMLen"		# 

Is 10, "between(10,20)"			# 1
Is 10, "between(11,20)"			# 

Is "abc", "len(0,3)"			# 1
Is "abc", "len(3)"				# 1
Is "abcx", "len(3)"				# 


```



## Документация

1. [Приложение](mark/00-app.markdown)
1. [ORM - объектно-реляционная модель](mark/01-model.markdown)
1. [Типы полей](mark/02-model-types.markdown)
1. [Метаинформация о структуре модели](mark/03-model-meta.markdown)
1. [Коннект к базе](mark/05-connect.markdown)
1. [Шаблонизатор](mark/06-view.markdown)
1. [Язык шаблонизатора R::View](mark/07-view-lang.markdown)
1. [Сессии](mark/11-session.markdown)
1. [Fulminant](mark/12-fulminant.markdown)
1. [Сервер разработки](mark/14-dev.markdown)
1. [i18n - локализация](mark/16-i18n.markdown)
1. [Формы](mark/20-form.markdown)
1. [Поля ввода в формах](mark/21-form-input.markdown)
1. [Теги в формах](mark/22-form-tag.markdown)
1. [Пейджеры](mark/23-pages.markdown)
1. [Файлы](mark/30-file.markdown)
1. [Урлы](mark/31-url.markdown)
1. [Процессы](mark/33-process.markdown)
1. [Волокна](mark/34-coro.markdown)
1. [Mime](mark/38-mime.markdown)
1. [Трейсбэк](mark/40-raise.markdown)
1. [R::Html и R::Htmlquery](mark/41-html.markdown)
1. [Вспомогательные функции](mark/42-perl.markdown)
1. [Coro](mark/44-test-cor_o.markdown)
