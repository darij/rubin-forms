try { eval("R$Form$Form") } catch(e) { eval("function R$Form$Form() {}") }
try { eval("R$Form$Input") } catch(e) { eval("function R$Form$Input() {}") }
'use strict'

class App$ {
	constructor() {
		this.form_href = {}
		this.ini_href = null // устанавливается при загрузке
	}
	ini() { return this.ini_href }
	cookies() { return new window.Cookies() }
	form() { return this.form_href }	// хеш с формами
}

var app$ = new App$()

var CODE$ = []

var DATA = {}

var ARG$

var _Arr = Array.prototype
var _Obj = Object.prototype
var _own = _Obj.hasOwnProperty

function _extend(child, parent, methods) {
	function F() { this.constructor = child }
	F.prototype = parent.prototype
	var pro = child.prototype = new F()
	child.SUPER = parent.prototype
	if(methods)
		for(var i in methods) pro[i] = methods[i]
	return child
}

var _mixin_counter = 0
function _mixin(extend, ...classes) {
	var cls_name = "_Mixin" + (_mixin_counter++)
	var cls
	eval("cls = class " + cls_name + " extends " + extend.name + " {}")
	for(let mix of classes) {
		for(let k of Object.getOwnPropertyNames(mix.prototype)) 
			if(k !== 'constructor') cls.prototype[k] = mix.prototype[k]
	}
	return cls
}

function Isa(obj, ...classes) {
	for(let cls of classes) {
		if(obj instanceof cls) return true
	}
	return false
}

function _Html() { this.push.apply(this, arguments) }
_extend(_Html, Array, {
	render: function() { return this.join("") }
})

/*
_Obj._apply = function(name, args) {
	return this[name].apply(this, args)
}

_Obj._assign = function() {
	for(var i in this) if(_own.call(this, i)) delete this[i]
	for(var i=0, n=arguments.length; i<n; i+=2) this[arguments[i]] = arguments[i+1]
	this
}
*/

function defined(a) {
	return a != null
}

function keys(a) {
	var r=[]
	for(var i in a) if(_own.call(a, i)) r.push(i)
	return r
}

function values(a) {
	var r=[]
	for(var i in a) if(_own.call(a, i)) r.push(a[i])
	return r
}

function _escape(s) {
	if(typeof s == 'object' && 'render' in s) return s.render();
	return String(s).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/\"/g, '&quot;').replace(/\'/g, '&#39;');
}

function _range(a, b) { var A=new Array(b-a); for(var i=0,n=b-a; i<n; i++) A[i]=a+i; return A }

function _hash2array(h) { var a=[], i; for(i in h) a.push(i, h[i]); return a }

function _pairmap(self, data, cb, from) {
	var A=[], a=data.a, b=data.b
	for(var i=0, n=from.length; i<n; i+=2) { data.a=from[i]; data.b=from[i+1]; A.push(cb.call(self)) }
	data.a=a
	return A
}

function _map(self, data, cb, from) {
	var A=[], a=data.a
	for(var i=0, n=from.length; i<n; i++) { data.a=from[i]; A.push(cb.call(self)) }
	data.a=a
	return A
}

function _grep(self, data, cb, from) {
	var A=[], a=data.a
	for(var i=0, n=from.length; i<n; i++) { data.a=from[i]; if(cb.call(self)) A.push(from[i]) }
	data.a=a
	return A
}

function _reduce(self, data, cb, from) {
	var a=data.a, b=data.b
	data.a = from[0]
	for(var i=1, n=from.length; i<n; i++) { data.b=from[i]; data.a=cb.call(self) }
	var A=data.a
	data.a=a; data.b=b
	return A
}

function _sort(self, data, cb, from) {
	var a = data.a, b = data.b
	from.sort(function(a,b) { data.a=a; data.b=b; return cb.call(self) })
	data.a = a; data.b = b
	return from
}

function join() { var a=_Arr.slice.call(arguments); return a.join(a.shift()) }
function split(a, s) { return s.split(a) }

function _push() { var a=_Arr.slice.call(arguments); return a.push.apply(a.shift(), a) }
function _unshift() { var a=_Arr.slice.call(arguments); return a.unshift.apply(a.shift(), a) }
function _splice() { var a=_Arr.slice.call(arguments); return a.splice.apply(a.shift(), a) }
function _pop(a) { return a.pop() }
function _shift(a) { return a.shift() }


if(!RegExp.escape)
	RegExp.escape = function(s) {
		return s.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
	};

var app_OUTPUT = [];



app$.ini_href = {"DDP":{"class":{"expand":"all","inherited":"all"},"colored":1,"deparse":1,"print_escapes":1,"show_readonly":1,"show_unicode":0},"coro":{"AnyEventModel":"EV","patch":"http"},"dev":1,"include":["user.yml"],"make":{"win":{"clipboard-paths":["/home/dart/__/rubin-forms","/home/dart/__/unicorn","/home/dart/__/miu"]}},"project":{"author":"Kosmina O. Yaroslav","company":"Emerald Labs","license":"BSD-2-Clause"},"service":{"aura":{"dev":0,"port":9011},"connect":{"basename":"rubin-forms","password":123,"user":"root"},"dev":{"gzip":0,"keep_alive":0,"log":null,"log_headers":0,"port":3000,"projects":{"rubin-forms":{"dir":"/home/dart/__/rubin-forms","subordinates":["unicorn"],"watch":["etc/user.yml","etc/rubin-forms.yml","lib","share","view"]},"unicorn":{"dir":"/home/dart/__/unicorn","init":"#$app->connect->logon;\nmy $sv_unicorn = app->aura(%{app->aura}, name=>\"unicorn\", base_dev=>0, dev=>0, port=>3001)->run->sv;\nmy $sv_dev = app->aura(%{app->aura}, name=>\"unicorn-dev\", base_dev=>1, dev=>0, port=>3002, init=>0)->run->sv;\n","proxy":"localhost:3001","run":"sleep 1000_000_000","static":0,"watch":["etc","model","view","html/css","html/js","html/img","html/sprite","html/sign"]},"unicorn-dev":{"inherits":"unicorn","proxy":"localhost:3002","static":0}},"std":0},"editor":{"cmd":"wine \"c:\\\\Program Files\\\\Notepad++\\\\notepad++.exe\" \"%s\" -n%l","win":0},"mail":{"sendmail":"/usr/sbin/sendmail -t"},"meta":{"addtestdata":"~ini:dev","base":"model","connect":"~connect","name":"R","~class":"R::Model::Metafieldset"},"model":{"meta":"~meta"},"perl":{"cipher":{"cost":"8","key":"&U S[\\M\\@Plc+!\t\n","salt":"&U9i*0Vv^"}},"session":{"prefix":"","store":"~session_cache"},"session_cache":{"~class":"R::Cache::Vinyl"},"smtp":{"pass":null,"port":465,"smtp":"smtp.example.com","timeout":60,"user":"user@example.com","~class":"R::Mail"}}}
window.Cookies = class Cookies {
constructor() { var DATA = {};; DATA.w = arguments[0]; 
		this.w = DATA.w || window;
	}
set() { var DATA = {};; DATA.k = arguments[0]; DATA.v = arguments[1]; DATA.options = arguments[2]; 
if( !DATA.options ) { DATA.options = {}};
if( typeof( DATA.options.expires )+'' == ''+ "number") {
			DATA.days = DATA.options.expires;
			DATA.t = DATA.options.expires = new window.Date;
			DATA.t.setTime(  -0 + + DATA.t  -0 + +  DATA.days * 864e+5 );
		};

		this.w.document.cookie = [
			encodeURIComponent(DATA.k), "=", encodeURIComponent(DATA.v),
(( DATA.options.expires 	)? (()=>{ return "; expires="  +''+  DATA.options.expires.toUTCString() })(): (()=>{ return "" })()), // use expires attribute, max-age is not supported by IE
(( DATA.options.path 		)? (()=>{ return "; path="  +''+  DATA.options.path })(): (()=>{ return "; path=/" })()),
(( DATA.options.domain 	)? (()=>{ return "; domain="  +''+  DATA.options.domain })(): (()=>{ return "" })()),
(( DATA.options.secure  	)? (()=>{ return "; secure" })(): (()=>{ return "" })()),
		].join("");

		return this;
	}
clear() { var DATA = {};; DATA.k = arguments[0];  return this.set(DATA.k, null, { expires: 0 })}
clearAll() { var DATA = {};; 
		var A_k= this.getAll() ; for(var K_k in A_k) { if(_own.call(A_k, K_k)) { DATA.k=K_k; DATA.v=A_k[K_k];  this.clear(DATA.k)}};
		return this;
	}
setAll() { var DATA = {};; DATA.cookies = arguments[0]; 
		var A_k= DATA.cookies ; for(var K_k in A_k) { if(_own.call(A_k, K_k)) { DATA.k=K_k; DATA.v=A_k[K_k];  this.set(DATA.k, DATA.v)}};
		return this;
	}
get() { var DATA = {};; DATA.k = arguments[0];  return this.getAll()[DATA.k]}
getAll() { var DATA = {};; 
		DATA.cookies = this.w.document.cookie.split("; ");
		DATA.ret = {};
		var A_cookie= DATA.cookies; for(var I_cookie=0, N_cookie=A_cookie.length; I_cookie<N_cookie; I_cookie++) { DATA.cookie=A_cookie[I_cookie]; 
			DATA.parts = DATA.cookie.split("=");
			DATA.name = decodeURIComponent(DATA.parts.shift());
			DATA.s = DATA.parts.join("=");
if(  DATA.s.indexOf("\"") === 0) {
				// This is a quoted cookie as according to RFC2068, unescape...
				DATA.s = DATA.s.slice(1, -1);
				DATA.s = DATA.s.replace(/\\"/g, "\"");
				DATA.s = DATA.s.replace(/\\/g, "\\\\");
			};

			DATA.ret[DATA.name] = decodeURIComponent(DATA.s.replace(/\+/g, " "));
		};

		return DATA.ret;
	}
}
window.RubinForm = class RubinForm extends R$Form$Form {
constructor() { var DATA = {};; DATA.setup = arguments[0]; 
		super(...arguments);
		var A_k= DATA.setup ; for(var K_k in A_k) { if(_own.call(A_k, K_k)) { DATA.k=K_k; DATA.v=A_k[K_k];  this[DATA.k] = DATA.v}};

		app$.form()[this.Name] = this;		// регистрируем форму в app

		this.inputset = DATA.inputset = [];	// множество полей ввода
		this.inputs = DATA.inputs = {};

		var A_data= DATA.setup.inputset; for(var I_data=0, N_data=A_data.length; I_data<N_data; I_data++) { DATA.data=A_data[I_data]; 
if( !(DATA.cls = window[DATA.data.cls]) ) { throw "нет класса "+ DATA.data.cls +""};
			DATA.input = new DATA.cls(DATA.data);
			DATA.input.form = this;
			DATA.inputset.push(DATA.inputs[DATA.input.name] = DATA.input);
		};
		this.info = DATA.info;			// тег формы в jQuery
		this.error = DATA.error;		// хеш ошибок к полям формы
		this.errors = DATA.errors;		// дополнительные ошибки формы
	}
mountDiv() { var DATA = {};;  this.div = jQuery("#"  +''+  this.Name); this.start(); return this}
mountEnd() { var DATA = {};; 
		var A_input= this.inputset ; for(var I_input=0, N_input=A_input.length; I_input<N_input; I_input++) { DATA.input=A_input[I_input];  DATA.input.mountEnd()};
		this.end();
		return this;
	}
mountElement() { var DATA = {};; return DATA.input = arguments[0]; }
start() { var DATA = {};return ; }
end() { var DATA = {};return ; }
at() { var DATA = {};; DATA.name = arguments[0];  return this.inputs[DATA.name]}
hasAllow() { var DATA = {};;  return this.allow}
}
window.RubinForms = class RubinForms extends RubinForm {
}
window.EmeraldForm = class EmeraldForm extends RubinForm {
mountElement() { var DATA = {};; DATA.input = arguments[0]; 
		DATA.input.div.on("change" , (function() { return this.submit()}).bind(this));
		return DATA.input.div.on("keyup" , (function() {DATA.e = arguments[0]; 
if( DATA.e.which()  -0 == +  13 ) { this.submit()};
		}).bind(this));
	}
}
window.EditForm = class EditForm extends RubinForm {
mountDiv() { var DATA = {};; 
		super.mountDiv();

if( this.allow) {
			this.div.hide();
			jQuery("#Label-"+ this.Name +"").on("click" , (function() {
				this.form_preview().div.toggle("fast");
				return this.form_edit().div.toggle("fast");
			}).bind(this));
		};

		return this;
	}
form_preview() { var DATA = {};;  return (( this.allow )? (()=>{ return app$.form()["Preview-"  +''+  this.Name] })(): (()=>{ return this})())}
form_edit() { var DATA = {};;  return (( this.allow )? (()=>{ return this })(): (()=>{ return app$.form()[this.Name.replace(/^Preview-/, "")]})())}
}
window.ChForm = class ChForm extends RubinForm {
}
window.NativeModelForm = class NativeModelForm {
}
window.LineInput = class LineInput extends R$Form$Input {
constructor() { var DATA = {};; DATA.setup = arguments[0]; 
if( typeof( DATA.setup )!== "object" ) { throw new window.Error("setup is not object: "  +''+  DATA.setup)};
		super(...arguments);
		var A_k= DATA.setup ; for(var K_k in A_k) { if(_own.call(A_k, K_k)) { DATA.k=K_k; DATA.v=A_k[K_k];  this[DATA.k] = DATA.v}};
	}
mountDiv() { var DATA = {};; 
		this.div = jQuery("#"+ this.id() +"");
if( this.div.length === 0 ) { this.mount_error()};
		this.mount();						// монтируем себя
		this.form.mountElement(this);		// сообщаем форме, что монтируемся
		return this;
	}
mount_error() { var DATA = {};;  throw new window.Error("нет #"+ this.id() +"")}
mountEnd() { var DATA = {};; 
if( !this.div ) { this.mountDiv()};
		return this;
	}
mount() { var DATA = {};return ; }
id() { var DATA = {};;  return this._id || (this._id = this.form.Name  +''+  "-"  +''+  this.name)}
}
window.FakeInput = class FakeInput extends LineInput {
}
window.PasswordInput = class PasswordInput extends LineInput {
}
window.ButtonInput = class ButtonInput extends LineInput {
mount_error() { var DATA = {};;  return (( this.form.allow )? (()=>{ return super.mount_error().apply( this, arguments )})(): void(0))}
}
window.SubmitInput = class SubmitInput extends ButtonInput {
}
window.ResetInput = class ResetInput extends ButtonInput {
}
window.EmailInput = class EmailInput extends LineInput {
}
window.NumInput = class NumInput extends LineInput {
}
window.RangeInput = class RangeInput extends LineInput {
}
window.HiddenInput = class HiddenInput extends LineInput {
}
window.TextAreaInput = class TextAreaInput extends LineInput {
mount() { var DATA = {};; 
		this.minheight = this.div.height();
		this.behavior = $("<div class=textarea-behavior></div>").insertBefore(this.div);
		this.slave = $("<div class=textarea></div>").appendTo(this.behavior);

		DATA.setSize = (function() {DATA.e = arguments[0]; 
			(console.log.apply(console, ARG$ = ["setSize", DATA.e]), ARG$[ARG$.length-1]);
			DATA.html = String(this.div.val()).replace(/</g, "&lt;").replace(/>/g, "&gt;").replace(/\r?\n|\r/g, "<br>").replace(/  /g, "&nbsp;");
if( /<br>$/.test(DATA.html) ) { DATA.html  += ''+  "&nbsp;"};
			this.slave.html( DATA.html );
			DATA.height = this.slave.height();
if( DATA.height  -0 < +  this.minheight ) { DATA.height = this.minheight};
			return this.div.height( DATA.height );
		}).bind(this);

if( !this.div.is(":visible")) {
			DATA.hiders = this.div.parents().filter((function() {DATA.idx = arguments[0]; DATA.e = arguments[1];  return DATA.e.style.display === "none"}).bind(this));
			DATA.hiders.show();
			DATA.setSize.call(this);
			DATA.hiders.hide();
		} else {;
			DATA.setSize.call(this);
		};

		(console.log.apply(console, ARG$ = [this.div]), ARG$[ARG$.length-1]);
		this.div.on("input keyup", DATA.setSize);

		return this.div.on("resize" , (function() {
			this.minheight = this.div.height();
			return this.behavior.width( this.div.width() );
		}).bind(this));

	}
}
window.AreaXInput = class AreaXInput extends TextAreaInput {
}
window.AreaInput = class AreaInput extends TextAreaInput {
mount() { var DATA = {};; 
		DATA.child = this.div.parent().children();
		this.pen = $(DATA.child[0]);
		this.show = $(DATA.child[1]);

if( this.div.val() !== "") {
			this.pen.show();
			this.show.show();
			this.div.hide();
		};

		this.pen.on("click" , (function() {
			this.pen.hide();
			this.show.hide();
			return this.div.show();
		}).bind(this));

		return super.mount();
	}
}
window.ArealineInput = class ArealineInput extends TextAreaInput {
}
window.SelectInput = class SelectInput extends LineInput {
}
window.CheckboxInput = class CheckboxInput extends LineInput {
}
window.CheckboxesInput = class CheckboxesInput extends SelectInput {
}
window.MenuInput = class MenuInput extends SelectInput {
}
window.RefInput = class RefInput extends LineInput {
}
window.FileInput = class FileInput extends LineInput {
}
window.ImageInput = class ImageInput extends FileInput {
}
window.ImagesInput = class ImagesInput extends FileInput {
}
window.HtmlInput = class HtmlInput extends LineInput {
mount() { var DATA = {};; 
if( this.allow) {
		new DATA.nicEditor({
			iconsPath: "/js/nicEdit/nicEditorIcons.gif",
			fullPanel: true,
		}).panelInstance(this.id());
	}return ;
}
}
window.RubinPages = class RubinPages extends RubinForm {
}
window.OpalPages = class OpalPages extends RubinPages {
}
window.LabelTag = class LabelTag {
}
window.NideLabelTag = class NideLabelTag extends LabelTag {
}
window.CheckTag = class CheckTag {
}
window.RubinTag = class RubinTag {
}
window.UserTag = class UserTag {
}
window.EnumTag = class EnumTag {
}