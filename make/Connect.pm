package R::Make::Connect;
# главное подключение

use common::sense;
use R::App;
use R::Make;



category "Connect";

name "base_info";
args "[маска таблиц]";
port "распечатывает информацию по базам";
sub base_info {
	my $re = $_[0];
	my $c = $app->connect;
	
	my $info = $c->base_info;
	for my $base (keys %$info) {
		next if $base !~ $re;
		my $txt = $app->perl->color($info->{$base});
		$app->log->info( $base, $txt );
	}
}


name "tab_info";
args "[маска таблиц]";
port "распечатывает информацию по таблицам";
sub tab_info {
	my $re = $_[0];
	my $c = $app->connect;
	
	my $info = $c->tab_info;
	for my $tab (keys %$info) {
		next if $tab !~ $re;
		my $txt = $app->perl->color($info->{$tab});
		$app->log->info( $tab, $txt );
	}
}

name "info";
args "[маска таблиц]";
port "распечатывает информацию по столбцам таблиц";
sub info {
	my $re = $_[0];
	my $c = $app->connect;
	
	my $info = $c->info;
	for my $tab (keys %$info) {
		next if $tab !~ $re;
		my $txt = $app->perl->color($info->{$tab});
		$app->log->info( $tab, $txt );
	}
}

name "index_info";
args "[маска таблиц]";
port "распечатывает информацию по индексам таблиц";
sub index_info {
	my $re = $_[0];
	my $c = $app->connect;
	
	my $info = $c->index_info;
	for my $tab (keys %$info) {
		next if $tab !~ $re;
		my $txt = $app->perl->color($info->{$tab});
		$app->log->info( $tab, $txt );
	}
}

name "fk_info";
args "[маска таблиц]";
port "распечатывает информацию по ссылкам с таблиц";
sub fk_info {
	my $re = $_[0];
	my $c = $app->connect;
	
	my $info = $c->fk_info;
	for my $tab (keys %$info) {
		next if $tab !~ $re;
		my $txt = $app->perl->color($info->{$tab});
		$app->log->info( $tab, $txt );
	}
}

name "inform";
args "[маска таблиц]";
port "распечатывает информацию по таблицам со столбцами, ключами и ссылками";
sub inform {
	my $re = $_[0];
	my $c = $app->connect;
	
	if(!length $re) {
		my $txt = $app->perl->color($c->base->load);
		$app->log->info( $txt );
		return;
	}
	
	my $info = $c->tab_info;
	
	for my $tab (keys %$info) {
		next if $tab !~ $re;
		my $txt = $app->perl->color($c->tab($tab)->load);
		$app->log->info( $tab, $txt );
	}
}



1;