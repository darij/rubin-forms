package R::Make::coro;
# 

use common::sense;
use R::App;
use R::Make;


category "Coro";


name "coro";
args "";
port "возвращает список волокон текущего процесса";
sub coro {
	$app->coro->ps->say;
	$app->coro->lis;
}

name "ae_ls";
args "";
port "список моделей AnyEvent";
sub ae_ls {
	$app->coro;
	my @models = @R::Coro::models;
	
	
	for(@models) {
		my ($package, $model) = @$_;
		my $is_pkg = eval "require $package";
		my $version = ${"$package\::VERSION"};
        my $is_model = eval "require $model";
		$app->log->info(":space", sprintf("%20s", $package), 
			$is_pkg? (":red", "  is"): " " x 4, 
			":cyan", sprintf("%6s", $version),
			$is_model? (":white", "  is"): " " x 4, 
			$AnyEvent::MODEL eq $model? (":green", "  A"): " " x 3,
			" " x 3, ":black bold", $model
		);
	}
}


1;