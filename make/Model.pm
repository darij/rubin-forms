package R::Make::Model;
# команды для работы с моделью

use common::sense;
use R::App;
use R::Make;


category "МОДЕЛЬ";
# models порядок       список моделей
# show модель          печатает столбцы модели
# sync                 синхронизирует базу
# drop                 удаляет все таблицы

name "models";
args "порядок";
port "список моделей";
spec "порядок - если 1, то выводится в порядке загрузки, а не алфавитном";
sub models {

	#$app->modelMetafieldset->load_all_models;

	my @models = @{$app->meta->{fields}};
	@models = sort {$a->{name} cmp $b->{name}} @models unless $_[0];

	my $max = max map {length $_->{name}} @models;
	$max += 2;

	# имена for_m2m - magenta
	# нет pk - on_red
	
	$app->log->info( ":empty", ($_->{pk}? (): ":on_red"), ($_->{for_m2m}? ":magenta": ()), $_->{name}, ":reset", " " x ($max - length $_->{name}), ($_->{remark}? $_->{remark}: ())) for @models;

}


name "show";
args "модель";
port "печатает столбцы модели";
sub show {

	my $fieldset = $app->meta->exists($_[0]);

	my $mask = $_[1];

	$app->log->error("нет модели $_[0]"), exit if !$fieldset;
	my $remark = $fieldset->{remark} // $fieldset->{name};
	$app->log->info("_" x length $remark);
	$app->log->info($remark);
	$app->log->info("");
	
	

	my @fields = @{$fieldset->{fieldset}};
	
	@fields = grep { $_->name =~ /$mask/o } @fields if $mask;

	my $max = 2;
	$max += max map {length $_->name} @fields;

	sub is ($) { ref $_ eq "R::Model::Field::".ucfirst($_[0]) }

	my %color_name = qw(R::Model::Field::Col :reset R::Model::Field::Ref :cyan R::Model::Field::Back :green R::Model::Field::M2m :magenta R::Model::Field::M2n :magenta R::Model::Field::Compute :red);
	$color_name{"R::Model::Field::Assign"} = ":bold";
	my $color_tab = ":cyan";
	my $color_col = ":red";

	my @fld = grep { !is("back") && !is("assign") } @fields;
	my @back = grep { (is("back") || is("assign")) and !$_->ref->fieldset->{for_m2m} } @fields;
	my @back_m2m = grep { (is("back") || is("assign")) and $_->ref->fieldset->{for_m2m} } @fields;
	
	my $show = sub {
		my ($field) = @_;
		my $cls = ref $field;
		
		my ($col, $arr, $info) = "";
		
		if($field->info =~ / /) { ($arr, $info) = ($`, $'); ($info, $col) = ($`, $') if $info =~ /\./; } else { ($arr, $info) = ($field->info, "") }
		
		$app->log->info( ":empty", 
			$color_name{$cls}, 
			$field->name, ":reset", 
			(" " x  ($max - length $field->name)), 
			($arr? ($arr, " ", $color_tab, $info, ":reset", ($col? (".", $color_col, $col): ())): ()), 
			(defined($field->{remark})? (":bold black", "\t", $field->{remark}): () )
		);
	};
	
	$show->($_) for @fld;
	$app->log->info("") if @back;
	$show->($_) for @back;
	$color_name{"R::Model::Field::Back"} = ":yellow";
	$app->log->info("") if @back_m2m;
	$show->($_) for @back_m2m;
	
	my @indexes = values %{$fieldset->{indexes}};
	$app->log->info("") if @indexes;
	$app->log->info(":space blue", $_->name, ":yellow", $_->type, ":green sep", @{$_->idx}) for @indexes;
	
	my @indexrefs = values %{$fieldset->{indexref}};
	$app->log->info("") if @indexrefs;
	$app->log->info(":space red", $_->name, ":sep( -> ) magenta", $_->field->name, ":yellow", $_->fk->name) for @indexrefs;
}

name "sync";
args "";
port "синхронизирует базу";
sub sync {
	#quit "работает только в режиме разработки. Используйте миграции: al migrate <name>; al up" if !$app->ini->{dev};
	
	$app->connect->logon;
	
	$app->meta->sync_for_script;
	#$app->log->info( ":white space", "[", ":red", "Ok", "]", ":white" );
}

name "drop";
args "[test]";
port "удаляет все таблицы";
spec "дополнительный параметр test указывает, что в конфигурационном файле нужно секцию [connect] заменить на [connect::test]";
sub drop {

	quit "операция возможна только в режиме разработки" if !$app->ini->{dev};
	
	$app->connect->logon;

	$app->meta->drop if $app->tty->confirm("Удалить базу и все данные безвозвратно?");

}

name "downorm";
args "каталог";
port "выгрузить базу в виде моделей";
spec "модели будут перезатёрты";
sub downorm {
	my ($dir) = @_;
	quit "укажите каталог в который будут сгружены модели" if !length $dir;
	$app->modelMetafieldset->down($dir);
	print $app->connect->basename . " ... down in $dir\n";
}

name "migrate";
args "[-m] name [port]";
port "создаёт миграцию";
spec "1. port - позволяет добавить комментарий к миграции
2. -m - добавляет в файл миграции все модели из каталога model
3. port - комментарий";
sub migrate {
	my ($name, $port) = grep { $_ ne "-m" } @_;
	
	my $use_model = grep { $_ eq "-m" } @_;
	
	$name = $app->tty->input_or("укажите имя миграции", "power") if !length $name;
	$name = $app->magnitudeLiteral->wordname($name);
	
	my $now = time;
	my $time = $app->perl->strftime("%Y_%m", $now);
	
	my $migrate = "Migrate::_${time}::$name";
	
	my $file = $app->project->file("migrate/$time/$name.pm");
	
	my $files = $app->project->file("migrate/**/$name.pm")->existing;
	
	die "Миграция $name уже существует: " . $files->join if $files->length;
	
	my $dop = $use_model? "my \$oldmeta = \$app->meta->new(\"$migrate\")->migrateload;\n\n\t": "";
	
	my $base = $app->connect->base->load;
	my $orm = $app->meta->scheme;
	
	my $up = $base->diff($orm);
	my $down = $orm->diff($base);
	
	$up =~ s/\n/\n\t/g;
	$down =~ s/\n/\n\t/g;
	
	my $f = $file->mkpath->open(">");
	print $f "package $migrate;
# $port

use common::sense;
use R::App;


# применяет миграцию
sub up {
	${dop}my \$c = \$app->connect;
	
	$up
}


# отменяет миграцию
sub down {
	${dop}my \$c = \$app->connect;
	
	$down
}

";
	
	$app->file("model")->find(sub {
		next if !/\.pm$/;
		my $path = $_;
		$_ = $app->file($_)->read;
		close($f), $file->rm, die "повреждён файл модели $path: нет package" if !s!package\s+${base}::([\w:]+)\s*;!package ${migrate}::$1;!;
		s!$!\n\n!;
		print $f $_;
		0
	}) if $use_model;
	
	print $f "1;";
	close $f;
	
	
	
	$app->log->info(":blue space", "миграция", ":red", $file->path, ":blue", "создана");
}


name "up";
args "[names...]";
port "применяет новые миграции";
spec "names - названия миграций";
sub up {
	
	# алгоритм
	# 1. получить файлы
	# 2. удалить из них применённые
	# 3. взять проименованные
	# 4. применить, на каждом шаге добавляя

	my $is_migrate = $app->connect->tab("_migrate")->exists;
	my $ref = $is_migrate? $app->model->_migrate->refby("id"): {};
	
	my %is = map {$_=>1} @_;
	
	my $filter = @_?
		sub { !$ref->{ $_->name } && $is{ $_->name } }:
		sub { !$ref->{ $_->name } };
	
	my $files = $app->project->base("migrate")->find(qr/\.pm$/)->filter($filter)->asc;
	
	quit "все миграции применены" if $files->length == 0;
	
	
	
	for my $file ($files->slices) {
		
		my ($migrate) = $file->read =~ /^package[ \t]+(\S*);/;
		require $file->dot->path;
		$migrate->up;
		
		my $name = $file->name;
		$app->model->_migrate( $name )->saveAs($name);
		
		$app->log->info(":space reset", sprintf("%10s", $name), ":white", "[", ":green", "Up", ":white", "]");
	}
	
}


name "down";
args "[names...]";
port "отменяет миграции";
spec "names - названия миграций";
sub down {
	
	quit "укажите отменяемые миграции" if !@_;
	

	my $is_migrate = $app->connect->tab("_migrate")->exists;
	my @mig = $is_migrate? $app->model->_migrate->find(id=>[@_])->order("id")->id: ();
	
	quit "все указанные миграции не применены" if @mig == 0;
	
	my %mig = map {$_=>1} @mig;
	
	quit "миграции " . join(", и ", grep { !$mig{$_} } @mig) . " нет в базе" if @mig != @_;
	
	my $files = $app->project->file("migrate")->find("**.pm")->filter(sub { $mig{$_->name} })->asc;
	
	if($files->length != @mig) {
		my %fmig = map {$_=>1} $files->map(sub { $_->name });
		quit "миграции " . join(", и ", grep { !$fmig{$_} } @mig) . " нет на диске";
	}
	
	for my $file ($files->slices) {
		
		require $file->dot->path;
		my ($migrate) = $file->read =~ /^package[ \t]+(\S*);/;
		$migrate->down;
		
		my $name = $file->name;
	
		if($app->connect->tab("_migrate")->exists) {
			$app->model->_migrate( $name )->erase;
		}
		$app->log->info(":space reset", sprintf("%10s", $name), ":white", "[", ":bold black", "Down", ":reset", ":white", "]");
	}
}


# name "migstat";
# args "";
# port "лог миграций";
# spec "";
# sub migdiff {
	# ...
# }


1;