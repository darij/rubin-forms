package R::Make::Process;
# работает с процессами

use common::sense;
use R::App;
use R::Make;

category "Процессы проекта";

name "ps";
args "[name [stop|kill|restart|info]]";
port "список процессов";
sub ps {

	TODO if @_;

	print $app->process->psapp->format;
}

name "kill";
args "[-signal] name1...";
port "убивает процесс по имени";
sub kill {
	my $sig;
	
	$sig = $_[0] =~ s/^-//? shift(): "INT";
	
	$app->process(@_)->kill($sig);
}

name "erase";
args "name1...";
port "удаляет информацию о процессах";
sub erase {
	#$app->process(@_)->find->kill("INT");
	$app->process(@_)->erase(3, 0.1);
}

name "killall";
args "";
port "удаляет все процессы perl";
sub killall {
	for my $line (split /\n/, `ps -a`) {
		$line =~ /(\d+)/;
		my $pid = $1;
		msg("kill -9 $line"), kill -9, $pid if $line =~ /\bperl\b/ && $pid != $$;
	}
}


1;