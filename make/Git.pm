package R::Make::Git;
# добавляет команды для работы с GIT

use common::sense;
use R::App;
use R::Make;
use List::Util qw/max/;


# возвращает текущую ветку
sub current_branch {
    my $branch = `git branch`;
    utf8::decode($branch);
    ($branch) = $branch =~ /^\s*\*\s+(\S+)/m;
    #$app->perl->trim($branch);
    $branch
}

# возвращает каталог проекта git
sub git_add_path {
	"."
}



category "GIT";

name "sta";
args "";
port "статус гит";
sub sta {
	$app->tty->raw;
	run "git status -s";
}

name "reset";
args "";
port "сбрасывает изменения в файлах";
sub reset {
	$app->tty->raw;
	return if !run "git status -s";
	return if !$app->tty->confirm("сбросить?");
	run "git reset --hard HEAD";
}



name "lg";
args "";
port "лог гит";
sub lg {
	$app->tty->raw;
	run "git log --name-only --graph";
}

name "upd";
args "[ветка]";
port "апдейтит из ветви master";
sub upd {
	my $branch = $_[0] // "master";
	$app->tty->raw;
	my $old_branch = current_branch();
	make "commit";
	run "git checkout $branch";
	run "git pull origin $branch";
	run "git checkout $old_branch";
	run "git merge --no-ff $branch";
}


# name "log";
# args "";
# port "лог гит";
# sub log {
	# $app->tty->raw;
	# #run "git log --name-only | cat -";
# }


name "pull";
args "[ветка]";
port "пулл в текущую ветку из такой же на origin";
sub pull {
    my $branch = $_[0] // current_branch();
    $app->tty->raw;
	make "commit";
    run "git pull origin $branch";
}

name "pushf";
args "[комментарий]";
port "пушит первый раз в текущую ветку";
sub pushf {
	make "commit", @_ if @_;

	$app->tty->raw;

    my $branch = current_branch();

    run "git push origin $branch";
}

name "push";
args "[комментарий]";
port "пушит в текущую ветку";
sub push {
	make "commit", @_ if @_;

	$app->tty->raw;

    my $branch = current_branch();

    run "git pull origin $branch && git push origin $branch";
}

name "pushj";
args "[сниппет комментарий | комментарий]";
port "тесты, eslint и пушит";
sub pushj {
	make "commit", @_ if @_;
	
	print run "yarn run test";
	print run "yarn run lint --fix";
	my $x = `git status -s`;
	make "push", length($x)? "eslint": ();
}

name "pushy";
args "[сниппет комментарий | комментарий]";
port "тесты, pylint и пушит";
sub pushy {
	print run "./test";
	make "commit", @_ if @_;
	make "push"
}

name "pusht";
args "[комментарий]";
port "perltidyt и пушит";
sub pusht {
	make "perltidy";
	make "perlcritic";
	make "commit", @_ if @_;
	make "perltidyt";
	make "perlcritict";
	my $x = `git status -s`;
	make "push", length($x)? "perltidy": ();
}

name "commit";
args "[[c|t] комментарий | 1]";
port "делает комит, если есть что комитить";
spec "
c - добавить \"код-ревью:\" перед комментарием
t - добавить комментарий ветки перед комментарием
1 - вместо комментария использовать название ветки
";
sub commit {

	$app->tty->raw;

	BEGIN:
	my $s = `git status -s`;
    if($s) {
        print "git status -s\n$s\n";
		my $n = defined($_[0])? 1: $app->tty->select(["комитим", "откатываемся", "git stash", "git diff"], "есть изменения");
        if($n == 1) {
            my $comment = $_[0] // $app->tty->input("введите комментарий к комиту (save) ");
			
			my $branch = current_branch();
			
			if(@_ == 2) {
				my ($s, $c) = @_;
				$comment = do { given($s) {
					"код-ревью: " when ["c", "с"];
					$app->tty->run("git config branch.$branch.description") when "t";
					default { quit "неизвестный сниппет `$s`" }
				}} . $c;
			}
			else {
				$comment = $app->tty->run("git config branch.$branch.description") if $comment eq 1; 
			}

            $comment =~ s/\s*$//;
            $comment = "save" if $comment =~ /^\s*$/;
			$comment =~ s!"!\\"!g;
			$comment =~ s/'/'\$"'"'/g;
        
            run "git add ${\git_add_path()}";
            run "git commit -am '$branch $comment'";
        }
		elsif($n == 2) {
			run "git reset --hard HEAD";
		}
		elsif($n == 3) {
			run "git stash";
		}
		elsif($n == 4) {
			run "git diff";
			goto BEGIN;
		}
    }

}

name "branch";
args "[remark]";
port "изменяет или показывает комментарий к текущей ветке";
task {
	my ($comment) = @_;

    $app->tty->raw;
	my $name = current_branch();
	
	if(@_==0) {
		run "git config branch.$name.description";
		return;
	}
	
	$comment =~ s/'/'\$"'"'/g;
	run "git config branch.$name.description '$comment'";
};

name "new";
args "[ветка] [комментарий]";
port "создаёт новую ветку";
spec "клонирует ветку master";
task {
	my ($name, $comment);

	$name = join " ", @_;
	$name =~ s/^\s*(.*?)\s*$/$1/;

	($name, $comment) = ($1, $2) if $name =~ /^(\S+)\s+(.+)$/s;

    $app->tty->raw;
    
	make "commit";
    
	length($name) == 0 && ($app->tty->input("введите название ветки", $name) || return);
	length($comment) == 0 && ($app->tty->input("введите комментарий к ветке", $comment) || return);
	
	$name =~ s/^\s*(.*?)\s*$/$1/;
	$comment =~ s/'/'\$"'"'/g;
	run "git config branch.$name.description '$comment'";
	
    my $branch = current_branch();
	my $dop = "";
    if($branch ne "master") {
        $dop = "git checkout master && ";
    }
    
    run "${dop}git pull origin master --no-edit && git checkout -b $name && git push origin $name";
};

name "fetch";
args "[ветка] [комментарий]";
port "скачивает с сервера существующую ветку первый раз";
sub fetch {
	my ($name, $comment);

	$name = join " ", @_;

	($name, $comment) = ($1, $2) if $name =~ /^(\S+)\s+(.+)$/s;

    $app->tty->raw;
    
	make "commit";
    
	length($name) == 0 && ($app->tty->input("введите название ветки", $name) || return);
	length($comment) == 0 && ($app->tty->input("введите комментарий к ветке", $comment) || return);
	
	run "git fetch && git checkout -b $name origin/$name && git pull --no-edit origin $name";
	
	$comment =~ s/'/'\$"'"'/g;
	run "git config branch.$name.description '$comment'";
}

name "mpull";
args "";
port "скачивает с сервера существующую ветку, если в ней были изменения от другого пользователя";
sub mpull {
    $app->tty->raw;
    
	make "commit";
    	
	run "git merge --no-ff --no-edit origin/" . current_branch();
}

name "mr";
args "[branch] [project]";
port "создаёт мерж-реквест";
sub mr {
	my ($branch, $project) = @_;
	$branch //= &current_branch;
	$project //= $app->file(".")->abs->name;
	my $title = `git config branch.$branch.description`;
	
	
	require "URI/Escape.pm";
	my $q = URI::Escape::uri_escape("merge_request[source_branch]")."=$branch&".
			URI::Escape::uri_escape("merge_request[title]")."=".URI::Escape::uri_escape("$branch $title")."&".
			URI::Escape::uri_escape("merge_request[description]")."=";
	my $mr = "https://gitee.220v.ru/220V/$project/merge_requests/new?$q";
	my $f = $app->file("/usr/bin/opera", "/usr/local/bin/opera", "$ENV{HOME}/bin/opera", "/cygdrive/c/Program Files/Opera/launcher.exe")->existing;
	run "'" . $f->path . "' '$mr'"
}


# возвращает массив бранчей
sub branches {
	$app->tty->raw;
	grep { length $_ } split /\n/, `git branch`;
}

# подсветка повторяющихся слов и цифр
sub color_branches {
	my ($real) = @_;
	
	use Term::ANSIColor qw/color/;
	
	my @branch = $real? @$real: branches();
	# если встречается слово дважды, то - выделить его
	my %word;
	@branch = map { while(m/[a-z]{3,}/gi) { $word{$&}++ } $_ } @branch;
	
	#delete $word{ED};
	
	# и подсвечиваем цыфры
	@branch = map { s/[a-z]{3,}|\d+/ !exists $word{$&}? color("cyan").$&.color("reset"): $word{$&}>1? color("red").$&.color("reset"): $& /gie; $_ } @branch;
	
	
	my $i = 0;
	for my $r (@$real) {
		my $x = $r;
		$x =~ s/\s*(\*\s*)?//;
		$x =~ s/\s*\n//;
		$x = `git config branch.$x.description`;
		$x =~ s/\s+$//;
		$branch[$i++] .= "\t$x";
	}
	
	@branch
}

name "del";
args "[ветка]";
port "удаляет ветку";
sub del {
	my ($branch) = @_;
	$app->tty->raw;

	$branch //= &current_branch;
	make("co", "master") if $branch eq &current_branch;
	
	$app->tty->run("git push origin :$branch");
	$app->tty->run("git branch -D $branch");
}

name "softdel";
args "[ветка]";
port "удаляет ветку локально";
sub softdel {
	my ($branch) = @_;
	$app->tty->raw;

	$branch //= &current_branch;
	make("co", "master") if $branch eq &current_branch;
	
	#$app->tty->run("git push origin :$branch");
	$app->tty->run("git branch -D $branch");
}


name "co";
args "[branch]";
port "переключиться на/создать/удалить ветку";
sub co {
    my ($branch) = @_;

    $app->tty->raw;
	
	my (@branch, $nbranch);
	if(!length $branch) {
		my @real = branches();
		@branch = color_branches(\@real);
		
		unshift @branch, $app->perl->qq("добавить"), $app->perl->qq("удалить");
		
		$nbranch = $app->tty->select(\@branch, "выберите ветку");
		
		if($nbranch == 1) {  # добавляем
			make("commit");
			make("new");
			return;
		}
		elsif($nbranch == 2) {	# удаляем
			make("commit");
			make("del");
			return;
		}
		
		$branch = $real[$nbranch-3];
	}

	if($branch =~ /^\d+$/) {
		/\w+-$branch\b/ and ($branch = $&) for &branches;
	}

	quit "вы остаётесь на ветке $branch" if $branch =~ /^\s*\* / or $branch eq &current_branch;

	make("commit");	
	
	my $rem = @branch? $branch[$nbranch]: (color_branches([$branch]))[0];
	
	print "$rem\n";
	$app->tty->run("git checkout $branch");
}

name "merge";
args "[branch]";
port "мёржит с указанной веткой или мастером";
sub merge {
    $app->tty->raw;
    
    my $branch = $_[0] || "master";
    
    run "git merge --no-ff --no-edit $branch";
}

name "rn";
args "new-name";
port "переименовывает текущий бранч локально и удалённо";
sub rn {
    $app->tty->raw;
    
	quit "укажите новое название ветки" if !length $_[0];
	
	make("commit");
	
	my $cur_branch = current_branch();
    my $new_branch = $_[0];
    
    run "git branch -m $new_branch";
	run "git push origin :$cur_branch";
	run "git push origin $new_branch";
}

name "mm";
args "";
port "сливает с мастером и пушит, но остаётся на ветке";
sub mm {
    $app->tty->raw;
   
    make("commit");
    
    my $branch = current_branch();
    
    # my $push = "git pull --no-edit origin $branch";
    # print "$push\n";
    # print `$push`;
    
    # my $push = "git push origin $branch";
    # print "$push\n";
    # print `$push`;
    
    $app->tty->run("git checkout master");
    
    $app->tty->run("git merge --no-ff --no-edit $branch");
    
    $app->tty->run("git pull --no-edit");
    
    $app->tty->run("git push");
    
    $app->tty->run("git checkout $branch");
}


name "dist-parent";
args "";
port "пушит все из этого каталога и всех каталогов в каталоге выше";
task {
	my $root = $app->file("..")->abs;
	
	$app->tty->raw;
	
	$root->sub("/*")->glob("-d")->then(sub {
		$_->chdir, msg1(":red", $_->path), print(`git add .; git commit -am dist; git pull --no-edit && git push`), $root->chdir if $_->sub("/.git")->isdir;
	});
};



name "dist1";
args "path";
port "";
sub dist1 {
	my ($dir) = @_;

	$app->tty->raw;
	
	quit "не указан путь к каталогу с проектом" if @_!=1;
	
	my $home = $app->file(".")->abs;
	my $sv = guard { $home->cd };
	
	chdir $_[0] or quit "не могу перейти в каталог проекта";
	quit "нет изменений" if !length run "git -c color.ui=always status -s";
	-e "man" and run "miu -r dot";

	my $diff = `git diff`;
	my ($commit) = $diff =~ /^[\t ]*#[\t ]+([^\n]{5,})/m;
	make "push", $commit || "save";
}

name "dist";
args "";
port "выполняет dist1 для всех проектов указанных в конфиге";
sub dist {
	my $p = $app->ini->{service}{dev}{projects};
	quit "нет записи в конфиге service.dev.projects" if !$p;
	my $c=0;
	for my $k (keys %$p) {
		my $r = $p->{$k};
		$app->log->info(":space", ":red", $k, ":black bold", $r->{dir});
		if(length $r->{dir}) {
			make "dist1", $r->{dir};
		}
	}
}


name "pullall";
args "";
port "обновляет все каталоги на каталог выше";
sub pullall {
	my $root = $app->file("..")->abs;
	
	$app->tty->raw;
	
	$root->sub("/*")->glob("-d")->then(sub {
		$_->chdir, msg1(":red", $_->path), print(`git add .; git commit -am pullall; git pull --no-edit`), $root->chdir if $_->sub("/.git")->isdir;
	});
}

name "perltidy";
args "[file]";
port "для всех изменённых файлов делает perltidy";
sub perltidy {
	my ($path, $force) = @_;
	my @pl = $path // grep { /\.(pm|pl|t)$/ } split /\n/, run "git status -s";
	for(@pl)  {
		next if /^D /;
		s/^\s*[\w\?]+\s+//;
		my $x;
		my $y = "perltidy -f \"$_\" -st";
		print "$y\n";
		$x=`$y` and $app->file($_)->encode(undef)->write($x);
	}
	
}

name "perlcritic";
args "[file]";
port "для всех изменённых файлов делает perlcritic";
sub perlcritic {
	my $path = shift;
	my @pl = $path // map { s/^\s+\w+\s+//; $_ } grep { !/^D / && /\.(pm|t)$/ } split /\n/, run "git status -s";
	run "perlcritic $_" for @pl;
}


name "perltidyt";
args "";
port "делает perltidy для некорректных файлов";
sub perltidyt {
	my $count = 0;
	$app->tty->raw;
	my $x = $app->tty->run("TEST_PERLTIDY_ENABLE=1 TEST_PERLTIDY_FILES=1 TEST_PERLTIDY_VERBOSE=1 prove xt/10-perltidy.t 2>&1");
	while($x =~ /#[ \t]*Failed test[ \t]*'(.*)'/g) {
		make "perltidy", $1;
		$count++;
	}
	$_ = $count;
}

name "perlcritict";
args "";
port "вызывает тест в xt";
sub perlcritict {
		run "TEST_PERLCRITIC_ENABLE=1 perl " . $app->file("xt")->find("**perlcritic*.t")->path;
}



1;
