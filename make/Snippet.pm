package R::Make::Snippet;

use common::sense;
use R::App;
use R::Make;

category "сниппеты";


# возвращает файл сниппета по имени
sub snipfile ($) {
	my ($snip) = @_;
	my $file = $app->project->files("snippet/$snip.*")->glob->existing->add(
		$app->project->files("snippet/$snip")->glob->existing
	);

	quit "нет сниппета $snip" if !$file->length;
	quit "одинаковые сниппеты: " . $file->join if $file->length>1;

	$file
}

# рендерит указанный файл
sub sniprender ($$;$) {
	my ($from, $to, $data) = @_;
	
	# путь от рута
	my $prj = $to->root($app->project->path);
	# путь от рута минус одну директорию
	my @src = split "/", $prj->path;
	shift @src;
	my $src = $app->file(join "/", @src);
	
	# пакет
	my $package = $src->exts("")->path;
	$package =~ s!/!::!g;
	$package =~ s!-(\w)!uc $1!ge;
	
	if($to->exists) {
		return if !$app->tty->confirm("файл существует, перезаписать?")
	}
	
	my $name = $data->{name};
	my $Name = ucfirst($name);
	$Name =~ s!-(\w)!uc $1!ge;
	
	$data = {
		%$data,
		name => lcfirst($name),
		Name => $Name,
		PATH => $prj->path,
		path => $to->path,
		src => $src->path,
		package => $package,
	};
	
	$_ = $from->read;
	
	s!\{\{\s*(\w+)(?:\s*-(.*?))?\s*\}\}!$data->{$1} //= $app->tty->input($2 || $1)!ge;
	
	$to->write($_);
	
	return;
}

name "mk";
args "сниппет название";
port "создаёт файл или директорию из сниппета";
spec "Сниппеты находятся в каталоге snippet проекта.

snippet/категория/каталог или snippet/категория/файл.

Файлы представляют из себя шаблон со стандартными переменными.
Если в двух разных директориях есть два файл с одинаковыми именами, то будет ошибка.

Переменные для вставки:

* name - имя сниппета с маленькой буквы
* Name - имя сниппета с большой буквы
* NAME - имя сниппета с большими буквами
* Class - имя CamelCase из camel-case
* class - имя snake_case из camel-case
* package - пакет perl из пути src (см. далее)
* PATH - путь от корня проекта
* path - путь от текущего каталога
* src - путь от корня проекта минус одну директорию

Так же доступны все переменные из ini-файла (app->ini->{project}). Обычно это:

* company
* license
* author

";
sub mk {
	my @args = grep { $_ ne "-c" } @_;
	my $is_flat = @args!=@_;
	my ($snip, $path) = @args==2? @args: $args[0] =~ m!\.([^\./]+)$!? ($1, $`): quit "use: `al mk snip name` or `al mk name.snip`";
	
	my $name = $app->file($path)->name;
	
	my $file = snipfile $snip;
	my $data = {
		%{ $app->ini->{project} },
		name => $name,
		NAME => uc($name),
		Name => ucfirst($name),
		Class => do { my $x=$name; $x =~ s!-(\w)!uc $1!ge; ucfirst $x },
		class => do { my $x=$name; $x =~ s!-(\w)!"_" . lc $1!ge; lcfirst $x },
	};
	
	if($file->isfile) {
		my $to = $app->file(join ".", $path, $file->exts);
		sniprender $file, $to, $data;
	}
	else {
		for my $f ($file->find->slices) {
			if($f->isfile || $f->issymlink) {
				my $to = $f->root($file)->path;
				$to =~ s!\{\{\s*(\w+)\s*\}\}!$data->{$1} // "?$1?"!ge;
				$to = $app->file($to);
				$to = $to->frontdir($name) if !$is_flat;
				$to->mkpath;
				$to->rm if $to->exists;
				sniprender $f, $to, $data;
			}
		}
	}
	
	$app->log->info(":red", "Ок")
}

name "ln";
args "сниппет имя";
port "создаёт ссылку на сниппет";
sub ln {
	my ($snip, $name) = @_;
	
	my $f = snipfile $snip;
	quit "файл `$name` существует" if -e $name;
	my $path = join "/", ("..") x scalar split /\//, $app->file(".")->abs->root($app->project->path)->path;
	$f = $f->root($app->project->path);
	$app->file("$path/" . $f->path)->symlink($name);
}

1;