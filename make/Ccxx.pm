package R::Make::ccxx;
# работа с ccxx

use common::sense;
use R::App;
use R::Make;


category "ccxx";

name "ty";
args "[-o|-O] [тест1.t, ...]";
port "тестирование питоновских микросервисов";
spec "
-O        открыть покрытие в браузере по умолчанию
-o            открыть покрытие в Опере
";
sub ty {
    #run "find . ! -path venv -name '*.py' -exec autopep8 --in-place --aggressive --aggressive {} \\;";
    if(-d 'bin') {
        run "flake8 bin/*";
        run "env PYTHONPATH=\"src:\$PYTHONPATH\" pylint --rcfile=.pylintrc bin/*";
    }

    run "flake8 .";
    #run "env PYTHONPATH=\"src:\$PYTHONPATH\" pylint --rcfile=.pylintrc `find . ! -path venv -name '*.py'`";
	run "pylint --rcfile=.pylintrc src";

    run "rm -fr htmlcov";
    
    #run "coverage run --branch --source=ccxx -m pytest `find tests -name '*.py'`";
    run "env PYTHONPATH=\"src:\$PYTHONPATH\" coverage run --branch --source=src -m pytest tests/";
    run "coverage report -m";
    run "coverage html";
    
    my $open = $_[0] eq "-O"? do { shift; "xdg-open"}: 
         $_[0] eq "-o"? do { shift; "opera"}:
        "";
    
    run "$open htmlcov/index.html" if $open;
    
}

name "ty_up";
args "";
port "апдейт питоновских микросервисов";
sub ty_up {
   run "pip3 install -r requirements.txt";
   run "pip3 install --upgrade --upgrade-strategy eager -e .";
}

name "tc";
args "[-o|-O] [тест1.t, ...]";
port "тестирование с cover";
spec "
-O        открыть покрытие в браузере по умолчанию
-o            открыть покрытие в Опере
";
sub tc {
    run "cover -delete";
    
    my $open = $_[0] eq "-O"? do { shift; "xdg-open"}: 
         $_[0] eq "-o"? do { shift; "opera"}:
        "";
    
    # for my $test ( @_? @_: $app->file("t")->find("**.t") ) {
    #     run "perl -MDevel::Cover -Ilib $test";
    # }
    run 'PERL5OPT="$PERL5OPT -MDevel::Cover" prove -Ilib ' . (@_? join(" ", @_): "-r t");
    
    run "cover -report html_basic";
    
    run "$open cover_db/coverage.html" if $open;
    
}

name "xt";
args "";
port "запускает тесты perltidy и perlcritic";
spec "";
sub xt {
    #TEST_PERLTIDY_VERBOSE=1 TEST_PERLCRITIC_FILES=1 TEST_PERLTIDY_FILES=1
    
    run "TEST_PERLTIDY_ENABLE=1 TEST_PERLTIDY_VERBOSE=1 prove xt";
    $app->log->info(":space", ":white", "[", ":green", "TIDY -", ":red", "Ok", ":white", "]");
    
    run "TEST_PERLCRITIC_ENABLE=1 prove xt";
    $app->log->info(":space", ":white", "[", ":green", "CRITIC -", ":red", "Ok", ":white", "]");
}

name "t";
args "";
port "запускает тесты t и xt";
spec "";
sub t {
    make "perltidyt";
    $app->log->info(":space", ":white", "[", ":green", "TIDY FIX -", ":red", "Ok", ":white", "]");

    make "tc", @_;
    make "xt";
    
    eval { run "TEST_PERLCOVER_ENABLE=1 prove xt"; };
    run("xdg-open cover_db/coverage.html"), die if $@;
    $app->log->info(":space", ":white", "[", ":green", "COVER -", ":red", "Ok", ":white", "]");
    $app->log->info(":space", ":white", "[", ":green", "ВСЕ ТЕСТЫ -", ":red", "Ok", ":white", "]");
}

name "ejectmd";
args "";
port "извлекает md из всех py-файлов в папке в md-файлы";
sub ejectmd {
    app->file($_[0] // ".")->find("**.py")->replace(sub {
        s!^"""(.*?)^"""|^'''(.*?)^'''!$_[0]->ext("md")->write($1 || $2); ""!mse;
    });
}

name "injectmd";
args "";
port "вставляет md-файлы в py-файлы в папке";
sub injectmd {
    app->file($_[0] // ".")->find("**.md")->then(sub {
        my $f = $_;
        $_->ext("py")->existing->replace(sub {
            s!^!"""${\$f->read}"""!ms;
        });
    });
}

name "sql2alchemy";
args "file [filename] [model] [в проект]";
port "переводит текст из sql в sqlalchemy";
sub sql2alchemy {
    my ($f, $file, $model, $in_project, $version) = @_;
	
	msg1 $f;
	
	$version //= 0.49.0;
	
    my $sql = app->file($f)->read;
    
    my %import_sql;
    my %import_dialect;
	my %import_tables;

    my ($tab) = $sql =~ /create\s+table\s+`?(\w+)`?/i;
    
    $file = app->magnitudeLiteral->to_snake_case($tab), $file =~ s!_+!_!g if !defined $file;
    $model = app->magnitudeLiteral->toCamelCase($file) if !defined $model;

    my $COMMENT = qr/"(?:""|[^"])*" | '(?:''|[^'])*'/x;

    sub comment {
        local $_ = $_[0];
        my ($f) = /^(.)/;
        s/^$f(.*)$f$/$1/;
        s/$f$f/$f/g;
        s!"!\\"!g;
        "\"$_\""
    }

    my $S = qr/[\ \t]/x;

    my $tab_comment = comment($sql =~ /COMMENT=($COMMENT)\s*$/i);

    my %key;
    my @key;
    while($sql =~ m!
        ^ $S* ( (?<unique>UNIQUE $S+)|(?<primary>PRIMARY $S+) )? KEY(\s+(`(?<name>\w+)`)|(?<name>\w+))?\s*\(\s*(?<keys>[^()]*)\s*\)?!mgxin) {
        my ($primary, $unique, $name) = @+{qw/primary unique name/};
        my $add = $name ne ""? ", name=\"$name\"": "";
        my @k = map {s/`//g; $_} split /\s*,\s*/, $+{'keys'};
        if(@k>1) {
            $import_sql{"UniqueConstraint"}=1, push @key, "UniqueConstraint(".join(", ", map { "\"$_\"" } @k)."$add)" if $unique;
            $import_sql{"PrimaryKeyConstraint"}=1, push @key, "PrimaryKeyConstraint(".join(", ", map { "\"$_\"" } @k)."$add)" if $primary;
            $import_sql{"Index"}=1, push @key, "Index(".join(", ", map { "\"$_\"" } $name, @k).")" if !$primary && !$unique;
        }
        else {
            $key{$k[0]}->{unique}++ if $unique;
            $key{$k[0]}->{primary}++ if $primary;
            $key{$k[0]}->{'index'}++ if !$primary && !$unique;
        }
    }
    
    #CONSTRAINT `order_cancellation_ibfk_1` FOREIGN KEY (`order_id`) REFERENCES `Ru_Order_General` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
    my %fk;
    while($sql =~ m!^ $S* CONSTRAINT \s+ (`(?<name>\w+)` | (?<name>\w+)) \s+
        FOREIGN \s+ KEY \s+ \( \s* `(?<col>\w+)` \s* \) \s*
        REFERENCES \s* `(?<ref_tab> \w+)` \s* \( \s* `(?<ref_col> \w+)` \s* \) \s* 
        (ON \s+ DELETE \s+ (?<delete>\S+) \s* )?
        (ON \s+ UPDATE \s+ (?<update>\S+) \s* )?
        [,\)]
    !mgxin) {
		$import_sql{"ForeignKey"} = 1;
		my $ref_tab = app->magnitudeLiteral->toCamelCase($+{ref_tab});
		$import_tables{"from ccxx.base.".lc($+{ref_tab})." import $ref_tab" } = 1;
        push @{ $fk{$+{col}} }, join "", "ForeignKey($ref_tab.".$+{ref_col},
            $+{'delete'}? ", ondelete='$+{delete}'": (),
            $+{update}? ", onupdate='$+{update}'": (),
        ")";
    }
    
    #msg1 \%fk, \@key, \%key;
    
    my @sql_cols;
    my $cols = [];
    while($sql =~ m!
        ^ $S* (`(?<col>\w+)` | (?<col>\w+)) 
            $S+ (?<type> \w+) ($S* \($S* (?<precision>\d+($S*, $S*\d+)? ) $S* \) )?
            (?<unsigned> $S* unsigned )?
            ($S* CHARACTER $S+ SET $S+ (?<charset>\w+) )?
            ($S* COLLATE $S+ (?<collate> \w+) )?
            (?<not_null> $S* not)? ($S*null)? 
            (?<autoincrement> $S* AUTO_INCREMENT)?
            ($S* DEFAULT $S* (?<default> \S+ ))?
            (?<on_update> \s+ ON \s+ UPDATE \s+ CURRENT_TIMESTAMP )?
            ($S* COMMENT $S* (?<comment> $COMMENT ))?
    !imsgxn) {
    
        my $col = uc $+{col};
        next if grep { $col eq $_ } qw/CREATE PRIMARY UNIQUE/;
    
        push @sql_cols, $&;

		my %x = %+;

        my $precision = $x{precision};
		$precision =~ s!\s*,\s*!, !g;
		
        my $type = uc $x{type};
        $type = $type eq "INT"? "INTEGER": $type;
        $type =
            $type eq "TINYINT" && $precision eq 1 && !$x{unsigned}? do { $import_sql{"Boolean"}=1; undef $precision; "Boolean" }:
            $type eq "VARCHAR"? do { $import_sql{"String"}=1; "String" }:
            $type eq "DATETIME"? do { $import_sql{"DateTime"}=1; $precision = "timezone=True"; "DateTime" }:
            do { $import_dialect{$type}=1; $type };
        #msg1 $type, {%+};
		
        my $column = join "", "    $x{col} = Column(",
            join(", ",
                join("", $type, "(", $precision, 
                    $x{unsigned}? ", unsigned=True": "", 
                    $x{collate}? ", collation='$x{collate}'": (),
                ")"),
                exists $fk{$x{col}}? join("", @{$fk{$x{col}}}): (),
                $x{not_null} && !exists $key{$x{col}}->{primary}? "nullable=False": (),
                exists $key{$x{col}}->{primary}? "primary_key=True": (),
                $x{autoincrement}? "autoincrement=True": (),
                exists $key{$x{col}}->{unique}? "unique=True": (),
                exists $key{$x{col}}->{'index'}? "index=True": (),
                $x{default} eq "CURRENT_TIMESTAMP"? do {
                    $import_sql{"func"}=1;
                    "server_default=func.now()"
                }:
                $x{default} && uc($x{default}) ne "NULL"? do {
					my $def = $x{default};
                    if($def =~ /^'.*'$/) {
                        "server_default=$def"
                    } else {
                        $import_sql{"text"}=1;
                        "server_default=text(" . app->magnitudeLiteral->to($def) . ")"
                    }
                }: (),
                $x{on_update}? do { $import_sql{func}=1; "onupdate=func.now()" }: (),
                "comment=".comment($x{comment}),
            ),
        ")";
		
		# тут нужно подогнать под 120 символов на строку
		sub words120 {
			my ($rem, $space, $splitter) = @_;
			my @column;
			while(120 < length $rem) {
				my @rem = split /(\s+)/, $rem;
				my $c; my $i;
				for($c = 0, $i = -1; $i < @rem && $c<120; $c += length $rem[++$i]) {}
				my $xf = join "", @rem[0..$i-1], $splitter;
				$xf =~ s!\s*$!!;
				push @column, $xf;
				$rem = "\n$space$splitter" . join("", @rem[$i..$#rem]);
			}
			join "", @column, $rem
		}
		
		
		sub column120 {
			my ($column, $from) = @_;
			if(120 < length $column) {
				$from //= "Column";
				
				my $rem;
				$column =~ s/ \w+=".*?$/$rem = $&; ""/e;
				$column =~ /\b$from\(/; my $l = length($`) + length($&) - 1;
				
				if(120 < length $column) {
					my $z;
					$column =~ s![^,]+,$!$z = $&; ""!e;
					$column .= "\n" . (" " x $l) . $z;
				}
				
				$rem = (" " x $l) . $rem;
				$rem = words120($rem, " " x ($l+1), '"');
				$column .= "\n$rem";
			}
			return $column;
		}
		
		$column = column120($column);
		
		push @$cols, $column;
    }
	
    $cols = join "\n", @$cols;
    
    my ($engine) = $sql =~ /ENGINE=(\w+)/i;
    my ($charset) = $sql =~ /DEFAULT CHARSET=(\w+)/i;
    my $collate = "${charset}_general_ci";
    
    my $import_sql = join "", map { ", $_" } sort keys %import_sql;
    my $import_dialect = %import_dialect? "\nfrom sqlalchemy.dialects.mysql import " . join(", ", sort keys %import_dialect): "";
	my $import_tables = %import_tables? join("", map {"\n$_"} sort keys %import_tables): "";
    
    my $i=0;
    my $start = @key? "(" . join("", map { "$_,\n                      " } @key) . "{": "{";
    my $end = @key? "})": "}";
    my $add_space = @key? " ": "";
    
    #msg1 \@key, $start, $end;
	
	my $tableargs = "    __table_args__ = ${start}'mysql_engine': '$engine',
                      ${add_space}'mysql_charset': '$charset',
                      ${add_space}'mysql_collate': '$collate',
                      ${add_space}'comment': $tab_comment$end
";
    
	my @tableargs = split "\n", $tableargs;
	
	my $sp = "                      ";
	
	for(my $i=0; $i<@tableargs; $i++) {
		if($i<@key) {
			my ($from) = $key[$i] =~ /^(\w+)/;
			$tableargs[$i] = column120($tableargs[$i], $from);
		} else {
			$tableargs[$i] = words120($tableargs[$i], $sp . $add_space, '"');
		}
	}
	
	$tableargs = join "\n", @tableargs;
	
    my $tab_comment3 = app->magnitudeLiteral->from($tab_comment);

    my $py=<<"END";
"""
NAME
====
$file — модель $model таблицы $tab

VERSION
=======
$version

SYNOPSIS
========

    from ccxx.db import DB
    from ccxx.base.$file import $model

    db = DB('host', 'user', 'password', 'database')
    session = db.get_session()
    data = session.query($model).all()

    session.close()


DESCRIPTION
===========
Модель предназначена для работы с таблицей '$tab' базы данных "220_volt".
$tab_comment3

MODELS
======
"""
from sqlalchemy import Column$import_sql$import_dialect$import_tables

from .base_model import BaseModel


class $model(BaseModel):
    """
    Модель таблицы $tab
    """
    __tablename__ = '$tab'
$tableargs

$cols
END

	$py = join "\n", map { words120($_) } split /\n/, $py;
	$py =~ s!$!\n!;

	app->file($f)->ext(".py")->write($py);

    my $zero = <<"END2";
from ccxx.base.$file import $model

class AdvancedTestSuite(unittest.TestCase):
    """Тестирование моделей"""

    def test_$file(self):
        """Тестирование модели $model"""

        self.assertEqual($model.__tablename__, '$tab')
END2

    #$zero =~ s!.{40}!!e;

    app->file($f)->file(app->file($f)->name . "_test.py")->write($zero);
    @sql_cols = map { s/^\s+//g; "ALTER TABLE $tab MODIFY COLUMN $_;\n" } @sql_cols;


    my $tab_comment1 = $tab_comment;
    $tab_comment1 =~ s/"/'/g;
    my $sql_file = "SET NAMES utf8;\n\nALTER TABLE $tab COMMENT=$tab_comment1;\n\n" . join "", @sql_cols;

    app->file($f)->file(app->file($f)->name . "_alter.sql")->write($sql_file);


    return if !$in_project;

    app->file(my $fn = "$ENV{HOME}/220v/base-python/src/ccxx/base/$file.py")->write($py);
	# --aggressive --aggressive
    #run "autopep8 $fn --in-place";

    app->file("$ENV{HOME}/220v/base-python/tests/test_models.py")->replace(sub {

        if(m!^from ccxx\.base\.$file!m) {}
        else {
            my $x = "from ccxx.base.$file import $model";
            while(m!^from ccxx\.base\.(\w+) import (\w+)!gm) {
                if($1 gt $file) {
                    my $r = quotemeta $&;
                    s!$r!$x\n$&!;
                    undef $x;
                    last;
                }
            }
            if(defined $x) { s!^(from ccxx\.base\.\w+ import \w+\n)\n!$1$x\n\n!m }
        }

        my $x = "test_$file";
        my $y = <<"END3";
    def $x(self):
        """Тестирование модели $model"""

        self.assertEqual($model.__tablename__, '$tab')

END3

        if(m!^ {4}def $x\(!m) {

        } else {

            while(m!^ {4}def (test_\w+)\(self\):\s+"""Тестирование модели(.*\n){3}!gm) {
                if($1 gt $x) {
                    my $r=quotemeta $&;
                    s!$r!$y$&!;
                    $x = undef;
                    last;
                }
            }
            if(defined $x) {
                s!\s+(if __name__)!\n\n$y\n$1!;
            }
        }
    });

    my $branch = lc `cd ~/220v/base-python; git branch --show-current`;
    chomp $branch;
    $sql_file =~ s!^SET NAMES utf8;!!;
    app->file("$ENV{HOME}/220v/base-python/migrations/$branch.sql")->append($sql_file);

}


name "s2a";
args "file1...";
port "переводит текст из sql в sqlalchemy";
sub s2a {
    my $branch = lc `cd ~/220v/base-python; git branch --show-current`;
    chomp $branch;
    app->file("$ENV{HOME}/220v/base-python/migrations/$branch.sql")->write("SET NAMES utf8;");
	
	my $x = app->file("$ENV{HOME}/220v/base-python/setup.py")->read;
	my ($version) = $x =~ /version='(.*)'/;
	
    sql2alchemy($_, undef, undef, 1, $version) for @_;
}

name "show_tables";
args "tab1...";
port "скачивает указанные таблицы из базы в файлы";
sub show_tables {
    $app->file("$_.sql")->write($app->connect->query_ref("show create table `$_`")->{'Create Table'}), app->log->info(":green", $_) for @_;
}


name "ct2alter";
args "";
port "переводит текст из sql в alter и дописывает комментарии в таблицы base-perl";
sub ct2alter {
    
    my $branch = lc `cd ~/220v/base-perl; git branch --show-current`;
    chomp $branch;

    my $f = app->file("$ENV{HOME}/220v/base-perl/sql_fix/$branch/comments.sql")->mkpath->open(">");
    print $f "SET NAMES utf8;\n\n";


    for my $sql_file (app->file(".")->find("**.sql")->slices) {
        local $_ = $sql_file->read;

        app->log->info(":green", $sql_file->name);

        my ($tab) = /create table `(\w+)`/i or quit "Нет таблицы";
        my ($rem) = /comment=('.*')/i or quit "Нет комментария у поля";

        my $x = $tab;
        $x =~ s/_//g;
        $x =~ s/s$//;
        my $m = app->file(my $module = "$ENV{HOME}/220v/base-perl/lib/CCXX/Faraday/DB/DBIC/Schema/Result/$x.pm")->read;
        
        print $f "ALTER TABLE $tab COMMENT=$rem;\n\n";

        while(/(`(\w+)`.*\bCOMMENT '(.*)'),/ig) {
            print $f "ALTER TABLE $tab MODIFY COLUMN $1;\n";

            my $col = $2;
            my $com = $3;
            #app->log->info(":red", $col, ":blue", $com);

            $m =~ s{^=head2 $col\n(.*?)^(=head2|=cut)}{
                my ($y, $z)=($1, $2);

                $y =~ s!^\S.*\n!!g;
                $y =~ s!$!\n$com\n!;

                #app->log->info(":yellow", $y, ":magenta", $z);

                "=head2 $col\n$y$z"
            }sme or quit "Нет столбца $col в модели $module";
        }

        print $f "\n";
        
        app->file($module)->write($m);
     }

     close $f;
}


name "const";
args "file";
port "ищет в файле повторяющиеся строки и цифры и выносит их в виде констант в заголовок";
sub const {
    my ($file) = @_;
    app->file($file)->replace(sub {
        my %x;
        my $qr = qr/(?<str>"[^"]*"|'[^']*')|\d+(\.\d+)?/;
        while(m!!g) { exists $+{str}? $x{$app->magnitudeLiteral->normalize($&)}++: $x{$&}++ }

        my %c;
        my $i = 0;

        my $fn = sub { 
            my $x = exists $+{str}? $x{$app->magnitudeLiteral->normalize($&)}: $x{$&};
            $x>1? ($c{$&} //= "# Константа определяет .\nCONSTANT_" . ($i++) . " = " . $&): $& 
        };
        s!$qr!$fn->()!ge;

        my $x = join "\n", sort keys %c;
        s!^!$x\n!;
    });
}


name "openapi_validator";
args "[openapi.yml] [path/to/schema]";
port "Конвертер openapi в валидатор на питоне";
spec "

    al openapi_validator x.yml 

";
sub openapi_validator {
    my ($path, $schema) = @_;
	
	$path //= "$ENV{HOME}/220v/selfapi/site/general/static/openapi/v3/selfapi.yml";
	$schema //= "components/schemas/CartProduct";
	
	my ($name) = $schema =~ /(\w+)$/;
	
	my $code = app->file($path)->read;
	
	sub schema_order {
		my ($name, $code) = @_;
		my ($space, $schema_only) = $code =~ /^( *)$name:(.*?)(?:^\1\w+:|\z)/ms or quit "Нет схемы $name!";
		my %order; my $n;
		$order{$1} = ++$n while $schema_only =~ /^$space    (\w+):/gm;
		return %order;
	}
	
	my %order = schema_order($name, $code);
	
    my $x = app->yaml->from($code);
    #$x = $x->[0] if ref $x eq "ARRAY";
	
	my @schema = split /\//, $schema;
	$x = $x->{$_} for @schema;

    my %required = map {$_=>1} @{$x->{required}};
    my $x = $x->{properties};
	
    my @x;
    my %fld;
    my %FLD = qw(integer IntegerField string CharUtfField number FloatField boolean BooleanField array ListField);
    my %ATTR = qw(minimum min_value maximum max_value minLength min_length maxLength max_length default default);

    for my $k (sort { $order{$a} <=> $order{$b} } keys %$x) {
        my $v = $x->{$k};
        my $fld = $FLD{$v->{type}};
        $fld{$fld}++;
		
		$v->{default} //= undef;

        my $req = $required{$k}? "True": "False";
		my $map = $v->{type} eq "array"? qq{
        child=$FLD{$v->{items}{type}}(required=$req),
        required=$req,
        allow_empty=False,
    }:
			join ", ", "required=$req", map { 
			exists $v->{$_}? "$ATTR{$_}=".(
				$v->{type} eq "boolean"? ($v->{$_}? "True": "False"):
				$v->{$_} =~ /^-?\d+(\.\d+)?$/? $v->{$_}:
				!defined $v->{$_}? "None":
				$app->magnitudeLiteral->to($v->{$_})
        ): () } sort keys %ATTR;

        push @x, "$k = $fld($map)";
    }

my $fld = join ", ", sort keys %fld;
my $attr = join "\n    ", @x;

print qq(from rest_framework.fields import $fld

from volt.helpers.validator import Validator


class ${name}Validator(Validator):
    """ Проверка полученных данных из тела запроса на соответствие схеме """
    $attr
);

}



name "openapi_view";
args "[schemas] [openapi.yml]";
port "Генерирует добавление в таблицы для контроллера на SQLAlchemy";
spec "

	Преобразует выбранную схему в запросе к базе.
	Поля берутся из description.
	
	

    al openapi_view components/schemas/Cart v3/openapi.yml > file.py

";
sub openapi_view {
	my ($schemas, $path) = @_;


	$path //= "$ENV{HOME}/220v/selfapi/site/general/static/openapi/v3/selfapi.yml";
	my $code = app->file($path)->read;
	
	
	my %i; my %orders;
	
	for my $name (split /,/, $schemas) {
	
		my $schema = "components/schemas/$name";

		my %order = schema_order($name, $code);
		%orders = (%orders, %order);
		
		my $x = app->yaml->from($code);
		#$x = $x->[0] if ref $x eq "ARRAY";
		
		my @schema = split /\//, $schema;
		$x = $x->{$_} for @schema;
		$x = $x->{properties};
		
		for my $k (sort { $order{$a} <=> $order{$b} } keys %$x) {
			
			my $v = $x->{$k};
			my $desc = $v->{description};
			
			while($desc =~ /[a-z_]+(?:\.\w+)+/gi) {
				my @r = split /\./, $&;
				my $last = pop @r;
				my $x = \%i;
				$x = ($x->{$_} //= {}) for @r;
				$x->{$last} = "$name']['$k";
			}
		}
	}
		
	my @r; my @i;
	for my $tab (sort keys %i) {
		my $model = app->magnitudeLiteral->toCamelCase($tab);
		$model =~ s/_//g;
		my $file = app->magnitudeLiteral->to_snake_case($model);
		push @i, "from ccxx.base.$file import $model\n";
		push @r, "$file = $model(\n";
		
		for my $col (sort { defined($orders{$a}) && defined($orders{$b})? ($orders{$a} <=> $orders{$b}): ($a cmp $b) } keys %{$i{$tab}}) {
			my $v = $i{$tab}{$col};
			push @r, "    $col=";
			if(ref $v) {
				sub _data_to_json {
					my ($v, $ident) = @_;
					if(ref $v) {
						"{\n" . join("", map { "$ident    '$_': " . _data_to_json($v->{$_}, "$ident        ") . ",\n" } sort keys %$v) . "$ident}";
					}
					else {"validator.data['$v']"}
				}
				
				push @r, "json.dumps(", _data_to_json($v, "    "), ", ensure_ascii=False)";
			} else {
				push @r, "validator.data['$v']";
			}
			push @r, ",\n";
		}
		push @r, ")\nsession.add($file)\n\n"
	}
	
	print @i, "\n\n", @r;
}



name "openapi_serializer";
args "path file [with_types] [as_object]";
port "Конвертер openapi в сериалайзер на питоне";
spec "

    al openapi_serializer components/schemas/Cart v3/openapi.yml
    al openapi_serializer components/schemas/Cart v3/openapi.yml 1
    al openapi_serializer components/schemas/Cart v3/openapi.yml 1 1
    al openapi_serializer components/schemas/Cart v3/openapi.yml 0 1

";
sub openapi_serializer {
    my ($path, $file, $with_types, $as_object) = @_;
	
	my $code = app->file($file)->read;
    my $schema = app->yaml->from($code);

    my @path = split /\//, $path;
    $schema = $schema->{$_} // die "нет $_" for @path;
    my $name = app->magnitudeLiteral->to_snake_case($path[$#path]);

    my $prop = $schema->{properties};
    my $Name = $path[$#path];

	my %order = schema_order($Name, $code);

    print "def ${name}_serializer($name, logger):
    \"\"\" Сериализация схемы `$Name` $schema->{title}

    **Args:**

    * $name ($Name): $schema->{title}.
    * logger (Logger): Логгер.

    **Returns:**

    * (dict): схема $Name.
    \"\"\"
    return {\n";
    print join "", map {
        my $v = $prop->{$_};
        my $key = $as_object? "$name.$_": "${name}['$_']";
        my $comment = join "", map { s/\.?$/./; "        # $_\n" } grep { length } map { s/\s*$//; $_ } split /\n/, $v->{title};
        "$comment        '$_': ".($with_types? "perl_$v->{type}".($v->{nullable}? "_nullable": "")."($key)": $key).",\n";
    } sort { $order{$a} <=> $order{$b} } keys %$prop;
    print "    }\n\n";
}


name "pydef";
args "name args...";
port "Генериркует скелет функции на python для ccxx";
spec "

    al pydef xyz a b c

    al pydef xyz:str a:int b:list 'c:dict(str)' 

";
sub pydef {

    my @type;
    my @arg = map { /:|$/; push @type, $'; $` } @_;
    
    my $name = shift @arg;
    my $ret = shift @type;
    
    my $arg = join ", ", @arg;
    my $i = 0;
    my $args = join "", map { "    * $_ (".$type[$i++]."): .\n" } @arg;

    print << "END";
def $name($arg):
    """ 

    **Args:**

$args
    **Returns:**

    * ($ret): .
    """
END
}


1;
