package R::Make::wget;
# скачивает что-либо из всемирной паутины

use common::sense;
use R::App;
use R::Make;


category "wget";


name "rosettacode";
args "язык";
port "скачивает все примеры по указанному языку";
spec "язык должен быть с большой буквы, как в https://rosettacode.org/wiki/Category:Octave";
sub rosettacode {
	my ($lang) = @_;
	my $Url = "http://rosettacode.org";
	
	# в строку
	my $to_s = sub {
		my ($page) = @_;
		$page =~ s!(\b(src|href|rel)=['"]?)/!$1$Url/!g;
		$page =~ s!<script\b.*?</script>!!gs;
		$page
	};
	
	my $p = $to_s->($app->url("$Url/wiki/Category:$lang")->read);
	my $q = $app->htmlquery($p);
	my @R;
	for my $anh ($q->find("#mw-pages")->find("a")->slices) {
		my $href = $anh->attr("href");
		$app->log->info($href);
		
		my $u = $app->url($href);
		my $page = $u->read;
		
		$page = $to_s->($page);
		
		my ($task) = $page =~ /<div class="contentHeader">(.*?)<div id="toc" class="toc">/s;
		my ($s) = $page =~ /<div id="toc" class="toc">(.*?)<div id='catlinks'/s;
		
		my $link = $app->file($u->path)->name;
		
		my @x = split /^<h2><span class="mw-headline" id="([^"]*)">/m, $s;
		shift @x;
		push @R, $task, map2 { "<h2><span class=\"mw-headline\" id=\"$link\">$b" } grep2 { $a =~ /\Q$lang\E/ } @x;
		
		$anh->attr("href", "#" . $link);
		
	}
	
	my $x = join "\n", @R;
	
	#$app->file("$lang-1.html")->write($x);
	
	my $p = $q->as_html;
	
	$p =~ s!<div \w+="catlinks" \w+="catlinks">!$x$&!;
	
	$app->file("$lang.html")->write($p);
}



1;