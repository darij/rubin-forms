package R::Make::Localize;
# следилки за изменением файлов

use common::sense;
use R::App;
use R::Make;

category "ЛОКАЛИЗАЦИЯ";


my $pot = $app->file("var/messages.pot");
my $files = $app->file("var/xgettext.files");

# обновляет список файлов
sub files {
	my $f = $files->mkpath->open(">");
	local $\ = "\n";
	$app->file(qw/lib form model/)->find(sub {
		next if !/\.pm\z/;
		print $f $_;
		0;
	});
	close $f;
	$files->path
}

# возвращает опции для xgettext
sub options {
	my $options = "--keyword=__ --keyword=%__ --keyword=\$__ --keyword=__x --keyword=__n:1,2 --keyword=__nx:1,2 --keyword=__xn:1,2 --keyword=__p:1c,2 --keyword=__px:1c,2 --keyword=__np:1c,2,3 --keyword=__npx:1c,2,3 --keyword=N__ --keyword=N__n:1,2 --keyword=N__p:1c,2 --keyword=N__np:1c,2,3 --flag=__:1:pass-perl-format --flag=%__:1:pass-perl-format --flag=\$__:1:pass-perl-format --flag=__x:1:perl-brace-format --flag=__x:1:pass-perl-format --flag=__n:1:pass-perl-format --flag=__n:2:pass-perl-format --flag=__nx:1:perl-brace-format --flag=__nx:1:pass-perl-format --flag=__nx:2:perl-brace-format --flag=__nx:2:pass-perl-format --flag=__xn:1:perl-brace-format --flag=__xn:1:pass-perl-format --flag=__xn:2:perl-brace-format --flag=__xn:2:pass-perl-format --flag=__p:2:pass-perl-format --flag=__px:2:perl-brace-format --flag=__px:2:pass-perl-format --flag=__np:2:pass-perl-format --flag=__np:3:pass-perl-format --flag=__npx:2:perl-brace-format --flag=__npx:2:pass-perl-format --flag=__npx:3:perl-brace-format --flag=__npx:3:pass-perl-format --flag=N__:1:pass-perl-format --flag=N__n:1:pass-perl-format --flag=N__n:2:pass-perl-format --flag=N__p:2:pass-perl-format --flag=N__np:2:pass-perl-format --flag=N__np:3:pass-perl-format";
	$options =~ s!\s*--flag=\S+:\d:pass-perl-format!!g;
	"--language=Perl --from-code=UTF-8 --no-wrap --add-comments $options"
}

#          xgettext                 msginit                   msgfmt
# hello.c ----------> po/hello.pot ---------> po/fr/hello.po --------> po/fr/hello.mo

name "mkmsg";
args "ru,de,fr...";
port "создаёт файл локализации";
spec "если не указан язык, то обновляет файлы";
sub mkmsg {
	my ($lang) = @_;
	
	quit "не указан язык" if !length $lang;
	
	my $guard = $app->project->file->withdir;
	
	my $po = $app->file("i18n/$lang.po")->mkpath;
	quit "файл ".$po->path." уже есть" if $lang && $po->exists;
	
	make "msg";
	
	run "msginit --no-translator -i " . $pot->path . " --locale=${lang}_" . uc($lang) . ".UTF-8 -o " . $po->path;
	#run "msgfmt --output-file=" . $po->ext("mo")->path . " " . $po->path;
}

#          xgettext -j                  msgmerge                   msgfmt
# hello.c -------------> po/hello.pot -----------> po/fr/hello.po --------> po/fr/hello.mo

name "msg";
args "";
port "апдейтит файлы локализации";
task {
	
	my $guard = $app->project->file->withdir;
	
	my $pofiles = $app->project->file("i18n")->find("**.po");
	
	run "xgettext " . options() . " -f " . files() . " -o " . $pot->path;
	
	for my $po ($pofiles->slices) {
		run "msgmerge --no-wrap --update " . $po->path . " " . $pot->path;
		#run "msgfmt --output-file=" . $po->ext("mo")->path . " " . $pot->path;
	}
	
};

name "msgopt";
args "";
port "выводит настройки для xgettext";
sub msgopt {
	#xgettext `perl -MLocale::TextDomain -e 'print Locale::TextDomain->options'`
	require "Locale/TextDomain.pm";
	print Locale::TextDomain->options . "\n";
}

1;