package R::Make::Watch;
# следилки за изменением файлов

use common::sense;
use R::App;
use R::Make;



category "watch";

name "watch";
args "команды shell";
port "перезапускает команды shell при изменении файлов от текущей директории";
spec "1. останавливайте через Ctrl+C
2. ";
sub watch {
	my $cmd = join " ", @_;
	
	while() {
		my $watch = $app->file(".")->watch;
		if($watch->modify) {
			$cmd =~ s!%c!$watch->changed->join(",")!ge;
			$cmd =~ s!%n!$watch->news->join(",")!ge;
			$cmd =~ s!%d!$watch->deleted->join(",")!ge;
			eval { run $cmd };
		}
		sleep 0.1;
	}
}


name "daemon";
args "start|stop|restart|status|loop команда";
port "делает указанную команду al демоном";
spec "";
sub daemon {
	my $cmd = join " ", @_;
	
	...
}



category "Сервера";

name "dev";
args "";
port "запускает сервер разработки";
sub dev {
	quit "укажите в файле конфигурации service.dev с описанием проектов" if !$app->ini->{service}{dev};
	$app->dev->run;
	sleep 1000_000_000;
}

my $aura;

name "aura";
args "";
port "запускает web-сервер проекта";
sub aura {
	quit "укажите в файле конфигурации service.aura" if !$app->ini->{service}{aura};
	
	my $ps = $app->process($app->process->name);
	if($ps->pid) {
		$app->tty->confirm("web-сервер запущен. Остановить?")? $ps->stop: quit;
	}
	
	$app->process->running(sub {
		$aura = $app->aura->run;
	});
	sleep 1000_000_000;
}

name "scenario";
args "";
port "запускает web-сервер проекта для сценария";
sub scenario {
	quit "укажите в файле конфигурации service.aura" if !$app->ini->{service}{aura};
	$app->ini->{dev} = 1;
	
	my $ps = $app->process($app->process->name);
	if($ps->pid) {
		$app->tty->confirm("web-сервер запущен. Остановить?")? $ps->stop: quit;
	}
	
	$app->process->running(sub {
		$aura = $app->aura(%{$app->aura}, base_dev=>1, port=>3000)->run;
	});
	sleep 1000_000_000;
}


sub END {
	$aura->shutdown if $aura;
}
1;