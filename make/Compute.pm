package R::Make::Compute;
# задачи для вычислений

use common::sense;
use R::App;
use R::Make;

# конструктор
sub new {
	my ($cls) = @_;
	bless {}, ref $cls || $cls;
}

category "ВЫЧИСЛЕНИЯ";

name "-e";
args "выражение";
port "вычисляет выражение на perl и печатает результат";
test {
	quit "нет выражения" if @_<1;
};
task {
	my @ret = do { package main; eval(join " ", "use R::App;", @_) };
	if($@) { die }
	else { $app->log->info( map { ref $_? $app->perl->color($_): $_ } @ret ) }
};

name "-s";
args "[порт] выражение [запрос]";
port "запускает сервер на 3000 порту для выражения";
spec "В выражении можно использовать \$q - запрос.

Если указан запрос (al -s '\"hi!\"' /mips), то он сразу отправляется на сервер, результат распечатывается и сервер завершается. Иначе сервер ожидает запросов.

Так сервер, который распечатывает все запросы к нему и возвращает ответ 200 с пустым ассоциативным массивом json (\"{}\"):

	al -s 'msg \$q->stringify; \"{}\"'

";
test {
	quit "нет выражения" if @_<1;
};
task {
	my $port = 3000;
	my $ritter = shift;
	
	$port = $ritter, $ritter = shift if $ritter =~ /^\d+$/;
	quit "не указано выражение" if !length $ritter;

	eval "package R::__::ServerEval { use common::sense; use R::App; sub ritter { my (\$q) = \@_; $ritter } }";
	die if $@;
	
	my $s = server { R::__::ServerEval::ritter(@_) } port => $port;
	my $sv = $s->sv;	# охраняемая переменная для завершения сервера
	
	$app->tty->raw;
	
	if(@_) {
		my $path = shift;
		my $query = "\$app->url('localhost:$port$path')" . join(" ", @_) . "->res";
		print "$query\n\n";
		my $res = do { package main;
			eval $query;
		};
		die if $@;
		
		$app->log->info($res->status_line);
		map2 { $app->log->info(":sep(: )", $a, "$b") } $res->headers->flatten;
		print "\n";
		print $res->decoded_content;
		print "\n";
		
		$s->shutdown;
		
		exit;
	} else {
		sleep 1000_000;
	}
};

name "-r";
args "выражение";
port "подгружает модели и вычисляет выражение на perl";
test {
	quit "нет выражения" if @_<1;
};
task {
	#$app->meta;
	#$app->bench->time;
	my @ret = do { package main; eval($_[0]) };
	$app->raise->pass if $@;
	$app->log->info( @ret );
	#$app->log->info( $app->bench->log );

};


name "-a";
args "выражение";
port "подгружает шаблоны и вычисляет выражение на lukull";
test {
	quit "нет выражения" if @_<1;
};
task {
	
	#$app->modelMetafieldset->load_all_models;
	$app->view->load;
	local $app->view->{log} = $_[1];
	my @ret = $app->view->eval($_[0], {}, "-a");
	$app->log->info( @ret );
};

name "-x";
args "выражение [-]";
port "вычисляет выражение на lukull";
spec "- - вывести время выполнения";
test {
	quit "нет выражения" if @_<1;
};
task {
	$app->bench->time if $_[1];
	my @ret = $app->view->eval($_[0], {});
	$app->log->info( @ret );
	$app->log->info( $app->bench->log ) if $_[1];
	
};

name "-A";
args "выражение";
port "подгружает шаблоны и вычисляет выражение на шаблонизаторе lukull";
test {
	quit "нет выражения" if @_<1;
};
task {
	
	#$app->modelMetafieldset->load_all_models;
	$app->view->load;
	my @ret = $app->view->create($_[0], {});
	$app->log->info( @ret );
};

name "-X";
args "выражение";
port "компиллирует выражение на lukull";
test {
	quit "нет выражения" if @_<1;
};
task {
	my ($code) = @_;
	my $js = $app->view->masking($code);
	print $app->color->perl( $js ), "\n";
};

name "-J";
args "выражение";
port "компиллирует выражение на lukull в переводе на js";
test {
	quit "нет выражения" if @_<1;
};
task {
	my ($code) = @_;
	local $app->view->{lang};
	local $app->view->{view} = "-J";
	my $js = $app->view->lang("js")->masking($code);
	print $app->color->js( $js ), "\n";
};

name "-j";
args "выражение";
port "вычисляет выражение на lukull в переводе на js";
test {
	quit "нет выражения" if @_<1;
};
task {
	my ($code) = @_;
	local $app->view->{view} = "-j";
	local $app->view->{lang};
	my $js = $app->view->lang("js")->masking($code);
	print $app->color->js( $js ), "\n";
	#$app->viewJavascript(view=>$app->view)->vitaland
	my $ret = $app->node->eval("var DATA={}; " . $js);
	$app->log->info( $ret );
};

name "-je";
args "выражение";
port "вычисляет выражение на JE.pm (javascript-библиотека perl)";
test {
	quit "нет выражения" if @_<1;
};
task {
	my ($code) = @_;
	my $ret = $app->javascript->from($code, "-je", 1);
	print $app->perl->color($ret);
	print "\n";
};

name "-js";
args "выражение";
port "вычисляет выражение на node";
test {
	quit "нет выражения" if @_<1;
};
task {
	#$app->bench->time
	$app->tty->raw;
	local $_ = $_[0];
	s/"/\\"/g;
	system "node -e 'console.dir(eval(\"$_\"), {colors: true, depth: null, showHidden: true})'";
	#$app->log->info( $app->bench->log );
};

name "-cs";
args "выражение";
port "вычисляет выражение на coffeescript";
test {
	quit "нет выражения" if @_<1;
};
task {
	#$app->bench->time
	$app->tty->raw;
	system "coffee -e 'console.dir((do -> $_[0]), {colors: true, depth: null, showHidden: true})'";
	#$app->log->info( $app->bench->log );
	
};

name "-go";
args "";
port "вычисляет выражение на GoLang";
task {
	my ($include, $code) = @_==1? (undef, @_): @_;
	
	$include = join "", map { "import \"$_\"\n" } split /,/, $include;
	
	$app->tty->raw;
	
	my @code = split /;/, $code;
	my $last = pop @code;
	$code = join "\n", @code;
	
	$code = qq{package main

$include
import "fmt"

func main() {
	$code
    //fmt.Printf("%#v\\n", $last)
	fmt.Println($last)
}	
};
	my $f = $app->file("compute__.go");
	my $cv = guard { $f->rm };
	$f->write($code);
	my $path = $f->path;
	print `go run $path`;
};


name "-io";
args "code";
port "вычисляет выражение на io";
task {
	my ($code) = @_;
	my $f = $app->file(".compute.io");
	my $cv = guard { $f->rm };
	$f->write($code . " println");
	$app->tty->raw;
	print `io ${\$f->path}`;
};

# INFO: https://www.ibm.com/developerworks/ru/library/l-gcc-hacks/index.html
name "-c";
args "[-i|-M библиотеки] [-s] [формат] выражение";
port "вычисляет выражение на gcc: al -c %i 'int a[2] = {10, 20}; a[1]' ";
spec "
-i либ1,...          -> #include <либ1.h>
-M либ1,...        -> #include \"либ1.h\"
-s                     печатать листинг ассемблера, без выполнения
-o опция1,...     добавить опции компиллятору, например: -o -std=gnu++11,-std=c++11,-lm
";
task {
	my ($include, $fmt, $assembler, $exp, $opt);
	for(my $i=0; $i<@_; $i++) {
		local $_ = $_[$i];
		if($_ eq "-i") { $include .= join "", map { "#include <$_.h>\n" } split /,/, $_[++$i]; }
		elsif($_ eq "-M") { $include .= join "", map { "#include \"$_.h\"\n" } split /,/, $_[++$i]; }
		elsif($_ eq "-s") { $assembler = 1; }
		elsif($_ eq "-o") { $opt = join " ", split /,_/, $_[++$i]; }
		elsif(defined $exp) { $fmt = $exp; $exp = $_; }
		else { $exp = $_; }
	}
	
	quit "используйте:  al -c -f '%i' 'int a[2] = {10, 20}; a[1]'" if !defined $exp;

	$app->tty->raw;
	
	if($fmt) {
		$fmt =~ s/"/\\"/g;
		$exp = "printf(\"$fmt\\n\", ({ $exp; }) )";
	}
	
	
	my $f = $app->file(($ENV{TEMP} // $ENV{TMP} // "/tmp") . "/rubin-forms/compute/compute.c")->mkpath->write("
#include <stdio.h>
#include <stdlib.h>
$include

int main() {
	$exp;
	return 0;
}
");

	$opt .= " -fextended-identifiers";
	$opt .= " -I " . $app->file->pwd;

	#-std=c++11 -std=gnu++11 
	if($assembler) {
		my $asm = $f->ext(".s");
		my $s = "gcc $opt -S ".$f->path. " -o ".$asm->path;
		print "$s\n";
		my $ret = system $s;
		print $asm->read if $ret == 0;
	}
	else {
		my $exe = $f->ext(".exe");
		my $s = "gcc $opt ".$f->path." -o ".$exe->path;
		print "$s\n";
		my $ret = system $s;
		system $exe->path if $ret == 0;	
	}	
};

name "-g";
args "выражение";
port "вычисляет выражение на g++";
test {
	quit "используйте: al -g 'map,vector' 'int a[2] = {10, 20}; a[1]'" if @_!=1 && @_!=2;
};
task {
	my ($include, $e) = @_==1? (undef, @_): @_;
	
	#$app->bench->time
	$app->tty->raw;
		
	$include = join "", map { "#include <$_>\n" } split /,/, $include;
	
	my $f = $app->file(($ENV{TEMP} // $ENV{TMP} // "/tmp") . "/rubin-forms/compute/compute-g.cpp")->mkpath->write("
#include <iostream>
#include <string>
#include <typeinfo>
#include <map>
#include <vector>
$include

using namespace std;

int main() {
	cout << ({ $e; }) << '\\n';
	return 0;
}
");
	my $exe = $f->ext(".exe");
	
	#-std=c++11 -std=gnu++11 
	my $s = "g++ ".$f->path." -o ".$exe->path;
	#print "$s\n";
	my $ret = system $s;
	system $exe->path if $ret == 0;
	#$app->log->info( $app->bench->log );
	
};

name "sh";
args "";
port "запускает perl-шелл";
sub sh {
	require "Term/ReadLine.pm";
	
	my $term = Term::ReadLine->new('Perl Shell');
	my $prompt = "𓂀 ";
	utf8::encode($prompt);
	my $OUT = $term->OUT || \*STDOUT;
	while ( defined ($_ = $term->readline($prompt)) ) {
		my @ret = do { package main; eval($_) };
		$app->log->error($app->raise->last) if $@;
		$app->log->info( map { ref $_? $app->perl->color($_): $_ } @ret ) unless $@;
		$term->addhistory($_) if /\S/;
	}
}

1;
