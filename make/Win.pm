package R::Make::win;
# работа с виндой

use common::sense;
use R::App;
use R::Make;


category "windows 10";

use constant VK_CONTROL => 0x11;


sub edit {
	my ($s) = @_;
	# удаляем пробелы и последнюю точку
	$s =~ s!^\s*(.*?)\.?\s*$!$1!s;

	# распознаём класс и преобразуем его в путь
	if($s =~ /^(\w+(?:::\w+)*) (\d+)$/) {
		my ($r, $n) = ($1, $2);
		$r =~ s!::!/!g;
		$s = "$r.pm:$n";
	}

	# разделитель файла и строки : или line
	return if $s !~ m!^(.*)(?::|\s+line\s+)(\d+)$!;
	my ($file, $line) = ($1, $2);
	
	# ищем в проекте в котром запустили и в фреймворке, а так же в доппутях из ini
	my $f;
	if($file !~ /^\//) {
		my $ff = $app->project->files->add(@{$app->ini->{make}{win}{"clipboard-paths"}});
		$f = $ff->sub($file)->add( $ff->sub("lib")->sub($file) )->abs;
	}
	else {
		$f = $app->file($file);
	}

	$f = $f->existing;
	
	return if !$f->length;
	
	$app->editor->open($f->path, $line);
}

name "clipboard";
args "";
port "распознаёт в буфере пути к файлам со №строки и открывает в редакторе";
spec "нужно установить в автозагрузку

в etc/user.yml добывьте пути к папкам в которых находятся ваши проекты:

make:
	win:
		clipboard-paths:
			- /cygdrive/q/__/unicorn/x
			- /cygdrive/q/__/unicorn/unicorn

";
sub clipboard {
	if($^O  =~ /^(?:MSWin32|cygwin)$/) { win32() }
	else { unix() }
}


# 
sub win32 {
	# require Win32::API;
	# our $rsGetKeyState = new Win32::API("user32", "GetKeyState", "I", "N");
	# sub GetKeyState {
		# my ($keyCode) = @_;

		# my $ret = our($rsGetKeyState)->Call($keyCode);
		# $ret = unpack("S", pack("s", $ret));
		
		# return( $ret & 2**15 ? 1 : 0 );
	# }

	

	require Win32::Clipboard;
 
	my $clipboard = Win32::Clipboard();
	
	$clipboard->WaitForChange(0);	# чтобы пропустить срабатывание буфера при запуске
	
	while() {
		my $ret = $clipboard->WaitForChange(10);
		
		# my $c = GetKeyState(ord 'c');
		# my $C = GetKeyState(ord 'C');
		# my $Ctrl = GetKeyState(VK_CONTROL);
		# return if $Ctrl && ($c || $C);
		
		die "error!!!" if !defined $ret;
		next if $ret == 0;
		
		next if !$clipboard->IsText();
		
		my $s = $clipboard->GetText();
		#print "Clipboard contains: $s ", $app->file($s)->cygwin->path, "\n";
		edit($s);
	}
	
	
	 
	# $CLIP->Set("some text to copy into the clipboard");
	 
	# $CLIP->Empty();
	 
	# $CLIP->WaitForChange();
	# print "Clipboard has changed!\n";
}


sub unix {

	my $prev = `xclip -o`;
	utf8::decode($prev);
	while() {
		sleep 0.25;
		my $s = `xclip -o`;
		utf8::decode($s);
		next if $prev eq $s;
		print "$s\n";
		edit($s);
		$prev = $s;
	}
}

1;