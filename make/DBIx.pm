package R::Make::DBIx;
# сниппеты для создания кучи файлов для орма

use common::sense;
use R::App;
use R::Make;

# конструктор
sub new {
	my ($cls) = @_;
	bless {}, ref $cls || $cls;
}


category "ccxx-faraday (DBIx)";

name "one2one";
args "resultset связь";
port "создаёт свойства для объединённой сущности";
sub one2one {
	my ($resultset, $assign) = @_;

	$assign //= "a";

	my $x = $app->file("lib/CCXX/Faraday/DB/DBIC/Schema/Result/$resultset.pm")->read;

	while($x =~ /^=head2 (\w+)/gm) {
		printf <<'END', $1, $assign, $1;
sub %s {
    my ($self, @args) = @_;
    return $self->%s->%s(@args);
}

END
	};
}

name "file_a";
args "поле json-поле1...";
port "создаёт свойства из json-поля";
sub file_a {
	my ($fld, @fld) = @_;

	for(@fld) {
		printf <<'END', $_, $fld, $_;
sub %s {
    my ($self) = @_;
    return $self->%s->{%s};
}

END
	};
}

name "entity";
args "name [наследуемый-класс-таблицы] [force]";
port "создаёт все нужные файлы для сущности DBIx";
sub entity {
	my ($name, $parent, $force) = @_;

	$parent //= $name;
	
	my $name_path = $name;
	$name_path =~ s!::!/!g;
	
	my $parent_path = $parent;
	$parent_path =~ s!::!/!g;
	
	my $entity = $app->file("lib/CCXX/Faraday/Entity/$name_path.pm");
	my $set = $app->file("lib/CCXX/Faraday/EntitySet/$name_path.pm");
	my $t_entity = $app->file("t/ccxx/faraday/entity/".lc($name_path).".t");
	my $resultset = $app->file("lib/CCXX/Faraday/DB/DBIC/Schema/Result/$parent_path.pm");
	
	print $entity->path . " exists\n" if $entity->exists;
	print $set->path . " exists\n" if $set->exists;
	print $t_entity->path . " exists\n" if $t_entity->exists;
	
	my $all = $entity->add($set, $t_entity);
	
	quit "таблицы существуют" if !$force && $all->any(sub { $_->exists });
	
	$all->mkpath;
	
	my $relations;
	$resultset->replace(sub { s/(=head1 RELATIONS\n.*?)^#/$relations = $1; '#'/sme });
	
	$relations =~  s!CCXX::Faraday::DB::DBIC::Schema::Result::!CXX::Faraday::Entity::!g;
	
	eval "use lib 'lib'";
	
	my $Resultset = "CCXX::Faraday::DB::DBIC::Schema::Result::$parent";
	my $rs = $app->use($Resultset)->new;
	
	my @columns = grep { $_ ne "id" } $rs->columns;
	my %row = map {
		my $col = $rs->column_info($_);
		my $type = $col->{data_type};
		my $null = $col->{is_nullable};
		my $size = $col->{size};
		
		if($type =~ /int/i) { $_ => 10 }
		elsif($type =~ /decimal|double|float|numeric/i) { $_ => 6.3 }
		elsif($type =~ /^time$/i ) { $_ => '00:00:00' }
		elsif($type =~ /time/i ) { $_ => '1991-12-26 00:00:00' }
		elsif($type =~ /date/i ) { $_ => '1991-12-26' }
		else { $_ => "str" }
		
	} @columns;
	
	my $row = $app->perl->dump(\%row);
	
	$entity->write("package CCXX::Faraday::Entity::$name;
use strict;
use warnings;
use utf8;

use parent '$Resultset';

__PACKAGE__->result_class(__PACKAGE__);
__PACKAGE__->resultset_class('CCXX::Faraday::EntitySet::$name');

$relations

1;
");

	$set->write("package CCXX::Faraday::EntitySet::$name;
use strict;
use warnings;
use utf8;

use parent 'CCXX::Faraday::EntitySet::Base';

1;
");


	$t_entity->write("#!/usr/bin/perl
use strict;
use warnings;
use utf8;
use open qw/:utf8 :std/;

use Test::More tests => 1;

use Test::Exception;
use CCXX::Test::Faraday;

my \$db = CCXX::Test::Faraday->new(namespace => 'CCXX::Faraday::Entity', debug=>0);

subtest 'Create $name entity' => sub {
	plan tests => 1;
	my \$entity = \$db->model('$name')->create($row);

	my \@result = \$db->model('$name')->search($row);
	is \@result, 1, \"Извлечена добавленная запись\";
};

");


	print "для тестирования:\n";
	print "al t220 0 ".$t_entity->path."\n";
	
}


name "faradaydump";
args "table [entity]";
port "дампит класс в фарадее";
spec '
ПАРАМЕТРЫ

* table - имя таблицы
* entity - класс сущности

УСТАНОВКА ПРОЕКТА ДЛЯ ИСПОЛЬЗОВАНИЯ faradaydump

1. клонируешь весь проект git clone git@bitbucket.org:darij/rubin-forms.git
2. cd rubin-forms
3. perl rubin-forms mkage ~/bin
3.1 проверить, что `echo $PATH` содержит ~/bin
4. cd ..
5. создать конфиг-файл с параметрами базы:

конфиг-файл называется так же как и папка в которой он находится

допустим структура папок такая:
	220v/
	  rubin-forms/			склонированный проект с командой faradaydump
	  CCXX-Faraday/			проект с DBIx
	  220v.yml 				конфиг-файл

Содержимое конфиг-файла (220v.yml):

include: []
    
dev: 0

project:
    company: mycompany
    license: BSD-2-Clause
    author: I am I

service:
    connect:
        basename: <имя базы данных>
        host: <хост базы данных>
        user: <пользователь базы данных>
        password: <пароль>
		
6. Укажите параметры базы данных в конфиг-файле.
7. cd CCXX-Faraday
8. al faradaydump <таблица>

';
sub faradaydump {
	my ($table, $entity) = @_;
	
	length $table or quit "Таблицу сюда! al faradaydump <таблица>";
	$app->connect->info->{$table} or quit "Таблицы $table нет в базе!";
	
	my $class = ucfirst $table;
	$class =~ s/s(_|$)/$1/i;
	$class =~ s/_([A-Z])/uc $1/gei;
	my $class_name = $class;
	$class = 'CCXX::Faraday::DB::DBIC::Schema::Result::' . $class;

	my $file = $class;
	$file =~ s!::!/!g;
	$file .= ".pm";

	
	$app->file("lib/$file")->exists && ($app->tty->confirm("Класс таблицы (lib/$file) существует. Перезаписать всё?") || quit "выхожу");
	
	my $guard = guard {
		print "copy\n";
		
		my $x = $app->file("ex/$file")->read;
		
		$entity //= $class_name;
				
		$x =~ s!^use utf8;\s*!!;
		$x =~ s!use warnings;!$&\nuse utf8;!;
		
		$app->file("lib/$file")->write($x);
		
		$app->file("ex")->rm;
		
		make "entity", $entity, $class_name, 1;
	};
	
	require DBIx::Class::Schema::Loader;
	
	$app->file("ex")->rm;
	my $sv = $app->file("ex")->mkdir->withdir;
	
	DBIx::Class::Schema::Loader::make_schema_at(
		'CCXX::Faraday::DB::DBIC::Schema',
		{ 
			debug => 0, 
			dump_directory => '.',
			#overwrite_modifications=>0,
			#dump_overwrite=>0,
		},
		[ 'DBI:mysql:database='.$app->connect->basename.';host='.$app->connect->host.';port='.$app->connect->port.';mysql_compression=1;mysql_init_command=set names utf8, sql_mode = "";',$app->connect->user,$app->connect->password,
		{
			mysql_enable_utf8 => 1,
		},
		],
		
		
	);
	
}

name "relation";
args "model_from model_to";
port "выдаёт связь";
sub relation {
	my ($from, $to) = @_;
	my $From = ucfirst $from;
	my $To = ucfirst $to;
	
	print "
# ссылка на другую таблицу
__PACKAGE__->belongs_to(
  $to =>
  'CCXX::Faraday::Entity::$To',
  { 'foreign.id' => 'self.${to}_id' },
  { cascade_copy         => 0, cascade_delete => 0 },
);

# ссылка из других таблиц
__PACKAGE__->has_many(
  $from =>
  'CCXX::Faraday::Entity::$From',
  { 'foreign.${to}_id' => 'self.id' },
  { cascade_copy         => 0, cascade_delete => 0 },
);
	";
}


1;