package R::Make::Assistant;
# разнообразные вспомогательные задания не вошедшие ни в одну категорию

use common::sense;
use R::App;
use R::Make;


        # cp_etc_home          копирует файл проектов в домашний файл проектов
        # help [name]          возвращает список команд
        # kill                 убивает сервер
        # look [тест]          функциональные тесты в папке look
        # man [опции miu] [маска файлов] [маска разделов]... компиллирует файлы man компиллятором miu
        # tags имя_функции     создаёт базу и ищет по ней функцию или класс
        # utf8                 проверка всех модулей на кодировку utf-8
		


category "АССИСТЕНТЫ";

name "cat";
args "[sep]";
port "список категорий команд";
spec "
если sep указан, то 
параметр sep может принимать значения:
* 0 и др. нулевые для perl - \n
* любое другое - сепаратор
";
task {
	my $sep = $_[0];
	$app->make->load;
	
	if(defined $sep) {
		for my $category (@R::Make::CATEGORY) {
			$sep ||= "\n";
			print "$category->{category}$sep";
		}
		return;
	}
		
	my $len = max map { length $_->{category} } @R::Make::CATEGORY;
	
	for my $category (@R::Make::CATEGORY) {
		$app->log->info( ":empty bold black", sprintf("%${len}s ", $category->{category}), ":space reset", map { $_->{name} } @{$category->{tasks}});
	}
};

name "-help";
args "задание";
port "информация о команде";
task {
	my $name = $_[0];

	$app->make->load($name);
	
	my $task = $R::Make::TASK{$name};
	quit "нет задачи `$name`" if !$task;

	$app->log->info(":bold white", "НАИМЕНОВАНИЕ");
	$app->log->info("    $name - $task->{port}\n");
	$app->log->info(":bold white", "РЕЗЮМЕ");
	$app->log->info(":bold white space", "    $name", ":reset", "$task->{args}\n");
	$app->log->info(":bold white", "РАСПОЛОЖЕНИЕ");
	$app->log->info("    ФАЙЛ      $task->{file}\n    СТРОКА    $task->{line}\n");
	$app->log->info(":bold white", "КОНЦЕПЦИЯ");
	$app->log->info($task->{spec} // "НЕТ");

};

name "help";
args "[маска]";
port "список команд";
spec "если указан параметр - распечатывает всю информацию по этому заданию";
sub help {
	my $name = $_[0];
	
	$app->make->load;
	
	for my $category (@R::Make::CATEGORY) {

		my $cat = $category->{category};
		my $flag = 1;

		my $tasks = $category->{tasks};
		
		for my $task (@$tasks) {
		
			#msg1 $category, $category =~ /$name/?1:0, $task, $task =~ /$name/?1:0;
			next unless $cat =~ /$name/i or $task->{name} =~ /$name/i;
		
			my $name = $task->{name};
			my $args = $task->{args};
			my $help = $task->{port};
			$name .= " $args" if defined $args;
			my $len = length $name;
			
			$flag = 0, $app->log->info( ":empty", ":bold", "\n", ":bold black", $cat, ":" ) if $cat ne "" and $flag;
			$app->log->info( ":empty", ":bold black", "\t", $name, ":reset", (" " x  (20 - $len)) . ($help? " ".$help: "") );
		}
	}

}

name "coffee";
args "[1] [файлы]";
port "компилирует coffee-файлы в /html";
sub coffee {
	my $force;
	$force = shift if $_[0] eq 1;
	$app->file(@_? @_: "html")->find("**.<coffee,iced,litcoffee>")->then(sub {
		if($force || !$_->ext("js")->exists || $_->mtime > $_->ext("js")->mtime) {
			$app->coffee->compile($_);
			$app->log->info(":space blue", $_->path, ":magenta", "compile");
		}
	});
}

name "xf";
args "[1] [файлы]";
port "компилирует xf-файлы в /html и /xf";
sub xf {
	my $force;
	$force = shift if $_[0] eq 1;
	
	$app->file(@_? @_: ("html", "xf"))->find("**.<xf>")->then(sub {
		if($force || !$_->sub("var")->exists || $_->mtime > $_->sub("var")->mtime) {
			$app->xf->compile($_);
			$app->log->info(":space blue", $_->path, ":magenta", "compile");
		}
	});
}

name "cc";
args "[1]";
port "компилирует coffee и xf";
sub cc {
	make "coffee", @_;
	make "xf", @_;
}

name "ccview";
args "[file...]";
port "компилирует все view или указанные файлы и распечатывает роуты";
sub ccview {
	$app->view->load(@_);
	R::map2 { print "$a -> $b\n" } @{$app->router->{routes}};
}


name "home";
port "копирует файл проектов в домашний файл проектов";
sub home {

	open my $f, "etc/notepad++.project" or die $!;

	my $flag;
	my @x;
	
	while(<$f>) {

		$flag=1 if /<Project name="miu">/;

		s/C:\\__/C:\\lang/, push @x, $_ if $flag;
	}

	close $f;

	open $f, ">", "etc/notepad++-home.project" or die $!;
	print $f "<NotepadPlus>\n";
	print $f @x;
	close $f;

}

name "shiva";
args "";
port "список заданий крона";
sub shiva {
	#my $tasks = $app->shiva->{tasks};
	TODO;
}

name "del13";
args "";
port "удаляет 13-й символ";
sub del13 {
	# "etc", "lib", "view", "migrate", "model", "html", "man", $0
	$app->file(".")->encode(undef)->find("**/.gitignore,**.<pm,pl,man,human,%htm%,js,css,coffee,yml,ini,%php%,twig,tmpl,txt>")->replace(sub { print $_[0]->path . "\n" if s/\r//g })
	
}

name "file_utf8";
args "";
port "проверка всех модулей на кодировку";
sub file_utf8 {
	
	$app->file($app->project->name, "etc/${\$app->project->name}.ini", "view", "model", "lib", "man")->find(sub {
		return if -d $_;
		my $file = `file "$_"`;
		print "file $_\n$file\n\n" if $file !~ /UTF-8/;
		0;
	});

}


my @DEF_FIND_DIRS = qw/man html lib view model/;
my $DEF_FIND_EXTS = "**.pl;**.pm;**.man;**.human;**.html;**.js;**.jsx;**.coffee;**.litcoffee;**.css;**.pass;**.sass;**.scss;**.less;**.stylus";

name "utf8";
args "[files] [exts] [codepage]";
port "проверка на кодировку";
sub utf8 {
	my ($files, $exts, $codepage) = @_;
	
	my @files = $files? split(/,/, $files): @DEF_FIND_DIRS;
	$exts ||= $DEF_FIND_EXTS;
	
	my $c=0;
	
	$app->file(@files)->encode($codepage || "utf8")->find($exts)->then(sub { 
		my $f = $_;
		local $SIG{__WARN__} = sub {
			print $f->path . "\n";
		};
		$f->read;
		$c++;
	});
	
	print "Проверено $c файлов\n";
}

name "find";
args "[options] regexp";
port "поиск файлов с текстом";
spec "-f маска - выбирает файлы, которые соответствуют маске
-i кодировка - файлы будут открыты в этой кодировке
-d директории - файлы и директории в которых будет производится поиск. Через запятую.
	По-умолчанию: .
-n маски - файлы и директории в которых не будет производится поиск. Через запятую. 
	По-умолчанию: /.git/
-r текст - заменяет найденный текст на этот. Можно использовать \$N, \$' и т.п. для замены групп
";
sub find {
	my $re = pop @_;
	my %opt = @_;
	my $encode = delete $opt{-i};
	my $mask = delete $opt{-f};
	my $replace = delete $opt{-r};
	my @dirs = split /\s*,\s*/, delete($opt{-d}) // ".";
	my @nf = split /\s*,\s*/, delete($opt{-n}) // "/.git/";
	
	quit "нераспознанные опции: " . join ", ", keys %opt if keys %opt;	
	
	if(!defined $encode) {
		utf8::encode($encode) if defined $encode and utf8::is_utf8($encode);
		utf8::encode($mask) if defined $mask and utf8::is_utf8($mask);
		utf8::encode($replace) if defined $replace and utf8::is_utf8($replace);
		eval "use bytes; no utf8";
	}
	
	$replace = eval join("", "sub {", $app->magnitudeLiteral->to($replace), "}") // die if defined $replace;
	
	$app->file(@dirs)->find("-f", \@nf, defined($mask)? $mask: (), sub {
		my $f = $app->file->one($_)->encode($encode);
		$_ = $f->read;
		
		my $count;
		if(defined $replace) {
			return 0 if !s/$re/$count++; $replace->()/ge;
			$f->write($_);
			$app->log->info(":blue", $f->path, ":bold red", $count);
		}
		else {
			while(/$re/g) { $count++ }
			return 0 if !$count;
			$app->log->info(":cyan", $f->path, ":bold magenta", $count);
		}
		
		0
	});
	
}

name "find_old";
args "[files [exts] [codepage]] regexp";
port "поиск в тексте по маске";
sub find_old {
	my ($files, $exts, $codepage, $regexp);
	if(@_ == 1) { ($regexp) = @_; }
	elsif(@_ == 2) { ($files, $regexp) = @_; }
	elsif(@_ == 3) { ($files, $exts, $regexp) = @_; }
	else { ($files, $exts, $codepage, $regexp) = @_; }
	
	my @files = $files? split(/,/, $files): @DEF_FIND_DIRS;
	$exts ||= $DEF_FIND_EXTS;
	$codepage = !defined($codepage)? "utf8": $codepage eq ""? undef: $codepage;
	
	$app->log->info(":green", "find", ":reset", @files, ":blue", $codepage, ":yellow", $exts, ":cyan", $regexp);
	
	for my $f ($app->file(@files)->encode($codepage)->find($exts)->slices) {
		my $count = 0;
		my $file = $f->read;
		while($file =~ /$regexp/go) {
			$count++;
		}
		$app->log->info(":empty black bold", " $count\t", ":reset", $f->path) if $count;
	}
}


name "replace_old";
args "[files [exts] [codepage]] regexp sub";
port "заменяет текст во всех файлах проекта";
spec "
files - файлы и директории в которых менять через запятую, по умолчанию - в ".join(",", @DEF_FIND_DIRS)."
exts - расширения файлов через точку с запятой, по умолчанию - $DEF_FIND_EXTS
regexp - регулярное выражение для замены в файле
sub - текст для замены, может содержать \$1-10, \$& и ${\uc \$ENV{PATH}}
";
sub replace_old {
	my ($files, $exts, $codepage, $regexp, $sub);
	
	if(@_ == 2) { ($regexp, $sub) = @_; }
	elsif(@_ == 3) { ($files, $regexp, $sub) = @_; }
	elsif(@_ == 4) { ($files, $exts, $regexp, $sub) = @_; }
	else { ($files, $exts, $codepage, $regexp, $sub) = @_; }

	my @files = $files? split(/,/, $files): @DEF_FIND_DIRS;
	$exts ||= $DEF_FIND_EXTS;
	$codepage = !defined($codepage)? "utf8": $codepage eq ""? undef: $codepage;
	
	my $code = eval "sub { qq{$sub} }";
	die if $@;
	
	$app->log->info(":green", "replace", ":reset", @files, ":blue", $codepage, ":yellow", $exts, ":cyan", $regexp, ":magenta", $sub);
	$app->file(@files)->encode($codepage)->find($exts)->replace(sub {
		my $file = $_[0];
		my $is = s!$regexp!$code->()!ge;
		app->log->info( ":empty red", ($is? "*  ": "   "), ":reset", $file->path);
	});
}



name "chmod";
args "";
port "изменяет права файлов на 0600, а директорий на 0744";
sub chmod {

    my $mod = 0600;
    my $grep_chmod = sub { $app->file($_)->mod != $mod };
    my $print_dir = sub { printf "-d %o\t%s", $mod, $_->path . "\n" };
    my $print_file = sub { printf "-f %o\t%s", $mod, $_->path . "\n" };

	if( 0 && $app->file(".git")->isdir ) {

		my @hide = qw/lib migrate model man var view etc ex .gitignore Makefile/;
		my @front = qw/html/;
		
        $mod = 0600;
		$app->file(@hide)->find("-f", $grep_chmod)->mod($mod)->then($print_file);
        $mod = 0744;
		$app->file(@hide, @front)->find("-d", $grep_chmod)->mod($mod)->then($print_dir);
		
        $mod = 0622;
		$app->file(@front)->find("-f", $grep_chmod)->mod($mod)->then($print_file);
        $mod = 0700;
		$app->file($app->project_name)->find("-f", $grep_chmod)->mod($mod)->then($print_file);
		
		$app->log->info("изменены права файлов на стандартные");
	}
	else {
        
        my @any = $app->file("*")->glob->grep(sub { !/^\.git$/ });
        
        $mod = 0644;
		$app->file(@any)->find("-f", $grep_chmod)->mod($mod)->then($print_file);
        $mod = 0755;
		$app->file(@any)->find("-d", $grep_chmod)->mod($mod)->then($print_dir);
        
        $mod = 0764;
		$app->file(".git")->find("-f", $grep_chmod)->mod($mod)->then($print_file);
        $mod = 0775;
		$app->file(".git")->find("-d", $grep_chmod)->mod($mod)->then($print_dir);
	}
	
	
	
}


name "mkage";
args "[dir=/bin]";
port "создаёт интерпретатор в dir";
spec "Создаёт в указанной директории скрипты al, ag, age, чтобы они были доступны отовсюду";
sub mkage {
	my ($dir) = @_;
	
	$dir //= "/bin";
	
	my $path = $app->file(__FILE__)->file("")->sub("/../lib")->abs->path;
	$path =~ s/'/\\'/g;
	
	my $ag = $app->file("$dir/ag");
	my $age = $app->file("$dir/age");
	my $al = $app->file("$dir/al");
	my @is;
	
	push @is, "ag" if $ag->exists;
	push @is, "age" if $age->exists;
	push @is, "al" if $al->exists;
	
	if(@is) {
		return if !$app->tty->confirm("Файлы ". join(", ", @is) ." уже существуют, перезаписать?");
	}
	
	# запустить указанный файл, если без параметров - то запускает сервер
	$ag->write("#!$^X\n" . $app->copiright->file . '
BEGIN { push @INC, \''.$path.'\' }
use common::sense;
use R::App;

my $file = @ARGV? shift( @ARGV ): "Aquafile";

$app->syntaxAg->ag($file, @ARGV);

1
')->mod(0755);

	# выполнить выражение с командной строки
	$age->write("#!$^X\n" . $app->copiright->file . '
BEGIN { push @INC, \''.$path.'\' }
use common::sense;
use R::App;

$app->log->info( $app->syntaxAg->eval($ARGV[0]) );
')->mod(0755);

	# Assetfile, Rubinfile, Alfile, Aquafile, Asfile, Accfile, Aliasfile
	# Al2O3, al2o3

	# мэйк
	$al->write("#!$^X\n" . $app->copiright->file . '
BEGIN { push @INC, \''.$path.'\' }
use common::sense;
use R::App;
use R::Make;

if( $app->file("Aquafile")->isfile ) {
	$app->syntaxAg->ag("Aquafile");
}

push @INC, $app->project->path("lib") if $app->project->path ne $app->framework->path;

make @ARGV;
')->mod(0755);

	# .gitignore, .rrrumiurc, .miurc, .rrrumiu

	$ag->add($age, $al)->ext("cmd")->then(sub {
		$_->write("\@perl ${\$_->ext('')->path} %*");
	});

	print "ag, age, al ".(@is? "перезаписаны": "созданы")."\n";
}


name "pkgls";
args "[dir] [cpan|cpanfile]";
port "показывает все подключаемые модули из use и require в проекте";
sub pkgls {
	my ($dir, $cpan) = @_;

	$dir //= "lib";
	
	my %package;
	$app->file(split /,/, $dir)->find(qr/\.(pl|pm)$/n)->then(sub {
		#print "finds in $_->{parts}[0]\n";
		my $file_path = $_->path;
		my $file = $_->read;
		while($file =~ m{
			\#.*  |
			"(\\"|[^"])*" | '(\\'|[^'])*' |
			\b(use|require)[\t\ ]+ ( (?<pkg>[a-zA-Z_][\w:]*) | (?<path> ['"][a-zA-Z_][\w/]*.pm['"] ) )
		}gnx) {
			my $pkg = $+{pkg};
			my $path = $+{path};
			
			next if !defined $pkg and !defined $+{path};
			
			my $lineno = 1;
			my $from = $`;
			$lineno++ while $from =~ /\n/g;
			
			if(defined $path) {
				$pkg = $app->perl->unstring($path);
				$pkg =~ s!/!::!;
				$pkg =~ s!\.(\w+)$!!;
			}
			
			push @{$package{ $pkg }}, "$file_path:$lineno";
		}
	});
	
	print "\n#-".(keys %package)."----------------------\n\n";
	
	for my $key (sort keys %package) {
		
		my $version = eval { my $path=$key; $path=~s!::!/!g; require "$path.pm"; $key->VERSION };
		
		my $files = join "", map { "# $_\n" } @{$package{$key}};
		
		if($@) {
			my $x = "$key: $@";
			$x =~ s!^!# !gm;
			print "$files$x\n\n";
		}
		
		if(defined $version) {
			if($cpan eq "cpan") {
				print "cpan install $key\n";
			} else {
				
				#$version = ">= $version" if $version ne "";
				
				
				
				print "${files}requires '$key', '$version';\n\n";
			}
		}
	}

}


# name "markmk";
# args "";
# port "создаёт документацию";
# sub markmk {
	
	# make("man", "", "-a");
	
	# $app->file("mark")->rmdown->mkdir;
	
	# my $link;
	
	# $app->file("var/.MAN/man/*.markdown")->glob->then(sub {
		# my $title = $_->read =~ /^\s*#[\t ]+(.*)/? $1: $_->nik;
		# $link .= "1. [$title](mark/" . $_->file . ")\n";
		# $_->cp("mark/" . $_->file);
	# });
	
	
	
	# $app->file("README.md")->replace(sub {
		# s!(## Документация).*$!$1\n\n$link\n!s;
	# });
# }


# name "trace";
# args "";
# port "трейс стека последней ошибки";
# sub trace {
	# my $file = $app->file("var/last.raise.trace");
	# print("нет трейса ошибки\n"), return if !$file->exists;
	# my $trace = $file->read;
	# $trace = $app->perl->loader($trace);
	# my $msg = $app->raise->new($trace, "error", undef);
	# $app->log->alert("$msg");
# }

# name "dier";
# args "";
# port "";
# sub dier {
	# die "для трейса!";
# }

# name "com";
# args "";
# port "компилирует com/*.!(jsx|coffee) в var/com/*.js";
# sub com {
	# $app->tty->raw;
	# my $rf = $app->framework->root;
	# $rf =~ s!^/cygdrive/(\w)/!$1:/! if $^O eq "cygwin";
	# print `gulp --cwd=$rf --gulpfile=$rf/node/gulpfile.js cc`
# }


name "chtag";
args "[filemask]";
port "подсчитывает открытые и закрытые теги";
sub chtag {
	use bytes;
	$app->tty->raw;	
	
	my ($exts) = @_;
	
	$exts //= "**";
	my $f = $app->file(".")->find($exts, "-f", sub {
		print STDERR "$_\n";
		my $f = $app->file->one($_)->encode(undef)->open;
		
		my @S;
		my @L;
		my $remark;
		my $fail;
		
		while(<$f>) {
			while(/\{%\s*(\w+)|(\{#)|(#\})/g) {
				#msg1 length($`), "$& => $line";
				msg1("$.: неоткрытый комментарий #}"), next if $3 and !$remark;
				$remark=0, next if $3;
				next if $remark;
				$remark=$., next if $2;
				
				my $tag = $1;
				if($tag =~ /^(?:else|elsif|elseif|now|include|extends|elif|csrf_token|include_legacy|rr_hash|static|thumbnail|load|block_super|templatetag|dump|include_file)$/) {}
				elsif($tag =~ s/^end//) {
					if(!@S || $S[$#S] ne $tag) {
						$fail++;
						msg1 "\n\t$.: неоткрытый тег `end$tag`. В стеке " . (!@S? '<ничего>': $S[$#S]);
					}
					else { pop @S; pop @L; }
				} else {
					push @S, $tag;
					push @L, $.;
				}
			}
		}
		
		my $i = 0;
		$fail++, msg1 "незакрытые теги:\n" . join "", map { "\t" . $L[$i++]. ":$_\n" } @S if @S;
	
		msg1 "$remark: незакрытый комментарий {#" if $remark;
	
		0;
	});
	
}

name "mkcpan";
args "[lib_dirs] [t_dirs]";
port "печатает cpanfile на stdout";
sub mkcpan {
	my ($lib_dir, $t_dir) = @_;
	$lib_dir //= ["lib"];
	$t_dir //= ["t"];
	
	my %pack = $app->perl->set(qw/lib open utf8/);
	my %module;
	$app->file(@$lib_dir)->find("**.pm")->then(sub {
		my $s = $_->open;
		
		while(<$s>) {
			s/#.*//;
			$pack{$1}++ if /\bpackage\s+([a-zA-Z_][\w:]*)/;
			do { my $x=$2; $x=~s!/!::!g; $module{$x}++ } if /\brequire\s*('|"|q[qw]?.)([\w\/]+)\.pm/;
			$module{$2}++ if /\b(use|require)\s+([a-zA-Z_][\w:]*)/;
		}
	});
	
	my %t_module;
	$app->file(@$t_dir)->find("**.t")->then(sub {
		my $s = $_->open;
		while(<$s>) {
			s/#.*//;
			do { my $x=$2; $x=~s!/!::!g; $t_module{$x}++ } if /\brequire\s*('|"|q[qw]?.)([\w\/]+)\.pm/;
			$t_module{$2}++ if /\b(use|require)\s+([a-zA-Z_][\w:]*)/;
		}
	});
	
	delete $t_module{$_} for keys(%module), keys %pack;
	delete $module{$_} for keys %pack;
	
	
	print "requires 'perl', '5.008001';\n\n";

	print "on 'test' => sub {\n";
	printf "%-40s# %s\n", "\trequires '$_';", eval "use $_ qw//; $_->VERSION" for sort keys %t_module;
	print "};\n\n";
	
	printf "%-40s# %s\n", "\trequires '$_';", eval "use $_ qw//; $_->VERSION" for sort keys %module;
}

name "n";
args "маска";
port "открывает найденные по маске файлы в notepad++";
sub n {
	my ($mask) = @_;
	my $re = $app->perl->like("**/*$mask*", "i");
	my $f = $app->file(@DEF_FIND_DIRS)->find($re, "-f");
	quit "нет таких файлов" if !$f->length;
	
	print $f->join("\n")."\n";
	
	return if !$app->tty->confirm("открыть в npp?");
	
	my $x = $f->join(" ");
	print `notepad++ $x`;
}

name "save";
args "[remark]";
port "сохраняет rubin-forms, unicorn и miu";
sub save {
	my ($rem) = @_;
	$rem //= "save";
	my $f = $app->file(@INC)->grep(sub { /rubin-forms/ })->file("..")->cd;
	
	
	for my $x (qw!rubin-forms unicorn miu!) {
		my $fx = $f->sub($x)->cd;
		$app->log->info($fx->path, ":blue", "al push $rem");
		eval { make "push", $rem };
	}
	
}

name "load";
args "";
port "обновляет rubin-forms, unicorn и miu";
sub load {
	my $f = $app->file(@INC)->grep(sub { /rubin-forms/ })->file("..")->cd;
	
	for my $x (qw!rubin-forms unicorn miu!) {
		my $fx = $f->sub($x)->cd;
		$app->log->info($fx->path, ":blue", "al pull");
		eval { make "pull" };
	}
	
}


my %MEDIATOR_METHOD = qw{
	get/files/{productId}/audios/{filename}									audio
	get/files/{productId}/{mediaType}/{watermarkType}/{filter}/{filename}	image
	get/files/{productId}/certificates/{filename}  							certificate
	get/prices/					prices
	post/prices/				create_prices
	get/prices/{priceType}/		prices_typed
	post/prices/{priceType}/	create_prices_typed
	get/products/{productIds}/	media
	get/products/{productIds}/{mediaType}/				products
	post/products/{productId}/{mediaType}/ 				create_products
	delete/products/{productId}/{mediaType}/{mediaId}/ 	erase_products
	put/products/{productId}/{mediaType}/{mediaId}/		update_products
	patch/products/{productId}/{mediaType}/				place_products
	post/sync/											sync	
	
	
};

my %MEDIATOR_PARAM = qw{
	productIds	ids
	mediaType	type
	mediaId		id
	productId	id
	priceType	type
	priceRegion	region
	priceClusterId id
	priceFile	file
	watermarkType	watermark
	product_ids	ids
};

my %MEDIATOR_EX = qw{
	productIds	[8199]
	mediaType	"certificates"
	mediaId		1
	productId	8199
	priceType	"spares"
	priceRegion	77
	priceClusterId 1
	priceFile 	"data:text/csv;base64,SGVsbG8gV29ybGQhCg=="
	file		"..."
	watermarkType	"thumb_diy"
	product_ids	[8199]
	filter		"n200_q40"
	filename	"1.jpeg"
	order 		[]
};

my %MEDIATOR = (path2name => \%MEDIATOR_METHOD, param2name=>\%MEDIATOR_PARAM, example => \%MEDIATOR_EX);

my %PAYMENT = qw{
	get/payments/{id}/ payment
	post/payments/{id}/ payment_action
	post/updates/ updates_create
	get/paylinks/ paylinks
	post/paylinks/ paylinks_create
	get/paylinks/{id}/ paylink
	get/shops/{shop_id}/orders/{shop_order_id}/ order
};

my %SELFAPI_METHOD = qw{
	get/orders/	orders
};

my %SELFAPI_PARAM = qw{

};

my %SELFAPI_EX = qw{

};

my %SELFAPI = (
	path2name => \%SELFAPI_METHOD, 
	param2name=>\%SELFAPI_PARAM, 
	example => \%SELFAPI_EX
);



name "swagger";
args "pod|perl|t [conf.yml]";
port "Формирует pod-документацию из yml-файла сваггера";
sub swagger {
	my ($method, $file) = @_;

	#$file //= "http://newmedia.220-volt.ru/openapi/medi/v1/mediator.yml";
	$file //= "https://selfapi.220-volt.ru/static/openapi/v2/selfapi.yml";
	
	my $swagger = $app->mime->swagger->from(($file =~ m!^https?://!? app->url($file): $app->file($file))->read);

	#msg $yml->{paths};
	print $swagger->$method(%SELFAPI);	
}

name "rank";
args "категория [символ] [размер]";
port "Распечатывает категорию и копирует в буфер обмена";
sub rank {
	my ($rank, $sym, $len) = @_;
	
	$sym //= "~";
	$len //= 93;
	
	$rank = " $rank ";
	my $x = ($len - length($rank) - 2) / 2;
	my $r = join "", ($sym x $x), $rank, ($sym x $x);

	$r = "$sym$r" if $len > length $r;
	$r = "# $r";
	#print "$r\n";
	print length $r;print "\n";
	
	run "echo '$r' | xsel -b -i";
}

name "pdoc";
args "каталоги и файлы";
port "формирует документацию в markdown-формате из исходников питона";
sub pdoc {
	my $x=$app->file(@_)->find("**.py")->then(sub {
		my $path = $_->path;
		local $_ = `pdoc3 $path`;
		utf8::decode($_);
		
		my $p = $path;
		$p =~ s!(/__init__)?\.py$!!;
		$p =~ s!/!.!g;
		
		s!^Module.*!## МОДУЛЬ $p!gm;
		s!^Functions!### FUNCTIONS!gm;
		s!^Classes!### CLASSES!gm;
		
		s!^===+$!!gm;
		s!^---+$!!gm;
		s!^[ \t]+!!gm;
		s!^#+ \S+!uc $&!mge;
		
		
		my $class = undef;

		my $r =	sub {
			local $_ = $1;
			my $opis = $2;
			my $all = $&;
			my ($name, $args) = m!^(\w+)\(([^\)]*)!;
			my @args = grep { $_ ne "self" } split /,\s*/, $args;
			
			$args = join ", ", @args;
			
			
			my $cls = $class? do { my $x=lcfirst $class; $x=~s![A-Z]!"_".lc $&!ge; "$x."}: "";
			$class//=$name;
			
			my @def;
			
			join "\n", "#### $name\n\n$opis\n\n`$name = $cls$name($args)`\n\n##### ARGUMENTS\n", (map {
				my ($n, $def) = split /\s*=\s*/, $_;
				my $x = defined($def)? do { push @def, "`$n` по умолчанию принимает значение `$def`."; "Необязательный"}: "Обязательный";
				"* `$n` - . $x"
			} @args), (@def? "\n".join("\n\n", @def): ""), "\n##### RETURNS"
		};

		s!^`([^`\n]+)`\s*:[ \t]*(.*)!$r->()!gme;
		print;
	});
}

name "version";
args "[версия]";
port "Синхронизирует версию в README.md, package.json и т.д.";
sub version {
	my $v = $_[0] // do {
		my @version;
		$app->file("package.json")->existing->then(sub { $_->read =~ /"version":\s*"([^"]*)"/ && push(@version, $1) && $app->log->info(":space red", "прочитана версия из", ":blue", $_->path, ":magenta", $1) });
		
		$app->file("README.md")->existing->then(sub { $_->read =~ /# VERSION\s*(\S+)/ && push(@version, $1) && $app->log->info(":space red", "прочитана версия из", ":blue", $_->path, ":magenta", $1) });
		
		my $v = max @version;
		$app->log->info(":space black bold", "выбрана версия", ":red", $v);
		$v
	};
	
	$app->file("README.md")->existing->replace(sub {s!(# VERSION\s*)\S+!$1$v! && $app->log->info(":space yellow", "обновлена версия в", ":blue", $_[0]->path) });
	$app->file("package.json")->existing->replace(sub {s!("version":\s*)"[^"]*"!$1"$v"! && $app->log->info(":space yellow", "обновлена версия в", ":green", $_[0]->path)});
}

name "to_str";
args "file";
port "переводит содержимое файла в строку";
sub to_str {
	print app->magnitudeLiteral->perl(app->file(@_)->encode(undef)->read);
}


# sub s_to_swagger1 {
	# my $code = shift;
	# my $s = {};	# сопоставления
	# my $root = $s->{$code} = {};

	# Tour {
		# my $h = $s->{$_} //= {};

		# if(ref $b eq "HASH") {
			
			# $s->{$b} = $h->{properties}{$a} =  {
				# type => "object",
      			# title => delete($b->{"~example"}),
      			# properties => $s->{$b},
			# };
		# }
		# elsif(ref $b eq "ARRAY") {

			# $s->{$b} = $h->{properties}{$a} =  {
				# type => "array",
      			# title => "", #delete($b->{"~example"}),
      			# items => $s->{$b},
			# };
		# }
		# elsif(Num $b and $b == int $b) {
			# $h->{properties}{$a} =  {
				# type => "integer",
      			# title => delete($_->{"~example_$a"}),
      			# example => $b,
			# };	
		# }
		# elsif(Num $b) {
			# $h->{properties}{$a} =  {
				# type => "number",
      			# title => delete($_->{"~example_$a"}),
      			# example => $b,
			# };
		# }
		# elsif(!defined $b) {
			# $h->{properties}{$a} =  {
				# type => "integer",
				# nullable => $app->json->true,
      			# title => delete($_->{"~example_$a"}),
      			# example => 123,
			# };	
		# }
		# elsif($b eq $app->json->true || $b eq $app->json->false) {
			# $h->{properties}{$a} =  {
				# type => "boolean",
      			# title => delete($_->{"~example_$a"}),
      			# example => 123,
			# };	
		# }
		# else {
			# msg1 "yyy", $a, $b, $_;
			# if(ref $_ eq "ARRAY") {
				# $h->{items}[$a] =  {
					# type => "string",
	      			# title => delete($_->{"~example_$a"}),
	      			# example => $b,
				# };		
			# } else {
				# $h->{properties}{$a} =  {
					# type => "string",
	      			# title => delete($_->{"~example_$a"}),
	      			# example => $b,
				# };		
			# }
		# }
	# } $code;

	# return $root;
# }


my %ORDER;
sub s_to_swagger {
	my ($v, $space, $k, $desc) = @_;
	
	my @x;
	
	if(defined $k) {
		push @x, "$space$k:\n";
	}
	
	$desc = $desc =~ /\n/? "|\n" . do { $desc =~ s/^/$space    /gm; $desc }: $desc =~ /['"]/? $app->magnitudeLiteral->to($desc): $desc;
	push @x, "$space  description: $desc\n";
	
	if(ref $v eq "HASH") {
		push @x, "$space  type: object
$space  properties:
", map { s_to_swagger($v->{$_}, "$space    ", $_, $v->{"$_.description"}) } grep { !/.description$/ } 
		sort { $ORDER{$a} <=> $ORDER{$b} } keys %$v;
	}
	elsif(ref $v eq "ARRAY") {
		push @x, "$space  type: array
$space  items:
", map { "$space  - " . s_to_swagger($_, "$space    ") } @$v
	}
	else {
		my $type = $v =~ /^-?\d+\z/? "integer": Num($v)? "number": "string";
		
		push @x, "$space  type: $type
";
		push @x, "$space  minimum: 0
" if Num $v;

		my $example = $v =~ /["']/? $app->magnitudeLiteral->to($v): $v;
		push @x, "$space  example: $example
";
	}

	join "", @x;
}


name "hash2openapi";
args "[pl|json|yaml|yml]";
port "переводит структуру в формат openapi (swagger)";
spec "
al hash2openapi < file.pl > openapi.yml
al hash2openapi json < file.json > openapi.yml
al hash2openapi yaml < file.yml > openapi.yml
";
sub hash2openapi {
	my ($fmt, $code) = @_;

	$code //= join "", <STDIN>;
	my $data;
    #msg1 $code;

	if(!defined $fmt or $fmt eq "pl") { $code = eval $code }
	elsif($fmt =~ /yml|yaml/) { $code = $app->yaml->from($code) }
	elsif($fmt eq "json") {
        #msg1 ":green", "hi!!!!", ":reset", $code;
		
		#$code =~ s!\{\s*#\s*(.*?)\s*$!"{ \"~example\": ".$app->magnitudeLiteral->to($1).","!gme;
		#$code =~ s!("(.*?)":.*?)(,?\s*)#\s*(.*?)\s*$!"$1, \"~example_$2\": ".$app->magnitudeLiteral->to($4).$3!gme;
		#print STDERR $code;
		
		$code =~ s/\t/    /g;
			
		$code = _jsonc2json($code);
		
		#print $code;
		
		$data = $app->json->from($code);
		
	}
	else { quit "неверный формат" }

	
	#my $x = $app->yaml->to(s_to_swagger($code));
	#$x =~ s{!!perl/scalar:JSON::PP::Boolean 1}{true}g;
	#$x =~ s{!!perl/scalar:JSON::PP::Boolean 0}{false}g;
	
	my $n = 0;
	while($code =~ /"(\w+)":/g) {
		$ORDER{$1} = ++$n;
	}
	
	print s_to_swagger($data);
}

name "jsonc2json";
args "file.jsonc";
port "переводит таблицу БД в формат openapi (swagger)";
spec "

	al jsonc2json x.json_with_comments > x.json
	
";
sub jsonc2json {
	my $file = shift;
	
	my $x = app->file($file)->read;
	
	sub comment {
		local $_ = shift;
		
		s/^[ \t]*# ?//gm;
		
		s/\.?\s*$/./;
		
		$app->magnitudeLiteral->to($_) . ",\n"
	}
	
	# #title { -> { .title: "title"
	sub _jsonc2json {
		local $_ = shift;
		s!((?:^[ \t]*#(?:.*\n))+)(\s+"(\w+)":\s*[\{\[])!"\"$3.description\": " . comment($1) . $2!gme;
		
		s!^(([ \t]*)"(\w+)":[^#\n]*)((?:[ \t]*#(?:.*\n))+)! "\n$2\"$3.description\":" . comment($4) . "$1\n" !gme;
		
		$_
	}
	
	print _jsonc2json($x);
}

name "table2openapi";
args "table";
port "переводит таблицу БД в формат openapi (swagger)";
spec "
al table2openapi таблица > openapi.yml
";
sub table2openapi {
	my ($tab) = @_;
	my $info = $app->connect->info->{$tab};

	#msg $info;

	my $c = keys %$info;
	my $acc;
	for my $col (asc {$_->{pos}} values %$info) {

		my $type = 
			#$col->{type} eq "tinyint(1)"? "boolean": 
			$col->{type} =~ /int/? "integer": 
			$col->{type} =~ /decimal/? "number": "string";

		#print STDERR ($acc++)."/$c $col->{name}\n";
		print "$col->{name}:\n";
		print "  type: $type\n";
		print "  nullable: true\n" if $col->{null};
		my $x = $col->{remark};
		if(!$x) {
			$x = "~";
		} else {
			$x =~ s/^[\w]/uc $&/e;
			$x =~ s/\s+$//;
			$x .= "." if $x !~ /\.$/;
			$x = $app->magnitudeLiteral->to($x) if $x =~ /["':\(\)\{\}\[\]]/ or $x eq "";
		}
		print "  description: $x\n";

		my $x = $app->connect->query($tab, "max($col->{name}) as $col->{name}");
		if(!defined $x) {
			$x = "~";
		}
		
		$x = $app->magnitudeLiteral->to($x) if $x =~ /['":\(\)\{\}\[\]]/ or $x eq "";

		print "  example: $x\n";
	}

}

name "openapi_params";
args "[in]";
port "Возвращает openapi парметры в yml";
spec "

	al openapi_params < x.json

";
sub openapi_params {
	my ($in) = @_;

	$in //= 'query';

	my @x; my @required;

	while(<STDIN>) {
		#title — 'Наименование производителя' (строка); обязательный параметр
		#price — "Цена товара в розничной сети, в копейках" (число); необязательный параметр
		quit "$.: строка не распознана" if !/^(\w+)\s*[—-]\s*['"](.*?)['"]\s*\((.*?)\);?\s*((?:не)?обязательный параметр)?(?:, по умолчанию\s+(.*))?/;
		my $required = $5? 'true': 'false';
		my $default = defined($6)? "\n    default: $6": "";
		my ($type, $ex) = $3 eq 'строка'? ('string', '?'): 
			$3 eq 'число'? ("integer\n                  minimum: 0", 123): 
			$3 eq 'булево'? ('boolean', 'false'): 
			'?';

		if($in ne "post") {
			push @x,
"  - name: $1
    in: $in
    required: $required
    description: $2$default
    schema:
      type: $type
      example: $ex
";
		} else {
			push @required, "                - $1\n" if $required eq "true";

			push @x, "
                $1:
                  type: $type
                  description: $2$default
                  example: $ex";
		}
        
	}
    

    if($in ne "post") {
    	unshift @x, "      parameters:\n";
    }
    else {
    	my $required = @required? "              required:\n".join("", @required): "";
    	unshift @x, "
      requestBody:
        required: true
        content:
          application/x-www-form-urlencoded:
            schema:
              type: object$required
              properties:";
    }

    print @x;
}


sub get_dict_titles {
	my $dict = {};
	my $key; my $ident;
	
	my $home = $ENV{HOME};
	
	for my $file (
		"$home/220v/220V/htdocs/openapi/site/v1/site.yml",
		"$home/220v/selfapi/site/general/static/openapi/v1/selfapi.yml",
		"$home/220v/selfapi/site/general/static/openapi/v2/selfapi.yml",
		"$home/220v/selfapi/site/general/static/openapi/v3/selfapi.yml",
	) {
		my $f = $app->file($file)->open;
		while(<$f>) {
			$ident=$1, $key = $2 if /^(\s*)(\w+):\s*$/;
			push @{$dict->{$key}}, $1 if /^$ident  title:\s*(.*)/;
			push @{$dict->{$key}}, $1 if /^$ident  description:\s*(.*)/;
		}
		close $f;
	}

	my $info = $app->connect->info;
	for my $tab (keys %$info) {
		#next if $tab !~ /Ru_Order/;
		my $col = $info->{$tab};
		
		do {
			my $x = $col->{$_}{remark};
			$x =~ s/\.$//;
			push @{$dict->{$_}}, "$x. Из таблицы $tab." if $col->{$_}{remark} 
		} for keys %$col;
	}
	
	$dict->{$_} = [R::uniq(@{$dict->{$_}})] for keys %$dict;
	
	return $dict;
}

name "dict_titles";
args "";
port "Возвращает yml словарь с параметрами";
spec "

	al dict_titles > titles.yml

";
sub dict_titles {
	print app->yaml->to(get_dict_titles());
}

name "json2schema";
args "";
port "переводит структуру в формат openapi (swagger)";
spec "
al json2schema < file.json > schema.yml
";
sub json2schema {
	my ($fmt, $code) = @_;

	my $title = get_dict_titles();


	my @S = [];

	while(<STDIN>) {
		# if(s/^\s*\{//) {
			# push @S, "  title: ~
  # type: object
  # properties:
    # \$ref: '#/components/schemas/'
# ";
			# next;
		# }
		# next if /^\s*$/;
		my $type = /^\s*"(\w+)":\s*(-?\d+\.\d+)\s*(,\s*)?$/? "float": 
			/^\s*"(\w+)":\s*(-?\d+)\s*(,\s*)?$/? "integer":
			/^\s*"(\w+)":\s*(true|false)\s*(,\s*)?$/? "boolean":
			/^\s*"(\w+)":\s*("(\\"|[^"])*")\s*(,\s*)?$/? "string":
			"";
		
		if($type eq "") {
			print;
		} else {
		
			my $key = $1; my $example = $2;
			
			$example = $app->magnitudeLiteral->from($example) if $type eq "string";
			
			my $t = $title->{$key} // "~";
			$t =~ s/\.\s*$//;
			print "  $key:
    title: $t
    type: $type
    example: $example
";
		}
	}

	
	
}

1;
