 use WWW::Scripter;
  $w = new WWW::Scripter;
  
  $w->use_plugin('JavaScript');
  $w->get('http://www.cpan.org/');
  $w->get('javascript:alert("Hello!")'); # prints Hello!
  
  $w->use_plugin(JavaScript =>
          engine  => 'SpiderMonkey',
          init    => \&init, # initialisation function
  );                         # for the JS environment