###
  * company: Emerald labs
  * license: MIT
  * author:  darthyar
  *
  * компонент реакт
###

import { h, Component } from 'Reactive'
import Layout from 'devel/layout'

export default class Routers extends Component

	constructor: (props)->
		super props
		#@asset = {}
		#@stats = {}
		

	render: ({routers})->
	
		Link = ({ enabled, children, href })->
			#say "Link" 
			if enabled
				<a href={href}>{children}</a>
			else
				<span>{children}</span>
	
		<Layout>
			<div class="">{
				for router in routers
					via = router.via.join("|")
					<div class="row">
						{via} <Link enabled={"GET" in router.via and router.mask and !/[\*:]/.test(router.mask)}
							href={router.mask}>{router.mask}</Link>
					</div>
			}</div>
		</Layout>

