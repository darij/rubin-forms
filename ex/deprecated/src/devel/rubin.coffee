###
  * company: Emerald labs
  * license: MIT
  * author:  darthyar
  *
  * документация по компонентам rubin
  *
  * навеян страничкой https://ajainvivek.github.io/preact-fluid/
###

import { h, Component } from 'Reactive'
import Head from 'lib/preact-head'


import {LeftMenu} from 'rubin-forms/layout'
import Layout from 'devel/layout'

Section = ({link, children})-> <div><h3><a name={link}>{link}</a></h3>{children}</div>

import {Grid, Cell} from 'rubin-forms/grid'


export default class Rubin extends Component

	render: (props, stats)->

		sections = <div>
			<Section link="Введение">
				Компоненты rubin
			</Section>
			
			<Section link="Grid">
				Grid - это сетка
				Cell - ячейка сети
				
				
				
				
			</Section>
		</div>
	
		<Layout>
			<Head>
				<title>Компоненты Rubin</title>
			</Head>
			
			<LeftMenu>
				<ol>{
					for section in sections.children
						link = section.attributes.link
						<li><a href={"#"+link}>{link}</a></li>
				}</ol>
			</LeftMenu>
			
			
			{sections}

		</Layout>
