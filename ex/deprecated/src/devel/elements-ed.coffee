###
  * company: Emerald labs
  * license: MIT
  * author:  darthyar
  *
  * страница
###

import { h, Component } from 'Reactive'


import {Menu, LeftMenu, RightMenu, Context, Footer} from 'rubin-forms/layout'
import Layout from 'devel/layout'

import {css, StyleSheet, minify} from 'lib/aphrodite'

minify false

style = StyleSheet.create
	table:
		height: "100%"
	planshet:
		position: "relative"
		#background: "hsla(340, 50%, 50%, 0.5)"
		overflow: "auto"
		borderBottom: "solid 20px hsl(230, 50%, 60%)"
	planshetRow:
		height: "80%"
	controlRow:
		height: "20%"
	mapTD:
		padding: "1px 3px 3px 1px"
	map:
		position: "relative"
		height: "100%"
	element:
		position: "absolute"
		background: "hsla(1, 0%, 0%, 0.5)"
		borderRadius: "10px"
		padding: "10px"
	point:
		position: "absolute"
		width: 2
		height: 2
		backgroundColor: "hsl(230, 50%, 70%)"
	far:
		position: "absolute"
		width: 0
		height: 0
	layer:
		overflow: "auto"
		height: "100%"
	history:
		background: "hsla(1, 0%, 0%, 0.5)"
		borderRadius: 10
		padding: 5
		margin: 5
		

# import Element from 'devel/elements-ed/element'
# import Model from 'devel/elements-ed/model'
# import Controller from 'devel/elements-ed/controller'



class Element extends Component
	
	@finalEvents
		dragStart: (e)->
			# установим картинку
			e.dataTransfer.setDragImage @base, e.offsetX, e.offsetY
			# скрываем элемент
			setTimeout (=> @base.style.display = 'none'), 1
			
			# сообщаем эдитору кого тащат
			@context.ed.dnd = this
			# запоминаем смещение
			@offsetX = e.offsetX
			@offsetY = e.offsetY
			
		# выполняется после drop-а
		dragEnd: (e)->
			@dnd = null
		# todo: удалить - не нужно
		dragOver: (e)->
			e.dataTransfer.dropEffect = 'none'
			e.preventDefault()
			
		# добавляем в историю
		click: ->
			@context.ed.addToHistory @props.src
		
	# вызывается из дропзоны (эдитора)
	drop: (e)->
		@base.style.display = ""
		planshet = @context.ed.planshet
		x = parseInt e.offsetX + planshet.scrollLeft - @offsetX
		y = parseInt e.offsetY + planshet.scrollTop - @offsetY

		@context.ed.setState (state)=> {elements: {state.elements..., [@props.src]: {@props.element..., x:x, y:y} }}
		elem = {@props.element..., x:x, y:y}
		fetch "/devel/elements-ed/" + @props.src,
			method: "PUT"
			body: JSON.stringify elem
			headers:
				Accept: 'text/plain'
				'Content-Type': 'application/json'
			
		
	render: ({src, element})->
		<div class={css(style.element)} style={{left: element.x, top: element.y}} 
			draggable="true"
			onDragStart={@dragStart}
			onDragEnd={@dragEnd}
			onDragOver={@dragOver}
			onClick={@click}
		>
			{src}
		</div>


# редактор элементов
export default class ElementsEd extends Component

	constructor: (props)->
		super arguments...
		
		maxx = maxy = 0
		
		for src, element of props.elements
			if maxx < element.x then maxx = element.x
			if maxy < element.y then maxy = element.y
		
		@state = {props..., width: maxx, height: maxy, history: props.history || []}
	
	# чилды смогут обращатся к редактору как @context.ed
	getChildContext: -> { ed: this }
	
	# добавляем элемент в историю
	addToHistory: (elem)->
	
		@setState (state)->
			history = [elem]
			for i in state.history when i!=elem
				if history.length > 15 then break
				history.push i
				
			fetch "/devel/elements-ed",
				method: "POST",
				headers:
					Accept: 'text/plain'
					'Content-Type': 'application/json'
				body: JSON.stringify {history: history}
				
			return
				history: history

	@finalEvents
		dragOver: (e)->
			e.dataTransfer.dropEffect = 'move'
			e.preventDefault()
			
			planshet = @planshet
			x=e.offsetX
			y=e.offsetY
			say "dragOver", x, y, e
			
			# если мы несём элемент рядом с границей - то делаем скролл
			# если скролл в конце, то увеличиваем координаты far
			# если скролл в начале, то увеличиваем координаты всех элементов
			stepX = stepY = 50
			
			stepRight = 0
			stepBottom = 0
			
			if x < stepX
				if planshet.scrollLeft > stepX then	planshet.scrollLeft -= stepX
				else
					stepLeft = stepX - planshet.scrollLeft
					planshet.scrollLeft = 0

			if y < stepY
				if planshet.scrollTop > stepY then planshet.scrollTop -= stepY
				else
					stepTop = stepY - planshet.scrollTop
					planshet.scrollTop = 0
			
			if stepLeft	or stepTop
				say "step", stepLeft, stepTop
				@setState (state)->
					elements = {}
					for src, element of state.elements
						elements[src] = {element..., x: element.x+stepLeft, y: element.y+stepTop}
					{elements: elements}
					
			
			
			limit = planshet.clientWidth - stepX
			if x > limit
				if planshet.scrollLeft < limit then	planshet.scrollLeft += stepX
				else
					@setState (state)-> {width: state.width+stepX}
					planshet.scrollLeft = planshet.scrollWidth
		
			limit = planshet.clientHeight - stepY
			if y > limit
				if planshet.scrollTop < limit then	planshet.scrollLeft += stepX
				else
					@setState (state)-> {width: state.height+stepY}
					planshet.scrollTop = planshet.scrollHeight
					
			return
			
			
		# передаём на элемент
		drop: (e)->	@dnd?.drop(e)

		clickOnMap: (e)->			
			map = @map.getBoundingClientRect()
			planshet = @planshet.getBoundingClientRect()
			
			@planshet.scrollLeft = (e.clientX-map.left) / map.width * (@planshet.scrollWidth - planshet.width)
			@planshet.scrollTop = (e.clientY-map.top) / map.height * (@planshet.scrollHeight - planshet.height)
		
	
	render: ({makers}, {elements, width, height, history})->
	
		<Layout title="Редактор элементов">
			<Context>
				<table class={css style.table}>
					<tr class={css style.planshetRow}>
						<td ref={@linkRef "planshet"} colspan="3" class={css style.planshet}
							onDragOver={@dragOver}
							onDrop={@drop}
						>
							{for src, element of elements
								<Element src={src} element={element} />
							}
							<div ref={@linkRef "far"} class={css style.far} style={{top: height, left: width}}></div>
						</td>
					</tr>
					<tr class={css style.controlRow}>
						<td class={css style.mapTD}>
							<div ref={@linkRef "map"} class={css style.map} onClick={@clickOnMap}>
								{for src, element of elements
									<Point element={element} x={element.x / width} y={element.y / height} />
								}
							</div>
						</td>
						<td>
							<div class={css style.layer}>
								{for src in history
									<div class={css style.history}>{src}</div>
								}
							</div>
						</td>
						<td>
							<div class={css style.layer}>
								{for maker in makers
									<div class={css style.history}>{maker.src}</div>
								}
							</div>
						</td>
					</tr>
					
				</table>
			</Context>
		</Layout>

# в пикселах element.x. Переводим в %. Для этого получаем максимальное значение
Point = ({element, x, y})->
	<div class={css(style.point)} style={{left: x*100+"%", top: y*100+"%"}}></div>
