package devel::routers;
# среда разработки

use common::sense;
use R::App;
use R::Yogi;

# обращает в 
sub flat_routers {
	my ($r, $lev) = @_;
	$lev++;
	return +{
		lev => $lev-1,
		(map { $_=>$r->{$_} } qw/via mask stash controller/),
	}, map { flat_routers($_, $lev) } @{$r->{routers}}
}

get "/devel/routers" => sub {
	my $routers = $app->dev->routers;
	render "devel/routers", {routers => [ flat_routers($routers, 0) ] };
};


1;