###
  * company: Emerald labs
  * license: MIT
  * author:  darthyar
  *
  * компонент реакт
###

import { h, Component } from 'Reactive'

import Head from 'lib/preact-head'
import Lay, {Menu, LeftMenu, Content, RightMenu, Footer} from 'rubin-forms/layout'

export default class Layout extends Component

	render: ({children, title, others...})->
		<Lay oval {others...}>
			<Head>
				<title>{title || "..."} | Среда разработки</title>
				
			</Head>
			
			<Menu>menu</Menu>
			<LeftMenu></LeftMenu>
			<RightMenu align="left">
				<ul>
					<li><a href="/devel/elements-ed">Редактор элементов</a></li>
					<li><a href="/devel/rubin">Документация по css-фреймворку</a></li>
					<li><a href="/devel/routers">Роутеры</a></li>
				</ul>
			</RightMenu>

			{children}
			
			<Footer></Footer>
		</Lay>
