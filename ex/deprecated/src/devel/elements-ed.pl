package devel::elementsEd;
# редактор элементов

use common::sense;
use R::App;
use R::Yogi;

my $elements_file = $app->project->file("etc/elements-ed.yml");
my $elements_ini = $elements_file->exists? $app->yaml->from($elements_file->read): {};

my $project_path = quotemeta $app->project->file("snippet")->path . "/";
my $framework_path = quotemeta $app->framework->file("snippet")->path . "/";

$elements_ini->{makers} = [
	map { s/^($project_path|$framework_path)//; +{src=>$_} } $app->project->files("snippet")->find("-f")->abs
];
# структура: elements => {file => {x,y}}

#Tour { $b = int $b if $a~~[qw/x y/] } $elements_ini->{elements};


get "/devel/elements-ed" => sub {

	my $root = $app->project->file("src");
	my $f = $root->find("**.(pl;pm;coffee;litcoffee;jsx)")->root($root->path);

	my $elements = $elements_ini->{elements} //= {};
	my $y=0;
	for my $path ($f->parts) {
		msg $path;
		$y+=50;
		$elements->{$path} //= do {
			my $x = 0;
			$x += 50 while $path =~ m!/!g;
			
			my $type = "element";
			
			$type="controller" if $path =~ /\.(pl|pm)$/;
			
			+{ x=>$x, y=>$y, type=>$type };
		};
	}
	
	render "devel/elements-ed", {
		%$elements_ini
	}
};

put "/devel/elements-ed/*id" => sub {
	my $elements = $elements_ini->{elements} //= {};
	$elements->{attr->id} = $app->json->from(request->body);
	$elements_file->write($app->yaml->to($elements_ini));
	[200, ["Content-Type" => "text/plain"], "Ok"]
};

del "/devel/elements-ed/*id" => sub {
	my $elements = $elements_ini->{elements} //= {};
	delete $elements->{attr->id};
	$elements_file->write($app->yaml->to($elements_ini));
	[200, ["Content-Type" => "text/plain"], "Ok"]
};

# для сохранения истории
post "/devel/elements-ed" => sub {
	%{$elements_ini} = (%{$elements_ini}, %{ data() });
	$elements_file->write($app->yaml->to($elements_ini));
	[200, ["Content-Type" => "text/plain"], "Ok"]
};


1;