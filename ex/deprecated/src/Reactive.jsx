/**
 * company: Emerald Labs
 * license: MIT
 * author: darthyar
 *
 * тулсы для реакта
 * asset - общее состояние
 */

import preact from 'lib/preact';

/**
 * assetState = { asset_key: [component...] ... }
 * component.asset = { state_key: asset_key ... }
 */

var assetState = {}

var setState = function setState(state, callback) {
	var assign = this.assign
	if(assign) {
		for(var i in state) {
			if(i in assign) {
				var assign_key = assign[i]
				var a = assetState[assign_key]
				for(var k=0, n=a.length; k<n; k++) {
					var c = a[k]
					var key = c._asset[i]
					c.setState({[key]: state[i]}, callback)
				}
			}
		}			
	}
	
	this.constructor.prototype.setState.call(this, state, callback)
}

/**
 * связываем состояния
 * @param {Component} component
 */
preact.options.afterMount = function (component) {
    if (component.asset) {
        
		component.setState = setState
		
		var _asset = component._asset = {}
		var asset = component.asset
		for(var i in asset) {
			var asset_key = asset[i]
			_asset[asset_key] = i
			var a = assetState[asset_key]
			if(!a) a = assetState[asset_key] = []
			a.push(component)
		}
    }
};

/**
 * отключаем состяния
 * @param {Component} component
 */
preact.options.beforeUnmount = function (component) {
    if (component.asset) {
		
		var asset = component.asset
		for(var i in asset) {
			var asset_key = asset[i]
			var a = assetState[asset_key]
			if(a.length == 1) delete assetState[asset_key]
			else assetState[asset_key] = a.filter(c => c!==component)
		}
		
		delete component.setState
    }
};


// /**
 // * апдейтим состяния
 // * @param {Component} component
 // */
// preact.options.afterUpdate = function (component) {
    // if (component.asset) {
		
    // }
// };

//var $ = jQuery;

var loaders = {};
export function load(elem, fn) {
    if(loaders[elem.src]) {
        fn();
    } else {
        document.getElementsByTagName("head")[0].appendChild(elem)
        loaders[elem.src] = 1;
    }
}

/**
 * декоратор @bind - метод всегда будет вызываться со своим объектом
 * @param target
 * @param key
 * @param fn
 * @returns {{configurable: boolean, get: (function())}}
 */
export function bind(target, key, {value: fn}) {
    return {
        configurable: true,
        get() {
            let value = fn.bind(this);
            Object.defineProperty(this, key, {
                value,
                configurable: true,
                writable: true
            });
            return value;
        }
    };
};

/**
 * декоратор эвента - отменяет
 * @param target
 * @param key
 * @param fn
 * @returns {*}
 */
export function evt(target, key, {value: fn}) {
    return {
        configurable: true,
        get() {
            let value = (function (e, ...args) {
                var ret = fn.call(this, e, ...args);
                e.stopPropagation();
                e.preventDefault();
                return ret;
            }).bind(this);
            Object.defineProperty(this, key, {
                value,
                configurable: true,
                writable: true
            });
            return value;
        }
    };
}

export var linkRef = function linkRef(component, ref) {
	var cache = component.__lrc || (component.__lrc = {});
	return cache[ref] || (cache[ref] = function(dest) { component[ref] = dest });
}

export var linkState = function linkState(component, statePath, valuePath="target.value") {
	var cache = component.__lsc || (component.__lsc = {});
	return cache[ref] || (cache[ref] = new Function("e", "this.setState({"+statePath+": e."+valuePath+"})"))
}


// биндит обработчики евентов в кофескрипт в классах
Function.prototype.linkEvents = function linkEvents(methods) {
	for(let i in methods) {
		let method = methods[i]
		Object.defineProperty(this.prototype, i, {
			get: function() {
				return method.bind(this);
			}
		});
	}	
}

// биндит обработчики евентов в кофескрипт в классах и прибавляет stopPropagation (не распростаняться дальше)
Function.prototype.finalEvents = function linkEvents(methods) {
	for(let i in methods) {
		let method = methods[i]
		Object.defineProperty(this.prototype, i, {
			get: function() {
				return (function(e) {
					e.stopPropagation();
					method.apply(this, arguments);
				}).bind(this);
			}
		});
	}
}


/**
 * делает пропсы - состоянием
 */
export class Component extends preact.Component {
    constructor() {
        super(...arguments);
    }

	linkRef(ref) {
		return component => { this[ref] = component };
	}
	
	linkState(statePath, valuePath="target.value") {
		var path = valuePath.split(".")
		var n = path.length
		return e => {
			let val = e
			for(let i=0; i<n; i++) val = val[path[i]]
			this.setState({[statePath]: val});
		}
	}
	
	linkOn(key) {
		return this[key].bind(this)
	}
	
	cherryPick() {
		var props = this.props
		var attrs = this.attrs = {}
		var S = {}

		for(var i=0; i<arguments.length; i++) S[arguments[i]] = 1			
	
		for(var i in props) {
			if(i in S) this[i] = props[i]
			else attrs[i] = props[i]
		}
		
		delete attrs.children
	}
}


//export var Component = preact.Component;

export var React = preact;
export var render = preact.render;
export var createElement = preact.createElement;
export var h = preact.h;
export var cloneElement = preact.cloneElement;
export var rerender = preact.rerender;
