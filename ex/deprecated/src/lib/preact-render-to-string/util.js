'use strict';

exports.__esModule = true;
exports.styleObjToCss = styleObjToCss;
exports.hashToClassName = hashToClassName;
exports.assign = assign;
exports.getNodeProps = getNodeProps;
// DOM properties that should NOT have "px" added when numeric
var NON_DIMENSION_PROPS = exports.NON_DIMENSION_PROPS = {
	boxFlex: 1, boxFlexGroup: 1, columnCount: 1, fillOpacity: 1, flex: 1, flexGrow: 1,
	flexPositive: 1, flexShrink: 1, flexNegative: 1, fontWeight: 1, lineClamp: 1, lineHeight: 1,
	opacity: 1, order: 1, orphans: 1, strokeOpacity: 1, widows: 1, zIndex: 1, zoom: 1
};

var ESC = {
	'<': '&lt;',
	'>': '&gt;',
	'"': '&quot;',
	'&': '&amp;'
};

var objectKeys = exports.objectKeys = Object.keys || function (obj) {
	var keys = [];
	for (var i in obj) {
		if (obj.hasOwnProperty(i)) keys.push(i);
	}return keys;
};

var encodeEntities = exports.encodeEntities = function encodeEntities(s) {
	return String(s).replace(/[<>"&]/g, escapeChar);
};

var escapeChar = function escapeChar(a) {
	return ESC[a] || a;
};

var falsey = exports.falsey = function falsey(v) {
	return v == null || v === false;
};

var memoize = exports.memoize = function memoize(fn) {
	var mem = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
	return function (v) {
		return mem[v] || (mem[v] = fn(v));
	};
};

var indent = exports.indent = function indent(s, char) {
	return String(s).replace(/(\n+)/g, '$1' + (char || '\t'));
};

var isLargeString = exports.isLargeString = function isLargeString(s, length, ignoreLines) {
	return String(s).length > (length || 40) || !ignoreLines && String(s).indexOf('\n') !== -1 || String(s).indexOf('<') !== -1;
};

// Convert an Object style to a CSSText string
function styleObjToCss(s) {
	var str = '';
	for (var prop in s) {
		var val = s[prop];
		if (val != null) {
			if (str) str += ' ';
			str += jsToCss(prop);
			str += ': ';
			str += val;
			if (typeof val === 'number' && !NON_DIMENSION_PROPS[prop]) {
				str += 'px';
			}
			str += ';';
		}
	}
	return str || undefined;
}

// See https://github.com/developit/preact/blob/master/src/util.js#L61
function hashToClassName(c) {
	var str = '';
	for (var prop in c) {
		if (c[prop]) {
			if (str) str += ' ';
			str += prop;
		}
	}
	return str;
}

// Convert a JavaScript camel-case CSS property name to a CSS property name
var jsToCss = exports.jsToCss = memoize(function (s) {
	return s.replace(/([A-Z])/g, '-$1').toLowerCase();
});

function assign(obj, props) {
	for (var i in props) {
		obj[i] = props[i];
	}return obj;
}

function getNodeProps(vnode) {
	var defaultProps = vnode.nodeName.defaultProps,
	    props = assign({}, defaultProps || vnode.attributes);
	if (defaultProps) assign(props, vnode.attributes);
	if (vnode.children) props.children = vnode.children;
	return props;
}