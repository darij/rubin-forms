'use strict';

exports.__esModule = true;
exports.shallowRender = exports.renderToString = exports.render = undefined;

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

exports['default'] = renderToString;

var _util = require('./util');

var SHALLOW = { shallow: true };

// components without names, kept as a hash for later comparison to return consistent UnnamedComponentXX names.
var UNNAMED = [];

var EMPTY = {};

var VOID_ELEMENTS = ['area', 'base', 'br', 'col', 'embed', 'hr', 'img', 'input', 'link', 'meta', 'param', 'source', 'track', 'wbr'];

/** Render Preact JSX + Components to an HTML string.
 *	@name render
 *	@function
 *	@param {VNode} vnode	JSX VNode to render.
 *	@param {Object} [context={}]	Optionally pass an initial context object through the render path.
 *	@param {Object} [options={}]	Rendering options
 *	@param {Boolean} [options.shallow=false]	If `true`, renders nested Components as HTML elements (`<Foo a="b" />`).
 *	@param {Boolean} [options.xml=false]		If `true`, uses self-closing tags for elements without children.
 *	@param {Boolean} [options.pretty=false]		If `true`, adds whitespace for readability
 */
renderToString.render = renderToString;

/** Only render elements, leaving Components inline as `<ComponentName ... />`.
 *	This method is just a convenience alias for `render(vnode, context, { shallow:true })`
 *	@name shallow
 *	@function
 *	@param {VNode} vnode	JSX VNode to render.
 *	@param {Object} [context={}]	Optionally pass an initial context object through the render path.
 */
var shallowRender = function shallowRender(vnode, context) {
	return renderToString(vnode, context, SHALLOW);
};

/** The default export is an alias of `render()`. */
function renderToString(vnode, context, opts, inner, isSvgMode) {
	var _ref = vnode || EMPTY,
	    nodeName = _ref.nodeName,
	    attributes = _ref.attributes,
	    children = _ref.children,
	    isComponent = false;

	context = context || {};
	opts = opts || {};

	var pretty = opts.pretty,
	    indentChar = typeof pretty === 'string' ? pretty : '\t';

	if (vnode == null || typeof vnode === 'boolean') {
		return '';
	}

	// #text nodes
	if ((typeof vnode === 'undefined' ? 'undefined' : _typeof(vnode)) !== 'object' && !nodeName) {
		return (0, _util.encodeEntities)(vnode);
	}

	// components
	if (typeof nodeName === 'function') {
		isComponent = true;
		if (opts.shallow && (inner || opts.renderRootComponent === false)) {
			nodeName = getComponentName(nodeName);
		} else {
			var props = (0, _util.getNodeProps)(vnode),
			    rendered = void 0;

			if (!nodeName.prototype || typeof nodeName.prototype.render !== 'function') {
				// stateless functional components
				rendered = nodeName(props, context);
			} else {
				// class-based components
				var c = new nodeName(props, context);
				// turn off stateful re-rendering:
				c._disable = c.__x = true;
				c.props = props;
				c.context = context;
				if (c.componentWillMount) c.componentWillMount();
				rendered = c.render(c.props, c.state, c.context);

				if (c.getChildContext) {
					context = (0, _util.assign)((0, _util.assign)({}, context), c.getChildContext());
				}
			}

			return renderToString(rendered, context, opts, opts.shallowHighOrder !== false);
		}
	}

	// render JSX to HTML
	var s = '',
	    html = void 0;

	if (attributes) {
		var attrs = (0, _util.objectKeys)(attributes);

		// allow sorting lexicographically for more determinism (useful for tests, such as via preact-jsx-chai)
		if (opts && opts.sortAttributes === true) attrs.sort();

		for (var i = 0; i < attrs.length; i++) {
			var name = attrs[i],
			    v = attributes[name];
			if (name === 'children') continue;
			if (!(opts && opts.allAttributes) && (name === 'key' || name === 'ref')) continue;

			if (name === 'className') {
				if (attributes['class']) continue;
				name = 'class';
			} else if (isSvgMode && name.match(/^xlink\:?(.+)/)) {
				name = name.toLowerCase().replace(/^xlink\:?(.+)/, 'xlink:$1');
			}

			if (name === 'class' && v && (typeof v === 'undefined' ? 'undefined' : _typeof(v)) === 'object') {
				v = (0, _util.hashToClassName)(v);
			} else if (name === 'style' && v && (typeof v === 'undefined' ? 'undefined' : _typeof(v)) === 'object') {
				v = (0, _util.styleObjToCss)(v);
			}

			var hooked = opts.attributeHook && opts.attributeHook(name, v, context, opts, isComponent);
			if (hooked || hooked === '') {
				s += hooked;
				continue;
			}

			if (name === 'dangerouslySetInnerHTML') {
				html = v && v.__html;
			} else if ((v || v === 0 || v === '') && typeof v !== 'function') {
				if (v === true || v === '') {
					v = name;
					// in non-xml mode, allow boolean attributes
					if (!opts || !opts.xml) {
						s += ' ' + name;
						continue;
					}
				}
				s += ' ' + name + '="' + (0, _util.encodeEntities)(v) + '"';
			}
		}
	}

	// account for >1 multiline attribute
	var sub = s.replace(/^\n\s*/, ' ');
	if (sub !== s && !~sub.indexOf('\n')) s = sub;else if (pretty && ~s.indexOf('\n')) s += '\n';

	s = '<' + nodeName + s + '>';

	if (VOID_ELEMENTS.indexOf(nodeName) > -1) {
		s = s.replace(/>$/, ' />');
	}

	if (html) {
		// if multiline, indent.
		if (pretty && (0, _util.isLargeString)(html)) {
			html = '\n' + indentChar + (0, _util.indent)(html, indentChar);
		}
		s += html;
	} else {
		var len = children && children.length,
		    pieces = [],
		    hasLarge = ~s.indexOf('\n');
		for (var _i = 0; _i < len; _i++) {
			var child = children[_i];
			if (!(0, _util.falsey)(child)) {
				var childSvgMode = nodeName === 'svg' ? true : nodeName === 'foreignObject' ? false : isSvgMode,
				    ret = renderToString(child, context, opts, true, childSvgMode);
				if (!hasLarge && pretty && (0, _util.isLargeString)(ret)) hasLarge = true;
				if (ret) pieces.push(ret);
			}
		}
		if (pretty && hasLarge) {
			for (var _i2 = pieces.length; _i2--;) {
				pieces[_i2] = '\n' + indentChar + (0, _util.indent)(pieces[_i2], indentChar);
			}
		}
		if (pieces.length) {
			s += pieces.join('');
		} else if (opts && opts.xml) {
			return s.substring(0, s.length - 1) + ' />';
		}
	}

	if (VOID_ELEMENTS.indexOf(nodeName) === -1) {
		if (pretty && ~s.indexOf('\n')) s += '\n';
		s += '</' + nodeName + '>';
	}

	return s;
}

function getComponentName(component) {
	return component.displayName || component !== Function && component.name || getFallbackComponentName(component);
}

function getFallbackComponentName(component) {
	var str = Function.prototype.toString.call(component),
	    name = (str.match(/^\s*function\s+([^\( ]+)/) || EMPTY)[1];
	if (!name) {
		// search for an existing indexed name for the given component:
		var index = -1;
		for (var i = UNNAMED.length; i--;) {
			if (UNNAMED[i] === component) {
				index = i;
				break;
			}
		}
		// not found, create a new indexed name:
		if (index < 0) {
			index = UNNAMED.push(component) - 1;
		}
		name = 'UnnamedComponent' + index;
	}
	return name;
}
renderToString.shallowRender = shallowRender;

exports.render = renderToString;
exports.renderToString = renderToString;
exports.shallowRender = shallowRender;