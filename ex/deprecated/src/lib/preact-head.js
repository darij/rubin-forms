'use strict';

exports.__esModule = true;
exports['default'] = undefined;

var _preact = require('lib/preact');

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; } /* @jsx h */


var DOMAttributeNames = {
  acceptCharset: 'accept-charset',
  className: 'class',
  htmlFor: 'for',
  httpEquiv: 'http-equiv'
};

var browser = typeof window !== 'undefined';
var mounted = [];

function reducer(components) {
  return components.map(function (c) {
    return c.children;
  }).filter(function (c) {
    return !!c;
  }).reduce(function (a, b) {
    return a.concat(b);
  }, []).reverse().filter(unique()).reverse().map(function (c) {
    var className = (c.className ? c.className + ' ' : '') + 'preact-head';
    return (0, _preact.cloneElement)(c, { className: className });
  });
}

function updateClient(head) {
  var tags = {};
  head.forEach(function (h) {
    var components = tags[h.nodeName] || [];
    components.push(h);
    tags[h.nodeName] = components;
  });

  updateTitle(tags.title ? tags.title[0] : null);

  var types = ['meta', 'base', 'link', 'style', 'script'];
  types.forEach(function (type) {
    updateElements(type, tags[type] || []);
  });
}

function updateElements(type, components) {
  var headEl = document.getElementsByTagName('head')[0];
  var oldTags = Array.prototype.slice.call(headEl.querySelectorAll(type + '.preact-head'));
  var newTags = components.map(domify).filter(function (newTag) {
    for (var i = 0, len = oldTags.length; i < len; i++) {
      var oldTag = oldTags[i];
      if (oldTag.isEqualNode(newTag)) {
        oldTags.splice(i, 1);
        return false;
      }
    }
    return true;
  });

  oldTags.forEach(function (t) {
    return t.parentNode.removeChild(t);
  });
  newTags.forEach(function (t) {
    return headEl.appendChild(t);
  });
}

function domify(component) {
  var el = document.createElement(component.nodeName);
  var attrs = component.attributes || {};
  var children = component.children;

  for (var p in attrs) {
    if (!attrs.hasOwnProperty(p)) continue;
    if (p === 'dangerouslySetInnerHTML') continue;

    var attr = DOMAttributeNames[p] || p.toLowerCase();
    el.setAttribute(attr, attrs[p]);
  }

  if (attrs['dangerouslySetInnerHTML']) {
    el.innerHTML = attrs['dangerouslySetInnerHTML'].__html || '';
  } else if (children) {
    el.textContent = typeof children === 'string' ? children : children.join('');
  }

  return el;
}

var METATYPES = ['name', 'httpEquiv', 'charSet', 'itemProp'];

// returns a function for filtering head child elements
// which shouldn't be duplicated, like <title/>.
function unique() {
  var tags = [];
  var metaTypes = [];
  var metaCategories = {};
  return function (h) {
    switch (h.nodeName) {
      case 'title':
      case 'base':
        if (~tags.indexOf(h.nodeName)) return false;
        tags.push(h.nodeName);
        break;
      case 'meta':
        for (var i = 0, len = METATYPES.length; i < len; i++) {
          var metatype = METATYPES[i];
          if (!h.attributes.hasOwnProperty(metatype)) continue;
          if (metatype === 'charSet') {
            if (~metaTypes.indexOf(metatype)) return false;
            metaTypes.push(metatype);
          } else {
            var category = h.attributes[metatype];
            var categories = metaCategories[metatype] || [];
            if (~categories.indexOf(category)) return false;
            categories.push(category);
            metaCategories[metatype] = categories;
          }
        }
        break;
    }
    return true;
  };
}

function updateTitle(component) {
  var title = void 0;
  if (component) {
    var children = component.children;

    title = typeof children === 'string' ? children : children.join('');
  } else {
    title = '';
  }
  if (title !== document.title) {
    document.title = title;
  }
}

function update() {
  var state = reducer(mounted.map(function (mount) {
    return mount.props;
  }));
  if (browser) updateClient(state);
}

var Head = function (_Component) {
  _inherits(Head, _Component);

  function Head() {
    _classCallCheck(this, Head);

    return _possibleConstructorReturn(this, _Component.apply(this, arguments));
  }

  Head.rewind = function rewind() {
    var state = reducer(mounted.map(function (mount) {
      return mount.props;
    }));
    mounted = [];
    return state;
  };

  Head.prototype.componentDidUpdate = function componentDidUpdate() {
    update();
  };

  Head.prototype.componentWillMount = function componentWillMount() {
    mounted.push(this);
    update();
  };

  Head.prototype.componentWillUnmount = function componentWillUnmount() {
    var i = mounted.indexOf(this);
    if (~i) mounted.splice(i, 1);
    update();
  };

  Head.prototype.render = function render() {
    return null;
  };

  return Head;
}(_preact.Component);

exports['default'] = Head;