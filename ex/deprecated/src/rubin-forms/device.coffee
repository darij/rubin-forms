###
  * company: Emerald labs
  * license: MIT
  * author:  darthyar
  *
  * Информация об устройстве на котором выполняется сайт
  *
  * Тип:
  * Экстрамалые девайсы («портретные телефоны», < 576px) - меню закрыты, открываются, сдвигая контент
  * Малые девайсы («ландшафтные телефоны» >= 576px) - меню закрыты, открываются, сдвигая контент
  * Средние девайсы («таблетки» >= 768px) - одно из боковых меню открыто, при открытии другое сворачивается
  * Большие девайсы (десктопы >= 992px) - оба меню открыты
  * Экстрабольшие девайсы (большие десктопы >= 1200px) - оба меню открыты + отступы по бокам
  * 1440px
  
  
  small: 0px
  medium: 640px
  large: 1024px
  xlarge: 1200px
  xxlarge: 1440px
  
   breakpoints={{lg: 1200, md: 996, sm: 768, xs: 480, xxs: 0}}
   cols={{lg: 12, md: 10, sm: 6, xs: 4, xxs: 2}}
  
###

import { h, Component } from 'Reactive'

export device = null

breakpoints =
	xxs: 576
	xs:  768
	sm:  992
	md:  1200
	lg:  1440
	xl:  Infinity

type2name =
	xxs: "portrait"
	xs:  "landscape"
	sm:  "tablet"
	md:  "desktop"
	lg:  "tv"
	xl:  "cinema"

name2type = {}
for _i of type2name then name2type[type2name[_i]] = _i

types = Object.keys type2name
names = Object.values type2name
	
export recalc = ->
	# размер виевпорта - по умолчанию - s-ка. Т.к. ландшафтных телефонов сейчас больше всего
	width = window?.innerWidth || 900

	min = {}
	type = null
	for t, break_width of breakpoints 
		min[t] = 1
		if width < break_width
			type = t
			break

	name = type2name[type]

	to_max = for i in types
		if i == type
			break
		i
	
	device =
		width: width
		type: type
		name: name
		[name]: 1
		[type]: 1
		to_max: to_max
		to_max_reverse: to_max.reverse()
		min: min

		
# вычисляет значение в зависимости от девайса
export fluid = (x, def)->
	if not x? then def
	else if typeof x == "object"
		for i in device.to_max_reverse
			if (k=x[i])? then return k
			if (k=x[type2name[i]])? then return k
		def
	else x


# <Device>
#   <Tablet>...</Tablet>
#   <Tv>...</Tv>
#   ...
# </Device>
# 
# для Tv и Cinema будет применять <Tv />
# а для Tablet и Desctop будет применять <Tablet />
# 
export default Device = ->

	
# этот и больше
export Portrait = ({children})-> if device.portrait then <div>{children}</div> else []
export Landscape = ({children})-> if device.landscape then <div>{children}</div> else []
export Tablet = ({children})-> if device.tablet then <div>{children}</div> else []
export Desktop = ({children})-> if device.desktop then <div>{children}</div> else []
export Tv = ({children})-> if device.tv then <div>{children}</div> else []
export Cinema = ({children})-> if device.cinema then <div>{children}</div> else []

# только
export PortraitOnly = ({children})-> if device.portrait then <div>{children}</div> else []
export LandscapeOnly = ({children})-> if device.landscape then <div>{children}</div> else []
export TabletOnly = ({children})-> if device.tablet then <div>{children}</div> else []
export DesktopOnly = ({children})-> if device.desktop then <div>{children}</div> else []
export TvOnly = ({children})-> if device.tv then <div>{children}</div> else []
export CinemaOnly = ({children})-> if device.cinema then <div>{children}</div> else []




