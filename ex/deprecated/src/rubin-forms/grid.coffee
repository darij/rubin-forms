###
  * company: Emerald labs
  * license: MIT
  * author:  darthyar
  *
  * сетка
  * 
  * указывает размеры ячеек в относительных единицах 1/8, 2/8 и т.д.
  * либо в целых. Тогда подразумевается основа сетки 12
  * 
  * ячейка - div. Внутри - ещё div c содержимым, gap установлен padding-ами
  *
###

import { h, Component } from 'Reactive'
import { StyleSheet, css } from 'lib/aphrodite'


# сетка. Строки определяются автоматически
export Grid = ({ cols, gap, vgap, hgap, children })->
	@context.cols = cols ? 12
	gap ?= 4
	@context.vgap = vgap ? gap
	@context.hgap = hgap ? gap
	
	set_end children
	
	<div class={css style.grid}>
		{children}
	</div>

# ячейка сетки
export Cell = ({ cols, children, end })->
	total_cols = @context.cols
	cols ?= 1
	
	set_end children
	
	dim =
		width: (cols/total_cols*100)+"%"

	dim.paddingRight = @context.hgap if not end
	
	<div class={style.cell} style={dim}>
		{children}
	</div>

# проставляет конечной ячейке атрибут end
set_end = (children)->
	if children instanceof Array and n = children.length
		vnode = children[n-1]
		if vnode.nodeName == Cell
			vnode.attributes.end = 1
	return

	

style = StyleSheet.create
	grid:
		overflow: "auto"
	cell:
		float: "left"
		height: "100%"
	content:
		width: "100%"
		height: "100%"
	