###
  * company: Emerald Labs
  * license: MIT
  * author:  darthyar
  *
  * создаёт типографию
  
 * 
 *  калькулятор ритмов: / https://drewish.com/tools/vertical-rhythm/ 
 *  
 *  выбираем ритм и вставляем их в
 *
 *  модульную сеть: / http://www.modularscale.com
 *  http://webtypography.net/2.2.2
 *  https://drewish.com/tools/vertical-rhythm/
 *  ещё генератор: https://www.gridlover.net/try
 
###

import { StyleSheet, css } from 'lib/aphrodite'

export default class Typography

	# набор констант для ratio
	@ratios:
		"15:16 – minor second": 1.067
		"8:9 – major second": 1.125
		"5:6 – minor third": 1.2
		"4:5 – major third": 1.25
		"3:4 – perfect fourth": 1.333
		"1:√2 – aug. fourth / dim. fifth": 1.414
		"2:3 – perfect fifth": 1.5
		"5:8 – minor sixth": 1.6
		"1:1.618 – golden section": 1.618
		"3:5 – major sixth": 1.667
		"9:16 – minor seventh": 1.778
		"8:15 – major seventh": 1.875
		"1:2 – octave": 2
		"2:5 – major tenth": 2.5
		"3:8 – major eleventh": 2.667
		"1:3 – major twelfth": 3
		"1:4 – double octave": 4
		
	@minor_second = 1.067
	@major_second = 1.125
	@minor_third = 1.2
	@major_third = 1.25
	@perfect_fourth = 1.333
	@eiler = Math.E # 2.71828182845905 - число Эйлера-Непра
	@pi = Math.PI # 3.141592653589793
	@aug = 1.414
	@perfect_fifth = 1.5
	@minor_sixth = 1.6
	@golden_section = 1.618
	@major_sixth = 1.667
	@minor_seventh = 1.778
	@major_seventh = 1.875
	@octave = 2
	@major_tenth = 2.5
	@major_eleventh = 2.667
	@major_twelfth = 3
	@double_octave = 4

	# base - базовый размер шрифта
	# ratio (scale factor) - коеффициент изменения размера
	# linehight - базовая высота линии
	constructor: (@base, @ratio, @linehight)->
		@base ?= 12
		@ratio ?= @constructor.aug_fourth_dim_fifth
		# обычно от 1.2 до 1.5
		@linehight ?= 1.4 * @base
		
		
	# модульная шкала (modular scale)
	# шаг - ступенька шкалы, может быть отрицательным
	# возвращает размер шрифта
	ms: (step)-> (@ratio ** step) * @base
	
	# вертикальный ритм (vertical rhythm)
	# для указанного шага шкалы формирует все необходимые свойства
	vr: (step)->
		scale = @ms step
		return
			fontSize: scale + "em"
			lineHight: @linehight
			marginTop: 0
			marginBottom: 0 
	
#style = StyleSheet.create
	 