###
  * company: Emerald labs
  * license: MIT
  * author:  darthyar
  *
  * должна наследоваться
  *
  * форма - фетчем обращается к серверу
  *  GET - данные, POST - валидация и обновление
  * 
  * отправляет запрос GET и получает данные при первом формировании (из конструктора)
  *
  * имеет переменные:
  * 
###

import { h, Component } from 'Reactive'

# ошибки формы
export Errors = ({errors})->
	

export default class Form extends Component
	
	render: ({action})->
		@fetch()
		
		<form action={action} enctype="multipart/form-data" onSubmit={@onSubmit}>
			{@form(@props, @state)}
		</form>
	

	# получение данных формы
	fetch: ->
		xhr = fetch @props.action
			method: "GET"
			credentials: "include"
			redirect: "follow"
			headers:
				Accept: "application/json"
			
		xhr.then (response)=>
			if response.status == 200
				@setState response.json()
			else
				@setState
					errors: ["ошибка сервера"]
		
		xhr.catch (error)=>
			@setState
					errors: ["ошибка соединения"]
					
		return this
					
	# отправка данных формы
	submit: ->
	
		xhr = fetch @props.action
			method: "POST"
			headers:
				Accept: "application/json"
				# Content-Type указывается автоматически!
			credentials: "include"
			redirect: "follow"
			body: new FormData(@base)
			
		xhr.then (response)=>
			if response.status == 200
				@setState response.json()
			else
				@setState
					errors: ["ошибка сервера"]
		
		xhr.catch (error)=>
			@setState
				errors: ["ошибка соединения"]
				
		return this
	
	@finalEvents
		onSubmit: (e)->
			e.preventDefault()
			if false != @props.onSubmit? this then @submit()			
			return
