###
  * company: Emerald labs
  * license: MIT
  * author:  darthyar
  *
  * лайоут
  * может содержать подкомпоненты: <Menu />, <LeftMenu />, <RightMenu> и <Footer>
  * имеет свойство background, которое указывает на фоновую картинку
  * <LeftMenu/> так же добавляет в меню слева кнопку для его открытия
  * <RightMenu/> так же добавляет в меню справа кнопку для его открытия
  * 
  * лайоут подстраивается под ширину экрана:
  *
  * Экстрамалые девайсы («портретные телефоны», < 576px) - меню закрыты, открываются, сдвигая контент
  * Малые девайсы («ландшафтные телефоны» >= 576px) - меню закрыты, открываются, сдвигая контент
  * Средние девайсы («таблетки» >= 768px) - одно из боковых меню открыто, при открытии другое сворачивается
  * Большие девайсы (десктопы >= 992px) - оба меню открыты
  * Экстрабольшие девайсы (большие десктопы >= 1200px) - оба меню открыты + отступы по бокам
  *
  * типы лайоута:
  *   absolute - всегда занимает всё пространство, меню открываются по требованию
  *   planshet - занимает максимум пространства, но с открытыми меню
  *   site - меню открывает, на больших девайсах добавляет поля
  * 
  * oval - атрибут указывает, что меню и футер будут под размер контента
###

import { h, Component } from 'Reactive'
import { StyleSheet, css, minify } from 'lib/aphrodite'

minify false

export Menu = ->
export LeftMenu = ->
export Context = ->
export RightMenu = ->
export Footer = ->

export default class Layout extends Component

	componentDidMount: ->
		
		return

	render: ({children, oval, width })->
		empty = {children: [], attributes: {}}
		menu = left = right = footer = context = empty
		dop = []
		
		# tag is VNode
		for tag in children
			if tag.nodeName == Menu then menu = tag
			else if tag.nodeName == LeftMenu then left = tag
			else if tag.nodeName == RightMenu then right = tag
			else if tag.nodeName == Footer then footer = tag
			else if tag.nodeName == Context then context = tag
			else dop.push tag
		
		leftWidth = 0
		if left != empty
			attr = left.attributes || {}
			leftWidth = attr.width || "15%"
			delete attr.width
			
		rightWidth = 0
		if right != empty
			attr = right.attributes || {}
			rightWidth = attr.width || "15%"
			delete attr.width
	
		attr = {}
		if width then attr.style = 
			width: width
			margin: "0 auto"
	
		<table class={css(style.layout)} {attr...}>
		
			<col width={leftWidth} />
			<col />
			<col width={rightWidth} />
			
			<tr height="0">
				{if oval then <td /> else []}
				<td colspan={if oval then 1 else 3} class={css(style.menu)}>{menu.children}</td>
				{if oval then <td /> else []}
			</tr>
			
			<tr height="100%">
				<td class={css(style.left_menu)} {left.attributes...}>{left.children}</td>
				<td class={css(style.context)} {context.attributes...}>{context.children}{dop}</td>
				<td class={css(style.right_menu)} {right.attributes...}>{right.children}</td>
			</tr>
			
			<tr height="0">
				{if oval then <td /> else []}
				<td colspan={if oval then 1 else 3} class={css(style.footer)}>{footer.children}</td>
				{if oval then <td /> else []}
			</tr>
		</table>

	
	
	
style = StyleSheet.create
	layout:
		minHeight: "100%"
	menu:
		backgroundColor: "hsl(230,10%,50%)"
	left_menu:
		backgroundColor: "hsl(230,25%,60%)"
	right_menu:
		backgroundColor: "hsl(230,25%,60%)"
	context:
		backgroundColor: "hsl(230,20%,50%)"
	footer:
		backgroundColor: "hsl(230,15%,55%)"
