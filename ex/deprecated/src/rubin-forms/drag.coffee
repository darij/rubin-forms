###
  * company: Emerald labs
  * license: MIT
  * author:  darthyar
  *
  * перетаскиваемый компонент
###

import { h, Component } from 'Reactive'

export default class Drag extends Component

	constructor: (props)->
		super arguments...
		@cherryPick "key", "onEffect", "onStart", "onEnd", "onDrop"
		@onEffect ?= => @base.style.display = 'none'
		
		
	# вызывается из дропзоны
	drop: (e)->
		@onDrop? e
		
	@finalEvents
		dragStart: (e)->
			# установим картинку
			e.dataTransfer.setDragImage @base, e.offsetX, e.offsetY
			# скрываем элемент
			setTimeout @onEffect, 1
			# сообщаем, что начато перетаскивание
			window._DnD = this
			# выполняем обработчик
			@onStart? e
			
		# выполняется после drop-а
		dragEnd: (e)->
			e.dnd = window._DnD
			window._DnD = null
			@onEnd? e

	render: ({children})->

		<div {@attrs...}
			draggable="true"
			onDragStart={@dragStart}
			onDragEnd={@dragEnd}
		>
			{children}
		</div>
