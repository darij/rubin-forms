###
  * company: Emerald labs
  * license: MIT
  * author:  darthyar
  *
  * поле ввода формы
###

import { h, Component } from 'Reactive'

export default class Input extends Component

	render: ({name, value})->
		form = @context.form
		if form and name
			if value == undefined
				@props.value = form.data[name]
			else
				form.data[name] = value
		<input {@props...} />
