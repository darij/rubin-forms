###
  * company: Emerald labs
  * license: MIT
  * author:  darthyar
  *
  * дропзона
###

import { h, Component } from 'Reactive'

export default class Drop extends Component

	constructor: (props)->
		super arguments...
		@cherryPick "key", "dropEffect", "onDrop", "onMove"

	@finalEvents
		dragOver: (e)->
			dnd = window._DnD
			if dnd?.key == @key
				e.dataTransfer.dropEffect = @dropEffect || 'move'
				e.preventDefault()
				e.dnd = dnd
				@onMove? e
			return
		
		# передаём на элемент
		drop: (e)->
			e.dnd = window._DnD
			@onDrop? e
			e.dropzone = this
			e.dnd.drop? e
			return
		
	render: ({children})->
		<div {@attrs...}
			onDragOver={@dragOver}
			onDrop={@drop}
		>
			{children}
		</div>
