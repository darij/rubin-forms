function Io(url) {

	this.url = url || "ws://"+location.hostname+":3001/__autoreload"
	this.emitter = {}
	this.timeout = {
		reconnect: {
			min: 100,
			max: 10000,
			step: 100
		}
	}
	
	this.connect()
}

Io.prototype.emit = function send() {
	var s = JSON.stringify([].slice.apply(arguments))
	console.log("����������:", s)
	socket.send(s)
}

Io.prototype.on = function on(name, fn) {
	this.emitter[name] = fn
}

Io.prototype.gosub = function gosub() {
	var args = Array.prototype.slice.apply(arguments)
	var name = args.shift()
	var emitter = this.emitter[name]
	emitter.apply(this, args)
}

Io.prototype.connect = function connect() {
	var socket = this.socket = new WebSocket(url)
	
	socket.onopen = function() {
		console.log("���������� �����������.")
		this.gosub("connect")
	}

	socket.onclose = function(event) {
		if (event.wasClean) {
			console.log('���������� ������� �����')
		} else {
			console.log('����� ����������') // ��������, "����" ������� �������
			this.reconnect()
		}
		console.log('���: ' + event.code + ' �������: ' + event.reason)
		this.gosub("disconnect", event)
	}

	socket.onmessage = function(event) {
		console.log("�������� ������ " + event.data, event)
		var ar = JSON.parse(event.data)
		this.gosub.apply(this, ar)
	}

	socket.onerror = function(error) {
		console.log("������ " + error.message, error)
		this.gosub("error", error.message, error)
	}
}

Io.prototype.reconnect = function reconnect() {
	setTimeout(this.reconnect.bind(this), this.timeout.reconnect.min)
	
	this.connect()
	 = this.timeout.reconnect.min
}


exports.Io = Io
expors['default'] = Io