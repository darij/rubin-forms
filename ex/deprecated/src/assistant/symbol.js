// символ

function $Symbol(key) {
	this.key = key === undefined? "": String(key)
}
$Symbol.prototype.toString = function() {
	return "Symbol("+this.key+")"
}

function Symbol(key) {
	if (this instanceof $Symbol) throw TypeError('Symbol is not a constructor!');
	return new $Symbol(key)
}

exports["default"] = Symbol

var symbols = {};
Symbol['for'] = function(key) {
	return symbols[key] || (symbols[key] = new $Symbol(key))
}

Symbol['keyFor'] = function(sym) {
	if (!isSymbol(sym)) throw new TypeError(sym + ' is not a symbol!');
	return symbols[sym.key]? sym.key: undefined
}



Symbol.iterator = "@@iterator"
Symbol.asyncIterator = "@@asyncIterator"
Symbol.toStringTag = "@@toStringTag"

String.prototype[Symbol.iterator] = function stringIterator() {
	var i = 0,
		n = this.length,
		s = this
	return {
		next: function next() {
			return i<n? {value: s.substr(i++, 1)}: {done: true}
		}
	}
}
