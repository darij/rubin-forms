class Cat {
	static alice = 20;
	minnty = 50;
	
	
	meow() {
		return {
			alice: Cat.alice,
			minntty: this.minnty,
			r: this.r,
		}
	}
}

class A extends Cat {
	static julia = 10;
	fn = a => this.r;
	
	constructor(r) {
		super(r)
		this.r = r
	}
}

a = new A(10);

console.log(a.meow())