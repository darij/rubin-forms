function* g1() {
  yield 2;
  yield 3;
  yield 4;
}

function* g2() {
  yield 1;
  yield* g1();
  yield 5;
}

let generator = g1();

for(let value of generator) {
  console.log(value); // 1..5
}


function* generateSequence(start, end) {

  for (let i = start; i <= end; i++) {
    yield i;
  }

}

//let sequence = [...generateSequence(2,5)]; - переладить [].concat
for(let value of generateSequence(2,5))
	console.log(value)
