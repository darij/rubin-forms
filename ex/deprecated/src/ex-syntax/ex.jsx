/**
  * company: Emerald labs
  * license: MIT
  * author: darthyar
  *
  * компонент реакт
  */

import { h, Component } from 'lib/preact'
import Head from 'lib/preact-head'




export default class ex extends Component {
	/**
	  * конструктор
	  */
	constructor (props) {
		super(props)
		this.state = {
			text: "hi!",
		}
	}

	/**
	 * рендерит отображение
	 */
	render (props, state) {
		return <div class="x1" onClick={::this.click}>
			<Head>
				<title>Мама-мия!</title>
			</Head>
			Привет {state.text}!
			<input value={state.text} onInput={::this.updateText} />
		</div>
	}

	updateText(e) {
		console.log(e, e.target.value)
        this.setState({ text: e.target.value })
    }
	
	click() {
		this.setState({text: "click!"})
	}
	
}
