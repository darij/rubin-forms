package X;
# тестовое приложение X

use common::sense;
use R::App;
use R::Yogi;

get "/" => sub {
	'<center>hi!<hr>X<hr>
		<a href=/ex><img src=/x.png></a>
	</center>
	<hr>
	<ul>
		<li><a href="/ex">ex-syntax/ex</a></li>
		<li><a href="/~devel/">~devel</a></li>
	</ul>
	'
};

get "/*path.:ext" => sub {
	root "media"
};

get "/ex" => sub {
	render "ex-syntax/ex", {text => "hi!"}
};

my $elements_file = $app->project->file("etc/elements.yml");
my $elements = $elements_file->exists? $app->yaml->from($elements_file->read): {};

get "/~devel/" => sub {
	render "devel/elementsEd.jsx", {
		elements => $elements
	}
};

put "/~devel/element/:id" => sub {
	$elements->{attr->id} = $app->json->from(request->body);
	$elements->write($app->yaml->to($elements));
	"Ok"
};

del "/~devel/element/:id" => sub {
	delete $elements->{attr->id};
	$elements->write($app->yaml->to($elements));
	"Ok"
};


1;