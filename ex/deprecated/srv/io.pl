package io;
# тестовое приложение для разработки вебсокетов

use common::sense;
use R::App;
use R::Yogi;


get "/" => sub {
	return << 'END'
	
	<input id=send>
	<input type=button value="send" onclick="send( 'echo', document.getElementById('send').value )">
	<div id=err></div>
	
	<script>
		var socket = new WebSocket("ws://"+location.hostname+":3001/io");
		socket.onopen = function() {
		  console.log("Соединение установлено.");
		  send('echo', 123);
		};

		socket.onclose = function(event) {
		  if (event.wasClean) {
			console.log('Соединение закрыто чисто');
		  } else {
			console.log('Обрыв соединения'); // например, "убит" процесс сервера
		  }
		  console.log('Код: ' + event.code + ' причина: ' + event.reason);
		};

		socket.onmessage = function(event) {
		  console.log("Получены данные " + event.data, event);
		};

		socket.onerror = function(error) {
		  console.log("Ошибка " + error.message, error);
		};
		
		function send() {
			var s = JSON.stringify([].slice.apply(arguments))
			console.log("отправлено:", s)
			socket.send(s)
		}
		
	</script>
END
};

websocket "/io" => sub {
	msg1 "connect", request;
	
	on disconnect => sub {
		msg1 "disconnect", request;
	};	
};

1;