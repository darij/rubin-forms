= Arey

== Инициализация

Подключаем наш фреймворк:

[init]
	
	use lib "lib";
	use common::sense;
	use R::App;
	
	my $PRJ_CLEARER = $app->man->project("14-arey", log=>0, logsqlinfo=>0);
	
[test]


== Введение

Arey, это Mustache-подобный шаблонизатор.
Он создан как для серверной, так и для клиентской части.

== Использование

=== Вставка c экранированием и без

@@14-arey/parts/ex-insert.html
	{{a}} {{&b}}
[test]

	$app->templateArey->render("parts/ex-insert.html", {a=>"<", b=>">"})	# &lt; >
	
=== Условие
	
@@14-arey/parts/ex-if.html
	{{?a}}123{{/a}}/
[test]

	$app->templateArey->render("parts/ex-if.html", {a=>1})	# 123/
	$app->templateArey->render("parts/ex-if.html", {a=>0})	# /
	
=== Цикл

@@14-arey/parts/ex-repeat.html
	{{*a}}-{{.}},{{/a}}/
[test]

	$app->templateArey->render("parts/ex-repeat.html", {a=>[1, 2, 3]})	# -1,-2,-3,/
	
=== Инклуд

@@14-arey/parts/ex-part.html
	-{{a}}-
@@14-arey/parts/ex-include.html
	/{{>parts/ex-part.html}}/
[test]

	$app->templateArey->render("parts/ex-include.html", {a=>6})	# /-6-/

=== Комментарий

@@14-arey/parts/ex-comment.html
	/{{! comment! }}/
[test]
	
	$app->templateArey->render("parts/ex-comment.html")	# //
	
=== Отображение виджета

	