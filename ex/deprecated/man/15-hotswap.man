= R::Hotswap

== Инициализация

Подключаем наш фреймворк:

[init]
	
	use lib "lib";
	use common::sense;
	use R::App;
	
	$app->ini->{service}{connect}{basename} = "rubin-forms-test";
	my $PRJ_CLEARER = $app->man->project("14-hotswap", log=>0, logsqlinfo=>0);

	
[test]


== Введение

Горячая замена перезагружает отдельные модули, когда они изменяются.

Используется для быстрой разработки. 

Недостаток этого подхода в том, что из-за глобальных переменных .

== Горячая замена модулей

@@14-hotswap/lib/Ex.pm
	package Ex;
	sub new { my $cls = shift; bless {@_}, $cls }
	BEGIN { our $NI = 123; }
	sub hello { $NI }
	1;
	
@@14-hotswap/lib/Ex/Sub.pm
	package Ex::Sub;
	sub new { my $cls = shift; bless {@_}, $cls }
	sub bazuka { 666 }
	1;
[test]

	require "Ex/Sub.pm";
	eval "use Ex";
	die if $@;

	
	Ex->hello 		# 123
	Ex::Sub->bazuka	# 666
	
	$app->file("lib/Ex.pm")->replace(sub { s/123/456/ });
	
	$app->hotswap(pkg=>"Ex")->remove;
	#$app->hotswap(pkg=>"Ex")->show;
	require "Ex.pm";
	
	Ex->hello 		# 456
	Ex::Sub->bazuka	# 666

== Горячая замена сервисов

# @@14-hotswap/html/ex.xf
	# 123
# [test]

	# $app->xf->render("html/ex.xf");	#~ 123
	
	# $app->hotswap(pkg => "R::Xf")->reload;
	
	# $app->xf->render("html/ex.xf");	#~ 123
	
	# $app->hotswap(file => "R/Xf.pm")->reload;
	
	# $app->xf->render("html/ex.xf");	#~ 123

# Сервер сохраняет сокет и его воркеры во время перезагрузки.

# @@14-hotswap/html/hotswap.xf
	# <%
		# app.hotswap(pkg: "R::Portal").reload
		# app.hotswap(pkg: "R::Rsgi::Request").reload
		# app.hotswap(pkg: "R::Rsgi::Response").reload
		# app.hotswap(pkg: "R::Rsgi::Bag").reload
		# app.hotswap(pkg: "R::File").reload
		# app.hotswap(pkg: "R::Fulminant").reload
		# app.hotswap(pkg: "R::Url").reload
		# #app.hotswap(pkg: "R::Coro").reload
	# %>678
# [test]

	# my $port = 9001;
	# $app->ini->{service}{portal}{port} = $port;
	# $app->ini->{service}{portal}{access} = {};
	# my $portal = $app->portal->run;
	# my $psv = $portal->sv;
	
	# $app->url(":$port/ex/")->read		# 123
	
	# # $app->hotswap(pkg => "R::Portal")->reload;
	# # $app->hotswap(pkg => "R::Rsgi::Request")->reload;
	# # $app->hotswap(pkg => "R::Rsgi::Response")->reload;
	# # $app->hotswap(pkg => "R::Rsgi::Bag")->reload;
	# # $app->hotswap(pkg => "R::File")->reload;
	# # $app->hotswap(pkg => "R::Fulminant")->reload;
	# #$app->hotswap(pkg => "R::Url")->reload;
	# #$app->hotswap(pkg => "R::Coro")->reload;
	
	# $app->url(":$port/ex/")->read		# 123
	
	# $app->url(":$port/hotswap/")->read		# 678
	

	# undef $psv;
	
=== Горячая замена библиотек

	#*first = \&List::Util::first;
	
	my $old_app = $app;
	
	$app->hotswap(pkg => "R::App")->reload;
	
	$app			##== $R::App::app
	$old_app		##== $R::App::app
	
	exists $app->{hotswap}			# 1
	$app->can("hotswap")			## undef
	
	$app->hotswap(pkg => "R::Log")->reload;
	
	print STDERR join ": ", grep2 { $a =~ /Log\.pm/ } %INC;
	print STDERR "\n";
	
	ref $app->log					# R::Log
	$app->log->can("info")?1:0		# 1

== Горячая замена шаблонов

@@14-hotswap/view/layout.html
	-<% block show %>123<% end %>-
@@14-hotswap/view/ex.html
	<% INHERITS "layout.html" %>
	<% block show %>666<% end %>
[test]

	$app->view->load;
	$app->hotswap->scan;

	$app->view->render("ex.html");	#~ -666-
	
	!!$app->view->get_name("ex.html")->can("dispatch") # 1
	
	msg1 "view/ex.html", $app->file("view/ex.html")->mtime, $app->hotswap->{scan}{watch}{"view/ex.html"};
	
	$app->file("view/ex.html")->replace(sub { s/666/456/ });
	
	$app->hotswap->scan;
	
	!!$app->view->get_name("ex.html")->can("dispatch") # 1
	
	$app->view->render("ex.html");	#~ -456-
	
А что если перезагружаемый шаблон будет в фреймворке?
	
	$app->framework->file("view/ex-123.html")->write("-123-");
	
	!!$app->view->get_name("ex-123.html")->can("dispatch") # 1
	
	$app->hotswap->scan;
	
	!!$app->view->get_name("ex-123.html")->can("dispatch") # 1
	
	$app->view->render("ex-123.html");	# -123-
	
	msg1 "keys", grep { /ex.*html/ } keys %INC;
	msg1 "values", grep { /ex.*html/ } values %INC;
	
	$app->framework->file("view/ex-123.html")->write("-345-");
	
	$app->hotswap->scan;
	
	!!$app->view->get_name("ex-123.html")->can("dispatch") # 1
	
	$app->view->render("ex-123.html");	# -345-
	
	
	

# @@14-hotswap/parts/ex.html
	# 123
# [test]

	# $app->templateArey->render("parts/ex.html");	#~ 123
	
	# $app->file("parts/ex.html")->replace(sub { s/123/456/ });
	
	# $app->hotswap->scan;
	
	# $app->templateArey->render("parts/ex.html");	#~ 456

# @@14-hotswap/html/ex.xf
	# 123
# [test]

	# $app->xf->render("html/ex.xf");	#~ 123
	
	# $app->file("html/ex.xf")->replace(sub { s/123/456/ });
	
	# $app->hotswap->scan;
	
	# $app->xf->render("html/ex.xf");	#~ 456

== Горячая замена моделей

@@14-hotswap/model/Author.pm
	package R::Row::Author;
	# модель автор
	use base R::Model::Row;

	use common::sense;

	# вызывается для создания структуры базы
	sub setup {
		my ($fields) = @_;
		$fields->
		col(name => "varchar(255)")->remark("имя")->
		m2m(reviewers => "author" => "authors")->remark("критики")->
		# индексы
		unique("name" => "unq_author_name")->
		meta(remark => "авторы")->		
		end;
	}
	
	sub queryFunBook {
		my ($self) = @_;
		$self->books->order("id")->first
	}
	
	1;
@@14-hotswap/model/Book.pm
	package R::Row::Book;
	# модель книга
	use base R::Model::Row;

	use common::sense;

	# вызывается для создания структуры базы
	sub setup {
		my ($fields) = @_;
		$fields->
		col(name => "varchar(255)")->remark("название")->
		ref("author")->remark("автор")->
		m2m(coauthor => "author" => "cobooks")->remark("соавторы")->
		# индексы
		unique("name" => "unique_key_name")->
		meta(remark => "книги")->		
		end;
	}
	
	1;
[test]
	
	# $app->meta;
	
	# $app->hotswap->scan;
	
	# msg map { $_->name } $app->meta->fieldsets;
	
	$app->connect->logon;
	$app->meta->sync;
	
	app->make->gosub("models");
	app->make->gosub("show", "author");
	app->make->gosub("show", "book");
	
	my $bukin = $app->model->author(name => "Букин");
	my $snejanna = $app->model->author(name => "Снежанна");
	my $kuzmin = $app->model->author(name => "Кузьмин");
	
	$bukin->reviewers->add($snejanna, $kuzmin);
	
	$app->model->book(name => "Роман об овцах", author => $snejanna);
	$app->model->book(name => "Роман о козлах", author => $snejanna);
	
	$app->model->author->funBook->name		# Роман об овцах
	
@@14-hotswap/share/Author.pm
	package R::Row::Author;
	# модель автор
	use base R::Model::Row;

	use common::sense;

	# вызывается для создания структуры базы
	sub setup {
		my ($fields) = @_;
		$fields->
		col(name => "varchar(255)")->remark("имя")->
		col(sex => "tinyint")->default(0)->
		m2m(reviewers => "author" => "my_reviewers")->remark("критики")->
		# индексы
		unique("name" => "unq_author_name")->
		meta(
			remark => "авторы",
			viewing => "sex, name",
		)->		
		end;
	}
	
	sub annonce { my($self)=@_; $self->name . " " . $self->sex }
	
	sub queryFunBook {
		my ($self) = @_;
		$self->books->order("-id")->first
	}
	
	1;
[test]
	
	$app->file("model/Author.pm")->write($app->file("share/Author.pm")->read);
	
	$app->hotswap->scan;
	
	$app->connect->logon;
	
	app->make->gosub("models");
	app->make->gosub("show", "author");
	app->make->gosub("show", "book");
	
	$app->meta->sync;
	$app->connect->logon;
	
	$app->model->book->find(author => $snejanna)->author->first->annonce	# Снежанна 0
	
	$app->model->author->funBook->name		# Роман о козлах

