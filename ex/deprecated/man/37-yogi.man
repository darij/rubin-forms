= Yogi

[init]

	use common::sense;
	use lib "lib";
	use R::App qw/$app msg msg1/;
	use R::Yogi;
	
[test]
	
== Роутинг

`Yogi` импортирует методы для создания роутинга, а так же создаёт метод `routers` в пакете, который возвращает все роутеры пакета.

Эти методы используют роутер `__PACKAGE__->routers`.

	get "/" => sub {
		"hi!"
	};
	
Для того чтобы примонтировать контроллер к текущему, используем `mount`
	
	
	package main::SubRouters {
	
		use R::Yogi;
		
		ajax "/use(:ps)t" => sub {
			"ajax!" . param->ps;
		};
		
		get "/use(:txt)r" => sub {
			"ajax2!" . param->txt;
		};
		
		
	};
	
	mount "/ajax" => "main::SubRouters";
	mount "" => "main::SubRouters";

`yogi` создаёт метод класса `routers`, который возвращает роутеры установленные `get`, `post` и т.д.
	
	my $q = main->routers->querier;
	
	$q->get("/")					# hi!
	$q->ajax("/ajax/useROMt")		# ajax!ROM
	$q->get("/ajax/useROMr")		# ajax2!ROM
	
	$q->ajax("/useROMt")			# ajax!ROM
	$q->get("/useROMr")				# ajax2!ROM
	
В `$Coro::current->{request}` записывается запрос. Как только он не нужен - освобождается.
	
	$Coro::current	## undef
	
	