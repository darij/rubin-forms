= Формы

== Инициализация

Подключаем наш фреймворк:

[init]
	
	use lib "lib";
	use common::sense;
	use R::App;
	
Зададим конфигурацию:

	my $prj_sv = $app->man->project("my-form-prj", log=>0, logsqlinfo=>0);
	
[test]


== Введение

Формы используются для двух связанных действий: валидации данных и отображения их в html. Причём отображение данных может быть как в режиме ввода, так и в режиме просмотра.

Формы появляются в результате вызова `$app->form->...`

	my $f = $app->form->first_form;
	$f->Meta->
		input(login => "логин")->need("логин должен быть не пустой")->
		password(passwd => "пароль")->regex('.{6,25}', "пароль от 6 до 25 символов")->
		submit("залогиниться")->
		reset("очистить");	
		
	$f->launch({login => "kiss", passwd => 123});
		
	$f->render		#~ логин</label>
	$f->render		#~ пароль</label>
	$f->render		#~ залогиниться</button>
	
	
	ref $f->{element}{login}		# R::Form::Element
	
	$f->{element}{login}			## $f->login
	$f->{element}{passwd}			## $f->passwd
	
	$f->{element}{submit}->render		# <button type="submit" id="first_form-submit">залогиниться</button>
	$f->{element}{reset}->render		# <button type="reset" id="first_form-reset">очистить</button>
	
	$f->login->value	# kiss
	$f->login->label->value	# логин
	$f->login->error->value	## undef
	$f->login->input->value	# kiss
	$f->login->preview->value	# kiss
	
	$f->passwd->value			# 123
	$f->passwd->preview->value	# ******
	
	$f->login->id	# first_form-login
	$f->login->label->id	# first_form-login
	$f->login->error->id	# first_form-login
	$f->login->input->id	# first_form-login
	$f->login->preview->id	# first_form-login
	
	$f->login->render			# <input id="first_form-login" name="login" value="kiss">
	$f->login->label->render	# <label for="first_form-login">логин</label>
	$f->login->error->render	# 
	
	$f->passwd->error->render	# <label class="error" for="first_form-passwd">пароль от 6 до 25 символов</label>
	
	$f->login->input->render	# <label for="first_form-login">логин</label>\n<input id="first_form-login" name="login" value="kiss">\n
	
	$f->passwd->input->render		#  <label for="first_form-passwd">пароль</label>\n<input type="password" id="first_form-passwd" name="passwd" value="123">\n<label class="error" for="first_form-passwd">пароль от 6 до 25 символов</label>
	
Превью формы и элементов:
	
	$f->login->preview->render		# <span id="first_form-login">kiss</span>
	$f->passwd->preview->render		# <span id="first_form-passwd">******</span>
	
	$f->preview->render		#~ <div id="first_form">
	
Как только удаляется форма - тут же удаляются и её поля.
	
	my $login = $f->{element}{login};
	
	ref $login->form	# R::Form::First_form
	
	undef $f;
	
	$login->form	## undef
	
	

== Пример приложения

@@my-form-prj/model/User.pm
	package R::Row::User;
	
	use common::sense;
	use R::App;
	
	sub setup {
		my ($fields) = @_;
		$fields->
			col(login => "varchar(255)")->
			col(password => "varchar(255)")->
			ref("role")->remark("роль пользователя")->
		meta();
	}
	
	sub realdata {
		$app->model->user->new->login("Василий")->password("всем чао!")->role("admin")->saveAs(1);
	}
	
	1;	
@@my-form-prj/model/Role.pm
	package R::Row::Role;
	
	use common::sense;
	use R::App;
	
	sub setup {
		my ($fields) = @_;
		$fields->
			col(name => "varchar(255)")->remark("название роли")->
			m2m("roleset" => "role" => "inroles")->remark("роль")->
		meta();
	}
	
	sub realdata {
		$app->model->role->new->name("admin")->saveAs(1);
	}
	
	1;
@@my-form-prj/form/Login.pm
	package R::Form::Login;
	
	use common::sense;
	use R::App;
	
	# описание полей
	sub setup {
		my ($fields) = @_;
		
		# тип(имя => лейбл => плейсхолдер)
		$fields->
			input(login => "логин")->need("логин должен быть не пустой")->
			password(passwd => "пароль")->regex('.{6,25}', "пароль от 6 до 25 символов")->
			submit("залогиниться")->
			reset("очистить");
	}

	# выполняется на запрос формы до ok и fail. Тут можно разместить дополнительную валидацию
	sub submit {
		my ($self) = @_;
		$self->info("submit");
	}
	
	# после submit выполняется валидация. Если она прошла, то всё ок
	sub ok {
		my ($self) = @_;
		my $user = $app->model->user->find(login=>$self->login->value, password=>$self->passwd->value)->exists;
		if($user) {
			# если пользователь уже зарегистрирован, то - разлогиниваемся
			if($self->request->user->id) {
				$app->form->logout->ok;
			}
			
			$self->session->user_id($user->id);
			$self->response->cookie("sess_in", $self->session->id);
			$self->info("успешно!");
		}
		else {
			$self->error("логин или пароль не верен");
		}
	}
	
	# выполняется если не прошла валидация или submit или ok вернул ошибки
	sub fail {
		my ($self) = @_;
		$self->error("fail");
	}
	
	# выполняется на заполнение полей при показе формы
	sub select {
		my ($self) = @_;
		my $user = $app->model->user(1);
		return login => $user->login // "no-login!!!";
	}
	
	1;
@@my-form-prj/lib/Controller/Gold.pm
	package Controller::Gold;
	use R::App qw/:yogi :max/;

	get post "/login-form/" => sub {
		# инициализируем форму
		my $form = $app->form->login( data->All );
		return $form->form_tag->render;
	};

	# показывает отображение данных
	get "/user/" => sub {
		return $app->form->login->preview->render;
	};
	
	1;
[test]
	
Синхронизируем базу.

	$app->meta->sync;	
	
	$app->model->user(1)->login	# Василий

	$app->sitemap->find("GET /user/")					#~ id="login-login">Василий
	
	$app->sitemap->find("GET /login-form/")				#~ value="Василий"
		
	$app->sitemap->find("POST /login-form/", {login=>"", passwd=>123})		#~ логин должен быть не пустой
	$app->sitemap->find("POST /login-form/", {login=>"", passwd=>123})		#~ пароль от 6 до 25 символов
	
	$app->sitemap->find("POST /login-form/", {login=>"root", passwd=>123})		#~ пароль от 6 до 25 символов
	
	$app->sitemap->find("POST /login-form/", {login=>"root", passwd=>123456})		#~ логин или пароль не верен

	my $success = $app->sitemap->find("POST /login-form/", {login=>"Василий", passwd=>"всем чао!"});		
	$success->[1][0]	# Set-Cookie
	$success->[1][1]	#~ sess_in=
	$success->[2] 		#~ успешно!


== Генератор форм

Поля формы могут размещаться вертикально или горизонтально. Однако разные поля требуют разных пропорций от формы. Генератор учитывает это и формирует таблицу с выделением оптимального места для полей.


