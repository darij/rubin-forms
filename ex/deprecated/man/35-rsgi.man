= Сервера

== Инициализируемся для тестов
[init]
	use lib "lib";
	use common::sense;
	use R::App qw/$app app msg1/;
	use R::Coro;

	my $port = 9013;

[test]


== Сервер в отдельном процессе

# Запустим наш `RSGI`-сервер в отдельном процессе.

	# my $fullminant = $app->process(fullminant => "
		# app->fulminant->new(
			# port => $port,
			# ritter => sub { 'OK' . int shift->{ns} },
		# )->open->accept
	# ")->waitrun(.2);

# Пусть запустится:
	
	# my $x;
	# $x = $app->url(":$port")->read		#~ OK
	# $app->url(":$port/10")->read		## $x

	# $fullminant->kill("INT");
	

== Сервер на волокнах

Создадим `RSGI`-сервер.

	my $fulminant = $app->fulminant(
		port => $port,
		ritter => sub { 'OK' . int shift->{ns} },
	)->open;
	
	my $a = async {
		$fulminant->accept;
	};
	
	$app->url(":$port")->res->status_line	# 200 OK
	
	my $x;
	$app->url(":$port")->read			#~ OK
	$app->url(":$port/10")->read		#~ OK
	
Гасим сервер:
	
	$a->throw("quit");
	$a->status # Q

	undef $fulminant;
	
	
== Excellent

`Excellent` предоставляет удобный интерфейс для запуска приложений.

	my $BIG = "123\n" x 10000;

	my $port = 3194;
	
	package Memorial {
		use common::sense;
		use R::App;
		use R::Yogi;
		
		set queriers => 10;
		set websockets => 0;
		set port => $port;
		
		get "/sleep-:time" => sub {
			sleep attr->time;
			"sleepped"
		};
	
		my $name = "memorial";
		get "/name" => sub {
			$name
		};
		
		put "/name" => sub {
			$name = data->name;
			"name puts $name"
		};
		
		get "/hello" => sub {
			"привет!"
		};
		
		get "/big" => sub {
			$BIG
		};
		
		get "/511" => sub {
			die [511];
		};
		
		get "/err" => sub {
			die "ошибка, знаете-ли";
		};
		
		get "/err0" => sub {
			die 123;
		};
	}

	my $excellent = $app->rsgiExcellent(pkg => "Memorial")->run;
	
Следующий запрос начнёт выполняться
	
	my $async = async {
		my $kiss = $app->url->timeout(1)->keep;
	
		$kiss->alive("http://localhost:$port/big")->read		## $BIG
		
		$kiss->alive("http://localhost:$port/511")->res->code	# 511
		
		$kiss->alive("http://localhost:$port/hello")->read		# привет!
	};

	cede;
	
и передаст управление на:
	
	my $less = $app->url->timeout(1)->keep;
	
	$less->alive("http://localhost:$port/err")->read		#~ ошибка, знаете-ли
	
	my $keep = $app->url->timeout(1)->keep;
	
	$keep->alive("http://localhost:$port/name")->read		# memorial
	
	$less->alive("http://localhost:$port/err0")->read		#~ 123
	
	$keep->alive("http://localhost:$port/name")->put(name => "six")->read	# name puts six
	
	$less->alive("http://localhost:$port/hello")->read		# привет!
	
	$keep->alive("http://localhost:$port/name")->read		# six
	
	$keep->alive("http://localhost:$port/xxx")->read		# 404 Not Found
	
	$keep->alive("http://localhost:$port/err")->read		#~ ошибка, знаете-ли
	
	$less->alive("http://localhost:$port/big")->read		## $BIG
	
	$async->join;
	
Теперь оттестируем таймаут:

	$excellent->{fulminant}{timeout} = 0.25;
	
	$app->url("http://localhost:$port/sleep-10")->read		# Gateway Timeout 0.25s
	
	undef $excellent;
	
== Файлы

	my $port = 3194;

	package Media {
		use common::sense;
		use R::App;
		use R::Yogi;
		
		set
			queriers => 1,
			port => $port;
		
		get "/*path.:ext" => sub {
			root "var/.miu/root"
		};
	}
	
@@image.png

	media
	
[test]

	my $excellent = $app->rsgiExcellent(pkg => "Media")->run;
	
	$app->url("http://localhost:$port/image.png")->res->content_type		# image/png
	$app->url("http://localhost:$port/image.png")->read						# \nmedia\n
	
	undef $excellent;
	
== Dev

Сервер разработки перенаправляет запросы на фоновый сервер. Когда код сайта изменяется, то фоновый сервер перезагружается.

Если фоновый сервер лежит, то разработчик увидит корректную ошибку.

#$app->rsgiExcellent()->devrun;
	

== jsx

@@jsx/test_ex.jsx

	import { h, Component } from 'lib/preact';

	export default class test_ex extends Component {
		constructor (props) {
			super(props);
			this.stats = {
				
			};
		}

		render (props, stats) {
			return <div class="x1">
				Привет!
			</div>;
		}

	}

@@jsx/test_front.coffee

	import { h, Component } from 'lib/preact'


	export default class test_front extends Component

		render: ({title})->
			<div>
				{title}
			</div>


[test]

Скомпиллируем:

	my $prefix = "var/.miu";

	$app->javascriptCompile->new(srcfrm=>undef, src => "$prefix/root/jsx", dst => "$prefix/js", phase=>1, echo => 1)->cc;

Настроим js на работу с нашими тестовыми каталогами:

	#$app->javascript->{normalize_css} = "";

	$app->javascript->{src} = app->file("$prefix/js")->add($app->javascript->{src});
	

Запустим сервер:

	my $port = 3194;
	
	package TestJsx {
		use common::sense;
		use R::App;
		use R::Yogi;
		
		set
			queriers => 1,
			port => $port
			;
		

		get "/" => sub {
			render "test_front", {title => "hi!"};
		};
		
		get "/ex" => sub {
			render "test_ex";
		};
	}

	my $excellent = $app->rsgiExcellent(pkg => "TestJsx")->run;
	
	#my $x=$app->url("http://localhost:$port/ex")->read;
	#msg1 $app->htmlquery($x)->text;
	
	$app->url("http://localhost:$port/ex")->res->code		# 200
	$app->url("http://localhost:$port/ex")->read			#~ Привет
	$app->url("http://localhost:$port/")->read				#~ hi!
	
	#msg1 $app->htmlquery($app->url("http://localhost:$port/")->read)->text;
	
	undef $excellent;
	

== websocket-ы

Создадим приложение:

	my $counter = 0;

	package Loki {
		use common::sense;
		use R::App;
		use R::Yogi;
		
		# конфигурация: запускаем 10 волокон на запросы и 20 на вебсокеты
		set port => $port,
			queriers => 1,
			websockets => 3
			;
		
		websocket "/ws" =>
			connect => sub { $counter .= "+" },
			disconnect => sub { $counter .= "-" },
			plus => sub {
				my ($q, $a, $b) = @_;
				msg1 "hi!";
				$q->emit( onplus => $a + $b );
				msg1 "hi!2";
			},
			minus => sub {
				my ($q, $a, $b) = @_;
				$q->emit( onminus => $a - $b );
			}
		;

	}

Запускаем приложение в этом же процессе.
	
	# my $excellent = $app->rsgiExcellent(pkg => "Loki")->run;
	
	
	# use AnyEvent::WebSocket::Client;
	# my $ws = AnyEvent::WebSocket::Client->new;

	# my $connection = $ws->connect("ws://127.0.0.1:$port/ws")->recv;
		
	# $connection->on(each_message => sub {
		# # $connection is the same connection object
		# # $message isa AnyEvent::WebSocket::Message
		# my($connection, $message) = @_;
		
		# if($message->is_text || $message->is_binary) {
			# my $body = $message->body;
			# my $data = $app->json->from($body);

			# my %ON = (
				# "onplus" => sub {
					# my ($q, $a) = @_;
					# $a			# 3
				# },
				# "onminus" => sub {
					# my ($q, $a) = @_;
					# $a			# -1
				# },
			# );
			
			# $ON{$data->[0]}->(@$data);
		# }
	# });
	
	
	# msg1 "emit 1";
	# $connection->send($app->json->to([plus => 1, 2]));
	# msg1 "emit 2";
	# $connection->send($app->json->to([minus => 1, 2]));
	
	# $connection->close;
	
	
	# my $ws = $app->httpWs->new("ws://127.0.0.1:$port/ws")->on(
		# "onplus" => sub {
			# my ($q, $a) = @_;
			# $a			# 3
		# },
		# "onminus" => sub {
			# my ($q, $a) = @_;
			# $a			# -1
		# },
	# );
	
	# msg1 "emit 1";
	# $ws->emit(plus => 1, 2);
	# msg1 "emit 2";
	# $ws->emit(minus => 1, 2);
	
Остановим сервер:
	
	# undef $excellent;
	
	# $counter	# 0+-
	
Ссылки:
1. [Работа с WebSocket в Perl](http://pragmaticperl.com/issues/26/pragmaticperl-26-работа-с-websocket-в-perl.html)
2. [Сервер Protocol::WebSocket](https://metacpan.org/pod/Protocol::WebSocket)
3. [Клиент Net::WebSocket](https://metacpan.org/pod/Net::WebSocket)
