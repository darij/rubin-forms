= Portal

== Инициализация

Подключаем наш фреймворк:

[init]
	
	use lib "lib";
	use common::sense;
	use R::App;
	
	my $PRJ_CLEARER = $app->man->project("13-portal", nodatabase=>1, log=>0, logsqlinfo=>0);
	
[test]


== Введение

Портал - это сервер с роутингом. Он использует шаблоны `R::Xf` и делает для них роутинг.

== R::File::Map

Используется для хранения роутинговых путей.

	$app->fileMap->get("x/y/z.pl");		## undef
	$app->fileMap->exists("x/y/z.pl");	## ""
	$app->fileMap->set("x/y/z.pl", 10);
	$app->fileMap->exists("x");			## ""
	$app->fileMap->get("x/y/z.pl");		# 10
	$app->fileMap->set("x/y", 20);
	
Обновлять нельзя:

	$app->fileMap->set("x/y", 20);		#@ ~ устанавливать можно только один раз
	
Но с разными ключами можно:
	
	$app->fileMap->set("x/y", 6, "\a");
	$app->fileMap->get("x/y", "\a");	# 6

По умолчанию используется ключ "\e":

	$app->fileMap->get("x/y", "\e")		# 20
	
	$app->fileMap->get("x/y/z.pl");		# 10
	$app->fileMap->exists("x/y");		# 1
	$app->fileMap->get("x/y");			# 20
	$app->fileMap->exists("x/y/z.pl");	# 1
	
Можно получить все установленные значения:
	
	[ $app->fileMap->rget("x/y/z.pl/tr") ]	## [20, 10]
	$app->fileMap->rget("x/y/z.pl/tr")		# 10
	
И удалять:

	$app->fileMap->del("x/y", "\a");
	$app->fileMap->get("x/y", "\a");	## undef
	$app->fileMap->set("x/y", 6, "\a");

	

== Контроллеры

Контроллер объединяет разные методы. У GET нет подрасширения, у остальных - есть.

@@13-portal/html/favicon.ico
	icon!

@@13-portal/html/x/z.xf
	i am $six
	
@@13-portal/xf/layout.xf
	<html>$CONTENT.RAW</html>

@@13-portal/xf/menu.xf
	<menu>$CONTENT.RAW</menu>

@@13-portal/html/die501.xf
	{{ die([501]) }}
	
@@13-portal/html/die502.xf
	{{ die([502]) }}

@@13-portal/xf/errors/501.xf
	501 hi!

@@13-portal/xf/errors/5xx.xf
	$response.status hi!!! $response.data.RAW

[test]

	my $port = 3024;
	my $site = $app->portal(
		port=>$port, 
		dev=>1,
		access => {
			"html" => {
				"/" => {
					error => {
						501 => "xf/errors/501.xf",
						5 => "xf/errors/5xx.xf",
					},
					layout => ["xf/layout.xf", "xf/menu.xf"],
				},
			},
			"site/ex.cc" => {
				"/" => {
					error => {
						501 => "xf/errors/501.xf",
						5 => "xf/errors/5xx.xf",
					},
					layout => ["xf/layout.xf", "xf/menu.xf"],
				},
			},
		},
	)->run;
	my $sv = $site->sv;

=== Тестируем проект
	
	$app->url(":$port/x/z")->noredirect->GET		# 307 Redirect on path/
	$app->url(":$port/x/z")->noredirect->POST		# 307 Redirect on path/
	
	$app->url(":$port/x/z")->header(Accept=>"text/html")->GET(six=>6)			# <html><menu>i am 6\n</menu>\n</html>\n
	$app->url(":$port/x/z/")->POST(six=>6)			#~ i am 6
	
	$app->url(":$port/x/fff/")->post(six=>6)->status		# 404


Проверим статику:
	$app->url(":$port/favicon.ico")->get->status		# 200
	$app->url(":$port/favicon.ico")->GET				#~ icon!
	$app->url(":$port/favicon.ico")->get->header("Content-Type")	#~ image/x-icon
	
	$app->url(":$port/favicon1.ico")->get->status		# 404
	
Выдача ошибок:

	$app->url(":$port/die501/")->GET		#~ <html><menu>501 hi!
	$app->url(":$port/die501/")->status		# 501
	$app->url(":$port/die502/")->GET		#~ <html><menu>502 hi!!!
	$app->url(":$port/die502/")->status		# 502

Другой сайт:

@@13-portal/site/ex.cc/favicon.ico
	icon ex!
	
@@13-portal/site/ex.cc/index.xf
	<noop widget="A">index $A.host $A:kia</noop>
	<script>
		class A extends ModelForm {
			<% @kia = kia %>
			host() {
				<% q.url.host %>
			}
		}
	</script>

[test]

	$app->url(":$port/die502/")->header(Host=>"ex.cc")->status		# 404
	$app->url(":$port/favicon.ico")->header(Host=>"ex.cc")->GET		#~ ^icon ex!
	$app->url(":$port/")->header(Host=>"ex.cc")->GET(kia=>20)		#~ ^<html><menu>index ex.cc 20
	
	$app->url(":$port/~a~host/")->header(Host=>"ex.cc")->GET		#~ ^<html><menu>ex.cc

@@13-portal/site/ex.cc/abc/x.xf
	<script>
		class Maps extends Widget {
			mips() {
				<% {Key: [6, 7, 8]} %>
			}
		}
	</script>
[test]
	
	$app->json->from($app->url(":$port/abc/x/~maps~mips/")->header(Host=>"ex.cc", Accept=>"text/json")->GET)		## {Key => [6, 7, 8]}
	
	
	
=== Передача файлов

@@13-portal/share/ava.jpg
	this is jpeg	
@@13-portal/html/ava.xf
	$request.POST.ava.remote.path <noop sub=xyz>($request.POST.ava.type)</noop> $request.POST.ava.read replace!
[test]
	#/
	
	$app->url(":$port/ava/")->PUT(ava => $app->file("share/ava.jpg"), x => 123)		# <html><menu>ava.jpg (image/jpeg) this is jpeg replace!</menu>\n</html>\n

=== Горячая замена модулей

При запросе происходит сканирование директорий и замена изменившихся файлов.

	$app->file("html/ava.xf")->write("<noop sub=xyz>hi!</noop>");
	
	$app->url(":$port/ava/")->PUT(ava => $app->file("share/ava.jpg"), x => 123)		# <html><menu>hi!</menu>\n</html>\n
	
Теперь заменим сервис.

@@13-portal/lib/Ex.pm
	package Ex;
	use common::sense;
	use R::App;
	sub new { my $cls = shift; bless {@_}, $cls }
	sub hello { 123 }
	1;
@@13-portal/html/ex/service.xf
	-{{ app.ini:service:ex = { "~class" => "Ex" }; app.ex.hello }}-
[test]

	$app->url(":$port/ex/service/")->GET		#~ -123-
	
	$app->file("lib/Ex.pm")->replace(sub { s/123/567/ });
	
	$app->url(":$port/ex/service/")->GET		#~ -567-
	
=== Отключаем портал
	
	undef $sv;
	
