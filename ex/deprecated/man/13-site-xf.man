= Роутинг R::Site и шаблонизатор R::Xf

== Инициализация

Подключаем наш фреймворк:

[init]
	
	use lib "lib";
	use common::sense;
	use R::App;
	
	my $PRJ_CLEARER = $app->man->project("13-site-xf", log=>0, logsqlinfo=>0, nodatabase=>1);
	
[test]

== Введение

== R::File::Map

	$app->fileMap->get("x/y/z.pl");		## undef
	$app->fileMap->exists("x/y/z.pl");	## ""
	$app->fileMap->set("x/y/z.pl", 10);
	$app->fileMap->exists("x");			## ""
	$app->fileMap->get("x/y/z.pl");		# 10
	$app->fileMap->set("x/y", 20);
	
Обновлять нельзя:

	$app->fileMap->set("x/y", 20);		#@ ~ устанавливать можно только один раз
	
	$app->fileMap->get("x/y/z.pl");		# 10
	$app->fileMap->exists("x/y");		# 1
	$app->fileMap->get("x/y");			# 20
	$app->fileMap->exists("x/y/z.pl");	# 1
	

== Контроллеры

Контроллер объединяет разные методы. У GET нет подрасширения, у остальных - есть.

@@13-site-xf/html/favicon.ico
	icon!

@@13-site-xf/html/x/z.pl
	return {x=>{y=>10, b=>[10,30]}, c=>"b"};

@@13-site-xf/html/x/z.xf
	hi! `$x:y` $x:b[0] $x{c}:1
	
@@13-site-xf/html/x/z.post.pl
	{p=>6}
	
@@13-site-xf/html/x/z.post.xf
	i am post $p

@@13-site-xf/html/.access.pl
	$q->GET->let && die [401];
	
@@13-site-xf/html/x/.access.pl
	$self->SUPER::ACCESS;
	$q->GET->let2 && die [402];
	
	
[test]

	my $port = 3023;
	my $site = $app->site(port=>$port, dev=>1)->run;
	my $sv = $site->sv;
	
	ref $site->access->get("html/x/z") 	# ACCESS::html::x
	ref $site->access->get("html/x") 	# ACCESS::html
	$site->access->get("html") 			## undef
	
	ref $site->action->get("html/x/z") 	# SITE::html::x::z
	$site->action->get("html/null") 	## undef
	
	$app->url(":$port/x/z")->noredirect->GET		# 307 Redirect on path/
	$app->url(":$port/x/z")->noredirect->POST		# 307 Redirect on path/

Шаблонизатор применяется, если затребован `text/html`.
	
	$app->url(":$port/x/z/")->header(Accept => "text/html")->GET		#~ hi! `10` 10 30
	
	$app->url(":$port/x/z/")->header(Accept => "any")->GET				# {"c":"b","x":{"b":[10,30],"y":10}}
	
	$app->url(":$port/x/z/")->header(Accept => "text/html")->POST		#~ i am post 6
	$app->url(":$port/x/z/")->header(Accept => "any")->POST				# {"p":6}
	
А тут сработают файлы `.access.pl`. `.access.pl` накрывает свой каталог с подкаталогами.
	
	$app->url(":$port/?let=1")->post->status			# 401
	$app->url(":$port/x/?let=1")->post->status			# 401
	$app->url(":$port/x/y/?let=1")->post->status		# 401
	$app->url(":$port/x/y/?let2=1")->post->status		# 402
	
	
Проверим статику:
	$app->url(":$port/favicon.ico")->get->status		# 200
	$app->url(":$port/favicon.ico")->GET				#~ icon!
	$app->url(":$port/favicon.ico")->get->header("Content-Type")	#~ image/x-icon
	
	$app->url(":$port/favicon1.ico")->get->status		# 404
	
	undef $sv;
	
	
== Шаблонизаторы

=== Вставки

	$app->file("html/ex-ins.xf")->write('hi! `$x:y` $x:b[0] $x{b}:1');
	$app->xf->render("html/ex-ins.xf", {x=>{y=>10, b=>[10,30]}, b=>"b"})		# hi! `10` 10 30
	
	$app->file("html/ex-ins-1.xf")->write("x{{ a:b:c < 20 }}y");
	$app->xf->render("html/ex-ins-1.xf", {a => {b=>{c=>10}}})		# x1y
	
Операторы сравнения чисел: <, >, <=, >=, =,  <>
Операторы сравнения строк: ~<, ~>, ~<=, ~>=, ~=, ~<>
	
	$app->file("html/ex-ne.xf")->write("x{{ a:b:c <> 20 }}y");
	$app->xf->render("html/ex-ne.xf", {a => {b=>{c=>10}}})		# x1y

	$app->file("html/ex-sless.xf")->write("x {{ a:b:c ~< '10y'? 'yea': 'nou' }} y");
	$app->xf->render("html/ex-sless.xf", {a => {b=>{c=>10}}})		# x yea y
	
Вызов методов на объектах:
	
	package A {
		sub hi { shift; join ", ", @_ }
	}
	
	$app->file("html/ex-obj.xf")->write('x $x.hi(y+1, "(<&\'\"") y');
	$app->xf->render("html/ex-obj.xf", {x=>bless({}, "A"), y=>5})		# x 6, (&lt;&amp;&#39;&quot; y
	
Вставки, как мы видели, экранируются. Чтобы вставить значение без экранирования:

	$app->file("html/ex-noesc.xf")->write('x $x.RAW y');
	$app->xf->render("html/ex-noesc.xf", {x=> "<b>hi!</b>"})		# x <b>hi!</b> y
	
Внутри строк в подстановках так же работают подстановки:

	$app->file("html/ex-string.xf")->write('x {{ a . " \'$a\'" .RAW }} y');
	$app->xf->render("html/ex-string.xf", {a => "i'am hi!"})		# x i'am hi! 'i'am hi!' y


=== app

Для специальных функций через `app` создаётся объект и:

	$app->file("html/ex-number-format.xf")->write('$app.magnitudeNumber(1024)');
	$app->xf->render("html/ex-number-format.xf", {})		# 1 024
	
=== Условие

Тег вставляется, если условие выполняется.

	$app->file("html/ex-if.xf")->write("<div if='z>10' any=\"123\" attr><b if=x>10</b>20</div>");
	$app->xf->render("html/ex-if.xf", {z => 11})		# <div any="123" attr>20</div>
	
Вставляется содержимое тега, если выполняется условие:

	$app->file("html/ex-in-if.xf")->write("<div in-if='z>10' any=\"123\" attr>20</div>");
	$app->xf->render("html/ex-in-if.xf", {z => 11})		# <div any="123" attr>20</div>
	$app->xf->render("html/ex-in-if.xf", {z => 10})		# <div any="123" attr></div>

=== Цикл
	
	$app->file("html/ex-by.xf")->write("<div by=z as=i id=x-\$i><noop if=\"i=11\">-</noop>{{ i+1 }}</div>");
	$app->xf->render("html/ex-by.xf", {z => [11, 12]})		# <div id=x-11>-12</div><div id=x-12>13</div>
	
`<noop ...>` и `</noop>` не вставляется, но вставляется содержимое, так же отрабатывают специальные атрибуты.
	
В `by` можно передавать и хеши:
	
	$app->file("html/ex-by-if.xf")->write("<div by=z key-as=k val-as=v if='hoop<>11' id=x-\$v>\$k-\$loop</div>");
	$app->xf->render("html/ex-by-if.xf", {z => {x=>11, y=>12}})	# <div id=x-12>y-y</div>
	
`in` аналогичен `by`, но повторяет содержимое тега:

	$app->file("html/ex-in.xf")->write("<p in=z key-as=k val-as=v in-if='v<>11'>\$k=\$v \$loop=\$hoop</p>");
	$app->xf->render("html/ex-in.xf", {z => {f=>10, x=>11, y=>12}})	# <p>f=10 f=10y=12 y=12</p>

=== Форма

В инпуты форм value вставляется из имя_формы.имя_инпута.

	$app->file("html/ex-form.xf")->write("
		<form name=myform>
			<input name='login' value='Паша'>
			<input name=passwd>
		</form>
	");
	$app->xf->render("html/ex-form.xf", {myform => {login=>"Зло", passwd=>"Диско"}})	#~ (?s)Зло.*Диско
	$app->xf->render("html/ex-form.xf", {myform => {login=>"Зло", passwd=>"Диско"}})	#!~ Паша
	$app->xf->render("html/ex-form.xf", {})	#~ Паша

Текстареа:

	$app->file("html/ex-form-textarea.xf")->write("
		<form name=f>
			<textarea name=t1></textarea>
			<textarea name=t2>123</textarea>
		</form>
	");

	$app->xf->render("html/ex-form-textarea.xf", {f=>{}})			#~ 123
	$app->xf->render("html/ex-form-textarea.xf", {f=>{t2=>""}})		#!~ 123
	$app->xf->render("html/ex-form-textarea.xf", {f=>{t2=>undef}})	#~ 123
	$app->xf->render("html/ex-form-textarea.xf", {f=>{t1=>456, t2=>789}})	#~ (?s)456.*789
	
	
=== include

Можно включать другие шаблоны.

	$app->file("html/ex-for-include.xf")->write("x \$x y");
	$app->file("html/ex-include.xf")->write("<p include='html/ex-for-include.xf'>123</p>");
	$app->xf->render("html/ex-include.xf", {x => 10})	# <p>x 10 y123</p>
	
=== Комментарии

В комментариях подстановки не осуществляются и они удаляются из вывода.

	$app->file("html/ex-comment.xf")->write("<p>123</p><!-- \$x --><i>456</i>");
	$app->xf->render("html/ex-comment.xf", {x => 10})	# <p>123</p><i>456</i>
	
=== <script> и <style>

В скрипте и стилях могут быть подстановки `{{ ... }}`, однако теги и `$...` не учитываются.

	# $app->file("html/ex-script.xf")->write('<script>x=$x</script>');
	# $app->xf->render("html/ex-script.xf", {x => 10})	# <p>123</p><!-- \$x --><i>456</i>
	

	