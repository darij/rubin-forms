= Widget

== Инициализация

Подключаем наш фреймворк:

[init]
	
	use common::sense;
	use R::App;
	
	my $PRJ_CLEARER = $app->man->project("17-widget", include=>["router.yml"], log=>0, logsqlinfo=>0);
	
[test]


== Введение

Виджет - класс, являющийся контроллером.
Виджеты подчиняются правилам БЭМ. Другими словами - файлы с ними сгруппированы по блокам. Виджеты являются блоками, однако одновременно они являются контроллерами, то есть содержат методы не только для отображения, но и для сохранения данных.

== Принципы

* БЭМ

== Пример библиотеки

Настраиваем сайт:

@@17-widget/etc/router.yml

	service:
		celestial:
			port: 9045
			sites:
				'my.cc':
					param:
						layout: [layout]
						error:
							404: error-404
							4: error-4xx
							5: error-5xx
					routes:
						- '/books/*':
							- 'book-%id': lib-book			
	
@@17-widget/widget/widget.library/layout/layout.pm
	package R::Widget::Layout;
	use base R::Template::Widget;
	
	use common::sense;
	use R::App;
	
	has qw/CONTEXT/;
	
	sub mount { qw// }
	
	sub head {}
	
	sub body {
		my ($self) = @_;
		"-" . $self->CONTEXT . "-"
	}
	
	1;	


@@17-widget/widget/widget.common/lib-book/lib-book.pm
	package R::Widget::LibBook;
	use base R::Template::Widget;
	
	use common::sense;
	use R::App;
	
	has qw/id/;
	
	sub mount { qw// }
	
	sub head {}
	
	sub body {
		my ($self) = @_;
		"*" . $self->id . "*"
	}
	
	1;
	
[test]

	my $server = $app->celestial->run;
	my $sv = $server->sv;
	
	$app->url(":".$server->port."/books/book-12")->header(Host => "my.cc")->read		# -*12*-
	
	undef $sv;
	
