= Sitemap

== Инициализируемся для тестов

[init]	
	use lib "lib";
	use common::sense;
	use R::App qw/$app app msg1 guard/;
	
[test]


== Опции конструктора

@@html/miu.rx
	miu!!!
@@lib/Controller//HelloWorld.pm
	package Controller::HelloWorld;
	
	use common::sense;
	use R::App qw/:yogi/;

	get "/hello" => [303, [Location => "/hello/"], undef];
	
	get "/hello/" => "Hello world!";
	put "/hello/:name" => sub { "Hello " . attr->name . "!" };
	
	get "/welcome" => sub {
		status 202;
		header Test => 10;
		my $six = cookie->six(6);
		cookie six => $six+6, path => "/";
		"Welcome"
	};
	
	my %counter;
	
	front head "/*av" => sub {
		$counter{attr->av}++;
	};
	
	onerror 404 => get "/mx/" => sub { 
		die [200, [], \%counter] if defined query->counter;
	};
	
	onerror "4xx", route "/*av" => sub {
		my $status = status;
		
Перезатираем статус:

		status 404;
		
Обратите внимание, что `front` и `onerror` запускаются в пустом контексте.
		
		data "4xx! $status";
	};
	
	
	1;

[test]

	my $test_dir = "var/.miu/root";

Укажем пути для файлов (`root`), контроллеров (`act`) и шаблонов (`map`):
	
	my $sitemap = $app->sitemap(
		root => "$test_dir/html", 
		act => "$test_dir/lib/Controller",
		route => [
			"GET /*x.html" => "DELETE /t/%x.xml",
			"DELETE /t/yyy/x.xml" => {this => [qw/is json/]},
			"/" => sub { my ($request) = @_; "hi! i'm called ".$request->count." times" },
		],
		# error => {
			# "4xx" => "GET /errors/4xx",
		# }
	);

Есть 3 формы задания роутов.

Первая форма просто подставляет вместо указанного урла - другой.

	$sitemap->find("/yyy/x.html")			## {this => [qw/is json/]}
	$sitemap->find("GET /yyy/x.html")		## {this => [qw/is json/]}
	$sitemap->find("DELETE /t/yyy/x.xml")	## {this => [qw/is json/]}	
	
Контроллеры сканируются при инициализации `R::Sitemap`, а при первом запросе соответствующий контроллер подгружается `require`-ом.

	my $request = $app->rsgiRequest(via => "GET", location => "/hello");
	$sitemap->find($request)				## [303, [Location => "/hello/"], "303 See Other"]

Просто строка:
	
	$sitemap->find("/hello/")	# Hello world!
	$sitemap->find("/welcome")	## [202, [Test => 10, 'Set-Cookie' => 'six=12; Path=/'], "Welcome"]
	
	$sitemap->find("PUT /hello/Ange")	# Hello Ange!
	
Файл:
	
	my $file = $sitemap->find("/miu.rx");
	ref $file 		# R::File
	$file->read		# miu!!!

== Сервер

`sitemap` является ещё и сервером.

	my $sv = $sitemap->port(9019)->run->sv;
	
Проверим ошибки:
	
	$app->url(":9019/hello/Ange")->put->read	# Hello Ange!
	$app->url(":9019/hello!")->put->read		# 4xx! 404
	$app->url(":9019/hello/Ange")->get->read	# 4xx! 405
	$app->url(":9019/mx/?counter")->get->read	# {}
	
	undef $sv;