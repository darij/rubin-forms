= Экшены и виджеты

Экшены находятся в папке `act` проекта.

Это файлы с расширением `.html`.

Их урлы формируются из их путей.

== Инициализация

[init]
	
	use common::sense;
	use R::App qw/$app msg msg1 server/;

	$app->man->conf;
	
	my $act = $app->templateAsp(inc => ["var/.miu/root/act"], ext => "act");
	
[test]
	
== Шаблоны

Шаблоны имеют синтаксис подобный `ASP`.

@@act/.act
	hi!
[test]
	
	$act->render("/");		# hi!
	
=== Наследование

@@act/Layout.act
	<div><%:= @$content %></div>
@@act/action.act
	<% extends "Layout" %>123
[test]

	$act->render("/action");	# <div>123</div>
	
=== Блоки

@@act/A.act
	<div><% block news %>123<% end %></div>
@@act/B.act
	<% extends "A" %><% block news %>456<% end %>
[test]

	$act->render("/A");	# <div>123</div>
	$act->render("/B");	# <div>456</div>
	
=== Оверлеи

Блоки при наследовании использовать нельзя если вы вставляете `@$content`, т.к. они сразу же и вызываются. Но можно использовать оверлей - это обычная `sub`, только с синтаксическим сахаром.

@@act/Sub.act
	<% overlay news %>456<% end %>123<%= $self->news %>
[test]

	$act->render("/Sub");	# 123456
	
=== Присвоение

@@act/123-444.act
	<div><%= "<ttt>" %></div>
[test]

	$act->render("/123-444");	# <div>&lt;ttt&gt;</div>

@@act/123-555.act
	<div><%:= "<ttt>" %></div>
[test]

	$act->render("/123-555");	# <div><ttt></div>

=== Включение экшенов

@@act/act-for-include.act
	<div><% include "/act-include" t => 456 %></div>
@@act/act-include.act
	<span><%= $self->request->attr->xxx %></span><b><%= $self->{t} %></b>
[test]
	
	my $r = $app->rsgiRequest(via=>"GET", _attr=>{"xxx"=>123});
	$act->render("/act-for-include", request => $r)	# <div><span>123</span><b>456</b></div>
	
== Сервер

@@html/x.html
	hi, i am x!
[test]

	my $dim = $app->dim(port=>9001, act=>$act, root=>["var/.miu/root/html"])->run->sv;
	
	$app->url("localhost:9001")->read			# hi!
	$app->url("localhost:9001/x.html")->read	# hi, i am x!
	$app->url("localhost:9001/A")->read			# <div>123</div>
	$app->url("localhost:9001/B")->read			# <div>456</div>
	
	undef $dim;

=== Роли

У нас есть сессии `$self->request->session`. У сессии есть пользователь `$self->request->user`. Все неизвестные пользователи привязаны к роли `$self->request->role("guest")`. Зарегистрированные прользователи: `$self->request->role("user")`. Так же есть роль `admin`.

Роли накладываются через админку на определённые пути. Таким образом можно заблокировать файлы и каталоги.



	# my $dim = $app->dim(port=>9001, act=>$act, root=>$app->file("var/.miu/root/html"))->run->sv;
	
	
	
	# undef $dim;
	
=== Таблицы роутинга

Экшены подгружаются только по запросу. Однако проверять - существует ли файл на диске - долго.
В режиме продакшена создаётся таблица роутинга, которая содержит все существующие файлы в папке `/html` и таблица для экшенов.


	
