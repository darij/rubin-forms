= Базовая функциональность

`R::App` добавляет несколько утилит в пакет.

== Инициализируемся для тестов
[init]
	
	use lib "lib";
	use common::sense;
	use R::App qw/$app app msg1 Tour guard/;
	
[test]


== has

С помощью `has` объявляются геттерры и сеттеры. Так же `has` создаёт стандартный конструктор, если его нет.

=== lazy

Устанавливается по запросу. Если вместо опций указать подпрограмму, то это будет `lazy`.

	my $counter = 0;

	package Alphabet {
		use R::App qw/has/;
		
		# просто свойства
		has qw/alef yota/;
		
		# если не были установлены в конструкторе, то подгружаются по требованию
		has summ => sub { $counter++; $_[0]->alef + $_[0]->yota	};
	}

	my $x = Alphabet->new(alef=>10, yota=>20);
	$counter 		# 0
	$x->summ		# 30
	$counter 		# 1
	$x->summ		# 30
	$counter 		# 1
	$x->summ(10)	## $x
	$x->summ		# 10
	
	Alphabet->new->summ				# 0
	
	Alphabet->new(summ => 6)->summ	# 6
	
	$counter		# 2
	
=== default

Дефолтное значение. Присваивается в конструкторе, если свойство не было указано в параметрах. Если это функция, то вызывается.

	package DefaultExample {
		use R::App qw/has/;
		
		has see => { default => 10 };
		has num => { default => 6 };
		has hash => { default => sub { +{x=>10} } };
	}

	DefaultExample->new(see => 5)->see	# 5
	DefaultExample->new(see => 5)->num	# 6
	DefaultExample->new(see => 5)->hash	## {x=>10}

=== builder

Билдер вызывается всегда после установки значения из параметров конструктора или `default`.

	package BuilderExample {
		use R::App qw/has/;
		
		has see => { default => 10, builder => sub {
			my ($self, $see) = @_;
			$self->see( $see + 10 );
		} };
	}

	BuilderExample->new->see	# 20
	BuilderExample->new(see=>16)->see	# 26
	
=== clearer

Персональный деструктор переменной.

	my $counter=0;

	package ClearerExample {
		use R::App qw/has/;
		
		has see => { default => 10, clearer => sub {
			my ($self) = @_;
			$counter += $self->see;
		} };
	}
	
	ClearerExample->new;
	
	$counter	# 10

	
=== Порядок аргументов в new

Вначале вызываются `default` в порядке указанном в файле.
Затем `builder` в порядке указанном в файле.
`clearer` срабатывают в деструкторе в противоположном порядке указанном в файле.

	package OrderNew {
		use R::App;
		
		my $counter = 0;
		
		has qw/a/ => {builder => sub {
			$counter++	# 0
		}, clearer => sub {
			$counter++	# 5
		}};
		has qw/b/ => {default => sub {
			$counter++	# 1
		}, builder => sub {
			$counter++	# 2
		}, clearer => sub {
			$counter++	# 4
		}};
		has qw/c/ => {builder => sub {
			$counter++	# 3
		}};
	}

	OrderNew->new(c=>5, a=>6);
	
=== get

Указывает геттер. Считанное значение устанавливается в `$_`.

	package GetExample {
		use R::App qw/has/;
		
		has see => {
			get => sub { $_ + 10 },
		};
	}

	GetExample->new(see => 6)->see	# 16
	
Если не нужно считывать значение в `$_`, то:
	
	package GetterExample {
		use R::App qw/has/;
		
		has see => {
			get => sub { $_[0]{see}+@_ },
			fetch => 0,
		};
	}

	GetterExample->new(see => 6)->see	# 7
	
	
=== set

Указывает сеттер. Присваивается свойтву объекта то, что возвращает сеттер.

	package SetExample {
		use R::App qw/has/;
		
		has see => { set => "set_see" };
		sub set_see { $_[1] + $_ }
	}

	SetExample->new(see => 6)->see(6)->see	# 12

Если нужно отменить установку атрибута:
	
	package SetterExample {
		use R::App qw/has/;
		
		has see => {
			out => 0,
			set => sub { $_[0]{look} = $_[1]+($_? 10: 20) },
			put => 0,
		};
		
		has qw/look/;
	}

	my $x = SetterExample->new;
	$x->{see} = 3;
	$x->see(6);
	$x->{see}			# 3
	$x->look			# 26

=== required

Обязателен в `new`.
	
	package RequiredExample {
		use R::App qw/has/;
		
		has see => {required=>1};
	}

	RequiredExample->new(see => undef)->see	## undef
	RequiredExample->new	#@ ~ see is required
	

=== late

Запрещает устанавливать через `new`.

	package LateExample {
		use R::App qw/has/;
		
		has see => {late=>1};
	}

	LateExample->new(see => 1)	#@ ~ see is late
	
`late` по умолчанию снято, однако `is=>"ro"` и `is=>"no"` - устанавливаает его.


=== role

Указывает тип свойства:

* getset - геттер: `self->prop(key)`, сеттер: `self->prop(key, val)`. По умолчанию.
* param - CGI-подобное свойство

=== isa

Проверяет тип при установке значения.
Можно передавать функцию-валидатор. Такая функция получает `my ($self, $val) = @_` и должна вернуть истину или ложь.
Либо строку с справилом для распознавания.
Типы записываются через пробел.
Могут быть: `undef`, `Int`, `Num`, `HASH`, `ARRAY` и т.д. Если в типе есть `::`, то считаются классом и берётся Isa. Классы первого уровня записываются как `::Class`.
Значения хешей берутся в `{}`, массивов в `[]`, а ссылок в `()`.

Типы могут быть рекурсивными:

	package IsIsaNix {
		use R::App qw/has subtype/;
		
		subtype Nix => "ARRAY[Nix] Num";
		
		has qw/x/ => { isa => qw/Nix/ };
	}
	
	
	my $nix = IsIsaNix->new;
	$nix->x(10);
	$nix->x([10, [-2.2, [50]], 20]); ## $nix
	$nix->x([10, [-2.2, []], 20]);
	$nix->x("");					#@ ~ has isa: no Nix for IsIsaNix->x =
	$nix->x([10, [-2.2, []], ""]);	#@ ~ has isa: no Nix for IsIsaNix->x =

	# package IsIsa {
		# use R::App qw/has enum type/;
		
		# enum Virt => [qw/-2004 -2050/];
		# type Natural => "Int" => sub { $_ > 0 };
		
		# has mix => {isa => "Undef HASH{Natural Virt}"};
	# }
	
	# IsIsa->new(mix => undef);
	# IsIsa->new(mix => {r=>10});
	# IsIsa->new(mix => {r=>-10});	#@ ~ err
	# IsIsa->new(mix => {r=>-2050});

=== is

Указывает доступность: **rw**, **ro**, **wo**, **no**.

	package IsExample {
		use R::App qw/has/;
		
		has wo => {is=>"wo"};
		has ro => {is=>"ro", late=>0};
		has no => {is=>"no", late=>0};
		
`ro` и `no` взводит `late`.
		
		has ro_late => {is=>"ro"};
		has no_late => {is=>"no"};
	}

	IsExample->new(ro_late=>10)				#@ ~ ro_late is late
	IsExample->new(no_late=>10)				#@ ~ no_late is late

	IsExample->new(ro => 10)->ro		# 10
	IsExample->new->ro(1)				#@ ~ ro is read only
	
	IsExample->new->no					#@ ~ no is never used
	IsExample->new->no(1)				#@ ~ no is never used
	IsExample->new(no=>10)->{no}		# 10
	
	IsExample->new->wo(10)->{wo}		# 10
	IsExample->new(wo =>10 )->{wo}		# 10
	IsExample->new->wo					#@ ~ wo is write only
	

=== secret

Свойство будет содержатся не в объекте, а в переменной пакета.

	package IsSecret {
		use R::App qw/has/;
		
		has secret => {secret=>1}, qw/unsecret/;
	}

	IsSecret->new(unsecret => 10)->{unsecret}	# 10
	IsSecret->new(secret => 10)->{secret}		## undef
	IsSecret->new(secret => 10)->secret			# 10

Как только объект удаляется - сразу же освобождаются и его секретные свойства.
	
	my $secret = IsSecret->new(secret => 10);
	my $address = int $secret;
	$R::Has::SECRET{$address}{secret}		# 10
	
	undef $secret;
	
	!exists $R::Has::SECRET{$address}		# 1
	
Деструктор можно использовать:

	my $counter = 0;

	package IsSecretDestroy {
		use R::App qw/has/;
		
		has secret => {secret=>1}, qw/unsecret/;
		
		sub DESTROY {
			$counter++;
		}
	}
	
	my $x = IsSecretDestroy->new(secret => 11);
	my $address = int $x;
	$R::Has::SECRET{$address}{secret}		# 11
	$counter # 0
	undef $x;
	$counter # 1
	!exists $R::Has::SECRET{$address}		# 1
	
	
	
=== Наследование и конструктор

Как видно из предыдущих примеров `has` создаёт конструктор.
Если в классе уже есть конструктор, то будет ошибка:

	eval {
		package IsNew {
			use R::App qw/has/;
			
			has qw/xxx/;	
			
			sub new {}
		}
	};
	$@		#~ IsNew->new has not `has` constructor
	
Но её можно отключить указав переменную пакета `our $disable_has_new = 1` перед `has`.

Если `has` видит, что класс наследуется, то она расширит свойства автоматически.

	package Animal {
		use R::App qw/has/;
		
		has qw/frrr/ => { default => "murrr" };
	}
	
	package Cat {
		use base "Animal";
	}
	
	Cat->new->frrr		# murrr

	package Animal2 {
		use R::App qw/has/;
		
		has qw/frrr prrr/ => { default => "murrr" };
	}
	
	package Cat2 {
		use base "Animal2";
		use R::App qw/has/;
		
		has qw/frrr/ => { default => "ku!" };
	}
	
	Cat2->new->frrr	# ku!
	Cat2->new->prrr	# murrr
	
	package Animal3 {
		sub lapa { 10 }
	}
	
	package Cat3 {
		use base "Animal3";
		use R::App qw/has/;
		
		has qw/frrr/ => { default => "new!" };
	}
	
	Cat3->new->frrr	# new!
	
Наследовать можно любыми способами и несколько классов:
	
	package GiperCat {
		our @ISA = qw/Cat2 Cat3/;
		use R::App qw/has/;
		
		has qw/risk/ => { default => "rusi" };
	}
	
	GiperCat->new->risk	# rusi
	GiperCat->new->frrr	# new!
	GiperCat->new->prrr	# murrr
	
В конструкторе устанавливаются переданные параметры, затем дефолтные, затем выполняются 
	
Если `default` или `builder` зависит от других 
	
=== private (TODO)

Указывает, что свойство доступно только в этом классе.

=== protected (TODO)

Указывает, что свойство доступно в этом и классах-наследниках.

	

== before

Добавляет обработчик выполняющийся до функции. Он получает те же аргументы, что и функция и может их изменить, если они не `read-only`. Для изменения аргументов рекомендуется использовать `beforemap`.

	package BeforeEx {
		
		use R::App;
		
		sub executor($$) {
			wantarray? @_: $_[0]+$_[1];
		}
		
		my @in = (555);
		my $wantarray = 50;
		
		before executor => sub {
			@in = @_;
			$wantarray = wantarray;
			123;
		};
		
		executor 5, 6	# 11
		\@in	## [5, 6]
		$wantarray ## ""
		
		@in = ();
		$wantarray = 50;
		
		[executor 5, 6]		## [5, 6]
		\@in	## [5, 6]
		$wantarray # 1
		
		executor 5, 6;
		\@in	## [5, 6]
		$wantarray ## undef
	}

== beforemap


	
== after

Добавляет обработчик выполняющийся после функции.
Он получает те же аргументы, что и функция, которые могли быть уже изменены функцией.

	package AfterEx {
		
		use R::App;
		
		sub executor($$) {
			wantarray? @_: $_[0]+$_[1];
		}
		
		my @in = (555);
		my $wantarray = 50;
		my @ret = (666);
		
		after executor => sub {
			@ret = @R::ret;
			@in = @_;
			$wantarray = wantarray;
			123;
		};
		
		executor 5, 6	# 11
		\@in	## [5, 6]
		$wantarray ## ""
		\@R::ret		## []
		\@ret			## [11]
		
		
		@in = ();
		$wantarray = 50;
		
		[executor 5, 6]		## [5, 6]
		\@in	## [5, 6]
		$wantarray # 1
		\@R::ret		## []
		\@ret			## [5, 6]
		
		executor 5, 6;
		\@in	## [5, 6]
		$wantarray ## undef
		\@R::ret		## []
		\@ret			## []
		
		after executor => sub {
			@R::ret = 25;
		};
		
		sub nowa { 34 }
		after nowa => sub {
			\@R::ret		## []
			executor 6, 5	## 25
			\@in	## [6, 5]
			\@R::ret		## []
			\@ret			## [11]
		};
		
		nowa();
		
Так как nowa выполнилась в невозвращаемом контексте (`wantarray` == `undef`), то `@R::ret` будет пуст.

	}

== araund

Получает в `$R::sub` замещаемую функцию и может делать с ней что заблагорассудится.

	package AraundEx {
		
		use R::App;
		
		my $counter = 0;
		
		sub executor($$) {
			$counter++;
		}
		
		my @in = (555);
		my $wantarray = 50;
		
		araund executor => sub {
			@in = @_;
			$wantarray = wantarray;
			
			$R::sub->(@_) if defined wantarray;
			
			123;
		};
		
		executor 5, 6	# 123
		\@in	## [5, 6]
		$wantarray ## ""
		$counter	# 1
		
		@in = ();
		$wantarray = 50;
		
		[executor 5, 6]		## [123]
		\@in	## [5, 6]
		$wantarray # 1
		$counter	# 2
		
		executor 5, 6;
		\@in	## [5, 6]
		$wantarray ## undef
		$counter	# 2
	}
	
== app

`$app` и `app` возвращают объект `R::App`, который подгружает сервисы по требованию.

	app->framework->name	# rubin-forms
	app->project->name		# rubin-forms
	ref app->ini			# R::Ini

Сервисы прописываются в ini-файле.
	
	{
		local $app->{ini};
		
		$app->{ini} = {};
	};
	
== Импорт-экспорт

	# package ExportEx {
		# use R::App export => [-mygroup => [qw/love died/], qw/undied $xchange @after %before/], default => [qw/-mygroup/];
		
		# our @after = qw/1 2 3/;
		# our %before = (a => 10, b => 20);
		# our $xchange = 50;
		
		# sub love { 10 }
		# sub died { -10 }
		# sub undied { 6 }
	# }
	
	# package ImportExDefault {
		# use ExportEx;
		
		# love	# 10
		# died	# -10
	# }
	
	# package ImportEx {
		# use ExportEx qw/-mygroup undied/;
		
		# love()		# 10
		# died()		# -10
		# undied()	# 6
	# }
	
	# package ImportNoEx {
		# use ExportEx qw//;
		
		# love()	#@ ~ err
	# }
	
	
	# package ImportNothing {
		# use ExportEx qw//;
		
		# love()	#@ ~ err
	# }
	
	# package ImportNo {
		# use ExportEx qw/%after @before/;
		
		# \our @after		## [1,2,3]
		# \our %before	## { a => 10, b => 20 }
		
		# no ExportEx qw/%after/;
		
		# \our @after		## []
		# \our %before	## { a => 10, b => 20 }
	# }
	
== Прочие импортируемые подпрограммы
	
=== Tour

Обходит все элементы рекурсивно. Не заходит в объекты.

В `$_` записывается хэш или массив.
В `$a` - ключ хэша или массива.
В `$b` - значение ключа в хеше или массиве.

Ничего не возвращает.

	my @flat;

	Tour {
		push @flat, "$a=$b" if !ref $b; 
	} 1, [
		{a=>"b", z=>["x", "y"]},
	], 3, [6];

	join ",", @flat			# 0=1,2=3,0=6,a=b,0=x,1=y
	
Тоесть обход осуществляется "в ширину": вначале обходится верхний уровень, а потом - следующий.

Обход ключей хешей осуществляется в алфавитном порядке.

`$b` можно изменять.

	my $data = [0, [2,3], 4];
	
	Tour { $b = 10 if $a==1 } $data;
	
	$data		## [0, 10, 4]
	
При этом обход продолжается для изменённого значения.

=== guard

Импортируется из модуля `Guard`. Возвращает для кода значение, как только оно исчезает из поля видимости, код срабатывает.

	my @x;

	my $val = guard { push @x, 6 };

	push @x, 3;
	
	undef $val;
	
	\@x	## [3, 6]

=== max

Выбирает максимальный элемент. Использует числовое сравнение.

	#max qw/1 3.1 3.05 2 -5/ # 3.1