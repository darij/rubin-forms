= Шаблонизатор Mid

== Инициализация

[init]
	
	use common::sense;
	use R::App;


	$app->man->conf;
	
	my $mid = $app->templateMid(inc => ["var/.miu/root/mid"], ext => "mid", show_code=>1);
	
[test]
	
== Вставка данных

Шаблоны имеют синтаксис подобный шаблонизаторам Django и Twig.

@@mid/hello//hi.mid
	hi, {{ name }}!
[test]
	
	$mid->render("hello/hi", { name=>"Doka" });		# hi, Doka!
	
== Наследование

@@mid/Layout.mid
	<div>{% block content %}{% endblock content %}</div>
@@mid/True.mid
	{% extends "Layout" %}{% block content %}123{% endblock content %}
[test]

	$mid->render("True");	# <div>123</div>
	
== Блоки

@@mid/A.mid
	<div>{% block news %}123{% endblock news %}</div>
@@mid/B.mid
	{% extends "A" %}{% block news %}456{% endblock %}
[test]

	$mid->render("A");	# <div>123</div>
	$mid->render("B");	# <div>456</div>

== if

@@mid/If.mid
	{% if a < 10 %}10{% elseif a < 20 %}20{% else %}30{% endif %}
[test]

	$mid->render("If", {a => 1});	# 10
	$mid->render("If", {a => 11});	# 20
	$mid->render("If", {a => 21});	# 30

== IfChange

@@mid/IfChange.mid
	{% for i in a %}{% ifchange i %}{{ i }}{% endifchange %}{% endfor %}
@@mid/IfChangeElse.mid
	{% for i in a %}{% ifchange i %}{{ i }}{% else %}_{% endifchange %}{% endfor %}
@@mid/IfChangeElseIf.mid
	{% for i in a %}{% ifchange i %}{{ i }}{% elseif i<6 %}_{% endifchange %}{% endfor %}
[test]

	$mid->render("IfChange", {a => [1,1,1,5,5,6,6,6]})			# 156
	$mid->render("IfChangeElse", {a => [1,1,1,5,5,6,6,6]})		# 1__5_6__
	$mid->render("IfChangeElseIf", {a => [1,1,1,5,5,6,6,6]})	# 1__5_6
	
== for

`for` может работать с массивами, хешами и итераторами.

@@mid/ForIn.mid
	{% for a in arr %}{{ a }}|{% endfor a %}
[test]

	$mid->render("ForIn", {arr => [1,2,3]});	# 1|2|3|
	
@@mid/ForOf.mid
	{% for k, v in arr %}{{ k }}={{ v }}|{% endfor k %}
[test]

	$mid->render("ForOf", {arr => {a=>1,b=>2}});	# a=1|b=2|

@@mid/ForEmpty.mid
	{% for a in arr %}x{% empty %}y{% endfor a %}
[test]

	$mid->render("ForEmpty", {arr => [1,2]});	# xx
	$mid->render("ForEmpty", {arr => []});		# y
	
	
== defer

`defer` выполнится в конце функции и вставит на своё место свой рендер.

@@mid/defer-layout.mid
	<html>
	<head>
		<script>
		{% defer %}-{{ def.acc }}-{% enddefer %}
		</script>
	</head>
	<body>
		{% block content %}{% endblock content %}
	</body>
	</html>
@@mid/defer-page.mid
	{% extends "defer-layout.mid" %}
	{% block content %}-{{ def.acc }}-{% endblock content %}	
[test]

	package DeferX {
		sub acc { $_[0]{acc}++ }
	}

	my $defer_x = bless {}, "DeferX";
	$mid->render("defer-page", { def => $defer_x });	#~ (?s)-1-.*-0-
	
	
== with

Устанавливает переменные для использования в блоке.

@@mid/with.mid
	{{ a }}{% with a=10 b=20 %}-{{ a }}+{{ b }}-{% endwith a %}{{ a }}
[test]

	$mid->render("with.mid", {a => 123})	# 123-10+20-123

== include

Вставляет шаблон.

@@mid/for-include.mid
	-{{ a }}-{{ b }}-
@@mid/includes.mid
	/{% include "for-include.mid" %}/
[test]

	$mid->render("includes.mid", {a => 123})	# /-123--/

А так с дополнительными данными:
	
@@mid/includeser.mid
	+{% include "for-include.mid" with a=10 b=20 %}+
[test]

	$mid->render("includeser.mid", {a => 123})	# +-10-20-+


== set

Присваивает переменной свой блок.

@@mid/set.mid
	+{% set A %}<br>{% endset A %}%{{ A|raw }}x
[test]

	$mid->render("set.mid")		# +%<br>x

	
== source

Вставляет файл. Путь задаётся относительно каталога мультимедиа.

Зададим каталог мультимедиа:

	$app->sitemap->root->{root} = $app->file("var/.miu/root/html");

@@html/x.txt
	123
@@mid/source.mid
	-{{ file|source }}-
[test]
	
	$mid->render("source.mid", {file => "x.txt"});	# -123-
	
== Данные
	
=== Данные по ключу, методу и скобки

@@mid/key_and_method.mid
	-{{ x:y }}-{{ x.y }}-{{ x{"p"}[1]{"t"} }}-
[test]
	
	package A {
		sub y { 20 }
	}
	
	$mid->render("key_and_method", {x => bless {y=>10, p=>[0, {t=>30}]}, "A"});	# -10-20-30-

=== Массивы

@@mid/data-array.mid
	{% for i in [1..3, 9] %}{{ i }}-{% endfor %}
[test]
	
	$mid->render("data-array.mid");	# 1-2-3-9-

=== Хеши

@@mid/data-hash.mid
	{% for k, v in {a: 1, b: "x"} %}.{{ k }}={{ v }}{% endfor %}
[test]
	
	$mid->render("data-hash.mid");	# .a=1.b=x

	
== Фильтры

Фильтры - любые функции из пакета `R::Template::Filters`.

=== default

@@mid/filter-default.mid
	-{{ a|default:10 }}-{{ b|default:20 }}-
[test]

	$mid->render("filter-default", {b=>30})	# -10-30-

=== raw

@@mid/123-444.mid
	<div>{{ serve }}</div>
[test]

	$mid->render("123-444", {serve => "<ttt>"});	# <div>&lt;ttt&gt;</div>

@@mid/123-555.mid
	<div>{{ serve|raw }}</div>
[test]

	$mid->render("123-555", {serve => "<ttt>"});	# <div><ttt></div>

	
=== json

@@mid/f-json.mid
	{{ serve|json|raw }}
[test]

	$mid->render("f-json", {serve => {a=>{b=>[1,2]}}});	# {"a":{"b":[1,2]}}

=== lower и upper

@@mid/f-lower-&-upper.mid
	{{ serve|lower }} {{ serve|upper }}
[test]

	$mid->render("f-lower-&-upper", {serve => "Hello World!"});	# hello world! HELLO WORLD!

=== size

@@mid/f-size.mid
	{{ a|size }} {{ b|size }} {{ c|size }} -{{ d|size|defined }}-
[test]

	$mid->render("f-size", {a => {a=>1, b=>2}, b=>[1..6], c=>"Hi!", d=>\123});	# 2 6 3 --

=== length

@@mid/f-length.mid
	{{ a|length }}
[test]

	$mid->render("f-length", {a => "Hi!"});	# 3
	
=== defined

@@mid/f-defined.mid
	-{{ a|defined }}-{{ b|defined }}-
[test]

	$mid->render("f-defined", {a => "Hi!"});	# -1--

