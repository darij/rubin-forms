= Коннект к базе

Объект коннект представляет собой низкий уровень доступа к базе. Он используется ORM.

== Инициализация

Подключаем наш фреймворк:

[init]

	use common::sense;
	use lib "lib";
	use R::App;


Зададим конфигурацию:

	$app->man->configure(log=>0);

[test]


== Информация о структуре базы

== SQL-запросы

* do(sql) - выполняет модифицирующий запрос

	$app->connect->do("create table N(id int primary key auto_increment, x int)");		# 0E0

* add - добавляет строку

	$app->connect->add("N" => {x => 67})	##== $app->connect
	
* append - добавляет строку и возвращает её `pk`
	
	$app->connect->append("N" => {x => 69})	# 2
	
* query - запрашивает строку в виде массива, а в скалярном контексте один колумн

	$app->connect->query("N" => "x" => {id => 1})		## 67
	join ", ", $app->connect->query("N" => "*" => 1)	# 1, 67

* query_ref - запрашивает строку в виде хеша
	
	$app->connect->query_ref("N" => "*" => {id => 1})		## {id=>1, x=>67}
	
* query_all - запрашивает все строки
	
	$app->connect->query_all("N" => [qw/id x/] => {id => [1..2]})	## [{id=>1, x=>67}, {id=>2, x=>69}]
	$app->connect->query_all("N" => "id, x" => "id in (1,2)")		## [{id=>1, x=>67}, {id=>2, x=>69}]

* query_col - запрашивает один колумн
	
	$app->connect->query_col("N" => "x" => {id => [1..2]})	## [67, 69]

* query_array - запрашивает строки в виде массивов
	
	$app->connect->query_array("N" => "*")		## [[1,67], [2,69]]
	my @A = $app->connect->query_array("N" => "*");
	\@A ## [[1,67], [2,69]]

	
== Транзакции

	my $c = $app->connect;

	$c->do("create table A(id int)");		# 0E0

Транзакция создаётся и все sql-запросы отправляются на транзакцию.
При этом только когда произойдёт `commit` транзакция запишет всё, что у неё накопилось.

	my $guard_transaction = $app->connect->begin;
	$c->{transaction}				## $c->dbh
	$c->{transaction}{AutoCommit};  # 
	$c->add("A", {id=>10});
	$c->add("A", {id=>20});
	$c->commit;
	undef $guard_transaction;
	
	$c->{transaction}  		## undef
	$c->dbh->{AutoCommit}	# 1
	
	$app->connect->query_col("A", "id")	## [10, 20]
	
Если не было `commit`, то произойдёт автоматом `rollback` на деструкторе.

	my $guard_transaction = $app->connect->begin;
	$c->{transaction}{AutoCommit};  # 
	$c->add("A", {id=>30});
	$c->add("A", {id=>40});
	$c->{transaction}				## $c->dbh
	undef $guard_transaction;
	
	$c->{transaction}  		## undef
	$c->dbh->{AutoCommit}	# 1
	
	$app->connect->query_col("A", "id")	## [10, 20]
	$c->add("A", {id=>40});
	$app->connect->query_col("A", "id")	## [10, 20, 40]
	
== DDL

=== create

Cоздаёт таблицу, столбец, индекс или ключ.

	my $c = $app->connect;

	my $author = $c->tab("author");
	
Пока автора нет в базе.
	
	$author->exists		# 
	
	$author->remark("hi!");
	
	$author->add(
		$author->integer("id")->primary->increments->remark("айдишник"),
		$author->string("name")->need->remark("имя автора"),
		$author->bool("sex")->remark("М/Ж"),
		$author->index("idx_name_sex", "name", "sex")->remark("индекс"),
	);
	
	# # для sqlite
	# my @sql = ("CREATE TABLE author (
	# id INTEGER PRIMARY KEY AUTOINCREMENT /* айдишник */,
	# name STRING NOT NULL /* имя автора */,
	# sex BOOLEAN /* М/Ж */
	# ) /* hi! */",
	# "CREATE INDEX idx_name_sex ON author (name, sex) /* индекс */",
	# );
	
	# [$author->show_create];		## \@sql
	
	$author->create->exists		# 1

	my $book = $c->tab("book");
	$book->add(
		$book->integer("id")->primary->increments->remark("айдишник"),
		$book->string("name")->need->remark("название книги"),
		$book->integer("author_id")->need->remark("ссылка на автора"),
		$book->fk("fk_author", "author_id")->ref("author")->remark("ссылка на автора"),
	)->remark("книга")->create;
	
Получим информацию:

	$c->tab("book")->load->remark								# книга
	$c->tab("book")->col("name")->load->remark 			# название книги
	$c->tab("book")->col("author_id")->load->remark 	# ссылка на автора
	
	
Теперь попробуем переименовать столбец.
	
	$c->tab("book")->col("name")->exists 			# 1
	$c->tab("book")->col("name")->rename("name1");
	
	$c->tab("book")->col("name")->exists			#
	$c->tab("book")->col("name1")->exists			# 1
	
И изменим его:

	$c->tab("book")->col("name1")->load->type("int")->default("10")->remark("что-то странное")->modify;
	
	my $name1 = $c->tab("book")->col("name1")->load;
	$name1->default		# 10
	$name1->type		# int
	$name1->remark		# что-то странное
	
	# msg $c->get_info;
	# msg $c->get_tab_info;
	# msg $c->get_index_info;
	# msg $c->get_fk_info;
	
Можно создать другую таблицу и загрузить её определение.
	
	#$c->tab("")->;
	
	# $app->connect->create("test_tab.col_a", "int default 0")
	# $app->connect->create("test_tab.col_a", "int default 0")
	# $app->connect->create("test_tab:idx_a", "col1", "col2")
	# $app->connect->create("test_tab:idx_a:unique", "col1")
	# $app->connect->create("test_tab:fk_a:fk", "tab1", "col1")
	# $app->connect->create("test_tab:fk_b:fk", "tab2") - по умолчанию - колумн id
	
== Acc

Данный интерфейс предназначен для выполнения коллекции запросов DDL и SQL вперемешку.
Он имеет следующие операции:

1. show - показать SQL или DDL запрос
2. save (store, sync) - сохранить
3. load - загрузить. Загружает данные при SQL или выводит `show create` для таблиц и отображений, а для  при DDL
4. rm (erase, drop) - удаляет сущность
5. val - возвращает/ устанавливает значения

$app->connect->acc(":test_base")->save->use->show		# CREATE DATABASE test_base
$app->connect->acc("ex.", "a int?")->save->show		# CREATE TABLE ex()
	
	
