= *PerlX* - аналог *jsx* для *perl* (PLANNED)

== Инициализация фреймворка

[init]

	use common::sense;
	use lib "lib";
	use R::App qw/$app msg msg1/;
	
	$app->man->conf;
	
[test]


== Компонент preact

@@A.plx
	# /
	package A {
		#use base PReact::Component;
	
		sub new {
			my ($cls, $prop) = @_;
			$cls->SUPER::new($prop);
		}
		
		sub render {
			my ($self, $prop, $state) = @_;
			
			<ul>
				{map {<li>$_</li>} @{$prop->{list}}}
			</ul>
		}
	}

	1;
[test]

	1 # 1

Регистрируем компиляцию файлов c расширением plx на лету:
	

	# BEGIN { $app->perlx->register }
	
	# require "A.plx";
	
Откомпилируем наш файл из perl в :

	#$app->perlx->cc("var//.plx", "");

Названия тегов начинающиеся на большую букву расцениваются как название класса:
	
	#$app->perlx->eval("<A ={}></A>")			# 
