= Xf

== Инициализация

Подключаем наш фреймворк:

[init]
	
	use lib "lib";
	use common::sense;
	use R::App;
	
	my $PRJ_CLEARER = $app->man->project("14-xf", log=>0, logsqlinfo=>0);
	
	$app->file("html")->mkdir;
	
[test]


== Введение

== R::Xf

Шаблонизатор.

=== Вставки

	$app->file("html/ex-ins.xf")->mkpath->write('hi! `$x:y` $x:b[0] $x{b}:1');
	$app->xf->render("html/ex-ins.xf", {x=>{y=>10, b=>[10,30]}, b=>"b"})		# hi! `10` 10 30
	
	$app->file("html/ex-ins-1.xf")->write("x{{ a:b:c < 20 }}y");
	$app->xf->render("html/ex-ins-1.xf", {a => {b=>{c=>10}}})		# x1y
	
Операторы сравнения чисел: <, >, <=, >=, ==,  !=
Операторы сравнения строк: lt, gt, le, ge, eq, ne
	
	$app->file("html/ex-ne.xf")->write("x{{ a:b:c != 20 }}y");
	$app->xf->render("html/ex-ne.xf", {a => {b=>{c=>10}}})		# x1y

	$app->file("html/ex-sless.xf")->write("x {{ a:b:c lt '10y'? 'yea': 'nou' }} y");
	$app->xf->render("html/ex-sless.xf", {a => {b=>{c=>10}}})		# x yea y
	
Вызов методов на объектах:
	
	package A {
		sub hi { shift; join ", ", @_ }
		sub hash {+{x=>10, y=>20}}
		sub array {[30, 40]}
	}
	
	$app->file("html/ex-obj.xf")->write('x $x.hi(y+1, "(<&\'\"") y');
	$app->xf->render("html/ex-obj.xf", {x=>bless({}, "A"), y=>5})		# x 6, (&lt;&amp;&#39;&quot; y
	
	$app->file("html/ex-obj-hash.xf")->write('x $x.hash{"x"} $x.hash(){"y"} y');
	$app->xf->render("html/ex-obj-hash.xf", {x=>bless({}, "A")})		# x 10 20 y
	
	$app->file("html/ex-obj-array.xf")->write('x $x.array[0] $x.array()[1] y');
	$app->xf->render("html/ex-obj-array.xf", {x=>bless({}, "A")})		# x 30 40 y
	
Вызов функций:

	$app->file("html/ex-fn.xf")->write('x {{ join(",", sort(3,1,2)) }} y');
	$app->xf->render("html/ex-fn.xf", {})		# x 1,2,3 y
	
Вставки, как мы видели, экранируются. Чтобы вставить значение без экранирования:

	$app->file("html/ex-noesc.xf")->write('x $x.RAW y');
	$app->xf->render("html/ex-noesc.xf", {x=> "<b>hi!</b>"})		# x <b>hi!</b> y
	
Внутри строк в подстановках так же работают подстановки:

	$app->file("html/ex-string.xf")->write('x {{ a . " \'$a\'" .RAW }} y');
	$app->xf->render("html/ex-string.xf", {a => "i'am hi!"})		# x i'am hi! 'i'am hi!' y

	
== Блоки
	
	#$app->file("html/ex-sub.xf")->write('x {{  .RAW }} y');
	#$app->xf->render("html/ex-sub.xf", {a => "i'am hi!"})		# x i'am hi! 'i'am hi!' y

=== app

Для специальных функций через `app` создаётся объект и:

	$app->file("html/ex-number-format.xf")->write('$app.magnitudeNumber(1024)');
	$app->xf->render("html/ex-number-format.xf", {})		# 1 024
	
=== Условие

Тег вставляется, если условие выполняется.

	$app->file("html/ex-if.xf")->write("<div if='z>10' any=\"123\" attr><b if=x>10</b>20</div>");
	$app->xf->render("html/ex-if.xf", {z => 11})		# <div any="123" attr>20</div>
	
Вставляется содержимое тега, если выполняется условие:

	$app->file("html/ex-in-if.xf")->write("<div in-if='z>10' any=\"123\" attr>20</div>");
	$app->xf->render("html/ex-in-if.xf", {z => 11})		# <div any="123" attr>20</div>
	$app->xf->render("html/ex-in-if.xf", {z => 10})		# <div any="123" attr></div>

=== Цикл
	
	$app->file("html/ex-by.xf")->write("<div by=z as=i id=x-\$i><noop if=\"i == 11\">-</noop>{{ i+1 }}</div>");
	$app->xf->render("html/ex-by.xf", {z => [11, 12]})		# <div id=x-11>-12</div><div id=x-12>13</div>
	
`<noop ...>` и `</noop>` не вставляется, но вставляется содержимое, так же отрабатывают специальные атрибуты.
	
В `by` можно передавать и хеши:
	
	$app->file("html/ex-by-if.xf")->write("<div by=z key-as=k val-as=v if='hoop != 11' id=x-\$v>\$k-\$loop</div>");
	$app->xf->render("html/ex-by-if.xf", {z => {x=>11, y=>12}})	# <div id=x-12>y-y</div>
	
`in` аналогичен `by`, но повторяет содержимое тега:

	$app->file("html/ex-in.xf")->write("<p in=z key-as=k val-as=v in-if='v != 11'>\$k=\$v \$loop=\$hoop</p>");
	$app->xf->render("html/ex-in.xf", {z => {f=>10, x=>11, y=>12}})	# <p>f=10 f=10y=12 y=12</p>

=== Форма

В инпуты форм value вставляется из имя_формы.имя_инпута.

	$app->file("html/ex-form.xf")->write("
		<form name=myform>
			<input name='login' value='Паша'>
			<input name=passwd>
		</form>
	");
	$app->xf->render("html/ex-form.xf", {myform => {login=>"Зло", passwd=>"Диско"}})	#~ (?s)Зло.*Диско
	$app->xf->render("html/ex-form.xf", {myform => {login=>"Зло", passwd=>"Диско"}})	#!~ Паша
	$app->xf->render("html/ex-form.xf", {})	#~ Паша

Текстареа:

	$app->file("html/ex-form-textarea.xf")->write("
		<form name=f>
			<textarea name=t1></textarea>
			<textarea name=t2>123</textarea>
		</form>
	");

	$app->xf->render("html/ex-form-textarea.xf", {f=>{}})			#~ 123
	$app->xf->render("html/ex-form-textarea.xf", {f=>{t2=>""}})		#!~ 123
	$app->xf->render("html/ex-form-textarea.xf", {f=>{t2=>undef}})	#~ 123
	$app->xf->render("html/ex-form-textarea.xf", {f=>{t1=>456, t2=>789}})	#~ (?s)456.*789
	
	
=== Атрибут include

Можно включать другие шаблоны.

	$app->file("html/ex-for-include.xf")->write('x $CONTENT y');
	$app->file("html/ex-include.xf")->write("<p include='html/ex-for-include.xf'>123</p>");
	$app->xf->render("html/ex-include.xf")		# <p>x 123 y</p>

=== Атрибут `head`. В <head>

Добавляет тег в переменную `HEAD`, которую нужно вставить в лайоут.

Даже если шаблон будет выполнен несколько раз в переменную HEAD тег попадёт единожды.

	{
	local $Coro::current->{request} = $app->rsgiRequest->new;

	$app->file("html/ex-head-include.xf")->write('__{{ CONTENT }}__<em head>$CONTENT</em>');
	$app->file("html/ex-head.xf")->write("
	<p include='html/ex-head-include.xf'>p</p>1
	<i head>i</i>2
	<p head>P</p>3
	<p include='html/ex-head-include.xf'>P</p>4
	");
	$app->xf->render("html/ex-head.xf")		# \n<p>__p__</p>1\n2\n3\n<p>__P__</p>4\n
	
	
	$Coro::current->{request}->HEAD		# <em>p</em><i>i</i><p>P</p>
	}
	
=== Комментарии

В комментариях подстановки не осуществляются и они удаляются из вывода.

	$app->file("html/ex-comment.xf")->write("<p>123</p><!-- \$x\n\n --><i>456</i>");
	$app->xf->render("html/ex-comment.xf", {x => 10})	# <p>123</p><i>456</i>
	
=== Атрибут `scenario`. Сценарии

Сценарии удаляются из вывода.

	$app->file("html/ex-scenario.xf")->write("<p>123</p><script scenario> ... </script><i>456</i>");
	$app->xf->render("html/ex-scenario.xf", {x => 10})	# <p>123</p><i>456</i>
	

	
=== Атрибуты `src` и `href`, а так же текст `url(...)`
	
В данных  происходит подстановка `?_mtime=...`, если файл будет на диске.
	
=== Атрибут sub. Блок

Блоки - шаблоны в других шаблонах, но могут быть вызваны и самостоятельно.

	$app->file("html/ex-sub.xf")->write("<p>123<i sub=six>\$x</i>456</p>");
	$app->xf->render("html/ex-sub.xf", {x => 10})	# <p>123<i>10</i>456</p>
	#$app->xf->get("html/ex-sub/.six", {x => 10})	# <i>10</i>
	
=== Атрибут fun. Функция

	$app->file("html/ex-fun.xf")->write("{{ six({x: x}).RAW }}<p>123<i fun=six>\$x</i>456</p>");
	$app->xf->render("html/ex-fun.xf", {x => 10})	# <i>10</i><p>123456</p>
	
=== ajax-запросы

	$app->file("html/ex-ajax.xf")->write('<p>123
	<i sub=A by=x as=i id=$i>-$i-</i>
	456</p>
	<script>
		class A extends Widget {
			constructor(div) { this.div = div; div.click(() => this.resix()) }
			resix() {
				var id = div.attr("id")
				// результат будет загнан в div.html(data), если не менять метод Widget.ok
				<% A({ i: id + 33 }) %>
			}
			seven() {
				var id = div.attr("id")
				<% { i: id + 33 } %>
				// выполнится в блоке .done, вместо Widget.ok. Соответственно доступны:
				//  data - {i: 43}
				//  reason - OK
				//  xhr - объект xhrHttpRequest
				this.div.html( json.i )
			}
			set() {
				var p = "html/mmm.xf", w = 123
				// а при таком раскладе нужно самому обрабатывать результат
				<% app.file(p).write(w); 10 %>.always(data => say("hI!", data))
			}
			hello() {
				var p = "html/mmm.xf", w = 123
				// двоеточие на конце говорит о том, что и результат и ошибка будет помещена в виджет:
				//  будет вызван Widget.ok или Widget.error_ok
				<% app.file(p).write(w); 10 %>:
			}
			hi() {
				var p = "html/mmm.xf", w = 123
				// двоеточие на конце говорит о том, что и результат и ошибка будет обрабатываться одинаково:
				//  data - тело ответа
				//  reason - OK или ошибка
				//  xhr - объект xhrHttpRequest
				<% app.file(p).write(w); 10 %>:
				say("hI!", data)
			}
			elsewhere() {
				var p = "html/mmm.xf", w = 123
				// <%-%> - указывает блок для обработки ошибки
				<% app.file(p).write(w); 10 %>
				say("done!", data)
				<%-%>
				say("catch!", data)
			}
		}
	</script>');
	
	#msg1 $app->xf->render("html/ex-ajax.xf", {x => [10, 20]});
	
	$app->xf->render("html/ex-ajax.xf", {x => [10, 20]})	#~ <p>123\n<i id=10>-10-</i><i id=20>-20-</i>\n456</p>
	
В `<% ... %>` находятся контроллеры, вместо них в **javascript** подставляется `this.fetch("path~class~method", {a,b,c})`, который должен возвращать промис.

	$app->xf->render("html/ex-ajax.xf", {x => [10, 20]})	#~ this\.fetch\("/ex-ajax/~a~resix/", \{id\}\)\.then.*ok.*fail
	$app->xf->render("html/ex-ajax.xf", {x => [10, 20]})	#~ this\.fetch\("/ex-ajax/~a~seven/", \{id\}\)\.always
	$app->xf->render("html/ex-ajax.xf", {x => [10, 20]})	#~ this\.fetch\("/ex-ajax/~a~set/", \{p, w\}\)\.always\(data => say\("hI!", data\)\)\s*\}
	$app->xf->render("html/ex-ajax.xf", {x => [10, 20]})	#~ this\.fetch\("/ex-ajax/~a~hello/", \{p, w\}\)\.then.*ok.*error_ok
	$app->xf->render("html/ex-ajax.xf", {x => [10, 20]})	#~ this\.fetch\("/ex-ajax/~a~hi/", \{p, w\}\)\.always.*twix
	$app->xf->render("html/ex-ajax.xf", {x => [10, 20]})	#~ this\.fetch\("/ex-ajax/~a~elsewhere/", \{p, w\}\)\.always.*always
	
	
	$app->xf->filemap->get("html/ex-ajax/~a~resix/")?1:0		# 1
	$app->xf->filemap->get("html/ex-ajax/~a~seven/")?1:0		# 1
	$app->xf->filemap->get("html/ex-ajax/~a~set/")?1:0			# 1
	$app->xf->filemap->get("html/ex-ajax/~a~hello/")?1:0		# 1
	$app->xf->filemap->get("html/ex-ajax/~a~hi/")?1:0			# 1
	$app->xf->filemap->get("html/ex-ajax/~a~elsewhere/")?1:0	# 1
	
=== Виджеты

	$app->file("html/ex-widget.xf")->write('
	<div widget="A">$A:id $A.this_id.RAW</div>
	<i sub="opt">$id</i>
	<script>
	class A extends Widget {
		<% @id = id // 10 %>
		this_id() {
			let id = 10
			<% opt({id: id}) %>
		}
	}
	</script>');
	
	$app->xf->render("html/ex-widget.xf")				#~ <div>10 <i>10</i></div>
	$app->xf->render("html/ex-widget.xf", {id => 20})	#~ <div>20 <i>20</i></div>
	
	$app->xf->filemap->get("html/ex-widget/~a~this_id")?1:0		# 1

=== Подъязык

==== Переводы строк считаются за `;`

	$app->file("html/ex-lang-newline.xf")->write("{{
		x = if 10 then 20 fi
		x + 6
	}}");
	$app->xf->render("html/ex-lang-newline.xf")	# 26

==== Комментарии

==== Конструкция `if`

	$app->file("html/ex-lang-if.xf")->write('<% x = if 10 then 20 fi %>$x');
	$app->xf->render("html/ex-lang-if.xf")	# 20
	
	$app->file("html/ex-lang-if-elif.xf")->write('{{ if 0 then 20 elif 1 then 40 fi }}');
	$app->xf->render("html/ex-lang-if-elif.xf")	# 40
	
==== Цикл `foreach`

