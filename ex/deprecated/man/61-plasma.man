= Построитель лексико-грамматико-семантических анализаторов

== Инициализация фреймворка

[init]

	use common::sense;
	use lib "lib";
	use R::App qw/$app msg msg1/;
	
	$app->man->conf;
	
	my $res;
	
[test]


== Анализатор

Создаём анализатор языка и строки:

	my $blaster = $app->plazma->new("blaster");
	my $string = $app->plazma->new("string");
	
Лексемы:
	
	$blaster->lex(
		"\n"=>qr/\n/,
		number => qr/(\d+(\.\d+)?|\.\d+)(E[+-]\d+)/ni,
        separator => qr/[a-z_]\w*:/i,
		word => qr/[a-z]\w*/i,
	);
	
Пропускаем:
	
	$blaster->space_lex(qw/\s+/);
	
Добавляем правила грамматики:
	
	$blaster->rules("

		mul: n * mul | n	", sub {}, "
		e:   mul + e | mul
		
		onecond: e <= e	|
				 e >= e	|
				 e <  e	|
				 e >  e	|
				 e =  e	|
				 e <> e
			  
		oreo:	e and 
			  
		cond:	oreo or cond |
				e
			  
		if:   word.if	cond  word.then   stmt
		
		stmt:	if 
		
		line: stmt \n line | stmt $
		
	");
	
	$blaster->rule(so		=> "e <= e" => sub {});
	$blaster->rule(so		=> "e >= e" => sub {});
	$blaster->rule(so		=> "e <> e" => sub {});
	$blaster->rule(so		=> "e =  e" => sub {});
    
    val:
            number
            word
            
    
    pow:
            val ^ pow
            val
    mul:
            pow * mul
            pow / mul
            pow
            
    e:
            mul + e
            mul - e
            mul
            
    onecond:
            e <= e <= e
            e <= e <  e
            e <  e <= e
            e <  e <  e
            e >= e >= e
            e >= e >  e
            e >  e >= e
            e >  e >  e
            e <= e
            e >= e
            e <  e	
            e >  e	
            e =  e	
            e <> e
          
    ands:
            onecond w.and ands
            onecond
          
    cond:
            ands w.or cond
            ands
          
    if:
            w.if	cond  w.then   line w.else    line
            w.if	cond  w.then   line
            w.if	cond  w.then   stmt
    
    let:
            var = e
    
    stmt:
            if
            for
            repeat
            case
            let
            e
    
    line:
            class
            sub
            
    
    lines:
       line \n lines
       line
    
	");
	
Обработчик всех правил, у которых не будет собственных обработчиков:

    $blaster->def(sub {
        
    });
    
Добавляем правила:
    
	$blaster->rule(cond => "e <= e" => sub {});
	$blaster->rule(cond => "e >= e" => sub {});
	$blaster->rule(cond => "e <> e" => sub {});
	$blaster->rule(cond => "e = e" => sub {});
	$blaster->rule(if => "word.if cond word.then stmt" => sub {  });
	
	$blaster->rule(coNot	=> "word.NOT so" => sub { +{ x=>"!($_[2])" } });
	$blaster->rule(coNot	=> "so" => sub { +{ x=>$_[1] } });
	$blaster->rule(coAnd	=> "coNot word.AND coand" => sub { +{ x=>"!($_[1])" } });
	$blaster->rule(cond		=> "coAnd word.OR cond" => sub {});
	
	$blaster->rule(if		=> "word.IF cond word.THEN stmt" => sub {  });
	
	$blaster->rule(lines	=> "" => sub {  });
	
Разбор всегда начинается с последнего правила.
	
Запускаем:
	
	my $text = "
		if 
	";
	
	$blaster->compile($text, file=>"Antuan.bas");
	
	$blaster->error;		# 
	
	$blaster->code;			# ...
    
    
Файл - и есть класс.
Имя файла без расширения - название класса.
Путь к файлу - название класса
	