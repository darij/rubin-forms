= Миграции

== Инициализация

Подключаем наш фреймворк:

[init]
	
	use lib "lib";
	use common::sense;
	use R::App;
	
Зададим конфигурацию:

	my $PRJ_CLEARER = $app->man->project("06-migrate", log=>0, logsqlinfo=>0);
	
[test]


== Сравнение

	my $c = $app->connect;

	my $author = $c->tab("author");
	
	$author->add(
		$author->integer("id")->primary->increments->remark("айдишник"),
		$author->string("name")->need->remark("имя автора"),
		$author->bool("sex")->remark("М/Ж"),
		$author->index("idx_name_sex", "name", "sex")->remark("индекс"),
		$author->integer("lucky_book_id")->remark("самая удачная книга автора"),
		$author->fk("fk_author_book", "lucky_book_id")->ref("book")->remark("дополнительный ключ"),
	)->remark("автор");
	
	my $book = $c->tab("book");
	$book->add(
		$book->integer("id")->primary->increments->remark("айдишник"),
		$book->string("name")->need->remark("название книги"),
		$book->integer("author_id")->need,
		$book->fk("fk_book_author", "author_id")->ref("author")->remark("ссылка на автора"),
		$book->unique("unq_name", "name")->remark("название книги - уникально"),
	)->remark("книга");
	
	
Сравнение выдаёт код, который должен привести одну схему к другой.
	
	$author->diff(undef);		# $c->tab('author')->drop;\n
	
Если имя сущности - `undef`, то она считается несуществующей.
	
	my $pk = $author->added_col("id");
	
	$author->col(undef)->diff($pk)		#  $c->tab('author')->col('id')->type('integer')->primary->increments->remark("айдишник")->create;\n

Сравнение можно производить рекурсивно:
	
	my $base = $c->base->add($book, $author);
	
	$base->diff($base)	## ""
	
База создастся полностью:
	
	$c->base(undef)->diff($base)	#~ (?s)\$c->base\('rubin-forms-test'\)->create;.*my \$author.*my \$book
	
Поменяем только поле:
	
	
	my $mybase = $base->deep_clone;	
	
	$mybase->added_tab("author")->added_col("sex")->type("integer");
	
	$mybase->added_tab("author")->added_idx("idx_name_sex")->remark("индекс");
	
	
	$base->diff($mybase)		# $c->tab('author')->col('sex')->type('integer')->modify;\n
	
Синхронизируем базу:

	$c->base->transmute_to($base);
	
Проверяем наличие ключей:

	$c->tab("author")->idx("idx_name_sex")->exists		# 1
	$c->tab("book")->unique("unq_name")->exists			# 1
	
	$c->tab("author")->fk("fk_author_book")->exists		# 1
	$c->tab("book")->fk("fk_book_author")->exists		# 1
	
	
== Команда migrate
	
Файлы с миграциями располагаются в подпапке `migrate` проекта.

Новая миграция создаётся командой `al migrate <name>`.

Так же можно воспользоваться командой `al sync <name>`.
Если имя миграции не будет задано, то команда попробует получить его из `git diff model/*`.

Создаётся файл миграции, куда записываются команды, которые приведут структуру базы в соответствие с моделью.
Если нужно изменить ещё и данные, то разработчик вписывает код в методы `up` и `down` миграции.
	
Создаём проект:

	$app->file("var/.miu/root/06-migrate/etc/06-migrate.yml")->mkpath->write($app->yaml->to($app->ini));
	
@@06-migrate/model/User.pm
	package R::Row::User;
	R::Model::Row;

	use common::sense;
	use R::App;

	sub setup {
		my ($fields) = @_;
		$fields->
			col(name => "varchar(255)")->remark("имя")->
			col(email => "varchar(255)")->remark("емаил")->
			m2m(authors => author => "users")->remark("связь многие-ко-многим с автором")->
		meta(
			remark=>"пользователь",
		);
	}

	1;
@@06-migrate/model/Author.pm
	package R::Row::Author;
	R::Model::Row;

	use common::sense;
	use R::App;
	
	sub setup {
		my ($fields) = @_;
		$fields->
			col(name => "string")->need->remark("имя")->
			col(sex => "bool")->remark("М/Ж")->
			index("name, sex" => "idx_name_sex")->remark("индекс")->
		meta(
			remark => "перекрывает автора",
		);
	}

	1;
[test]
	
Создаём новую миграцию:

	$app->make->al("migrate", "first");
	
	my $file = $app->file("migrate")->find("**/first.pm");
	$file->exists		# 1

	$app->make->al("up");
	$app->connect->clean;
	
	$app->connect->tab("user")->exists	# 1
	$app->connect->tab("book")->exists	# 
	
	$app->make->al("down", "first");
	$app->connect->clean;
	
	$app->connect->tab("user")->exists	# 
	
	