= Поля ввода в формах

== Инициализация

Подключаем наш фреймворк:

[init]
	
	use lib "lib";
	use common::sense;
	use R::App;
	
[test]


== Поля ввода

=== hidden

	subtype "f1_range" => "between(1,3)" => "о, как!";

	my $form = $app->form->f1;
	$form->Meta->
		hidden(x => "метка никогда не выведется")->is("f1_range");
		
	$form->launch({x=>10});
	
	my $form_tag = $form->form_tag->render;
	
	$form_tag	#~ <input type="hidden" id="f1-x" name="x" value="10">
	$form_tag	#!~ <label for="f1-x">
	
	$form->is_valid	## ""
	
Если ошибка какого-то поля не была отрендерена, то она покажется в начале формы.
	
	$form_tag	#~ <li>x: о, как!
	$form_tag	#!~ for="f1-x"
	

=== checkbox

	my $form = $app->form->f2;
	$form->Meta->
		checkbox(x => "placeholder" => "label");
		
	$form->val({x=>"on"});
	
	$form->render	#~ <input type="checkbox" id="f2-x" name="x" checked> placeholder
	
	$form->val({x=>0});
	
	$form->render	#~ <input type="checkbox" id="f2-x" name="x"> placeholder
	
	$form->render	#~ <label for="f2-x">label</label>

=== select

У селекта нет плейсхолдера, поэтому третьим параметром передаются установленные значения. Если это массив, то селект становится мультиселектом.

	my $form = $app->form->f3;
	$form->Meta->
		select(s => "hi!", 2)->options([["", 0], {text=>"нихт", value=>1}, ["ферштейн", 2]])->
		select(m => "hi!", [])->options([{text=>"тссс", value=>1, selected=>1}, ["тихо!", 2], ["циц!", 3]]);
		
	$form->launch;
		
	$form->render	#~ <select id="f3-s" name="s" value="2">
		
	$form->val({s => 1});
	
	$form->render	#~ <select id="f3-s" name="s" value="1">
	
	$form->launch;
	$form->render	#~ <select id="f3-m" name="m" multiple>

	$form->launch({m => [3]});
	

~Мультиселект~

~Группы~

		# select(g => "hi!")->multiple->options([
			# {group=>"жрицы", options=>[["Валар", 11], ["Инанны", 12, 1], ["Митры", 13, 1]]},
			# {group=>"охотницы", options=>[["На нечисть", 21], ["На монстров", 22, 1], ["На ночных бабочек", 23, 1]]},
		# ]);


=== autocomplete


=== radiogroup

	# my $form = $app->form->f2;
	# $form->Meta->
		# radio(x => "hi!")->;
		
	# $form->val(x=>"on");
	
	# $form->render	# <input type="checkbox" id="f1-x" name="x" checked>
	
	# $form->val(x=>0);
	
	# $form->render	#~ <input type="checkbox" id="f1-x" name="x">

=== pager

Пейджер позволяет выводить много строк подформ. На джаваскрипте он смотрит на прокрутку и добавляет страницу вниз к уже выведенным.

На форме поиска он представлен сокрытым полем в котором находится id записи c которой находится очередная страница.

Так же пейджер содержит форму для показа записей. Её можно использовать как в режиме чтения, так и отдельно. Это полноценная форма.


	# my $form = $app->form->f4;
	# $form->Meta->
		# pager("mix")->
			# input("jux")->
		# end
		# ;
		
	# $form->val(x=>"on");
	
	# $form->render	# <input type="checkbox" id="f1-x" name="x" checked>
	
	# $form->val(x=>0);
	
	# $form->render	#~ <input type="checkbox" id="f1-x" name="x">
