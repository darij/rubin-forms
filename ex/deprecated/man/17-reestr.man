= Реестры

== Инициализация

Подключаем наш фреймворк:

[init]
	
	use lib "lib";
	use common::sense;
	use R::App;

	my $PRG_CD = $app->man->project("my-reestr-prj", log=>1, logsqlinfo=>0);
	
[test]


== Введение

Реестры использууются для отображения списка данных и списка по ним.

Грубо говоря, реестр, это пейджер, с формой поиска и формой отображения строки своего набора данных. Форма строки может быть в режиме реад-онли.


== Примеры реестров на основе моделей


@@my-reestr-prj/model/Author.pm
	package R::Row::Author;
	use common::sense;
	use R::App;
	
	sub setup {
		my ($fields) = @_;
		$fields->
			col(name => "varchar(255)")->remark("имя автора")->
		meta(
			ordering => "name",
		);
	}
	
	sub realdata {
		my $authors = $app->model->author;
		$authors->new->name("Василий")->saveAs(1);
		$authors->new->name("Василиса")->saveAs(2);
	}
	
	1;
@@my-reestr-prj/model/Book.pm
	package R::Row::Book;
	use common::sense;
	use R::App;
	
	sub setup {
		my ($fields) = @_;
		$fields->
			col(name => "varchar(255)")->remark("название книги")->
			ref("author")->remark("автор книги")
		;
	}
	
	sub realdata {
		my ($vasyliy, $vasilisa) = $app->model->author;
	
		my $books = $app->model->book;
		$books->new->name("Крабы на горе")->author($vasyliy)->saveAs(1);
		$books->new->name("Крабы свищут")->author($vasyliy)->saveAs(2);
		$books->new->name("Почему крабы, а не раки?")->author($vasilisa)->saveAs(3);
	}

	1;
@@my-reestr-prj/form/Library.pm
	package Reestr::Library;
	# библиотека

	use common::sense;
	use R::App qw/$app/;

	# конструктор
	sub setup {
		my $reestr = shift;

		$reestr->
			input(search => "" => "введите ФИО автора или название книги")->
			checkbox(by_author => "по автору")->initial(1)->
			checkbox(by_book => "по книге")->initial(1)->
			
			pager("books")->
				input(author => "автор")->
				input(book => "книга")->
			end;
	}
	
	sub select {
		my ($self) = @_;
		
		my $phrase = "%" . $self->val("search") . "%";
		
		my $books = $app->model->book;
		
		my $rows = $self->val("by_author") && $self->val("by_book")?
			$books->find(name__like => $phrase)->
				or(author__name__like => $phrase):
			
		$self->val("by_author")? 
			$books->find(author__name__like => $phrase):
			
		$self->val("by_book")?
			$books->find(author__name__like => $phrase):
		
		$books;
			
		return books => $rows->page($self->val("page"));
	}

	1;
@@my-reestr-prj/form/Modellibrary.pm
	package Reestr::Modellibrary;
	# 

	use common::sense;
	use R::App qw/$app/;

	# конструктор
	sub setup {
		my $reestr = shift;

		$reestr->
			input(search => "" => "введите ФИО автора или название книги")->
			
			pager("books")
				
			;
	}

	1;
[test]


Синхронизируем модели с базой:

	$app->meta->sync;
	
Отрендерим форму:
	
	$app->form->library->render		# 
	
	$app->form->library->launch({ by_author=>1 })->render		# 







