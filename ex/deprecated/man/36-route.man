= Роутер

== Инициализируемся для тестов
[init]
	use lib "lib";
	use common::sense;
	use R::App qw/$app app msg1/;

[test]


== Методы и местоположения

Создадим роутеры:

	my $r = $app->routers->new;
	my $q = $r->querier;
	
	$r->get("/" => sub { "index" });
	$r->post("/" => sub { "index-post" });
	$r->put("/" => sub { "index-put" });
	$r->del("/" => sub { "index-delete" });
	$r->patch("/" => sub { "index-patch" });
	
	$q->get("/")	# index
	$q->post("/")	# index-post
	$q->put("/")	# index-put
	$q->del("/")	# index-delete
	$q->patch("/")	# index-patch

Перекрыть метод `OPTIONS`, который возвращает в заголовке опции можно так:

	$r->options("/" => sub { [200, ["Opt"=>12], ""] });
	$q->options("/")		## [200, ["Opt"=>12], ""]
	
Для всех методов:
	
	$r->route("/user/:id/:action" => sub { my $q=shift; $q->attr->id ."-". $q->attr->action });
	
	$q->get("/user/10/setname")			# 10-setname
	$q->post("/user/10/setname")		# 10-setname
	$q->head("/user/10/setname")		# 10-setname
	$q->del("/user/10/setname")			# 10-setname
	$q->options("/user/10/setname")		# 10-setname
	$q->patch("/user/10/setname")		# 10-setname
	
И для избранных:
	
	$r->route(['GET', 'POST'] => '/bye' => sub { 'bye' });
	
Для `GET` и `POST` запросов с заголовком `X-Requested-With=XMLHttpRequest`:
	
	$r->ajax("/ajax" => sub {
		my $q=shift;
		"ajax " . $q->via . "-" .$q->query->x ."-".$q->data->x;
	});
	
	$q->get("/ajax?x=10" => {"X-Requested-With"=>"XMLHttpRequest"} => [ x=>20 ])		# ajax GET-10-20
	$q->post("/ajax?x=10" => ["X-Requested-With"=>"XMLHttpRequest"] => { x=>20 })		# ajax POST-10-20
	$q->put("/ajax?x=10" => {"X-Requested-With"=>"XMLHttpRequest"} => { x=>20 })		##@ [404]
	
	$r->route(["POST", "GET"] => '/ajax2')->grep(sub { shift->header("X-Requested-With") eq "XMLHttpRequest" })->to(sub {
		my $q=shift;
		"ajax2 " . $q->via . "-" .$q->query->x ."-".$q->data->x;
	});
	
	$q->ajax("/ajax2?x=10" => {} => [ x=>20 ])		# ajax2 POST-10-20
	
== Подроутеры

Методы `get`, `post` и т.д. возвращают подчинённый роутер к которому можно добавлять свои роутеры.

	$r->get("/main" => sub { "main" })
		->get("/sub" => sub { "level-2" })
			->get("/sub" => sub { "level-3" });
	
	$q->get("/main")			# main
	$q->get("/main/sub")		# level-2
	$q->get("/main/sub/sub")	# level-3
	
Для подроутера, который не имеет обработчиков просто не указывайте подпрограмму:

	$r->route("/major")
		->get("/sub" => sub { "level-2" })
			->get("/sub" => sub { "level-3" });
			
	$q->get("/major")			##@ [404]
	$q->get("/major/sub")		# level-2
	$q->get("/major/sub/sub")	# level-3
	
	
== Хуки

=== over

Срабатывает до экшена.

	# $r->over(sub {})->get("/over" => sub { "over" })
		# ->get("/sub" => sub { "level-2" })
			# ->get("/sub" => sub { "level-3" });
	
=== then

Срабатывает после экшена.
	

== Stash

Сташ предоставляет общую (глобальную) информацию для экшена, доступные во всех запросах.

== session

