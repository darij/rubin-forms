package R::Row::Queue;
# очередь заданий

use base R::Model::Row;

use common::sense;
use R::App;

# раз в час удаляем лишние задания
$app->shiva->on("queue-eraser" => "1 * * * *" => sub {
	$app->model->queue->find(queuename=>undef)->erase;
});

# вызывается для создания структуры базы
sub setup {
	my ($fields) = @_;
	
	$app->meta->fieldset("queuename")->
		col(name => 'varchar(255)')->min_length(1)->remark("наименование задания")->
		unique("name")->	
	meta(
		remark => "наименования заданий",
	);
	
	$fields->
	
	ref('queuename')->remark("канал")->
	col(beneficiary=>'int unsigned')->default(0)->remark("уникальный номер получателя")->
	col(running => 'unixtime')->default(0)->remark("задание взято на обработку")->
	col(args => 'varchar(255)')->remark("аргументы задания")->
	
	meta(
		remark => "очередь заданий",
	);

	
}

# тестовые данные
sub testdata {
}


# зарегистрировать канал
sub channel {
	my ($self, $name) = @_;
	
	die "задание уже есть" if $app->model->queuename->find(name => $name)->exists;
	
	$app->model->queuename(name=>$name)->save;
	
	$self
}

# добавить задание
sub push {
	my ($self, $name, $args) = @_;
	
	my $queuename = $app->model->queuename->find(name => $name)->exists;
	
	die "не зарегистрирован канал `$name`" if !$queuename;
	
	my @args = (queuename=>$queuename, args=>$args);
	
	# организовать пулл заданий
	$app->model->queue->find(queuename=>undef)->limit(1)->update(@args);
	if( $app->connect->last_count == 0 ) {
		$app->model->queue(@args)->save;
	}
	
	$self
}

# получить задание
sub pull {
	my ($self, $name, $beneficiary) = @_;
	
	die "используйте: pull(канал, уникальный_номер_получателя)" if !Num($beneficiary) || $beneficiary<=0;
	
	my $queuename = $app->model->queuename->find(name => $name)->exists;
	
	die "не зарегистрирован канал `$name`" if !$queuename;
	
	my $time = time();
	
	$app->model->queue->find(queuename => $queuename, beneficiary=>0)->limit(1)->update(beneficiary=>$beneficiary, running=>$time);
	
	my $task;
	if($app->connect->last_count) {
		$task = $app->model->queue->find(queuename => $queuename, beneficiary=>$beneficiary, running=>$time)->exists;
	}
	
	$task
}

# отметить, что задание выполнено
sub pop {
	my ($self) = @_;
	$self->queuename(undef)->save;
}


1;