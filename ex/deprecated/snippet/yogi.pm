﻿package Controller::{{ Name }};
# {{ name }} - ...

use common::sense;
use R::App qw/:yogi :max/;


get "/{{ name }}" => sub {
	render "{{ name }}";
};


1;

__DATA__

@@ {{ name }}.mid
{% extends "menu.mid" %}
{% block content %}

{% endblock content %}