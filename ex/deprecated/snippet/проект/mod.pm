package {{ package }};
# модуль

use common::sense;
use R::App;

# свойства
has qw//;

# конструктор
sub new {
	my $cls = shift;
	bless {@_}, ref $cls || $cls;
}

1;