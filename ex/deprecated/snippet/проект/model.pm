package R::Row::{{ Name }};
# модель {{ name }}

use base R::Model::Row;

use common::sense;
use R::App;

# вызывается для создания структуры базы
sub setup {
	my ($fields) = @_;
	$fields->

	col(name => "varchar(255)")->remark("имя")->
	
	# индексы
	unique("name")->

	match(
		"name"=> 1,
	)->
	
	meta(
		remark => "{{ name }}",
	)->
	
	end;
}

# реальные данные
sub realdata {
	my ($self) = @_;
	
	$self
}

# тестовые данные
sub testdata {

	my ${{ name }} = $app->model->{{ name }}(1);
	${{ name }}->name('NAME');
	${{ name }}->store;
	
}

# выдаёт краткую информацию о себе
sub annonce {
	my ($self) = @_;
	$self->name
}


1;

1;