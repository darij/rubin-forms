package R::Mime::Text::{{ Name }};
# тип

use common::sense;
use R::App;

use Types::Serialiser;

# конструктор
sub new {
	my $cls = shift;
	bless {@_}, ref $cls || $cls;
}

# из {{ name }}
sub from {
	my ($self, $text) = @_;
	...
}

# в {{ name }}
sub to {
	my ($self, $data) = @_;
	...
}

# колоризированная строка
sub color {
	my ($self, $data) = @_;
	...
}



1;