###
  * company: {{ company }}
  * license: {{ license }}
  * author:  {{ author }}
  *
  * страница
###

import { h, Component } from 'Reactive'
import Head from 'lib/preact-head'


import {Menu, LeftMenu, RightMenu, Content, Footer} from 'rubin/layout'
import Layout from '{{ src_dir }}/layout'


export default class {{ Name }} extends Component

	render: (props, stats)->
	
		<Layout title="{{ name }}">
		
			<Content>{{ src }}</Content>
			
		</Layout>
