package R::Widget::{{package_name}};
# 

use base R::Widget;

use common::sense;
use R::App;

# методы для запросов /ajax/{{name}}/<метод>
sub mount {
	qw/save/
}

# устанавливает заголовок для экземпляров виджета на странице
sub head {
	my ($self) = @_;
	$self->request->require_js("/asset/{{name}}/{{name}}.js");
	$self->request->require_css("/asset/{{name}}/{{name}}.css");
}

# возвращает тело виджета
sub body {
	my ($self) = @_;
	
	$self->render("{{name}}.html", {
		
	});
}

# сохраняет информацию
sub save {
	my ($self, $content) = @_;
	
	
}


1;