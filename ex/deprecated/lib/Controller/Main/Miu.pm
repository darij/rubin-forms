﻿package Controller::Main::Miu;
# контроллер 

use common::sense;
use R::App qw/:yogi :max/;

#my ($miu) = $app->file("miu")->whereis->read =~ /()/;

# возвращает мануал
sub get_manual {
	my ($man) = @_;
	my $f = $app->project->files("man/$man.man")->existing;
	my $text = $f->read;
	my ($title) = $text =~ /^=[ \t]+(.*)/m;
	return $title, $text;
}

sub get_man_files {
	my $man = $app->project->files("man")->find("**.man");
	[ $man->map(sub {
		my ($title) = get_manual($_->name);
		+{
			file => $_,
			title => $title,
		}
	}) ]
}

# возвращает меню
sub get_menu {
	[ map { ["/~/man/".$_->{file}->name, $_->{title}] } @{get_man_files()}]
}

get "/~/man" => sub {
	render "rubin-forms/man/index", {
		files => get_man_files(),
		title => "Мануал",
	};
};

# тест
get "/~/man/*man" => sub {
	my $man = attr->man;
	my $man_file = $app->project->files("man/$man.man")->existing;
	
	die [404] if !$man_file->length;
	
	my $mark_file = $app->project->files("mark/$man.markdown");
	my $markdown = $mark_file->existing;
	
	
	my $miu_error;
	if(!$markdown->length) {
		my $framework = $app->framework->path;
		my $x = `cd $framework; miu -a`;
		utf8::decode($x);
		$miu_error = $x;
		if($? == 0) {
			$markdown = $mark_file->existing;
			$miu_error .= "\nТест так и не появился" if !$markdown->length;
		}
	}
	
	my ($title) = get_manual($man);
	
	my $article;
	if($markdown->length) {
		$article = $app->mime->markdown->to_html($markdown->read);
	}

	render "rubin-forms/man/man", {
		menu => get_menu(),
		article => $article,
		title => $title,
		miu_error => $miu_error,
		man => $man,
	};
};

put "/~/man/*man" => sub {
	my $man = attr->man;
	my $framework = $app->framework->path;
	my $x = `cd $framework; miu $man`;
	utf8::decode($x);
	
	return {
		output => $app->html->ansi($x),
		status => $?,
	};
};


1;


__DATA__

@@ rubin-forms/man/index.mid
{% extends "rubin-forms/menu" %}
{% block content %}
	<table>
	<tr><th>Глава<th>Файл
	{% for f in files %}
		<tr>
		<td><a href="/~/man/{{ f:file.name }}">{{ f:title }}</a>
		<td>{{ f:file.path }}
	{% endfor %}
	</table>
{% endblock content %}

@@ rubin-forms/man/man.mid
{% extends "rubin-forms/menu" %}
{% block lmenu %}{{ menu|ul }}{% endblock %}
{% block content %}
	
	<p>{{ miu_error }}</p>
	<p><button onclick="miu('{{ man }}')">Тестировать</button></p>
	<p id="status"></p>
	<p id="output"></p>
	
	
	{% if not article %}
		Мануал не скомпилирован <button>Скомпиллировать</button>
	{% else %}
		<link rel="stylesheet"
      href="//cdnjs.cloudflare.com/ajax/libs/highlight.js/9.13.1/styles/default.min.css">
		<script src="//cdnjs.cloudflare.com/ajax/libs/highlight.js/9.13.1/highlight.min.js"></script>
	
		<div>{{ article|raw }}</div>
		
		<script>
			$('code[lang]').each(function() {
				hljs.highlightBlock(this);
			});
		</script>
	{% endif %}
	
	
	<script>
		function miu (man) {
			$.ajax("/~/man/"+man, {method: "PUT"}).then(function(args) {
				$("#status").text(args.status)
				$("#output").html(args.output)
			})
		}
	</script>

{% endblock content %}
