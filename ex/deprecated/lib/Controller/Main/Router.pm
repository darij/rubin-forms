﻿package Controller::Main::Router;
# router - показывает роутеры в приложении

use common::sense;
use R::App qw/:yogi :max/;


get "/~/routers" => sub {
	my $routers = $app->sitemap->flat;
	@$routers = ascending { $_->{router}->controller } @$routers;
	render "/rubin-forms/routers", {routers => $routers};
};


1;

__DATA__

@@ /rubin-forms/routers.mid
{% extends "rubin-forms/menu" %}
{% block content %}

<table>
<tr><th>root
	{% if app.sitemap.root|size %}
		{% for i in app.sitemap.act %}<tr><td>{{ i }}{% endfor %}
	{% else %}
		<tr><td>нет
	{% endif %}
</table>

<table>
<tr><th>act
	{% if app.sitemap.act|size %}
		{% for i in app.sitemap.act %}<tr><td>{{ i }}{% endfor %}
	{% else %}
		<tr><td>нет
	{% endif %}
</table>

<table class="">
<tr><th>via<th>path<th>type
{% for router in routers %}
	{% ifchange router:router.controller %}
		<tr><th colspan=3>{{ router:router.controller|default:"-" }}
	{% endifchange %}
	<tr><td>{{ router:via }}<td>{{ router:path }}<td>{{ router:router.type }}
{% endfor %}
</table>
{% endblock %}


