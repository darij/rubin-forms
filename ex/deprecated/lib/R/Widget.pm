package R::Widget;
# менеджер виджетов
# app->widget->mywidget(...)

use common::sense;
use R::App qw/$app/;

# конструктор
sub new {
	my $cls = shift;
	my $self = bless {
		# уровни переопределения
		override_levels => ["widget.common", "widget.library"],
		base => "R::Widget",
		rebase => "R::Widget",
		@_
	}, ref $cls || $cls;
	
	$self
}


# возвращает форму
sub AUTOLOAD {
	my ($self) = @_;
	
	our $AUTOLOAD =~ /([^:]+)$/;
	my $prop = $1;
	
	my $name = lcfirst $prop;
	$name =~ s![A-Z]!"-".lc $&!ge;
	
	my $files = $app->project->files(map { "widget/$_/$name/$name.pm" } @{$self->{override_levels}})->existing;
	
	eval {
		require $files->dot->path;
	};
	R::msg("not exists widget `$name` in [".join(", ", @{$self->{override_levels}})."]"), die if $@;

	my $class = ucfirst $prop;
	$class =~ s!-(\w)!uc $1!ge;
	my $sub = ref($self) . "::" . lcfirst $class;
	$class = "$self->{base}::$class";
	
	# копируем 
	if($self->{base} ne $self->{rebase}) {
		#my $reclass = $class;
		R::TODO();
	}
	
	# возвможно в будущем будет кеширование, а чтобы изменить что-то в форме придётся делать deep_copy
	# но в любом случае вначале нужна i18n
	my $eval = "sub $sub { shift; $class->new(\@_) }";
	eval $eval;
	die if $@;
	my $sub = *{$sub}{CODE};
	
	goto &$sub;
}

sub DESTROY {}


1;