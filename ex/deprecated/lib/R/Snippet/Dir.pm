package R::Snippet::Dir;
# создают целые директории

use common::sense;
use R::App;

# конструктор
sub new {
	my ($cls) = @_;
	bless {}, ref $cls || $cls;
}

# создаёт портальный проект
sub portal {
	my ($self, $dir) = @_;
	
	# создаём директорию
	if($dir) {
		$dir = $app->file($dir)->mkdir;
	} else {
		$dir = $app->file(".")->abs;
	}

	# имя нового проекта:
	my $name = $dir->file;
	
	# создаём подкаталоги
	$dir->sub("etc")->mkdir;
	$dir->sub("com")->mkdir;
	$dir->sub("html")->mkdir;
	$dir->sub("html/css")->mkdir;
	$dir->sub("html/js")->mkdir;
	$dir->sub("html/img")->mkdir;
	$dir->sub("lib")->mkdir;
	$dir->sub("model")->mkdir;
    $dir->sub("man")->mkdir;
	$dir->sub("var")->mkdir;
	
	# копируем ini
	my $x = $dir->sub("etc/$name.ini");
	$app->file($app->framework->root . "/etc/portal.sample.ini")->cp($x);
	$x->replace(sub {
		s!\bNAME\b!$name!g;
		s!CHIPHER_KEY!$app->perl->unic_id!e;
		s!CHIPHER_SALT!$app->perl->unic_id!e;
	});
	$x->cp($dir->sub("etc/$name.sample.ini"));
	
	# создаём пример компонента
	$dir->sub("com/Hi.jsx")->write($app->snippetFile->com("Hi", "Hi!"));
	
	# создаём пример компонента
	$dir->sub("model/User.pm")->write($app->snippetFile->model("User"));
	
	
	
	# создаём коммандер
	# $dir->sub($name)->write('#!'.$^X.'
# BEGIN { push @INC, "lib" }
# use common::sense;
# use R::App;
# use R::Make;

# $app->make->load->run;
# ');
	
	# отображения
	
	
	# git
	
	#$dir->chdir;
	#print `git init` if !$dir->sub(".git")->exists;
	
	
	$self
}

1;