package R::Snippet::File;
# содержит файлы сниппетов

use common::sense;
use R::App;

# конструктор
sub new {
	my ($cls) = @_;
	bless {
		company => $app->ini->{copiright}{company} // "Emerald labs",
		license => $app->ini->{copiright}{license} // "MIT",
		author => $app->ini->{copiright}{author} // "darthyar",
	}, ref $cls || $cls;
}

# сниппет модели
sub model {
	my ($self, $name) = @_;
	my $x = <<'END';
	package R::Row::NAME;
# модель

use base R::Model::Row;

use common::sense;
use R::App;

# вызывается для создания структуры базы
sub setup {
	my ($fields) = @_;
	$fields->

	col(name => "varchar(255)")->remark("имя NAME")->
	
	
	# индексы
	unique("name")->

	match(
		"name"=> 1,
	)->
	
	meta(
		remark => "модель",
	)->
	
	end;
}

# реальные данные
sub realdata {
	my ($self) = @_;
	#$self->autoincrement_inc(1000000) if $self->autoinctrement_get < 1000000;
	$self
}

# тестовые данные
sub testdata {

	my $MODEL = $app->model->MODEL(1);
	$MODEL->name('NAME');
	
}

# выдаёт краткую информацию о себе
sub annonce {
	my ($self) = @_;
	$self->name
}


1;
END

	my $model = lcfirst $name;
	$x =~ s!MODEL!$model!;
	
	$name = ucfirst $name;
	$x =~ s!NAME!$name!;

	$x
}

# сниппет компонента
sub com_jsx {
	my ($self, $name) = @_;
	
	$name = ucfirst $name;
	
	return <<"END";
/**
  * company: $self->{company}
  * license: $self->{license}
  * author:  $self->{author}
  *
  * компонент реакт
  */

import { h, Component } from 'Reactive';
/** \@jsx h */


export default class $name extends Component {
	/**
	  * конструктор
	  */
	constructor (props) {
		super(props);
		this.stats = {
			
		};
	}

	/**
	 * рендерит отображение
	 */
	render (props, stats) {
		return <div class="">
			
		</div>;
	}

}
END
}

# сниппет компонента на coffee
sub com_coffee {
	my ($self, $name) = @_;
	
	$name = ucfirst $name;
	
	return <<"END";
###
  * company: $self->{company}
  * license: $self->{license}
  * author:  $self->{author}
  *
  * компонент реакт
###

import { h, Component } from 'Reactive'

export default class $name extends Component
	assign = {}

	constructor: (props)->
		super(props)
		\@stats = {
			
		}

	render: (props, stats)->
		<div class="">
			
		</div>

END
}


1;