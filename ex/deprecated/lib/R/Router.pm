package R::Router;
# роутер получает таблицу путей и при запросе выполняет dispatch на объекте

use common::sense;
use R::App;

# конструктор
sub new {
	my $cls = shift;
	my $self = bless {
		direct => {},		# прямые пути
		serial => [],		# последовательные пути с регулярками
		table => [],		# исходная таблица роутинга
		exists => {},		# локейшены вперемешку для проверки, что не добавлен ещё какой-то
		controllers => {},	# загруженные контроллеры
		from => [$app->act, $app->root],	# из каких сервисов заполнить таблицы роутингов
		@_
	}, ref $cls || $cls;
	
	$self->fill
}

# заполняет таблицы роутинга по переданным сервисам
sub fill {
	my ($self) = @_;
	
	$self->add($_->routers) for @{$self->{from}};
	
	$self
}

# добавляет в роутер таблицы путей
sub add {
	my $self = shift;

	msg1 ":size10000", [@_];
	
	my $exists = $self->{exists};
	my @route = map2 { $a } grep2 { exists $exists->{$a} } @_;
	die "роут ".join(", ", @route)." уже добавлен" if @route == 1;
	die "роуты ".join(", ", @route)." уже добавлены" if @route > 1;
	
	push @{$self->{table}}, @_;
	
	for(my $i=0; $i<@_; $i+=2) {
		my ($route, $direct) = @_;
		
		die "нет ссылкам!" if ref $route;
		die "роутер должен начинаться на /" if $route !~ /^\//;
		
		$exists->{$route} = 1;
		
		push(@{$self->{serial}}, $route, $direct), next if ref $route eq "Regexp";
		push(@{$self->{serial}}, $self->location($route), $direct), next if $route =~ /[:#%*]/;
		
		$self->{$route} = $direct;
	}
	
	$self
}

# ищет в таблицах соответствие и возвращает контроллер
sub scan {
	my ($self, $path, $request) = @_;
	
	$self->{direct}{$path} // do {
		my $serial = $self->{serial};
		for(my $i=0; $i<@_; $i+=2) {
			my $re = $serial->[$i];
			%{$request->{_attr}} = %+, return $serial->[$i+1] if $path =~ $re;
		}
		return undef;
	}
}

# запускает контроллер
sub dispatch {
	my ($self, $path, $request) = @_;
	my $direct = $self->scan($path, $request) // die [404];
	$direct->dispatch($request, $path)
}

# преобразовывает location к 
sub location {
	my ($self, $location) = @_;

	$location = quotemeta $location;
	
	my $x = qr! \\ (
		:  (?<word>\w+) |
		\# (?<str>\w+)  |
		%  (?<int>\w+)  |
		\* (?<all>\w+)  
	) !x;

	$location =~ s{
		$x |	\\ \( $x \\ \)
	}{
		$+{word}? "(?<$+{word}>[^/.]+)":
		$+{str}? "(?<$+{str}>[^/]+)":
		$+{int}? "(?<$+{int}>\d+)":
		$+{all}? "(?<$+{all}>.*)":
		""
	}xge;
	
	qr/$location/
}

1;