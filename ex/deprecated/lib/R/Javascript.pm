package R::Javascript;
# движок javascript

use common::sense;
use R::App;
use R::Coro;

has qw/src dst/;

my $SRC = $app->project->files('src', 'js', 'var/.jsx');
my $DST = $app->project->file('var/.jsx');
my $NORMALIZE = $app->project->file('node/css/normalize.css', 'node/css/rubin.css');

# конструктор
sub new {
	my $cls = shift;
	my $self = bless {
		src => $SRC,	# в каких директориях искать подключаемые файлы
		dst => $DST, 	# директория в которую будет производиться компилляция
		normalize => $NORMALIZE,  # css для нормализации
		@_,
	}, ref $cls || $cls;
	$self
}

# преобразует для печати
sub _je_perl {
	map {
		my $v = $_;
		$v = $v->get if ref $v eq "JE::LValue";
		my $ref = ref $v;
		if($ref =~ /^(JE::Object::Array|JE::Object::Function::Arguments)$/) {
            bless [ _je_perl(@{$v->value}) ], "X-Array"
		} elsif($ref =~ /^(JE::Object)$/) { bless +{ _je_perl(%{$v->value}) }, "X-Hash"
		} elsif($ref =~ /^(HASH)$/) { +{ _je_perl(%$v) }
		} elsif($ref =~ /^(ARRAY)$/) { [ _je_perl(@$v) ]
		} elsif($ref =~ /^(JE::Null)$/) { undef
		#} elsif(ref $v eq "JE::String") { $v =~ s/"/\"/g; "$v"
		#} elsif(Can $v, "value") { $v->value
		} else { "$v" }
	} @_
}

# рекурсивно превращает перловую структуру в стркутуру je
sub upgrade {
	my ($self, $data) = @_;
	
	if(ref $data eq "ARRAY") {
		JE::Object::Array->new($self->je, [ map { $self->upgrade($_) } @$data ])
	}
	elsif(ref $data eq "HASH") {
		JE::Object->new($self->je, { value => +{ map2 { $a => $self->upgrade($b) } %$data } })
	}
	elsif(ref $data eq "CODE") {
		JE::Object::Function->new($self->je, $data)
	}
	elsif(ref $data eq "RegExp") {
		JE::String->new($self->je, "$data")
	}
	elsif(!defined $data) {
		$self->je->null
	}
	elsif(!ref $data) {
		Num($data)? JE::Number->new($self->je, $data): JE::String->new($self->je, $data)
	}
	else {
		$self->je->upgrade($data);
	}
}

# рекурсивно превращает структуру из je в perl
sub downgrade {
	my ($self, $v) = @_;

	$v = $v->get if ref $v eq "JE::LValue";
	my $ref = ref $v;
	if($ref =~ /^(JE::Object::Array|JE::Object::Function::Arguments)$/) {
		[ map { $self->downgrade($_) } @{$v->value} ]
	} elsif($ref =~ /^(JE::Object)$/) { 
		+{ map { $self->downgrade($_) } %{$v->value} }
	} elsif($ref =~ /^(JE::Boolean)$/) { 
		$v
	} elsif($ref =~ /^(JE::Null|JE::Undefined)$/) { 
		undef
	#JE::Object::RegExp
	#JE::Code
	#} elsif($v ) { +"nan"
	#} elsif(ref $v eq "JE::String") { "$v"
	} elsif(Can $v, "value") { 
		$v->value
	} else { "$v" }
}

# создаёт интерпретатор js
sub je {
	my ($self) = @_;
	
	return $self->{je} if exists $self->{je};
	
	require JE;

	my $je = $self->{je} = JE->new;
	#my $undefined = $je->undefined;

#     name      property name
#     value     new value
#     dontenum  whether this property is unenumerable
#     dontdel   whether this property is undeletable
#     readonly  whether this property is read-only
#     fetch     subroutine called when the property is fetched
#     store     subroutine called when the property is set
#     autoload  see below
	
	$je->eval("Object")->new_function("defineProperty" => sub {
        my ($obj, $key, $desc) = @_;
		
        my $conf = {};
        $conf->{name} = $key->value;
        $conf->{value} = $desc->{value} if exists $desc->{value};
        $conf->{readonly} = !$desc->{writable}->value if exists $desc->{writable};
        $conf->{dontdel} = !$desc->{configurable}->value if exists $desc->{configurable};
        $conf->{dontenum} = !$desc->{enumerable}->value if exists $desc->{enumerable};
        if(exists $desc->{set}) {
            my $set = $desc->{set};
            $conf->{store} = sub { my ($obj, $val)=@_; $set->call_with($obj, $val) };
        }
        if(exists $desc->{get}) {
            my $get = $desc->{get};
            $conf->{fetch} = sub { my ($obj)=@_; $get->call_with($obj) };
        }
        $obj->prop($conf);
        return $obj;
    });
	
	$je->new_function(setTimeout => sub {
		my ($code, $miniseconds) = @_;
		my $timers = $self->{timers} //= {};
		my $key = $self->{timer_counter}++;
		$timers->{$key} = AE::timer($miniseconds->value/1000, 0, sub {
			delete $timers->{$key};
			Isa($code, qw/JE::Object::Function/)? $code->(): $self->from($code->value, undef, "<from setTimeout>")
		});
		$key
	});
	
	$je->new_function(setInterval => sub {
		my ($code, $miniseconds) = @_;
		my $timers = $self->{timers} //= {};
		my $key = $self->{timer_counter}++;
		my $interval = $miniseconds->value/1000;
		$timers->{$key} = AE::timer($interval, $interval, sub {
			Isa($code, qw/JE::Object::Function/)? $code->(): $self->from($code->value, undef, "<from setInterval>")
		});
		$key
	});
	
	$je->new_function(clearInterval => sub {
		my ($key) = @_;
		my $timers = $self->{timers} //= {};
		delete $timers->{$key->value};
		return;
	});
	
	$je->new_function(require => sub {
		my ($path) = @_;
		$self->_require($path->value)->{exports};
	});
	
	$je->new_function("say" => sub {
        $app->log->info( ":on_cyan black space", " say ", ":reset sep", _je_perl(@_) );
        return @_? $_[$#_]: undef;
	});
	
	my $console = $je->eval("console={}");
	$console->new_function("log" => sub {
        $app->log->info( ":on_cyan black space", " console.log ", ":reset sep", _je_perl(@_) );
        return;
	});
	$console->new_function("error" => sub {
        $app->log->info( ":on_red black space", " console.error ", ":reset sep", _je_perl(@_) );
        return;
	});
	
	# подключаем полифиллы
	my $stack = delete $self->{stack_modules};
	my $stack_guard = guard { $self->{stack_modules} = $stack };
	$self->_require("lib/polyfill");
	undef $stack_guard;
	
	#$je->{document} = {};
	
	# рендер
	$self->eval("function render(path, data) {
		var reactToString = require('lib/preact-render-to-string')
		var preact = require('lib/preact')
		var Head = require('lib/preact-head')['default']
		var StyleSheetServer = require('lib/aphrodite').StyleSheetServer;
		
		var Component = require(path)['default']
		
		if(!Component) throw Error('нет компонента в модуле '+path)
		
		var result = StyleSheetServer.renderStatic(function() {
			var vnode = preact.h(Component, data)
			return reactToString.render(vnode)
		});

		result.head = Head.rewind().map(reactToString.render).join('')
		return result
	}	
	", "<render>", 1);
	
	# # создаёт стили из js
	# $je->{em} = "em";
	# $je->{px} = "px";
	
	# $je->new_function("newStyleSheet" => sub {
		
	# });
	
	$je
}

# подключает модуль
sub _require {
	my ($self, $filename) = @_;

	my $f = $app->file($filename)->ext("");
	
	if($filename =~ m!^\.+/!) {
		my $cur_dir = $self->top_module->{dir};
		$f = $f->abs("$cur_dir/");
	}

	my $path = $f->path;
	
	#msg ":on_cyan black", "require", ":reset", $path;
	
	# если модуль уже есть
	if(my $module = $self->{module}{$path}) {
		$self->child_module($module);
		return $module;
	}
	
	my $src = $self->src->sub("$path.js")->existing;
	if(!$src->length) {
		$src = $self->src->sub("$path/index.js")->existing;
		$f = $f->sub("index.js");
	}

	die "js-require: не найден $path в\n\t" . $self->src->join("\n\t") if !$src->length;
	
	my $code = join "", "(function(exports, require, module, __filename, __dirname) {\n", $src->read,"\n})(module.exports, require, module, module.path, module.dir)";
	
	my $module = {
		dir => $f->dir,
		path => $path,
		code => $code,
		children => [],
	};

	my $je = $self->je;
	$je->{module} = {};
	$je->{module}{path} = $path;
	$je->{module}{dir} = $module->{dir};
	$je->{module}{exports} = {};
	my $m = $je->{module};
	
	my $save = $self->push_module( $module );
	
	$self->eval($code, $src->path, 0);
	
	undef $save;
	
	$self->child_module($module);
	$self->{module}{$path} = $module;
	
	$module->{exports} = $m->{exports};
	
	$module
}

# добавляет модуль к тому что на вершине стека
sub child_module {
	my ($self, $module) = @_;
	my $top_module = $self->top_module;
	push @{ $top_module->{children} }, $module if $module;
	$self
}

# возвращает на вершине стека модуль
sub top_module {
	my ($self) = @_;
	my $stack = $self->{stack_modules};
	@$stack? $stack->[$#$stack]: undef;
}

# добавляет модуль в стек. Возвращает гуард, который удалит модуль
sub push_module {
	my ($self, $module) = @_;
	push @{$self->{stack_modules}}, $module;
	guard { my $x=pop @{$self->{stack_modules}}; msg1 "<=>", $module, $x if $module != $x }
}

# сбрасывает je
sub clear {
	my ($self) = @_;
	delete @$self{qw/je stack_modules module/};
	$self
}

# хелпер, возвращающий все подмодули
sub __flat {
	my ($module) = @_;
	return (map { __flat($_) } @{$module->{children}}), $module;
}

# рендерит шаблон preact
sub render {
	my ($self, $path, $data) = @_;
	
	# преобразуем в относительный путь
	
	$path //= "index";
	$path =~ s!\.\w+$!!;
	$path =~ s!'!\\'!g;

	my $virtual_module = {
		path => ".",
		dir => "",
		children => [],
	};
	my $save = $self->push_module( $virtual_module );
		
	my $das = $self->je->{render}->($path, $self->upgrade($data));
	
	undef $save;
	
	my ($css, $body, $head) = @$das{qw/css html head/};
	
	$css = $css->{content};
	
	my $styles = $self->{normalize_css} //= $self->{normalize}->cat;
	
	my $code = join "", map { !defined($_->{code})? (): "\nmodule = {exports:{},path:'$_->{path}',dir:'$_->{dir}'};\n\n$_->{code};\nmodules[module.path] = module;\n" } __flat($virtual_module);
	
	# Tour { delete @$b{qw/code exports dir/} if ref $b eq "HASH"; } $virtual_module;
	# $code = $app->yaml->to( $virtual_module );
	
	my $props = $app->json->to($data);
	
	# клиентский javascript:
	return << "END";
<!doctype html>
<html lang="ru">
<head>
<meta charset="UTF-8">
$head
<style>
$styles
$css
</style>
	
<script>
function say() {
	if(arguments.length === 0) throw new Error("say без аргументов")
	console.log.apply(this, arguments)
	return arguments[arguments.length-1]
}

var module, modules = {};

function _norm(path) {
	var paths = path.split(/\\/+/)
	
	for(var i=0; i<paths.length; i++) {
		if(paths[i] == ".") paths.splice(i, 1)
		else if(paths[i] == "..") {
			if(i<=1) throw new Error("выход за пределы рутовой директории в пути модуля "+path)
			paths.splice(i-1, 2)
		}
	}
	
	return paths.join("/")
}

function require(path) {
	if(/^\\./.test(path)) path = _norm(module.dir+"/"+path)
	path = path.replace(/\\.\\w+\$/, "");
	
	var m = modules[path]
	
	if(!m) throw new Error("нет модуля "+path+".js или "+path+"/index.js")
	return m.exports;
}

$code
</script>
	
</head>
<body>
	<div id=content>
		$body
	</div>

	<script>
	(function() {
		var preact = require('lib/preact')
		var component = require('$path')['default']
		var vnode = preact.h(component, $props)
		var content = document.getElementById("content")
		preact.render(vnode, content, content.firstElementChild)
	})()
	</script>
</body>
</html>
END
}

# выполняет js и возвращает результат
*eval = \&from;
sub from {
	my ($self, $code, $file, $lineno, $data) = @_;
	
	die "данные должны быть хешем" if defined($data) && ref $data ne "HASH";
	
	$file //= "{JE-EVAL}";
	$lineno //= 1;

	
	my $je = $self->je;
	
	while(my ($key, $val) = each %$data) { $je->{$key} = $self->upgrade($val) }
	
	#msg ":cyan space", "jseval", ":magenta", $file, $lineno;
		
	my $parsed = $je->parse($code, $file, $lineno);
	#die "$@" if Isa $@, 'JE::Object::Error';
	die if $@;
	my $ret = $parsed->execute;
	#die "$@" if Isa $@, 'JE::Object::Error';
	die if $@;
	my ($ret) = _je_perl($ret);
	$ret
}

# сериалайзер в js. получает структуру perl, возвращает строку
sub to {
	my ($self, $struct) = @_;
	require JavaScript::Dumper;
	return $struct if Num $struct;
	return JavaScript::Dumper::js_dumper($struct) if ref $struct;
	
	my $x = JavaScript::Dumper::js_dumper([$struct]);
	$x =~ s/^\[(.*)\]$/$1/gs;
	$x
}


1;
