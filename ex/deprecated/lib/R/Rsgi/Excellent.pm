package R::Rsgi::Excellent;
# рутовый сервер

use common::sense;
use R::App;
use R::Coro;

# конструктор
sub new {
	my $cls = shift;
	
	my $con = 1;
	
	bless {
		backend => {
			timeout_recv => 1,
			timeout => 30,
			timeout_send => 1,
		},
		
		timeout_recv => 10,
		timeout => 60,
		timeout_send => 10,
		stdout_con => $con,
		stderr_con => $con,

		@_
	}, ref $cls || $cls;
}

my $name_counter = 0;

# возвращает имя
sub name {
	my ($self) = @_;
	$self->{name} // $self->{pkg} // $self->{yogi} // "Excellent" . $name_counter++
}

# возвращает путь к yogi
sub yogi {
	my ($self) = @_;
	return !defined($self->{path})? (): $self->{path} if exists $self->{path};
	my $yogi = $self->{yogi} // die "нет yogi";
	my @paths = ($yogi, "$yogi.pl", "$yogi.pm");
	my $files = $app->file(@paths);
	my $f = $files->frontdir(".")->add($files->frontdir($app->project->root . "/srv"));
	$f=$f->grep("-e");
	die "не найден файл $yogi" if !$f->length;
	$self->{path} = $f->path
}

# считывает конфигурацию из yogi, без подключения пакета
sub get_conf {
	my ($self) = @_;
	my $x = $app->file($self->yogi)->read;

	my @conf;
	while( $x =~ /\bset\b([^;]+)/g ) {
		push @conf, map { /(\w+)\s*=>\s*(.*)/s? ($1 => $2): () } split /\s*,\s*/, $1;
	}
	
	#msg("не обнаружена конфигурация в " . $self->yogi) if !@conf;
	
	return +{@conf};
}

# устанавливает паремтры, если они не заполнены
sub set_conf_default {
	my ($self, $conf) = @_;
	
	for my $param (keys %$conf) {
		$self->{$param} //= $conf->{$param};
	}
	
	$self
}

# имя пакета
sub pkg {
	my ($self) = @_;
	return $self->{pkg} if $self->{pkg};
	
	my $path = $self->yogi;
	die "нет пакета в файле приложения `$path`" if $app->file($path)->cat !~ /\bpackage\s+([\w:]+)/;
	$self->{pkg} = $1;
	
	require $path;
	
	$self->{pkg}
}

# возвращает роутеры
sub routers {
	my ($self) = @_;
	
	return $self->{routers} if $self->{routers};
	
	my $pkg = $self->pkg;
	
	die "нет роутеров в пакете $pkg" if !$pkg->can("routers");
	die "нет конфигурации в пакете $pkg" if !$pkg->can("configuration");
	
	my $conf = $pkg->configuration;
	
	$self->set_conf_default($conf);
	
	$self->{routers} = $pkg->routers;
}

# запускает сервер
sub run {
	my ($self) = @_;

	#my $conf = eval { $self->pkg->configuration };
	
	$self->{queriers} //= 10;
	$self->{websockets} //= 10;
		
	my $name = $self->name;
	my $routers = $self->routers;
	my $port = $self->{port} //= 3000;
	
	my $fulminant = $self->{fulminant} = $app->fulminant(
		name => $name,
		port => $port,
		timeout_recv => $self->{timeout_recv},
		timeout => $self->{timeout},
		timeout_send => $self->{timeout_send},
		
		ritter => sub {
			my $q = shift;
			$routers->query($q);
		},
		middleware => sub {
			my ($q, $response) = @_;
			$routers->middleware($q, $response)
		},
	)->open;

	$self->{fibers} = $app->coro("$name-querier" => sub {
		$fulminant->accept;
	})->dup($self->{queriers} + $self->{websockets})->immortal->ready;
	
	$self
}

# деструктор
sub DESTROY {
	my ($self) = @_;
	$self->shutdown;
}

# останавливает
sub halt {
	my ($self) = @_;
	$self->{fibers}->mortal->exme->kill, Coro::cede() if $self->{fibers};
	$self->{fulminant}->shutdown if $self->{fulminant};
}

# останавливает
sub shutdown {
	my ($self) = @_;

	$self->{fibers}->mortal->exme->throw([503]), Coro::cede() if $self->{fibers};
	$self->{fulminant}->shutdown if $self->{fulminant};
	
	$self
}

# # запускает сервер разработки
# #@deprecated
# sub dev {
	# my ($self) = @_;
	# $self->run;	
	# $app->process->autoreload;
# }

# запускает сервер разработки в отдельном процессе
sub devrun {
	my ($self, $jsx) = @_;
	
	$self->set_conf_default($self->get_conf);
	
	#$self->{stdout_con} = $self->{stderr_con} = 1;
	
	# сервер разработки
	my %data = (%$self, %{$self->{backend}});
	delete $data{backend};
	
	
	my $development_port = $data{port} = $self->{backend}{port} // $data{port}+1;
	$data{name} = $self->name . "-BACKEND";
	my $dev = $app->process($data{name} => '
		my $excellent = $app->rsgiExcellent(%{'.$app->perl->dump(\%data).'});
		$excellent->run;
		waits while 1;
		undef $excellent;
	');
	
	$dev->stdout if !$self->{stdout_con};
	$dev->stderr if !$self->{stderr_con};
	
	$dev->waitrun;
		
	my $last_error;
	
	# поднимаем проксирующий сервер
	$self->{routers} = $app->routers->new;
	
	# вебсокет для разработки: авторелоада и т.д.
	my @sockets;
	$self->{routers}->websocket("/__dev__" => 
		connect => sub {
			# при коннекте добавляем сокет в очередь
			push @sockets, $Coro::current->{request};
		},
		disconnect => sub {
			# а при дисконнекте - удаляем
			@sockets = grep { $Coro::current->{request} != $_ } @sockets;
		},
	);
	
	# любой другой запрос должен быть перенаправлен на бакенд
	$self->{routers}->route("*path" => sub {
		my $q = shift;
		
		# $app->log->info( ":space", ":white", "start", ":magenta", $q->via, ":cyan", $q->location );
		my $time = Time::HiRes::time();
		
		$self->{script_autoreload} //= $app->framework->file("node/autoreload.html")->read;
		
		# если сервер не отвечает: выводим последнюю ошибку
		if(!$dev->exists) {
			#msg1 "no dev exists";
			my $err = $dev->out.$dev->err;
			$last_error = $err if $err ne "";
			$last_error = "-- нет сообщения об ошибке --" if $last_error eq "";
			$app->log->info(":space red", "  ", 500, ":black bold", "dev not ok", ":reset");
			return [500, ["Content-Type" => "text/html; charset=utf-8"], $self->_ansi($last_error).$self->{script_autoreload}];
		}
		
		# иначе отправляем запрос:
		my $result = eval {
			$q->url->noredirect->port($development_port)->method($q->via)->header(%{$q->{headers}})->data($q->body)->res;
		};
		if($@) {
			$app->log->info(":space red", "  ", 503, "-> $@");
			return [503, ["Content-Type" => "text/html; charset=utf-8"], $self->_ansi("connect{" . $self->name . "}: " . $app->raise->last."")];
		}
		
		my $content = $result->content;
		
		# считываем вывод, который был в серваке и прибавляем
		if( $result->content_type =~ /^text\/html$/i && !grep2 { $a =~ /^Location$/i } $result->headers->flatten ) {

			# html файлы должны быть всегда в utf-8!
			utf8::decode($content);
		
			my $out = $dev->out.$dev->err;
			if(length $out) {
				#msg1 "add out";
				$out = $self->_ansi($out);
				$content = $out . $content;
			}
			
			# добавляем ауторелоад
			$content .= $self->{script_autoreload};
		}
		
		#, ":white", "end ",
		$app->log->info(":space", ":magenta", $q->via, ":cyan", $q->location, ":reset", $app->perl->fsec(Time::HiRes::time() - $time), ":green", $result->code, $result->content_length, $result->content_type, $result->content_charset);
		return [
			$result->code,
			[ grep2 { $a !~ /^Content-Length$/i } $result->headers->flatten ],
			$content
		];
	});
	
	# запускаем
	$self->run;
	
	# логируем:
	$app->log->info(":empty white", "start on :", ":red", $self->{port}, ":black bold", " with ", ":blue", ":", $development_port, ":reset", $dev->exists? (":green", " ok"): (":red", " fail"));
	
	# определяем каталоги для слежения
	my $f = $app->file(
		$app->project->lib, 
		$app->project->files("src"), 
		$app->project->files("node/css"), 
		$self->yogi,
	)->watchify;
	#my $mtime = time;
	
	while() {
		sleep 1;
		
		# если были изменения в файлах - перезагружаем
		# если упал - ждём изменения в файлах
		#if($f->find->grep(sub { $_ =~ /(pm|pl)$/n || -d $_ })->maxmtime > $mtime) {
		if($f->watch->modified->grep(sub { /\.(pm|pl)$/ || !/\.\w+$/ })->length) {
			#$mtime = time;
			$dev->stop->waitrun;
			my $active = $dev->exists;
			$app->log->info(":empty black bold", "restart", $active? (":green", " ok"): (":red", " fail"));
			if($active) {
				$_->emit("autoreload") for @sockets;
			}
		}
		
		# дополнитльные процессы (jsx). Если от них был вывод, то сбрасываем джаваскрипт
		if($jsx) {
			my $x = $jsx->out . $jsx->err;
			if(length $x) {
				$dev->kill("USR1");
				print $x;
				$_->emit("autoreload") for @sockets;
			}
		}
		
	}
	
	$self
}

# хелпер для вывода окошка ошибки
sub _ansi {
	my ($self, $err) = @_;
	$app->html->ansi($err, "border: solid 2px #7B68EE")
}

# устанавливаем HUP
our $HUP = AE::signal "USR1" => sub {
	msg1 "DEV HUP!!!";
	$app->javascript->clear;
};

1;
