package R::Rsgi::Querier;
# запросник

use common::sense;
use R::App;

# конструктор
sub new {
	my $cls = shift;
	bless {
		cookie => undef,
		@_
	}, ref $cls || $cls;
}

# "посылает" запрос
sub request {
	my ($self, $via, $location, $headers, $data) = @_;
	
	$self->{routers}->query($via, $location, $headers, $data);
}

# запрос с указанным методом
sub get {
	my $self = shift;
	$self->request("GET", @_)
}

# запрос с указанным методом
sub post {
	my $self = shift;
	$self->request("POST", @_)
}

# запрос с указанным методом
sub put {
	my $self = shift;
	$self->request("PUT", @_)
}

# запрос с указанным методом
sub del {
	my $self = shift;
	$self->request("DELETE", @_)
}

# запрос с указанным методом
sub options {
	my $self = shift;
	$self->request("OPTIONS", @_)
}

# запрос с указанным методом
sub patch {
	my $self = shift;
	$self->request("PATCH", @_)
}

# запрос с указанным методом
sub head {
	my $self = shift;
	$self->request("HEAD", @_)
}

# ajax-запрос
sub ajax {
	my ($self, $location, $headers, $data) = @_;
	my $header = $app->rsgiBag($headers)->Add("X-Requested-With"=>"XMLHttpRequest");
	$self->request("POST", $location, $header, $data);
}

1;