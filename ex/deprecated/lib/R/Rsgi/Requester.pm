package R::Rsgi::Requester;
# запросник

use base R::Rsgi::Querier;

use common::sense;
use R::App;

# конструктор
sub new {
	my $cls = shift;
	my $self = $cls->SUPER::new(@_);
	$self
}

# "посылает" запрос
sub request {
	my ($self, $via, $location, $headers, $data) = @_;
	
	$self->{routers}->request($via, $location, $headers, $data);
}


1;