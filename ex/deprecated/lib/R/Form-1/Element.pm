package R::Form::Element;
# элемент. Получает

use common::sense;
use R::App;

use R::Form::Error;
use R::Form::Input;
use R::Form::Label;
use R::Form::Element::Preview;

# свойства
has qw/form widget name/;

# конструктор
sub new {
	my $cls = shift;
	my $self = bless {@_}, ref $cls || $cls;
	
	weaken $self->{form};
	
	$self
}

# возвращает id
sub id {
	my ($self) = @_;
	join "-", $self->{form}->id, $self->{name};
}

# возвращает значение с формы
sub value {
	my ($self) = @_;
	$self->{form}{val}{$self->{name}}
}

# устанавливает значения для выбора
sub options {
	my ($self, $options) = @_;
	$self->{options} = [map {
		Is($_, "ArrayRef")? +{
			text => $_->[0], 
			value => $_->[1], 
			defined($_->[2])? (selected=>$_->[2]): ()
		}:
		Is($_, "HashRef")? do {
			my $x = {};
			@$x{qw/text value/} = delete @$_{qw/text value/};
			$x->{selected} = delete $_->{selected} if exists $_->{selected};
			die "нераспознанные ключи в options: ".join ", ", %$_ if keys %$_;
			$x
		}:
		die "что-то странное в options";
	} @$options];
	$self
}

# устанавливает валидаторы
sub is {
	my ($self, $is, $error) = @_;
	push @{$self->{is}}, $is;
	push @{$self->{is_err}}, $error;
	$self
}

# валидирует значение на форме по установленным валидаторам
sub _valid {
	my ($self) = @_;
	my $val = $self->{form}{val}{$self->{name}};
	my $i = 0;
	for my $is (@{$self->{is}}) {
		if(!Is $val, $is) {
			my $msg = $self->{is_err}[$i] // R::Subtype::get_msg($is);
			$self->{form}{error}{$self->{name}} = R::Subtype::fmt_msg($msg, $val, $is);
			return 0;
		}
	}
	continue { $i++ }
	return 1;
}

# проверяет, что нет ошибки
sub is_valid {
	my ($self) = @_;
	!$self->{form}{error}{$self->{name}}
}

# устанавливает обработчики js
sub on {
	my ($self, $names, $code) = @_;
	push @{$self->{on}}, map { $_ => $code } split /\s+/, $names;
	$self
}

# возвращает представление метки
sub label {
	my ($self) = @_;
	$self->{label} //= R::Form::Label->new(element => $self)
}

# возвращает представление поля ввода. Однако при рендере инпута вызывается рендер элемента и наоборот
*tag = \&input;
sub input {
	my ($self) = @_;
	$self->{input} //= R::Form::Input->new(element => $self)
}

# возвращает представление описания ошибки
sub error {
	my ($self) = @_;
	$self->{error} //= R::Form::Error->new(element => $self)
}

# возвращает представление превью элемента
sub preview {
	my ($self) = @_;
	$self->{preview} //= R::Form::Element::Preview->new(element => $self)
}

# рендерит представления поля ввода
sub render {
	my ($self) = @_;
	my $widget = $self->{widget} // "rubin-forms/widgets/input";
	$widget? $app->templateMid->render($widget, +{input => $self->input}): ""
}

1;