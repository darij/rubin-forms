package R::Form::Error;
# представление ошибки

use base R::Form::Element::Widget;

use common::sense;
use R::App;

has qw/was_render/;

# значение ошибки
sub value {
	my ($self) = @_;
	$self->{element}{form}{error}{$self->{element}{name}}
}

# рендерит ошибку
sub render {
	my ($self) = @_;
	return "" if !defined $self->value;
	my $widget = $self->{widget} // $self->{element}{form}{error_widget} // "rubin-forms/widgets/error";
	$self->{was_render} = !!$widget;
	$widget? $app->templateMid->render($widget, {error => $self}): ""
}

1;