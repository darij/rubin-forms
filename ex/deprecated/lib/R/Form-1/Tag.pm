package R::Form::Tag;
# тег - используется для представлений формы: ошибок, информации и превью

use common::sense;
use R::App;

# свойства
has qw//;

# конструктор
sub new {
	my $cls = shift;
	my $self = bless {@_}, ref $cls || $cls;
	weaken $self->{form};
	$self
}

# рендерит представление
sub render {
	my ($self) = @_;
	$app->templateMid->render($self->{widget}, {form => $self->{form}});
}

1;