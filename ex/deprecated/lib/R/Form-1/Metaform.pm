package R::Form::Metaform;
# метаформа - содержит элементы

use common::sense;
use R::App;

use R::Form::Element;

# конструктор
sub new {
	my $cls = shift;
	my $self = bless {
		form => undef,		# форма
		@_
	}, ref $cls || $cls;
	
	$self
}


# устанавливает свойства метаформы
my %PROPS = qw/
	widget		widget
	xwidget		element_widget
	iwidget		preview_widget
	lwidget		label_widget
	ewidget		error_widget
/;
sub meta {
	my $self = shift;
	
	for(my $i=0; $i<@_; $i+=2) {
		my ($k, $v) = @_;

		if($k =~ /^on/) {
			$self->{form}{attrs}{$k} = $v;
			next;
		}
		
		$self->{form}->preview->widget($v), next if $k eq "preview";
		$self->{form}->errors_tag->widget($v), next if $k eq "errors_tag";
		$self->{form}->info_tag->widget($v), next if $k eq "info_tag";
		$self->{form}->form_tag->widget($v), next if $k eq "form_tag";
		
		die "свойство $k нельзя устанавливать в meta формы" if !$PROPS{$k};		
		$self->{form}{$PROPS{$k}} = $v;
	}
	
	$self
}

# возвращает последний элемент
sub last {
	my ($self) = @_;
	my $e = $self->{form}{elements};
	$e->[$#$e]
}

#@@category группировка полей

# открывает группу элементов
sub fieldset {
	my ($self, $legend) = @_;
	TODO;
	$self
}

#@@category поля ввода

# поле ввода
sub input {
	my ($self, $name, $label, $placeholder) = @_;
	
	die "у инпута нет имени" if !length $name;
	
	my $form = $self->{form};
	
	die "инпут $name уже есть" if exists $form->{element}{$name};
	
	my $element = R::Form::Element->new(form => $form, name => $name);
	
	$element->label->value($label);
	$element->input->placeholder($placeholder);
	
	push @{$form->{elements}}, $element;
	$form->{element}{$name} = $element;
	
	$self
}

# скрытое поле
sub password {
	my $self = shift;
	$self->input(@_)->type("password");
	$self
}

# текстовая зона
sub textarea {
	my $self = shift;
	$self->input(@_)->widget("rubin-forms/widgets/input/textarea");
	$self
}

# устанавливает мультиплет
sub multiple {
	my ($self) = @_;
	$self->last->input->{multiple} = 1;
	$self->last->input->attrs(multiple => undef);
	$self
}

# выбор
sub select {
	my ($self, $name, $label, $initial) = @_;
	$self->input($name, $label)->widget("rubin-forms/widgets/input/select");
	$self->multiple if Is $initial, "ArrayRef";
	$self->initial($initial) if $initial;
	$self
}

# указывает выбор на поле
sub options {
	my ($self, $options) = @_;
	$self->last->options($options);
	$self
}

# скрытое информационное поле
sub hidden {
	my ($self, $name, $initial) = @_;
	$self->input($name)->
		type("hidden")->
		lwidget(0)->
		iwidget(0)->
		ewidget(0);
	
	$self->initial($initial) if @_>2;
	$self
}

# галочка
sub checkbox {
	my ($self, $name, $label, $placeholder) = @_;
	$self->input($name, $placeholder, $label)->type("checkbox")->widget("rubin-forms/widgets/input/checkbox");
}


#@@category кнопки

# кнопка c именем и таким же текстом, если не указан другой в placeholder
sub button {
	my ($self, $name, $placeholder) = @_;
	
	$self->input($app->magnitudeLiteral->wordname($name), undef, $placeholder // $name)->
		type("button")->
		widget("rubin-forms/widgets/input/button")->
		iwidget(0);
	$self
}

# устанавливает кнопку с типом и именем submit
sub submit {
	my ($self, $remark) = @_;
	$self->button(submit => $remark)->type("submit");
	$self
}

# устанавливает кнопку с типом и именем reset
sub reset {
	my ($self, $remark) = @_;
	$self->button(reset => $remark)->type("reset");
	$self
}

#@@category пейджеры

# пейджер
sub pager {
	my ($self, $name, $form) = @_;
	$self->input($name)->
		widget("rubin-forms/widgets/input/pager")->
		iwidget("rubin-forms/widgets/input/pager_preview")->
		lwidget(0)->
		ewidget(0);
	if($form) {
		$self->last->{form} = $form;
		$self
	} else {
		my $form_name = "$self->{form}{name}__$name";
		$form = $app->form->$form_name;
		$self->last->{row_form} = $form;
		my $meta = $form->Meta;
		$meta->{parent_meta} = $self;
		$meta
	}
}

# заканчивает описание вложенной формы
sub end {
	my ($self) = @_;
	delete $self->{parent_meta}
}

#@@category свойства элементов

# тип элемента
sub type {
	my ($self, $type) = @_;
	$self->last->input->type($type);
	$self
}

# шаблон элемента
sub хwidget {
	my ($self, $widget) = @_;
	$self->last->input->widget($widget);
	$self
}

# шаблон инпута
sub widget {
	my ($self, $widget) = @_;
	$self->last->widget($widget);
	$self
}

# шаблон для лейбла
sub lwidget {
	my ($self, $widget) = @_;
	$self->last->label->widget($widget);
	$self
}

# шаблон для ошибки элемента
sub ewidget {
	my ($self, $widget) = @_;
	$self->last->error->widget($widget);
	$self
}

# шаблон для предпросмотра элемента
sub iwidget {
	my ($self, $widget) = @_;
	$self->last->preview->widget($widget);
	$self
}

# инициализирует значение
sub initial {
	my ($self, $default) = @_;
	my $name = $self->last->name;
	
	$self->{form}{initial}{$name} = $default;
	
	$self
}

# устанавливает доп.-аттрибуты, вроде класса
sub attrs {
	my $self = shift;
	%{$self->last->input->attrs} = @_;
	$self
}

# устанавливает атрибут class
sub class {
	my $self = shift;
	$self->attrs(class => join " ", @_)
}

# дисаблит элемент
sub disabled {
	my ($self) = @_;
	$self->last->disabled;
	$self
}

# только для чтения
sub readonly {
	my ($self) = @_;
	$self->last->readonly;
	$self
}

# делает элемент превьюшкой
sub preview {
	my ($self) = @_;
	
	TODO; #!!!
	
	$self->last->preview;
	$self
}

#@@category валидаторы

# устанавливает валидатор
sub is {
	my ($self, $subtype, $error) = @_;
	$self->last->is($subtype, $error);
	$self
}

# требуется хоть один символ
sub need {
	my ($self, $error) = @_;
	$self->is("Need", $error);
}

# регулярка
my %REG;
sub regex {
	my ($self, $regex, $message) = @_;
	$regex = qr/$regex/ if ref $regex ne "Regexp";
	my $type = $REG{$regex} //= subtype("input_".$self->last->name."_regex", $regex, $message);
	
	$self->last->is($type);
	$self
}


#@@category js

# устанавливает обработчик js
sub on {
	my ($self, $on, $js) = @_;
	$self->last->on($on, $js);
	$self
}

# Потеря фокуса.
sub onblur {
	my ($self, $js)=@_;
	$self->on("blur", $js);
	$self
}


# Изменение значения элемента формы
sub onchange {
	my ($self, $js)=@_;
	$self->on("change", $js);
}


# Щелчок левой кнопкой мыши на элементе
sub onclick {
	my ($self, $js)=@_;
	$self->on("click", $js);
}

# копирование
sub oncopy {
	my ($self, $js)=@_;
	$self->on("copy", $js);
}

# вырезание
sub oncut {
	my ($self, $js)=@_;
	$self->on("cut", $js);
}

# Двойной щелчок левой кнопкой мыши на элементе
sub ondblclick {
	my ($self, $js)=@_;
	$self->on("dblclick", $js);
}

# Получение фокуса
sub onfocus {
	my ($self, $js)=@_;
	$self->on("focus", $js);
}

# ввод в поле
sub oninput {
	my ($self, $js)=@_;
	$self->on("input", $js);
}


# Клавиша нажата, но не отпущена
sub onkeydown {
	my ($self, $js)=@_;
	$self->on("keydown", $js);
}


# Клавиша нажата и отпущена
sub onkeypress {
	my ($self, $js)=@_;
	$self->on("keypress", $js);
}


# Клавиша отпущена
sub onkeyup {
	my ($self, $js)=@_;
	$self->on("keyup", $js);
}


# Документ или картинка загружена
sub onload {
	my ($self, $js)=@_;
	$self->on("load", $js);
}


# Нажата левая кнопка мыши
sub onmousedown {
	my ($self, $js)=@_;
	$self->on("mousedown", $js);
}


# Перемещение курсора мыши
sub onmousemove {
	my ($self, $js)=@_;
	$self->on("mousemove", $js);
}


# Курсор покидает элемент
sub onmouseout {
	my ($self, $js)=@_;
	$self->on("mouseout", $js);
}


# Курсор наводится на элемент
sub onmouseover {
	my ($self, $js)=@_;
	$self->on("mouseover", $js);
}


# Левая кнопка мыши отпущена
sub onmouseup {
	my ($self, $js)=@_;
	$self->on("mouseup", $js);
}


# Форма очищена
sub onreset {
	my ($self, $js)=@_;
	$self->on("reset", $js);
}

# вставка
sub onpaste {
	my ($self, $js)=@_;
	$self->on("paste", $js);
}

# Выделен текст в поле формы
sub onselect {
	my ($self, $js)=@_;
	$self->on("select", $js);
}


# Форма отправлена
sub onsubmit {
	my ($self, $js)=@_;
	$self->on("submit", $js);
}


# Закрытие окна
sub onunload {
	my ($self, $js)=@_;
	$self->on("unload", $js);
}




1;