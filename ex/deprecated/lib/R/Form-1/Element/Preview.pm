package R::Form::Element::Preview;
# представление предпросмотра элемента

use base R::Form::Element::Widget;

use common::sense;
use R::App;

# value из элемента
sub value {
	my ($self) = @_;
	$self->{element}->input->{type} eq 'password'? '******': $self->{element}->value
}

# рендерит предпросмотр
sub render {
	my ($self) = @_;
	my $widget = $self->{widget} // $self->{element}{form}{element_preview_widget} //
		"rubin-forms/widgets/element/preview";
	$widget? $app->templateMid->render($widget, {preview => $self}) : ""
}

1;