package R::Form::Element::Pager;
# модуль

use base R::Form::Element;

use common::sense;
use R::App;

# свойства
has qw//;

# конструктор
sub new {
	my $cls = shift;
	bless {@_}, ref $cls || $cls;
}

1;