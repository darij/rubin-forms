package R::Form::Element::Widget;
# базовый класс для input, label и error

use common::sense;
use R::App;

# свойства
has qw/element widget/;

# конструктор
sub new {
	my $cls = shift;
	my $self = bless {@_}, ref $cls || $cls;
	weaken $self->{element};
	$self
}

# id элемента
sub id {
	my ($self) = @_;
	$self->{element}->id
}

# имя
sub name {
	my ($self) = @_;
	$self->{element}->name
}

# форма
sub form {
	my ($self) = @_;
	$self->{element}{form}
}

# свойство атрибутов
sub attrs {
	my $self = shift;
	
	!@_ && return $self->{attrs};
	
	if(@_==1 && Is $_[0], "ArrayRef") {
		push @{$self->{attrs}}, @{$_[0]};
	}
	elsif(@_==1 && Is $_[0], "HashRef") {
		push @{$self->{attrs}}, %{$_[0]};
	}
	else {
		push @{$self->{attrs}}, @_;
	}
	
	$self
}

1;