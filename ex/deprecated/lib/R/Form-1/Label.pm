package R::Form::Label;
# метка

use base R::Form::Element::Widget;

use common::sense;
use R::App;

has qw/value/;


# рендерит метку
sub render {
	my ($self) = @_;
	return "" if !length $self->{value};
	my $widget = $self->{widget} // $self->{element}{form}{label_widget} // "rubin-forms/widgets/label";
	$widget? $app->templateMid->render($widget, {label => $self}): ""
}

1;