package R::Form::Form;
# базовая форма

use common::sense;
use R::App qw/$app/;
use R::Form::Metaform;

R::has qw/name val/;

## инициализированные формы складываются сюда
#our %Meta;

# конструктор
sub new {
	my ($cls, $val) = @_;
	
	$cls = ref $cls || $cls;
	
	my $self = bless {
		name => ($cls =~ /([^:]+)$/ && lcfirst $1),
		val => {},			# значения полей формы
		element => {},		# элементы формы по имени
		elements => [],		# элементы формы по порядку
		error => {},		# ошибки по полям формы
		errors => [],		# ошибки самой формы
		info => [],			# информация
		widget => "rubin-forms/widgets/form/middle",			# шаблон для показа середины формы
		initial => {},		# инициализирующие значения
		instance => undef,	# роусет, который следует показать
	}, $cls;
	
	# создаём элементы формы
	my $setup = $cls->can("setup");
	$setup->( $self->Meta ) if $setup;
	
	$self->launch($val);
	
	$self
}

# возвращает метаформу для описания
sub Meta {
	my ($self) = @_;
	R::Form::Metaform->new(form => $self)
}

# возвращает текущий запрос из себя или $Coro::current, так же - устанавливает
sub request {
	my ($self) = @_;
	if(@_ > 1) { $self->{request} = $self; $self }
	else { $self->{request} // $Coro::current->{request} }
}

# возвращает сессию
sub session {
	my ($self) = @_;
	$self->request->session
}

# возвращает респонсе
sub response {
	my ($self) = @_;
	$Coro::current->{response} //= $app->rsgiResponse->new_def
}

# возвращает id формы
sub id {
	my ($self) = @_;
	$self->{id} // $self->{name}
}

# возвращает атрибуты формы в виде хеша
sub attrs {
	my ($self) = @_;
	$self->{attrs}
}

# возвращает элементы формы
sub elements {
	my ($self) = @_;
	wantarray? @{$self->{elements}}: $self->{elements}
}

# возвращает объект для предпросмотра данных формы
sub preview {
	my ($self) = @_;
	$self->{preview} //= $app->formTag(form => $self, widget => "rubin-forms/widgets/form/preview")
}

# тег с ошибками
sub errors_tag {
	my ($self) = @_;
	$self->{errors_tag} //= $app->formTag(form => $self, widget => "rubin-forms/widgets/form/errors")
}

# тег с информацией
sub info_tag {
	my ($self) = @_;
	$self->{info_tag} //= $app->formTag(form => $self, widget => "rubin-forms/widgets/form/info")
}

# тег с полной формой
sub form_tag {
	my ($self) = @_;
	$self->{form_tag} //= $app->formTag(form => $self, widget => "rubin-forms/widgets/form")
}

# рендерит сердцевину формы
sub render {
	my ($self) = @_;
	$app->templateMid->render($self->{widget}, {form => $self})
}

# алгоритм отправки формы
sub _valid {
	my ($self) = @_;
	
	$self->submit;
	
	$_->_valid for $self->elements;
	
	if($self->is_valid) {
		$self->ok;
	} else {
		$self->fail;
	}
	
	$self
}

# val=undefined|""|{} - инициализируем
# val={...} - субмитим
# val=request - из указанного реквеста
# val=1 - загрузить из текущего реквеста
sub launch {
	my ($self, $val) = @_;
	
	$self->clear_info;
	
	my $submit;
	
	if(!defined $val) {}
	elsif(!ref $val && $val eq "") {}
	elsif(ref $val eq "HASH") {
		$submit = keys %$val;
	}
	elsif(!ref $val && $val == 1) {
		$val = $self->request->data->All;
		$submit = $self->request->via eq "POST";
	}
	elsif(Isa $val, "R::Rsgi::Request") {
		$submit = $val->via eq "POST";
		$val = $val->data->All;
	}
	else {
		die "странные данные переданы в launch формы";
	}
	
	if($submit) {
		$self->{val} = $val;
		$self->_valid;
		$self->_preload if $self->is_valid;
	}
	else {
		$self->_preload;
	}
	
	$self
}

# алгоритм предзагрузки данных для показа формы
sub _preload {
	my ($self) = @_;
	my @select = $self->select;
	$self->{val} = +{%{$self->{initial}}, @select == 1 && ref $select[0]? %{$select[0]}: @select};
	$self
}

# событие на показ формы - заполняет её данными
sub select {}

# событие на отправку формы
sub submit {}

# событие срабатывает, когда форма прошла валидацию
sub ok {}

# событие срабатывает, когда на форме есть ошибки
sub fail {}

# очищает ошибки и info
sub clear_info {
	my ($self) = @_;

	my $errors = $self->{errors};
	my $error = $self->{error};
	my $info = $self->{info};
	@$errors = %$error = @$info = ();
	
	$self
}

# есть ошибки
sub is_errors {
	my ($self) = @_;
	scalar( @{$self->{errors}} || keys %{$self->{error}} )
}

# форма валидна
sub is_valid {
	my ($self) = @_;
	!$self->is_errors
}

# возвращает информацию
sub info {
	my $self = shift;
	if(@_) {
		push @{$self->{info}}, @_;
		$self
	} else {
		wantarray? @{$self->{info}}: $self->{info}
	}
}

# возвращает ошибки формы
sub errors {
	my ($self) = @_;
	wantarray? @{$self->{errors}}: $self->{errors}
}

# добавляет ошибку
sub error {
	my $self = shift;
	push @{$self->{errors}}, @_;
	$self
}

# возвращает ошибки формы и полей которые ещё не были отрендерены
sub general_errors {
	my ($self) = @_;
	return [@{$self->{errors}}, R::map2 { my $e = $self->{element}{$a}; $e->{error} && $e->error->{was_render}? (): "$a: $b" } %{$self->{error}}]
}

# создаём метод, если ему соответствует элемент
sub AUTOLOAD {
	my ($self) = @_;
	
	our $AUTOLOAD =~ /([^:]+)$/;
	my $prop = $1;
	
	die "у формы $self->{name} нет элемента $prop" if !exists $self->{element}{$prop};
	
	my $eval = "sub $AUTOLOAD { shift->{element}{$prop} }";
	eval $eval;
	die if $@;
	my $sub = *{$AUTOLOAD}{CODE};
	
	goto &$sub;
}

# деструктор
sub DESTROY {
	# my ($self) = @_;
	# R::msg1 ":on_red yellow", int($self), $self->{name}, ":on_cyan magenta", "FORM DESTROY!"
}

1;