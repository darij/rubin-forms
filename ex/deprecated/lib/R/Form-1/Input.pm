package R::Form::Input;
# инпут

use base R::Form::Element::Widget;

use common::sense;
use R::App;

has qw/type placeholder/;

# value из элемента
sub value {
	my ($self) = @_;
	$self->{element}->value
}

# возвращает опции
sub options {
	my ($self) = @_;
	$self->{element}{options}
}

# возвращает атрибут selected для опции
sub selected {
	my ($self, $option) = @_;
	return if !$self->{multiple};
	my $v = $self->value;
	$option->{value} ~~ $v? " selected": ()
}

# рендерит весь элемент
sub render {
	my ($self) = @_;
	my $widget = $self->{widget} //
		$self->{element}{form}{element_widget} // "rubin-forms/widgets/element";
	$widget? $app->templateMid->render($widget, {element => $self->{element}}): ""
}

1;