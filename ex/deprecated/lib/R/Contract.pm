package R::Contract;
# контрактное программирование

use common::sense;
use R::App;

#use Exporter 'import';
#our @EXPORT = qw(in out body invariant RETURN assert);

our %EXPORT = map {$_ => 1} qw/in out body invariant RETURN assert/;

# импортирует символы
sub import {
	my $self = shift;
	my $caller = caller;
	
	for my $name (@_? @_: keys %EXPORT) {
		*{"${caller}::$name"} = \&$name;
	}
	
	$self;
}

# конструктор
my $singleton = bless { dev => 1 }, __PACKAGE__;
sub new {
	my $cls = shift;
	%$singleton = (%$singleton, @_);
	$singleton
}

### Контрактное программирование ###

# проверяем условие
sub assert ($;$) {
	my ($condition, $message) = @_;
	
	die $message // "ASSERT" if !$condition;
	
	return;
}

# добавляем блок в инвариант
sub invariant (&) {
	my ($code) = @_;
	
	return if !$singleton->{dev};
	
	my $package = caller;

	die "уже один инвариант установлен" if *{"${package}::__INVARIANT__"}{CODE};
	
	${"${package}::__CONTRACT__"}{INVARIANT} = $code;
	
	return;
}

# возвращаемое значение
sub RETURN() {}
my $EMPTY_RETURN = \&RETURN;
my $EMPTY_INVARIANT = sub(&) {};


# устанавливает предусловие подпрограммы
sub in (&;@) {
	my ($code) = @_;
	
	return if !$singleton->{dev};
	
	my ($package, $file, $lineno, $ref) = caller(1);
	
	$package = $ref;
	local ($`, $', $&);
	$package =~ s/::\w+$//;
	
	${"${package}::__CONTRACT__"}{$ref}{IN} = $code;
	
	return;
}

# устанавливает постусловие подпрограммы
sub out (&;@) {
	my ($code) = @_;
	
	return if !$singleton->{dev};
	
	my ($package, $file, $lineno, $ref) = caller(1);
	
	$package = $ref;
	local ($`, $', $&);
	$package =~ s/::\w+$//;
	
	${"${package}::__CONTRACT__"}{$ref}{OUT} = $code;
	
	return;
}

# возвращает аргументы предпредпоследнего вызова
package DB {
	sub __CONTRACT__ARGS__ {
		my @a = caller(2);
		return @DB::args;
	}
}

# устанавливает тело подпрограммы
sub body (&;@) {
	my ($code) = @_;
	
	my ($package, $file, $lineno, $ref) = caller(1);
	
	if(!$singleton->{dev}) {
	
		*$ref = $code;
		
	}
	else {
	
		my ($package, $file, $lineno, $ref) = caller(1);
	
		$package = $ref;
		local ($`, $', $&);
		$package =~ s/::\w+$//;
	
		my $contract = \%{"${package}::__CONTRACT__"};
		my $return = $ref;
		$return =~ s/\w+$/RETURN/;
	
		*$ref = (sub {
			my ($return, $contract, $in, $out, $body) = @_;
			sub {

				# берётся тут, т.к. может быть установлен после
				my $invariant = $contract->{INVARIANT} // $EMPTY_INVARIANT;
				
				if(wantarray) {
					()=$in->(@_);
					()=$invariant->(@_);
					my $RETURN = [ $body->(@_) ];
					*$return = (sub { my ($args) = @_; sub() { @$args } })->($RETURN);
					()=$invariant->(@_);
					()=$out->(@_);
					*$return = $EMPTY_RETURN;
					return @$RETURN;
				}
				else {
					scalar $in->(@_);
					scalar $invariant->(@_);
					my $RETURN = scalar $body->(@_);
					*$return = (sub { my ($arg) = @_; sub() { $arg } })->($RETURN);
					scalar $invariant->(@_);
					scalar $out->(@_);
					*$return = $EMPTY_RETURN;
					return $RETURN;
				}
			}
		})->(
			$return,
			$contract,
			$contract->{$ref}{IN} // $EMPTY_INVARIANT,
			$contract->{$ref}{OUT} // $EMPTY_INVARIANT,
			$code
		);
	}
	
	#msg1 "args:", &DB::__CONTRACT__ARGS__;
	
	@_ = &DB::__CONTRACT__ARGS__;
	goto &$ref;
}



1;