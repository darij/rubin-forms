package R::Site;
# сервер с роутингом

use base R::Fulminant;

use common::sense;
use R::App;

# свойства
has qw/dev rename action access xf filemap/;

# конструктор
sub new {
	my $cls = shift;
	
	my $self = $cls->SUPER::new(
		dev => $app->ini->{dev},		# если dev - то проверять время изменения
		rename => $app->router->new,	# qr// => new_path - обработчики роутинга
		filemap => $app->fileMap->new,
		# /path => package - роутинг контроллеров
		action => $app->siteController->new,	
		# html/path => package - .access.pl. Файлы конфигурации для каталога и подкаталогов
		access => $app->siteAccess->new,	
		xf => $app->xf->new,		# шаблоны сайта
		stash => $app->siteStash->new,			# stash
		@_
	);
	
	$self->action->site($self);
	$self->access->site($self);
	$self->xf->filemap($self->filemap)->dev($self->dev);
	$self->{stash}->site($self);
	
	# подгружаем карту файлов
	if(!$self->{dev}) {
		for my $f ($app->file("html")->find("-f")->slices) {
			if($f->ext eq "pl") {
				if($f->file eq ".access.pl") {
					$self->access->load($f->path);
				}
				else {
					$self->action->load($f->path);
				}
			}
			elsif($f->ext eq "xf") {
				$self->xf->load($f->path);
			}
			else {
				$self->filemap->set($f->path, 1);
			}
		}
	}
	
	$self
}

# рыцарь - обработчик запроса
sub ritter {
	my ($self, $request) = @_;
	
	
	my $path = $request->path;
	
	# смена путей:
	if(defined(my $new_path = $self->{rename}->get($path))) {
		$request->path($path = $new_path)->attr->Add(%+);
	}
	
	# трансформируем путь
	my @path = split m!/!, $path;
	$path[0] = "html";
	
	# добавляем stash
	$request->{stash} = bless {%{$self->{stash}}}, "R::Site::Stash";
	
	# доступ
	my $access = $self->access->get(@path==1? ["html", "?"]: \@path);
	if(defined $access) {
		$access->new(request=>$request, site=>$self)->ACCESS;
	}
	
	# статика
	if($path =~ m!\.(\w+)\z!) {
		# в режиме dev ищем на диске
		my $is_file = $self->{dev}? -e "html$path": $self->filemap->get(\@path) == 1;
		return [404] unless $is_file;
		return $app->file("html$path");
	}
	
	# перевод на .../ - устарел. В процессе
	if($path !~ m!/\z!) {
		return [307, [Location => $request->url->path("$path/")->location, "Content-Type" => "text/plain"], "307 Redirect on path/"];
	}
	
	my $controller;
	if($path eq "/") {
		$path[1] = "index.pl";
		$controller = $self->action->get(\@path);
	} else { 
		$path[$#path] .= ".pl";
		$controller = $self->action->get(\@path);
		if(!$controller) {	# пробуем подгрузить из /index.pl
			$path[$#path] =~ s/.pl\z//;
			$controller = $self->action->get([@path, "index.pl"]);
		}
	}
	
	return [404] if !defined $controller;

	return $controller->new(request => $request, site => $self)->run;
}

# находит и преобразует ответ
sub middleware {
	my ($self, $request, $response) = @_;
	
	my $status = $response->status;
	my $controller = $request->{stash}->{error}->{$status};
	if(!$controller) { $controller = $request->{stash}->{error}->{substr $status, 0, 1}; }
	
	if(ref $controller eq "SCALAR") {
		$request->path($controller);
		$response = $self->ritter($request);
	}
	elsif(defined $controller) {
		# if($self->{request}->header->Accept =~ m!\b text/html \b!x) {
		my $html = $self->xf->render_with_layout($controller, $request->{stash}->{layout}, {});
		$response->data($html);
		# }
		# else {
			# my $msg = "" . $response->data;
			# $response->data({status => $response->status, error => $msg});
		# }
	}
	
	$response
}


1;