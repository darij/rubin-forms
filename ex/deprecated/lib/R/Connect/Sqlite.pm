package R::Connect::Sqlite;
# драйвер для СУБД скулат

use base R::Connect;

use common::sense;
use R::App;


# зарезервированные слова sql
my $SQL_WORD = $app->perl->setref(qw/ABORT ACTION ADD AFTER ALL ALTER ANALYZE AND AS ASC ATTACH AUTOINCREMENT BEFORE BEGIN BETWEEN BY CASCADE CASE CAST CHECK COLLATE COLUMN COMMIT CONFLICT CONSTRAINT CREATE CROSS CURRENT_DATE CURRENT_TIME CURRENT_TIMESTAMP DATABASE DEFAULT DEFERRABLE DEFERRED DELETE DESC DETACH DISTINCT DROP EACH ELSE END ESCAPE EXCEPT EXCLUSIVE EXISTS EXPLAIN FAIL FOR FOREIGN FROM FULL GLOB GROUP HAVING IF IGNORE IMMEDIATE IN INDEX INDEXED INITIALLY INNER INSERT INSTEAD INTERSECT INTO IS ISNULL JOIN KEY LEFT LIKE LIMIT MATCH NATURAL NO NOT NOTNULL NULL OF OFFSET ON OR ORDER OUTER PLAN PRAGMA PRIMARY QUERY RAISE RECURSIVE REFERENCES REGEXP REINDEX RELEASE RENAME REPLACE RESTRICT RIGHT ROLLBACK ROW SAVEPOINT SELECT SET TABLE TEMP TEMPORARY THEN TO TRANSACTION TRIGGER UNION UNIQUE UPDATE USING VACUUM VALUES VIEW VIRTUAL WHEN WHERE WITH WITHOUT/);

sub SQL_WORD {
    my ($self, $word) = @_;
    exists $SQL_WORD->{uc $word}
}

sub AUTO_INCREMENT { "AUTOINCREMENT" }
sub COMMENT { my ($self, $comment, $type) = @_; "/* $comment */" }
sub ADD_AS_INSERT {1}

# типы столбцов для alter table
my $COLUMN_TYPE = {
	VARCHAR  => "STRING",
	CHAR  => "STRING",
	TEXT  => "STRING",
	TINYTEXT  => "STRING",
	MEDIUMTEXT  => "STRING",
	LONGTEXT  => "STRING",
	FLOAT => "REAL",
	DOUBLE => "REAL",
	BINARY => "BLOB",
	VARBINARY => "BLOB",
	LONGBLOB => "BLOB",
	MEDIUMBLOB => "BLOB",
	TINYBLOB => "BLOB",
};

sub COLUMN_TYPE {
    my ($self, $type) = @_;
	($type) = $type =~ /^(\w+)/;
	$type = uc $type;
	$COLUMN_TYPE->{$type} // $type
}

# формирует DNS
sub DNS {
	my ($self) = @_;
	#dbi:SQLite:uri=file:$path_to_dbfile?mode=rwc
	"dbi:SQLite:dbname=" . $self->basepath
}

# дополнительные опции для DBD
sub default_options {}

# кавычки для столбца
sub word_quote {
	my ($self, $col) = @_;
	"\"$col\""
}

# кавычки для таблицы
sub tab_word_quote {
	my ($self, $tab) = @_;
	"\"$tab\""
}

sub after_connect {
	my ($self) = @_;
	
	$self->do('PRAGMA encoding="UTF-8"');
	$self->do('PRAGMA foreign_keys=ON');
	
	$self
}


#pragma table_info(\"B\")
# [
          # {
            # 'notnull' => 0,
            # 'type' => 'integer',
            # 'name' => 'i',
            # 'pk' => 1,
            # 'dflt_value' => undef,
            # 'cid' => 0
          # },
          # {
            # 'pk' => 0,
            # 'cid' => 1,
            # 'dflt_value' => '"xyz"',
            # 'name' => 'b',
            # 'type' => 'varchar(600)',
            # 'notnull' => 0
          # }
        # ]

        
# [
          # {
            # 'tbl_name' => 'A',
            # 'type' => 'table',
            # 'sql' => 'CREATE TABLE A(i int, b int not null)',
            # 'rootpage' => 2,
            # 'name' => 'A'
          # },
          # {
            # 'tbl_name' => 'B',
            # 'type' => 'table',
            # 'sql' => 'CREATE TABLE B(i integer primary key autoincrement, b varc
# har(600) default "xyz")',
            # 'rootpage' => 3,
            # 'name' => 'B'
          # },
          # {
            # 'name' => 'sqlite_sequence',
            # 'rootpage' => 4,
            # 'sql' => 'CREATE TABLE sqlite_sequence(name,seq)',
            # 'tbl_name' => 'sqlite_sequence',
            # 'type' => 'table'
          # }
        # ]

		
# информация о базах
sub get_base_info {
	my ($self) = @_;
	my $rows = $self->query_all("PRAGMA database_list");
	my $info = {};
	for my $row (@$rows) {
		$row->{name} = $self->basename if $row->{name} eq "main";
		$info->{$row->{name}} = $row;
	}
	
	$app->file($self->basepath)->find("**.sqlite3")->add(":memory:")->then(sub {
		$info->{$_->name}{attached} = !!scalar keys %{$info->{$_->name}};
		$info->{$_->name}{name} = $_->name;
		$info->{$_->name}{file} = $_->path;
	});
	$info
}

our %HIDE_TABLE = $app->perl->set(qw/sqlite_sequence sqlite_stat1 sqlite_stat2 sqlite_stat3 sqlite_stat4/);

# возвращает информацию о таблицах
sub get_tab_info {
	my ($self) = @_;
	
    #name, engine, charset, remark, options, type
	my $sql = "select *	from sqlite_master where type='table'";
	my $rows = $self->nolog(sub { $self->query_all($sql); });

	my $info = {};
	for my $row (@$rows) {	# создаём info
		next if exists $HIDE_TABLE{$row->{name}};
		$info->{$row->{name}} = $row;
	}
	return $info;
}

# возвращает информацию о столбцах таблиц
sub get_info {
	my ($self) = @_;
	
	
	my $tab_info = $self->tab_info;
	my $info = {};
	
	my $sql = "SELECT m.tbl_name, c.*
FROM sqlite_master As m, pragma_table_info(m.name) As c
WHERE m.type='table'";
	my $rows = $self->nolog(sub { $self->query_all($sql); });

	for my $row (@$rows) {
		my $tab = $row->{tbl_name};
		next if exists $HIDE_TABLE{$tab};
		
		$row->{name} = $row->{name};
		$row->{type} = $self->COLUMN_TYPE($row->{type});
		$row->{null} = $row->{notnull} == 0;
		$row->{default} = $row->{dflt_value};
		$row->{pk} = $row->{pk};
		$row->{autoincrement} = $tab_info->{$tab}{sql} =~ /\b$row->{name}\b[^,]+\bAUTOINCREMENT\b/;
		#? $row->{comment} = $row->{column_comment};
		#?$row->{charset} = $row->{character_set_name};
		#?$row->{collation} = $row->{collation_name};
		
		$info->{$tab}{$row->{name}} = $row;
	}

	
	return $info;
}

# индексы
sub get_index_info {
	my ($self) = @_;
	
	# Table => "tab",
	# Non_unique => "non_uniq",
	# Key_name => "name",
	# Seq_in_index => "pos",
	# Column_name => "col",
	# Comment => "comment",
	# Index_comment => "index_comment",
	# Null => "null",
	# Index_type => "type",
	# Packed => "packed",
	# Cardinality => "cardinality",
	# Sub_part => "part",
	# Collation => "charset",
	
	my $sql = "select m.tbl_name As tab, il.*, ii.name As col, ii.cid, ii.seqno As pos
FROM sqlite_master As m,
pragma_index_list(m.name) AS il,
pragma_index_info(il.name) AS ii
where type='table'
order by tab, name, pos";
	my $rows = $self->nolog(sub { $self->query_all($sql); });
	my $info = {};
	for my $row (@$rows) {
		next if exists $HIDE_TABLE{$row->{tab}};
		push @{$info->{$row->{tab}}{$row->{name}}}, $row;
	}
	$info
}


# ссылки
sub get_fk_info {
	my ($self) = @_;
	
	my $sql = "select m.tbl_name, * from sqlite_master As m,
pragma_foreign_key_list(m.name) AS fk
where type='table'";
	my $rows = $self->nolog(sub { $self->query_all($sql); });
	my $info = {};
	for my $row (@$rows) {
		next if exists $HIDE_TABLE{$row->{name}};
		$info->{$row->{name}} = $row;
	}
	$info	
}


#@@category show

# путь к файлу
sub basepath {
	my ($self, $basename) = @_;
	$basename //= $self->basename;
	$basename eq ":memory:"? $basename: $app->project->file("var/sqlite/$basename.sqlite3")->mkpath->path;
}

# атачит базу
sub show_attach {
	my ($self, $basename) = @_;
	my $path = $self->basepath($basename);
	join " ", "ATTACH DATABASE", $self->word($path), "AS", $self->word($basename)
}

# атачит базу
sub show_detach {
	my ($self, $basename) = @_;
	join " ", "DETACH DATABASE", $self->word($basename)
}

# аттачит
sub attach {
	my ($self, $basename) = @_;
	$self->do($self->show_attach($basename));
}

# детачит
sub detach {
	my ($self, $basename) = @_;
	$self->do($self->show_detach($basename));
}


# sql use
sub show_use {
	my ($self, $basename) = @_;
	$self->show_attach($basename);
}

# sql для создания базы
sub show_create_database {
	my ($self, $base) = @_;
	$self->show_attach($base->name);
}

# создать базу
sub create_database {
	my ($self, $base) = @_;
	my $path = $self->basepath($base->name);
	die "база `".$base->name."` существует" if -e $path;
	$self->connect->do($self->show_create_database($base));
}

# sql для создания базы
sub show_drop_database {
	my ($self, $base) = @_;
	$base->name eq $self->basename? $self->base("_unused")->show_create: (),
	join " ", "DETACH DATABASE", $self->word($base->name)
}

# удалить базу
sub drop_database {
	my ($self, $base) = @_;
	$self->connect->do($self->show_drop_database($base));
	$app->file($self->basepath($base->name))->rm;
}


# изменяет индекс
# 1. Переименовываем таблицу
# 2. Создаём такую же, но без столбца
# 3. Добавляем столбец
# 4. Переписываем в неё все данные
sub show_modify_column {
	my ($self, $col) = @_;
	my $tab = $col->table->load;
	@{$tab->columns} = map { $_->name eq $col->name? $col: $_ } @{$tab->columns};
	
	return
		"BEGIN TRANSACTION",
		$tab->show_rename("_tmp_"),
		$tab->show_create,
		join("", "INSERT INTO ", $self->word($tab->name), " AS SELECT * FROM _tmp_"),
		$self->tab("_tmp_")->show_drop,
		"COMMIT";
}

# изменяет индекс
# 1. Переименовываем таблицу
# 2. Создаём такую же, но без столбца
# 3. Добавляем столбец
# 4. Переписываем в неё все данные
sub show_drop_column {
	my ($self, $col) = @_;
	my $tab = $col->table->load;
	@{$tab->columns} = grep { $_->name ne $col->name } @{$tab->columns};
	
	my $columns = join ", ", map { $self->word($_->name) } @{$tab->columns};
	
	return
		"BEGIN TRANSACTION",
		$tab->show_rename("_tmp_"),
		$tab->show_create,
		join("", "INSERT INTO ", $self->word($tab->name), " AS SELECT $columns FROM _tmp_"),
		"COMMIT";
}


#@@category idx

# формирует индекс из info
sub show_create_index {
	my ($self, $idx) = @_;
	join "", "CREATE INDEX ", $self->word($idx->name), " ON ", $self->word($idx->tab), " ", $self->show_definition_index($idx)
}

# изменяет индекс
sub show_modify_index {
	my ($self, $idx) = @_;
	return
		join("", "DROP INDEX ", $self->word($idx->tab), ".", $self->word($idx->name)),
		join "CREATE INDEX ", $self->word($idx->name), " ON ", $self->word($idx->tab), " ", $self->show_definition_index($idx);
}

# переименовывает индекс
sub show_rename_index {
	my ($self, $idx, $to) = @_;
	$to = $idx->clone->name($to) if !ref $to;
	return
		join("", "DROP INDEX ", $self->word($idx->tab), ".", $self->word($idx->name)),
		join "CREATE INDEX ", $self->word($to->name), " ON ", $self->word($to->tab), " ", $self->show_definition_index($to);
}

# удалить индекс
sub show_drop_index {
	my ($self, $idx) = @_;
	join "", "DROP INDEX ", $self->word($idx->tab), ".", $self->word($idx->name);
}

# переопределяем - вызывается только из show_create_table
sub show_index {
	my ($self, $idx) = @_;
	$self->set_other($self->show_create_index($idx)), return if $idx->type eq "INDEX";
	
	$self->SUPER::show_index($idx);
}

# # переопределяем - вызывается только из show_create_table
# sub show_foreign {
	# my ($self, $fk) = @_;
	# $self->set_other($self->show_create_foreign($fk));
	# return;
# }

# # переопределяем
# sub show_create_foreign {
	# my ($self, $fk) = @_;
	# join "", "ALTER TABLE ", $self->word($fk->tab), " ADD ", $self->show_foreign($fk)
# }



1;