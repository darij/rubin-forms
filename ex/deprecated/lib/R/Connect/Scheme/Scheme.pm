package R::Connect::Scheme::Scheme;
# базовый класс для баз, таблиц и сущностей (индексов, связей и столбцов)

use common::sense;
use R::App;

has qw/name void/;

# конструктор
sub new {
	my $cls = shift;
	bless {@_}, ref $cls || $cls;
}

# клонирует себя
sub clone {
	my $self = shift;
	bless { %$self, @_ }, ref $self;
}

# возвращает пустой
sub empty {
	my ($self) = @_;
	
	return $self->connect->base(undef) if ref $self eq "R::Connect::Scheme::Base";
	return $self->base->tab(undef) if ref $self eq "R::Connect::Scheme::Table";
	return $self->table->col(undef) if ref $self eq "R::Connect::Scheme::Col";
	return $self->table->idx(undef) if ref $self eq "R::Connect::Scheme::Idx";
	return $self->table->fk(undef) if ref $self eq "R::Connect::Scheme::Fk";
	
	die "???";
}

# существует
sub exists {
	my ($self) = @_;
	!!$self->info
}

# загружает из базы
sub load {
	my ($self) = @_;
	my $info = $self->info;
	$self->void(!$info);
	$info // return $self;
	for my $key ( $self->load_keys ) {
		$self->$key( $info->{$key} );
	}
	$self
}

# добавляет неустановленные свойства из базы
sub upd {
	my ($self) = @_;
	my $info = $self->info;
	for my $key ( keys %$info ) {
		$self->$key( $info->{$key} ) if !defined $self->{$key};
	}
	$self
}

#@@category DDL

# создаёт
sub create {
	my $self = shift;
	$self->connect->do($self->show_create);
	$self
}

# удаляет
sub drop {
	my ($self) = @_;
	$self->connect->do($self->show_drop);
}

# модифицирует
sub modify {
	my ($self) = @_;
	$self->connect->do($self->show_modify);
}

# переименовывает
sub rename {
	my ($self, $to) = @_;
	$self->connect->do($self->show_rename($to));
}

#@@category сравнение

# сортирует фореигны и заодно джойнит
sub diff_sort {
	my $self = shift;
	
	my (@up, @mid, @down);
	
	for( @_ ) {
		push @up, $_ when /->fk\b.*->drop;/s;
		push @down, $_ when /->fk\b.*->create;/s;
		default { push @mid, $_	}
	}
	
	@up = uniq @up;
	@down = uniq @down;
	
	@up = sort @up;
	@mid = sort @mid;
	@down = sort @down;
	
	wantarray? (@up, @mid, @down): join("", (@up, @mid, @down))
}

# сравнивает и возвращает в виде протокола
sub diff {
	my ($self, $scheme) = @_;
	
	my $self_void = $self->void || !defined $self->name;
	my $scheme_void = !$scheme || $scheme->void || !defined $scheme->name;
	
	# ничего делать не нужно - сущности не существуют
	return if $self_void and $scheme_void;
	
	# удалить - приводимой сущности не существует
	return $self->diff_sort($self->diff_drop($scheme)) if $scheme_void;
	
	my @diff = $self->diff_prop($scheme);
	
	# создать, раз self-а нет
	return $self->diff_sort($scheme->diff_create(\@diff)) if $self_void;
	
	return $self->diff_sort($self->diff_components($scheme)) if !@diff;
	
	return $self->diff_sort(
		$self->diff_modify($scheme, \@diff),
		$self->diff_components($scheme)
	);
}

# diff для модификации
sub diff_modify {
	my ($self, $scheme, $diff) = @_;
	$diff //= [$self->diff_prop($scheme)];
	
	join("", '$c->', $scheme->diff_entity, @$diff, "->modify;\n"), 
}

# diff для удаления
sub diff_drop {
	my ($self) = @_;
	join "", '$c->', $self->diff_entity, "->drop;\n"
}

# сравнение свойств
my %RENAME = qw/comment remark/;
sub diff_prop {
	my ($self, $scheme) = @_;
	
	#msg ":inline size10000", $self->diff_entity, $scheme->name, ":sep( -> )", +{ map { ($_ => [$self->{$_}, $scheme->{$_}, ($self->{$_}//"") ~~ ($scheme->{$_}//"")? "True": "False"] ) } $self->diff_keys };
	
	my @prop = map {
	
		my $from = $self->{$_} // "";
		my $to = $scheme->{$_} // "";
	
		if($_ eq "charset") {
			$from ||= $self->super_charset;
			$to ||= $scheme->super_charset;
		}
		
		if($_ eq "collation") {
			$from ||= $self->super_collation;
			$to ||= $scheme->super_collation;
		}
	
		if(/^(pk|autoincrement)$/n) {
			$from ||= 0;
			$to ||= 0;
		}
	
		my $x = ref($from) || ref($to)? $from ~~ $to: $from eq $to;
	
		$x? ():
			join "", "->", ($RENAME{$_} // $_), "(", $app->perl->inline_dump($to), ")"
	} $self->diff_keys;
	
	my $i = 0;
	my %map = map {$_=>$i++} @prop;
	
	if(exists $map{"->null(0)"} and exists $map{"->pk(1)"}) {
		$prop[ delete $map{"->null(0)"} ] = undef;
		$prop[ $map{"->pk(1)"} ] = "->primary";
	}
	
	if(exists $map{"->autoincrement(1)"}) {
		$prop[ $map{"->autoincrement(1)"} ] = "->increments";
	}
	
	if(exists $map{"->null(0)"}) {
		$prop[ $map{"->null(0)"} ] = "->need";
	}
	
	grep { defined $_ } @prop
}

# сравнение вложенных полей - переопределяется у таблицы
sub diff_components {}

# переопределяется у таблицы
sub diff_create {
	my ($self, $diff) = @_;
	$diff or die "use: ->diff_create(\\\@diff)";
	join '', '$c->', $self->diff_entity, @$diff, "->create;\n"
}

# обратное стравнение
sub rediff {
	my ($self, $scheme) = @_;
	($scheme // $self->empty)->diff($self)
}

# обратный diff-sql
sub rediff_sql {
	my ($self, $scheme) = @_;
	($scheme // $self->empty)->diff_sql($self)
}

# обратный diff color sql
sub rediff_csql {
	my ($self, $scheme) = @_;
	($scheme // $self->empty)->diff_csql($self)
}


# возвращает sql, вместо перла
sub diff_sql {
	my ($self, $scheme) = @_;
	my @sql;
	my $c = $app->connect;
	
	my $code = join "", map {
		s/^(my (\$\w+).*)->create;\n\n\z/$1;\npush \@sql, $2->show_create;\n\n/s or
		s/^(.*)->(\w+);(\n+)\z/push \@sql, $1->show_$2;$3/s;
		$_ 
	} $self->diff($scheme);
	#msg $code;
	my $sql = eval $code;
	die if $@;
	wantarray? @sql: join("\n", @sql)
}

# раскрашенный sql-код
sub diff_csql {
	my ($self, $scheme) = @_;
	join "", $self->connect->color(scalar $self->diff_sql($scheme), "do");
}

# преобразует из
sub transmute_from {
	my ($self, $scheme) = @_;
	($scheme // $self->empty)->transmute_to($self)
}

# преобразует в
sub transmute_to {
	my ($self, $scheme) = @_;
	my $code = $self->diff($scheme);
	return $self if !length $code;
	my $c = $self->connect;
	#msg1 $app->color->perl($code);
	eval $code;
	msg1($code), die if $@;
	$self
}

1;