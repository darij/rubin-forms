package R::Connect::Scheme::Accumulation;
# коллекция

use base R::Collection;

use common::sense;
use R::App;

has qw/connect/;

# язык запросов:
# @base
# tab
# .col
# 
# :unq :unique :idx :index :pk :spatial :fulltext - тип индекса
# fk
# tab#10 - значение айдишника -> tab.id=10
# idx(tab.col) - индекс
# tab.col = 10 - константы: 10, -10, 10.1
# tab.col < .col2 - операторы сравнения: <, >, <=, >=, =, <>, ~ (like), =~ (regexp)
# логические операторы: !, |, &
# скобки: ( )
# , - несколько выражений
# 
# $app->connect->acc("tab.col < ?", 10)->load("col1 as x, col2")
# $app->connect->acc("tab.col < {num}", num=>10)->load("col1 as x, col2")
# $app->connect->acc("tab.col < {num}", num=>10)->rm
# $app->connect->acc("tab.col:idx_name")->save
# $app->connect->acc("tab.col^pk")->save
# $app->connect->acc("tab.col^unq_name")->save
# $app->connect->acc("^idx_name")->rm
# $app->connect->acc("tab.col -> tab2.col2^^fk_my")->save
# $app->connect->acc("^^fk_my")->rm


# конструктор
sub new {
	my $cls = shift;
	my $connect = shift;
	bless {
		connect => $connect,
		parts => [map {
			Isa($_, "R::Connect::Scheme::Scheme")? $_:
			/^:(.*)/? $connect->base($1):
			/:/? $connect->tab($`)->fk($'):
			/:/? $connect->tab($`)->idx($'):
			/\./? $connect->tab($`)->col($'):
			$connect->tab($_)
		} @_],
	}, ref $cls || $cls;
}


# выборка из базы - возвращает коллекцию bean-ов
#*unsync = \&load;
sub load {
	my $self = shift;
	$_->__load(@_) for $self->parts;
	$self
}

# сохраняет сущности (бины, таблицы, столбцы и индексы)
*sync = \&save;
*store = \&save;
sub save {
	my $self = shift;
	$_->__save(@_) for $self->parts;
	$self
}

# удаляет записи, базы, таблицы, столбцы, индексы
*rm = \&erase;
*drop = \&erase;
sub erase {
	my $self = shift;
	$_->__erase(@_) for $self->parts;
	$self
}

# апдейтит указанные столбцы бинов. 
sub update {
	my ($self) = @_;
	my $self = shift;
	$_->__update(@_) for $self->parts;
	$self
}

# возвращает/устанавливает значение: 
#   bean - prop=>val,...
#	col - "int not null default 10"
#	tab - набор колумнов в виде acc
sub val {
	my $self = shift;
	
	$self
}

1;