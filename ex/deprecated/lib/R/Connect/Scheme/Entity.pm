package R::Connect::Scheme::Entity;
# базовый класс для столбцов индексов и связей (fk)
# определяет интерфейс

use base R::Connect::Scheme::Scheme;

use common::sense;
use R::App;

has qw/table comment/;

*remark = \&comment;

# возвращает коннект
sub connect {
	my ($self) = @_;
	$self->table->connect
}

# возвращает имя таблицы
sub tab {
	my ($self) = @_;
	$self->table->name
}

# возвращает чарсет из базы
sub super_charset {
	my ($self) = @_;
	$self->{charset} // $self->table->super_charset
}

# возвращает чарсет из базы
sub super_collation {
	my ($self) = @_;
	$self->{collation} // $self->table->super_collation
}

# для diff - название сущности
sub diff_entity {
	my ($self) = @_; 
	join "", "tab('", $self->tab, "')", $self->diff_entity_me
}


1;