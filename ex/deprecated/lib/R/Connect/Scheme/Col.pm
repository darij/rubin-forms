package R::Connect::Scheme::Col;
# столбец

use base R::Connect::Scheme::Entity;

use common::sense;
use R::App;

# свойства
has qw/null default pk autoincrement charset collation pos extra/;

# какие ключи подгружать из базы
sub load_keys {
	qw/type null default pk autoincrement charset collation comment pos extra/
}

#@@category сравнение

# свойства для сравнения
sub diff_keys {
	qw/type null default pk autoincrement charset collation comment/
}

# для diff - название сущности
sub diff_entity_me {
	my ($self) = @_;
	"->col('", $self->name, "')"
}

# diff для модификации
sub diff_modify {
	my ($self, $scheme, $diff) = @_;
	
	my @fk = $self->related_foreigns;
	my @maybe_fk = $scheme->related_foreigns;
	
	return
		$self->SUPER::diff_modify($scheme, $diff),
		(map {$_->diff_drop} @fk),
		(map {$_->diff_create} @maybe_fk)
}

# diff для удаления
sub diff_drop {
	my ($self) = @_;
	
	return
		$self->SUPER::diff_drop,
		map {$_->diff_drop} $self->related_foreigns
		
}

# возвращает все связанные ключи
sub related_foreigns {
	my ($self) = @_;
	return $self->foreigns_on, $self->foreigns_from;
}

# ключи указывающие на столбец
sub foreigns_on {
	my ($self) = @_;
	my $name = $self->name;
	grep { $name ~~ $_->cols } @{$self->table->foreigns}
}

# ключи с этого столбца
sub foreigns_from {
	my ($self) = @_;
	my $name = $self->name;
	my $tab = $self->tab;
	
	grep { $tab eq $_->ref_tab && $name ~~ $_->refs } $self->table->base->foreigns
}

#@@category описание

# информация или пусто
my @INT_KEYS = qw/null pk autoincrement/;
sub info {
	my ($self) = @_;
	my $info = $self->connect->info->{$self->tab} // return undef;
	my $info = $info->{$self->name} // return undef;
	$info = { %$info };
	@$info{@INT_KEYS} = map { int $_ } @$info{@INT_KEYS};
	$info
}

# тип столбца
sub type {
	my ($self, $type) = @_;
	return $self->connect->COLUMN_TYPE($self->{type}) if @_ == 1;
	$self->{type} = $type;
	return $self;
}

# добавлет unsigned, если его нет
sub unsigned {
	my ($self) = @_;
	$self->type($self->type . " unsigned") if $self->type !~ /\bunsigned\b/i;
	$self
}


# снимает null
sub need {
	my ($self) = @_;
	$self->null(0)
}

# устанавливает PRIMARY KEY
sub primary {
	my ($self) = @_;
	$self->pk(1)->null(0)
}

# устанавливает автоинкремент
sub increments {
	my ($self) = @_;
	$self->autoincrement(1)
}

#@@category sql для DDL

# создаёт
sub show_create {
	my ($self, $after) = @_;
	$self->connect->show_create_column($self, $after);
}

# удаляет
sub show_drop {
	my ($self) = @_;
	$self->connect->show_drop_column($self);
}

# модифицирует
sub show_modify {
	my ($self, $after) = @_;
	$self->connect->show_modify_column($self, $after);
}

# переименовывает
sub show_rename {
	my ($self, $to) = @_;
	$self->connect->show_rename_column($self, $to);
}


1;