package R::Connect::Scheme::Fk;
# ссылка

use base R::Connect::Scheme::Entity;

use common::sense;
use R::App;

has qw/ref_tab on_update on_delete cols refs/;

# какие ключи подгружать из базы
sub load_keys {
	qw/cols ref_tab refs on_update on_delete comment/
}

# свойства для сравнения
sub diff_keys {
	qw/cols ref_tab refs on_update on_delete comment/
}

# для diff - название сущности
sub diff_entity_me {
	my ($self) = @_;
	"->fk('", $self->name, "')"
}

# создаёт diff
sub diff_create {
	my ($self, $diff) = @_;
	$diff //= [$self->table->fk(undef)->diff_prop($self)];
	$self->SUPER::diff_create($diff)
}

# diff для модификации
sub diff_modify {
	my ($self, $scheme, $diff) = @_;
	$diff = [$self->table->fk(undef)->diff_prop($scheme)];
	$self->SUPER::diff_modify($scheme, $diff);
}

# информация или пусто
sub info {
	my ($self) = @_;
	my $ti = $self->connect->fk_info->{$self->tab} // return undef;
	+{%{$ti->{$self->name}}}
}

# информация, если ключ обратный - на таблицу
sub bk {
	my ($self) = @_;
	my $info = $self->connect->fk_info_backward->{$self->tab};
	$info && $info->{$self->name}
}

# быстро устанавливает ссылку
#@deprecate - нужно переименовать на rel, либо изначально проставлять id, либо вообще никак
sub ref {
	my ($self, $ref_tab, @refs) = @_;
	
	$self->cols(["id"]) if !$self->cols;
	
	@refs = "id" if !@refs;
	
	$self->ref_tab($ref_tab)->refs([@refs]);
}

#@@category sql для DDL

# создаёт
sub show_create {
	my ($self) = @_;
	$self->connect->show_create_foreign($self);
}

# удаляет
sub show_drop {
	my ($self) = @_;
	$self->connect->show_drop_foreign($self);
}

# модифицирует
sub show_modify {
	my ($self) = @_;
	$self->connect->show_modify_foreign($self);
}

# переименовывает
sub show_rename {
	my ($self, $to) = @_;
	$self->connect->show_rename_foreign($self, $to);
}



1;