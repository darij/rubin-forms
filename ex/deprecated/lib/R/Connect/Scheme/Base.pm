package R::Connect::Scheme::Base;
# База

use base R::Connect::Scheme::Scheme;

use common::sense;
use R::App;

# свойства
has qw/charset collation tables connect/;

# какие ключи подгружать из базы
sub load_keys {
	qw/charset collation/
}

# свойства для сравнения
sub diff_keys {
	qw/charset collation/
}

# вызывается при создании слота $app
sub DEFAULT_new {
	shift;
}

# конструктор
sub new {
	my $cls = shift;
	my $self = $cls->SUPER::new(
		#charset => "utf8",
		#collation => "utf8_unicode_ci",
		tables => {},
		@_
	);
	die "required connect" if !Isa $self->{connect}, "R::Connect";
	die "required name" if !exists $self->{name};
	$self
}

# информация или пусто
sub info {
	my ($self) = @_;
	my $info = $self->connect->base_info->{$self->name} // return undef;
	+{%$info}
}

# загружает из базы
sub load {
	my ($self) = @_;
	$self->SUPER::load;
	
	return $self if !$self->exists;
	
	my $c = $self->connect;
	my $main = $c->basename;
	my $guard;
	# тут требуется переключиться на эту базу, если база не текущая
	if($self->name ne $main) {
		$guard = guard { $c->use($main) };
		$c->use($self->name);
	}
	
	$self->{tables} = +{ map { $_ => $self->tab($_)->load } keys %{$self->connect->info} };
	
	$self
}

# добавляет таблицы в tables
sub add {
	my $self = shift;
	
	for my $tab ( @_ ) {
		die "не R::Connect::Scheme::Table!" if !Isa $tab, "R::Connect::Scheme::Table";
		my $name = $tab->name;
		die "таблица $self->{name}.$name уже добавлена" if exists $self->{tables}{$name};
		$self->{tables}{$name} = $tab;
	}
	
	$self
}

# глубокое копирование
sub deep_clone {
	my ($self) = @_;
	my $clone = $self->clone;
	my $tables = $clone->{tables};
	$clone->{tables} = { map { $_ => $tables->{$_}->deep_clone } keys %$tables };
	$clone
}

# возвращает таблицу
sub tab {
	my ($self, $tab) = @_;
	$app->connectSchemeTable(name => $tab, base => $self, engine => "InnoDB")
}

# добавленная таблица
sub added_tab {
	my ($self, $name) = @_;
	$self->{tables}{$name}
}

# возвращает таблицы
sub tabs {
	my ($self) = @_;
	my $tables = $self->tables;
	wantarray? (map { $tables->{$_} } sort keys %$tables): $tables;
}

# возвращает ссылки из всех таблиц
sub foreigns {
	my ($self) = @_;
	my @fk = map { @{$_->foreigns} } $self->tabs;
	wantarray? @fk:
		+{ map { ($_->{name} => $_) } @fk }
}


#@@category сравнение

# для diff - название сущности
sub diff_entity { my ($self) = @_; "base('$self->{name}')" }

# сравнение вложенных полей
sub diff_components {
	my ($self, $scheme) = @_;
	
	my $empty_tab = $self->tab(undef);
	
	map { ($_->[0] // $empty_tab)->diff($_->[1]) } 
		ascending { defined($_[0]) && $_[0]->name }
		cross_join { $_->name } 
			[values %{$self->tables}], 
			[values %{$scheme->tables}]
}

# создать
sub diff_create {
	my ($self, $diff) = @_;
	
	my $tables = $self->tables;
	my @tables = map { $tables->{$_} } sort keys %$tables;
	
	return 
		join('', '$c->', $self->diff_entity,  @$diff, "->create;\n\n"),
		map { $_->diff_create } @tables
}



#@@category DDL

# показывает sql
sub show_create {
	my ($self) = @_;
	$self->connect->show_create_database($self)
}

# показывает sql
sub show_drop {
	my ($self) = @_;
	$self->connect->show_drop_database($self)
}

# показывает sql
sub show_modify {
	my ($self) = @_;
	$self->connect->show_modify_database($self)
}

# показывает sql
sub show_rename {
	my ($self, $to) = @_;
	$self->connect->show_rename_database($self, $to)
}


#@@category манипуляции с базой напрямую

# использовать эту БД
sub use {
	my ($self) = @_;
	$self->connect->use($self->name);
	$self
}

# переключиться и вернуть гварда
sub with {
	my ($self) = @_;
	my $old = $self->connect->basename;
	return undef if $self->name eq $old;
	$self->use;
	guard { $self->connect->use($old) }
}

# очистить базу
sub clear {
	my ($self) = @_;
	
	my $guard = $self->with;
	my $x = $self->connect->base($self->name)->load;
	
	$_->drop for $x->foreigns;
	$_->drop for $x->tabs;
	
	$self
}

# очищает таблицы
*trunc = \&truncate;
sub truncate {
	my ($self) = @_;
	
	my $defer_use = $self->with;
	my $x = $self->connect->base($self->name)->load;
	
	my $defer_checks = $self->connect->unfkchecks;
	
	$_->truncate for $x->tabs;
	
	$self
}

1;