package R::Connect::Scheme::Bean;
# бин - позволяет устанавливать или считывать свои свойства, представляет запись в таблице базы

use common::sense;
use R::App;

# конструктор
sub new {
	my $cls = shift;
	bless {@_}, ref $cls || $cls;
}

use vars '$AUTOLOAD';

sub AUTOLOAD {
	my ($self, $val) = @_;
    my ($prop) = $AUTOLOAD =~ /([^:]+)$/;
	
	if(@_>1) {
		$self->{row}{$prop} = $val;
	} else {
		$self->{row}{$prop}
	}
}

# перекрываем
sub DESTROY {}

# загружает из базы
sub _load {
	my ($self) = @_;
	$self
}

# сохраняет в базу
sub _store {
	my ($self) = @_;
	$self
}


1;