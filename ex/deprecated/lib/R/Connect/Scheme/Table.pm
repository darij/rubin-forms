package R::Connect::Scheme::Table;
# представляет таблицу

use base R::Connect::Scheme::Scheme;

use common::sense;
use R::App;

# столбцы таблицы, индексы и связи
use R::Connect::Scheme::Col;
use R::Connect::Scheme::Idx;
use R::Connect::Scheme::Fk;

has qw/base engine autoincrement comment charset collation options/;

# какие ключи подгружать из базы
sub load_keys {
	qw/engine charset collation options comment autoincrement/
}

# свойства для сравнения
sub diff_keys {
	qw/engine charset collation options comment/
}

# алиас
*remark = \&comment;

# возвращает connect
sub connect {
	my ($self) = @_;
	$self->base->connect
}

# возвращает чарсет из базы
sub super_charset {
	my ($self) = @_;
	$self->{charset} // $self->base->charset
}

# возвращает чарсет из базы
sub super_collation {
	my ($self) = @_;
	$self->{collation} // $self->base->collation
}


# колумны таблицы
*column = \&cols;
*columns = \&cols;
sub cols {
	my $self = shift;
	return $self->{columns} if @_ == 0;
	@{$self->{columns}} = map { ref $_? $_: $self->col($_) } @_;	
	$self
}

# индексы
sub indexes {
	my $self = shift;
	return $self->{indexes} if @_ == 0;
	@{$self->{indexes}} = map { ref $_? $_: $self->idx($_) } @_;	
	$self
}

# связи
*references = \&foreigns;
sub foreigns {
	my $self = shift;
	return $self->{foreigns} if @_ == 0;
	@{$self->{foreigns}} = map { ref $_? $_: $self->fk(split /[:\.]/, $_) } @_;	
	$self
}


# возвращает колумн
sub col {
	my ($self, $col) = @_;
	R::Connect::Scheme::Col->new(name => $col, table => $self, null => 1);
}

# возвращает индекс
*index = \&idx;
sub idx {
	my ($self, $idx, @cols) = @_;
	R::Connect::Scheme::Idx->new(name => $idx, table => $self, type => "INDEX",
		(@cols? (cols => [@cols]): ()),
	);
}

# возвращает ссылку
*reference = \&fk;
*foreign = \&fk;
sub fk {
	my ($self, $idx, @cols) = @_;
	R::Connect::Scheme::Fk->new(name => $idx, table => $self, (@cols? (cols => [@cols]): ()),
		on_update => scalar $self->connect->ON_UPDATE,
		on_delete => scalar $self->connect->ON_DELETE,
	);
}

# уникальный ключ
sub unique {
	my $self = shift;
	$self->idx(@_)->type("UNIQUE")
}

# информация или пусто
sub info {
	my ($self) = @_;
	$self->connect->tab_info->{$self->name} // undef
}

# загружает
sub load {
	my ($self) = @_;
	$self->SUPER::load;
	
	my $tab = $self->{name};
	
	# добавляем колумны
	my $info = $self->connect->info->{$tab};
	
	for my $col (asc { $info->{$_}{pos} } keys %$info) {
		$self->add($self->col($col)->load);
	}
	
	# добавляем ключи
	my $info = $self->connect->index_info->{$tab};
	for my $idx (sort keys %$info) {
		next if $idx eq "PRIMARY";
		$self->add($self->idx($idx)->load);
	}
	
	# добавляем фореигны
	my $info = $self->connect->fk_info->{$tab};
	for my $fk (sort keys %$info) {
		$self->add($self->fk($fk)->load);
	}
	
	$self
}

# добавляет свойства
sub add {
	my $self = shift;
	
	for my $prop (@_) {
		if(Isa $prop, "R::Connect::Scheme::Col") {
			die "$self->{name}.$prop->{name} уже добавлен" if $self->added_col($prop->name);
			push @{$self->{columns}}, $prop;
		}
		elsif(Isa $prop, "R::Connect::Scheme::Idx") {
			die "$self->{name}~>$prop->{name} уже добавлен" if $self->added_idx($prop->name);
			push @{$self->{indexes}}, $prop;
		}
		elsif(Isa $prop, "R::Connect::Scheme::Fk") {
			die "$self->{name}->$prop->{name} уже добавлен" if $self->added_fk($prop->name);
			push @{$self->{foreigns}}, $prop;
		}
		else { die "неопознанное свойство $prop" }
	}
	
	$self
}

# глубокое копирование
sub deep_clone {
	my ($self) = @_;
	my $clone = $self->clone;
	$self->{columns} = [ map { $_->clone } @{$self->{columns}} ];
	$self->{indexes} = [ map { $_->clone } @{$self->{indexes}} ];
	$self->{foreigns} = [ map { $_->clone } @{$self->{foreigns}} ];
	$clone
}

# добавленное поле
sub added_col {
	my ($self, $name) = @_;
	(first { $_->name eq $name } @{$self->columns})[0]
}

# добавленный индекс
*added_index = \&added_idx;
sub added_idx {
	my ($self, $name) = @_;
	(first { $_->name eq $name } @{$self->indexes})[0]
}

# добавленная связь
sub added_fk {
	my ($self, $name) = @_;
	(first { $_->name eq $name } @{$self->foreigns})[0]
}


#@@category сравнение

# для diff - название сущности
sub diff_entity {
	my ($self) = @_; 
	"tab('$self->{name}')"
}

# сравнение вложенных полей
sub diff_components {
	my ($self, $scheme) = @_;

	my $empty_col = $self->col(undef);
	my $empty_idx = $self->idx(undef);
	my $empty_fk = $self->fk(undef);
	
	return (
		(map { ($_->[0] // $empty_col)->diff($_->[1]) } cross_join { $_->name } $self->columns, $scheme->columns),
		(map { ($_->[0] // $empty_idx)->diff($_->[1]) } cross_join { $_->name } $self->indexes, $scheme->indexes),
		(map { ($_->[0] // $empty_fk)->diff($_->[1]) } cross_join { $_->name } $self->foreigns, $scheme->foreigns),
	);
}

# создать
sub diff_create {
	my ($self, $diff) = @_;
	
	
	$diff //= [$self->base->tab(undef)->diff_prop($self)];
	
	my $tab = '$' . $app->magnitudeLiteral->wordname($self->name);
	
	my $empty_col = $self->col(undef);
	my $empty_idx = $self->idx(undef);
	my $empty_fk = $self->fk(undef);
	
	my @cols = map { join "", $tab, $_->diff_entity_me, $empty_col->diff_prop($_) } @{$self->columns};
	my @ind = map { join "", $tab, $_->diff_entity_me, $empty_idx->diff_prop($_) } @{$self->indexes};
	
	return 
		join('', "my $tab = \$c->", $self->diff_entity, ";\n$tab->add(\n", 
			(map { ("\t", $_, ",\n") } @cols, @ind),
		")",  @$diff, "->create;\n\n"),
		map { $_->diff_create } @{$self->foreigns}
}

# все fk ведущие на таблицу и с неё - уничтожить!
sub diff_drop {
	my ($self) = @_;

	my $name = $self->name;
	
	return
		$self->SUPER::diff_drop,
		map { $_->diff_drop } grep { $_->tab eq $name || $_->ref_tab eq $name } $self->base->foreigns
}


#@@category DDL

# показывает sql
sub show_create {
	my ($self) = @_;
	$self->connect->show_create_table($self)
}

# показывает sql
sub show_drop {
	my ($self) = @_;
	$self->connect->show_drop_table($self)
}

# показывает sql
sub show_modify {
	my ($self) = @_;
	$self->connect->show_modify_table($self)
}

# показывает sql
sub show_rename {
	my ($self, $to) = @_;
	$self->connect->show_rename_table($self, $to)
}

# показывает sql
sub show_truncate {
	my ($self) = @_;
	$self->connect->show_truncate_table($self)
}

# очищает таблицу
*trunc = \&truncate;
sub truncate {
	my ($self) = @_;
	$self->connect->do($self->show_truncate_table($self));
	$self
}


#@@category столбцы

# создаёт столбец
*int = \&integer;
sub integer {
	my ($self, $col, $len) = @_;
	$len = "($len)" if $len;
	$self->col($col)->type("integer$len");
}

# создаёт столбец
*boolean = \&bool;
sub bool {
	my ($self, $col) = @_;
	$self->col($col)->type("boolean");
}

# создаёт столбец с плавающей строкой
*varchar = \&string;
sub string {
	my ($self, $col, $len) = @_;
	$len = "($len)" if $len;
	$self->col($col)->type("string$len");
}

# создаёт фиксированный столбец
sub char {
	my ($self, $col, $len) = @_;
	$len //= 26;
	$self->col($col)->type("char($len)");
}

# создаёт фиксированный столбец
sub text {
	my ($self, $col, $len) = @_;
	$len = "($len)" if $len;
	$self->col($col)->type("text$len");
}

# время и дата
sub datetime {
	my ($self, $col) = @_;
	$self->col($col)->type("datetime");
}

# дата
sub date {
	my ($self, $col) = @_;
	$self->col($col)->type("date");
}

# время
sub time {
	my ($self, $col) = @_;
	$self->col($col)->type("time");
}

# blob
*binary = \&blob;
*bin = \&blob;
sub blob {
	my ($self, $col, $len) = @_;
	
	$self->col($col)->type("blob");
}

# real
*float = \&real;
*double = \&real;
sub real {
	my ($self, $col, $len, $scale) = @_;
	$len //= 0;
	my $x = $scale? "($len,$scale)": $len? "($len)": "";
	$self->col($col)->type("real$x");
}

# decimal
sub decimal {
	my ($self, $col, $len, $scale) = @_;
	$len //= 11;
	$scale //= 0;
	$self->col($col)->type("decimal($len,$scale)");
}


1;