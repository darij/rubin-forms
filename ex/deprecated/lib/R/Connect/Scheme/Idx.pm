package R::Connect::Scheme::Idx;
# столбец

use base R::Connect::Scheme::Entity;

use common::sense;
use R::App;

# type - UNIQUE | FULLTEXT | SPATIAL
has qw/type cols/;

# какие ключи подгружать из базы
sub load_keys {
	qw/type cols comment/
}

# свойства для сравнения
sub diff_keys {
	qw/type cols comment/
}

# для diff - название сущности
sub diff_entity_me {
	my ($self) = @_;
	"->idx('", $self->name, "')"
}


# информация или пусто
sub info {
	my ($self) = @_;
	my $info = $self->connect->index_info->{$self->tab} // return undef;

	my $i = $info->{$self->name} // return undef;
	my $res = { %{$i->[0]} };
	$res->{cols} = [map { $_->{col} } asc { $_->{pos} } @$i];
	$res->{type} = $res->{name} eq "PRIMARY"? "PRIMARY": $res->{unique}? "UNIQUE": $res->{type}=~/FULLTEXT|SPATIAL/? $res->{type}: "INDEX";
	$res->{comment} = $res->{index_comment};
	delete @$res{qw/col pos part packed tab null unique index_comment cardinality charset collation/};
	$res

}

#@@category sql для DDL

# создаёт
sub show_create {
	my ($self) = @_;
	$self->connect->show_create_index($self);
}

# удаляет
sub show_drop {
	my ($self) = @_;
	$self->connect->show_drop_index($self);
}

# модифицирует
sub show_modify {
	my ($self) = @_;
	my $info = $self->info;
	$self->connect->show_modify_index($self);
}

# переименовывает
sub show_rename {
	my ($self, $to) = @_;
	$self->connect->show_rename_index($self, $to);
}


1;