package R::Dim;
# сервер использует стандартный роутер (app->route), который должен быть настроен в конфигурации

use base R::Fulminant;

use common::sense;
use R::App;

has qw/root act sitemap filemap counters usercounters/;

# конструктор
sub new {
	my $cls = shift;
	my $self = $cls->SUPER::new(
		name => $app->project->name . "-dim",
		port => 3000,
		act => $app->act,
		root => [$app->project->files("html")->parts],
		@_
	);
	
	$self->{root} = $app->file(@{$self->{root}});
	
	$self
}

# инициализируемся при открытии сокета
sub open {
	my ($self) = @_;
	$self->SUPER::open;
	
	my $i = -1;
	$self->{filemap} = +{
		map {
			my $root = $_;
			$i++;
			map { ("/" . $_->root($root)->path => $i) } $root->find("-f")->slices
		} $self->{root}->slices
	};
	$self->{sitemap} = $self->act->sitemap;
	
	$self
}

# перекрываем обработчик
sub ritter {
	my ($self, $request) = @_;
	my $path = $request->path;
	
	if($path =~ m!\.\w+$!) {	# возвращаем файл или 404
		my $i = $self->{filemap}->{$path};
		return [404] if !defined $i;
		$self->{counters}{$path}++;
		$self->{usercounters}{$path}++ if $request->user->id;
		return $self->{root}->eq($i)->sub($path);
	}

	# # устанавливаем сессию
	# if $request->session;
	
	return [404] if !exists $self->{sitemap}->{$path};
	
	$self->{counters}{$path}++;
	$self->{usercounters}{$path}++ if $request->user->id;
	$self->{act}->act($path, request=>$request);
}

# обрабатываем ошибки
sub middleware {
	my ($self, $request, $response) = @_;
	
	my $status = $response->status;
	
	return $response if $status < 400;
	
	my @args = (request=>$request, response=>$response);
	
	return [404, [], $self->{act}->act("/error/404", @args)] if $status == 404 && exists $self->{sitemap}->{"/error/404"};
	return [$status, [], $self->{act}->act("/error/4xx", @args)] if 400 <= $status && $status <= 499 && exists $self->{sitemap}->{"/error/4xx"};
	return [$status, [], $self->{act}->act("/error/5xx", @args)] if 500 <= $status && $status <= 599 && exists $self->{sitemap}->{"/error/5xx"};

	$response
}


1;