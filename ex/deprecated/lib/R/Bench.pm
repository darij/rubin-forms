package R::Bench;
# бенчмарк - измеряет время

use common::sense;
use R::App;

has qw/user system childuser childsystem/;

# конструктор
sub new {
	my $cls = shift;
	bless {@_}, ref $cls || $cls;
}

# запоминает время
sub time {
	my ($self, $sub) = @_;
	my ($user,$system,$cuser,$csystem) = times;
	
	$sub->(@_[2..$#_]);
	
	my ($user2,$system2,$cuser2,$csystem2) = times;
	
	$self->new(
		user=>$user2-$user,
		system=>$system2-$system,
		childuser=>$cuser2-$cuser,
		childsystem=>$csystem2-$csystem,
	)
}

# выдаёт для лога
sub log {
	my ($self) = @_;
	
	my ($user, $system, $childuser, $childsystem) = @$self{qw/user system childuser childsystem/};
	
	(":bold black", $app->perl->fsec($user), 
		($system? $app->perl->fsec($system): ())
	);
}

# печатает в лог
sub say {
	my $self = shift;
	msg @_, $self->log;
	$self
}

# время запуска интерпретатора
sub start_perl {
	$^T
}

# время запуска программы
sub start_time {
	$R::App::start_time
}


1;