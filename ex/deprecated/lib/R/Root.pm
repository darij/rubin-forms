package R::Root;
# файловая таблица роутинга

use common::sense;
use R::App;

# конструктор
sub new {
	my $cls = shift;
	bless {
		from => ["html"],
		@_
	}, ref $cls || $cls;
}

# возвращает роутеры
sub routers {
	my ($self) = @_;
	[ map {
		my $root = $_;
		my $f = $app->file($_)->find("-f"); 
		$f->map(sub { "/" . $_->root($root)->path => $_ })
	} @{$self->{from}} ];
}


1;