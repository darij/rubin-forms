package R::Yogi;
# описание роутеров в стиле данцера

use common::sense;
use R::App;

# ajax websocket on proxy

my %EXPORT = map {$_=>1} qw/
initialware middleware front onerror
route get post put del head options patch websocket
model permission
request attr query data param session
header cookie
response status
redirect root render
/;

sub import {
	my $cls = shift;
	my $package = caller;

	return _import($package, [@_]);
}

# импортирует 
sub _import {
	my ($package, $export) = @_;
	
	for my $name ( @$export ? @$export : keys %EXPORT ) {
		*{"${package}::$name"} = \&$name;
	}
	
	return;
}

#@@category роутеры

# сработает перед всеми запросами
sub initialware (&) {
	push @{caller(0)."::__Yogi__initialware__"}, @_;
}

# сработает после всех запросов
sub middleware (&) {
	push @{caller(0)."::__Yogi__middleware__"}, @_;
}


# добавляет перед всеми запросами по маске
sub front ($) {
	my $routers = \@{caller(0)."::__Yogi__route__"};
	my $response = pop @$routers;
	my $mask = pop @$routers;
	
	push @{caller(0)."::__Yogi__front__"}, $mask, $response;
	return;
}

# добавляет перед всеми запросами по маске
sub onerror ($$) {
	my ($status) = @_;
	
	my $routers = \@{caller(0)."::__Yogi__route__"};
	my $response = pop @$routers;
	my $mask = pop @$routers;
	
	push @{caller(0)."::__Yogi__error__"}, $status, $mask, $response;
	return;
}


# регистратор роутера
sub __route__ (@) {
	my ($method, $location, $response) = @_;
	
	if(!defined $location) {	# использовано как get post "/" => ...
		my $S = \@{caller(1)."::__Yogi__route__"};
		$S->[@$S-2] = "$method+".$S->[@$S-2];
		return;
	}
	push @{caller(1)."::__Yogi__route__"}, "$method $location", $response;
	return;
}

# регистраторы экшенов
sub route($$;$) {
	my ($method, $path, $response) = @_==2? ("*", @_): @_;
	$method = $1 if @_==2 && $path =~ s!^([\+\w]+)\s+!!;
	$method = join "+", @$method if ref $method;
	__route__ $method, $path, $response;
}

sub get($;$) { __route__ "GET", @_ }
sub post($;$) { __route__ "POST", @_ }
sub put($;$) { __route__ "PUT", @_ }
sub del($;$) { __route__ "DELETE", @_ }
sub head($;$) { __route__ "HEAD", @_ }
sub options($;$) { __route__ "OPTIONS", @_ }
sub patch($;$) { __route__ "PATCH", @_ }

# sub ajax($$) { 
	# my ($location, $response)  = @_;
	# __route__ "GET+POST", $location, $app->sitemapAjax()
# }

# вебсокет. Поддерживает два протокола:
# 	websocket "/ws" => { connect => ..., ... }
# и websocket "/ws" => sub { ...; on xyz => sub { ... }; ... }
sub websocket {
	my ($location, $to) = @_;
	
	my $events = [ref $to eq "CODE"? (connect => $to): @_[2..$#_]];
	
	__route__ "*", $location, $app->sitemapWebsocket(events => $events);
}

# # регистратор ajax-запроса
# sub ajax($$) {
	# caller->routers->ajax(@_);
# }

# # регистратор proxy-экшена: proxy ["GET", "POST"], "/x" => "http://127.0.0.1/xt"
# sub proxy($$;$) {
	# caller->routers->proxy(@_);
# }

# # регистратор websocket-экшена
# sub websocket($@) {
	# caller->routers->websocket(@_);
# }

# регистратор websocket-on-экшена (должен находится в обработчике websocket)
sub on($$) {
	$Coro::current->{request}{io}->on(@_);
}

# отправляет сообщение в текущий веб-сокет
sub emit (@) {
	$Coro::current->{request}->emit(@_);
}

#@@category утилиты


# # добавляет условия на поиск роутера
# sub grep (&@) {
	# my $grep = shift;
	# for my $router ( @_ ) { $router->grep($grep) }
	# @_
# }

# # добавляет хук
# sub over (&@) {
	# my $over = shift;
	# for my $router ( @_ ) { $router->over($over) }
	# @_
# }

# # добавляет хук
# sub then (&@) {
	# my $then = shift;
	# for my $router ( @_ ) { $router->then($then) }
	# @_
# }

# 
sub hook {
	my ($name, $sub) = @_;
	TODO;
}


#@@category запрос

# запрос
sub request () {
	$Coro::current->{request}
}

# атрибуты
sub attr (;@) {
	$Coro::current->{request}->attr(@_)
}

# query-параметры
sub query (;@) {
	$Coro::current->{request}->query(@_)
}

# общие
sub param (;@) {
	$Coro::current->{request}->param(@_)
}

#@@category запрос-ответ

# сессия
sub session (;@) {
	$Coro::current->{request}->session(@_)
}

# модель
sub model () {
	$app->model
}

# пользователь из сессии
sub user () {
	$Coro::current->{request}->user
}

# проверяет роли пользователя
sub permission (@) {
	my $is = request->user->role ~~ \@_;
	return $is if defined wantarray;
	die [403] if !$is;
}

#@@category запрос-ответ

sub response ();

# заголовок
sub header (;@) {
	return $Coro::current->{request}->header(@_) if defined wantarray;
	die "set header: нет параметров" if @_==0;
	die "set header: нечётные параметры" if @_ % 2 != 0;
	push @{response->headers}, @_;
}

# куки
sub cookie (;@) {
	return $Coro::current->{request}->cookie(@_) if defined wantarray;
	die "set cookie: нет параметров" if @_==0;
	die "set cookie: нечётные параметры" if @_ % 2 != 0;
	response->cookie(@_)
}

# данные
sub data (;@) {
	return $Coro::current->{request}->data(@_) if defined wantarray;
	die "set response data: нет параметров" if @_==0;
	die "set response data: несколько параметров" if @_!=1;
	response->data(@_)
	
}

#@@category ответ

# ответ
sub response () {
	$Coro::current->{response} //= $app->rsgiResponse->new_def
}

# установить статус
sub status (;$) {
	return response->status if defined wantarray; 
	response->status($_[0]);
}

# редирект на location
sub redirect ($) {
	header "Location" => $_[0];
	my $status = $Coro::current->{response}{status};
	status 307 if !$status || !(300 <= $status && $status <= 399);
	"Redirect..."
}

# выдать медиа-файл
sub root ($) {
	$app->{sitemap}->root->serve($app->rsgiRequest(via=>"GET", location => $_[0]))
}

# шаблоны
sub render ($;$) {
	$app->templateMid->render(@_)
}


1;
