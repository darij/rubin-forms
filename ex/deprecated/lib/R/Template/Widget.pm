package R::Template::Widget;
# базовый класс виджетов

use common::sense;
use R::App;

# конструктор
sub new {
	my $cls = shift;
	bless {@_}, ref $cls || $cls;
}

# вызывается при загрузке модуля
# возвращает методы для /ajax/widget_name/method_name/
sub mount {
	qw//
}

# вызывается раз на реквест
# добавляет в request заголовки
sub head {
	my ($cls) = @_;
	return;
}

# возвращает в место отображения
sub body {
	my ($self) = @_;
	""
}

#@@category вспомогательные методы

# рендерит
sub render {
	my ( $self, $path, $context ) = @_;
	if($path !~ m!^/!) {
		my ($pkg, $file, $lineno) = caller;
		$path = join '', $app->file($file)->dir, "/", $path;
	}
	
	$app->templateArey->render($path, $context);
}

# отображает всё
sub show {
	my ($self) = @_;
	#my $result = eval {
		$self->head if !$self->request->{widget}{ref $self}++;
		$self->body;
	#};
	#$@? $app->html->escape_ansi("$@"): $result
}

# возвращает запрос
sub request {
	$Coro::current->{request}
}

1;