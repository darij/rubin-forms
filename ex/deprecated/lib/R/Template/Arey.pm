package R::Template::Arey;
# модуль

use common::sense;
use R::App;
use R::Html;

# свойства
has qw//;

# конструктор
sub new {
	my $cls = shift;
	bless {
		partial => {},		# path => sub, скомпиллированные шаблоны
		@_
	}, ref $cls || $cls;
}

# рендерит
sub render {
	my ($self, $path, $data) = @_;
	join "", $self->load($path)->($data)
}

# загружает
sub load {
	my ($self, $path) = @_;
	$self->{partial}{$path} //= $self->compile($path)
}

# компилирует
sub compile {
	my ($self, $path) = @_;
	my $f = $app->file($path);
	my $pkg = "AREY::" . $app->perl->path2pkg($f->ext(""));
	my $code = $f->read;
	
	my @S;	# стек
	my $err = sub {
		my $x = $`; my $n = 1;
		$n++ while $x =~ /\n/;
		"$path:$n: $_[0]"
	};
	my $request = sub {
		my ($quest) = @_;
		die $err->("укажите значение между {{ и }}") if !length $quest;
		$quest eq "."? "\$_":
		join "", "\$A", map { "->{$_}" } split /\./, $quest;
	};
	my $compile = sub {
		$+{quest}? do {
			my $quant = $+{quant};
			my $quest = $+{quest};
			my $body = !defined($quant)? join("", "', ", "R::Html::_escape(", $request->($quest), "), '"):
			$quant eq "&"? join("", "', ", $request->($quest), ", '"):
			$quant eq "*"? do { push @S, $quest; join "", "', ; for(\@{", $request->($quest), "}) { push \@R, '" }:
			$quant eq "?"? do { push @S, $quest; join "", "'; if(", $request->($quest),") { push \@R, '" }:
			$quant eq "/"? do { my $x = pop @S; die $err->("текущий блок $quest, а закрывается $x") if $quest ne $x; "' } push \@R, '"}:
			$quant eq ">"? "', \$R::App::app->templateArey->render('$quest', \$A), '":
			$quant eq "!"? "":	# комментарий
			die "?"
		}:
		$+{esc_print}? "', do { $+{print} }, '":
		$+{print}? "', R::Html::_escape(do { $+{print} }), '":
		$+{perl}? "'; $+{print}; push \@R, '":
		$+{quot}? "\\'":
		die "?"
	};
	$code =~ s{
		\{\{ (?<quant> [!&*?/>])? (?<quest>  .*? ) \}\} |
		<\? ( (?<esc_print> =) | (?<print> :=) )? (?<perl> .*? ) \?> |
		(?<quot> ')
	}{ $compile->() }gexn;
	
	$code = "package $pkg; sub RENDER { my (\$A) = \@_; my \@R; push \@R, '$code'; \@R } 1;";
	
	$f->frontdir("var")->mkpath->write($code);
	{
		my $wd = $app->file("var")->withdir;
		local $INC[@INC] = ".";
		require $f->path;
	}
	
	\&{"${pkg}::RENDER"}
}

1;