package R::Xf;
# шаблонизатор по атрибутам тегов

use common::sense;
use R::App;
use R::Html;	# для _escape

# счётчик head-блоков. Используется для создания уникального идентификатора
my $HEAD_COUNT = 0;

# свойства
has qw/filemap dev/;

# конструктор
sub new {
	my $cls = shift;
	bless {
		dev => 1,	# по умолчанию подгружаем с диска
		filemap => $app->fileMap,
		@_
	}, ref $cls || $cls;
}

# возвращает шаблон. Путь c расширением
sub get {
	my ($self, $path) = @_;
	if($self->{dev}) {
		return $self->load($path);
	}
	$self->{filemap}->get($path)
}

# рендерит шаблон, если нужно - подгружая его
sub render {
	my ($self, $path, $data) = @_;
	my $template = ref($path) eq "CODE"? $path: $self->get($path);
	die "шаблон ".$app->perl->inline_dump($path)." не обнаружен" if !defined $template;
	$template->($data)
}

# рендерит и оборачивает
sub render_with_layout {
	my ($self, $main, $layout, $data) = @_;
	
	my $response = $self->render($main, $data);
	
	for(my $i=$#$layout; $i>=0; $i--) {
		local $data->{CONTENT} = $response;
		#msg1 "run", $layout->[$i];
		$response = $self->render($layout->[$i], $data);
	}
	
	$response
}


my $re_str = qr/ '(\\'|[^'])*' | "(\\"|[^"])*" /xn;

# заменяет символы в выражении
sub _in_exp {
	my ($self, $exp) = @_;
	
	my $exp_process = sub {
	
		exists $+{newline}? ";\n":
		exists $+{str}? $self->_ins_str($+{str}):
		exists $+{sk1}? "$+{before}->$+{sk1}":
		exists $+{set}? "$+{set} =>":
		exists $+{key}? (Num($+{key})? "->[$+{key}]": "->{$+{key}}") . ($+{sk}? "->$+{sk}": ""):
		#exists $+{arrow}? "=>":
		exists $+{gosub}? "->$+{gosub}" . ($+{sk}? "->$+{sk}": ""):
		exists $+{call}? "$+{call}(":
		exists $+{selfy}? "\$self->{$+{selfy}}":
		exists $+{arr_inline}? "->@*":
		exists $+{hash_inline}? "->%*":
		exists $+{app}? "\$app":
		exists $+{request}? "\$Coro::current->{request}":
		exists $+{response}? "(\$Coro::current->{response} //= \$app->rsgiResponse(request=>\$Coro::current->{request}))":
		exists $+{session}? "\$Coro::current->{request}->session":
		exists $+{user}? "\$Coro::current->{request}->user":
		#exists $+{role}? "\$Coro::current->{request}->role":
		exists $+{me}? "\$A":
		exists $+{nil}? "undef()":
		exists $+{true}? "true()":
		exists $+{false}? "false()":
		exists $+{loop}? "\$key":
		exists $+{hoop}? "\$for->{\$key}":
		exists $+{for}? "my \$for = do {":
		exists $+{as}? "}; for my \$key (ref \$for eq 'HASH'? keys(%\$for): @\$for) { \$A->{$+{k}}=\$key; " . ($+{v}? "\$A->{$+{v}}=\$for->{\$key}; ": ""):
		exists $+{while}? "while(":
		exists $+{do}? ") {":
		exists $+{od}? "}":
		exists $+{next}? "next":
		exists $+{last}? "last":
		exists $+{od}? "}":
		exists $+{if}? "do{if(":
		exists $+{then}? ") {":
		exists $+{elseif}? "} elsif(":
		exists $+{else}? lc "} $+{else} {":
		exists $+{fi}? "}}":
		exists $+{end}? "}":
		exists $+{asis}? lc $+{asis}:
		exists $+{var}? do { $self->{var}->{$+{var}}++; "\$A->{$+{var}}" }:
		die "?"
	};
	$exp =~ s{
		(?<newline> \n ) |
		(?<str> $re_str) |
		
		(?<set> [A-Za-z_]\w* ) : (?! [A-Za-z_]) |
		: (?<key> \w+ ) (?<sk> [\{\[])? |
		\. (?<gosub> \w+ ) (?<sk> [\{\[])? |
		
		#(?=>[\w\)]) (?<sk> [\{\[]) |
		
		(?<before> \) ) (?<sk1> [\{\[]) |
		
		# (?<arrow> : ) |
		
		(?<call> [A-Za-z_]\w* ) \( |
		\@ (?<selfy> [A-Za-z_]\w* ) |
		(?<arr_inline> \.\.\. ) |
		(?<hash_inline> \*\.\. ) |
		
		\b (?<app> app ) \b |
		\b (?<request> request | q ) \b |
		\b (?<response> response ) \b |
		\b (?<user> user ) \b |
		\b (?<nil> nil ) \b |
		\b (?<true> true ) \b |
		\b (?<false> false ) \b |
		\b (?<me> me ) \b |
		\b (?<loop> loop ) \b |
		\b (?<hoop> hoop ) \b |
		
		\b (?<for> for) \b |
		\b (?<as> as ) \s+  (?<k> [A-Za-z_]\w*) ( \s* => \s* (?<v> [A-Za-z_]\w*) )? \b |
		\b (?<while> while) \b |
		\b (?<do> do) \b |
		\b (?<od> od) \b |
		\b (?<next> next) \b |
		\b (?<last> last) \b |
		
		\b (?<if> if) \b |
		\b (?<then> then) \b |
		\b (?<elseif> elseif | elif | elsif ) \b |
		\b (?<fi> fi ) \b |
		\b (?<else> else | continue) \b |
		\b (?<end> end) \b |
		
		
		\b (?<asis> ne|eq|ge|le|gt|lt|not|or|and|last|next|return ) \b |
		
		(?<var> [A-Za-z_]\w* )
				
	}{ $exp_process->() }gxne;
	
	$exp
}

# внешний exp
sub _exp {
	my ($self, $exp, $without) = @_;
	my $esc = $without || $exp =~ s!\.RAW\s*\z!!? "": "R::Html::_escape";
	join "", "', $esc(", $self->_in_exp($exp), "), '"
}

# внешний exp
sub _exps {
	my ($self, $exp, $without) = @_;
	my $esc = $without || $exp =~ s!\.RAW(\s*)\z!$1!? "": "R::Html::_escapes";
	join "", "', $esc(do { ", $self->_in_exp($exp), " }), '"
}

# внешний exp
sub _exp_str {
	my ($self, $exp) = @_;
	join "", "\", (", $self->_in_exp($exp), "), \""
}

# строковый exp
sub _exps_str {
	my ($self, $exp) = @_;
	join "", "\", (do { ", $self->_in_exp($exp), " }), \""
}

# убирает ' и "
sub _attr_val {
	my ($s) = @_;
	$s =~ s/^((?<quot>')(?<in>.*)'|(?<quot>")(?<in>.*)")\z/$+{in}/sn;
	wantarray? ($+{quot}, $s): $s;
}

# _in_exp для атрибута
sub _in_exp_attr {
	my ($self, $s) = @_;
	$self->_in_exp(scalar _attr_val($s));
}

# распознаёт ', $xxx:x.y{.}[.](.) и {{ ... }}
my $re_exp_get = qr{
	\$ (?<get> 
		[a-z_]\w* (
			[:.]\w+ | 
			\{ ( $re_str | [^\{\}] )* \} | 
			\[ ( $re_str | [^\[\]] )* \] | 
			\( ( $re_str | [^\(\)] )* \)
		)* 
	) |
	\{\{ (?<gets> .*? ) \}\}
}sxin;

my $re_exp = qr{ $re_exp_get | (?<one_quote> ['\\] ) }x;

# разбираем вставки
sub _ins {
	my ($self, $html) = @_;
	my $let_process = sub {	exists $+{gets}? $self->_exps($+{gets}): exists $+{get}? $self->_exp($+{get}): $+{one_quote}? "\\$+{one_quote}": die "?" };
	$html =~ s{	$re_exp }{ $let_process->() }gexi;
	$html
}

# разбираем вставки в строке
sub _ins_str {
	my ($self, $s) = @_;
	$s = $app->magnitudeLiteral->from($s);
	my $str_process = sub {	exists $+{gets}? $self->_exps_str($+{gets}): exists $+{get}? $self->_exp_str($+{get}): $+{one_quote}? "\\$+{one_quote}": die "?" };
	$s =~ s{ $re_exp | (?<one_quote> ") }{ $str_process->() }gexi;
	join '', 'join("", "', $s, '")'
}

# компилирует файл по пути
my %ONE_TAG = map {$_=>1} qw/area base basefont bgsound br col command embed hr img input isindex keygen link meta param source track wbr/;

# подгружает файл, путь - /index.xf или .xf - удаляются
sub load {
	my ($self, $path) = @_;
	
	$path = join "/", @$path if ref $path;
	$path =~ s!(/index)?\.xf$!!;
	
	my $xf = $self->filemap->get($path);
	return $xf if $xf;
	
	#msg1 "load", $path;
	
	# сносим доступ к ajax:
	if($path =~ m!/~[a-z]\w*~[a-z]\w*$!) {
		$self->load($`);
		return $self->filemap->get($path);
	}
	
	my $f = $app->file("$path/index.xf", "$path.xf")->existing;
	return undef if $f->length == 0;
	die "есть два файла: ".$f->path." и ".$f->path(1).", не могу выбрать" if $f->length == 2;

	my $orig = my $html = $f->read;
	
	my $pkg = $self->package_by($path);
	my $XFClass = $pkg =~ /^XF/? "XFClass$'": die "?";	# пакет для виджетов
	
	# разбираем теги
	my @S;
	my @F;	# формы
	my $form = "";
	my @SK;		# скобки { и } для подсчёта в js
	my @subs;	# блоки виджетов
	my @ajax;	# контроллеры для ajax
	
	# выбрасывает SK
	my $pop_SK = sub {
		die "стек \@SK - пуст" if !@SK;
		my $sk = pop @SK;
		$sk = $sk->() if ref $sk;
		$sk
	};
	
	# выбрасывает тег
	my $pop =  sub {
		my ($S, $close) = @_;
		my $tag = $S->{tag};
		
		#msg1 "</$tag>";
		
		if($tag eq "form") {
			pop @F;
			$form = @F? $F[$#F]: "";
		}

		my @RET;
		
		if(defined $S->{aclose}) {
			push @RET, "'; ", $S->{aclose}, " push \@R, '";
		}
		
		if($tag ne "noop" && $tag ne "nop") {
			push @RET, $close // "</$tag>";
		}
		
		if(defined $S->{close}) {
			push @RET, "'; ", $S->{close}, " push \@R, '";
		}
		
		return @RET;
	};

	# приоритеты тегов
	my %prio = (
		script => 100,
		style => 99,
		table => 10,
		thead => 9,
		tbody => 9,
		tfoot => 9,
		tr => 7,
		td => 5,
		th => 5,
	);
	
	# разбираем теги
	my $tag_process = sub {
		
		exists $+{open}? do {
			my $tag = lc $+{tag};
			my $TAG = $+{tag};
			my $attrs = $+{attrs};
			my $once = $+{once};
			#msg1 "<$tag$attrs>";
			my @RET;
		
			# выбрасываем все у которых приоритет меньше или равен
			if($tag ne "table" && exists $prio{$tag}) {
				while(@S && $prio{$tag} >= $prio{$S[$#S]->{tag}}) {
					push @RET, $pop->(pop @S);
				}
			}
			
			push @S, my $S = {tag=>$tag, attr=>{}};
			
			# разбираем атрибуты
			my @ST;	# перед тегом ... <tag>
			my @LT; # после тега <tag> ...
			
			my @LX; # перед закрывающим тегом ... </tag>
			my @SX; # после закрывающего тега </tag> ...
			
			my $attrs_process = sub {
				exists $+{key}? do {
					my $KEY = $+{key};
					my ($key, $val) = (lc($+{key}), $+{val});
					(my $quot, $val) = _attr_val($val);
					$S->{attr}->{$key} = $val;
					
					#msg1 ":sep(=)", "attr", $key;
					
					if($key eq "if" || $key eq "in-if") {
						my $s = $self->_in_exp_attr($val);
						if($key eq "if") { push @ST, "if($s) {"; push @SX, "}"; } else { push @LT, "if($s) {"; push @LX, "}" }
						""
					}
					elsif($key eq "by" || $key eq "in") {
						my $s = $self->_in_exp_attr($val);
						$s = "my \$idx=0; my \$for=do { $s }; for my \$key (ref \$for eq 'HASH'? (sort keys %\$for): @\$for) {";
						if($key eq "by") { push @ST, $s; push @SX, "}" }
						else { push @LT, $s; push @LX, "}"; $S->{in} = 1;}
						""
					}
					elsif($key eq "as" || $key eq "key-as") {
						my $s = "local \$A->{$val} = \$key;";
						if($S->{in}) {push @LT, $s} else {push @ST, $s}
						""
					}
					elsif($key eq "val-as") {
						my $s = "local \$A->{$val} = \$for->{\$key};";
						if($S->{in}) {push @LT, $s} else {push @ST, $s}
						""
					}
					elsif($key eq "idx-as") {
						my $s = "local \$A->{$val} = \$idx++;";
						if($S->{in}) {push @LT, $s} else {push @ST, $s}
						""
					}
					elsif($key eq "value" and $form and my $f=$form->{name} and my $name=$S->{attr}->{name}) {
						$quot //= "'";
						" $KEY=\"{{ $f:$name // $quot$val$quot }}\"";
					}
					elsif($key eq "widget") {
						push @ST, "local \$A->{$val} = bless({}, '${XFClass}::$val')->fetch(\$A);";
						push @SX, 'push @R, qq{<script>\nnew '.$val.'(\$(\'script:last\').prev())\n</script>};';
						""
					}
					elsif($key eq "file") {
						if($self->dev) {
							my $f = $app->magnitudeLiteral->to($val);
							push @LT, "push \@R, \$app->file($f)->read;";
						}
						else {
							my $f = $app->magnitudeLiteral->to($app->file($val)->read);
							push @LT, "push \@R, $f;";
						}
						""
					}
					elsif($key eq "include") {
						my $sub = $self->get($val);
						die "include=\"$val\" not found" if !length $sub;
						my $pkg = $self->package_by($val);
						push @LT, "my \$C; { my \@R; ";
						push @LX, "\$C = join '', \@R } { local \$A->{CONTENT} = \$C; push \@R, ${pkg}::RENDER(\$A) }";
						#$q //= "'";
						#push @LT, "push \@R, \$Coro::current->{request}{stash}{site}->xf->render($q$path$q, \$A);";
						""
					}
					elsif($key eq "sub") {
						# блок - метод + вызов
						push @subs, $val;
						push @ST, "sub $val { my (\$A) = \@_; my \@R;";
						push @SX, "join '', \@R } push \@R, $val(\$A);";
						""
					}
					elsif($key eq "fun") {
						# функция
						push @subs, $val;
						push @ST, "sub $val { my (\$A) = \@_; my \@R;";
						push @SX, "join '', \@R }";
						""
					}
					elsif($key eq "head") {
						# записывает в $Coro::current->{request}{HEAD}
						my $id = $HEAD_COUNT++;
						push @ST, "{ next if exists \$Coro::current->{request}{HEAD}{$id}; my \@R;";
						push @SX, "\$Coro::current->{request}{HEAD}{$id} = join '', \@R }";
						""
					}
					# elsif($key =~ /^(href|src)\z/n) {
						# if($val =~ /(\.\w+)(\?|\z)/) {
							# my $path = $` . $1;
							# my $sep = length($2)? "&": "?";
							# "$key=\"$val${sep}_mtime={{ local(path) = request.site . '$path'; app.filemap.get(path) // app.file(path).mtime }}\"";
						# }
						# else { $& }
					# }
					else { $& }
				}:
				exists $+{get}? $self->_exp($+{get}):
				exists $+{gets}? $self->_exps($+{gets}):
				exists $+{one_quote}? "\\$+{one_quote}":
				die "?"
			};
			$attrs =~ s{ \s*
				(?<key> [-\w]+) (\s* = \s* (?<val> $re_str|[^\s'"]+) )? |
				$re_exp_get 
			}{ $attrs_process->() }xnge;
			
			if($tag eq "form") {
				push @F, $form = $S->{attr};
			}
			
			if($form and my $f=$form->{name} and my $name=$S->{attr}->{name}) {
				if($tag =~ /^(input|select)$/n && !$S->{attr}->{value}) {
					$attrs .= " value=\"{{ $f:$name }}\"";
				}
				elsif(!$ONE_TAG{$tag} && $tag ne "select") {
					push @LT, "if(defined(my \$v=\$A->{'$f'}->{'$name'})) { push \@R, R::Html::_escape(\$v); } else {";
					push @LX, "}";
				}
			}
			
			
			# разбор чего добавить:
			if(@SX) {
				$S->{close} = join " ", reverse @SX;
			}
			
			if(@LX) {
				$S->{aclose} = join " ", reverse @LX;
			}
			
			if(@ST) {
				push @RET, "'; ", join(" ", @ST), " push \@R, '";
			}
			
			# пропускаем теги noop и их атрибуты!
			push @RET, "<", $TAG, $self->_ins($attrs), $once, ">" if $tag ne "noop" && $tag ne "nop";
			
			if(@LT) {
				push @RET, "'; ", join(" ", @LT), " push \@R, '";
			}
			#msg1 ":inline", $once, @S;
			if($once || $ONE_TAG{$tag}) {
				#die "тег не содержит тела" if $S->{aclose};
				push @RET, $pop->(pop(@S), "");
			}
			
			join "", @RET
		}:
		# ЗАКРЫВАЮЩИЙ ТЕГ
		exists $+{close}? do {
			my $close = $+{close};
			my $tag = lc $+{tag};	# закрывающий
		
			my @RET;
		
			# выбрасываем все у которых приоритет меньше или равен
			if( exists $prio{$tag} ) {
				while(@S && $prio{$tag} > $prio{$S[$#S]->{tag}}) {
					push @RET, $pop->(pop @S);
				}
			}
		
			my $S = pop @S;
			if($S->{tag} ne $tag) {
				die "пришёл тег </$tag>, а нужен </$S->{tag}>";
			}
			
			join "", @RET, $pop->($S, $close);
		}:
		# устанавливаем текущий js-класс
		exists $+{class}? do { 
			my $class = $self->{js_class} = $+{class};
			my $extends = $+{extends};
			$extends = $extends? "our \@ISA = \"${XFClass}::${extends}\"; ": "";
			undef $self->{js_method};
			my $body = $self->_ins($+{class_body});
			push @SK, sub { undef $self->{js_class}; "}" };
			"'; package ${XFClass}::${class} { ${extends} }; push \@R, '$body";
		}:
		# js-метод	
		exists $+{method}? do {
			my ($m, $body) = @+{qw/method method_body/};
			if($m !~ /^(switch|if|for|while)$/n) {
				$self->{js_method} = $m;
				push @SK, sub { undef $self->{js_method}; "}" };
			}
			else {
				push @SK, "}";
			}
			
			$self->_ins($body)
		}:
		
		# контроллер для ajax-запроса, или метод fetch для виджета, или блок кода, к-й ничего не возвращает
		exists $+{ajax}? do {
			my $twix = $+{twix};
			my $space = $+{space};
			my $end = $+{end};
			my $class = $self->{js_class}; 
			my $method = $self->{js_method};
			
			local $self->{var} = {};				# переменные в контроллере
			my $ajax = $self->_in_exp($+{ajax});	# код контроллера
			
			if(!$class) {
				# это просто блок кода, который ничего не возвращает
				$end = $pop_SK->() if length $end;
				return "'; $ajax; push \@R, '$twix$space$end";
			}
			
			if(!$method) {
				# это лоадер для виджета
				$end = $pop_SK->() if length $end;
				return "'; sub ${XFClass}::${class}::fetch { my (\$self, \$A) = \@_; $ajax; \$self } push \@R, '$twix$space$end";
			}
			
			my $name = "${XFClass}::${class}::__${method}";			# имя подпрограммы для рендеринга
			my $lclass = lc $class;
			my $lmethod = lc $method;
			
			my $iri = "$path/~$lclass~$lmethod/";
			push @ajax, [$name, $iri];
			my ($uri_path) = $path =~ m!^(?:html|site/[^/]+)/?(.*)!s;
			my $uri = "/$uri_path/~$lclass~$lmethod/";	# uri для вызова контроллера
			# js
			my $js = "this.fetch(\"$uri\", {".join(", ", sort keys %{$self->{var}})."})";
			if(length $end) {	# <% %> }
				$js .= $twix? 
					".then((...av)=>this.ok(...av), (...av)=>this.error_ok(...av))":
					".then((...av)=>this.ok(...av), (...av)=>this.fail(...av))";
				$js .= $pop_SK->();
			}
			elsif(length $space) { # <% %>\n ... }
				if($twix) {
					# jqXHR, textStatus, errorThrown
					$js .= ".always((...av)=>this.twix(av, (data, reason, xhr, thrown) => {";
				}
				else {
					$js .= ".always((...av)=>this.always(av, (data, reason, xhr) => {";
				}
				$SK[$#SK] = sub { undef $self->{js_method}; "}))}" }
			}
			else { # <% %>.then
			}
			$js =~ s!'!\\'!g;
			"'; sub $name { my (\$A) = \@_; my \$cls = '${XFClass}::'.\$A->{'\@class'}; my \$pcls = '${XFClass}::${class}'; \$cls=\$pcls if !\$cls->isa(\$pcls); bless(\$A, \$cls)->$method } sub ${XFClass}::${class}::$method { my (\$A) = \@_; $ajax } push \@R, '$js$space$end"
		}:
		
		exists $+{ajax_fail}? "}, (data, reason, xhr, thrown) => {":
		
		exists $+{sk_open}? do { push @SK, "}"; "{" }:
		exists $+{sk_close}? $pop_SK->():
		
		exists $+{comment}? join('', "', ", ("\n" x $app->perl->lines($+{comment})), "'"):	# удаляем
		exists $+{scenario}? join('', "', ", ("\n" x $app->perl->lines($+{comment})), "'"):	# удаляем
		# вставки
		exists $+{get}? $self->_exp($+{get}):
		exists $+{gets}? $self->_exps($+{gets}):
		exists $+{one_quote}? "\\$+{one_quote}":
		die "?"
	};
	
	undef $self->{js_class};
	undef $self->{js_method};
	
	$html =~ s{
		# не обрабатываются
		(?<comment> <!-- .*? --> ) |
		(?<scenario> <script\s+scenario\b .*? </script\s*> ) |
	
		(?<close> </ (?<tag> \w[^<>'"\s]* ) \s*> ) |
		(?<open> < (?<tag> \w[^<>"'\s]*) (?<attrs> ($re_exp_get|$re_str|[^<>])*?) (?<once> /? ) > ) |
		
		$re_exp |
		
		(?<class_body> \b class \s+ (?<class> [a-z_]\w* ) ( \s+ extends \s+ (?<extends> \w+) )? \s* \{ ) |
		(?<method_body> \b (?<method> [a-z_]\w* ) \s* \( [^\(\)]* \) \s* \{ ) |
		
		(?<ajax_fail>   <%-%> ) |
		<% (?<ajax> .*? ) %> (?<twix> : )? ( (?<end> \s* \} ) | (?<space> \s*) ) |
		
		(?<sk_open> \{ ) |
		(?<sk_close> \} )
	}{ $tag_process->() }nsgexi;
	
	die "остались не закрытые фигурные скобки" if @SK;
	
	my $controller = "package $pkg; use common::sense; use R::App; sub RENDER { my (\$A) = \@_; my \@R; push \@R, '$html'; join '', \@R } 1";
	msg ":empty", "\n", $app->color->html($orig), "\n\n", $app->color->perl($controller) if $self->{log};
	$f->frontdir("var")->mkpath->write($controller);
	{
		my $wd = $app->file("var")->withdir;
		local $INC[@INC] = ".";
		require $f->path;
	}
	
	# # блоки виджетов
	# for my $sub (@subs) {
		# $self->{filemap}->set("$path/$sub", \&{"${pkg}::$sub"}, "\f");
	# }
	# @{"${pkg}::SUBS"} = @subs;
	
	# ajax-контроллеры
	for my $ajax (@ajax) {
		my ($name, $iri) = @$ajax;
		$self->{filemap}->set($iri, \&{$name});
	}
	@{"${pkg}::AJAX"} = @ajax;

	$xf = \&{"${pkg}::RENDER"};
	
	$self->{filemap}->set($path, $xf);
	
	return $xf;
}

# удаляет из filemap - для app->hotswap->remove
sub remove {
	my ($self, $path) = @_;
	
	$path =~ s!((/index)?\.xf|/)\z!!;
	
	my $pkg = $self->package_by($path);
	
	# # блоки виджетов
	# for my $sub (@{"${pkg}::SUBS"}) {
		# $self->{filemap}->del("$path/$sub", "\f");
	# }
	
	# ajax-контроллеры
	for my $ajax (@{"${pkg}::AJAX"}) {
		my ($name, $iri) = @$ajax;
		$self->{filemap}->del($iri);
	}
	
	$self->{filemap}->del($path);
	
	$self
}

# возвращает пакет по пути
sub package_by {
	my ($self, $path) = @_;
	
	my $pkg = $path;
	$pkg =~ s!(/index)?\.xf\z!!;
	join "::", "XF", map { $app->magnitudeLiteral->wordname($_) } split m!/!, $pkg;
}

#@@category Обслуживание

# при горячей замене этого модуля нужно удалить все скомпиллированые шаблоны
sub HOTSWAP_REMOVE {
	my ($self, $hotswap) = @_;
	
	$hotswap->new(file => $_)->remove for grep /\.xf$/, keys %INC;
	
	$self
}

#@@category логирование

# включить логирование
sub logon {
	my ($self) = @_;
	$self->{log} = 1;
	$self
}

# отключить логирование
sub logoff {
	my ($self) = @_;
	$self->{log} = 0;
	$self
}


my %IMG;
# base64-картинка
sub img {
	my ($self, $path) = @_;
	$IMG{$path} //= $app->file($path)->img;
}

# base64-картинка c url
sub css_img {
	my ($self, $path) = @_;
	my $img = $self->img($path);
	"url(\"$img\")"
}

1;