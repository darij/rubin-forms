package R::Form;
# менеджер форм
# app->form->myform({data})

use common::sense;
use R::App qw/$app/;
use R::Form::Form;

# конструктор
sub new {
	my $cls = shift;
	my $self = bless {
		forms_dir => "form",
		base_class => "R::Form",
		@_
	}, ref $cls || $cls;
	
	$self
}


# возвращает форму
sub AUTOLOAD {
	my ($self) = @_;
	
	our $AUTOLOAD =~ /([^:]+)$/;
	my $prop = $1;
	
	my $path = $prop;
	$path =~ s![A-Z]!/$&!g;
	$path = ucfirst($path);

	my $class = $path;
	$class =~ s!/!::!g;
	$class = "$self->{base_class}::$class";
	
	my $files = $app->project->files("$self->{forms_dir}/$path.pm")->existing;
	
	if($files->length) {
		require $files->dot->path;
	}
	
	unshift @{"${class}::ISA"}, "R::Form::Form" if !$class->can("R::Form::Form");
	
	my $data = \*{"${class}::DATA"};
	$app->sitemap->attach($class) if !eof $data;
	
	# возвможно в будущем будет кеширование, а чтобы изменить что-то в форме придётся делать deep_copy
	# но в любом случае вначале нужна i18n
	my $eval = "sub $AUTOLOAD { shift; $class->new(\@_) }";
	eval $eval;
	die if $@;
	my $sub = *{$AUTOLOAD}{CODE};
	
	goto &$sub;
}

sub DESTROY {}


1;