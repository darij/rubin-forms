package R::Sitemap;
# роутер для сайта

use base R::Fulminant;

use common::sense;
use R::App;

has qw/routemap routereg root act multi/;

sub _request ($);

# конструктор
sub new {
	my $cls = shift;
	my $self = $cls->SUPER::new(
		multi => 0,
		routemap => {},				# прямые пути: "/" => { "GET" => route }
		routereg => [],				# регулярки:   [qr!^/(?<all>.*)$!, { "GET" => route }]...
		initialware => $app->signal->new,
		middleware => $app->signal->new,
		front => $cls->freemap,
		error => undef,		# статус => sitemap
		@_
	);
		
	if(my $act = $self->{act} //= [$app->project->files("lib/Controller")->grep("-d")->parts]) {
		$act = $self->{act} = [$act] if !ref $act;
		$self->scan if @$act;
	}
	
	if(exists $self->{route}) {
		$self->route(delete $self->{route});
	}
	
	if(my $root = $self->{root} // [$app->project->files("html")->grep("-d")->parts]) {
		$root = [$root] if $root && !ref $root;
		$root = $self->{root} = $app->sitemapRoot(root => $app->file(@$root));
		$self->add("GET" => qr!^/.*\.[^/\.]+\z! => $root);
	}

	#$self->statuses(delete $self->{error});
	
	$self
}

# конструктор. создаёт пустой мап
sub freemap {
	my $cls = shift;
	
	bless {
		multi => 1,
		routemap => {},
		routereg => [],
		@_
	}, ref $cls || $cls
}

# создаёт реквест
sub _request ($) {
	my ($request) = @_;
	if(!ref $request) {
		my ($via, $location) = $request =~ /^(\w+)\s+(.*)\z/;
		$_[0] = $app->rsgiRequest(via => $via // "GET", location => $location // $request);
	}
	$_[0]
}


#@@category scan

my @METHODS = qw/GET PUT POST ROUTE DELETE HEAD OPTIONS PATCH/;
my $re_methods = join "|", map { lc $_ } @METHODS, "route", "websocket", "ajax";
my $RE_METHODS = join "|", @METHODS;

# добавляет роуты "GET /" => response, ...
sub route {
	my ($self, $routers, $replace) = @_;
	
	$routers = [%$routers] if ref $routers eq "HASH";
	my @act = $replace? (from_act => $replace->{act}): ();
	
	for(my $i=0; $i<@$routers; $i+=2) {
		my ($key, $router) = @$routers[$i, $i+1];
		my ($via, $location) = $self->path2route($key);
				
		if(!ref($router) && $router =~ m!^($RE_METHODS)\s+(/.*)\z!ion) {
			my ($_via, $_location) = $self->path2route($router);
	
			$_location = $app->magnitudeLiteral->escqq($_location);
			
			$_location =~ s!%(?<w>\w+)|%\{(?<w>\w+)\}!\$_[0]->{_attr}{$+{w}}!g;
			my $sub = eval "sub { \$app->rsgiRequest(via=>\"$_via\", location=>\"$_location\") }";
			die if $@;
		
			$router = $app->sitemapIntermediate(sitemap => $self, serve => $sub, @act);
		}
		elsif(!ref $router || ref $router eq "ARRAY" || ref $router eq "HASH") {
			$router = $app->sitemapData(serve => $router, @act);
		}
		elsif(ref $router eq "CODE") {
			$router = $app->sitemapRoute(serve => $router, @act);
		}
		elsif(Isa $router, "R::Sitemap::Route") {
			# ничего не делать
		}
		else {
			die "роутер $router имеет неподдерживаемый формат";
		}
		
		$self->add($via, $location, $router, $replace);
	}
	
	$self
}

# превращает путь в пару via, location
sub path2route {
	my ($self, $path) = @_;
	my ($via, $location) = $path =~ /^(\w+(?:\+\w+)*|\*)\s+(.*)\z/;
	return $via // "GET", $location // $path;
}

# добавляет статусы
sub statuses {
	my ($self, $status, $replace) = @_;
	
	# $routers - "GET /" => any
	while(my ($key, $routers) = each %$status) {
		die "статус `$key` невалидный" if $key !~ /^(xxx|\dxx|\d\dx|\d\d\d)\z/n;
		
		for my $status ($key =~ /^(\d\d)x$/? map { "$1$_" } 0..9:
			$key =~ /^(\d)xx$/? map { sprintf "$1%02d", $_ } 0..99:
			$key =~ /^xxx/? map { sprintf "%03d", $_ } 1..599:
			$key) {
			my $sitemap = $self->{error}{$status} //= $self->freemap;

			$sitemap->route($routers, $replace);
		}
	}
	
	$self
}

# сканирует контроллеры и создаёт отображение роутеров и шаблонов
sub scan {
	my ($self) = @_;
	
	for my $dir ( @{$self->{act}} ) {
		die "каталога `$dir` с контроллерами не существует" if !-e $dir;
		
		my $cat = $app->file($dir)->abs->path;
		
		# подбираем root из lib
		my ($lib) = desc { length $_ } grep { substr($cat, 0, length $_) eq $_ } @INC;
		
		my $files = $app->file($dir)->find("**.pm");
		FILE: for my $file ( $files->slices ) {
			my $f = $file->open;
			my $route_act = $app->sitemapAct(
				act => $file->root($lib)->path,
				sitemap => $self,
			);
			
			while(<$f>) {
				close($f), next FILE if /^__DATA__\s*$/;
				
				if(/^( 
						(?<front>front) \s+ | 
						onerror \s* ( (?<status>\d+) | ["'] (?<status>[^'"]*) ["'] ) \s* (=>|,) \s*
					)?
					(?<via>($re_methods) ( \s+ ($re_methods) )* ) \s* ["'](?<path>[^'"]*)["'] /onx) {
					
					my $front = $+{front};
					my $via = $+{via};
					my $path = $+{path};
					my $status = $+{status};
					
					$via =~ s!\s+!+!g;
					
					if($front) {
						$self->{front} //= $self->freemap;
						$self->{front}->add($via, $path, $route_act);
					}
					elsif($status) {
						$self->statuses({$status => ["$via $path" => $route_act]});
					}
					else {
						$self->add($via, $path, $route_act);
					}
					
					
				}
			}
			
			close $f;
		}
	}
	
	$self
}

# преобразовывает location к регулярке
my $re_attr = qr! \\ (
	:  (?<word>\w+) |
	\# (?<str>\w+)  |
	%  (?<int>\w+)  |
	\* (?<all>\w+)  
) !x;

sub location {
	my ($self, $location) = @_;
	
	return $location if ref $location;
	
	$location = quotemeta $location;

	return qr/^$location\z/n if $location =~ s{
		$re_attr |	\\ \( $re_attr \\ \)
	}{
		$+{word}? "(?<$+{word}>[^/.]+)":
		$+{str}? "(?<$+{str}>[^/]+)":
		$+{int}? "(?<$+{int}>\d+)":
		$+{all}? "(?<$+{all}>.*)":
		""
	}xge;
	
	return undef;
}

# добавляет роутер
sub add {
	my ($self, $via, $path, $route, $replace) = @_;
	
	die "$route не R::Sitemap::Route" if !Isa $route, "R::Sitemap::Route";
	die "путь должен начинаться на /" if ref $path ne "Regexp" && $path !~ m!^/!;
	die "$via не $RE_METHODS|*|route" if $via !~ m!^(($RE_METHODS)(\+($RE_METHODS))*|\*|route|ajax|websocket)$!oi;
	
	$route->{path} = $path;
	$route->{via} = $via;
	
	if(my $re = $self->location($path)) {
		my $routers = first { $_->[0] eq $re } @{$self->routereg};
		push @{$self->routereg}, $routers = [$re, {}, $path] if !$routers;
		$self->_merge($routers->[1], $via, $path, $route, $replace);
	} else {
		my $routers = $self->{routemap}{$path} //= {};
		$self->_merge($routers, $via, $path, $route, $replace);
	}
	
	$self
}

# мёржит методы
sub _merge {
	my ($self, $routers, $via, $path, $route, $replace) = @_;
	
	my @via = $via =~ /^(route|\*)\z/n? @METHODS: map { uc $_ } split /\+/, $via;
	for my $_via (@via) {
		my $old = $routers->{$_via};
		die "роутер `$_via $path` уже существует" . ($old? " в " . $old->{act} . $old->{from_act}: "") if $old and $replace != $old;
		$routers->{$_via} = $route;
	}
	
	$self
}

# связывает файлы из секции __DATA__ указанного пакета
sub attach {
	my ($self, $pkg) = @_;
	
	my $data = \*{"${pkg}::DATA"};
	
	if(!eof $data) {
		my %template;
		my @code;
		my $path;

		my $add_tmpl = sub {
			$path =~ s!^/!!;
			if($app->file($path)->ext eq "mid") {
				$template{$path} = join "", @code;
			}
			else {
				#die "" if exists $self->root->map->{"/$path"};
				$self->root->map->{"/$path"} = join "", @code;
			}
		};
		
		while(<$data>) {
			if(/\@\@\s*(.*?)\s*$/) {
				my $new_path = $1;
				$add_tmpl->() if length $path;
				@code = ();
				$path = $new_path;
			}
			else {
				push @code, $_;
			}
		}
		close $data;
		
		$add_tmpl->() if length $path;
		
		$app->templateMid->memory(%template);
	}
	
	$self
}


#@@category запросы

# возвращает роутинг или undef
sub _exists_route {
	my ($self, $request) = @_;
	
	my $path = $request->path;
	
	my $route = $self->{routemap}{$path};
	return $route if defined $route;

	for my $routereg ( @{$self->{routereg}} ) {
		my ($re, $route) = @$routereg;
		$request->{_attr} = {%+}, return $route if $path =~ $re;
	}

	return undef;
}

# возвращает роутер или undef
sub exists {
	my ($self, $request) = @_;
	_request $request;
	$self->_exists($request);
}

# возвращает роутер или undef
sub _exists {
	my ($self, $request) = @_;
	my $route;
	$route = $self->_exists_route($request) and $route->{$request->{via}}
}

# эмулирует полный запрос: выполняет все роутеры, в том числе и промежуточные
sub find {
	my ($self, $to) = @_;
	
	_request $to;
	
	for(my $i=2; $i<@_; $i+=2) {
		my $k = $_[$i];
		$to->data->Add($k), $i--, next if ref $k;
		$to->{headers}{$k} = $_[$i+1];
	}
	
	local $Coro::current->{request} = $to;
	local $Coro::current->{response};
	
	my $response = $self->ritter($to);
	
	$response = $self->{response}->from($response);
	
	$Coro::current->{response} //= $response;
	
	$response = $self->middleware($to, $response);
	
	$response = [$response->status, $response->headers, $response->data];
	$response->[0] == 200 && @{$response->[1]} == 0? $response->[2]: $response
}

# найти и выполнить роутер
sub serve {
	my ($self, $to, $request) = @_;
	
	my $route = $self->_exists_route($to);
	
	return [404] if !defined $route;		# not found
	
	my $router = $route->{$to->{via}};
	return [405] if !defined $router;		# method not allowed
	
	$router->serve($request // $to);
}


# рыцарь - обработчик запроса
sub ritter {
	my ($self, $request) = @_;
	
	$self->{initialware}->send($request);
	
	$self->{front}->serveAll($request);
	
	my $result = $self->serve($request);
	
	$result = $Coro::current->{response}->merge($result) if $Coro::current->{response};
	$result
}

# находит и преобразует ответ
sub middleware {
	my ($self, $request, $response) = @_;
	
	my $status = $response->status;
	my $middleware = $self->{error}{$status};
	
	if(defined $middleware) {
		$middleware->serveAll($request);
	}
	
	$self->{middleware}->send($request, $response);
	
	$response
}

# запускает все. response передаётся в $Coro::current->{response}
sub serveAll {
	my ($self, $request) = @_;

	my $path = $request->path;
	my $via = $request->via;
	my $router;
	
	my $route = $self->{routemap}{$path};
	$router = $route->{$via} if defined $route;
	$router->serve($request) if $router;

	for my $routereg ( @{$self->{routereg}} ) {
		my ($re, $route) = @$routereg;
		if(defined($router = $route->{$via}) && $path =~ $re) {
			local $request->{_attr} = {%+};
			$router->serve($request);
		}
	}

	return;
}

# представляет роутеры в виде массива
sub flat {
	my ($self) = @_;
	my $routers = [];
	
	while(my ($path, $byvia) = each %{$self->{routemap}}) {
		while(my ($via, $route) = each %$byvia) {
			push @$routers, {
				via => $via,
				path => $path,
				router => $route,
			};
		}
	}

	for my $r (@{$self->{routereg}}) {
		my ($re, $byvia, $path) = @$r;
		while(my ($via, $route) = each %$byvia) {
			push @$routers, {
				via => $via,
				path => $path // $re,
				router => $route,
			};
		}
	}
	
	$routers
}

1;