package R::Routers;
# коллекция роутеров

use common::sense;
use R::App;


# конструктор
sub new {
	my $cls = shift;
	bless {
		via => [],				# методы
		mask => undef,			# оригинальный location в виде маски (:x)/:y
		location => undef,		# регулярка для пути
		grep => [],				# функции с уточняющими условиями
		to => undef,			# подпрограмма-обработчик
		stash => {},			# глобальные переменные для этого роутера (доступны при каждом вызове роутера)
		over => [],				# хуки перед роутером
		map => [],				# хуки заменяющие роутер (сами должны запускать роутер)
		then => [],				# хуки выполняющиеся после обработки роутера
		error => {},			# ошибки Nxx и NNN (404, 4xx, 500, 5xx)
		routers => [],			# подчинённые роутеры
		controller => undef,	# контроллер в котором описан роутер
		@_,
	}, ref $cls || $cls;
}

# # магический метод: возвращает отчёт по роутеру для raise
# sub REPR {
	# my ($self) = @_;
	# $self->name
# }

# магический метод для JE - возвращает свойство
sub prop {
	my ($self, $prop) = @_;
	
	die "устанавливать нельзя" if @_>2;
	
	$self->{$prop}
}

# возвращает контроллер
sub controller {
	my ($self) = @_;
	$self->{controller}
}

#@@section Поиск среди роутеров

# ищет роутер в прилинкованных и выдаёт его или undef
sub search {
	my ($self, $q, $pos) = @_;
	
	my $location = $q->{location};
	my $via = $q->{via};
	my $routers = $self->{routers};
	
	for my $router (@$routers) {
		
		#msg1 "search", ":cyan", $router->name, ":magenta", "$via $location", $router->{location};
		# msg1 ":cyan", $router->name, ":magenta", "$via $location", ":reset", "via", $via ~~ $router->{via},		"location", scalar($location =~ $router->{location});
		
		pos($location) = $pos;
		my $re = $router->{location};
		next if $location !~ /$re/g;
		
		if(do { $' =~ /^(\?|\z)/ }) {
			%{ $q->{_attr} } = %+;
			my $rvia = $router->{via};
			#msg1 ":size10000", "route", $q, $router;
			return $router if ($rvia->[0] eq "*" || $via ~~ $rvia) and all { $_->($q) } @{$router->{grep}};
			next;
		}
		
		my $in = $router->search($q, pos $location);
		return $in if $in;
	}
	
	return;
}


#@@section опции

# создаёт и возвращает роутер. Роутер регистрируется в верхнем роутере
sub route {
	my $self = shift;
	push @{$self->{routers}}, my $route = $self->new;
	$route->via(@{shift()}) if ref $_[0];
	my ($location, $to) = @_;
	$route->location($location);
	$route->to($to) if defined $to;
	$route
}

# устанавливает/возвращает location
sub location {
	my $self = shift;
	if(@_) {
		my $location = shift;
		
		if(!ref $location) {
			$self->{mask} = $location;
			$location = quotemeta $location;
			
			my $x = qr! \\ (
				:  (?<word>\w+) |
				\# (?<str>\w+)  |
				%  (?<int>\w+)  |
				\* (?<all>\w+)  
			) !x;
		
			$location =~ s{
				$x |	\\ \( $x \\ \)
			}{
				$+{word}? "(?<$+{word}>[^/.]+)":
				$+{str}? "(?<$+{str}>[^/]+)":
				$+{int}? "(?<$+{int}>\d+)":
				$+{all}? "(?<$+{all}>.*)":
				""
			}xge;

		}
		
		$self->{location} = qr!\G$location!;

		$self
	}
	else {
		$self->{location}
	}
}

# добавляет условие на выбор
sub grep {
	my $self = shift;
	push @{$self->{grep}}, @_;
	$self
}

# указывает методы
sub via {
	my $self = shift;
	push @{$self->{via}}, @_;
	$self
}

# устанавливает stash
sub stash {
	my $self = shift;
	%{$self->{stash}} = @_;
	$self
}

# хук для роутера
sub over {
	my $self = shift;
	push @{$self->{map}}, @_;
	$self
}

# хук для роутера
sub then {
	my $self = shift;
	push @{$self->{then}}, @_;
	$self
}

# устанавливает обработчик "не найдено" (404)
sub not_found {
	my ($self, $to) = @_;
	$self->{not_found} = $to;
	$self
}

# устанавливает обработчик ошибки (500)
sub catch {
	my $self = shift;
	push @{$self->{catch}}, @_;
	$self
}


# устанавливает назначение
sub to {
	my ($self, $to) = @_;
	die "route(${\$self->name})->to уже установлено" if $self->{to};
	$self->via("*") if !@{$self->{via}};
	$self->{to} = $to;
	$self
}

# via+маска
sub name {
	my ($self) = @_;
	(@{$self->{via}}? join("|", @{$self->{via}}): "*").(@{$self->{grep}}? " GREP": "")." $self->{mask}"
}



#@@section Роутеры по методам



# обращение по методу get
sub get {
	my ($self, $location, $to) = @_;
	$self->route($location)->via("GET")->to($to);
}

# обращение по методу post
sub post {
	my ($self, $location, $to) = @_;
	$self->route($location)->via("POST")->to($to);
}

# обращение по методу put
sub put {
	my ($self, $location, $to) = @_;
	$self->route($location)->via("PUT")->to($to);
}

# обращение по методу delete
sub del {
	my ($self, $location, $to) = @_;
	$self->route($location)->via("DELETE")->to($to);
}

# обращение по методу patch
sub patch {
	my ($self, $location, $to) = @_;
	$self->route($location)->via("PATCH")->to($to);
}

# обращение по методу options
sub options {
	my ($self, $location, $to) = @_;
	$self->route($location)->via("OPTIONS")->to($to);
}

# обращение по методу head
sub head {
	my ($self, $location, $to) = @_;
	$self->route($location)->via("HEAD")->to($to);
}

# обращение по ajax
sub ajax {
	my ($self, $location, $to) = @_;
	$self->route($location)->via("GET", "POST")->grep(sub { shift->header("X-Requested-With") eq "XMLHttpRequest" })->to($to);
}

# обращение по методу websocket
sub websocket {
	my ($self, $location, $to) = @_;
	
	my @io = ref $to eq "CODE"? (connect => $to): @_[2..$#_];
	
	$self->route($location)->grep(sub {
		my ($q) = @_;
		$q->header("Upgrade") =~ /\bwebsocket\b/i && $q->header("Connection") =~ /\bUpgrade\b/i
	})->to(sub {
		my ($q) = @_;
	
		# создаём менеджера операций ввода-вывода
		my $io = $q->{io} = $app->rsgiIo->new(@io); 

		# бесконечный цикл
		$io->adventure($q);
		#msg1 "end!", $@;
	});
}


#@@section Ошибки

# добавляет обработчик ошибки
sub error {
	my ($self, $number, $to) = @_;
	$self->{error}{$number} = $to;
	$self
}


#@@section запросы к роутерам

# создаёт запрос
sub mkrequest {
	my ($self, $via, $location, $headers, $data) = @_;

	#msg1 $via, $location, $headers, $data,
	@_==2? $via: $app->rsgiRequest(
		protocol => "HTTP/1.1",
		via => $via,
		location => $location,
		header => $app->rsgiBag($headers),
		data => $app->rsgiBag($data),
	)
}

# емулирует запрос
sub query {
	my $self = shift;
	
	my $q = $self->mkrequest(@_);
	my $router = $self->search($q);

	die [404] if !defined $router;
	
	$q->{stash} = $router->{stash};
	
	my $unrequest = guard { !Scalar::Util::blessed($Coro::current)? undef($Coro::current): delete $Coro::current->{request} };
	$Coro::current->{request} = $q;
	
	#$self->{over}->($q) if $self->{over};
	my $result = $router->{to}->($q);
	#$self->{then}->($q) if $self->{then};
	
	undef $unrequest;
	
	$result
}

# перекрываем ошибки
sub middleware {
	my ($self, $q, $response) = @_;
	
	my $err = $response->status;
	
	#return $response if $err == 200;
	
	my $error = $self->{error};
	my $handler = $error->{$err} // $error->{int($error / 100) . "xx"};
	if(!$handler) {
		$response->data("middleware: " . $response->status . " " . $response->reason) if !defined $response->data;
		return $response;
	}
	
	my $unrequest = guard { !Scalar::Util::blessed($Coro::current)? undef($Coro::current): delete $Coro::current->{request} };
	$Coro::current->{request} = $q;
	
	my $res = $handler->($q, $response);
	$res = $response->new(res=>$res)->resulting($err) if !Isa $res, ref $response;
	
	undef $unrequest;
	
	return $res;
}

# эмулирует полноценный запрос
#@throws ()
#@returns R::Rsgi::Response
sub request {
	my $self = shift;
	my $q = $self->mkrequest(@_);
	my $result = eval {	$self->query($q) };
	$result = $app->rsgiResponse(res => $@)->resulting(500, "html") if $@;
	$result = $app->rsgiResponse(res => $result) if !Isa $result, "R::Rsgi::Response";
	
	eval {
		$self->middleware($q, $result);
	};
	if($@) {
		my $err = eval {"$@"} // "/no stringify error/";
		$result = $app->rsgiResponse(keepAlive=>0, res => "request: $err")->resulting(500, "html");
	}
	
	return $result;
}


#@@section фабричные методы

# возвращает связанный запросник
sub querier {
	my ($self) = @_;
	$app->rsgiQuerier(routers => $self)
}

# возвращает связанный запросник. Но он возвращает всегда response
sub requester {
	my ($self) = @_;
	$app->rsgiRequester(routers => $self)
}

1;