package R::Perlx;
# аналог jsx: транслирует perl c html (plx) в код perl

use common::sense;
use R::App;


# конструктор
sub new {
	my ($cls) = @_;
	bless {
		input => "plx/**/*.plx",				# входная директория
		output => "var/lib",					# выходная директория
	}, ref $cls || $cls;
}

# регистрирует хук в @INC
sub register {
	my ($self) = @_;
	
	#push @INC, sub { msg1 "push", $_[1]; };
	#msg1 "start";
	#unshift @INC, sub { msg1("unshift", $_[1]); 0 };
	$app->log;
	$app->file;
	*CORE::GLOBAL::require = sub {
		my ($path) = @_;
		if($path =~ /\.plx\z/) {
			for my $inc (@INC) {
				my $p = "$inc/$path";
				if(-f $p) {
					my $f = $app->file($p)->read;
					$f = $self->trans($f);
					return eval $f;
				}
			}
		}
		CORE::require($path);
	};
	
	$self
}

# с plx на pl
sub trans {
	my ($self, $code) = @_;
	
	# s{
		# #.*		|
		# (?<tag> <[a-z] )
	# }{}gx;
	
	$code
}

# компилирует указанный файл
sub cc {
	my ($self, $in, $out, $ext) = @_;
	
	$in //= $self->{input};
	$out //= $self->{output};
	
	$app->file($in)->find($app->perl->like($in))->then(sub {
		$app->file("$out/".$_->path)->write($self->trans($_->read));
	});
	
	$self
}



# perl5 в js
my @perl2js = (
	"{" => sub { "{" },
	"}" => sub { "}" },
	
);

for @perl2js;

sub tojs {
	my ($self, $code) = @_;
	
	# разбиваем на составляющие
	my @code = split m{
		\{\}
	}x, $code;
	
	#
	
	$_
}

1;