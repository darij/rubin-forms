package R::Portal::Access;
# доступ

use common::sense;
use R::App;

# свойства
has qw/layout error/;

# конструктор
sub new {
	my $cls = shift;
	bless {@_}, ref $cls || $cls;
}

# клонирует и проверяет доступ
sub access {
	my ($self, $q) = @_;
	
	# копируем себя
	bless {%$self}, ref $self || $self
}

1;