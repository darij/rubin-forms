package R::Site::Controller;
# модуль

use common::sense;
use R::App;

# свойства
has qw/site/;

# конструктор
sub new {
	my $cls = shift;
	bless {
		@_
	}, ref $cls || $cls;
}

# возвращает контроллер из загруженных
sub get {
	my ($self, $path) = @_;
	if($self->{site}->dev) { return $self->load($path); }
	$self->site->filemap->get($path)
}

my @PROTO = qw/post put delete head options patch/;

# загрузка контроллера
sub load {
	my ($self, $path) = @_;
	
	$path = [split m!/!, $path] if !ref $path;
	
	die "контроллер не может начинаться на `.`" if $path->[$#$path] =~ m!/^\.!;
	
	my $is = $self->site->filemap->get($path);
	return $is if defined $is;
	
	my $xpath = join "/", @$path;
	$xpath =~ s!\.pl!!;
	my $f = $app->file("$xpath.pl", map {"$xpath.$_.pl"} @PROTO)->existing;
	return undef if $f->length == 0;
	
	#msg1 ":inline sep( -> )", "load action", $xpath, $path;

	# класс контроллера
	my $pkg = join "::", "SITE", map { $app->magnitudeLiteral->wordname($_) } split m!/!, $xpath;
	
	my $first = " use base R::Action; our %RENDER; sub RENDER { \\%RENDER }";
	for my $e ( $f->slices ) {
		my ($sub) = $e->exts =~ /(\w+)\.pl$/;
		$sub //= "get";
		my $controller = "package $pkg;$first use common::sense;use R::App; sub ${\uc($sub)} { my (\$self) = \@_; my \$q = \$self->{request}; my \$stash = \$q->{stash}; ${\$e->read} } 1";
		$first = "";
		$e->frontdir("var")->mkpath->write($controller);
		
		{
			my $wd = $app->file("var")->withdir;
			local $INC[@INC] = ".";
			require $e->path;
		}

		# определяем наличие шаблона
		my $r = $e->ext("xf");
		if($r->exists) {
			${"${pkg}::RENDER"}{uc $sub} = $self->site->xf->load($r->path);
		}
		
	}

	my $ret = $pkg->new;
	#msg1 "set", "$xpath.pl"; 
	$self->site->filemap->set("$xpath.pl", $ret);
	return $ret;
}

1;