package R::Access;
# базовый класс для доступа

use common::sense;
use R::App;

# свойства
has qw/request/;

# конструктор
sub new {
	my $cls = shift;
	bless {@_}, ref $cls || $cls;
}

1;