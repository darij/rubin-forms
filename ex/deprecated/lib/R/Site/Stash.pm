package R::Site::Stash;
# передаётся в request->stash. В .access.pl может устанавливаться

use common::sense;
use R::App;

# свойства
has qw/site/;

# конструктор
sub new {
	my $cls = shift;
	
	#my $xf = $app->file("html/errors")->find("*.xf");
	#my @error = map {  } $xf->slices;
	
	bless {
		layout => [ grep { -e $_ } qw!html/menu.xf html/layout.xf!],	# окружение для шаблонов в контроллерах		
		error => { },	# 404 => "/errors/404/" роутинг ошибок: указывается путь. 4 => "...", для всех 400-х, если не найдено 
		@_
	}, ref $cls || $cls;
}

# устанавливает в stash ошибки
sub error {
	my $self = shift;
	die "где параметры?" if !@_;
	$self->{error} = +{@_};
	$self
}

# устанавливает в stash шаблоны окружения
sub layout {
	my $self = shift;
	die "где параметры?" if !@_;
	$self->{layout} = [@_];
	$self
}


1;