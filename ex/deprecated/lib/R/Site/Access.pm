package R::Site::Access;
# содержит ассеты сайта

use common::sense;
use R::App;

# свойства
has qw/site/;

# конструктор
sub new {
	my $cls = shift;
	bless {
		@_
	}, ref $cls || $cls;
}


# возвращает актуальный access для каталога (вверх по иерархии)
sub get {
	my ($self, $path) = @_;
	
	$path = [split m!/!, $path] if !ref $path;
	delete local $path->[$#$path]; # отбрасываем файл. Для путей контроллеров: html/index/ -> html
	
	if($self->{site}->dev) { return $self->find($path); }
	$self->site->filemap->rfind($path, ".access.pl")
}

# ищет на диске вверх по иерархии и компиллирует
# path - каталог
sub find {
	my ($self, $path) = @_;
	
	$path = join "/", @$path if ref $path;
	my $dir = $app->file($path);
	
	my @path = split m!/!, $dir->path;
	for(my $i=$#path; $i>=0; $i--) {
		my $dir = join "/", @path[0..$i];
		my $access = join "/", $dir, ".access.pl";
		if(-e $access) {
			return $self->load($access);
		}
	}
	
	return undef;
}

# загружает файл доступа для каталога path=html/...
sub load {
	my ($self, $path) = @_;
	
	# если уже загружен - то возвращаем
	my $access = $self->site->filemap->get($path);
	return $access if defined $access;
	
	$path = join "/", @$path if ref $path;
	$access = $app->file($path);
	
	my @path = split m!/!, $access->dir;
	my $access_pkg = join "::", "ACCESS", (map { $app->magnitudeLiteral->wordname($_) } @path);
			
	my $basic_pkg = @path>1? $self->find(join "/", @path[0..$#path-1]): "";
	$basic_pkg = ref($basic_pkg) || "R::Action";
	
	my $f = $access->read;
	my $access_f = "package $access_pkg;use base $basic_pkg; use common::sense; use R::App; sub ACCESS { my (\$self) = \@_; my \$q = \$self->{request};  my \$stash = \$q->{stash}; $f } 1";
	$access->frontdir("var")->mkpath->write($access_f);
		
	{
		my $wd = $app->file("var")->withdir;
		local $INC[@INC] = ".";
		require $access->path;
	}

	my $acc = $access_pkg->new;
	$self->site->filemap->set($path, $acc);
	return $acc;
}

1;