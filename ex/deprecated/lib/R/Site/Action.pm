package R::Action;
# базовый для акций

use common::sense;
use R::App;

# свойства
has_const qw/request/;

# конструктор
sub new {
	my $cls = shift;
	bless {@_}, ref $cls || $cls;
}

# запускает
sub run {
	my ($self) = @_;

	my $via = $self->{request}->via;
	my $response = $self->$via;
	if(my $tmpl = $self->RENDER->{$via} and ref $response eq "HASH" and $self->{request}->header->Accept =~ m!\b text/html \b!x) {
		my $xf = $self->{request}->{stash}->{site}->xf;
		my $layout = $self->{layout} // $self->{request}->{stash}->{layout};
		$response = $xf->render_with_layout($tmpl, $layout, $response);	
	}
	
	$response
}

sub GET { [501] }
sub POST { [501] }
sub PUT { [501] }
sub HEAD { [501] }
sub OPTIONS { [501] }
sub DELETE { [501] }
sub PATCH { [501] }


# устанавливает лайоуты
sub layout {
	my $self = shift;
	$self->{layout} = [@_];
	$self
}

1;