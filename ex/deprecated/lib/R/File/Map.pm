package R::File::Map;
# карта значений. Строит иерархическую структуру

use common::sense;
use R::App;

# свойства
has qw//;

# конструктор
sub new {
	my $cls = shift;
	bless {
		root => {},
		@_
	}, ref $cls || $cls;
}

# проверяет, что значение существует
sub exists {
	my ($self, $path, $key) = @_;
	$path = [split m!/!, $path] if !ref $path;
	my $c = $self->{root};
	for my $part (@$path) {
		$c = $c->{$part} or return "";
	}
	
	exists $c->{$key // "\e"}
}

# возвращает значение или undef
sub get {
	my ($self, $path, $key) = @_;
	$path = [split m!/!, $path] if !ref $path;
	
	my $c = $self->{root};
	for my $part (@$path) {
		!exists $c->{$part} && return undef;
		$c = $c->{$part};
	}
		
	$c->{$key // "\e"}
}

# устанавливает значение
sub set {
	my ($self, $path, $val, $key) = @_;
	$path = [split m!/!, $path] if !ref $path;
	
	my $c = $self->{root};
	for my $part (@$path) {
		$c = $c->{$part} //= {};
	}
	
	$key //= "\e";
	
	die "устанавливать можно только один раз: " . join("/", @$path) . " = $val" if exists $c->{$key};
	
	$c->{$key} = $val;
	
	$self
}

# удаляет значение
sub del {
	my ($self, $path, $key) = @_;
	$path = [split m!/!, $path] if !ref $path;
	
	my $c = $self->{root};
	for my $part (@$path) {
		!exists $c->{$part} && return $self;
		$c = $c->{$part};
	}
	
	delete $c->{$key // "\e"};
	
	$self
}


# ищет в пути все установленные значения
sub rget {
	my ($self, $path, $key) = @_;
	
	$path = [split m!/!, $path] if !ref $path;
	
	my $c = $self->{root};
	my @vals;
	$key //= "\e";
	for my $part (@$path) {
		!exists $c->{$part} && last;
		$c = $c->{$part};
		exists $c->{$key} && push @vals, $c->{$key};
	}
	
	wantarray? @vals: @vals? $vals[$#vals]: undef;
}

# возвращает итератор для пробега по файлам
sub iterator {
	my ($self) = @_;
	
	TODO;
	
	$self
}

1;