package R::Portal;
# сервер с роутингом
# портал управляет 

use base R::Fulminant;

use common::sense;
use R::App;

# свойства
has qw/dev rename access xf filemap/;



# конструктор
sub new {
	my $cls = shift;
	
	$cls->SUPER::new(
		dev => $app->ini->{dev},		# если dev - то проверять время изменения
		#rename => $app->router->new,	# qr// => new_path - обработчики роутинга
		filemap => $app->fileMap,
		xf => $app->xf,					# шаблоны сайта
		#max_age => 60*60*24*365*20,		# 20 лет
		
		access => {
			"html" => {
				"/" => {
					error => {
						404 => "xf/errors/404.xf",
						4 => "xf/errors/4xx.xf",
						5 => "xf/errors/5xx.xf",
					},
					layout => ["xf/layout.xf", "xf/menu.xf"],
					allow => "*"
				},
			},
		},
		@_
	);
}

# запуск портала
sub run {
	my ($self) = @_;
	
	$self->xf->filemap($self->filemap)->dev($self->dev);
	
	# лайоуты, ошибки и права доступа
	my @paths;
	my $access = $self->{access};
	while(my ($path, $site) = each %$access) {
		push @paths, $path;
		while(my ($sub_path, $val) = each %$site) {
			my $portal_access = $app->portalAccess(%$val);
			$self->{filemap}->set("$path$sub_path", $portal_access, "\a");
		}
	}

	$app->hotswap->scan_html(@paths) if $self->{dev};
	
	# подгружаем карту файлов
	if(!$self->{dev}) {
		for my $f ($app->file(@paths)->find("-f")->slices) {
			if($f->ext eq "xf") {
				$self->xf->load($f->path);
			}
			else {
				$self->filemap->set($f->path, $f->mtime);
			}
		}
	}
	
	$self->log($app->log) if $self->{dev} && !$self->log;
	
	$self->SUPER::run
}

# рыцарь - обработчик запроса
sub ritter {
	my ($self, $request) = @_;
	
	my $host = $request->url->host;
	my $path = $request->path;

	# трансформируем путь
	my @path = split m!/!, $path;
	
	if($host ne "localhost") { $path[0] = $host; unshift @path, "site" }
	else { $path[0] = "html" }
	
	die [503, "обнаружено .. в пути"] if grep { $_ eq ".." } @path;
	
	# доступ
	my $access = $self->filemap->rget(\@path, "\a");
	if(defined $access) {
		$request->{access} = $access->access($request);
	}
	
	# статика
	if($path =~ m!\.(\w+)\z!) {
		# в режиме dev ищем на диске
		my $is_file = $self->{dev}? -e join("/", @path): $self->filemap->get(\@path) == 1;
		return [404, "нет файла " . join "/", @path] unless $is_file;
		return $app->file(join "/", @path);
	}
	
	# перевод на .../ - устарел. В процессе
	if($path !~ m!/\z!) {
		my $url = $request->url->path("$path/");
		return [307, [Location => join(":", $url->scheme, $url->opaque), "Content-Type" => "text/plain"], "307 Redirect on path/"];
	}
	
	if($self->{dev}) {
		$app->hotswap->startscan, $self->{FLAG_SCAN} = 1 if !$self->{FLAG_SCAN};
		$app->hotswap->scan if $self->{FLAG_SCAN};
	}
	
	my $action = $self->xf->get(\@path);
	
	if(!defined $action) {
		# пробуем заменить индексной страницей
		push @path, "index.html";
		# в режиме dev ищем на диске
		my $is_file = $self->{dev}? -e join("/", @path): $self->filemap->get(\@path) == 1;
		return [404, "404 - нет " . join "/", @path[0..$#path-1]] unless $is_file;
		return $app->file(join "/", @path);
	}
	
	# ставим свой HEAD 
	local $request->{HEAD} = {};
	
	# без лайоутов
	if($request->header->Accept !~ m!\btext/html\b!) {
		return $action->($request->param->All);
	}

	$self->xf->render_with_layout($action, $request->{access}->{layout}, $request->param->All)
}

# находит и преобразует ответ
sub middleware {
	my ($self, $request, $response) = @_;
	
	$app->hotswap->startscan if $self->{dev} && $request->path =~ /\/$/;
	
	my $status = $response->status;
	my $controller = $request->{access}->{error}->{$status};
	if(!$controller) { $controller = $request->{access}->{error}->{substr $status, 0, 1}; }
	
	if(ref $controller eq "SCALAR") {
		$request->path($$controller);
		$response = $self->ritter($request);
	}
	elsif(defined $controller) {
		my $html = $self->xf->render_with_layout($controller, $request->{access}->{layout}, {});
		$response->data($html);
	}
	
	$response
}

1;