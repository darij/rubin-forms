package R::Template::Mid;
# транслятор c django-подобного шаблона в классы perl
# создаёт класс
# тесты <% TEST: %> и админка <% ADMIN: %> пропускаются

use common::sense;
use R::App;
use R::Html;
use R::Template::Filters;
use R::Template::Subs;
use R::Template::Defer;
use Time::HiRes qw//;


has qw/inc base base_class ext show_code/;

# конструктор
sub new {
	my $cls = shift;
	bless {
		inc => ["mid"],			# добавочные пути
		base => "_Mid_",
		ext => "mid",
		classes => {},			# path => cls
		classloadtime => {},	# cls => time
		memory => {},			# можно указать какие брать не из диска, а из этого хеша
		#check => 0,			# проверять изменение времени на диске
		@_
	}, ref $cls || $cls;
}

# выполняет экшн
sub render {
	my ($self, $path, $data) = @_;
	
	$path =~ s!^/!!;
	
	my $cls = $self->retrive($path);
	
	local $data->{__mid__} = $self;	
	local $Coro::current->{render} = $self if !$Coro::current->{render};
	
	bless $data, $cls;
	
	#local $data{_self} = $data if !exists $data->{_self};
	
	$data->RENDER
}


# добавляет в memory шаблоны, проверяя, чтобы их пути не пересекались с шаблонами на диске
sub memory {
	my $self = shift;
	
	my $memory = $self->{memory};
	my %hash;
	
	if(@_==1) {
		%hash = ref $_[0] eq "ARRAY"? @{$_[0]}: %{$_[0]};
	}
	else {
		%hash = @_;
	}
	
	while(my ($path, $template) = each %hash) {
		$path = $self->path($path);
		die "app->mid->memory: шаблон $path уже существует" if $self->{classes}{$path};
		
		die "app->mid->memory: шаблон $path есть на диске " if $self->file($path)->length;
		
		$self->{memory}{$path} = $template;
	}

	
	$self
}

# проверяет наличие класса и, если нужно, то загружает его
sub retrive {
	my ($self, $path) = @_;

	$path = $self->path($path);
		
	my $cls = $self->{classes}{$path};
	
	return $self->load($path) if !defined $cls;
	
	# # если изменился хоть один из сторонних модулей
	# $self->{check} && $app->process->autoreload;
	
	# return $self->load($path) if $self->{check} && $self->file($path)->mtime > $self->{classloadtime}{$cls};
	
	$cls
}

# загрузить компил.
sub load {
	my ($self, $path) = @_;

	my $code = delete $self->{memory}{$path};
	$code = $self->file($path)->read if !defined $code;
	
	$self->compile($path, $code);
}

# преобразует выражение
my $ID = qr/[a-zA-Z_]\w*/;
my $STR = qr/"(\\"|[^"])*" | '(\\'|[^'])*'/x;
my $NUM = qr/-?\d+\.\d+/;
sub expression {
	my ($self, $exp) = @_;
	
	# a|add:b,"xx",1|rm|uc:"tt",12 -> uc(  a|add:b,"xx",1|rm , "tt",12 )
	
	my $f = sub {
		$+{method}? "->$+{method}":
		$+{key}? "->{$+{key}}":
		$+{inkey}? "$+{inkey} =>":
		$+{assignvar}? "; local \$self->{$+{assignvar}} = ":
		$+{var} eq "_self"? '$self':
		$+{var} eq "_main"? '$Coro::current->{render}':
		$+{var} eq "app"? '$R::App::app':
		$+{var} eq "request"? '$Coro::current->{request}':
		$+{var} =~ /^(and|or|not|eq|ne|lt|gt|le|ge)$/? $1:
		$+{var}? "\$self->{$+{var}}":
		$+{str}? $+{str}:
		$+{or}? $+{or}:
		$+{filter}? do {
			my $param = $+{param};
			my $filter = $+{filter};
			my $head = $+{head};
			join("", "R::Template::Filters::", $filter, "(", $self->expression($head), $param? (",", $self->expression($param)): (), ")") 
		}:
		"???"
	};
	
	$exp =~ s{
		\. (?<method>$ID) |
		: (?<key>$ID) |
		(?<inkey>$ID) \s* : (?! [a-zA-Z_] ) |
		(?<assignvar>$ID) \s* = (?! = ) |
		(?<str> $STR ) |
		(?<or> \|\| ) |
		(?<head> \S+) \| (?<filter> $ID) ( : (?<param> ("[^"]*"|'[^']*'|\S)+?) (?>\s|$) )? |
		(?<var>$ID)
	}{
		$f->();
	}gxne;


	$exp
}

sub compile {
	my ($self, $path, $code) = @_;
	
	die "файл $path уже существует" if exists $self->{classes}{$path};
	
	my $save_code = $code;
	
	my $file = $app->file($path);
	
	my $cls = join "::", $self->{base}, map { $app->magnitudeLiteral->wordname($_) } split m!/!, $file->ext("")->path;

	my @S;
	my $extends;
	my $stmt_counter = 0;
	
	my $replace = sub {
		
		my ($before, $after);
		my ($s1, $s2);
		
		if(!defined $+{s1} or !defined $+{s2}) {
			($before, $after) = @+{qw/s1 s2/};
		}
		else {
			($s1, $s2) = @+{qw/s1 s2/};
		}
		
		my $ret = exists $+{rem}?	do { s/'/\\'/g; "', do { '$s1$+{rem}$2'; () }, '" }: do {
		
			local $_ = $+{block} // $+{print};
			my $print = $+{print};
			
			s!\\'!'!g;
			
			$print?	do {
				my $raw = s/\|raw$// || /\|(\w+)$/ && $R::Template::Filters::NOESC{$1};
				my $exp = $self->expression($_);
				$exp = $raw? "do { $exp }": "R::Html::_escapes(do { $exp })";
				defined($s1)? do {
					length $s1 and $s1 = "'$s1', ";
					length $s2 and $s2 = ", '$s2'";
					#my $exp1 = $exp;
					#$exp1 =~ s/'/\\'/g;
					#!~ /^\\s*\\z/
					"', do { my \$r = join '', $exp; length(\$r)? ($s1\$r$s2): () },'"
				}: "', $exp, '";
				#defined $s1? "', do { my \$r=join '', $exp; length(\$r)? ('$s1', \$r, '$s2'): () }, '": "', $exp, '";
			}:
			
			s/^extends\s*($STR)//? do {
				die "use {% extends \"path\" %}" if length $_;
				$extends = $app->magnitudeLiteral->unstring($1); "$s1$s2" }:
			s/^include\b(?<exp>($STR|.)+?)\s*(\bwith\b(?<with>.*))?$//n? do {
				my ($exp, $with) = ($+{exp}, $+{with});
				die "use {% include \"path\" ... %}" if length $_ and !$with;
				$exp = $self->expression($exp);
				$with = defined($with)? $self->expression($with): "";
				"'; { $s1$with; push \@out, \$self->{__mid__}->render($exp, \$self); bless \$self, __PACKAGE__; } push \@out, $s2'" }:
			s/^block\s+($ID)//? do {
				die "use {% block name %}" if length $_;
				push @S, ["block", $1];
				"';$s1 sub $1 { my (\$self) = \@_; my \@out; $_; push \@out, $s2'" }:
			s/^defer\b\s*//s?	do {
				# defer выполнится в конце функции и вставит на своё место свой рендер
				die "use {% defer %}...{% enddefer %}" if length $_;
				push @S, ["defer"];
				"', ${s1}R::Template::Defer->new(context=>\$self, code=>sub { my (\$self) = \@_; my \@out; push \@out, $s2'"
			}:
			s/^ifchange\b\s*(.*)//s?	do {
				my $exp = $self->expression($1);
				my $idx = $stmt_counter++;
				push @S, ["ifchange", "", $idx];
				"';$s1 my \$ch$idx=$exp; if(\$self->{'#change'}{$idx} ne \$ch$idx) { push \@out, $s2'" }:
			s/^if\b\s*(.*)//s?	do {
				my $exp = $self->expression($1);
				push @S, ["if", ""];
				"';$s1 if($exp) { push \@out, $s2'" }:
			s/^elseif\b\s*(.*)//s?	do {
				my $exp = $self->expression($1);
				die "{% elseif %} не в if" if !@S || $S[$#S][0] !~ /^(if|ifchange)$/;
				"'$s1 } elsif($exp) { push \@out, $s2'" }:
			s/^else\b//s?	do {
				die "используйте {% else %}, а не {% else ... %}" if length $_;
				die "{% else %} не в if" if !@S || $S[$#S][0] !~ /^(if|ifchange)$/;
				"'$s1 } else { push \@out, $s2'" }:
			# s/^for\s+($ID)\s+in\s+(.*)//s?	do {
				# my ($name, $exp) = ($1, $2);
				# push @S, ["for", $name];
				# $exp = $self->expression($exp);
				# "';$s1 my \$i=0; for (\@{$exp}) { \$i++; local \$self->{$name} = \$_; push \@out, $s2'" }:
			s/^for\b//s?	do {
				/\s*($ID)(?:\s*,\s*($ID))?\s+in\s+(.*)/ or die "используйте: {% for k[, v] in  %}";
				my ($k, $v, $exp) = ($1, $2, $3);
				push @S, ["for", $k];
				$exp = $self->expression($exp);
				$v = " local \$self->{$v} = \$A_${k}->{\$_};" if length $v;
				"';$s1 my \$I_$k = 0; my \$A_$k = $exp; for (ref \$A_$k eq 'HASH'? (sort keys %\$A_$k): \@\$A_$k) { \$I_$k++; local \$self->{$k} = \$_;$v push \@out, $s2'" }:
			s/^empty\b//s?	do {
				die "используйте {% empty %}, а не {% empty ... %}" if length $_;
				die "{% else %} не в for" if !@S || $S[$#S][0] !~ /^for$/;
				"';$s1 } if(\$I_$S[$#S][1] == 0) { push \@out, $s2'" }:
			s/^with\b//? do {
				my $with = $self->expression($_);
				push @S, ["with", /($ID)/];
				"';$s1 { $with; push \@out, $s2'" }:
			s/^set\b//? do {
				my ($id) = /^\s*($ID)\s*$/ or die " {% set <нет>id %}";
				push @S, ["set", $id];
				"';$s1 local \$self->{$id} = do { my \@out; push \@out, $s2'" }:
			s/^end(\w*)(?:\s+(\w+))?//? do {
				die "use {% end<type> [name] %}" if length $_;
				die "use {% end<type> %}" if !length $1;
				die "нет открывающего утверждения" unless my $lvl = pop @S;
				
				my ($type, $name) = @{$lvl};
				die "ожидается {% end$type %}, а указан {% end$1 %}" if $type ne $1;
				die "ожидается {% end$type $2 %}, а стоит {% end$type $name %}" if length $2 and $2 ne $name;
				
				given($type) {
					when ("block") { "';$s1 return \@out }; push \@out, \$self->$name, $s2'"  }
					when ("defer") { "';$s1 return join '', \@out }), '" }
					when ("if") { "';$s1 }; push \@out, $s2'" }
					when ("ifchange") { my $idx = $lvl->[2]; "';$s1 }; \$self->{'#change'}{$idx} = \$ch$idx; push \@out, $s2'" }
					when ("for") { "';$s1 }; push \@out, $s2'" }
					when ("with") { "';$s1 } push \@out, $s2'" }
					when ("set") { "'; join '', \@out $s1}; push \@out, $s2'" }
					default {
						die "не распознан тип {% end$type $name %}";
					}
				}
			}:
			s/^($ID)//? do {
				join("", "';$s1 R::Template::Subs::$1(", $self->expression($_), "); push \@out, $s2'");
			}:
			die "что-то неизвестное попало в блок: `{% $_ %}`"
		};

		
		"$before$ret$after"
	};
	
	$code =~ s/'/\\'/g;
	
	$code =~ s{
		(?<s1> ^[\t\ ]*)? (
	
		\{\{ \s* (?<print> .*? ) \s* \}\} |
		\{% \s* (?<block> .*? ) \s* %\} |
		\{\# (?<rem> .*? ) \#\}
		
		) (?<s2> [\t\ ]*\n)?
	}{
		$replace->();
	}msxgen;

	my $extends_class = defined($extends)? "BEGIN { our \@ISA = qw/".$self->retrive($extends)."/; }": "";
	my $sub_render = defined($extends)? "CONTENT": "RENDER";
	
	$code = "package $cls;$extends_class use common::sense; sub $sub_render { my (\$self) = \@_; my \@out; push \@out, '$code'; join '', \@out }; 1;";
	
	msg ":magenta empty", $path, "\n",
		$self->show_code_log(":green", $save_code), 
		$self->show_code_log(":reset", $code)
	if $self->{show_code};
	my $lpath = $file->ext($self->{ext})->path;
	
	$app->file("var/.mid/$lpath")->mkpath->write($code);
	
	{
		my $wd = $app->file("var/.mid")->withdir;
		local $INC[@INC] = ".";
		require $lpath;
	}
	
	$self->{classes}{$path} = $cls;
	$self->{classloadtime}{$cls} = Time::HiRes::time();
	
	$cls
}

# возвращает код со строками для логирования
sub show_code_log {
	my ($self, $color, $code) = @_;
	my $i = 1;
	map { (":red", sprintf("%02d ", $i++), $color, $_, "\n") } split /\n/, $code;
}

# возвращает канонический путь
sub path {
	my ($self, $path) = @_;
	$path =~ s!^/!!;
	my $f = $app->file($path);
	$path = $f->ext($self->{ext})->path if $f->ext eq "";
	$path
}

# возвращает файл
sub file {
	my ($self, $path) = @_;
	$app->file($app->project->path(@{$self->{inc}}), $app->framework->path . "/mid")->unique->sub($app->file($path)->ext($self->{ext})->path)->existing
}

# включает логирование
sub logon {
	my ($self) = @_;
	$self->show_code(1)
}

# включает логирование
sub logoff {
	my ($self) = @_;
	$self->show_code(0)
}


1;