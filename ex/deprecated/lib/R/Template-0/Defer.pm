package R::Template::Defer;
# модуль

use overload '""' => 'stringify';

use common::sense;
use R::App qw/$app/;

# конструктор
sub new {
	my $cls = shift;
	bless {@_}, ref $cls || $cls;
}

# возвращает строку 
sub stringify {
    my ($self) = @_;
	$self->{code}->($self->{context})
}

1;