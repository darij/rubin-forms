package R::Template::Filters;
# фильтры

use common::sense;
use R::App;
use R::Html;

our %NOESC;

*uc = *upper = \&CORE::uc;
*lc = *lower = \&CORE::lc;

*ucfirst = \&CORE::ucfirst;
*lcfirst = \&CORE::lcfirst;
*length = \&CORE::length;
*defined = \&CORE::defined;

sub json { $app->json->to(@_) }
sub size { my ($x) = @_; Is($x, "ArrayRef")? scalar @$x: Is($x, "HashRef")? scalar keys %$x: Is($x, "Str")? length($x): undef }

sub default { $_[0] // $_[1] }

sub keys { [ keys %{$_[0]} ] }
sub values { [ values %{$_[0]} ] }
sub zip  (@) { [ R::zip @_ ] }

# объединяет строки в массиве
sub join {
	my ($arr, $sep) = @_;
	join $sep // ", ", @$arr
}

# включает файл
$NOESC{source} = 1;
sub source {
	my ($path) = @_;
	$path =~ m!/! && return $app->file($path)->read;
	$app->sitemap->root->{root}->sub($path)->existing->read;
}

# получает пары ключ-значение и возвращает a="b" через пробел - т.е. атрибуты элемента
$NOESC{attrs} = 1;
sub attrs {
	my ($attrs) = @_;
	$attrs = [map { $_ => $attrs->{$_} } sort keys %$attrs] if ref $attrs eq "HASH";
	map2 { !defined $b? " $a": join("", " $a=\"", $app->html->escape($b), '"') } @$attrs;
}


#@@category html-списки

# преобразует массив в список. Элемент списка должен быть списком из ссылки и описания
$NOESC{ul} = 1;
sub ul {
	my ($ul) = @_;
	return "<ul>", (map { 
		sprintf "<li><a href=\"%s\">%s</a>", R::Html::_escapes(@$_) 
	} @$ul), "</ul>"
}

1;