package R::Template::Asp;
# транслятор c asp-подобного шаблона в классы perl
# создаёт класс
# тесты <% TEST: %> и админка <% ADMIN: %> пропускаются

use common::sense;
use R::App;
use R::Html;
use R::Template::AspBase;
use Time::HiRes qw//;


has qw/inc base base_class ext/;

# конструктор
sub new {
	my $cls = shift;
	bless {
		inc => [$app->project->files("act")->parts],
		base => "_Asp_",
		base_class => "R::Template::AspBase",
		ext => "asp",
		classes => {},			# path => cls
		classloadtime => {},	# cls => time
		#check => 0,			# проверять изменение времени на диске
		@_
	}, ref $cls || $cls;
}

# выполняет экшн
sub render {
	my ($self, $path, $data) = @_;
	
	my $cls = $self->retrive($path);
	
	local $data->{__asp__} = $self;

	bless($data, $cls)->render
}

# проверяет наличие класса и, если нужно, то загружает его
sub retrive {
	my ($self, $path) = @_;
	
	my $cls = $self->{classes}{$path};

	return $self->load($path) if !defined $cls;
	
	# # если изменился хоть один из сторонних модулей
	# $self->{check} && $app->process->autoreload;
	
	# return $self->load($path) if $self->{check} && $self->file($path)->mtime > $self->{classloadtime}{$cls};
	
	$cls
}

# загрузить компил.
sub load {
	my ($self, $path) = @_;

	my $file = $self->file($path);
	
	my $save_code = my $code = $file->read;
	
	my $cls = $self->{base} . join "::", map { $app->magnitudeLiteral->wordname($_) } split m!/!, $path;
	$cls =~ s!/!::!g;
	$cls = "$cls";

	my @S;
	my $extends;
	
	my $replace = sub {
		local $_ = $+{ex};

		s/\\'/'/g;
		
		s/^:=//?	"', do { $_ }, '":
		s/^=//?		"', R::Html::_escapes(do { $_ }), '":
		s/^[\t ]*block\b//?	do {
			die "use <% block name %>" if !s!^[\t ]+([a-z_]\w*)!!i;
			push @S, $1;
			"'; sub $1 { my (\$self) = \@_; my \@out; $_; push \@out, '" }:
		s/^[\t ]*overlay\b//?	do {
			die "use <% overlay name %>" if !s!^[\t ]+([a-z_]\w*)!!i;
			push @S, "";
			"'; sub $1 { my (\$self) = \@_; my \@out; $_; push \@out, '" }:
		s/^[\t ]*end\b//?	do { die "use <% end %>" if !/^\s*$/;
			die "<% end %> without <% block %>" unless defined(my $block = pop @S);
			$block = "\$self->$block, " if length $block;
			"'; return \@out }; push \@out, $block'" }:
		s/^[\t ]*extends\b//? do { die "use <% extends \"path\" %>" if !m!^[\t ]*"([^\"]*)"[\t ]*$!; 
			$extends = "/$1"; () }:
		s/^[\t ]*include\b//? do { die "use <% include \"path\" ... %>" if !m!^[\t ]*"([^\"]*)"(.*)!; 
			"', do { \$self->{__asp__}->retrive(\"$1\")->new(%\$self, $2)->render }, '" }:
		"'; $_; push \@out, '";
	};
	
	$code =~ s/'/\\'/g;
	
	$code =~ s{<% (?<ex> .*? ) (?: %> | \z) | ^[\t ]* % (?<ex> [^\n]*) }{
		$replace->();
	}msgxe;
	
	# A->render <- B->render <- C->render
	# A: <div><%= @_ %></div>
	# B: <span><%= @_ %></span>
	# C: <a>123</a>
	
	my $extends_class = defined($extends)? $self->retrive($extends): $self->{base_class};
	
	$code = "package $cls; BEGIN { our \@ISA = qw/$extends_class/; } use common::sense; use R::App; sub render { my (\$self, \$content) = \@_; my \@out; push \@out, '$code'; \$self->SUPER::render(\\\@out) }; 1;";
	
	msg $code if $self->{show_code};
	my $lpath = join "", substr($path, 1), ".", $self->{ext};
	
	$app->file("var/.asp/$lpath")->mkpath->write($code);
	
	{
		my $wd = $app->file("var/.asp")->withdir;
		local $INC[@INC]=".";
		require $lpath;
	}
	
	$self->{classes}{$path} = $cls;
	$self->{classloadtime}{$cls} = Time::HiRes::time();
	
	$cls
}

# возвращает файл
sub file {
	my ($self, $path) = @_;
	$app->file(@{$self->{inc}})->sub($path)->ext($self->{ext})->existing
}

1;