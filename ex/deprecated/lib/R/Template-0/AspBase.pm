package R::Template::AspBase;
# базовый класс экшенов

use common::sense;
use R::App qw//;

# конструктор
sub new {
	my $cls = shift;
	bless {@_}, ref $cls || $cls;
}

# базовый рендер
sub render {
	@{ $_[1] }
}

1;