package R::Sitemap::Act;
# роутер в контроллере:
# подгружает при первом serve свой контроллер, а затем - выполняет его

use base R::Sitemap::Route;

use common::sense;
use R::App;


# выполняет роутер и возвращает результат в любом из форматов RSGI
sub serve {
	my ($self, $request) = @_;

	my $sitemap = delete $self->{sitemap};
	
	# заменяем роуты
	require $self->{act};
	my $pkg = $self->{act};
	$pkg =~ s/\.pm$//;
	$pkg =~ s!/!::!g;
	
	$sitemap->attach($pkg);
	
	my $front = \@{"${pkg}::__Yogi__front__"};
	my $error = \@{"${pkg}::__Yogi__error__"};
	my $routers = \@{"${pkg}::__Yogi__route__"};
	die "нет роутов в $pkg" if !@$routers && !@$front && !@$error;
	
	my %error;
	for(my $i=0; $i<@$error; $i+=3) {
		my ($status, $mask, $response) = @$error[$i..$i+2];
		push @{$error{$status}}, $mask, $response;
	}
	
	$sitemap->{front}->route($front, $self);
	$sitemap->route($routers, $self);
	$sitemap->statuses(\%error, $self);
	
	
	# очищаем
	@{"${pkg}::__Yogi__front__"} = ();
	@{"${pkg}::__Yogi__error__"} = ();
	@{"${pkg}::_Yogi_route"} = ();
	
	# ещё раз вызываем
	$sitemap->serve($request);
}

1;