package R::Sitemap::Data;
# роутер просто возвращает какой-то response

use base R::Sitemap::Route;

use common::sense;
use R::App;

# выполняет роутер и возвращает результат в любом из форматов RSGI
sub serve {
	my ($self) = @_;
	$self->{serve}
}

1;