package R::Sitemap::Root;
# роутер в контроллере:
# подгружает при первом serve свой контроллер, а затем - выполняет его

use base R::Sitemap::Route;

use common::sense;
use R::App;

has qw/map/;

# конструктор
sub new {
	my $cls = shift;
	$cls->SUPER::new(map => {}, @_);
}

# выполняет роутер и возвращает результат в любом из форматов RSGI
sub serve {
	my ($self, $request) = @_;
	
	my $path = $request->path;
	
	my $code = $self->{map}{$path};
	return [200, ["Content-Type" => $app->mime->Type($app->file($path)->ext, charset => "utf-8")], $code] if defined $code;
	
	my $f = $self->{root}->sub($path)->grep("-f");
	$f->length? $f: [404]
}


1;