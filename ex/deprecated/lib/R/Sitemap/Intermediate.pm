package R::Sitemap::Intermediate;
# промеждуточный роут, который вызывает распознанный роут

use base R::Sitemap::Route;

use common::sense;
use R::App;

# intermediate - повторить
sub serve {
	my ($self, $request) = @_;
	my $to = $self->{serve}->($request);
	$self->{sitemap}->serve($to, $request);
}

1;