package R::Sitemap::Route;
# базовый роутер

use common::sense;
use R::App;

# конструктор
sub new {
	my $cls = shift;
	my $self = bless {@_}, ref $cls || $cls;	
	$self
}

# возвращает контроллер в котором находится роутер
sub controller {
	my ($self) = @_;
	$self->{from_act} // $self->{act}
}

# возвращает тип роутера
sub type {
	my ($self) = @_;
	my ($type) = ref($self) =~ /(\w+)$/;
	lc $type
}

# выполняет роутер и возвращает результат в любом из форматов RSGI
sub serve {
	my ($self, $request) = @_;
	$self->{serve}->($request)
}

1;