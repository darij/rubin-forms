package R::Action;
# базовый класс экшенов

use common::sense;
use R::App qw//;

R::has qw/request/;

# конструктор
sub new {
	my $cls = shift;
	bless {@_}, ref $cls || $cls;
}

# базовый рендер
sub render {
	@{ $_[1] }
}


# распределяет запрос по методу
sub dispatch {
	my ($self) = @_;
	my $via = $self->{request}{via};
	
	return $self->get if $via eq "GET";
	return $self->ajax if $via eq "POST" and $self->{request}->header("X-Requested-With") eq "XMLHttpRequest";
	return $self->post if $via eq "POST";
	return $self->put if $via eq "PUT";
	return $self->del if $via eq "DELETE";
	return $self->patch if $via eq "PATCH";
	return $self->head if $via eq "HEAD";
	return $self->options if $via eq "OPTIONS";
	return [400];
}

# обрабатывает get-запрос
sub get {
	my ($self) = @_;
	join "", $self->render;
}

# вызывает соответствующий метод-обработчик формы
sub ajax {
	my ($self) = @_;
	return [] unless my $form = $self->{request}->header("X-Form");
	my $method = "${form}Ajax";
	$self->$method;
}

# остальные методы контроллера
sub post { [405] }
sub put { [405] }
sub del { [405] }
sub patch { [405] }
sub head { [405] }
sub options { [405] }

1;