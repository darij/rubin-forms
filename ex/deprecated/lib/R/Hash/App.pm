package R::Hash::App;
# расширяет app

use common::sense;
use R::App;

# конструктор
sub new {
	my $cls = shift;
	bless {@_}, ref $cls || $cls;
}

# чтобы деструктор не вызывал AUTOLOAD
sub DESTROY {}

# создаёт метод, если есть ключ. Так же подгружает модуль, если это строка
sub AUTOLOAD {
	my ($self) = @_;
	
	my ($prop) = our $AUTOLOAD =~ /([^:]+)$/;
	
	die "нет $prop" if !exists $self->{$prop};
	
	my $x = $self->{$prop};
	if(!ref $x) {
		require $x;
		$self->{$prop} = $x->new
	}
	elsif(ref $x eq "ARRAY") {
		require $x->[0];
		$self->{$prop} = $x->[0]->new(@$x[1..$#$x])
	}
	
	eval "sub $prop { my \$self=shift; \@_? \$self->{$prop}->new(\@_): $self->{$prop} }";
	die $@ if $@;
	
	goto &$prop;
}

1;