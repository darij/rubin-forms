package R::Celestial;
# сервер виджетов

use base R::Fulminant;

use common::sense;
use R::App;

# свойства
has qw/dev/;



# конструктор
sub new {
	my $cls = shift;
	
	$cls->SUPER::new(
		dev => $app->ini->{dev},		# если dev - то проверять время изменения
		#max_age => 60*60*24*365*20,	# 20 лет
		@_
	);
}

# запуск портала
sub run {
	my ($self) = @_;
	
	$app->hotswap->scan_html(qw/html widget.common widget.library/) if $self->{dev};
	
	my $sites = $self->{sites};
	for my $name (%$sites) {
		my $site = $sites->{$name};
		$sites->{$name} = $app->router(routes => $site->{routes}, param => $site->{param});
	}
	
	$self->SUPER::run
}

# рыцарь - обработчик запроса
sub ritter {
	my ($self, $request) = @_;

	my $path = $request->path;
	die [503, "обнаружено .. в пути"] if $path =~ m!/..(/|$)!n;
	
	my $host = $request->header->Host;
	my $router = $self->{sites}{$host};
	return [200, "<center>host: $host not found<hr>celestial</center>"] if !$router;
	
	# получаем лайоуты и ошибки, а если повезёт - то и виджет
	my ($widget, $attr, $param) = $router->get($path);
	local $request->{_attr} = $attr;
	local $request->{attr};
	$request->{access} = $param;
	
	if($widget) {
	
		if($self->{dev}) {
			$app->hotswap->startscan, $self->{FLAG_SCAN} = 1 if !$self->{FLAG_SCAN};
			$app->hotswap->scan if $self->{FLAG_SCAN};
		}
		
		return $self->_runs($widget);
	}
	
	# ajax
	if($path =~ m!^/ajax/([^/]+)/([^/]+)/\z!) {
		my ($widget, $method) = ($1, $2);
		my $class = $app->widget->$widget;
		return [404, "404 - нет виджета $widget"] if !$class;
		return [404, "404 - нет точки монтирования $widget.$method"] unless defined $method and $method ~~ [ $class->mount ];
		return $class->new->$method;
	}
	
	# статика
	if($path =~ m!\.(\w+)\z!) {
		my $f = $app->file($path =~ s!^/asset/!!? ("widget.common/$path", "widget.library/$path"): "html/$path")->existing;
		return [404, "нет файла $path"] if !$f->length;
		return $f;
	}
	
	return [404, "404 - нет $path"];
	
	
}

# находит и преобразует ответ
sub middleware {
	my ($self, $request, $response) = @_;
	
	$app->hotswap->startscan if $self->{dev} && $request->path =~ /\/$/;
	
	my $status = $response->status;
	my $widget = $request->{access}->{error}->{$status};
	if(!$widget) { $widget = $request->{access}->{error}->{substr $status, 0, 1}; }
	
	if(defined $widget) {
		my $html = $self->_runs($widget);
		$response->data($html);
	}
	
	$response
}

# отображает страницу
sub _runs {
	my ($self, $widget) = @_;
	
	my $request = $Coro::current->{request};
	
	# ставим свой HEAD
	local $request->{HEAD} = {};
	
	my $param = $request->param->All;
	my $page = $app->widget->$widget(%$param)->show;
	
	# без лайоутов
	if($request->header->Accept !~ m!\btext/html\b!) {
		return $page;
	}
	
	my $layouts = $request->{access}->{layout};
	
	for my $layout (@$layouts) {
		local $param->{CONTEXT} = $page;
		$page = $app->widget->$layout(%$param)->show;
	}

	$page
}

1;