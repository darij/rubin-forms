package R::Plazma;
# построитель грамматических анализаторов

use common::sense;
use R::App;

# конструктор
sub new {
	my ($cls) = @_;
	bless {
		rules => {},	# правила
		lex => [],		# лексемы
		lexx => qr//,	# лексический анализатор
		lineno => 1,	# номер строки
		space => [],	# токены
		map => [],		# маппинг
	}, ref $cls || $cls;
}

# стандартная функция для лексемы
sub _lex {
	my ($self, $rule) = @_;
	$self
}

# добавляет лексемы
sub lex {
	my $self = shift;
	
	for(my $i=0; $i<@_; $i+=2) {
		my ($name, $lex) = @_[$i..$i+1];
		my $sub = ;
		push @{$self->{lex}{$name}}, $lex, $sub;
	}
	
	$self
}

# добавляет правила
sub rule {
	my $self = shift;
	
	for(my $i=0; $i<@_; $i+=3) {
		my ($name, $rule, $sub) = @_[$i..$i+2];
		
		die "$name - имя лексемы, его нельзя использовать как имя правила" if $self->{lex}{$name};
		
		$rule = [ split /\s+/, $rule ];
		
		push @{$self->{rules}{$name}}, $rule, $sub;
	}
	
	$self
}


# компиллирует указанный текст
sub compile {
	my ($self, $text) = @_;
	
	# формируем лексемы из правил
	for my $rule (values %{$self->{rules}}) {
		for(my $i=0; $i<$rule; $i++) {
			
		}
	}
	
	
	# лексический анализ
	my $lex = $self->{lex};
	while($text =~ qr/$lex/go) {
		push @{$self->{space}}, $^R;
	}
	
	# грамматический анализ
	
	
	
	$self
}

# возвращает ошибку синтаксиса или undef
sub error {
	my ($self) = @_;
	$self
}

# возвращает полученный код
sub code {
	my ($self) = @_;
	
}

1;