package R::Designers::Araund;


# добавляет обработчик при завершении
sub after (@) {
	unshift @_, 1;
	goto &_araund;
}

# добавляет обработчик перед
sub before (@) {
	unshift @_, -1;
	goto &_araund;
}

# обработчик вместо
sub araund (@) {
	unshift @_, 0;
	goto &_araund;
}


# оборачивает указанные функции
sub _araund {
	my $type = shift;
	my $pkg = caller;
	my $araund = pop;
	
	die "не указана функция у " . ($type==1? "after": $type==0? "araund": $type==-1? "before": "?") if ref $araund ne "CODE";
	
	for my $x1 (@_) {
		my $x = $x1;
		$x = "${pkg}::$x" if $x !~ /::/;
		
		my @subs;
		if($x =~ /\*/) {
			
		} else {
			push @subs, $x if *{$x}{CODE};
		}
		
		die "$type: нет $x" if !@subs;
		
		for my $sub (@subs) {

			my $code = *{$sub}{CODE};
			die "$type: не обнаружена $sub" if !$code;
			
			my $prototype = prototype $code;				
			my $closure = (sub {
				my ($code, $araund) = @_; 
				if($type == -1) {
					sub { ($araund->(@_), goto &$code) }
				}
				elsif($type == 0) {
					sub { local $R::sub = $code; $araund->(@_) }
				}
				elsif($type == 1) {
					sub {
						if(!defined wantarray) {
							$code->(@_);
							{
								local @ret = ();
								$araund->(@_);
							};
							return;
						}
						elsif(wantarray) {
							local @ret = $code->(@_);
							() = $araund->(@_);
							@ret
						} else {
							local @ret = scalar $code->(@_);
							scalar $araund->(@_);
							$ret[0];
						}
					}
				}
			})->($code, $araund);

			Scalar::Util::set_prototype(\&$closure, $prototype) if defined $prototype;
			
			my $subname = Sub::Identify::sub_name($code);
			Sub::Name::subname($subname, $closure) if defined $subname;
			
			*$sub = $closure;
		}
	}
	
	return;
}

1;