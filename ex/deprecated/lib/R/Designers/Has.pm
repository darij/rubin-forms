package R::Designers::Has;
# расширение для App::has

use common::sense;
require Scalar::Util;
require UNIVERSAL;
require Guard;

our %HAS;		# хэш с опциями свойств в классах: %HAS = has => cls => required=>{...}...
our %SECRET;	# адрес_в_памяти_объекта => { имя_свойства => значение }
our %ROLE = (	# роли
	getset => \&getset,
);


# чекает соответствие типу и возвращает значение
sub ISA_CHECK {
	my ($isa, $cls, $name, $val) = @_;
	local $_ = $val;
	
	my $res = $TYPE{$isa}();
	#print STDERR "ISA_CHECK: ${cls}->$name ---> $res\n";
	die "has isa: no $isa for ${cls}->${name} = $val" if !$res;
	$val
}

# удаляет секретные данные и вызывает очистители
sub DESTROY {
	my ($self) = @_;
	
	eval { $self->DESTROY_ORIGINAL } if $self->can("DESTROY_ORIGINAL");
	
	my $has = $HAS{ref $self};
	my $clearers = $has->{clearers};
	
	for my $clearer (@$clearers) {
		eval { $self->$clearer };
	}

	delete $SECRET{int $self};
}


# расширяет деструктор
sub extend_destroy {
	my ($cls) = @_;
	
	my $has = $HAS{$cls};
	if(%{$has->{secret}} || @{$has->{clearers}}) {
		my $destroy = *{"${cls}::DESTROY"}{CODE}; #$cls->can("new");
		#die "${cls}->DESTROY has not `has` destructor" if $destroy and $destroy != \&DESTROY;
		*{"${cls}::DESTROY_ORIGINAL"} = $destroy if $destroy && $destroy != \&DESTROY;
		*{"${cls}::DESTROY"} = \&DESTROY if $cls->can("DESTROY") != \&DESTROY;
	}
}

# расширяет класс
sub extend {
	my ($cls) = @_;
	
	# конструктор
	my $new = *{"${cls}::new"}{CODE}; #$cls->can("new");
	
	
	die "${cls}->new has not `has` constructor" if $new and $new != \&new and !${"${cls}::disable_has_new"};
	
	*{"${cls}::new"} = \&new if !$new;
	
	# добавляем конфиги из ISA
	die "has: конфиг для класса $cls при наследовании уже есть!" if exists $HAS{$cls};
	
	my $my = $HAS{$cls} = {};
	
	for my $class (@{"${cls}::ISA"}) {
		if(my $has = $HAS{$class}) {
			my $opt = $has->{has};
			my @plain = @{$has->{plain}};
			push @plain, grep { !$_ ~~ @plain } keys %$opt;
			_has(0, $cls, $_ => $opt->{$_}) for @plain;
		}
	}

	extend_destroy($cls);
	
	$my
}

# создаёт функции-свойства в указанном классе
sub new {
	my $cls = shift;
	$cls = ref $cls || $cls;
	my $self = bless {}, $cls;
	
	# has = атрибут => опции
	my $has = $HAS{$cls};
	
	# пробуем унаследовать
	$has = extend($cls) if !defined $has;
	
	# все опции
	my $opt = $has->{has};
	
	# устанавливаемые аргументы
	my %arg = @_;
	my @arg = keys %arg;
	
	my @err;
	
	# обязательные опции
	my %required = %{$has->{required}};
	delete @required{@arg};
	push @err, join(", ", keys %required) . " is required" if %required;
		
	# левые аргументы, ro, no и late
	my @no_prop;
	my @late;
	
	for my $k (@arg) {
		my $option = $opt->{$k};
		push(@no_prop, $k), next if !$option;
		push(@late, $k), next if $option->{late};
	}
	
	
	push @err, join(", ", @no_prop) . " has no property" if @no_prop;
	# опции нельзя устанавливать в констукторе
	push @err, join(", ", @late) . " is late" if @late;
	
	die join ", ", @err if @err;
	
	# идём по списку опций, is=w, default и/или builder
	my $plain = $has->{plain};
	for my $k (@$plain) {
		my $option = $opt->{$k};
	
		# получаем устанавливаемое значение
		my $x;
		
		if(exists $arg{$k}) {
			$x = $arg{$k};
			# чекаем, если надо
			ISA_CHECK($option->{isa}, $cls, $k, $x) if exists $option->{isa};
		} elsif(exists $option->{default}) {
			$x = $option->{default};
			$x = $x->($self) if ref $x eq "CODE";
			
		}
		else {
			goto BUILDER;
		}

		# устанавливаем значение
		if($option->{secret}) {
			$SECRET{int $self}{$k} = $x;
		} else {
			$self->{$k} = $x;
		}
		
		# вызываем билдер
		BUILDER:
		my $builder = $option->{builder};
		$self->$builder($x) if defined $builder;
	}

	$self
}


our @HAS_OPTION_NAMES = qw/accessor lazy fetch get set out put default required late role is isa secret builder clearer/;


sub has (@) {
    my ($cls) = caller(0);
	unshift @_, 1, $cls;
	goto &_has;
}

sub _has {
	my $create_sub = shift;
	my $cls = shift;
	my $has = $HAS{$cls};
	$has = extend($cls) if !defined $has;
	
	my @props;
	
	push @_, {} if !ref $_[$#_] and @_;
	
    for my $prop (@_) {
		die "undefined prop" if !defined $prop;
		push(@props, $prop), next if !ref $prop;
		
		die "используйте: has prop1... => {options...}" if !@props;
		
		my %options = ref($prop) eq "CODE"? (lazy => $prop): ref($prop) eq "HASH"? %$prop: die "используйте: has prop1... => {options...} или has prop1... => sub { lazy }";

		#__msg1 $cls, \%options;
		
		my $opt = {%options};
		
		my $lazy1 = $options{lazy};
		my $get1 = $options{get};
		my $set1 = $options{set};
		my $role1 = $options{role} // "getset";
		my $isa1 = $options{isa};
		my $is1 = $options{is} // "rw";
		my $required1 = $options{required};
		my $late1 = $options{late} // !$required1 && ($is1 =~ /ro|no/);
		my $secret1 = $options{secret};
		my $exists_default = exists $options{default};
		#my $default1 = $options{default};
		my $accessor1 = $options{accessor};
		my $builder1 = $options{builder};
		my $clearer1 = $options{clearer};
		
		$opt->{late} = $late1;
		
		delete @options{@HAS_OPTION_NAMES};
		
		die "нераспознанные опции: " . join(", ", keys %options) . "\nОжидаются: " . join ", ", @HAS_OPTION_NAMES if keys %options;

		die "используйте: has ... => { is => \"ro|wo|rw|no\" }, а не $is1" if $is1 !~ /^(ro|rw|wo|no)$/;
		
		die "is=>$is1 не может использоваться с set" if $is1 =~ /ro|no/ && $set1;
		die "is=>$is1 не может использоваться с get" if $is1 =~ /wo|no/ && $get1;
		die "is=>$is1 не может использоваться с lazy" if $is1 =~ /wo|no/ && $lazy1;
		
		$TYPE{$isa1} = codeunion($isa1) if defined $isa1;
		
		for my $name (@props) {
			my $accessor = $accessor1 // $name;

			die "слот ${cls}::$name занят" if $create_sub && *{"${cls}::$accessor"}{CODE};

			$has->{has}{$name} = $opt;
			#$has->{default}{$name} = $default1 if $exists_default;
			if($required1) { $has->{required}{$name} = 1; } else { delete $has->{required}{$name} }
			if($late1) { $has->{late}{$name} = 1; } else { delete $has->{late}{$name} }
			if($secret1) { $has->{secret}{$name} = 1; } else { delete $has->{secret}{$name} }
			
			#; # if $is1 =~ /rw|wo/
			my $plain = $has->{plain} //= [];
			@$plain = grep { $_ ne $name } @$plain;
			push @$plain, $name if !$late1 || $exists_default || $builder1;
			

			for my $key (qw/set get lazy builder clearer/) {
				my $v = $opt->{$key};
				if(ref $v eq "CODE") {
					$opt->{$key} = my $k = "${cls}::${name}__" . uc $key;
					*{$k} = $v;
				}
				elsif(defined $v) {
					$opt->{$key} = "${cls}::$v" if $v !~ /::/;
				}
			}
			
			if(defined $opt->{clearer}) {
				#@{$has->{clearers}} = grep { $_ ne $name } @{$has->{clearers}};
				unshift @{$has->{clearers}}, $opt->{clearer};
			}
			
			if($create_sub) {
				# устанавливается то, что возвращает сеттер
				# if(@_>0) { $_[0]->{name} = set(@_); $_[0] } else { local $_=$_[0]->{name}; get(@_) }
				my $role = ($ROLE{$role1} // die "нет роли $role1")->($cls, $name, $opt);
			
				my $code = "package $cls; sub $accessor { $role }";
				#print "---> $code\n";
				eval $code;
				die if $@;
				
			}
		}
		@props = ();
    }
	
	extend_destroy($cls);
}

# стандартная роль
sub getset {
	my ($cls, $name, $opt) = @_;
	
	my ($set1, $get1, $lazy1, $fetch1, $out1, $put1, $is1, $secret1, $isa1) = @$opt{qw/set get lazy fetch out put is secret isa/};
	
	my $obj = "\$_[0]";
	my $getset = $secret1? "\$R::Has::SECRET{int $obj}{$name}":	"${obj}->{$name}";
	my $set = "$getset = \$_[1]";
	my $get = $getset;
	my $isa = "";
	my $lazy = "";
	
	$isa = "R::Has::ISA_CHECK('$isa1', '$cls', '$name', \$_[1]); " if defined $isa1;
	$lazy = "$getset = $lazy1(\@_) unless exists $getset; " if $lazy1;
	
	if(defined $set1) {
		$set = "$set1(\@_)";
		$set = "$getset = $set" if !defined $put1 or $put1;
		$set = "${lazy}local \$_ = $getset; $set" if !defined $out1 or $out1;
	}
	
	if(defined $get1) {
		$get = "$get1(\@_)";
		$get = "local \$_ = $getset; $get" if !defined $fetch1 or $fetch1;
	}
	
	$set = "die('$name is read only')" if $is1 eq "ro";
	$get = "die('$name is write only')" if $is1 eq "wo";
	$set = $get = "die('$name is never used')" if $is1 eq "no";
	
	my $role = "if(\@_==1) { $lazy$get } elsif(\@_==2) { $isa$set; \$_[0] } else { die 'лишние аргументы' }";
	
	#$role = "*{${cls}::$name} = sub { $role }; $getset = ${cls}::${name}__LAZY($obj); goto &{${cls}::$name}" if $lazy1;
	
	$role
}


1;