package R::Designers::Exporter;
# 

use common::sense;
use R::App;


# экспортирует (заменяет модулю import)
sub export {
	my ($pkg, $export, $default) = @_;
	
	die "в пакете $pkg уже есть import" if *{"${pkg}::import"}{CODE};
	die "в пакете $pkg уже есть unimport" if *{"${pkg}::unimport"}{CODE};
	
	my $pax = {};
	for(my $i=0; $i<@$export; $i++) {
		my $name = $export->[$i];
		if($name =~ /^-/) {
			#$pax->{group}{}
		}
		elsif($name =~ /^\@/) {
			$pax->{array}{$`} = 1;
		}
	}
	
	(sub {
		*{"${pkg}::import"} = sub {
			unshift @_, $export, $default;
			goto &_import;
		};
	})->($export, $default);
}

# импорт
sub _import {
	my ($export, $export_default, $pkg, @args) = @_;
	
	# for my $name ( @_ ? @_ : (keys %EXPORT_CODE, keys %EXPORT_VAR) ) {
		# if(my $v = $EXPORT_CODE{$name}) {
			# *{"${pkg}::$name"} = $v;
		# } elsif(my $v = $EXPORT_VAR{$name}) {
			# *{"${pkg}::$v"} = \${$v};
		# }
		# else {
			# die "нет такого имени `$name`";
		# }
	# }

	$pkg
}


1;