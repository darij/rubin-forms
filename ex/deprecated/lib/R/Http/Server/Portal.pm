package R::Http::Server::Portal;
# http-сервер на Coro

use base R::Http::Server;

use common::sense;
use R::App;

# подключаем coro
$app->coro;

# конструктор
sub new {
	my ($cls) = @_;
	
	$cls->SUPER::new(
		port => $app->ini->{site}{port},			# порт
		ritters => $app->ini->{site}{ritters} // 1,	# количество процессов
		guards => $app->ini->{site}{guards},		# количество волокон
		test => $app->ini->{site}{test},			# тестовый режим
	)
}

# запускается когда сокет уже создан, а воркеры ещё не порождены
sub setup {
	my $self = shift;
	
	# общее время
	$app->bench->time;
	
	$app->bench->time;
	$app->connect;
	
	msg ":space", "создание сокета $self->{port}:", ":bold black", $self->{bench_make};
	msg ":space", "коннект к базе:", ":bold black", $app->bench->log;
	
	
	# загружаем модели, для того, чтобы появились все связи
	$app->bench->time;
	$app->meta->load_all_models;
	msg ":space", "загрузка моделей:", $app->bench->log;
		
	
	# всегда выдаём одну и ту же страницу
	$app->bench->time;
	
	my $page = $app->file( $app->framework->root . "/node/index.html" )->read;
	$page =~ s!<title></title>!<title>$app->ini->{site}{name}</title>!;
	$self->on(qr// => sub { $page });
	
	# позволяет подключать страницы
	$self->message(require => sub {
		msg1 ":red", "require:", ":reset", \@_;
	});
	
	
	msg ":space", "добавление диспетчеров:", $app->bench->log;
	
	msg ":space", "общее время:", $app->bench->log;
	msg ":space", "от старта:", $app->bench->start_time->log;
	
	# создаём сервер кэша
	
	# запускаем шедулер
	# $app->shiva->start;
	
	$app->connect->close if $self->role ne "loop" and $app->connect->{dbh};
	
}


# инициализирует воркер
sub init {
	my ($self) = @_;

	# подключаем базу данных
	$app->connect->connect if $self->role ne "loop";
	
	$self
}

# вызывается перед обработкой запроса
sub before {
	my $self = shift;
	
	# my $q = $app->q;
	# my $path = $q->uri->path;
	# my $ip = $q->ip;
	
	# $app->cache->inc("vi-$path", "visit");			# подсчитываем посещения
	# $app->cache->inc("vi-$ip-$path", "visit-ip");	# подсчитываем уникальные посещения
	
	$app->connect->reconnect;
	
	$self
}

# вызывается после обработки запроса
sub after {
	my ($self) = @_;

	$self
}




1;