package R::Queue;
# очереди

use common::sense;
use R::App;

$app->meta->fieldset("_queue")->setup;

# конструктор
sub new {
	my ($cls) = @_;

	bless {}, ref $cls || $cls;
}

# регистрирует канал и получателя
sub channel {
	my ($self, $name) = @_;
	
	$app->model->_queue(undef)->channel($name);
	
	$self
}

# добавляет сообщение в канал
sub push {
	my $self = shift;
	
	$app->model->_queue(undef)->push(@_);
	
	$self
}

# получает из очереди очередное сообщение по имени
sub pull {
	my $self = shift;
	
	$app->model->_queue(undef)->pull(@_);
}


package R::Row::_queue {
# очередь заданий

	use base "R::Model::Row";

	use common::sense;
	use R::App;

	# раз в час удаляем лишние задания
	$app->shiva->on("queue-eraser" => "1 * * * *" => sub {
		$app->model->_queue->find(_queuename=>undef)->erase;
	});

	# вызывается для создания структуры базы
	sub setup {
		my ($fields) = @_;
		
		$app->meta->fieldset("_queuename")->
			col(name => 'varchar(255)')->min_length(1)->remark("наименование задания")->
			unique("name")->	
		meta(
			remark => "наименования заданий",
		);
		
		$fields->
		
		ref('_queuename')->remark("канал")->
		col(beneficiary=>'int unsigned')->default(0)->remark("уникальный номер получателя")->
		col(running => 'unixtime')->default(0)->remark("задание взято на обработку")->
		col(args => 'varchar(255)')->remark("аргументы задания")->
		
		meta(
			remark => "очередь заданий",
		);

		
	}

	# тестовые данные
	sub testdata {
	}


	# зарегистрировать канал
	sub channel {
		my ($self, $name) = @_;
		
		die "задание уже есть" if $app->model->_queuename->find(name => $name)->exists;
		
		$app->model->_queuename(name=>$name)->save;
		
		$self
	}

	# добавить задание
	sub push {
		my ($self, $name, $args) = @_;
		
		my $_queuename = $app->model->_queuename->find(name => $name)->exists;
		
		die "не зарегистрирован канал `$name`" if !$_queuename;
		
		my @args = (_queuename=>$_queuename, args=>$args);
		
		# организовать пулл заданий
		$app->model->_queue->find(_queuename=>undef)->limit(1)->update(@args);
		if( $app->connect->last_count == 0 ) {
			$app->model->_queue(@args)->save;
		}
		
		$self
	}

	# получить задание
	sub pull {
		my ($self, $name, $beneficiary) = @_;
		
		die "используйте: pull(канал, уникальный_номер_получателя)" if !Num($beneficiary) || $beneficiary<=0;
		
		my $_queuename = $app->model->_queuename->find(name => $name)->exists;
		
		die "не зарегистрирован канал `$name`" if !$_queuename;
		
		my $time = time();
		
		$app->model->_queue->find(_queuename => $_queuename, beneficiary=>0)->limit(1)->update(beneficiary=>$beneficiary, running=>$time);
		
		my $task;
		if($app->connect->last_count) {
			$task = $app->model->_queue->find(_queuename => $_queuename, beneficiary=>$beneficiary, running=>$time)->exists;
		}
		
		$task
	}

	# отметить, что задание выполнено
	sub pop {
		my ($self) = @_;
		$self->_queuename(undef)->save;
	}

}

1;