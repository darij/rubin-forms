
var fs = require('fs')
var glob = require('glob')
var colors = require('colors')
var babel = require("babel-core")
var coffeescript = require("/lib/coffeescript")

function norm(f) { return f.replace(/\\/g, "/") }

var src = norm(process.argv[2]).split(",")
var base = src[0].replace(/\/?$/, "/")
var dst = norm(process.argv[3])
var all = process.argv[4]
all = all === undefined? 0: parseInt(all)

var compile_all = all == 1
var watchify = all > 1

if(isNaN(all)) {
	for(var i=0; i<src.length; i++)
		src[i] += "/"+process.argv[4]
	
	dst += "/"+process.argv[4]
	compile_all = 1
}

//console.log(src, dst, all)

var mkpathCache = {}
function mkpath(f) {
	var x = f.split(/[\\\/]/)
	for(var i=1, n=x.length; i<n; i++) {
		var r = x.slice(0, i).join("/")
		if(r in mkpathCache) continue
		mkpathCache[r] = 1
		try { fs.mkdirSync(r) } catch(e) {}
	}
}

function sec(s) {
	if(s/1000>1) return s/1000+" s"
	return s+" ms"
}

// расширения для компилляции
var Exts = "@(js|jsx|coffee|coffee|litcoffee|png|jpeg|jpe|jpg|gif)"

// компиллирует
function compile() {
	
	//process.stderr.write("compile!\n")
	
	var compilation = {}	// скомпиллированные файлы
	
	// процесс компилляции
	for(var i=0, n=src.length; i<n; i++) {
		var paths = glob.sync(src[i]+"/**/*."+Exts)
		for(var j=0, k=paths.length; j<k; j++) {
			
			var result = null
			var code = null

			
			var start = new Date()
			var path = norm(paths[j])
			var ext = path.match(/\.(\w+)$/)
			ext = ext[1]
			
			var r = path.replace(base, "")
			var status
			var x1 = path.slice(src[i].length).replace(/^\//, "")
			var x = dst + "/" + x1.replace(/.\w+$/, '.js')
			var map_x = x+".map"
			
			compilation[x] = 1
			compilation[map_x] = 1
			
			
			mkpath(x)
			
			try {
				
				if(!compile_all) {
					var m = false
					try { m = fs.statSync(x).mtime } catch(e) {}
					//console.log(path, x, fs.statSync(path).mtime, m)
					if(m && fs.statSync(path).mtime < m) continue;
				}
				
				

				
				var opt = {
					filename: x1,
					presets: [
						['env', {
							loose: true,
							useBuiltIns: true,
							debug: false,
							modules: false, // false, commonjs (default), amd, umd, systemjs
							
							spec: false,
							// "targets": {
								// "ie": 6
							// },
						}]
						], // 'react', "stage-1", env, "stage-0", 'react', "stage-2", es2015, es3
					plugins: [
						["transform-react-jsx", {pragma: "h"}],
					
						"transform-es5-property-mutators",
						
						// import from -> require
						['transform-es2015-modules-commonjs', {loose: true}],
						
						// трансформация static и др. свойств внутри класса
						["transform-class-properties", { spec: false }],
						
						// export v from 'mod';
						"transform-export-default",
						
						// оператор ...
						"transform-object-rest-spread",
						
						// ::this.x
						"transform-function-bind",
						
						// async await
						"transform-async-generator-functions",
						
						// через require, а не полифилы
						// ["transform-runtime", {
						  // "helpers": false,
						  // "polyfill": false,
						  // "regenerator": true,
						  // "moduleName": "babel-runtime"
						// }],
						
						// компиляция eval("e => e.x") -> eval("function(e) { return e.x }")
						"transform-eval",
					
						// @декораторы
						"transform-decorators-legacy", 
						
						// do { if() {} else {} }
						"transform-do-expressions", 
						
						// .default -> ["default"]
						"transform-es3-property-literals",
						"transform-es3-member-expression-literals",
						"transform-es3-modules-literals",

						// ["module-resolver",	{
							// "root": ["."],
							// "alias": {
								// "react": "lib/preact-compat",
								// "react-dom": "lib/preact-compat",
								// "create-react-class": "lib/preact-compat/lib/create-react-class",
								// "preact": "lib/preact",
								// //"prop-types": "lib/prop-types",
							// }
						// }]


					],
					compact: false,
					minified: false,
					sourceFileName: path,
					sourceMaps: true,
					sourceMapTarget: map_x,
				}
				
				
				if(/\.(coffee|litcoffee)$/.test(path)) {
				
					code = fs.readFileSync(path, "utf8")
				
					//{js, v3SourceMap, sourceMap}
					result = coffeescript.compile(code, {
						filename: path,
						transpile: opt,
						sourceMap: true,
						inlineMap: false,
						bare: false,
						header: true,
						literate: /litcoffee$/.test(path)
					})
					
					result = { code: result.js, map: result.sourceMap }
				
				}
				else if(/\.(jsx)$/.test(path)) {
					code = fs.readFileSync(path, "utf8")
					result = babel.transform(code, opt)
				}
				else if(/\.(js)$/.test(path)) {
					code = fs.readFileSync(path, "utf8")
					result = {
						code: code,
						map: null
					}
				}
				// else if(/\.(css)$/.test(path)) {
					// code = fs.readFileSync(path, "utf8")
					
					// result = {
						// code: ['exports["default"]=', JSON.stringify(code)].join(""),
						// map: null
					// }
				// }
				else if(/\.(png|jpeg|jpe|jpg|gif)$/.test(path)) {
					code = fs.readFileSync(path)
					var mime_ext = ext.replace(/^(jpeg|jpe)$/, 'jpg')
					code = new Buffer(code).toString('base64')

					result = {
						code: ['exports["default"]="data:image/', mime_ext, ';base64,', code, '"'].join(""),
						map: null
					}
				}
				else {
					throw new Error("Незарегистрированное расширение "+ext)
				}
				
				fs.writeFileSync(x, result.code, 'utf8')
				if(result.map) fs.writeFileSync(map_x, JSON.stringify(result.map))
				
				status = " done".white
	
			} catch(e) {
				//console.dir(e)
				var error = String(e)
				if(e.codeFrame) error = e.codeFrame +'\n'
				error = JSON.stringify(error)
				try { fs.writeFileSync(x, "throw new Error("+error+")") } catch(e) {
					console.log("ошибка не записана в файл!\n\n".red+error)
				}
				status = " fail".red
			}
			
			process.stderr.write(r.green+status+" "+sec(new Date() - start).gray+"\n")
		}
	}
	
	// определеляем файлы, которые должны быть удалены
	var paths = glob.sync(dst + "/**/*."+Exts)
	for(var i=0, n=paths.length; i<n; i++) {
		var path = paths[i]
		if(!compilation[path]) {
			try {
				fs.unlinkSync(path)
				status = " done".white
			} catch(e) {
				status = " fail".red
			}
			process.stderr.write(path.substr(dst.length+1).green+" remove".blue+status+"\n")
		}
	}
	
}

compile()

if(watchify) {
	process.stderr.write("WATCH".red+" "+"enabled".white+"\n")
	setInterval(compile, 1000)
}

