package R::Make::Has;


name "has";
args "�����";
port "���������� �������� ������";
task {
	my ($class) = @_;
	
	quit "������� �����" if !defined $class;
	
	my $path = $class;
	$path =~ s!::!/!g;
	$path .= ".pm";
	require $path;
	
	$app->log->info(":white space", $class, ":reset", @{"${class}::ISA"}? ("<", @{"${class}::ISA"}): ());

	my %scalar;
	my %array;
	my %hash;
	my %code;
	my %io;
	my %glob;
	my %format;
	
	while(my ($k, $v) = each %{"${class}::"}) {
		$scalar{$k} = *$v{SCALAR} if *$v{THING};
		$array{$k} = *$v{ARRAY} if *$v{ARRAY};
		$hash{$k} = *$v{HASH} if *$v{HASH};
		$code{$k} = *$v{CODE} if *$v{CODE};
		$io{$k} = *$v{IO} if *$v{IO};
		$glob{$k} = *$v{GLOB} if *$v{GLOB};
		$format{$k} = *$v{FORMAT} if *$v{FORMAT};
		
		#$app->log->info($key, *$val{CODE});
	}

	
	if(%scalar) {
		$app->log->info(":bold black space", "�������:", ":cyan sep", sort keys %scalar);
		print "\n";
	}
	
	if(%array) {
		$app->log->info(":bold black space", "�������:", ":blue sep", sort keys %array);
		print "\n";
	}
	
	if(%hash) {
		$app->log->info(":bold black space", "����:", ":magenta sep", sort keys %hash);
		print "\n";
	}
	
	if(%io) {
		$app->log->info(":bold black space", "������:", ":blue sep", sort keys %io);
		print "\n";
	}
	
	if(%glob) {
		$app->log->info(":bold black space", "�����:", ":red sep", sort keys %glob);
		print "\n";
	}
	
	if(%format) {
		$app->log->info(":bold black space", "�������:", ":green sep", sort keys %format);
		print "\n";
	}
	
	if(%code) {
		$app->log->info(":bold black space", "������:", ":cyan sep", sort keys %code);
		print "\n";
	}
	
	my $has = $R::Has::HAS{$class};
	
	if($has) {
		$app->log->info(":bold black", "��������:");
		
		my $opt = $has->{has};
		for my $k ( sort keys %$opt ) {
			my $option = $opt->{$k};
			$app->log->info(":blue space", " $k", ":red", "->", ":reset inline", map { $_ eq "late" && !$option->{$_}? (): (":sep( => )", $_, ":sep", $option->{$_}) } sort keys %$option);
		}
		
	}
	
	print "\n";
	
};
