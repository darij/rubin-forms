package R::Make::Fulminant;
# задачи для работы с сервером

use common::sense;
use R::App;
use R::Make;


category "fulminant - web-сервер";

name "dev";
args "[yogi-file][:port] [query-fibers][/websocket-fibers]";
port "сервер разработки";
spec "
	
";
sub dev {
	my ($yogi, $fibers) = @_;
	
	my $jsx = $app->process("jsx" => "use R::Make; app->make->load; make 'cc', 3")->stderr->stdout->run;
	
	make "dev1", $yogi, $fibers, $jsx;
}


name "dev1";
args "[yogi-file][:port] [query-fibers][/websocket-fibers]";
port "сервер разработки без jsx";
spec "
	
";
sub dev1 {
	my ($yogi, $fibers, $jsx) = @_;
	
	# включаем поддержку coro
	$app->coro;
	
	$app->dev->{port} = $1 if $yogi =~ s/:(\d+)$//;
	$app->dev->{yogi} = "dev" if length $yogi;
	
	$fibers =~ m!^(?:(\d+))?(?:/(\d+))?$! and do {
		$app->dev->{queries} = $1 if length $1;
		$app->dev->{websockets} = $2 if length $2;
	};
	
	$app->dev->devrun($jsx);
}


# обходит роутеры рекурсивно и печатает в таблицу
sub _r_fmt {
	my ($r, $space) = @_;
	my $via = join "|", @{$r->{via}};
	my $mask = $r->{mask};
	my $controller = $r->{controller};
	app->log->info(":space", $space,
		length($via)? (":cyan", $via): (":red", "*"),
		length($mask)? (":black bold", $mask): (":red", "/"),
		defined($controller)? (":magenta", $controller): (),
	);
	
	_r_fmt($_, "  $space") for @{$r->{routers}};
	
	my $e = $r->{error};
	app->log->info(":cyan space", $space, $_) for sort keys %$e;
}

name "routers";
args "[yogi-file]";
port "список роутеров";
sub routers {
	my ($yogi) = @_;
	$app->dev->{yogi} = "dev" if length $yogi;
	
	_r_fmt($app->dev->routers, "");
}


category "Jsx - шаблоны";

name "cc";
args "[1|2|3]";
port "компиллирует jsx-шаблоны";
spec "
1 - скомпиллировать всё
3 - скомпиллировать и запустить ватчер
";
sub cc {
	my ($phase) = @_;
	
	app->tty->raw;
	
	my $cmd = $app->javascriptCompile->new(phase => $phase)->cmd;
	run $cmd;
	
	# my $src = $app->project->files("src")->cygwin->join(",");
	# my $dst = $app->project->file("var/.jsx")->mkpath->cygwin->path;
	
	# my $compile = $app->framework->file("node/compile.js")->cygwin->path;
	
	# $ENV{NODE_PATH} = $app->framework->file("node/node_modules/")->cygwin->path;
	
	# run "node '$compile' '$src' '$dst' $phase";
}


name "cclean";
args "";
port "очищает выходную директорию jsx-шаблонов";
sub cclean {
	$app->project->file("var/.jsx")->rm;
	$app->log->info(":red", "Ок");
}

name "ccerr";
args "[path]";
port "показывает ошибку у указанного файла";
sub ccerr {
	my ($path) = @_;
	
	if($path) {
		eval {
			$app->javascript->_require($path);
		};
		print $@;
	}
	else {
		my $pf = $app->project->file("var/.jsx");
		$pf->find("**.js")->then(sub {
			$path = $_->root($pf)->ext("")->path;
			eval {
				$app->javascript->_require($path);
			};
			$app->log->info(":empty magenta", $path, ":reset", "\n", "$@") if $@;
		});
	}
	
}


name "render";
args "путь [perl-data]";
port "рендерит шаблон jsx";
spec "";
test {
	quit "нет выражения" if @_<1;
};
sub render {
	my ($template, $code) = @_;
	my $data = $code? do {
		my $ret = do { package main; +{eval $code} };
		die if $@;
		$ret
	}: undef;
	my $path = $app->file($template)->root($app->project->path . "/src")->path;
	my $html = $app->javascript->render($path, $data);
	print $html;
	#my $f = $app->file("$template.html")->dir("")->write($html);
	#$app->log->info(":red", "Ок");
	#print $app->color->html($html);
}



1;