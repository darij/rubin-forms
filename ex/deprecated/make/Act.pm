package R::Make::Act;
# контроллеры и шаблоны

use common::sense;
use R::App;
use R::Make;


category "КОНТРОЛЛЕРЫ И ШАБЛОНЫ";


name "extract";
args "[шаблоны или директории]";
port "перемещает шаблоны из контроллеров в папку mid";
sub extract {
	
	my $c = sub {
		my @a = split /^\@\@[ \t]*([^\n]+?)[\r\t ]*(?:\n|\z)/m, $1;
		shift @a;
		map2 {
			my $f = $app->project->file($a);
			$f->frontdir($f->ext eq "mid"? "mid": "html");
			$f->mkpath->write($b);
		} @a;
		
		"";
	};
	
	my $files = $app->file(@_ || "lib/Controller");
	
	for my $f ($files->find("**.pm")->slices) {
	
		$f->replace(sub {
			s{^__DATA__\s+(.*)}{$c->()}gems;
		});

	}

}

name "gather";
args "контроллер";
port "собирает в контроллер все шаблоны, которые у него прописаны в render";
sub gather {
	...
}

name "menu";
args "";
port "распечатывает меню из контроллеров";
sub menu {
	$app->log->info( $app->sitemap->menu );
}

1;