#!/usr/bin/perl
use Coro;
use Coro::Multicore;

$to = 20;

push @async, async {
	print "- 1 -   $_\n" for 1..$to;
};

push @async, async {
	print "- 2 -   $_\n" for 1..$to;
};

$_->join for @async;