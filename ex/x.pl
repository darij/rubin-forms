package Curr;
use strict;
use warnings;
use Moose;
 
 
has 'price' => (
	isa => 'Int', 
	is => 'rw', 
	accessor => "_price",
);

sub price {
	my ($self, $price) = @_;
	if(@_ > 1) {
		$self->_price(int($price*100))
	} else {
		$self->_price / 100
	}
}


package main;

my $x = Curr->new(price=>10);

print STDERR $x->price."\n";