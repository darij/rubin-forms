requires 'perl', '5.008001';

on 'test' => sub {
	requires 'Coro::Channel';              # 6.55
	requires 'Coro::Specific';             # 6.55
	requires 'Coro::Timer';                # 6.55
	requires 'Nix';                        # 
	requires 'Test::More';                 # 1.302073
};

	requires 'AnyEvent';                   # 7.16
	requires 'AnyEvent::Util';             # 7.16
	requires 'Coro';                       # 6.55
	requires 'Coro::AIO';                  # 
	requires 'Coro::AnyEvent';             # 6.55
	requires 'Coro::Debug';                # 6.55
	requires 'Coro::Handle';               # 6.55
	requires 'Coro::LWP';                  # 6.55
	requires 'Coro::PatchSet';             # 0.13
	requires 'Coro::Semaphore';            # 6.55
	requires 'Coro::Signal';               # 6.55
	requires 'Coro::Socket';               # 6.55
	requires 'Coro::State';                # 6.55
	requires 'Crypt::Eksblowfish::Bcrypt'; # 0.009
	requires 'DBI';                        # 1.642
	requires 'DDP';                        # 0.40
	requires 'Data::Dumper';               # 2.167
	requires 'Devel::Peek';                # 1.26
	requires 'Digest::MD5';                # 2.55
	requires 'Digest::SHA1';               # 2.13
	requires 'Encode';                     # 2.88
	requires 'Encode::Detect::Detector';   # 1.01
	requires 'Errno';                      # 1.28
	requires 'Exporter';                   # 5.72
	requires 'Fcntl';                      # 1.13
	requires 'File::Find';                 # 1.34
	requires 'Guard';                      # 1.023
	requires 'HTML::Selector::XPath';      # 0.25
	requires 'HTML::TreeBuilder';          # 5.07
	requires 'HTML::TreeBuilder::Select';  # 0.111
	requires 'HTTP::Cookies';              # 6.04
	requires 'HTTP::Date';                 # 6.02
	requires 'IO::AIO';                    # 
	requires 'IO::Compress::Brotli';       # 0.004001
	requires 'IO::Compress::Bzip2';        # 2.074
	requires 'IO::Compress::Gzip';         # 2.074
	requires 'IO::String';                 # 1.08
	requires 'JSON::Color';                # 0.12
	requires 'JSON::XS';                   # 4.02
	requires 'LWP::ConnCache';             # 6.31
	requires 'LWP::Protocol::Coro::http';  # v1.14.0
	requires 'LWP::UserAgent';             # 6.31
	requires 'List::Util';                 # 1.52
	requires 'MIME::Base64';               # 3.15
	requires 'MIME::Lite';                 # 3.030
	requires 'Math::Trig';                 # 1.23
	requires 'POSIX';                      # 1.76
	requires 'Proc::FastSpawn';            # 1.2
	requires 'Protocol::WebSocket::Frame'; # 
	requires 'Protocol::WebSocket::Handshake::Server';# 
	requires 'Scalar::Util';               # 1.52
	requires 'Socket';                     # 2.020_03
	requires 'Socket::Linux';              # 0.01
	requires 'Storable';                   # 3.11
	requires 'Sub::Identify';              # 0.14
	requires 'Symbol';                     # 1.08
	requires 'Syntax::Highlight::Engine::Kate';# 0.14
	requires 'Sys::Syslog';                # 0.35
	requires 'TOML::Dumper';               # 0.01
	requires 'TOML::Parser';               # 0.91
	requires 'Term::ANSIColor';            # 4.06
	requires 'Term::ProgressBar';          # 2.22
	requires 'Term::ReadKey';              # 2.38
	requires 'Term::Screen';               # 1.06
	requires 'Text::Levenshtein::XS';      # 0.503
	requires 'Time::HiRes';                # 1.9741
	requires 'Types::Serialiser';          # 1.0
	requires 'URI';                        # 1.73
	requires 'URI::Escape::XS';            # 0.14
	requires 'Win32::Clipboard';           # 
	requires 'Win32::GuiTest';             # 
	requires 'YAML::Syck';                 # 1.31
	requires 'YAML::Tiny::Color';          # 0.04
	requires 'base';                       # 2.26
	requires 'bytes';                      # 1.05
	requires 'common::sense';              # 3.74
	requires 'mro';                        # 1.20
	requires 'namespace::autoclean';       # 0.28
	requires 'overload';                   # 1.28
	requires 'parent';                     # 0.236
	requires 'vars';                       # 1.03
