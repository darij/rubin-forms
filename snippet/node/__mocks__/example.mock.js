'use strict';

/**
 * ExampleMock
 * @class
 * @constructor
 * @public
 */
module.exports = class ExampleMock {
    constructor(setup) {
        this.prop = setup.prop;
    }
};
