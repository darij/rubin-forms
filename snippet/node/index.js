'use strict';

const {{Class}} = require('./lib/{{name}}');

/**
 * {{Class}}
 * @class
 * @constructor
 * @public
 */
module.exports = class {{Class}} {

    constructor (setup) {
        
    }

};

