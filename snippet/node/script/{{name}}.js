#!/usr/bin/env node
'use strict';

// ������ �������� ��� ������������ ������ ���������
// ��� ����� ������� ��� ��������

const {{Class}} = require('..');

const {{class}} = argv => new  {{Class}}({
    server: argv.server,
    space: argv.space,
});

// �����
function reply (reply) {
    console.dir(reply, { colors: true });
    process.exit();
}


require('yargs')
    .wrap(100)
    .usage('�����������: {{name}} get --server <������ ��������> --space <������������ ����>')
    .option('s', {
        alias: 'server',
        type: 'array',
        default: ['{{name}}.test:7001'],
        describe: '������ ��������: --server <������ 1> --server <������ 2> ...',
    })
    .option('p', {
        alias: 'space',
        default: 'page',
        describe: '������������ ����',
    })
    .command('get', '����������� ������ �� �������', {}, (argv) => {
        {{class}}(argv).get(...argv._).then(reply, reply);
    })
    .demandCommand()
    .help()
    .argv;  // �� �������! ��� .argv �� ��������!
