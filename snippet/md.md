# NAME

{{name}} - 

# VERSION

0.0.0

# SYNOPSIS

```js
let {{name}} = new {{Name}}() {}
```

# DESCRIPTION



# INSTALL

```sh
$ sudo cpm install -g {{name}}
```

# SUBROUTINES/METHODS

## new

```js
let {{name}} = new {{Name}}() {}
```

Конструктор.

Параметры:

* `` - 

Возвращает: 

# LICENSE

Copyright (C) {{author}}

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.

# AUTHOR

{{author}} {{email}}