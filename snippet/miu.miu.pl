= NAME

{{ package }} - 

= SINOPSIS



= DESCRIPTION



= INSTALL

`$ cpm install -g {{ package }}`

= SUBROUTINES/METHODS

== new

Constructor.

= SEE ALSO

* 

= LICENSE

Copyright (C) Yaroslav O. Kosmina.

This library is free software; you can redistribute it and/or modify it under the same terms as Perl itself.

= AUTHOR

Yaroslav O. Kosmina <dart@cpan.org>
