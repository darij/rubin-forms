"""
A setuptools based setup module.
"""

from os import path
from setuptools import setup, find_packages

here = path.abspath(path.dirname(__file__))

with open(path.join(here, 'README.md'), encoding='utf-8') as f:
    long_description = f.read()

setup(
    name='ccxx-{{name}}',
    version='0.0.0',
    description=long_description,
    long_description=long_description,
    long_description_content_type='text / markdown',
    url='https://gitee.220v.ru/220V/{{name}}',
    author='220-volt',
    author_email='dev@220-volt.ru',
    package_dir={'': 'src'},
    packages=find_packages(where='src'),
    python_requires='>=3.6, <4',
    classifiers=[
        'Intended Audience :: Developers',
        'Topic :: Software Development :: Build Tools',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
    ],
    install_requires=[
    ],
)
