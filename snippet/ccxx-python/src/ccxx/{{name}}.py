"""
NAME
====
{{name}} — демонстрационный модуль,
цель которого осуществить проверку работоспособности пакета.

VERSION
=======
0.0.0

SYNOPSIS
========

    from ccxx.{{name}} import {{name}}
    to_kelvin.kelvin(32)


DESCRIPTION
===========

FUNCTIONS
=========
"""

from prettytable import PrettyTable


def kelvin(fahrenheit: int) -> float:
    """Конвертирует температуру в градусах по Фаренгейту и возвращает ее в Кельвинах

    Args:

        fahrenheit (int): значение в градусах по Фаренгейту

    Returns:

        (float): значение в градусах по Кельвину

    """

    kelvin = 5. / 9. * (fahrenheit - 32.) + 273.15

    table = PrettyTable(["Фаренгейтах", "Кельвинах"])
    table.add_row([fahrenheit, kelvin])
    print(table)

    return kelvin
