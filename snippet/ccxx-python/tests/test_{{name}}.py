"""
unittest case
"""

import unittest
import src.ccxx.{{name}}


class AdvancedTestSuite(unittest.TestCase):
    """Advanced test cases."""

    def test_simple(self):
        """test_simple"""

        self.assertEqual("hi!", "hi!")

if __name__ == '__main__':
    unittest.main()
