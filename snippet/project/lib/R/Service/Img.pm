package R::Service::Img;
# конвертирует указанную картинку

use common::sense;
use R::App;

# конструктор
sub new {
	my $cls = shift;
	bless {@_}, ref $cls || $cls;
}

# конвертирует картинку
sub convert {
	my ($self, $path) = @_;
	my $orig = $app->file($path)->image;

	$path = $orig->dir;

	my $img = $orig->resizing(2000, 2000)->save;
	$img = $img->resizing(600, 600)->save($path . "/main.jpg");
	$img->preview(200)->save($path . "/small.png");
	$img->preview(100)->save($path . "/mini.png");
	
	return 1;
}

1;