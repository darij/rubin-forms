package R::Make::{{name}};
# разнообразные вспомогательные задания

use common::sense;
use R::App;
use R::Make;

category "{{name}}";

name "{{name}}";
args "";
port "хелло {{name}}";
spec "";
sub {{name}} {
	$app->log->info("Привет, {{name}}!");
}

1;
