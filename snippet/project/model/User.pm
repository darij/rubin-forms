package R::Row::User;
# модель пользователя

use base R::Model::Row;

use common::sense;
use R::App;

# вызывается для создания структуры базы
sub setup {
	my ($fields) = @_;
	$fields->

	col(email => "varchar(255)")->remark("e-mail пользователя")->
	col(passwd => "binary(31)")->remark("зашифрованный пароль")->
	compute(pass => qw/passwd/)->remark("пароль")->
	
	ref(ava => 'img')->remark("ава")->
	
	
	col("prefix" => "varchar(50)")->remark("обращение")->
	col("firstname" => "varchar(50)")->remark("имя")->
	col("surname" => "varchar(50)")->remark("фамилия")->
	col("patronymic" => "varchar(50)")->remark("отчество")->
	
	compute(fullname => qw/prefix firstname surname patronymic/)->remark("полное имя пользователя")->
	
	compute(name => qw/prefix firstname surname/)->remark("имя пользователя без отчества")->
	
	col(sex => "enum(мужской,женский,средний)")->default("средний")->remark("пол")->
	
	col(description => 'msg(120k)')->remark("описание пользователя")->
	
	
	# индексы
	unique("email")->
	
	match(
		"prefix"=> 1,
		"firstname"=> 3,
		"surname" => 4,
		"patronimic"=> 5,
		"description" => 2
	)->
	
	meta(
		remark => "пользователь",
		href => "am",
	)->
	
	end;
}

# реальные данные
sub realdata {
	my ($self) = @_;
	
	$self
}

# тестовые данные
sub testdata {

	my $t = $app->t("testdata");

	my $admin = $app->model->user(1);
	$admin->email('@');
	$admin->pass('123');
	$admin->fullname("мистер Бесперебойный Админ Батькович");
	$admin->cost(1000);
	$admin->description("Я люблю кофе");
	$admin->store;
	$admin->ava->description("ава админа")->owner($admin)->file("html/img/testdata/admin.jpg");
	
	my $user = $app->model->user(2);
	$user->email('u@');
	$user->pass('123');
	$user->fullname("Странник между мирами");
	$user->description("Так и живу");
	$user->cost(100);
	$user->store;
	$user->ava->description("ава пользователя")->owner($user)->file("html/img/testdata/user.jpg");
	
}


# добавление
sub onAdd {
	my ($self) = @_;

	$self
}

# добавление
sub onAdded {
	my ($self) = @_;

	$self
}


# возвращает зашифрованный пароль, и зашифровывает пароль при установке
sub pass {
	my $self = shift;
	if(@_) {
		$self->passwd( $app->perl->cipher( shift ) );
	}
	else {
		$self->passwd
	}
}

# имя
sub fullname {
	my $self = shift;
	if(@_) {
		my $name = shift;
		my ($prefix, $surname, $firstname, $patronimic) = $name =~ /^\s*(?:(?:(\S+)\s+)?(\S+)\s+)(\S+)(\s+(.*))?/s;
		$self->prefix($prefix // "");
		$self->surname($surname // "");
		$self->firstname($firstname // "");
		$self->patronymic($patronimic // "");
	} else {
		join " ", $self->prefix, $self->surname, $self->firstname, $self->patronymic;
	}
}

# без отчества
sub name {
	my $self = shift;
	if(@_) {
		my $name = shift;
		my ($prefix, $firstname, $surname) = $name =~ /^\s*(?:(\S+)\s+)?(\S+)(\s+(.*))?/s;
		$self->prefix($prefix);
		$self->firstname($firstname);
		$self->surname($surname);
	} else {
		join " ", $self->prefix, $self->firstname, $self->surname;
	}
}


1;