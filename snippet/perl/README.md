# NAME

perl::mime::multipart - It's new $module

# SYNOPSIS

    use perl::mime::multipart;

# DESCRIPTION

perl::mime::multipart is ...

# LICENSE

Copyright (C) Yaroslav O. Kosmina.

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.

# AUTHOR

Yaroslav O. Kosmina <kosmina.yaroslav@220-volt.ru>
