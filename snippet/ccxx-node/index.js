'use strict';

// Точка входа в библиотеку. Добавлена для демонстрации работы пайплайна
// Её можно удалить или изменить
// Точка входа прописана в package.json как "main": "index.js"

const ExampleLib = require('./lib/example.lib.js');

/**
 * Example
 * @class
 * @constructor
 * @public
 */
module.exports = class Example {
    constructor (setup) {
        this.client = new ExampleLib(setup);
    }
};
