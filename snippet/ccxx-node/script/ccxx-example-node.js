#!/usr/bin/env node
'use strict';

// ������ �������� ��� ������������ ������ ���������
// ��� ����� ������� ��� ��������

const Example = require('..');

const example = argv => new Example({
    server: argv.server,
    space: argv.space,
});

// �����
function reply (reply) {
    console.dir(reply, { colors: true });
    process.exit();
}


require('yargs')
    .wrap(100)
    .usage('�����������: ccxx-example-node get --server <������ ��������> --space <������������ ����>')
    .option('s', {
        alias: 'server',
        type: 'array',
        default: ['example.220v.test:7001'],
        describe: '������ ��������: --server <������ 1> --server <������ 2> ...',
    })
    .option('p', {
        alias: 'space',
        default: 'page',
        describe: '������������ ����',
    })
    .command('get', '����������� ������ �� �������', {}, (argv) => {
        example(argv).get(...argv._).then(reply, reply);
    })
    .demandCommand()
    .help()
    .argv;  // �� �������! ��� .argv �� ��������!
