package {{ package }};

use strict;
use warnings;
use utf8;

use Moose;

has {{name}} => (
	is => "rw",
	isa => "Str",
);

1;

__END__

=encoding utf-8

=head1 NAME

{{ package }} - ...

=head1 SYNOPSIS

    use {{ package }};

    my ${{name}} = {{ package }}->new(
    );

    my $href = ${{name}}->get();
	
=head1 DESCRIPTION

...

=head1 SUBROUTINES/METHODS



=head1 LICENSE

Copyright (C) {{author}}.

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.

=head1 AUTHOR

{{author}} E<lt>{{email}}E<gt>

=cut
