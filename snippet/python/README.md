# NAME

{{name}} - 

# VERSION

0.0.1

# SYNOPSIS

```python
from {{name}} import {{Name}}

{{name}} = {{Name}}()

print({{name}})        # -> Здравствуй, Мир!
```

# DESCRIPTION

This is the 

# CLASSES

# METHODS

## 

### ARGUMENTS

* `` - . Обязательный

### RETURNS

Any

# INSTALL

```sh
$ pip install {{name}}
```

# REQUIREMENTS

Нет

# AUTHOR

{{author}} <{{email}}>

# LICENSE

MIT License

Copyright (c) 2020 {{author}}

