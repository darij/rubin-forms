package R::Row::{{ Name }};
# модель {{ name }}

use base R::Model::Row;

use common::sense;
use R::App;

# вызывается для создания структуры базы
sub setup {
	my ($fields) = @_;
	$fields->

	col(name => "varchar(255)")->remark("наименование")->
	
	# индексы
	unique("name")->
	
	# метaинформация
	meta(
		remark => "{{ name }}",
	)->
	
	end;
}

# реальные данные
sub realdata {
}

# тестовые данные
sub testdata {
	$app->model->{{ name }}(1)->name('NAME')->store;
}

# выдаёт краткую информацию о себе
sub annonce {
	my ($self) = @_;
	$self->name
}


#@@category методы набора записей
package R::Rowset::{{ Name }};

1;