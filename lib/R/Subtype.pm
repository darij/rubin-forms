package R::Subtype;
# создаёт subroutine type, попросту чекеры 

use common::sense;
use R::App qw/:min/;

our %TYPE;		# типы - code
our %TYPE_MSG;	# сообщение при нарушении типа
our %TYPE_JS;	# типы для javascript - выводятся в формах

# при перезагрузке - сохраняем
sub HOTSWAP_REMOVE {
	my ($cls, $hotswap, $from_reload) = @_;
	$hotswap->{STASH}{$cls} = {
		TYPE => {%TYPE},
		TYPE_MSG => {%TYPE_MSG},
		TYPE_JS => {%TYPE_JS},
	};
}

# при перезагрузке - восстанавливаем
sub HOTSWAP_LOAD {
	my ($cls, $hotswap, $service) = @_;
	my $x = $hotswap->{STASH}{$cls};
	%TYPE = %{$x->{TYPE}};
	%TYPE_MSG = %{$x->{TYPE_MSG}};
	%TYPE_JS = %{$x->{TYPE_JS}};
}

# чекает на соответствие типу
# если указан только один параметр - то проверяет $_
sub Is ($;$) {
	my ($val, $type) = @_==1? ($_, $_[0]): @_;
	local $_ = $val;
	codeunion($type)->()
}

# возвращает сообщение по типу
sub get_msg ($) {
	my ($type) = @_;
	$TYPE_MSG{$type} // "значение `%dump` не типа %type"
}

# форматирует сообщение 
sub fmt_msg ($$$) {
	my ($msg, $val, $type) = @_;
	my $fn = sub {
		!defined($+{param})? "%": 
		$+{param} eq "type"? $type: 
		$+{param} eq "val"? $val:
		$+{param} eq "dump"? $app->perl->inline_dump($val, 100):
		die "неизвестный параметр $+{param}"
	};
	$msg =~ s/%(?<param>\w+)|%\((?<param>\w+)\)|%%/$fn->()/gne;
	$msg
}

# чекает на соответствие типу и возвращает ошибку или ""
# если указан только один параметр - то проверяет $_
# если вызван в void-контексте, то выбрасывает эксепшн
sub IsErr ($;$) {
	my ($val, $type) = @_==1? ($_, $_[0]): @_;
	local $_ = $val;
	codeunion($type)->()? undef: do {
		my $msg = get_msg $type;
		$msg = fmt_msg $msg, $val, $type;
		die $msg if !defined wantarray;
		$msg
	}
}

# переводит строку типа в функцию
# Str HahRef{Num ::A }
# $TYPE{Str}() || $TYPE{HahRef}() && (all { $TYPE{Num}() || UNIVERSAL::isa("::A") } values %$_)
my $ID = qr/[a-z_]\w*/i;
my $CLASS = qr/::$ID | $ID(::$ID)+/x;
my $NUM = qr/\s*-?\d+(\.\d+)?\s*/;
sub codeunion {
	my ($s) = @_;
	
	return $s if ref $s eq "CODE";
	
	$s =~ s/\s+/ /g;
	
	return $TYPE{$s} if exists $TYPE{$s};
	
	my @x = split /( [\{\[<] \s* | \s* [\}\]>] | \s+)/ox, $s;
	
	#print "$s -> \n";
	
	my $and = "";
	
	my $evl = join "", map {
	
		my $out =
		/^(?<id>$ID)\((?<nums>$NUM(,$NUM)*)\)\z/on? do {
			my ($id, $nums) = ($+{id}, $+{nums});
			my @a = split /\s*,\s*/, $nums;
		
			my $r = "(ref(\$_) eq ''? length(\$_): Scalar::Util::reftype(\$_) eq 'ARRAY'? scalar(\@\$_): Scalar::Util::reftype(\$_) eq 'HASH'? scalar(keys %\$_): 'nan')";
		
			$id eq "min"? "\$_ >= $a[0]":
			$id eq "max"? "\$_ <= $a[0]":
			$id eq "minlen"? "$r >= $a[0]":
			$id eq "maxlen"? "$r <= $a[0]":
			$id eq "between"? "\$_ ~~ [$a[0] .. $a[1]]":
			$id eq "len"? (@a == 2? "$r ~~ [$a[0] .. $a[1]]": "$r == $a[0]"):
			$id eq "in"? "\$_ ~~ [$nums]":
			$id eq "lenin"? "$r ~~ [$nums]":
			die "нет функции $id для описания типа";
			
		}:
		/^$/? "":
		/^\s+$/? " || ":
		/^$CLASS$/o? "UNIVERSAL::isa(\$_, '$_')":
		/^$ID$/o? do { die "нет типа `$_` в правиле `$s`" if !exists $TYPE{$_}; "\$TYPE{'$_'}()" }:
		/^\{\s*$/? "${and}Scalar::Util::reftype(\$_) eq 'HASH' && (keys(\%\$_) == grep { ":
		/^\s*\}$/? " } values %\$_)":
		/^\[\s*$/? "${and}Scalar::Util::reftype(\$_) eq 'ARRAY' && (\@\$_ == grep { ":
		/^\s*\]$/? " } \@\$_)":
		/^<\s*$/? "${and}Scalar::Util::reftype(\$_) eq 'SCALAR' && (1 == grep { ":
		/^\s*>$/? " } \$\$_)":
		die "subtype syntax error: `$s`";
		
		$and = /^\s*$/? "": " && ";
		
		$out
	} @x;
	
	$evl = "sub { $evl }";
	#print "$evl\n";
	my $code = eval $evl;
	die if $@;
	$TYPE{$s} = $code
}

# генерирует код для javascript
sub jscodeunion {
	my ($name, $s) = @_;
	
	my @x = split /( [\{\[<] \s* | \s* [\}\]>] | \s+)/ox, $s;
	
	my $and = "";
	
	my $code = join "", map {
	
		my $out =
		/^(?<id>$ID)\((?<nums>$NUM(,$NUM)*)\)\z/on? do {
			my ($id, $nums) = ($+{id}, $+{nums});
			my @a = split /\s*,\s*/, $nums;
		
			my $r = "(typeof a == 'string' || a instanceof Array? a.length: typeof a == 'object'? Object.keys(a).length: NaN)";
		
			$id eq "min"? "parseFloat(a) >= $a[0]":
			$id eq "max"? "parseFloat(a) <= $a[0]":
			$id eq "minlen"? "$r >= $a[0]":
			$id eq "maxlen"? "$r <= $a[0]":
			$id eq "between"? "$a[0]<=parseFloat(a) && parseFloat(a) <= $a[1]":
			$id eq "betweenlen"? "$a[0] <= $r && $r <= $a[1]":
			$id eq "in"? do { ; "parseFloat(a) ~~ [$nums]" }:
			$id eq "lenin"? "$r ~~ [$nums]":
			die "нет функции $id для описания типа";
			
		}:
		/^$/? "":
		/^\s+$/? " || ":
		/^$CLASS$/o? do { my $x=$_; $x=~s/^::|::$//; $x=~ s/::/./g; "(a instanceof $x)" }:
		/^$ID$/o? do { die "нет типа `$_` в правиле `$s`" if !exists $TYPE_JS{$_}; "TYPE_JS['$_'](a)" }:
		/^\{\s*$/? "${and}typeof(a) === 'object' && !(a instanceof Array) && (Object.values(a).length === Object.values(a).filter(function(a) { return ":
		/^\s*\}$/? " })":
		/^\[\s*$/? "${and}a instanceof Array && a.length === a.filter(function(a) { return ":
		/^\s*\]$/? " })":
		/^<\s*$/? die("в js нет ссылки на скаляр"):
		/^\s*>$/? die("в js нет ссылки на скаляр"):
		die "jstype syntax error: `$s`";
		
		$and = /^\s*$/? "": " && ";
		
		$out
	} @x;
	
	$code = "function $name(a) { return $code }";
	
	$code
}

# создаёт тип
#  subtype Intref => "ArrayRef[INT]";
#  subtype Natural => sub { Is("Int") && $_ >= 1 };
#  subtype NaturalFloat => "Natural Float" => "%%: значение %val не %(type)en";
sub subtype ($$;$) {
	my ($name, $union, $message) = @_;
	
	die "subtype `$name` exists" if exists $TYPE{$name};
	
	# чтобы не пропустить одинаковые
	$union =~ s!\s+! !g if !ref $union;
	
	die "$union not defined" if $union eq $name;	
	
	if(ref $union eq "Regexp") {
		die "некорректный regexp: `$union`" unless "$union" =~ /^\(\?\^(\w*):(.*)\)\z/;
		my ($re, $wa) = ($2, $1);
		$re =~ s!/!\\/!g;
		$re = "/$re/$wa";
		$TYPE{$name} = eval "sub { $re }";
		die if $@;
		$TYPE_JS{$name} = "function $name(a) { return $re.test(a) }";
		$TYPE_MSG{$name} = $message;
	} else {	
		$TYPE{$name} = 1;
		$TYPE{$name} = codeunion($union);
		$TYPE_MSG{$name} = $message;
		$TYPE_JS{$name} = 1, $TYPE_JS{$name} = jscodeunion($name, $union) if ref $union eq "";
	}

	return $name;
}

# устанавливает тип в js. Может использоваться только если нет кода
sub jstype {
	my ($name, $jscode) = @_;
	
	die "subtype `$name` not exists" if !exists $TYPE{$name};
	
	$TYPE_JS{$name} = "function $name(a) { return $jscode }";

	return $name;
}

# # создаёт тип в пределах пакета
# sub localtype ($$;$;$) {

# }

# перечислимый тип
sub subenum ($$) {
	my ($name, $enum) = @_;
	subtype $name => sub { $_ ~~ $enum };
	$TYPE_JS{$name} = join "", "function $name(a) { var e = ", $app->json->to($enum), "; for(var i=0, n=e.length; i<n; i++) if(e[i]==a) return true; return false }";
	return;
}

#Any
#    Item
#        Bool
#        Maybe[`a] - нет
#        Undef
#        Defined
#            Value
#                Str
#                    Num
#                        Int
#                    ClassName
#                    Class - класс имеет метод new
#            Ref
#                ScalarRef[`a]
#                ArrayRef[`a]
#                HashRef[`a]
#                CodeRef
#                RegexpRef
#                GlobRef
#                FileHandle
#                Object

subtype Any => sub {1}; 
subtype Item => sub {1};
# 1, 0, "", undef
subtype Bool => sub { /^(1|0|)$/n };
subtype Undef => sub {!defined $_};
subtype Defined => sub {defined $_};
subtype Value => sub { defined $_ && ref $_ eq "" };
subtype Str => sub { defined $_ && ref $_ eq "" };
subtype Num => sub { ref $_ eq "" && Scalar::Util::looks_like_number($_) };
subtype Int => sub { ref $_ eq "" && Scalar::Util::looks_like_number($_) && $_ == int $_ };
subtype Id => sub { ref $_ eq "" && /^[A-Z_]\w*$/i };
subtype ClassName => sub { ref $_ eq "" && /^(::)?[A-Z]\w*(::[A-Z]\w*)*$/n };
subtype Class => sub { ref $_ eq "" && $_->can("new") };

subtype Ref => sub { ref $_ ne "" };
subtype ScalarRef => sub { Scalar::Util::reftype($_) eq "SCALAR" };
subtype SCALAR => sub { ref $_ eq "SCALAR" };
subtype ArrayRef => sub { Scalar::Util::reftype($_) eq "ARRAY" };
subtype ARRAY => sub { ref $_ eq "ARRAY" };
subtype HashRef => sub { Scalar::Util::reftype($_) eq "HASH" };
subtype HASH => sub { ref $_ eq "HASH" };
subtype CodeRef => sub { Scalar::Util::reftype($_) eq "CODE" };
subtype CODE => sub { ref $_ eq "CODE" };
subtype RegexpRef => sub { Scalar::Util::reftype($_) eq "Regexp" };
subtype Regexp => sub { ref $_ eq "Regexp" };
subtype GlobRef => sub { Scalar::Util::reftype($_) eq "GLOB" };
subtype GLOB => sub { ref $_ eq "GLOB" };
subtype FileHandle => sub { *{$_}{IO} };
subtype Object => sub { Scalar::Util::blessed($_) };


# js-типы
jstype Any => 1; 
jstype Item => 1;
# 1, 0, "", undef
jstype Bool => "a===true || a===false";
jstype Undef => "a==null";
jstype Defined => "a!=null";
jstype Value => "typeof a === 'number' || typeof a === 'string'";
jstype Str => "typeof a === 'number' || typeof a === 'string'";
jstype Num => "typeof a === 'number' || typeof a === 'string' && !isNaN(parseFloat(a))";
jstype Int => "(typeof a === 'number' || typeof a === 'string') && /^-?\d+\$/.test(a)";
jstype Id => "typeof a === 'number' || typeof a === 'string' && /^[A-Z]\w*\$/i.test(a)";
#jstype ClassName => Str => sub { /^(::)?[A-Z]\w*(::[A-Z]\w*)*$/n };
jstype Class => 'typeof a === "function" && "constructor" in a';

jstype Ref => "typeof a === 'object'";
#jstype ScalarRef => sub { Scalar::Util::reftype($_) eq "SCALAR" };
#jstype SCALAR => sub { ref $_ eq "SCALAR" };
jstype ArrayRef => "a instanceof Array";
jstype ARRAY => "typeof a === 'object' && a.constructor === Array";
jstype HashRef => "typeof a === 'object'";
jstype HASH => "a.constructor === Object";
jstype CodeRef => "typeof a === 'function'";
jstype CODE => "typeof a === 'function' && a.constructor === Function";
jstype RegexpRef => "a instaceof RegExp";
jstype Regexp => "typeof a === 'object' && a.constructor === RegExp";
#jstype GlobRef => sub { Scalar::Util::reftype($_) eq "GLOB" };
#jstype GLOB => sub { ref $_ eq "GLOB" };
#jstype FileHandle => sub { *{$_}{IO} };
jstype Object => "typeof a === 'object'";


# вспомогательные типы
subtype Need => "minlen(1)";

1;