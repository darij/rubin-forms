package R::Coffee;
# компиллирует coffeescript посредством node-сервера

use common::sense;
use R::App;

has qw/ps/;

# конструктор
sub new {
	my $cls = shift;
	my $self = bless {
		name => "coffee",
		port => 9017,
		@_
	}, ref $cls || $cls;
	
	$self->{ps} //= $app->process($self->{name} => $self->node);
	
	$self
}

# компилирует файл
sub compile {
	my ($self, $path, $root) = @_;
	
	# поднимаем ноду, если нужно
	$self->{ps}->waitrun if !$self->{ps}->exists;
	die "node не запустилась" if !$self->{ps}->exists;
	
	# делаем к ноде запрос
	my $xpath = $app->file($path)->cygwin->path;
	my $res = $app->url(":$self->{port}")->param(path => $xpath)->res;
	die "node error! " . $res->code .": ". $res->content if $res->code != 200;
	
	$res->content
}

# возвращает код для процесса node
sub node {
	my ($self) = @_;
	"#!node var port = $self->{port}; " . $app->framework->file("share/dev/compile.js")->read
}

# деструктор
sub DESTROY {
	my ($self) = @_;
	$self->{ps}->kill("KILL");
}

1;