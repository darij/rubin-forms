package R::Man;
# для тестов

use common::sense;
use R::App;


# конструктор
sub new {
	my ($cls) = @_;
	bless {}, ref $cls || $cls;
}

# конфигурация для тестов
sub conf {
	my ($self) = @_;
	
    # узнаём имя проекта
    # $app->file(".")->abs->path =~ //;
    # $app->project->name;
    
	# укажем конфигурационный файл, иначе он будет зависеть от имени скрипта
	#$app->ini( $app->ini->parse("etc/rubin-forms.ini") );
		
	select STDERR; $|=1;
	select STDOUT; $|=1;
		
	$self
}

# конфигурация для баз
sub configure {
	my ($self, %opt) = @_;
	
	$self->conf;
	
	# не логировать sql
	my $log = delete $opt{log};
	$app->ini->{service}{connect}{log_info} = delete($opt{logsqlinfo}) // $log;
	$app->ini->{service}{connect}{log} = delete($opt{logsql}) // $log;
	$app->ini->{service}{cache}{log} = delete($opt{logcache}) // $log;
	
	# не кэшировать - "", val - использовать row->{val}, memory - R::Cache
	#$app->ini->{service}{meta}{cache} = $opt{cache} // "val";
	
	# коннектимся без базы
	$app->ini->{service}{connect}{basename} = "";
	# меняем название базы
	my $base = delete($opt{basename}) // "rubin-forms-test";	#":memory:"
	
	# таблицы будут в памяти - так быстрее
	$app->ini->{service}{meta}{engine} = delete($opt{engine}) // "memory";
	
	# if($opt{model} ne "no") {
		# # дабы не пытаться подгружать модели с диска
		# $app->meta($app->modelMetafieldset->base(undef));
	# }
	
	# удалим базу, дабы не нарушать эксперимент
	$app->meta->database($base);
	$app->meta->existsDatabase && $app->meta->dropDatabase;
	$app->meta->createDatabase && $app->connect->use($base);
	
	
	
	die "лишние опции" . join ", ", keys %opt if keys %opt;
	
	$self
}


# для форм
sub form {
	my ($self) = @_;
	
	# Подгружаем шаблоны в view
	my $t = $app->t->new("view load");	$app->view->load; $t->say;
	
	$self
}

# # для сценариев
# sub scenario {
	# my ($self) = @_;
	
	# $app->ini;
	# $app->ini( $app->perl->union( $app->{ini}, $app->{ini}{scenario}, $app->{ini}{'scenario-server'} ) );
	# $app->log( $app->log->new );
	
	# require "Sc.pm";
	
	# $self
# }

# создаёт тестовый проект и переключается в его директорию
sub project {
	my ($self, $name, %args) = @_;
	
	my $f = $app->project->file(".rrrumiurc");
	my $dir = $f->exists && $f->read =~ /^lib_dir[ \r\t]*=[ \r\t]*(.*?)[ \r\t]*$/m && $1 || ".miu/lib";
	
	my $prj = "$dir/$name";
	
	local $app->ini->{include} = delete $args{include};
	$app->file("$prj/etc/$name.yml")->mkpath->write($app->yaml->to($app->ini));

	my $sv = $app->file($prj)->withdir;

	$app->project->change;

	$self->configure(%args) if !$args{nodatabase};
	
	$sv
}

#@@category тестирование

# устанавливает пример запроса
sub simple {
	$app->rsgiRequest->new(
		via => "GET",
		location => "/",
		version => "1.1",
		ip => "127.0.0.1",
		paddr => undef,
		ns => undef,
		data => undef,
		headers => {
			"User-Agent"=> "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:38.9) Gecko/20100101 Goanna/2.0 Firefox/38.9 PaleMoon/26.0.0",
			"Content-Length"=>1,
			"Content-Type"=> "application/x-www-form-urlencoded",
			"Accept"=> "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
			"Accept-Language"=> "en-US,en;q=0.5",
			"Host"=> "http_unicorn",
			"Referer"=> "http://unicorn/ex?x=",
			"Connection"=> "keep-alive",
			"Accept-Encoding"=> "gzip, deflate",
		}
	);
}

# устанавливает некоторые параметры (для тестов)
sub action {
	my ($self, $url, @kwargs) = @_;
	
	unshift @kwargs, "post" if @kwargs == 1;
	
	my %kwargs = @kwargs;
	
	my $q = $self->q;
	
	if(exists $q->{form}) {
		my $form = $q->{form};
		if(!$form->{Concat} && $form->isErrors) {
			msg ":empty red", "ошибки на не выведенной форме ", ":cyan", $form->{Name}, ":space red", ":", ":reset", $form->{errors}, $form->{error};
		}
	}
	
	my $q = $self->simple;
	
	$Coro::current->{request} = $q;
	
	# %$Coro::current = (
		# request => $q,
		# desc => $Coro::current->{desc},
	# );
	
	my $uri = $app->url($url);
	
	my $post = $kwargs{post};
	$q->{via} = $post? "POST": "GET";
	$q->{location} = $uri->location;
	$q->{data} = $app->rsgiBag($post);
	%{$q->{headers}} = (%{$q->{headers}}, Host => $uri->host, %{$kwargs{head}});
	
	my $action = $q->param('@action');
	$self->action_form($action) if defined $action;
	
	$self
}


# обрабытывает запрос формы
sub action_form {
	my ($self, $path) = @_;
	
	my ($class, $attrs, $param) = $app->router->get($path);
	die "action: нет пути $path в роутере" if !$class;
	# if(!defined $class) {
		# $class = $self->param('@action');
		# $class =~ s!/(\w)!ucfirst $1!ge;
	# }
	
	#msg1 $class;
	die "в q уже есть форма" if $self->q->{form};
	$self->q->{form} = my $form = $class->new;
	#die "в q номер формы ".$self->q->{NumberForms}{$class}." != 0" if $self->q->{NumberForms}{$class} != 0;
	$self->q->{NumberForms}{$class} = 0;
	
	# устанавливаем номер формы и уменьшаем счётчик этой формы на странице (в списках может быть много одинаковых форм)
	#my $number = $form->{Number} = $self->param('@number');
	#$form->{Name} = $number? $class . $number: $class;
	

	$form->{save} = 1;	# брать параметры из q.can(lc metaform.method)	
	$form->save;		# возвращает answer
	
	$self
}

# запрос
sub q {
	$Coro::current->{request} //= shift->simple;
}

1;