package R::Model::Metafieldset;
# база данных

use common::sense;
use R::App;
use R::Model;
use R::Model::Index;
use R::Model::Indexref;
use R::Model::Fieldset;

# все меты по именам
our %META;

# строковые типы
our $string_types = {"varchar"=>1, "char"=>1, "text"=>1, "mediumtext"=>1, "tinytext"=>1, "lagetext"=>1};

our $type_to_input = {"mediumtext" => "area", "lagetext"=>"area"};

our $default_checks = {};


has qw/name base bases connect database fields cls ordering string_types type_to_input charset input_default input inputset checks engine pk_type types_path types cache model paging lang touch softerase addtestdata/;


# конструктор
sub DEFAULT_new {
	my $cls = shift;
	
	my $self = bless {
		name => "R",		# name - мета-имя. Оно используется для создания классов
		base => "model",	# каталог с моделями в проекте
		bases => {},		# какие модели уже загружены: "catalog"=>1
		connect => undef,	# коннект к базе

		database => undef,	# имя базы
		fieldset => {},		# имя => таблица
		fields => [],		# [таблица, таблица...] - порядок таблиц
		cls => {},			# class => таблица
		ordering => undef,	# массив или строка через пробел или "," с сортировкой по умолчанию для всех таблиц, например, id или -id

		string_types => $string_types,		# строковые типы
		type_to_input => $type_to_input,	# тип к вводу

		# если charset не соврпадает с началом collate до _, то записывается через ":": ucs1:wsc12
		charset => "utf8_unicode_ci",

		input_default => "line",	# дефолтный input для всех столбцов - вычислимых так же
		input => "select",			# дефолтный input для ref
		inputset => "checkboxes",	# дефолтный input для множественных полей
		
		checks => { %$default_checks },	# дефолтные валидаторы

		engine => "InnoDB",
		pk_type => "int unsigned",

		types_path => "lib/R/Model/Type",	# пути к директориям с типами через ":"


		# types - типы столбцов: имя=>класс Perl. Пример: {unixtime => "R::Model::Type::Unixtime", ...}
		# при установке: (ref $x? тип->new($x): $x)->toCol()
		# при возврате: тип->fromCol($x)		
		types => undef,
		cache => undef,

		#column_type

		
		model => undef,	# объект созданного специального класса (R::Model) для меты 

		# размер страницы
		paging => 6,

		lang => "Rus", # дефолтный язык для match-индексов
		touch => 0, # touch - булево значение, означает, что во всех таблицах будут поля now и upd
		softerase => 0, # softerase - 1 - мягкое удаление во всех таблицах
		addtestdata => 0, # addtestdata - нужно ли при синхронизации добавлять тестовые данные
		@_,
	}, ref $cls || $cls;
	
	# теперь app->meta бцдет работать при инициализации
	$app->{$self->{_APP_KEY}} = $self if exists $self->{_APP_KEY};
	
	my $name = $self->{name};
	die "метафилдсет `$name` уже существует" if exists $META{$name};
	$META{$name}++;
	
	$self->{base} = $app->project->file($self->{base})->relative->path;
	
	$self->{database} //= $self->connect->basename;
	
	$self->{model} //= R::Model->new(meta => $self);
	
	if(length $self->{types_path}) {
		my $get_types = {};
		my @path = split /:/, $self->{types_path};
		$app->project->files(@path)->find(sub {
			if(/([^\/]+)\.pm$/) {
				my $name = lc $1;
				my $path = $_;
				$path =~ s!^lib/!!;
				$path =~ s!^!./! if $path !~ m!/!;
				$get_types->{$name} = [$path, $app->file($_)->get_class];
			}
			undef
		});
		$self->{types} = $get_types;
	}

	$self->load;
	
	$self
}


# закрывает метаописание
sub close {
	my ($self) = @_;
	
	delete $META{$self->name};
	$self->connect->close;
	$self->connect->connect if $self->connect == $app->connect;
	#$app->{modelMetafieldset} = undef if $app->{modelMetafieldset} == $self;
	$self
}

# возвращает филдсеты
sub fieldsets {
	my ($self) = @_;
	wantarray? @{$self->{fields}}: $self->{fields};
}

# создаёт и возвращает новую таблицу
sub fieldset {
	my ($self, $name, $cls_row_name) = @_;
	$self->{fieldset}{$name} // R::Model::Fieldset->new($name, $self, $cls_row_name);
}

our %Meta = $app->perl->set(qw/name charset paging ordering pk_type engine input_default input inputset base types_path lang addtestdata touch softerase/);

# устанавливает метаинформацию для базы
sub meta {
	my ($self, %args) = @_;
	while(my($k, $v) = each %args) {
		#next if $k eq "cache";
		die "нет опции `$k` в meta" unless exists $Meta{$k};
		$self->{$k} = $v;
	}
	$self
}

# проверяет на существование филдсет по имени и возвращает его, если тот есть
sub exists {
	my ($self, $name) = @_;
	$self->{fieldset}{$name}
}

# существует ли БД
sub existsDatabase {
	my ($self) = @_;
	exists $self->connect->base_info->{$self->{database}};
}


# проверяет таблицу на существование
sub existsTab {
	my ($self, $tab) = @_;
	!!$self->connect->info->{$tab}
}

# проверяет колумн на существование
sub existsCol {
	my ($self, $tab) = @_;
	
	$tab =~ /\./; my $col = $'; $tab = $`;
	
	!!$self->connect->info->{$tab}{$col};
}

our %inspect = $app->perl->set(qw/sync syncing synced setup setuped/);

# проверяет, что есть такой обработчик
sub _inspect {
	my ($self, $name) = @_;
	die "нет зарегистрированного события $name" if !exists $inspect{$name};
}

# устанавливает обработчик события на мету
sub on {
	my ($self, $name, $cb) = @_;
	$self->_inspect($name);
	($self->{signal}{$name} //= $app->signal->new)->cb($cb);
	$self
}

# возбуждает событие на мете
sub fire {
	my ($self, $name) = splice @_, 0, 2;
	$self->_inspect($name);
	my $signal = $self->{signal}{$name};
	$signal->send(@_) if $signal;
	$self
}

# проверяет - установлено ли событие
sub on_exists {
	my ($self, $name) = @_;
	if(ref $name) {	# одно из должно быть усановлено
		for my $n (@$name) {
			$self->_inspect($n);
			return 1 if exists $self->{signal}{$n}
		}
		undef
	}
	else {
		$self->_inspect($name);
		$self->{signal}{$name}
	}
}

# синоним
*load = \&load_all_models;

# загружает все модели
sub load_all_models {
	my ($self, $base) = @_;
	
	# подгружаем дефолтные модели, например: модель миграций
	$self->default_fieldsets if !$self->{default_fieldsets};
	$self->{default_fieldsets} = 1;
	
	$base //= $self->{base};
    
	# если нечего загружать
	return $self if !defined $base;
	
	# модели должны загружаться только раз
	return $self if exists $self->{bases}{$base};
	
	$self->{bases}{$base} = 1;
	
	# копируем, т.к. в $fieldsets будут добавляться таблицы порождённые m2m в начало и указатель for-а будет сбиваться
	my @fieldsets;
	
	$app->file($base)->find("**.pm")->sort->then(sub {
		
		require $_->dot->path;
		
		push @fieldsets, lcfirst $_->nik;
		
		0
	}) if $base;

	$self->setup(\@fieldsets);
	
	$self
}


# запускает сетапы на моделях
sub setup {
	my ($self, $fieldsets) = @_;
	
	my $flag = $self->{flag_load};	# undef будет только в 1-й load, а в последующих - нет
	$self->{flag_load} = 1;
	#my $fields = $self->{fields};
	
	local $self->{on_end_setup} = $app->signal->new;
	
	$self->fire("setup", $fieldsets) if !$flag;
	
	# вызываем сетапы на моделях
	$self->fieldset($_) for @$fieldsets;
	
	$self->{on_end_setup}->send;
	
	$self->fire("setuped", $fieldsets) if !$flag;
	
	$self->{flag_load} = $flag;
	
	$self
}

# вызывает колбэк в конце сетапа
sub on_end_setup {
	my ($self, $fn) = @_;
	
	# добавляем к сигналу
	if(my $sig = $self->{on_end_setup}) {
		$sig->cb($fn);
		return $self;
	}
	
	# если сигнала нет - выполняем сразу
	$fn->();
	
	$self
}

# загружает модели миграции
sub migrateload {
	my ($self) = @_;
	
	# FIXME: переделать порядок филдсетов миграций с sort keys на загружаемые
	R::TODO();
	
	$self->default_fieldsets;
	
	my @fieldsets;
	my $migrate = $self->{name};
	
	my @keys = sort keys %{"${migrate}::Row::"};
	
	for my $i (@keys) {
		my $cls = $i;
		$cls =~ s/::$//;
		my $name = lcfirst $cls;
		#msg1 $cls, $name;
		unshift @fieldsets, $self->fieldset($name, $cls);
	}
	
	$self->setup(\@fieldsets);
	
	$self
}

# создаёт филдсет миграций
sub default_fieldsets {
	my ($self) = @_;
	
	$self->create("_migrate", sub {	shift->
		pk("varchar(255)")->remark("применённые миграции")->
		meta(
			remark => "список применённых миграций"
		);
	});

	$self
}

# запускает setup, если такого филдсета нет
# филдсет в сетап передаётся и параметром и в $_
sub create {
	my ($self, $name, $setup) = @_;
	
	return $self if $self->exists($name);
	
	$self->fieldset($name)->setup($setup);
	
	$self
}

# возвращает текстовый тип для базы по размеру
sub getUIntType {
	my ($self, $max) = @_;
	
	return $max if $max =~ /^(?:tiny|small|medium|big)?int(?:\s+unsigned)?$/i;
	
	$max = $app->perl->size($max);
	
	if($max < 1 << 8) {
		"tinyint unsigned"
	}
	elsif($max < 1 << 16) {
		"smallint unsigned"
	}
	elsif($max < 1 << 24) {
		"mediumint unsigned"
	}
	elsif($max < 1 << 32) {
		"int unsigned"
	}
	elsif($max < 1 << 64) {
		"bigint unsigned"
	}
	else {
		die "слишком большое значение $max: максимум 1024G-1"
	}
}


# возвращает текстовый тип для базы по размеру
sub getTextType {
	my ($self, $length) = @_;
	
	return "varchar(255)" if $self->{engine} eq "memory";
	
	return $length if $length =~ /^(?:tiny|medium|long)?text$/i;
	
	$length = $app->perl->size($length);
	
	if($length < 1 << 8) {
		"tinytext"
	}
	elsif($length < 1 << 16) {
		"text"
	}
	elsif($length < 1 << 24) {
		"mediumtext"
	}
	elsif($length < 1 << 32) {
		"longtext"
	}
	else {
		die "слишком большое значение $length: максимум 4G-1"
	}
}

# синхронизация используемая в скриптах - запрашивает разрешение
sub sync_for_script {
	my ($self) = @_;
	
	$self->presync;
	
	my $sql_before = $self->{sql_before};
	my $sql_main = $self->{sql_main};
	my $sql_after = $self->{sql_after};
	
	# спрашиваем: применить ли указанные действия?
	msg "Будут выполнены следущие действия:\n";
	my $c = $self->connect;
	$app->log->info(":empty", $c->color($_)) for @$sql_before, @$sql_main, @$sql_after;
	return $self if !$app->tty->confirm("Выполнить синхронизацию?");
	
	$self->syncing->synced;
	
	$app->log->info( ":white space", "[", ":green", "Ok", ":white", "]" );
}

# синхронизирует базу
sub sync {
	my ($self) = @_;
	$self->presync->syncing->synced;
}

# формирует запросы для синхронизации базы
sub presync {
	my ($self, $yes, $_test) = @_;
	
	$self->fire("sync");
	
	my $c = $self->connect;

	# создаём базу, если её нет
	$c->do($self->create_database), $c->use($self->{database}) if !$self->existsDatabase;

	$c->use($self->{database}) if $self->{database} ne $c->{basename};
	
	
	# подгружаем все модели
	#$self->load_all_models;
	
	# сбрасываем кеши
	$c->clean;
	
	my $info = $c->info;
	my $fk_info = $c->fk_info_backward;
	
	# синхронизация таблиц
	my $sql_before = $self->{sql_before} = [];
	my $sql_main = $self->{sql_main} = [];
	my $sql_after = $self->{sql_after} = [];

	
	for my $fieldset (@{$self->{fields}}) {
		$fieldset->{sync} = $fieldset->{synced} = undef;
	}
	
	#my $fieldsets = $self->{fieldset};
	my %tab;
	
	for my $fieldset (@{$self->{fields}}) {
		$fieldset->presync;
		$tab{lc $fieldset->{tab}} = 1;
	}
	
	# удаление таблицы
	my @sql;
	while(my($key, $val) = each %$info) {
		if(!exists $tab{lc $key}) {
			push @sql, "DROP TABLE " . $c->word($key);
			# удаление внешних ключей
			while(my($k, $idx) = each %{$fk_info->{$key}}) {
				my $sql = R::Model::Indexref::drop($self, $idx->{tab}, $k);
				push @$sql_before, $sql;
				#exit;
			}
		}
	}

	unshift @$sql_main, @sql;
	
	#push @$sql_before, "SET FOREIGN_KEY_CHECKS = 0";
	#push @$sql_after, "SET FOREIGN_KEY_CHECKS = 1";
	
	$self
}

# синхронизация после presync
sub syncing {
	my ($self) = @_;
	
	my $sql_before = $self->{sql_before};
	my $sql_main = $self->{sql_main};
	my $sql_after = $self->{sql_after};
	
	my $c = $self->connect;
	
	# удаление внешних ключей, как задала синхронизация таблиц
	for my $sql (@$sql_before) {
		$c->do($sql);
	}
	
	# синхронизайия таблиц
	for my $sql (@$sql_main) {
		$c->do($sql);
	}
	
	# создание fk-ключей
	for my $sql (@$sql_after) {
		$c->do($sql);
	}

	$c->clean;

	$self->fire("syncing");
	
	$self
}
	

# выполняем функции синхронизации в установленном порядке 
sub synced {
	my ($self, $_test) = @_;
	
	die "ненужный параметр test в synced" if @_>1;
	
	for my $fieldset (@{$self->{fields}}) {

		$fieldset->sync_data;
		
		delete $fieldset->{sync};
	}
	
	$self->fire("synced");
	
	
	$self
}

# sql для опций БД
sub sql {
	my ($self) = @_;
	my ($charset, $collate) = $self->connect->split_charset($self->{charset});
	" DEFAULT CHARACTER SET $charset COLLATE $collate";
}

# sql для alter database
sub alter {
	my ($self) = @_;
	my $c = $self->connect;
	"ALTER DATABASE " . $c->word($self->{database}) . $self->sql;
}

# sql для создания БД
sub create_database {
	my ($self) = @_;
	my $c = $self->connect;
	"CREATE DATABASE ". $c->word($self->{database}) . $self->sql;
}

# sql для создания БД
sub drop_database {
	my ($self) = @_;
	my $c = $self->connect;
	"DROP DATABASE ". $c->word($self->{database});
}

# удаляет базу
sub dropDatabase {
	my ($self) = @_;
	$self->connect->do($self->drop_database);
	$self->connect->clean;
	$self
}

# создаёт базу
sub createDatabase {
	my ($self) = @_;
	$self->connect->do($self->create_database);
	$self
}

# # если такой БД нет - создаёт и переключает на неё (???)
# sub database {
	# my ($self, $name) = @_;
	# $self->{database} = $name;
	# $self
# }

# удаляет данные из всех таблиц
sub truncate {
	my ($self) = @_;
	
	$self->connect->do("SET foreign_key_checks=0");
	my $fk_defer = R::guard { $self->connect->do("SET foreign_key_checks=1") };
	
	for my $fieldset (@{$self->{fields}}) {
		$fieldset->truncate;
	}
	
	undef $fk_defer;
	
	$self->connect->clean;
	
	$self
}

# удаляет таблицы базы
sub drop {
	my ($self) = @_;
	
	#$self->load_all_models;
	my $info = $self->connect->info;
	
	$self->connect->do("SET foreign_key_checks=0");
	my $fk_defer = R::guard { $self->connect->do("SET foreign_key_checks=1") };
	
	for my $fieldset (@{$self->{fields}}) {
		$fieldset->drop if delete $info->{$fieldset->{tab}};
	}
	
	for my $tab (keys %$info) {
		#$self->fieldset($tab)->meta(tab=>$tab)->drop;
		R::Model::Fieldset::drop($self, $tab);
	}
	
	undef $fk_defer;
	
	$self->connect->clean;
	
	$self
}


# сгружает в указанную директорию структуру базы в виде моделей
# Создаётся папка с именем базы
sub down {
	my ($self, $outdir) = @_;
	
	local ($a, $b);
	
	my $c = $self->connect;
	
	$app->file($outdir //= $c->basename)->mkdir->rmdown;
	
	my $tab_info = $c->tab_info;
	my $info = $c->info;
	my $index_info = $c->index_info;
	
	while(my ($tab, $cols) = each %$info) {
		
		my $name = $tab;  #$c->uc( $tab );
		
		my $file = $app->file("$outdir/$name.pm")->open(">");
		
		msg $tab;
		
		print $file "package R::Row::$name;
# модель $tab

use base R::Model::Row;

use common::sense;
use R::App;


# вызывается для создания структуры базы
sub setup {
	my (\$fields) = \@_;
	\$fields->pk(undef)->
	
	
";
	
		$self->{col_to_name} = {};
	
		for my $col (sort { $a->{ordinal_position} <=> $b->{ordinal_position} } values %$cols) {
			print $file "\t" . $self->declare_col($tab, $col) . "->\n";
		}
		
		my $idx_info = $index_info->{$tab};
		
		print $file "\n" if %$idx_info;
		
		my $pk_index = $idx_info->{"PRIMARY"};
		print $file "\tprimary_key('" . join(",", map {$_->{col}} @$pk_index) . "')->\n" if @$pk_index > 1;
		
		while(my ($idx, $cols) = each %$idx_info) {
			next if $idx eq 'PRIMARY';
			print $file "\t" . ($cols->[0]{unique}? "unique": "index") . "('" . join(", ", map {$self->{col_to_name}{$_->{col}}} @$cols) . "' => '$idx')->\n";
		}
		
		my $declare_tab = $self->declare_tab($tab_info->{$tab});
		print $file "\n" . $declare_tab . "->\n" if $declare_tab;
		
		$self->{col_to_name} = {};
		
		print $file "

	end;
	
}

# тестовые данные
sub testdata {
}

1;
";
		close $file;
	}

	
	$self
}

# возвращает код представления модели в setup
sub declare_col {
	my ($self, $tab, $col) = @_;
	
	my $name = $col->{column_name};
	
	my $pk_index = $self->connect->index_info->{$tab}{"PRIMARY"};
	my $type = $col->{type};
	$type =~ s/'/\\'/g;
	
	my @col;
	if(@$pk_index==1 && $col->{pk}) {
		$self->{col_to_name}{$name} = "id";
		
		my $real_name = $name ne "id" ? " => '$name'": "";
		
		push @col, "pk('$type'$real_name)";
	}
	else {
		if($app->can($name) || R::Model::Fieldset->can($name) || R::Model::Row->can($name) || R::Model::Rowset->can($name)) {
			my $fname = "a_$name";
			$self->{col_to_name}{$name} = $fname;
			push @col, "col('$fname' => '$type' => '$name')";
		} else {
			$self->{col_to_name}{$name} = $name;
			my $real_name = $name !~ /^[a-z_0-9]+$/ ? " => '$name'": "";
			push @col, "col('$name' => '$type'$real_name)";
		}
	}
	
	push @col, "->autoincrement" if $col->{autoincrement};
	push @col, "->null" if $col->{null};
	
	push @col, ($col->{default} =~ /^CURRENT_TIMESTAMP$/i? "->default_raw": "->default") . "(". $self->connect->quote($col->{default}) .")" if defined $col->{default};
	my $set;
	push @col, "->charset('$col->{charset}')" if $col->{charset};
	
	push @col, "->extra('$col->{extra}')" if $col->{extra} ne "";
	
	push @col, "->remark(" . $self->connect->quote($col->{remark}) . ")" if $col->{remark};
	
	join "", @col;
}


# декларирует информацию о таблице
sub declare_tab {
	my ($self, $meta) = @_;
	local ($`, $');
	my @meta;
	
	push @meta, "\t\ttab => '$meta->{name}',\n" if $meta->{name};
	push @meta, "\t\tcompute => 1,\t# table_type=$meta->{type}\n" if $meta->{type} !~ /TABLE/i;
	push @meta, "\t\tengine => '$meta->{engine}',\n" if $meta->{engine};
	push @meta, "\t\tcharset => '$meta->{charset}',\n" if $meta->{charset};
	push @meta, "\t\tremark => '$meta->{remark}',\n" if $meta->{remark};
	push @meta, "\t\toptions => '$meta->{options}',\n" if $meta->{options};
	
	return "" if !@meta;
	unshift @meta, "\tmeta(\n";
	push @meta, "\t)";
	join "", @meta;
}


#@@category удаление и горячая перезагрузка

# удаление
sub delete {
	my ($self) = @_;
	
	my @copy = @{$self->{fields}};
	for my $fieldset (@copy) {
		$fieldset->delete;
	}
	
	$self
}

sub HOTSWAP_APP_REMOVE {
	my ($self, $app_key) = @_;
	$self->delete;
}



1;