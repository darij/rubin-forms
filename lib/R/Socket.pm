package R::Socket;
# функции для работы с сокетами

use common::sense;
use R::App;

use Socket qw/IPPROTO_TCP AF_INET SOCK_STREAM SOL_SOCKET SO_REUSEADDR SO_KEEPALIVE INADDR_ANY SOMAXCONN PF_UNIX SOMAXCONN sockaddr_in INADDR_ANY PF_UNIX PF_INET inet_aton/;
use Socket::Linux qw//;
use Symbol qw/gensym ungensym/;



# конструктор
sub new {
	my $cls = shift;
	bless {@_}, ref $cls || $cls;
}

# открывает и возвращает сокет
sub open {
	my ($self, $_port) = @_;
	my $sd = gensym;
	
	# если указан порт, то ожидается tcp-сокет
	if($_port !~ s!^unix:/*!!) {
		socket $sd, AF_INET, SOCK_STREAM, getprotobyname("tcp") or die "socket: $!\n";
		# захватываем сокет, если он занят другим процессом
		setsockopt $sd, SOL_SOCKET, SO_REUSEADDR, pack("l", 1) or die "setsockopt reuseaddr: $!\n"; 
		# проверять что сокет существует и закрывать его если нет
		setsockopt $sd, SOL_SOCKET, SO_KEEPALIVE, pack("l", 1) or die "setsockopt KEEPALIVE: $!\n"; 
		# 1 minute keep-alive
		setsockopt($sd, IPPROTO_TCP, Socket::Linux::TCP_KEEPIDLE(), pack("l", 60)) or die "setsockopt KEEPIDLE: $!\n" if $Socket::{Linux::}{'TCP_KEEPIDLE'}{CODE};
		# 3 seconds for each attempt
		setsockopt($sd, IPPROTO_TCP, Socket::Linux::TCP_KEEPINTVL(), pack("l", 3)) or die "setsockopt KEEPINTVL: $!\n" if $Socket::{Linux::}{'TCP_KEEPINTVL'}{CODE};
		# 3 times to try
		setsockopt($sd, IPPROTO_TCP, Socket::Linux::TCP_KEEPCNT(), pack("l", 3)) or die "setsockopt KEEPCNT: $!\n" if $Socket::{Linux::}{'TCP_KEEPCNT'}{CODE};
		
		
		unless( bind $sd, sockaddr_in($_port, INADDR_ANY) ) {
			if($! == 112) {
				sleep 2;
				bind $sd, sockaddr_in($_port, INADDR_ANY) or die "$$ bind: (".int($!).") $!\n";
			} else { die "$$ bind: (".int($!).") $!\n" }
		}
		listen $sd, SOMAXCONN or die "listen: $!\n";
	} else {
		socket $sd, PF_UNIX, SOCK_STREAM, 0 or die "socket: $!\n";
		unlink $_port;
		bind $sd, Socket::sockaddr_un($_port) or die "bind: $!\n";
		#msg1 $sd, sockaddr_un($_port), $_port;
		listen $sd, SOMAXCONN  or die "listen: $!\n";
	}
	
	$sd
}

# коннектится к клиенту. Возвращает сокет
sub connect {
	my ($self, $port) = @_;
	my $ns = gensym;
	
	if( $port =~ m!^unix:/*! ) {
		my $remote = $';
		socket($ns, PF_UNIX, SOCK_STREAM, 0)		|| die "unix socket: $!";
		connect($ns, sockaddr_un($remote))			|| die "unix connect($remote): $!";
	}
	else {
		
		my $remote = "127.0.0.1";
		if($port =~ /\D/) {
			($remote, $port) = split /:/, $port;
		}
		
		# получить порт службы по её имени
		#if ($port =~ /\D/) { $port = getservbyname($port, "tcp") }
		die "No port" unless $port;
		my $iaddr   = inet_aton($remote)			|| die "no host: $remote";
		my $paddr   = sockaddr_in($port, $iaddr);
		my $proto   = getprotobyname("tcp");

		socket($ns, PF_INET, SOCK_STREAM, $proto)	|| die "socket: $!";
		connect($ns, $paddr)              			|| die "connect: $!";
	}
	
	$ns
}

1;