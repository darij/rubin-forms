package R::Locale;
# локализация

use common::sense;
use R::App qw/:min/;

# конструктор
sub new {
	my $cls = shift;
	bless {
		lang => "<no>",	# язык
		msg => {},		# переводы: msgid => msgstr
		ctx => {},		# переводы с контекстом: ctx => msgid => msgstr
		n => {},		# переводы: msgid => msgid_plural => msgstr[]
		ctxn => {},		# переводы с контекстом: ctx => msgid => msgid_plural => msgstr[]
		plural => undef, # функция индекса для определения формы склонения
		@_
	}, ref $cls || $cls;
}


#@@category depracate

### константы
sub PERMISSION_DENIED { "нет прав" }


### функции приведения

# возвращает аббревиатуру
*plural = \&abbr;
sub abbr {
	my ($self, $n, $x, $y, $z) = @_;
	my $day100 = $n % 100;
	my $day10 = 10<=$day100 && $day100<=20? 10: $n % 10;
	$day10==0? $z: $day10==1? $x: $day10<=4? $y: $z;
}

# для дней
sub days {
	my ($self, $days) = @_;
	$self->abbr($days, "день", "дня", "дней")
}

# для месяцев
sub months {
	my ($self, $days) = @_;
	$self->abbr($days, "месяц", "месяца", "месяцев")
}

# для лет
sub years {
	my ($self, $days) = @_;
	$self->abbr($days, "год", "года", "лет")
}

# месяц c 1-цы. 5-го мая
my @MONTH = qw/января февраля марта апреля мая июня июля августа сентября октября ноября декабря/;
sub month {
	my ($self, $month) = @_;
	$MONTH[$month-1]
}

# 3 1-х символа
sub mon {
	my ($self, $month) = @_;
	substr $MONTH[$month-1], 0, 3
}

my @MONTH_NAME = qw/январь февраль март апрель май июнь июль август сентябрь октябрь ноябрь декабрь/;
sub month_name {
	my ($self, $month) = @_;
	$MONTH_NAME[$month-1]
}


# месяц c 1-цы. 5-го мая
my @WEEK = qw/понедельник вторник среда четверг пятница суббота воскресенье/;
my @WEEK_ABBR = qw/Пн Вт Ср Чт Пт Сб Вс/;
sub weekdayabbr {
	my ($self, $day) = @_;
	@WEEK_ABBR[$day-1]
}


# часов
sub hours {
	my ($self, $abbr) = @_;
	$self->abbr($abbr, "час", "часа", "часов")
}

# минут
sub minutes {
	my ($self, $abbr) = @_;
	$self->abbr($abbr, "минута", "минуты", "минут")
}

# секунд
sub seconds {
	my ($self, $abbr) = @_;
	$self->abbr($abbr, "секунда", "секунды", "секунд")
}


### функции трансляции на иные языки

# # устанавливает язык для перевода
# sub lang {
	# my ($self, $lang) = @_;
	# $self->{lang} = $lang;
	# $self
# }

# # транслятор
# sub translate {
	# my ($self, $msgid, $lang) = @_;
	# $lang //= $self->{lang};
	# ($self->{msg}{$lang} //= $self->load($lang))->{$msgid} // $msgid
# }

# загружает po-файл
my $re_str = qr/"(\\"|[^"\n])*"/n;
my $re_str_nl = qr/$re_str[ \t\r]*(\n|$)/;
my $re_str_many = qr/($re_str_nl)+/n;
sub load {
	my ($self) = @_;
	
	my $x = $app->project->file("i18n/$self->{lang}.po")->read;
	
	my $ctx;
	my $id;
	my $arr;
	while($x =~ m!^(\w+(?:\[\d+\])?)[\t ]+($re_str_many)!gm) {
		my $key = $1;
		my $val = join "", map { $app->magnitudeLiteral->unstring($_) } split /[ \t\r]*\n/, $2;
		
		$val = undef if $val !~ /\S/;
		
		given($key) {
			$ctx = $val when "msgctxt";
			$id = $val when "msgid";
			when("msgstr") {
				if($ctx) { $self->{ctx}->{$ctx}->{$id} = $val }
				else { $self->{msg}->{$id} = $val }
				undef $id; undef $ctx;
			}
			when("msgid_plural") {
				if($ctx) { $arr = $self->{ctxn}->{$ctx}->{$id}->{$val} = [] }
				else { $arr = $self->{n}->{$id}->{$val} = [] }
				undef $id; undef $ctx;
			}
			default {	# msgstr[0]
				die "странное: $key" if $key !~ /^msgstr\[\d+\]$/;
				push @$arr, $val;
			}
		}
	}
	
	my $h = $self->{headers} = $app->mime->httpheaders->from(delete $self->{msg}->{""});
	my $s = $h->{"Plural-Forms"};
	$s =~ s/nplurals=\d+; plural=//;
	$s =~ s/\bn\b/\$n/g;
	$s || die "нет Plural-Forms";
	$s = "sub { my (\$n)=\@_; $s }";
	$self->{plural} = eval $s;
	die if $@;
	
	$self
}

# проверяет, что на диске есть 
my $mo;
sub exists {
	my ($self, $lang) = @_;
	$mo //= +{ $app->project->file("i18n")->find("**.po")->map(sub { $_->name => 1 }) };
	exists $mo->{$lang}
}

# возвращает кешированную локаль
my $loc = {};
sub factory {
	my ($self, $lang) = @_;
	return undef if !$self->exists($lang);
	$loc->{$lang} //= $app->locale(lang=>$lang)->load
}

1;