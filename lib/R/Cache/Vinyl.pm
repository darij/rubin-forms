package R::Cache::Vinyl;
# кэш удаляет на каждый N-й запрос просроченные сессии

use common::sense;
use R::App;


# конструктор
sub new {
	my $cls = shift;
	my $self = bless {
		lifetime => 3600*24*3,	# время жизни ключа по дефолту в секундах
		cache => {},		# хэш с ключами
		times => {},		# хэш с временем рождения ключей
		cleaninterval => 3600/2,	# каждые пол-часа - удаляем просроченные ключи
		@_
	}, ref $cls || $cls;
	$self->{cleantime} = time() + $self->{cleaninterval};
	$self
}

# устанавливает
sub set {
	my ($self, $k, $v, $lifetime) = @_;
	$self->{cache}{$k} = $v;
	$self->{times}{$k} = time() + ($lifetime // $self->{lifetime});
	$self
}

# возвращает
sub get {
	my ($self, $k, $lifetime) = @_;
	my $time = time();
	if($self->{cleantime} < $time) {
		$self->{cleantime} = $time + $self->{cleaninterval};
		my $times = $self->{times};
		my @del;
		for(keys %$times) {
			$self->del($_) if $times->{$_} < $time;
		}
	}
	$self->del($k) if exists $self->{times}{$k} and $self->{times}{$k} < $time;
	$self->{cache}{$k}
}

# удаляет
sub del {
	my ($self, $k) = @_;
	delete $self->{cache}{$k};
	delete $self->{times}{$k};
	$self
}

# квантор существования
sub exists {
	my ($self, $k) = @_;
	$self->del($k) if exists $self->{times}{$k} and $self->{times}{$k} < time();
	exists $self->{cache}{$k};
}

# # вернуть кэш, а если его нет - выполнить функцию и установить
# # $lifetime в секундах
# sub ret {
	# my ($self, $key, $lifetime, $sub) = @_;
	
	# return $self->{cache}{$k} if $self->exists($key);
	
	# $sub = $lifetime, $lifetime = $self->{lifetime} if @_==3;
	
	# $self->set($key => $sub->($key, $self) => $lifetime)
# }


1;