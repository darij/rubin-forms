package R::Tty;
# утилиты для работы с терминалом

use common::sense;
use R::App qw/:min/;
use Term::ANSIColor qw//;
use Term::Screen qw//;

my $scr;

# конструктор
sub new {
	my ($cls) = @_;
	bless {}, ref $cls || $cls;
}

# спрашивает пользователя через stdin
sub confirm {
	my ($self, $confirm) = @_;
	
	die [506, [], $confirm] if exists $Coro::current->{request};
	
	my $yes = "";
	do { print("$confirm (yes/no) "); } while ($yes = <STDIN>) !~ /^yes|no$/;
	return scalar $yes =~ /yes/;
}

# вводит строку
sub input {
	my ($self, $prompt) = @_;
    
	die [506, [], $prompt] if exists $Coro::current->{request};
	
    print "$prompt: "; my $i = <STDIN>;
    chop $i;
	$_[2] = $i;
    $i
}

# вводит строку или дефолт
sub input_or {
	my ($self, $prompt, $default) = @_;
    
	die [506, [], "$prompt [$default]"] if exists $Coro::current->{request};
	
    print "$prompt [$default]: "; my $i = <STDIN>;
    $i =~ s!^\s*(.*)\s*$!$1!;
	$i = $default if !length $i;
	$_[3] = $i;
    $i
}

# перемещает курсор в $x, $y
sub locate {
	my ($self, $x, $y) = @_;
    
    print STDOUT $self->at($x, $y);
    
	$self
}

# предлагает пользователю выбрать вариант через stdin
sub select {
	my ($self, $data, $confirm) = @_;
	
	$confirm //= "Выберите вариант";
	
	die [506, [], join "\n", $confirm, @$data ] if exists $Coro::current->{request};
	
	$data = [split /\n/, $data] if !ref $data;
	
	my $i = 0;
	for my $d (@$data) {
		$i++;
		print "$i) $d\n";
	}
	
	my $yes;
	do { print("$confirm (1..$i) "); } while ($yes = int(<STDIN>)) < 1 or $yes > $i;
	return wantarray? ($yes, $data->[$yes-1]): $yes;
}

# устанавливает кодировку всем потокам ввода-вывода
sub mode {
	my ($self, $encode) = @_;
	
	binmode STDIN, $encode;
	binmode STDOUT, $encode;
	binmode STDERR, $encode;
	
	$self
}

# устанавливает кодировку raw потокам ввода-вывода
sub raw {
	my ($self) = @_;
	
	$self->mode(":raw");
	
	$self
}


# Loading… [][][][][][][][][][] 0%
# Loading… █[][][][][][][][][] 10%
# Loading… ██[][][][][][][][] 20%
# Loading… ███[][][][][][][] 30%
# Loading… ████[][][][][][] 40%
# Loading… █████[][][][][] 50%
# показывает прогресс-бар
# ->update()
# ->message()
# fh => \*STDOUT - куда выводить
#@returns Term::ProgressBar
sub progress {
	my ($self, $to, $name, $remove, $update) = @_;
		
	require "Term/ProgressBar.pm";
	my $progress = Term::ProgressBar->new({name => $name, count => $to, remove => $remove});
	$progress->update($update) if defined $update;
	$progress
}

# отображает на терминал и запускает команду
sub run {
	my ($self, $run, $prefix) = @_;
	
	#$run =~ s/[\n\r]/\\\n/g;
	
	print "$prefix$run\n";
	
	# TODO: :utf8
	use Symbol;
	my $f = gensym;
	open $f, "-|", $run or die "run: $!";
	binmode $f, ":utf8";
	select $f; $| = 1;
	select STDOUT; $| = 1;
	my $c; my $output = "";
	while(defined($c = getc $f)) {
		print $c;
		$output .= $c;
	}
	close $f;
	$output
}

# колоризирует строку
sub colored {
    my ($self, $s, $color) = @_;
    Term::ANSIColor::colored($s, $color);
}

# позиция курсора
sub pos {
	my ($self) = @_;
	require Term::ReadKey;
	# Term::ReadKey::ReadMode(3);
	# print STDOUT "\e[6n";
	# local $/ = "R";
	# my $x=<STDIN>;
	# Term::ReadKey::ReadMode(0);
	# my($n, $m)=$x=~m/(\d+)\;(\d+)/;
	# $n = rows, $m = cols
	
	my ($cols, $rows, $width_px, $height_px) = Term::ReadKey::GetTerminalSize();
	
	die "Terminal is not get position!" if !defined $cols;
	return wantarray? ($cols, $rows): "$cols:$rows";
}

# строк в терминале
sub rows {
	my ($self) = @_;
	my ($n, $m) = $self->pos;
	$m
}

# столбцов в терминале
sub cols {
	my ($self) = @_;
	my ($n, $m) = $self->pos;
	$n
}

sub home {
	return "\e[H";              # Put the cursor on the first line
}

sub kks {
	return "\e[J";              # Clear from cursor to end of screen
}

sub clear {
	return  "\e[H\e[J";          # Clear entire screen (just a combination of the above)
}

sub kk {
	return "\e[K";              # Clear to end of current line (as stated previously)
}

sub reset {
	return  "\e[m";              # Turn off character attributes (eg. colors)
}

sub color {
	my ($self, $N) = @_;
	sprintf "\e[%dm", $N;        # Set color to $N (for values of 30-37, or 100-107)
}

sub at {
	my ($self, $C, $R) = @_;
	sprintf "\e[%d;%dH", $R, $C; # Put cursor at row $R, column $C (good for "drawing")
}

sub store_pos {
	"\e[s"
}

sub restore_pos {
	"\e[u"
}

1;
