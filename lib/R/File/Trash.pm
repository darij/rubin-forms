package R::File::Trash;
# корзина
# помещает файлы в корзину (default: /var/trash) и достаёт из неё

use common::sense;
use R::App;

# свойства
has qw/not_found/;

# конструктор
sub new {
	my $cls = shift;
	bless {
		trash => "/var/trash/",
		not_found => $app->signal->new,
		@_
	}, ref $cls || $cls;
}

# корзина
sub trash {
	my ($self) = @_;
	$self->{_trash} //= $app->file($self->{trash})->as_dir->mkpath
}

# переносит файл или файлы в корзину
sub to {
	my ($self, $f) = @_;
	$app->file($f)->mv($self->trash);
	$self
}

# переносит из корзины, если в корзине нет ни одного из запрошенных файлов - вызывает колбэк и возвращает false
sub from {
	my ($self, $f, $cb) = @_;
	$f = $app->file($f);
	
	my $in_trash = $f->frontdir($self->trash);
	
	if($in_trash->existing->length == 0) {
		$f->mkpath;	# создаёт пути к файлам
		$cb->($f, $in_trash) if $cb;
		$self->not_found->send($f, $in_trash);
		return 0;
	}
	
	my $i = 0;
	for my $x ($in_trash->slices) {
		if($x->exists) {
			$x->mv($f->part($i));
		}
	} continue {$i++}
	
	return 1;
}

1;