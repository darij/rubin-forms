package R::File::Watch;
# объект-обёртка для результатов функции app->file->watch

use common::sense;
use R::App;

has qw/changed created deleted/;

# конструктор
sub new {
	my $cls = shift;
	bless {@_}, ref $cls || $cls;
}

# количество изменённых
sub modify {
	my ($self) = @_;
	$self->{changed}->length + $self->{created}->length + $self->{deleted}->length
}

# возвращает очередь из всех
sub modified {
	my ($self) = @_;
	$self->changed->add($self->created, $self->deleted)
}

1;