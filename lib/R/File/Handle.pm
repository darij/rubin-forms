package R::File::Handle;
# tie на IO::AIO

use common::sense;
use R::App qw/$app msg msg1/;
use R::Coro;
use IO::AIO;

R::TODO;

# The method invoked by the command tie *glob, classname . Associates a new glob instance with the specified class. LIST would represent additional arguments (along the lines of AnyDBM_File and compatriots) needed to complete the association.
sub TIEHANDLE {
	my ($classname, $mode, $path) = @_;
	
}

# Write length bytes of data from scalar starting at offset.
sub WRITE {
	my ($this, $scalar, $length, $offset) = @_;
}

# Print the values in LIST
sub PRINT {
	my $this = shift;
}

# Print the values in LIST using format
sub PRINTF {
	my ($this, $format, $LIST) = @_;
}

# Read length bytes of data into scalar starting at offset.
sub READ {
	my ($this, $scalar, $length, $offset) = @_;
}

# Read a single line
sub READLINE {
	my ($this) = @_;
}

# Get a single character
sub GETC {
	my ($this) = @_;
}

# Close the handle
sub CLOSE {
	my ($this) = @_;
}

# (Re-)open the handle
sub OPEN {
	my ($this, $filename) = @_;
}

# Specify content is binary
sub BINMODE {
	my ($this) = @_;
}

# Test for end of file.
sub EOF {
	my ($this) = @_;
}

# Return position in the file.
sub TELL {
	my ($this) = @_;
}

# Position the file.
# Test for end of file.
sub SEEK {
	my ($this, $offset, $whence) = @_;
}

sub DESTROY {
	my ($this) = @_;
}


1;