package R::Rsgi::Request;
# реквест для http-сервера

use common::sense;
use R::App qw/$app msg1 msg Num/;

use Socket qw//;

# stash - это R::Site::Stash
R::has qw/buf_size release_buf_size stash start_time recv_time form was_sending/;

# конструктор
sub new {
	my $cls = shift;
	
	my $self = bless {
		#buf_size => 1024*1024*4,		# размер буфера для тела запроса и web-сокетов
		#release_buf_size => 1024*8,		# размер буфера для релиза - считывания запроса
		@_
	}, ref $cls || $cls;
	
	$Coro::current->{_REQUEST}{int $self} = ref $self;
	
	$self
}


sub DESTROY {
	delete $Coro::current->{_REQUEST}{int $_[0]};
}

#@@category обработка запроса

# считывает запрос
sub recv {
	my ($self) = @_;

	my $telemetry_defer = $app->telemetry->defer("fulminant/recv");

	$self->{recv_time} = Time::HiRes::time();
	
	my $ns = $self->{ns};
	
	#msg1 "R";
	
	$self->{HTTP} = my $HTTP = readline $ns;
	
	#msg1 "R1", $HTTP;
	
	goto EXIT if !defined $HTTP;
	
	goto EXIT unless defined $HTTP and ($self->{via}, $self->{location}, $self->{protocol}) = $HTTP =~ m!^(\w+) (\S+) (HTTP\/\d\.\d)\r?$!;
	
	# считываем заголовки
	my @h;
	push @h, $_ while defined($_ = readline $ns) and !/^\s*$/;
	$self->{headers} = $app->mime->httpheaders->from(\@h);
	
	#msg1 "R2", $self->{headers};
	
	EXIT:
	
	$self->{start_time} = Time::HiRes::time();
	$self->{recv_time} = $self->{start_time} - $self->{recv_time};
	
	$self
}

# возвращает тело запроса
sub body {
	my ($self) = @_;
	die "тело запроса уже было считано" if $self->{release};
	my $len = $self->{headers}{"Content-Length"};
	
	my $buf_size = $self->{buf_size} // 1024*1024*4;
	
	# боди слишком большое
	$self->{not_keep_alive} = 1, die [413] if $len > $buf_size;
	
	my $ns = $self->{ns};
	$ns->partial(1);
	$ns->read(my $buf, $buf_size);
	$ns->partial(0);

	$self->{release} = 1;
	
	die [411, [], "Content-Length: $len, однако прочитано " . length($buf)] if defined $len and length($buf) != $len;
	
	$buf
}

# считывает оставшиеся post-данные
#@deprecated
sub __release {
	my ($self) = @_;	
	$self->body if !$self->{release};
	$self
}

# считывает оставшиеся post-данные
sub release {
	my ($self) = @_;
	my $ns = $self->{ns};
	
	#msg1 '$ns->readable='.$ns->readable;
	
	#return $self if !$ns->readable;
	
	# $ns->partial(1);
	# $ns->read(my $buf, 1024*1024*256);
	# $ns->partial(0);

	
	my $fh = $ns->fh;
	my $buf = \$ns->rbuf;
	$$buf = "";
	
	my $release_buf_size = $self->{release_buf_size} // 1024*8;
	
	my $bytes;
	do {
		$bytes = sysread $fh, my $body, $release_buf_size;
	} while( $bytes );
	
	$self
}

#@@category информационные методы

# сокет был закрыт удалённым сервером
sub disconnected {
	my ($self) = @_;
	!defined $self->{HTTP}
}

# запрос не был распознан
sub bad {
	my ($self) = @_;
	!defined $self->{via}
}

# возвращает строку HTTP
sub http {
	my ($self) = @_;
	$self->{HTTP}
}

# возвращает хост
sub host {
	my ($self) = @_;
	$self->{host} //= $self->url->host;
}

# возвращает порт
sub port {
	my ($self) = @_;
	$self->{port} //= $self->url->port;
}

# возвращает ip:port посетителя
sub remote {
	my ($self) = @_;
	$self->{remote} //= $self->header("X-IP") // do {
		my($port, $iaddr) = Socket::sockaddr_in($self->{paddr});
		my $ip = Socket::inet_ntoa($iaddr);
		"$ip:$port"
	}
}

# возвращает местоположение
sub location {
	my ($self) = @_;
	die "лишние параметры" if @_>1;
	$self->{location}
}

# возвращает метод
sub METHOD { goto &via }
sub via {
	my ($self) = @_;
	die "лишние параметры" if @_>1;
	$self->{via}
}

sub isGET { shift->METHOD eq "GET" }
sub isPOST { shift->METHOD eq "POST" }
sub isPATCH { shift->METHOD eq "PATCH" }
sub isHEAD { shift->METHOD eq "HEAD" }
sub isOPTIONS { shift->METHOD eq "OPTIONS" }
sub isDELETE { shift->METHOD eq "DELETE" }

# возвращает $app->url для location
sub url {
	my ($self) = @_;
	die "лишние параметры" if @_>1;
	$self->{url} //= $app->url($self->header("Host") . $self->{location});
}

# путь из location
# если нужно - путь можно менять. Он используется в роутинге
# Чтобы получить оригинальный: request->url->path
sub path {
	my ($self, $path) = @_;
	if(@_>1) { $self->{path} = $path; return $self }
	$self->{path} //= ($self->{location} =~ /\?/? $`: $self->{location});
}

# возвращает реферер без хоста, если хост совпадает с хостом сайта
sub referer {
	my ($self, $default) = @_;
	my $referer = $self->header->Referer || $default;
	my $host = quotemeta($self->header->Host // "localhost");
	$referer = $default unless $referer =~ s/^https?:\/\/$host(\/|$)/\//i;
	$referer
}

# # возвращает реферер с хостом
# sub referer_full {
	# my ($self, $default) = @_;
	# $self->url->path()
# }

# возвращает каталог сайта на диске
sub site {
	my ($self) = @_;
	my $host = $self->{headers}->{"Host"};
	defined($host) && $host !~ /^localhost:\d+\z/? "site/$host": "html"
}

# если соединение keep-alive
sub keep_alive {
	my ($self) = @_;
	!$self->{not_keep_alive} and scalar($self->{headers}{Connection} =~ /\bkeep-alive\b/i)
}

# возвращает заголовок
sub header {
	my $self = shift;
	($self->{header} //= $app->rsgiBag($self->{headers}))->Get(@_)
}

# возвращает content-type
sub type {
	my ($self) = @_;
	$self->{headers}{"Content-Type"}
}

# возвращает mime-type
sub mime_type {
	my ($self) = @_;
	my $type = $self->type;
	$type =~ /;/? $`: $type
}

# возвращает размер тела запроса
sub size {
	my ($self) = @_;
	$self->{headers}{"Content-Length"}
}

# возвращает время когда ресурс был помещён в кеш
sub modified {
	my ($self) = @_;
	$self->{modified} //= do {
		my $modified = $self->{headers}{"If-Modified-Since"};
		defined($modified)? $app->magnitudeDate->parse_http($modified)->epoch: ""
	}
}

# возвращает атрибут
sub URI { goto &attr }
sub attr {
	my $self = shift;
	($self->{attr} //= $app->rsgiBag($self->{_attr}))->Get(@_)
}

# возвращает из запроса
sub GET { goto &query }
sub query {
	my $self = shift;
	($self->{query} //= $app->rsgiBag($app->mime->urlencoded->from($self->{location} =~ /\?(.*)/)))->Get(@_)
}

# возвращает из POST. Файлы так же тут, только в виде объектов R::Stream
sub POST { goto &data }
sub data {
	my $self = shift;
	($self->{data} //= do {
		if($self->size == 0) { $app->rsgiBag->new }
		elsif($self->type) {
			my $reader = $app->mime->Get($self->type);
			if($reader->can("from_request")) {
				# для считывания напрямую из сокета и получения заголовков
				$app->rsgiBag($reader->from_request($self))
			} else {
				$app->rsgiBag($reader->from($self->body))
			}
		}
		else {
			die [415, [], "не указан тип данных"];
		}
	})->Get(@_)
}

# возвращает из attr и query
sub uriparam {
	my $self = shift;
	($self->{uriparam} //= $app->rsgiBag($self->attr, $self->query))->Get(@_)
}

# возвращает из attr, query, data
sub param {
	my $self = shift;
	($self->{param} //= $app->rsgiBag($self->attr, $self->query, $self->data))->Get(@_)
}

# возвращает куки
sub cookie {
	my $self = shift;
	($self->{cookie} //= $app->rsgiBag($app->mime->urlencoded->from($self->{headers}{Cookie}, qr/;\s*/)))->Get(@_)
}

# выдаёт красивую информацию о себе
sub stringify {
	my ($self) = @_;
	join "\n", $self->http,
	(map {  "$_: " . $self->header($_) } keys %{$self->header}),
	"",
	$self->body
}


#@@category вебсокет

# посылает сообщение по вебсокету
sub emit {
	my ($self) = @_;
	$self->{io}->emit(@_);
	$self
}

#@@category хелперы

# загружает модель
sub load {
	my ($self, $model, $name, $cols) = @_;
	$name //= "id";
	my $id = $self->param($name);
	return undef if !$id;
	my $mod = $app->model->$model->view(@$cols)->find(id=>$id)->exists;
	die [404, "нет модели $model c № $id"] if !$mod;
	$mod
}

# загружает и проверяет, чтобы owner был текущим пользователем
sub owner {
	my ($self, $model, $owner, $name) = @_;
	$owner //= "owner";
	my $mod = $self->load($model, $name, [$owner]);
	if($mod) {
		die [401, "модель `$model` Вам не принадлежит"] if $mod->$owner->id != $self->auth;
	}
	$mod
}


#@@category сессия

# сессия - возвращает объект с сессией
sub session {
	my ($self) = @_;
	$self->{session} //= $app->session(id => scalar $self->cookie("sess"))->touch;
}

# пользователь из сессии
sub user {
	my ($self) = @_;
	$self->{"user"} //= $self->session->user
}

# ошибка, если пользователь гуест
sub auth {
	my ($self) = @_;
	my $user = $self->user;
	die [401] if !$user->id;
	$user->{id}
}

#@@category локаль

# возвращает локаль запроса
sub locale {
	my ($self) = @_;
	my $locale;
	return $locale if $locale = $self->{locale};
	return $self->{locale} = $locale if $locale = $app->locale->factory($self->cookie->lang);
	
	my $lang = $self->header("Accept-Language");
	while($lang =~ /\b([a-z]{2})\b/i) {
		return $self->{locale} = $locale if $locale = $app->locale->factory(lc $1);
	}
	
	return undef;
}

#@@category фабрики ответов

# # файл
# sub root {
	# my ($self, $root) = @_;
	# my $f = $app->project->file($root)->sub($self->path)->abs;
	# my $re_path_root = "^" . quotemeta $app->project->path($root);
	# die [403] if $f->path !~ $re_path_root;
	# die [404] if !$f->exists;
	# $f
# }

# # шаблонизатор
# sub render {
	# my ($self, $path, $data) = @_;
	# $path //= substr $self->path, 1;
	# $data //= {%{$self->param}};
	# $app->javascript->render($path, $data)
# }

# возвращает редирект
sub redirect {
	my ($self, $status, $location, $text) = @_ == 2? ($_[0], 303, $_[1]): @_;
	$status //= 303;
	$location //= $self->referer("/");
	$text //=  "Redirect to $location ...\nServer: $self->{fulminant}{name}";
	[$status, [Location => $location], $text]
}


1;