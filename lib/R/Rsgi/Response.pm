package R::Rsgi::Response;
# ответ http-сервера

use common::sense;
use R::App;

has qw/res/;

# конструктор
sub new {
	my $cls = shift;
	my $self = bless {
		@_
	}, ref $cls || $cls;
	
	$Coro::current->{_RESPONSE}{int $self} = ref $self;
	
	$self
}


sub DESTROY {
	delete $Coro::current->{_RESPONSE}{int $_[0]};

}

# дефолтный конструктор
sub new_def {
	my ($self) = @_;
	$self->new(status => 200, headers => [], data => "")
}

# отправляет ответ
sub send {
	my ($self, $request, $keep_alive_timeout) = @_;
		
	my $telemetry_defer = $app->telemetry->defer("fulminant/send");
	
	#msg1 "S";
	
	# времена
	my $send_time = Time::HiRes::time();
	my $process_time = $send_time - $request->{start_time};
	
	# сервер с настройками
	my $fulminant = $request->{fulminant};
	
	# заголовки
	my @headers;

	my $content_type;
    my $is_len;
	my $added_server;
	my $added_session;

	my $x = $self->headers;
	my $len_headers = @{$self->headers};
	
	for(my $i=0; $i<@$x; $i+=2) {
		my ($k, $v) = @$x[$i, $i+1];
		
		$content_type = $v, next if $k =~ /^Content-Type$/i;
		$is_len = $v, next if $k =~ /^Content-Length$/i;
		next if $k =~ /^(Connection|Content-Encoding)\z/in;
		$added_server = $v if $k=~ /^Server$/i;
		$added_session = $' if $k=~ /^Set-Cookie$/i && $v =~ /^sess=/;
		
		push @headers, $k, ": ", $v, "\n";
	}

	# устанавливаем сессию
	
	$self->cookie("sess" => $request->session->id) if !defined($added_session) && !defined $request->cookie("sess");
	
	# данные
	my $data = $self->data;
	my $file = 0;
	my $r_file = 0;
	if(!defined $data) {
		$data = join "", "<center>", $self->status, " ", $self->reason, "<hr>fulminant/$fulminant->{name}</center>"
	}
	elsif(ref $data eq "HASH") {
		$content_type = "text/json; charset=utf-8" if !defined $content_type;
		$data = $app->json->to($data);
	}
	elsif(ref $data eq "GLOB" || Can $data, "sysread") {
		$file = 1;
		$data->binmode(":raw");
	}
	elsif(Isa $data, "R::File") {
		$r_file = 1;
	}
	elsif(ref $data) {	# для overload ""
		$data = "$data";
	}

	utf8::encode($data) if !$file && !$r_file && utf8::is_utf8($data);
	
	# кеширование: if-modified-since
	my $mtime;
	my $length;
	my $check_browser_cache = sub {
		#msg1 ":space", $fulminant->{file_modified} || undef, $mtime, ">", $request->modified || undef, "=", $mtime > $request->modified;
		if( $fulminant->{file_modified} && $request->modified && $mtime <= $request->modified ) {
			$length = $file = $r_file = $data = "";
			$self->status(304);
			msg ":red", 304, ":blue", $mtime, ":green", "If-Modified-Since", ":blue", $request->modified;
		}
	};
	
	# параметры: длина и mtime
	if($file) {
		my @stat = stat $data;
		$mtime = $stat[9] || time;
		$length = $stat[7];
		$check_browser_cache->();
	}
	elsif($r_file) {
		$mtime = $data->mtime || time;
		$length = $data->size;
		$check_browser_cache->();
	}
	else {
		$length = length $data;
	}
	
	msg "Передан Content-Length: $is_len != $length" if defined $is_len and $is_len != $length;
	
	# gzip
	my $real_gz; my $gz;
	if($fulminant->{gzip} && $length > $fulminant->{gzip_min_length}) {
		my $accept = $request->header->{"Accept-Encoding"};
		for my $zip (@{$fulminant->{gzip}}) {
			$gz = 1, $real_gz = $zip, last if $accept =~ /\b$zip\b/;
		}
	}
	
	if($r_file) {
		if(!defined $content_type) {
			$content_type = $app->mime->Type($data->ext, charset => "utf-8");
		}
		
		if($gz) {
			msg1 "media", $data->path, $real_gz;
			$data = $app->media->get_gz($data->path, $real_gz, $fulminant->{gzip_comp_level});
			$gz = 0;	# не надо зипить - файл уже будет gzip-ом
			#$length = $data->size;
		}
		
		$data = $data->encode(undef)->open;
		$file = 1;
		$r_file = 0;
	}
	
	my $ns = $request->{ns};
	
	my $keep_alive = $keep_alive_timeout && $fulminant->keep_alive && $request->keep_alive;
	
	if($keep_alive) {
		$request->release;
		unshift @headers, "Keep-Alive: timeout=$keep_alive_timeout, max=1000\n";
	}

	$content_type //= "text/html; charset=utf-8";
	unshift @headers, 
		"Content-Type: ", $content_type, "\n",
		"Connection: ", ($keep_alive? "Keep-Alive": "Close"), "\n",
		$added_server? "Proxy-Server: fulminant/1.1\n": "Server: fulminant/1.1\n";
		
	unshift @headers,
		"Date: ", $app->magnitudeDate->http, "\n",
		"Last-Modified: ", $app->magnitudeDate($mtime)->http, "\n",
		"Cache-Control: max-age=0\n"
	if $request->via eq "GET";
		
	unshift @headers, "Content-Length: ", int($length), "\n" if !$real_gz;
	unshift @headers, "Content-Encoding: ", $real_gz, "\n" if $real_gz;
	
	# добавляем заголовки установленные во время формирования ответа. Например, куку сессии
	push @headers, R::map2 { ($a, ": ", $b, "\n") } @{$self->{headers}}[$len_headers .. $#{$self->{headers}}];

	my $status = $self->status;
	my $reason = $self->reason;
	
	unshift @headers, "HTTP/1.1 $status $reason\n";
	
    $ns->write(join "", @headers, "\n");
	
	#msg1 "S2", join "", @headers;
	
	if($file) {
		if($gz) {
			$app->file($data)->anyzip($real_gz, $ns, $fulminant->{gzip_comp_level});
		} else {
			my $size = $self->{blksize} // 1024*1024;
			my $offset = 0;
			my $readed;
			$offset += $readed, $ns->write($_) while $readed = $data->sysread($_, $size, $offset);
		}
		
		#$ns->sendfile($data);
	}
	else {
		if($gz) {
			#msg1 "media","context!", $real_gz, $fulminant->{gzip_comp_level};
			$app->file(\$data)->anyzip($real_gz, $ns, $fulminant->{gzip_comp_level});
		}
		else {
			$ns->write($data);
		}
	}
	
	if(my $log = $fulminant->{log}) {
	
		my ($type, $charset) = $content_type =~ /([^;]+)\s*(.*)/;
		my $recv_time = $request->recv_time;
		my $send_time = Time::HiRes::time() - $send_time;
		my $host = $request->header->Host;
		undef $host if $host =~ /:/;

		my $num = $Coro::current->{desc} =~ /my-querier-(\d+)/? $1: $Coro::current->{desc};
		my $action = $request->param('@action');
		$log->info(":space bold black", 
			"$num)",
			$host? "[$host]": (),
			":reset magenta", $request->via, ":cyan", $request->location, 
			defined($action)? (":red", '@action', ":cyan", $action): (), 
			":reset", 
			$app->perl->fsec($recv_time),
			$app->perl->fsec($process_time), 
			$app->perl->fsec($send_time),
			
			do { given($self->status) {
				":blue" when [100..199];
				":green" when [200..299];
				":bold black" when [300..399];
				":magenta" when [400..499];
				":red" when [500..599];
				default { ":yellow" }
			}},
			$self->status,
			":reset magenta",
			$app->magnitudeByte->to($length, 2),
			":reset",
			$type,
			$real_gz? (":green", $real_gz eq "br"? "brzip": $real_gz): (),
			$keep_alive? (":black bold", "keep-alive"): (),
			#$result->content_type =~ /^text/? $result->content_charset: (),
		);
		
		if($fulminant->{log_headers}) {
			$log->info(":nonewline", $_) for @headers;
		}
	}
	
	#$ns->close, undef $request->{ns} if !$keep_alive;
	
	$request->was_sending(1);
	
	$keep_alive
}

# конструктор: ошибка 500 или 
sub fail {
	my ($cls, $res, $request) = @_;

	my $self = $cls->new;
	
	%$self = (%$self, %{$res->resulting}), return $self if Isa $res, ref $self;
	
	if(ref($res) ne "ARRAY") {
		# печатаем в лог
		msg ":red sep( -> )", "FAIL!", ":magenta", $request->via, ":blue", $request->location, ":reset", "\n", $res;
		$res = $app->ini->{dev}? [500, [], $app->html->ansi($res, "border: solid 2px #DC143C;")]: [500];
	}

	$self->{res} = $res;
	$self->resulting(500)
}

# определяет, что res - типа себя и если нет - оборачивает
sub from {
	my ($self, $res) = @_;
	Isa($res, $self)? $res: $self->new(res=>$res);	
}

# объединяет два запроса. Второй считается приоритетным. 
sub merge {
	my ($self, $response) = @_;
	
	return $response if ref $self eq "";
	
	if(Isa $response, $self) {
		$self->status($response->status) if $response->status != 200;
		push @{$self->headers}, @{$response->headers};
		$self->data($response->data);
	}
	elsif(ref $response eq "ARRAY" && @$response ~~ [1..3] && ref $response->[0] eq "" && $response->[0] =~ /^\d\d\d$/) {
		$self->status($response->[0]) if $response->[0] != 200;
		push @{$self->headers}, @{$response->[1]};
		$self->data($response->[2]) if defined $response->[2];
	}
	else {
		$self->data($response) if defined $response;
	}
	
	$self
}

# устанавливает результат
sub resulting {
	my ($self, $status) = @_;

	$status //= 200;
	
	my $res = $self->{res};
	
	$res = [$status, [], $res] if ref $res ne "ARRAY";
	
	$res = [$status, [], $res] if !defined($res->[0]) || ref($res->[0]) || $res->[0] !~ /^\d{3}\z/;
	
	$res = [520, [], "520 bad response: Array size is " . @$res] if @$res > 3;
	$res = [$res->[0], [], $res->[1]] if @$res == 2 and !ref $res->[1];
	$res = [520, [], "520 bad response: headers ne ARRAY"] if defined $res->[1] and ref $res->[1] ne "ARRAY";
	
	$self->{nothing} = defined($res->[2])? 1: 0;
	
	$self->status($res->[0]);
	$self->headers($res->[1] // []);
	$self->data($res->[2]);

	$self
}


# свойство кода
sub status {
	my $self = shift;
	if(@_) {
		$self->{status} = shift;
		$self
	}
	else {
		$self->{status} // $self->resulting->{status}
	}
}

# свойство заголовков
sub headers {
	my $self = shift;
	if(@_) {
		$self->{headers} = shift;
		$self
	}
	else {
		$self->{headers} // $self->resulting->{headers}
	}
}

# свойство данных
sub data {
	my $self = shift;
	if(@_) {
		$self->{data} = shift;
		$self
	}
	else {
		$self->{data} // $self->resulting->{data}
	}
}

# показывает, что данных небыло
sub nothing {
	my $self = shift;
	if(@_) {
		$self->{nothing} = shift;
		$self
	}
	else {
		$self->{nothing} // $self->resulting->{nothing}
	}
}


# возвращает название статуса
sub reason {
	my $self = shift;
	if(@_) {
		$self->{reason} = shift;
		$self
	}
	else {
		$self->{reason} // $app->httpStatus->{$self->status} // "Undefined Reason (${\$self->status})"
	}
}

# устанавливает куку
sub cookie {
	my ($self, $name, $value, %param) = @_;
		
	my %param_allow = map { $_=>1 } qw/expires path domain secure httponly/;
	my @noallow = grep { !exists $param_allow{$_} } keys %param;
	die "неизвестный ключ в cookie: " . join ", ", @noallow if @noallow;
	
	$param{path} //= "/";
	
	require HTTP::Date;
	
	$name = $app->mime->urlencoded->escape($name);
	$value = $app->mime->urlencoded->escape($value);
	
	my $val = join "", $name, "=", $value,
		(exists $param{expires}? ("; Expires=" , HTTP::Date::time2str($param{expires})): ()),
		(exists $param{path}? "; Path=$param{path}": ()),
		(exists $param{domain}? "; Domain=$param{domain}": ()),
		(exists $param{secure}? "; Secure": ()),
		(exists $param{httponly}? "; HttpOnly": ());
	
	push @{$self->headers}, "Set-Cookie" => $val;
	$self
}


1;