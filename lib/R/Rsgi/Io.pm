package R::Rsgi::Io;
# расширяет R::Rsgi::Request и делает вебсокетом

use common::sense;
use R::App;

use Protocol::WebSocket::Handshake::Server;
use Protocol::WebSocket::Frame;

use Errno qw(EAGAIN EINTR);
use AnyEvent::Util qw(WSAEWOULDBLOCK);

my $BUF_SIZE = 1024*1024*4;

# конструктор
sub new {
	my $cls = shift;
	bless {
		echo => sub { msg @_ },
		@_
	}, ref $cls || $cls;
}


# обработка события на веб-сокете
sub adventure {
	my ($self, $q) = @_;

	return [426] unless $q->header("Upgrade") =~ /\bwebsocket\b/i && $q->header("Connection") =~ /\bUpgrade\b/i;
	
	my $fulminant = $q->{fulminant};
	return [503] if $fulminant->{max_websockets} <= $fulminant->{busy_websockets};
	my $defer_unbusy = R::guard { 
		--$fulminant->{busy_websockets} if exists $fulminant->{busy_websockets};
		++$fulminant->{busy_queriers} if exists $fulminant->{busy_queriers};
	};
	++$fulminant->{busy_websockets};
	--$fulminant->{busy_queriers};
	
	# создаётся сообщение	
	my $handshake = Protocol::WebSocket::Handshake::Server->new;
	$q->{websocket_frame} = my $frame = $handshake->build_frame;
	$q->{io} = $self;

	# partial(1) делаем сокет неблокирующим
	my $ns = $q->{ns};
	$ns->partial(1);
	$ns->read(my $buf, $q->{buf_size} // $BUF_SIZE);
	$ns->partial(0);
		
	# передаётся заголовок в парсер
	my $head = $q->{headers};
	my $chunk = join "", $q->{HTTP}, map({ "$_: $head->{$_}\n" } keys %$head), "\n" , $buf;
	
	#msg ":red", "in", ":reset", $chunk;
	
	die [400, [], $handshake->error] if !defined $handshake->parse($chunk);
	die [505, [], "ws: нужно досчитать заголовок"] if !$handshake->is_done;
	
	# записываем заголовок ответа: HTTP/1.1 101 WebSocket Protocol Handshake ...
	$ns->write($handshake->to_string);
	
	# отключаем таймаут
	undef $q->{timer};
	
	# ответ отправлен: больше не отправлять
	$q->was_sending(1);
	
	# если движения по сокету не было, то закрываем соединение
	my $stamp = 123456789;
	my $msg_now = R::time();
	my $me = $Coro::current;
	
	# таймер пингует клиент и если через timeout_websocket_pong сек не получит ответ, то грохает сокет
	my $kill_timer;
	my $timer = AE::timer( $fulminant->{timeout_websocket},  $fulminant->{timeout_websocket}, sub {
		if( R::time() - $msg_now >= $fulminant->{timeout_websocket} ) {

			$self->ping($q);

			$kill_timer = AE::timer( $fulminant->{timeout_websocket_pong},  0, sub {
				$me->throw($stamp);
			});
			
		}
	});
	
	# регистрируем пользователя, чтобы потом можно было ему отправлять сообщения
	my $register_user_defer = $q->user->id? do {
		my $user_ws = $q->{fulminant}->{user_websockets}->{$q->user->id} //= {};
		$user_ws->{$q} = $q;
		guard { 
			delete $user_ws->{$q}; 
			delete $q->{fulminant}->{user_websockets}->{$q->user->id} if !scalar keys %$user_ws;
		}
	}: ();
	
	eval {
	
		# сообщаем, что приконнектились
		$self->gosub("connect", $q) if exists $self->{connect};

		while() {

			$ns->readable;	# тут происходит переключение на другие волокна, пока не придёт фрейм
		
			# добавляем время для сокета
			$msg_now = R::time();
			undef $kill_timer;
		
			$ns->partial(1);
			my $len = $ns->read($chunk, $q->{buf_size} // $BUF_SIZE);
			$ns->partial(0);
			
			$frame->append($chunk);
			undef $chunk;

			while (defined(my $msg = $frame->next)) {
				#msg1 $msg;
				
				if($frame->is_text) {
					my $json = $app->json->from($msg);
					$self->gosub(@$json);
				}
				elsif($frame->is_close) {
					# обработчик дисконнекта
					return;	# выходим из блока eval
				}			
				elsif( $frame->is_ping ) { $self->pong($q) } # msg1 "ping websocket frame";
				elsif( $frame->is_pong ) {} #msg1 "pong websocket frame" }
				elsif( $frame->is_continuation ) { msg1 "continuation websocket frame" }
				elsif( $frame->is_binary ) { msg1 "binary websocket frame" }
				else {
					msg1 "undefined websocket frame";
				}
				
				# my $args = $app->json->from($mess);
				# msg("получен запрос не по протоколу: `$mess`"), $q->close, last if ref $args ne "ARRAY";
				# my $name = shift @$args;
				# msg("повреждён запрос websocket - не указано имя сообщения: `$mess`"), $q->close, last if !defined $name;
				# my $cmd = $self->{messages}{$name};
				# msg("не зарегистрировано сообщение с именем: `$name`"), next if !defined $cmd;
				
				# $app->contact->init;
				# eval { $cmd->(@$args) };
				# msg "message.$name: $@" if $@;
				# $app->contact->change;
			}			
		}
	};
	
	if(defined $@ and $@ != $stamp and $@ ne "") {
		{
			local $@;
			$self->close($q);
		}
		die;
	}
	
	msg1 ":green", "io", ":red", "disconnect";
	$self->gosub("disconnect", $q) if exists $self->{disconnect};
	
	$self->close($q);
	
	return "";
}

# устанавливает обработчики
sub on {
	my $self = shift;
	for(my $i=0; $i<@_; $i+=2) {
		$self->{$_[$i]} = $_[$i+1];
	}
	$self
}

# вызывает обработчик
sub gosub {
	my ($self, $name, @args) = @_;
	eval { $self->{$name}->(@args) };
	msg "websocket.emit($name): $@" if $@;
	$self
}

# отправляет сообщение
sub emit {
	my ($self, $q, @args) = @_;
	
	#msg1 ":on_magenta cyan", "emit", "$q", ":reset", @args;
	
	my $json = $app->json->to(\@args); 
	my $bytes = $q->{websocket_frame}->new($json)->to_bytes;
	$q->{ns}->write($bytes);
	
	$self
}

# корректно закрывает сокет
sub close {
	my ($self, $q) = @_;
	
	my $bytes = $q->{websocket_frame}->new(buffer=>'', type=>"close")->to_bytes;
	$q->{ns}->write($bytes);
	
	$self
}

# пингует клиента
sub ping {
	my ($self, $q) = @_;
	
	my $bytes = $q->{websocket_frame}->new(buffer=>'', type=>"ping")->to_bytes;
	$q->{ns}->write($bytes);
	
	$self
}

# отвечает на ping
sub pong {
	my ($self, $q) = @_;
	
	my $bytes = $q->{websocket_frame}->new(buffer=>'', type=>"pong")->to_bytes;
	$q->{ns}->write($bytes);
	
	$self
}

# отправляет сообщение всем вебсокетам пользователя
sub user_emit {
	my ($self, $q, $user, @args) = @_;
	
	my $user_id = ref $user? $user->id: defined $user? $user: $q->user->id;
	
	return $self if !$user->id or not my $wss = $q->{fulminant}->{user_websockets}->{$user_id};
	
	for my $q ( values %$wss ) {
		$self->emit($q, @args);
	}
	
	$self
}

1;