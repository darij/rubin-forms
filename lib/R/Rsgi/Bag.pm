package R::Rsgi::Bag;
# сумка для параметров

use common::sense;
use R::App;

# конструктор
sub new {
	my $cls = shift;
	my $self = bless {}, ref $cls || $cls;

	$self->Add(@_);
}

# добавляет данные
sub Add {
	my ($self) = @_;
	
	for(my $i=1; $i<@_; $i+=2) {
		my $k = $_[$i];
		
		$self->Data($k), $i--, next if !defined $k or ref $k;
		
		my $v = $_[$i+1];
		
		if(exists $self->{$k}) {
			my $x = $self->{$k};
			$self->{$k} = ref $x eq "R::Mime::Array"? push(@$x, $v): bless [$x, $v], "R::Mime::Array";
		} else {
			$self->{$k} = $v;
		}
	}
	
	$self
}

# устанавливает хэш или массив с данными
sub Data {
	my ($self, $data, $sep) = @_;
	
	return $self if !defined $data;
	
	$data = $app->mime->urlencoded->from($data, $sep) if "" eq ref $data;
	if(ref $data eq "HASH" or Isa $data, ref $self) {
		$self->Add(%$data);
	}
	elsif(ref $data eq "ARRAY") {
		$self->Add(@$data);
	}
	else {
		die "устанавливаем валидные параметры";
	}
	
	$self
}

# возвращает из сумки ключи сумки или параметр
sub Get {
	my $self = shift;
	return $self if !@_;
	my $name = shift;
	my $res = $self->{$name};
	return wantarray? @_: $_[0] if !defined $res;
	return wantarray? @$res: $res->[0] if ref $res eq "R::Mime::Array";
	$res
}

# возвращает параметр
sub AUTOLOAD {
	my $self = shift;
	our $AUTOLOAD =~ /([^:]+)\z/;
	eval "sub $AUTOLOAD { splice \@_, 1, 0, '$1'; goto &Get }";
	die if $@;
	$self->Get($1, @_)
}

# деструктор
sub DESTROY {}

# возвращает ключи в виде массива
sub All {
	my ($self) = @_;
	+{ %$self }
}

# количество ключей
sub Size {
	my ($self) = @_;
	scalar keys %$self
}

1;