package R::Copiright;
# тут находятся копирайты

use common::sense;
use R::App;

# конструктор
sub new {
	my ($cls) = @_;
	bless {}, ref $cls || $cls;
}

# для вставки в начало файлов
sub file {
	my ($self) = @_;
	
	'#  
#@AUTHOR      Yaroslav O. Kosmina
#@EMAIL       darviarush@mail.ru
#@SITE        darviarush.narod.ru
#@HUB         https://bitbucket.org/darij/rubin-forms/overview
#@LICENSE     BSD-2 (aka MIT)
#
'
}


1;