package R::Log::File;
# логирует на терминал

use common::sense;
use R::App;

use POSIX qw/strftime/;
use Fcntl qw(:flock SEEK_END);   # импортируем константы LOCK_*

# конструктор
sub new {
	my ($cls, %x) = @_;
	
	my $f = $app->process->pidfile->ext("log");
	$f->name("!!!noname!!!" . $app->project->name) if !length $app->process->name;
	
	if($f->exists) {
		$f->bzip2( $f->dir($f->dir . "/log-bk")->adddate("bz2")->mkpath->path );
	}
	
	bless {
		%x,
		"file" => $f->open(">"),
	}, $cls;
}

# сообщение
sub send {
	my ($self, $level, $message) = @_;
	
	my $log = $self->{file};
	#$message =~ s!^!\t!gm;
	#$message =~ s/$/\n/;
	my $c = 0;
	$c++ while $message =~ /\n/g;
	
	my $info = "";
	
	my $log_info_prefix = 1; #$app->ini->{'log_info_prefix'};
	if($log_info_prefix) {
		my $coro = $Coro::main? " <".($app->coro->me->name? $app->coro->me->name: "#".$app->coro->me->id).">": "";
		my $name = $app->process->name? " (".$app->process->name.")": "";
		$info = strftime("%Y-%m-%d %H:%M:%S", localtime) . " [$$] {$level}$name$coro";
	}
	
	$self->lock;
	
	if($c<2) {
		print $log "$info $message";
	} else {
		print $log "\n\t$info\n$message\n";
	}

	$self->unlock;
	
	$self
}

sub lock {
	my $self = shift;
	my $log = $self->{"file"};
	flock($log, LOCK_EX) or do { print STDERR "Cannot lock log - $!\n"; exit };
	seek($log, 0, SEEK_END)  or do { print STDERR "Cannot seek - $!\n"; exit };	# и если что-то добавилось, пока мы ожидали...
	$self
}

sub unlock {
	my $self = shift;
	flock($self->{"file"}, LOCK_UN) or do { print STDERR "Cannot unlock log - $!\n"; exit };
	$self
}

1;
