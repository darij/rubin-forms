package R::Mail;
# почтальён

use common::sense;
use R::App;

use MIME::Base64;
use MIME::Lite;

R::has qw/mail param dev/;

# конструктор транспорта письма
sub new {
	my $cls = shift;
	bless {@_}, ref $cls || $cls;
}

# создаёт новое письмо
sub letter {
	my $self = shift;
	bless {
		%$self,
		param => {
			%{$self->{param}},
			@_,
		},
	}, ref $self;
}

# геттер-сеттер
sub _property {
	my ($name, $self, $val) = @_;
	@_==2? $self->{param}->{$name}: do { $self->{param}->{$name} = $val; $self }
}

for my $sub (qw/subject from to cc type data encoding path/) {
	eval "sub $sub { 
		unshift \@_, '${\ucfirst $sub}';
		goto &_property;
	}";
	die if $@;
}

# подключает файл
sub attach {
	my $self = shift;
	my $file = shift;
	
	my $type = $app->mime->Alias( $app->file($file)->ext );
	
	$type .= "; codepage=utf-8" if $type =~ m!^text/!;
	
	push @{$self->{attach}}, {Path => $file, Type => $type, @_};
	$self
}

# рендерит и подключает файл
sub real_render {
	my ($self, $path, $data) = splice @_, 0, 3;
	
	my $file = $app->view->render($path, $data);
	my $type = $app->mime->Type( $app->file($path)->ext );
	
	$type .= "; codepage=utf-8" if $type =~ m!^text/!;
	
	push @{$self->{attach}}, {Data => $file, Type => $type, @_};
	
	$self
}

# рендерит из папки view/mail и добавляет как attach
sub render {
	my $self = shift;
	my $path = shift;
	$self->real_render("mail/$path", @_);
}

# декодирует все ключи
sub no_utf8(@) {
	my @x = @_;
	utf8::encode($_) for @x;
	@x
}

# формирует письмо
sub form_mail {
	my ($self) = @_;
	
	my $param = $self->{param};
	my $attach = $self->{attach};

	if(@$attach==1 && !defined $param->{Data} && !defined $param->{Path}) {
		%$param = (%$param, %{$attach->[0]});
		@$attach = ();
	}

	$param->{"From"} //= "noreply\@" . $app->ini->{site}{host};
	$param->{"To"} //= "dev\@" . $app->ini->{site}{host};
	$param->{"Type"} //= @$attach? "multipart/mixed": "text/plain; codepage=utf-8";
	
	my $subject = $param->{Subject} //= "(Без темы)";
	
	my $mail;
	{
		utf8::encode($subject);
		$subject = MIME::Base64::encode($subject, "");
		$subject = "=?UTF-8?B?$subject?=";
		local $param->{"Subject"} = $subject;
		
		$mail = $self->{mail} = MIME::Lite->new(no_utf8(%$param));
	}
	
	for my $i_attach (@$attach) {
		$mail->attach(no_utf8(%$i_attach));
	}

	$self
}

# отправляет
sub send {
	my ($self) = @_;
	
	$self->form_mail;
	
	if(	$self->{to_log} ) {
		msg ":red", "письмо:", ":reset", $self->stringify;
	}
	if(	$self->{to_file} ) {
		my $param = $self->{param};
		$self->to_file($app->file("var/mail/$param->{From} - $param->{To}/$param->{Subject}.eml")->mkpath);
	}
	elsif($self->{sendmail}) {
		$self->mail->send;
	}
	elsif(my $smtp = $self->{smtp}) {
		$self->mail->send('smtp', $smtp->{smtp}, 
			($smtp->{user}? (AuthUser=>$smtp->{user}, AuthPass=>$smtp->{pass}): ()),
			($smtp->{timeout}? (Timeout=>$smtp->{timeout}): ()),
			($smtp->{port}? (Port => $smtp->{port}): ())
		);
	}
	else {
		die "Не указан smtp или sendmail в параметрах объекта " . __PACKAGE__;
	}
	
	#open my f, "| /usr/sbin/sendmail -t" or die "Нет sendmail";
		#print f msg.as_string;
		#close f;
	
	$self
}

# возвращает текст письма
sub stringify {
	my ($self) = @_;
	
	$self->form_mail;
	
	my $stringify = $self->mail->as_string;
	utf8::decode($stringify);
	$stringify
}

# возвращает текст письма
sub to_file {
	my ($self, $path) = @_;
	
	$self->form_mail;
	
	$self->mail->print($app->file($path)->encode(undef)->open(">"));
	
	$self
}

		


1;