package R::Collection;
# базовый класс для различных коллекций. Поддерживает методы clone, map, grep и др.

use common::sense;
use R::App qw/$app msg1 msg TODO closure Num Isa Can/;

# my %EXPORT_CODE = (
	# has_get => \&has_get,
# );

# # импортирует
# sub import {
	# my $self = shift;
	
	# my $pkg = caller;
	# for my $name (@_ ? @_ : keys %EXPORT_CODE) {
		# *{"${pkg}::$name"} = $EXPORT_CODE{$name};
	# }
	
	# $self
# }

# # создаёт свуйство коллекции: устанавливает для всех или возвращает для первого
# sub has_get {
	# my ($cls) = caller(0);
	# for my $name (@_) {
		# eval "sub $cls::$name { my (\$self, \$val)=\@_; if(\@_>1) { for my \$x (\@{\$self->{parts}}) { \$x->{$name}=\$val } \$self } else { \@{\$self->{parts}}? \$self->{parts}[0]{$name}: undef } }";
		# die if $@;
	# }
	# return;
# }

# конструктор
sub new {
	my $cls = shift;
	bless {parts=>[@_]}, ref $cls || $cls;
}

# клонирует
sub clone {
	my $self = shift;
	my $new = bless {%$self, @_}, ref $self || $self;
	if(ref $new->{parts} eq "HASH") {
		my $p = $new->{parts};
		$new->{parts} = [ map { +{%$_, %$p} } @{$self->{parts}} ] ;
	}
	wantarray? @{$new->{parts}}: $new;
}

# просто возвращает себя - сокращение для scalar
sub asis { $_[0] }

# возвращает часть
sub part {
	my ($self, $i) = @_;
	$self->{parts}->[$i]
}

# возвращает массив частей
sub parts {
	my ($self) = @_;
	@{$self->{parts}}
}


# сокращение для  $self->clone(parts=>[$one])  в скалярном контексте
sub one {
	my ($self, $one) = @_;
	scalar $self->clone(parts => [defined($one)? $one: ()]);
}

# возвращает массив коллекций элементов
sub slices {
	my ($self) = @_;
	map { $self->one($_) } @{$self->{parts}}
}

# возвращает i-й обёрнутый слайс
*eq = \&slice;
sub slice {
	my ($self, $i) = @_;
	$self->one($self->{parts}[$i]);
}

# добавляет в коллекцию
sub add {
	my $self = shift;
	
	my $cls = ref $self;
	local $_;
	
	my @new = grep { !Isa $_, $cls } @_;
	my @is = grep { Isa $_, $cls } @_;

	$self->clone(parts => [$self->parts, (map { $_->parts } @is), $self->new(@new)->parts]);
}

# переворачивает коллекцию
sub reverse {
	my ($self) = @_;
	$self->clone(parts => [reverse @{$self->{parts}}])
}

# выполняет функцию для каждого из файлов
sub then {
	my ($self, $sub) = @_;
	local $_;
	for my $part (@{$self->{parts}}) {
		my $x = $_ = $self->one($part);
		$sub->();
		delete($x->{last}), last if $x->{last};
	}
	$self
}

# Устанавливает атрибут last, как сигнал для выхода из цикла then
sub last {
	my ($self) = @_;
	$self->{last} = 1;
	$self
}

# для сортировки
sub _cmp {
	$_[0]->name cmp $_[1]->name
}

# возвращает name первого процесса или устанавливает для всех
sub name {
	my ($self, $name) = @_;
	return !!@{$self->{parts}} && $self->{parts}[0]{name} if @_ == 1;
	for my $ps ($self->parts) {
		$ps->{name} = $name;
	}
	$self
}

# для переопределения
sub _filters {
	my $self = shift;
	return if !@_;
	
	my @codes;
	
	for(my $i=0; $i<@_; $i+=2) {
		my ($k, $v) = @_[$i, $i+1];
		
		if(ref $k) {
			die "параметр - не функция: $k" if ref $k ne "CODE";
			push @codes, $k;
			$i--;
			next;
		}
		
		push @codes, (sub { my ($x, $y)=@_; sub { $self->one($_)->$x ~~ $y } })->($k, $v);
	}
	R::reduce { (sub { my ($x, $y)=@_; sub { $x->() && $y->() } })->($a, $b) } @codes;
}

# возвращает файлы прошедшие проверку
sub grep {
	my $self = shift;
	my $filters = $self->_filters(@_);
	if($filters) {
		$self->clone(parts => [grep { $filters->() } @{$self->{parts}}]);
	} else {
		$self->clone
	}
}

# аналог grep, только в $_ помещается ->one($_)
sub filter {
	my ($self, $fn) = @_;

	local $_;

	my $parts = [];

	for my $part (@{$self->{parts}}) {
		$_ = $self->one($part);
		next if !$fn->();
		push @$parts, $part;
	}
	
	$self->clone(parts => $parts);
}

# возвращает существующие
sub existing {
	my ($self) = @_;
	$self->filter(sub { $_->exists })
}

# маппинг для файлов
sub map {
	my ($self, $sub) = @_;
	local $_;
	my @ret;
	for my $part ( @{$self->{parts}} ) {
		$_ = $self->one($part);
		push @ret, $sub->();
	}
	@ret
}

# объединяет
sub reduce {
	my $self = shift;
	my $pkg = caller;
	my $a = \${"${pkg}::a"};
	my $b = \${"${pkg}::b"};
	my $parts = $self->{parts};
	my $i = 0;
	$$a = @_==1? do {$i++; $self->one($parts->[0]) }: shift @_;

	my $sub = shift;
	
	for(; $i<@$parts; $i++) {
		$$b = $self->one($parts->[$i]);
		$$a = $sub->();
	}
	
	$$a
}

# для всех частей в коллекции выполняется указанное условие
sub all {
	my ($self, $code) = @_;
	local $_;
	for my $part (@{$self->{parts}}) {
		$_ = $self->one($part);
		return "" if !$code->();
	}
	return 1;
}

# хотя бы для одной части в коллекции выполняется указанное условие
sub any {
	my ($self, $code) = @_;
	local $_;
	for my $part (@{$self->{parts}}) {
		$_ = $self->one($part);
		return 1 if $code->();
	}
	return "";
}

# сортирует части
sub sort {
	my ($self, $code) = @_;
	if(@_ == 1) {
		my $cmp = $self->can("_cmp");
		die "нет ".ref($self)."->_cmp для sort" if ref $cmp ne "CODE";
		return $self->clone(parts => [sort { $cmp->($a,$b) } @{$self->{parts}} ]);
	}
	die "укажите функцию сравнения" if ref $code ne "CODE";
	my $pkgname = caller();
	my $x = \${"${pkgname}::a"};
	my $y = \${"${pkgname}::b"};
	$self->clone(parts => [ map { $_->{parts}[0] } sort { $$x=$a; $$y=$b; $code->() } $self->slices ]);
}

# прямая сортировка
sub ascending {
	my ($self, $code) = @_;
	die "укажите функцию сравнения" if ref $code ne "CODE";
	$self->sort(sub { $_ = $a; my $r=$b; my $x=$code->(); $_=$r; my $y=$code->(); $x cmp $y })
}

# обратная сортировка
sub descending {
	my ($self, $code) = @_;
	die "укажите функцию сравнения" if ref $code ne "CODE";
	$self->sort(sub { $_ = $a; my $r=$b; my $x=$code->(); $_=$r; my $y=$code->(); $y cmp $x })
}

# прямая сортировка
sub asc {
	my ($self, $code) = @_;
	return $self->sort if @_ == 1;
	die "укажите функцию сравнения" if ref $code ne "CODE";
	$self->sort(sub { $_ = $a; my $r=$b; my $x=$code->(); $_=$r; my $y=$code->(); $x<=>$y })
}

# обратная сортировка
sub desc {
	my ($self, $code) = @_;
	return $self->sort->reverse if @_ == 1;
	die "укажите функцию сравнения" if ref $code ne "CODE";
	$self->sort(sub { $_ = $a; my $r=$b; my $x=$code->(); $_=$r; my $y=$code->(); $y<=>$x })
}


# количество частей в коллекции
sub length {
	my ($self) = @_;
	0+@{$self->{parts}}
}

# удаляет дубли
sub unique {
	my ($self) = @_;
	my %u;
	my $parts = [];
	for my $e ( $self->parts ) {
		next if exists $u{$e};
		$u{$e} = 1;
		push @$parts, $e;
	}
	$self->clone(parts=>$parts)
}


1;