package R::Mime::Multipart::Formdata;
# сериалайзер для multipart/form-data

use common::sense;
use R::App;

# конструктор
sub new {
	my $cls = shift;
	bless {@_}, ref $cls || $cls;
}

# в
sub to {
	...
}

# соглашение для http-запроса 
sub from_request {
	my ($self, $request) = @_;
	my ($param, $file) = $self->_from($request->{ns}, $request->type);
	$param
}

# разбор данных
# если это файл, то значение - R::File. В file->remote хранится переданный filename
sub _from {
	my ($self, $stdin, $type) = @_;
	local ($_);
	die "не multipart/form-data!" if $type !~ m!^multipart/form-data;\s*boundary=!i;
	my $boundary = qr/^--$'(--)?\r?\n/;
	my $param = {};
	my $file = {};
	my $is_val = 0;
	my $file_name;
	my $content_type;
	my @buf;
	
	my ($head, $is_head);
	my ($name, $encoding) = ("");
	
	while(defined($_ = readline $stdin)) {
		if($_ =~ $boundary) {
			my $the_end = $1;
			@buf = "" if @buf == 0;
			$buf[$#buf] =~ s/\r?\n//;
			if($name ne "") {

				my $body = join '', @buf;
			
				my $val = $is_val? $body: $app->file(\$body)->remote($app->file($file_name))->type($content_type);
				my $file_val = {body => $body, head=>$head};
				
				# устанавливается и для параметров
				if(exists $file->{$name}) {
					my $a = $param->{$name};
					my $p = $file->{$name};
					$param->{$name} = $a = bless([$a], "R::Mime::Array"), $file->{$name} = $p = bless [$p], "R::Mime::Array" unless ref $p eq "ARRAY";
					push @$p, $file_val;
					push @$a, $val;
				}
				else {
					#print STDERR "name new: $name\n";
					$file->{$name} = $file_val;
					$param->{$name} = $val;
				}
				
			}
			last if $the_end;
			$is_head = 1;
			$head = {};
			@buf = ();
			$is_val = 0;
			$name = "";
			$file_name = "";
			$content_type = "";
			#$encoding = "";
		} elsif($is_head && /^\r?$/) {
			$is_head = undef;
		} elsif($is_head) {
			$name = $1, $is_val = !/\bfilename=['"]?([^'";]+)/i, $file_name=$1 if /^Content[-_]Disposition: .*?\bname=['"]?([^\s'";]+)/i;
			$content_type = $1 if /^Content[-_]Type:\s*(.*?)\s*$/i;
			#$encoding = $1 if /Content-Transfer-Encoding: ([\w-]+)/;
			s/\r?\n//;
			/: /; $head->{$`} = $';
		} else {
			push @buf, $_;
		}
	}
	

	#R::msg1 "param:", $param;
	#R::msg1 "file:", $file;
	
	# в параметрах 
	return $param, $file;
}

# # считывает из потока параметры POST
# sub _param_from_post {
	# no utf8;
	# my ($stdin, $type, $len) = @_;
	# #print STDERR "param_from_post: $stdin, $type, $len\n";
	# return unless $len;
	# local ($_, $`, $', $1);
	# if($type =~ m!^multipart/form-data;\s*boundary=!i) {
		
	# } elsif($type =~ m!\bapplication/json\b!i) {
		# read $stdin, $_, $len;
		# $app->json->from($_);
	# } else {
		# read $stdin, $_, $len;
		# _param($_);
	# }
# }



1;