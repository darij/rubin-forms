package R::Mime::Message::Httpheaders;
# заголовки http

use common::sense;
use R::App;

# конструктор
sub new {
	my $cls = shift;
	bless {@_}, ref $cls || $cls;
}

# формирует из текста или массива строк заголовки
sub from {
	my ($self, $text) = @_;
	
	my @lines = ref $text? @$text: do { use bytes; split /\n/, $text };
	my $k;
	my $headers = {};
	for my $line (@lines) {
		#utf8::decode($_), /Cookie:/? msg1 ":on_red yellow", "to utf8 -> $_": ()  if !utf8::is_utf8($_);
		utf8::decode($line) unless utf8::is_utf8($line);
		$line =~ s/\s+$//;
		$headers->{$k} .= $line, next if $line =~ s/^\s+/ /;
		if( $line =~ /:\s*/ ) {
			my $v = $';
			$k = ucfirst lc $`;
			$k =~ s!-(\w)!"-" . uc $1!ge;
			if(defined( my $p = $headers->{$k} )) {
				if($p =~ /,/ || $v =~ /,/) {
					my %k = map { $_ => 1 } split(/\s*,\s*/, $p), split(/\s*,\s*/, $v);
					$v = join ",", sort keys %k;
				}
			}
			$headers->{$k} = $v;
		}
	}

	$headers
}

# сериализует заголовки из массива или хеша
sub to {
	my ($self, $headers) = @_;
	
	my @x;
	$headers = [map { $_ => $headers->{$_} } sort keys %$headers] if ref $headers eq "HASH";
	
	for(my $i=0; $i<@$headers; $i+=2) {
		push @x, "$headers->[$i]: $headers->[$i+1]\n";
	}

	join "", @x
}

1;