package R::Mime::Application::Swagger;
# разбирает и преобразует конфиг api-билдера swagger
# https://editor.swagger.io/

use common::sense;
use R::App;

# свойства
has qw/swagger/;

# конструктор
sub new {
    my $cls = shift;
    bless {@_}, ref $cls || $cls;
}

our $ORDER;
our $DESCRIPTION;
sub s_to_swagger {
    my ($v, $space, $k, $desc) = @_;
    
    my @x;
    
    if(defined $k) {
        push @x, "$space$k:\n";
    }

    $desc = $desc =~ /\n/? "|\n" . do { $desc =~ s/^/$space    /gm; $desc }: $desc =~ /['"]/? $app->magnitudeLiteral->to($desc): $desc;
    $desc = "$space  description: $desc\n";

    if(ref $v eq "HASH") {
        push @x, $desc;
		my ($ex_k) = each(%$v);
		my $hash = Num($ex_k)? {$ex_k => $v->{$ex_k}}: $v;
        push @x, "$space  type: object
$space  properties:
", map { s_to_swagger($hash->{$_}, "$space    ", $_, $hash->{"$_.description"} // join "\n", @{$DESCRIPTION->{$_}}) } grep { !/.description$/ } 
        #(?(sort { $ORDER->{$a} <=> $ORDER->{$b} } keys %$v):  
        sort
        keys %$hash;
    }
    elsif(ref $v eq "ARRAY") {
        push @x, $desc;
        push @x, "$space  type: array
$space  items:
", s_to_swagger($v->[0], "$space  ")
    }
    else {
        my $type = $v =~ /^-?\d+\z/? "integer": Num($v)? "number": ref $v eq "JSON::PP::Boolean"? "boolean": "string";
        
        push @x, "$space  type: $type\n";
        #push @x, "$space  minimum: 0\n" if Num $v;

        my $example = ref $v eq "JSON::PP::Boolean"? ($v? "true": "false"): $v =~ /["']/? $app->magnitudeLiteral->to($v): $v;
        push @x, "$space  example: $example\n";
        push @x, $desc;
    }

    join "", @x;
}

# Переводит данные в текст-yml
# возвращает текст
sub to {
    my ($self, $data, $space, $description_href) = @_;
    
    #local $ORDER = $order // {};
	local $DESCRIPTION = $description_href;
    my $res = s_to_swagger($data, $space);
    $res
}

# добавляет конф-данные
sub from {
    my ($self, $conf) = @_;
    
    $conf = $app->yaml->from($conf) if !ref $conf;
    
    $self->{swagger} = $conf;
    Tour {
        $b = $self->swagger_ref($b->{'$ref'}) if ref $b eq "HASH" && $b->{'$ref'};
    } $conf;
    $self
}

# загружает из файла
sub load {
    my ($self, $file) = @_;
    $self->from($app->file($file)->read);
}

# получает ссылку из пути
#$ref: '#/components/schemas/Processing'
sub swagger_ref {
    my ($self, $ref) = @_;
    my $y = $self->{swagger};
    my @k = split '/', $ref;
    shift @k;
    ($y = $y->{$_}) // warn "нет пути $ref" for @k;
    $y;
}

# преобразует в данные из формата swagger-а
sub to_data {
    my ($self, $x) = @_;
    
    if($x->{type} eq "object") {
        $x->{properties}? +{ map2 { $a => $self->to_data($b) } %{$x->{properties}} }: undef
    }
    elsif($x->{type} eq "array") {
        $x->{items}? [ map { $self->to_data($_) } @{ref $x->{items} eq "ARRAY"? $x->{items}: [$x->{items}]} ]: undef
    }
    else {
        $x->{example}
    }    
}

# возвращает ответ для json
sub result_data {
    my ($self, $r, $code) = @_;
    $code //= 200;    
    $self->to_data($r->{responses}{$code}{content}{"application/json"}{schema})
}

# превращает в pod
my $SDVIG_REM = 50;
sub _add_sdvig_rem($$) {  
    my ($r, $rem) = @_;
    return "$r\n" if 0 == length $rem;
    join "", $r, " " x ($SDVIG_REM - length $r), " # ", $rem, "\n"
}
sub to_pod {
    my ($self, $x, $sdvig, $beg) = @_;
    
    if($x->{type} eq "object") {
        my $s = _add_sdvig_rem "${beg}{", $x->{title};
        join "", $s, 
            $x->{properties}? ( ascending {$_} map2 {
                $self->to_pod($b, "$sdvig    ", "$sdvig    $a => ") 
            } %{$x->{properties}}): "undef", 
        "$sdvig},\n"
    }
    elsif($x->{type} eq "array") {
        my $s = _add_sdvig_rem "${beg}[", $x->{title};
        join "", $s,
            $x->{items}? (map { 
                $self->to_pod($_, "$sdvig    ", "$sdvig    ")
            } @{ref $x->{items} eq "ARRAY"? $x->{items}: [$x->{items}]} ): "undef", 
        "$sdvig],\n"
    }
    else {
        my $ex = $x->{example} // (
            $x->{type} eq "string"? "simple":
            $x->{type} eq "integer"? 8199:
            $x->{type} eq "number"? 0.12:
            "?"
        );
        my $r = $x->{type} eq "string"? $app->magnitudeLiteral->to($ex): $ex;
        _add_sdvig_rem "$beg$r,", $x->{title} // $x->{description};
    }    
}

# возвращает код perl c комментариями к структуре
sub result_pod {
    my ($self, $r, $code, $sdvig, $beg) = @_;
    $code //= 200;    
    $self->to_pod($r->{responses}{$code}{content}{"application/json"}{schema}, $sdvig, $beg)
}

# сгенерировать pod-документацию
sub pod {
    my ($self, %k) = @_;
    
    use lib "lib";
    #my $mediator = $app->use("CCXX::Mediator")->new(server=>"http://newmedia.220-volt.ru", api_key=>"test");
    my $mediator = $app->use("CCXX::Selfapi")->new(server=>"https://selfapi.220-volt.ru", authorization=>"77d876f0765e8d9aec45a74b860a308203ba2a52");
    
    my %types = (integer => "Целое число", string => "Строка", number => "Вещественное число");
    
    my $param_cref;
    
    $self->map(
        k => \%k,
        param => ($param_cref=sub {
            my ($param, $r, %kw) = @_;
            
            my $name = $param->{name};
            
            my $type = $param->{type} // $param->{schema}->{type};
                $type = $types{$type} // $type;
                my $required = $param->{required}? "Обязательный параметр.": "Необязательный параметр.";
                my $desc = $param->{description} // $param->{title} // ($param->{name} eq "id"? "Идентификатор": "");
                "=item B<$param->{name}>

$desc. $type.

$required

"        
        }),
        fn => sub {
            my ($r, %kw) = @_;
            my $name = $kw{name};
            
            # my $json200;
            # for my $code (sort keys %{$r->{responses}}) {
                # $json200 = $self->result_pod($r, $code, "    ", "    my \$${name}_href = ");
                # last if $json200 =~ /= [\[\{]/;
            # }
            
            my $json200 = $self->result_pod($r, 200, "    ", "    my \$${name}_href = ");
            
            $json200 =~ s!,\s*$!!;
            my $type = "href";
            $json200 =~ s/my \$${name}_href = \[/my \$${name}_aref = \[/ and $type = "aref";
            
            my $example = $k{example};
            
            my %kw1 = map { msg $_->{name}, $_->{old_name}, eval $example->{$_->{old_name}}; ($_->{name} => eval($example->{$_->{old_name}}))} @{$kw{params}};
            msg1 $name, \%kw1, $example;
            my $res = $mediator->$name(%kw1);
            $json200 = "    my \$${name}_href = ".$app->perl->dump($res);
             
            
            my @params = map {$_->{_text}} @{$kw{params}};
            
            
            my $params = join ", ", map {  "$_->{name} => $example->{$_->{old_name}}" } @{$kw{params}};
            my $url_params = join ", ", map { "$_->{name} => $example->{$_->{old_name}}" } @{$kw{url_params}};
            
            my @url_params = map { $param_cref->($_, $r, %k) } @{$kw{url_params}};
            
            my %kw = map { ($_->{name} => eval($example->{$_->{old_name}}))} @{$kw{url_params}};
            
            my $method = "url_$name";
            my $url = $mediator->$method(%kw);
            
            my $man_params = @params? "

Принимает параметры:

=over 4

@params=back": "";

            my $man_url_params = @url_params? "

Принимает параметры:

=over 4

@params=back": "";
            
            "=head2 $name

    my \$${name}_$type = \$mediator->${name}($params);

$kw{summary}.

Возвращает ссылку на хеш следующего вида:

$json200;$man_params

=head2 url_$name

    my \$url_${name} = \$mediator->url_${name}($url_params);

Возвращает url метода:

    $url$man_url_params

"
        }
    );
}

# возвращает код для методов perl
sub perl {
    my ($self, %k) = @_;
    
    my %types = (integer => "Int", string => "NonEmptyStr", number => "Num", array => "ArrayRef [PositiveInt]");
    
    $self->map(
        k => \%k,
        param => sub {
            my ($param, $r, %kw) = @_;
            
            my $name = $param->{name};
            
            my $type = $param->{type} // $param->{schema}->{type};
            $type = $name eq "ids"? "ArrayRef [PositiveInt]": $types{$type} // $type;
            my $required = $param->{required}? "": ", optional => 1";
            my $desc = _rem($param->{description} // $param->{title} // ($name eq "id"? "Идентификатор": ""));
            "        $name => { type => $type$required }, $desc\n" 
            
        },
        fn => sub {
            my ($r, %kw) = @_;
            
            my $fmt = $kw{path};
            $fmt =~ s!\{([^{}]+)\}!%s!g;

            my $summary = $r->{summary};
            $summary =~ s/\s*$//;
            my $len = R::max map {length $_} split /\n/, $summary;
            $summary = _rem($summary . "\n" . ("-" x $len));
            my $name = $kw{name};

            my @params = map {$_->{_text}} @{$kw{params}};
            my @url_params = map {$_->{_text}} @{$kw{url_params}};
            my $up = $kw{url_params};
            my $down = $kw{post_params};
            
            my $url_param_names = join " ", @{$kw{url_param_names}};
            
            my $url_params = join ", ", ((grep {$_ eq "ids"} @{$kw{url_param_names}})? 
                (map { $_ eq "ids"? "join(\",\", \@{\$kwarg{ids}})": "\$kwarg{$_}" } @{$kw{url_param_names}}):
                "\@kwarg{qw/$url_param_names/}");

            my $type_data = $r->{requestBody}{content}{"application/json"}? "_json_xs->encode":
                $r->{requestBody}{content}{"multipart/form-data"}? "_multipart":
                $r->{requestBody}{content}{"application/x-www-form-urlencoded"}? "_urlencoded":
                     "_json_xs->encode";

            my $kw = @url_params!=@params? ", $type_data({".join(", ",
                map { "$_->{old_name} => ".($_->{format} eq "binary"? "\\": "")."\$kwarg{$_->{name}}" } @$down)."})": "";
            my $args = @url_params!=@params? join(", ", map { "$_ => \$kwarg{$_}"} @{$kw{url_param_names}}): "%kwarg";



            return "
$summary
my \$validator_$name = validation_for(
    params => {
        @params    }
);

sub $name {    ## no critic [Subroutines::RequireArgUnpacking]
    my ( \$self, %kwarg ) = ( shift, \$validator_$name->(\@_) );
    my \$url = \$self->_url_$name($args);
    return \$self->_json_xs->decode( \$self->_get( \"$kw{METHOD}\", \$url$kw ) );
}

" . (@url_params? "my \$validator_url_$name = validation_for(
    params => {
        @url_params    }
);

sub url_$name {    ## no critic [Subroutines::RequireArgUnpacking]
    my ( \$self, %kwarg ) = ( shift, \$validator_url_$name->(\@_) );
    return \$self->_url_$name(%kwarg);
}

sub _url_$name {
    my ( \$self, %kwarg ) = \@_;
    return sprintf \"%s$fmt\", \$self->uri, $url_params;
}
": "sub url_$name {
    my ( \$self ) = \@_;
    return \$self->_url_$name;
}

sub _url_$name {
    my ( \$self ) = \@_;
    return sprintf \"%s$fmt\", \$self->uri;
}
");
        },
    );
}

# возвращает код для тестов perl
sub t {
    my ($self, %k) = @_;

    $self->map(
        k => \%k,
        param => sub {
            my ($param, $r, %kw) = @_;

            my $name = $param->{name};

            my $type = $param->{type} // $param->{schema}->{type};
            "$name"

        },
        fn => sub {
            my ($r, %kw) = @_;
            my $name = $kw{name};
            my $method = $kw{method};
            my $url = $kw{url};

            my $uri = "$method $url";

            my $params = join ", ", map { $_->{_text} } @{$kw{params}};

            my $json200 = $self->result_pod($r, 200, "    ", "    my \$${name}_href = ");

            return "

    $json200

    is_deeply \$mediator->$name( $params ), \$RESULT{'$uri'}, 'Получен сырой ответ';
"
        },
    );
}

# делает комментарием
sub _rem {
    my ($desc) = @_;
    $desc =~ s!\s*$!!;
    $desc =~ s!^!# !gm;
    $desc
}

# выполняет $fn для каждого запроса, а $params для каждого параметра и 
sub map {
    my ($self, %kwarg) = @_;
    
    my ($fn, $param, $k) = @kwarg{qw/fn param k/};
    
    my @result;
    my $yml = $self->swagger;
    
    for my $path (sort keys %{$yml->{paths}}) {
        my $v = $yml->{paths}->{$path};
        for my $method (sort keys %$v) {
            my $r = $v->{$method};

            msg1 $path, $method;

            next if ref $r ne "HASH";
            next if $r->{deprecated};
            
            
            my $name = $k->{path2name}->{"$method$path"};
            next if !$name;


            $name //= "<$method$path>";
            
            #next if $name ne "create_prices_typed";
            
            #msg1 $r;
            
            

            my @x = (@{$r->{parameters}},
                map2 {+{
                    name => $a,
                    %$b
                }} do {
                    
                    my $is_content = exists $r->{requestBody}{content};
                    if(my $f=$r->{requestBody}{content}{"application/x-www-form-urlencoded"} // $r->{requestBody}{content}{"application/json"} // $r->{requestBody}{content}{"multipart/form-data"}) {
                        my %x = (%{$f->{properties}}, %{$f->{schema}{properties}});
                        $x{$_}{required}=1 for @{$f->{required}}, @{$f->{schema}{required}};
                        %x
                    } elsif($is_content) {
                        my $f=$r->{requestBody};
                        $f->{type} //= "string";
                        $f->{description} //= "Путь к файлу или содержимое файла";
                        (file => $f)
                    }
                    else {}
                }
            );

            $_->{old_name}//=$_->{name}, $_->{name} = $k->{param2name}{$_->{old_name}} // $_->{name} for @x;

            my @kw;
            while($path=~/\{([^{}]+)\}/g) { push @kw, $k->{param2name}{$1} // $1 }
            my %kw_href = map { $_->{name} => $_ } @x;
            my %xxx = map {$_=>1} @kw;
            
            my @post = map {$_->{name}} grep { !$xxx{$_->{name}} } @x;

            my $summary = $r->{summary};
            $summary =~ s!\s*$!!;
            $summary = ($summary? "АННОТАЦИЯ\n\n$summary\n\n": "") . "ТЕГИ\n\n" . join "", map {"- $_\n"} @{$r->{tags}} if $r->{tags} && @{$r->{tags}};

            $summary .= $r->{description}? "\nОПИСАНИЕ\n\n$r->{description}": "";

            my ($data_type) = %{$r->{requestBody}{content}};

            my %kw = (
                METHOD => uc $method,
                method => $method,
                path => $path,
                name => $name,
                summary => $summary,
                params=>\@x,
                data_type => $data_type,
                url_params => [map { $kw_href{$_} // die "нет $_" } @kw],
                url_param_names => \@kw,
                post_params => [map { $kw_href{$_} // die "нет post $_" } @post],
                post_params_names => \@post,
            );
            
            $_->{_text} = $param->($_, $r, %kw) for @x;

            push @result, $fn->($r, %kw);
        }
    }
    return join "", @result;
}

1;