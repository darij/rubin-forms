package R::Mime::Application::Urlencoded;
# декодер-енкодер для application/x-www-form-urlencoded

use common::sense;
use R::App;

use URI::Escape::XS qw//;
#use URI::Escape qw/uri_escape_utf8/;

# конструктор
sub new {
	my $cls = shift;
	bless {@_}, ref $cls || $cls;
}

# эскейпит
sub escape {
	my ($self, $param) = @_;
	utf8::encode($param);
	URI::Escape::XS::uri_escape($param)
}

# унэскейпит
sub unescape {
	my ($self, $value) = @_;
	utf8::encode($value);
	$value = URI::Escape::XS::uri_unescape($value);
	utf8::decode($value);
	$value
}

# сериализует в www-form-urlencoded
# TODO: { x=> {y=>1, z=>2} } -> x[y]=1&x[z]=2 - доделать
sub to {
	my ($self, $data, $sep) = @_;
	
	$data = [map { $_ => $data->{$_} } sort keys %$data] if ref $data eq "HASH";
	
	my @x;
	for(my $i=0; $i<@$data; $i+=2) {
		my ($key, @val) = @$data[$i, $i+1];
		@val = @{$val[0]} if ref $val[0] eq "R::Mime::Array";
		$key = $self->escape($key);
		for my $val (@val) {
			push @x, sprintf "%s=%s", $key, $self->escape($val);
		}
	}
	
	join $sep // "&", @x
}

# из www-form-urlencoded
sub from {
	no utf8; use bytes;
	my ($self, $data, $sep) = @_;
	# распаковывает данные переданные в виде параметров url
	#	$sep - разделитель параметров. По умолчанию "&". Для кук установить ";\s*"
	
	local ($_, $`, $');
	my $param = {};
	for ($data? split($sep // qr/&/, $data): ()) {
		tr/+/ /;
		/$/ unless /=/;
		my $key = $self->unescape($`);
		my $val = $param->{$key};
		my $newval = $self->unescape($');
		if(defined $val) {
			if(ref $val eq "R::Mime::Array") { push @$val, $newval } else { $param->{$key} = bless [$val, $newval], "R::Mime::Array"}
		} else {
			$param->{$key} = $newval;
		}
	}
	
	return $param;
}

1;