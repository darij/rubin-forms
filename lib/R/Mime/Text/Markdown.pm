package R::Mime::Text::Markdown;
# конвертер маркдовна в html

use common::sense;
use R::App;

use R::Html;
*e = \&R::Html::_escape;


# конструктор
sub new {
	my $cls = shift;
	bless {@_}, ref $cls || $cls;
}

# конвертирует в html
sub to_html {
	my ($self, $markdown) = @_;
	
	my $flag = "";
	
	my $replace = sub {
	
	
		# код
		exists $+{code}? "<code lang=\"${\e($+{lang})}\">${\e($+{code})}</code>":
		exists $+{codeline}? "<code>${\e($+{codeline})}</code>":
	
		#Форматирование символов
		exists $+{bold}? "<b>${\e($+{bold})}</b>":
		exists $+{italic_bold}? "<b><i>${\e($+{italic_bold})}</i></b>":
		exists $+{italic}? "<i>${\e($+{italic})}</i>":
		exists $+{underline}? "<u>${\e($+{underline})}</u>":
		
		exists $+{head}? do {
			my $h = length $+{head}; 
			my $header = e($+{header}); 
			"<h$h><a name=\"$header\">$header</a></h$h>"
		}:
		
		exists $+{link}? "<a href=\"${\e($+{url})}\">${\e($+{link})}</a>":
		exists $+{img}? "<img src=\"${\e($+{src})}\" alt=\"${\e($+{img})}\">":
		
		exists $+{link_punkt}? "<a href=\"#${\e($+{punkt})}\">${\e($+{link_punkt})}</a>":
		exists $+{img_punkt}? "<img src=\"#${\e($+{src})}\" alt=\"${\e($+{img_punkt})}\">":
		
		exists $+{blockquote}? ($flag ne "</blockquote>"? do { $flag = "</blockquote>"; "$flag<blockquote>" }: ""):
		
		exists $+{ul}? ($flag ne "</ul>"? do { 
			$flag = "</ul>";
			my %type = qw/* disc + square - circle/;
			my $type = $type{$+{ul}};
			"$flag<ul type=\"$type\"><li>"
		}: "<li>"):
		
		exists $+{ol}? ($flag ne "</ol>"? do { 
			$flag = "</ol>";
			my $type = $+{ol};
			$type =~ s/\d+/1/;
			"$flag<ol type=\"$type\"><li>"
		}: "<li>"):
		
		exists $+{begin}? do { my $end=$flag; $flag = ""; $end }:
		
		exists $+{hr}? "<hr>":
		exists $+{tag}? $+{tag}:
		exists $+{escape}? e($+{escape}):

		

		die "что-то странное"
		
	};
	
	$markdown =~ s{
		(?<blockquote> ^> ) |
		(?<ul> ^[-+\*] ) |
		^ (?<ol> \d+|A|a|I|i ) [.\)] |
		
		(?<begin> ^) |
	
	
		```(?<lang>[^\n]*)(?<code>.*?) (```|\z) |
		`(?<codeline>[^`\n]+)` |
		
		\*\*\* (?<italic_bold> .*?) \*\*\* |
		\*\* (?<bold> .*?) \*\* |
		\* (?<italic> .*?) \* |
		~~ (?<underline> .*?) ~~ |
		
		^(?<head>\#+)[ \t]* (?<header> [^\n]*) $ |
		
		\[ (?<link> [^\[\]\n]+ ) \] \( (?<url> [^\(\)\n]+ ) \) |
		! \[ (?<img> [^\[\]\n]+ ) \] \( (?<src> [^\(\)\n]+ ) \) |
		
		\[ (?<link_punkt> [^\[\]\n]+ ) \] \[ (?<punkt> [^\[\]\n]+ ) \] |
		! \[ (?<img_punkt> [^\[\]\n]+ ) \] \[ (?<punkt> [^\[\]\n]+ ) \] |
		
		
		
		(?<hr> \*\*\* | --- ) |
		
		(?<tag> <[a-z_][-:\w]* [^<>]* > | </[a-z_][-:\w]*\s*> | &[a-z_]\w*; | &\#\d+; ) |
		
		(?<escape> [<>&] )
		
	}{ $replace->() }simxge;
	
	$markdown
}


1;