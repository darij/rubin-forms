package R::Mime::Text::Toml;
# провайдер для конфига toml (аналог json и yaml)

use common::sense;
use R::App;

use TOML::Parser;
use TOML::Dumper;
use Types::Serialiser;

my $parser = TOML::Parser->new(
	inflate_boolean  => sub { $_[0] eq "true"? $Types::Serialiser::true: $Types::Serialiser::false }
);

my $dumper = TOML::Dumper->new;

# конструктор
sub new {
	my $cls = shift;
	bless {@_}, ref $cls || $cls;
}

# из томла
sub from {
	my ($self, $text) = @_;
	$parser->parse( $text )
}

# в томл
sub to {
	my ($self, $data) = @_;
	$dumper->dump( $data )
}



1;