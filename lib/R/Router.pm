package R::Router;
# простой роутер, получает строку и выдаёт значение или undef

use common::sense;
use R::App qw/$app msg msg1/;

# конструктор
sub new {
	my ($cls, %kw) = @_;
	(bless {
		routes => [],
		param => $kw{param},
	}, ref $cls || $cls)->add(@{$kw{routes}})
}

# преобразовывает location к Regexp
sub location {
	my ($self, $location) = @_;

	$location =~ s/\./\\./g;
	
	my $x = qr! (
		:  (?<word>\w+) |
		# (?<str>\w+)  |
		%  (?<int>\w+) |
		\* (?<all>\w+)
	) !x;

	$location =~ s{
		$x | \( $x \)
	}{
		$+{word}? "(?<$+{word}>[^/.]+)":
		$+{str}? "(?<$+{str}>[^/]+)":
		$+{int}? "(?<$+{int}>\\d+)":
		$+{all}? "(?<$+{all}>.*)":
		die "?"
	}xge;
	
	qr/^$location\z/s
}

# добавляет роуты в формате {route => sub|widget|array}, ...
# или
sub add {
	my $self = shift;
	
	for(my $i=0; $i<@_; $i++) {
		my $r = $_[$i];
		my ($route, $sub) = ref $r eq "HASH"? %$r: ref($r) && ref $r ne "Regexp"? die "нарушен протокол аргументов":
			($r => $_[++$i]);
		die "добавляейте регулярки или строки" if ref $route ne "" && ref $route ne "Regexp";
		die "значением роутера не может быть undef" if !defined $route;
		
		$route = $self->location($route) if !ref $route;
		
		$sub = $self->new(routes => $sub) if ref $sub eq "ARRAY";
		$sub = $self->new(routes => $sub->{routes}, param => $sub->{param}) if ref $sub eq "HASH";
		
		push @{$self->{routes}}, $route => $sub;
	}
	
	$self
}

# выдаёт только первое значение или undef
# ^xy*
sub get {
	my ($self, $path) = @_;
	
	die "метод get может быть запущен только в контексте массива" if !wantarray;
	
	my $routes = $self->{routes};

	for(my $i=0; $i<@$routes; $i+=2) {
		#msg1 $path, $routes->[$i], $path !~ $routes->[$i];
		next if $path !~ $routes->[$i];
		my $x = $routes->[$i+1];
		my $attr = {%+};
		return $x, $attr, $self->{param} if ref $x ne __PACKAGE__;
		my ($x, $attr2, $param) = $x->get($+{__NEXT__});
		$attr = {%$attr, %$attr2};
		delete $attr->{__NEXT__};
		return $x, $attr, $param // $self->{param};
	}
	
	return undef, undef, $self->{param};
}

# # выдаёт все подходящие значения
# sub getAll {
	# my ($self, $path) = @_;
	# my @RET;
	# for(my $i=0; $i<@$self; $i+=2) {
		# push @RET, $self->[$i+1] if $path =~ $self->[$i];
	# }
	# @RET
# }

# # посылает сообщение всем подходящим
# sub send {
	# my ($self, $path, @args) = @_;
	
	# for my $fn ($self->getAll($path)) {
		# $fn->(@args);
	# }
	
	# $self
# }

1;