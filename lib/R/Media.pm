package R::Media;
# сервис статики

use common::sense;
use R::App;

# свойства
has qw/dev/;

# конструктор
sub new {
	my $cls = shift;
	bless {
		dev => 1,
		@_
	}, ref $cls || $cls;
}

# возвращает файл
# такая схема использутся, чтобы не 
sub get {
	my ($self, $path) = @_;
	$app->file($path)
}

# возвращает gz-файл
sub get_gz {
	my ($self, $path, $type, $gzip_comp_level) = @_;
	my $ext = $type eq "gzip"? "gz": $type;
	my $f = $app->file("var/media-gz/$path.$ext");
	#msg1 $self->dev;
	return $f if !$self->{dev};
	my $e = $f->exists;
	$f->mkpath if !$e;
	#msg1 $f->path, $type, $e;
	$app->file($path)->anyzip($type, $f, $gzip_comp_level) if !$e || $f->mtime < $app->file($path)->mtime;
	$f
}

1;