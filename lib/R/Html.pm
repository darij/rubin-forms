package R::Html;
# всё что связано с html

use common::sense;
use R::App;

use Scalar::Util qw/blessed/;

use overload
	'""' => \&stringify,
	'.' => sub { $_[2]? $_[1] . $_[0]->stringify: $_[0]->stringify . $_[1] },
	'bool' => sub { !!@{$_[0]} },
	"0+" => sub { 0+@{$_[0]} },
	"qr" => \&stringify,
	fallback => 1
;

# конструктор
sub new {
	my $cls = shift;
	bless [@_], ref $cls || $cls;
}


my %SIGN = (
'nbsp' => ' ',
'ensp' => ' ',
'emsp' => ' ',
'ndash' => '–',
'mdash' => '—',
'shy' => '­',
'copy' => '©',
'reg' => '®',
'trade' => '™',
'ordm' => 'º',
'ordf' => 'ª',
'permil' => '‰',
'pi' => 'π',
'brvbar' => '¦',
'sect' => '§',
'deg' => '°',
'micro' => 'µ',
'para' => '¶',
'hellip' => '…',
'oline' => '‾',
'acute' => '´',
'times' => '×',
'divide' => '÷',
'lt' => '<',
'gt' => '>',
'plusmn' => '±',
'sup1' => '¹',
'sup2' => '²',
'sup3' => '³',
'not' => '¬',
'frac14' => '¼',
'frac12' => '½',
'frac34' => '¾',
'frasl' => '⁄',
'minus' => '−',
'le' => '≤',
'ge' => '≥',
'asymp' => '≈',
'ne' => '≠',
'equiv' => '≡',
'radic' => '√',
'infin' => '∞',
'sum' => '∑',
'prod' => '∏',
'part' => '∂',
'int' => '∫',
'forall' => '∀',
'exist' => '∃',
'empty' => '∅',
'Oslash' => 'Ø',
'isin' => '∈',
'notin' => '∉',
'ni' => '∗',
'sub' => '⊂',
'sup' => '⊃',
'nsub' => '⊄',
'sube' => '⊆',
'supe' => '⊇',
'oplus' => '⊕',
'otimes' => '⊗',
'perp' => '⊥',
'ang' => '∠',
'and' => '∧',
'or' => '∨',
'cap' => '∩',
'cup' => '∪',
'euro' => '€',
'cent' => '¢',
'pound' => '£',
'current' => '¤',
'yen' => '¥',
'fnof' => 'ƒ',
'bull' => '•',
'middot' => '·',
'spades' => '♠',
'clubs' => '♣',
'hearts' => '♥',
'diams' => '♦',
'loz' => '◊',
'quot' => '"',
'amp' => '&',
'laquo' => '«',
'raquo' => '»',
'prime' => '′',
'Prime' => '″',
'lsquo' => '‘',
'rsquo' => '’',
'sbquo' => '‚',
'ldquo' => '“',
'rdquo' => '”',
'bdquo' => '„',
'larr' => '←',
'uarr' => '↑',
'rarr' => '→',
'darr' => '↓',
'harr' => '↔',
'crarr' => '↵',
'lArr' => '⇐',
'uArr' => '⇑',
'rArr' => '⇒',
'dArr' => '⇓',
'hArr' => '⇔',
'Alpha' => 'Α',
'Beta' => 'Β',
'Gamma' => 'Γ',
'Delta' => 'Δ',
'Epsilon' => 'Ε',
'Zeta' => 'Ζ',
'Eta' => 'Η',
'Theta' => 'Θ',
'Iota' => 'Ι',
'Kappa' => 'Κ',
'Lambda' => 'Λ',
'Mu' => 'Μ',
'Nu' => 'Ν',
'Xi' => 'Ξ',
'Omicron' => 'Ο',
'Pi' => 'Π',
'Rho' => 'Ρ',
'Sigma' => 'Σ',
'Tau' => 'Τ',
'Upsilon' => 'Υ',
'Phi' => 'Φ',
'Chi' => 'Χ',
'Psi' => 'Ψ',
'Omega' => 'Ω',
'alpha' => 'α',
'beta' => 'β',
'gamma' => 'γ',
'delta' => 'δ',
'epsilon' => 'ε',
'zeta' => 'ζ',
'eta' => 'η',
'theta' => 'θ',
'iota' => 'ι',
'kappa' => 'κ',
'lambda' => 'λ',
'mu' => 'μ',
'nu' => 'ν',
'xi' => 'ξ',
'omicron' => 'ο',
'pi' => 'π',
'rho' => 'ρ',
'sigmaf' => 'ς',
'sigma' => 'σ',
'tau' => 'τ',
'upsilon' => 'υ',
'phi' => 'φ',
'chi' => 'χ',
'psi' => 'ψ',
'omega' => 'ω',
'Rub' => '₽',
'rub' => '₽',
);

# возвращает текст без тегов
sub text {
	my ($self, $html) = @_;
	
	local ($1, $`, $', $&);
	
	$html =~ s!</?\w[^<>]*>!!g;
	
	# переводим html-сущности в символы
	$html =~ s!&#(\d+);!chr($1)!ge;
	
	$html =~ s!&(\w+);!$SIGN{$1}!ge;
	
	$html
}

# декодирует из html
sub unescape {
	my ($self, $html) = @_;
	TODO;
}
*from = \&unescape;

# кодирует в html
sub escape {
	shift;
	goto &_escape;
}
*to = \&escape;

# стрингифицирует
sub stringify {
	my ($self) = @_;
	join "", @$self
}

# магический метод render
sub render { @{$_[0]} }

sub _escape {
	my ($val) = @_;
	if(ref $val and blessed $val and $val->can("render"))  {
		return $val->render;
		#return join "", @$val if Isa $val, "R::Html";
		#return @{$val->render} if Isa $val, "R::Form::Form", "R::Form::Input", "R::Form::Tag";
	}
	#local ($', $', $1, $2, $3, $4);
	$val =~ s!(&)|(<)|(>)|(")|(')! $1? '&amp;': $2? '&lt;': $3? '&gt;': $4? '&quot;': '&#39;' !ge;
	$val
}

# эскейпит все свои параметры
sub _escapes {
	map { _escape($_) } @_;
}

# пепреводит hsl в rgb
sub _hsl2rgb {
	my ($h, $s, $l) = @_;

    my ($r, $g, $b);

    if($s == 0) {
        $r = $g = $b = $l; # achromatic
    } else {
        my $hue2rgb = sub {
			my ($p, $q, $t) = @_;
            $t < 0 and $t += 1;
            $t > 1 and $t -= 1;
            $t < 1/6 and return $p + ($q - $p) * 6 * $t;
            $t < 1/2 and return $q;
            $t < 2/3 and return $p + ($q - $p) * (2/3 - $t) * 6;
            return $p;
        };

        my $q = $l < 0.5 ? $l * (1 + $s) : $l + $s - $l * $s;
        my $p = 2 * $l - $q;
        $r = $hue2rgb->($p, $q, $h + 1/3);
        $g = $hue2rgb->($p, $q, $h);
        $b = $hue2rgb->($p, $q, $h - 1/3);
    }

    return sprintf("%.0f", $r * 255), sprintf("%.0f", $g * 255), sprintf("%.0f", $b * 255);
}

# пепреводит rgb в hsl
sub _rgb2hsl {
	my ($r, $g, $b) = @_;
    $r /= 255; $g /= 255; $b /= 255;
	my $max = max($r, $g, $b); 
	my $min = min($r, $g, $b);
    my ($h, $s, $l);
	$l = ($max + $min) / 2;

    if($max == $min) {
        $h = $s = 0; # achromatic
    } else {
        my $d = $max - $min;
        $s = $l > 0.5 ? $d / (2 - $max - $min) : $d / ($max + $min);
		
        if($max == $r) { $h = ($g - $b) / $d + ($g < $b ? 6 : 0); }
        if($max == $g) { $h = ($b - $r) / $d + 2; }
		if($max == $b) { $h = ($r - $g) / $d + 4; }
        
        $h /= 6;
    }

    return $h, $s, $l
}


# для ошибок
sub escape_ansi {
	my ($self, $html, $table) = @_;

	$table //= {
		black => "rgb(7,54,66)",
		bright_black => "rgb(80,96,114)",
		red => "rgb(220,50,47)",
		bright_red => "rgb(203,75,22)",
		green => "rgb(0,168,81)",
		bright_green => "rgb(58,232,186)",
		yellow => "rgb(250,250,0)",
		bright_yellow => "rgb(250,246,172)",
		blue => "rgb(38,139,210)",
		bright_blue => "rgb(13,148,150)",
		magenta => "rgb(211,54,130)",
		bright_magenta => "rgb(108,113,196)",
		cyan => "rgb(115,205,204)",
		bright_cyan => "rgb(180,212,212)",
		white => "rgb(238,232,213)",
		bright_white => "rgb(253,246,227)",
	};
	
	my $bold_table={};
	my $dark_table={};
	
	# переводим цвета в цвета html
	my @TAG;	# </u> | </i> | </font>
	my $bold_dark = 0;
	my $fg = "";	# цвет html
	my $bg = "";	# цвет html для подложки
	my $u;
	my $i;
	my $prefix = "";
	
	my $tag = sub {
		my $close = join "", delete(@TAG[0..$#TAG]);
		my @o;
		push(@o, "<font color=$prefix$fg>"), unshift @TAG, "</font>" if $fg;
		push(@o, "<font bgcolor=$prefix$fg>"), unshift @TAG, "</font>" if $bg;
		push(@o, "<i>"), unshift @TAG, "</i>" if $i;
		push(@o, "<u>"), unshift @TAG, "</u>" if $u;
		join "", $close, @o;
	};
	my $clear = sub { $bold_dark=$fg=$bg=$u=$i=$b=$prefix=""; };
	
	$html = _escape($html);
	
	my $fn = sub {
		exists $+{color}? do {
			my @color = split /;/, $+{color};
			my $color = $color[$#color];
			my $bb = @color==2? ($color[0]==1? "light": $color[0]==2? "dark": ""): "";
			
			local $_ = $Term::ANSIColor::ATTRIBUTES_R{$color};
		
			if($color eq 0) { $clear->(); $tag->() }
			elsif($color eq 1) { $bold_dark = 1; "" }
			elsif($color eq 2) { $bold_dark = -1; "" }
			elsif($color eq 3) { $i=1; $tag->() }
			elsif($color eq 4) { $u=1; $tag->() }
			elsif(!defined $_) { "\e${color}m" }
			else {
				my $bgf = s/^on_//;
				my $c = "$bb$_";
				
				# TODO: добавить для 256-цветов терминала
				# my $c = /rgb/? sprintf("rgb(%d,%d,%d)", split /;/, $color): 
					# $bold_dark == 1? $bold_table->{$_} //= do { $table->{$_} }: 
					# $bold_dark == -1? $dark_table->{$_} //= do { $table->{$_} }:
					# $table->{$_};
			
				if(!defined $c) {
					"\e${color}m"
				} else {
					if($bgf) { $bg = $c } else { $fg = $c }
					$tag->()
				}
			}
		}:
		exists $+{br}? "<br>\n":
		exists $+{space2}? " &nbsp;\n":
		exists $+{tab}? " &nbsp; &nbsp; &nbsp; &nbsp;":
		exists $+{error_path}? do {
			my ($path, $line, $in) = ($+{error_path}, $+{error_line}, $&);
			$path =~ s!^var/c_view/!view/!;
			$path =~ s!^var/c_scenario/!view/!;
			"<a href='/admin/open_in_editor/?path=$path&line=$line' target=__error_frame__>$in</a>"
		}:
		die "no ansi";
	};
	
	$html =~ s!
		(?<br>\n) | (?<space2>\ {2}) | (?<tab>\t) | 
		\e\[(?<color>[\d;]+)m |
		(?<= [\ \t] )    (?<error_path> [^\e\s:]+ ) : (?<error_line> \d+ )   (?= [\ \t] )	|
			\b at \ (?<error_path> .*? ) \ line \ (?<error_line> \d+ )
		
		
	! $fn->() !xgen;

	$clear->();
	$html = join "", $html, $tag->(), "<iframe name=__error_frame__ style='position:absolute;margin-left:-9999px; width:1px;height:1px'></iframe>";
	
	$html
}

# div c ошибкой
sub ansi {
	my ($self, $err, $style, $id) = @_;
	return $err if $err eq "";
	$err = $self->escape_ansi($err);
	$id = " id=$id" if $id;
	"<div$id style='padding: 5px; font-family: monospace; color: white; background: #000; border: solid 1px #ccc;$style'>$err</div>"
}

# чекбокс для css:
# <label for="id">кликать сюда</label> ... <% app.html.ch(id) %> <div>а тут всё меняется</div>
sub ch {
	my ($self, $label) = splice @_, 0, 2;
	my $checked = 0;
	$checked = shift if !defined $_[0] or Num $_[0];
	$checked = $checked? " checked": "";
	
	# my @attr = @_;
	# for(my $i=0; $i<@attr; $i+=2) {
		# $attr[$i+1] .= " ch-up" if $attr[$i] eq "class";
	# }
	
	my $attr = $self->attr(@_);
	my $id = "ch" . ++$app->{q}{HTML_CH};
	$self->new("<input id=\"$id\" class=\"ch\" type=checkbox$checked><label for=\"$id\"$attr>", _escape($label), "</label>");
}

# возвращает атрибуты
sub attr {
	my ($self, %attr) = @_;
	my $attr = join " ", map { defined($attr{$_})? "$_=\"" . _escape($attr{$_}) . "\"": $_ } keys %attr;
	$attr? " $attr": ""
}

# переводит абревиатурную запись (а ля css) в текст html
use R::Re;
sub emmet {
	my ($self, $abbr) = @_;
	local ($_, $`, $', $&, $1, $2);
	
	my $pre_text; # текст в начале тега
	my $post_text; # текст в конце тега
	my $is_tag;	# тег уже был
	my $is_id;	# id уже было
	my @class;	# классы тега
	my @attr;	# атрибуты тега
	my @pre;	# теги до
	my @post;	# теги после
	
	pos($abbr) = 0;
	while($abbr =~ /
		(?P<s>\s+|$) |
		(?P<tag>$R::Re::css_id) |
		(?: (?P<class>\.) | (?P<id>\#) ) (?P<name>$R::Re::css_id) |
		\[ (?P<attr> [^\[\]]+ ) \] |
		(?P<post>!)? \{ (?P<text> [^\{\}]+ ) \} |
		(?P<error> .* )
	/gxo) {
		if(exists $+{error}) {
			die("ошибка в описании на `$+{error}` в `$abbr`");
		}
		elsif(exists $+{tag}) {
			die "тег `$is_tag` уже описан" if defined $is_tag;
			$is_tag = $+{tag};
		}
		elsif(exists $+{id}) {
			die "id в теге `$is_tag` уже было" if defined $is_id;
			$is_id = $+{name};
		}
		elsif(exists $+{class}) {
			push @class, $+{name};
		}
		elsif(exists $+{attr}) {
			push @attr, $+{attr};
		}
		elsif(exists $+{text}) {
		
			if($is_id || $is_tag || @class || @attr) {
				if(exists $+{post}) {
					$post_text .= $+{text};
				}
				else {
					$pre_text .= $+{text};
				}
			} else {
				if(exists $+{post}) {
					unshift @post, $+{text};
				}
				else {
					push @pre, $+{text};
				}
			}
		}
		elsif(exists $+{s}) {	# окончание описания тега
			$is_tag = "div" if !defined $is_tag;
			
			my @abbr;

			push @abbr, " id=\"$is_id\"" if $is_id;
			push @abbr, " class=\"".join(" ", @class)."\"" if @class;
			push @abbr, join " ", "", map { s/=/="/, s/$/"/ if /^$R::Re::css_id=[^'"]/o; $_ } @attr if @attr;
			
			push @pre, join "", "<$is_tag", @abbr, ">$pre_text";
			unshift @post, "$post_text</$is_tag>";
			$pre_text = $post_text = $is_tag = $is_id = undef;
			@class = @attr = ();
		}
		else {
			die "неизвестная ошибка при разборе описания"
		}
	}
	
	#msg1 length $abbr, '!=', pos $abbr;
	
	die "строка не разобрана до конца `$abbr`" if defined pos $abbr;
	

	$self->new(join("", @pre), join "", @post);
}



1;
