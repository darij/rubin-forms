package R::App;

# приложение - через него доступны другие классы

use common::sense;
use namespace::autoclean ();

BEGIN { 
	vec( ${^WARNING_BITS}, $warnings::Offsets{'recursion'}, 1 ) = 1;
}

#use Time::Piece qw/:override/;

use R;


# импортирует в вызывавший модуль функции
my %EXPORT_VAR = (
	'$app' => 'app',
);
my %EXPORT_LOCALE = (
	__ => \&R::__,
	__x => \&R::__x,
	__n => \&R::__n,
	__nx => \&R::__nx,
	__p => \&R::__p,
	__px => \&R::__px,
	__np => \&R::__np,
	__npx => \&R::__npx,
);
my %EXPORT_CODE = (
	%EXPORT_LOCALE,
	app => \&R::app,
	msg => \&R::msg,
	msg1 => \&R::msg1,

	has => \&R::has,
	has_const => \&R::has_const,
	
	Is => \&R::Is,
	IsErr => \&R::IsErr,
	subtype => \&R::subtype,
	subenum => \&R::subenum,
	
	closure => \&R::closure,
	TODO => \&R::TODO,

	Isa => \&R::Isa,
	Can => \&R::Can,
	
	# проверяет: является ли переданный скаляр числом
	# будьте осторожней, т.к. inf и nan - так же числа
	Num => \&R::Num,
	
	weaken => \&R::weaken,

	Prefix => \&R::Prefix,
	Suffix => \&R::Suffix,
	Infix => \&R::Infix,
	
	map2 => \&R::map2,
	grep2 => \&R::grep2,
	keys2 => \&R::keys2,
	values2 => \&R::values2,
	first2 => \&R::first2,
	first => \&R::first,
	reduce => \&R::reduce,
	
	all => \&R::all,
	any => \&R::any,
	
	max => \&R::max,
	min => \&R::min,
	maximal => \&R::maximal,
	minimal => \&R::minimal,
	
	max_by => \&R::max_by,
	min_by => \&R::min_by,
	maximal_by => \&R::maximal_by,
	minimal_by => \&R::minimal_by,
	
	sum => \&R::sum,
	product => \&R::product,
	
	zip => \&R::zip,
	cross => \&R::cross,
	cross_join => \&R::cross_join,
	indexify => \&R::indexify,
	
	uniq => \&R::uniq,
	singlify => \&R::singlify,
	
	asc => \&R::asc,			# числовые
	desc => \&R::desc,
	ascending => \&R::ascending,		# строчные
	descending => \&R::descending,

	#time => \&R::time,
	sleep => \&R::sleep,
	waits => \&R::waits,
	
	guard => \&R::guard,

	Tour => \&R::Tour,
	#Detour => \&R::Detour, # заходит в объекты
	
	#__ => \&R::gettext,
	#gettext => \&R::gettext,
	#ngettext => \&R::ngettext,
	
	server => \&R::server,
	
	try => \&R::try,
	catch => \&R::catch,
);
my %EXPORT_EXT = (
	# ":yogi" => sub {
		# my ($pkg) = @_;
		# require "R/Yogi.pm";
		# R::Yogi::_import($pkg);
		# return;
	# },
	":min" => sub {
		return qw/$app msg1 msg sleep/;
	},
	":max" => sub {
		return keys %EXPORT_CODE, keys %EXPORT_VAR;
	},
	":locale" => sub { keys %EXPORT_LOCALE }
);

sub import {
    my $self = shift;

    my $pkg = caller;

	namespace::autoclean->import(-cleanee => $pkg) if !R::first {$_ eq "!clean"} @_;
	
	#my $caller1 = \%{caller."::"};

	#my $export = [%EXPORT_CODE, keys %EXPORT_VAR];
	#my $default = [keys %EXPORT_CODE, keys %EXPORT_VAR];
	#R::export($pkg, $export, $export);
	
    my $once = $warnings::Offsets{'once'};

    my $save = vec( ${^WARNING_BITS}, $once, 1 );

    for my $name ( @_ ? (map { /^!/? (): /^:/? $EXPORT_EXT{$_}($pkg): $_} @_): $EXPORT_EXT{":max"}($pkg) ) {
		if(my $v = $EXPORT_CODE{$name}) {
			*{"${pkg}::$name"} = $v;
		} elsif(my $v = $EXPORT_VAR{$name}) {
			*{"${pkg}::$v"} = \${$v};
		}
		else {
			die "нет такого имени `$name`";
		}
    }

    vec( ${^WARNING_BITS}, $once, 1 ) = $save;

    vec( ${^WARNING_BITS}, $warnings::Offsets{'recursion'}, 1 ) = 1;

    $self;
}

# переменная приложения. При app->hotswap(pkg=>"R::App")->reload должна сохранятся
our $app = $R::app //= bless {}, "R::App";

# игнорируем PIPE. Я никогда не буду работать с процессами, которые завершаются из-за того, что сервер закрыл с ним соединение
$SIG{PIPE} = "IGNORE";

# по INT нужно завершаться корректно - вызвать все обработчики завершения
our $sigint = $SIG{INT} = sub {
	my $coro = $Coro::current? "#" . ($Coro::current->{desc} || "$Coro::current"): "";
	my $ps = $app->process->name || $app->process->pid;
	print STDERR "INT!!! $ps$coro\n";
	exit;
};

END {
	#my $coro = $Coro::current? "#" . ($Coro::current->{desc} || "$Coro::current"): "";
	#my $ps = $app->process->name || $app->process->pid;
	#print STDERR "END!!! $ps$coro\n";
	#print STDERR "END!!!\n";
	my $child = $app->process->child;
	
	#print STDERR  "$$, ".$child->length."\n";
	
	if($child->length) {
		my @names = map $_->{name}, $child->loadname->parts;
		$child->kill("INT")->wait($app->process->{wait_children_time} // 5);
		$app->log->info(":space blue size10000", "kill", @names, !$child->existing->length? (":green", "ok"): (":red", "fail"));
	}
	$app->process->erasepid;
}

# потоки ввода-вывода приводятся к utf-8. Вернуть: $app->tty->raw
binmode STDIN,  ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";

select STDERR; $|=1;
select STDOUT; $|=1;

# и ARGV
{
	for my $e ( @ARGV ) { utf8::decode($e); }
}

our @ARGS = @ARGV;    # сохраняем для app->process

# и переменные окружения
{
	for my $e ( keys %ENV ) { utf8::decode($ENV{$e}) }
}

# для app->bench
#our $start_time = Time::HiRes::time();

# устанавливаем ошибки
$app->raise;

# устанавливаем ddp
$app->perl->set_ddp;

# подключает и возвращает класс
sub use {
    my ( $self, $class ) = @_;
    my $path = $class;
    $path =~ s!::!/!g;
    require "$path.pm";
    $class;
}

my $translate = sub {
	local $_ = $_[0];

	my $translate_once = sub {
		exists $+{key}? (R::Num($+{key})? "->[$+{key}]": "->{$+{key}}"):
		exists $+{sk}? "->$+{sk}":
		exists $+{gosub}? "->$+{gosub}":
		exists $+{app}? "\$app->":
		die "?"
	};
	
	s{
		: (?<key> \w+ ) |
		(?=>\w) (?<sk> [\{\[]) |		
		\. (?<gosub> \w+ ) |
		(?<app> ~ )
	}{ $translate_once->() }gxe;
	
	$_
};

# если функция не найдена
our $AUTOLOAD;

sub AUTOLOAD {
    my ($prop) = $AUTOLOAD =~ /([^:]+)$/;

# ВНИМАНИЕ! Не использовать msg, msg1, app->log!, т.к. это приводит к зацикливанию!
	#print STDERR "$app -> $prop\n";

	# создаём заглушку
	eval "sub $AUTOLOAD {
		my \$self = shift;
		return \$self->{'$prop'}->new(\@_) if \@_ > 0;
		\$self->{'$prop'}
	}";
    die "not make $AUTOLOAD: $@" if $@;

	goto &$prop if exists $app->{$prop};
	
    my $new;
    my %param;
    my $param;
    if ( $prop !~ /^(framework|project|ini|file)$/
        and $param = $app->ini
        and $param = $param->{service}
        and $param = $param->{$prop} )
    {
		# нужно параметр заменить на выражение
		if(!ref $param) {
			$app->{$prop} = eval $translate->($param);
			die if $@;
			goto &$prop;
		}
	
		%param = %$param;
		$new = delete $param{"~class"};
		
		# обходим всю структуру и заменяем ~xxx на $app->xxx
		R::Tour( sub { 
			if(!ref $b and $b =~ s/^~/\$app->/) {
				$b = eval $translate->($b);
				die if $@;
			}
		}, \%param);
    }
	
	my $load;
	
	if(!defined $new) {
		$new = $prop;
		my $base = "R";
		$new =~ s![A-Z]!::$&!g;
		$new = $base . "::" . ucfirst($new);
		#die "опишите service:$prop в ".$app->project->ini if !$app->file($load)->lib->length;
	}
	
	$load = "$new.pm";
	$load =~ s!::!/!g;
	eval {
		require $load;
	};
	if($@) {
		my $qload = quotemeta $load;
		die "not found service:$prop in etc/".($app->project->name // "~this~project~").".yml by path $load: $@" if $@ =~ /Can't locate $qload/;
		die;
	}

	# если не прописан в service:, то оставляем класс
    
	#$new->CHECK_new($self, $prop, \%param) if $new->can("CHECK_new");
    #$self->{$prop} = %param? $new->new(%param): $new->can("DEFAULT_new")? $new->DEFAULT_new: $new;
	
	my $service = $app->{$prop} = $new->can("DEFAULT_new")? $new->DEFAULT_new(_APP_KEY => $prop, %param): $new->new(%param);
	
	delete $service->{_APP_KEY} if R::Is($service, "HashRef");
	
	#print STDERR "$_[0]->$prop => $app->{$prop} <". $prop->($app) .">\n";
	
    goto &$prop;
}

sub DESTROY {
	# print STDERR "APP DESTRIY!!!\n" 
}

1;
