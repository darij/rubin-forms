package R::Raise::Quit;
# модуль

use common::sense;
use R::App;

use overload
	'""' => \&stringify,
	'.' => \&concat,
	'bool' => sub { 1 },
	"0+" => sub { $_[0]->{msg}? 1: 0 },
	"qr" => \&stringify,
	fallback => 1
;


# конструктор
sub new {
	my $cls = shift;
	bless {
		msg => "<<<Quit>>>",
		@_
	}, ref $cls || $cls;
}

# стрингифицирует
sub stringify {
	my ($self) = @_;
	$self->{msg}
}

# объединяет со строкой
sub concat {
	my ($self, $str, $wrap) = @_;
	$wrap? $str . $self->stringify: $self->stringify . $str
}

1;