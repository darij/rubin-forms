package R::Rpc;
# заглушка. Поднимает сервер и отправляет ему запросы

use common::sense;
use R::App qw/$app/;

# конструктор
sub new {
	my $cls = shift;
	bless {
		client => undef,	# клиент
		ps => undef,		# процесс сервера
		port => undef,		# порт на котором запускать
		service => undef,	# наименование сервиса, который будет запущен в отдельном процессе
		workers => 3,		# количество воркеров процесса
		@_
	}, ref $cls || $cls;
}

# если функция не найдена
our $AUTOLOAD;

sub AUTOLOAD {
    my ($prop) = $AUTOLOAD =~ /([^:]+)$/;
	
	# создаём заглушку
	eval "sub $AUTOLOAD {
		my \$self = shift;
		\$self->__CREATE__IF__NEED__ if !\$self->{ps};
		\$app->tcpClient(port => \$self->{port})->send(['$prop', \@_]);
	}";
    die "not make $AUTOLOAD: $@" if $@;
	
	goto &$AUTOLOAD
}

# чтобы не вызывался автолоадом
sub DESTROY {}

# создаёт процесс сервера и клиент
sub __CREATE__IF__NEED__ {
	my ($self) = @_;
	
	die "не указан параметр name" if !length $self->{name};
	die "не указан параметр port" if !$self->{port};
	#die "AnyEvent уже запущен!" if AE->can("signal");
	#die "Coro уже запущен!" if $Coro::current && ref $Coro::current ne "HASH";
	
	my $service = $self->{service};
	
	$self->{ps} = $app->process($self->{name} => '

		$server->loop;

		END { $server->close if $server->{sd} }
		
	')->init('
		$R::Coro::DISABLED = 1;

		my $server = $app->tcpServer(name => q{'.$self->{name}.'}, port => q{'.$self->{port}.'}, workers => q{'.$self->{workers}.'}, ritter => q{
			my ($q) = @_;
			my $method = shift @{$q->{res}};
			$app->'.$service.'->$method(@{$q->{res}})
		})->run;
	')->waitrun;
	
	# время ожидания для завершения чилдренов
	$app->process->{wait_children_time} = 20;
	
	$self
}

1;