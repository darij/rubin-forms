package R::Magnitude::Date;
# модуль

use common::sense;
use R::App qw/:min/;
use POSIX qw//;
use Time::HiRes qw//;
#use Time::Format qw(%time %strftime %manip);

# свойства
R::has qw/epoch/;


# конструктор
sub new {
	my ($cls, $epoch) = @_;
	bless {
		epoch => $epoch // 0,
	}, ref $cls || $cls;
}

# текущее время
sub now {
	my ($self) = @_;
	$self->new(Time::HiRes::time())
}

# возвращает время в указанном формате
sub fmt {
	my ($self, $fmt) = @_;
	POSIX::strftime($fmt // "%F %T", localtime($self->{epoch}))
}

my $len_year = 365.2425;	# по григорианскому календарю
my $len_month = $len_year / 12;

# переводит секунды в единицы CI
sub to {
	my ($self, $t, $precision) = @_;
	
	$precision //= 2;
	$t //= $self->{epoch};
	
	my $f = "";
	if( $t > 60*60*24*$len_year*1000_000_000 ) { $t/=60*60*24*$len_year*1000_000_000; $f="Gyr" }
	elsif( $t > 60*60*24*$len_year*1000_000 ) { $t/=60*60*24*$len_year*1000_000; $f="Myr" }
	elsif( $t > 60*60*24*$len_year*10_000 ) { $t/=60*60*24*$len_year*10_000; $f="myriad" }
	elsif( $t > 60*60*24*$len_year*1000 ) { $t/=60*60*24*$len_year*1000; $f="millenium" }
	elsif( $t > 60*60*24*$len_year*100 ) { $t/=60*60*24*$len_year*100; $f="century" }
	elsif( $t > 60*60*24*$len_year ) { $t/=60*60*24*$len_year; $f="year" }
	elsif( $t > 60*60*24*$len_month ) { $t/=60*60*24*$len_month; $f="month" }
	elsif( $t > 60*60*24 ) { $t/=60*60*24; $f="d" }
	elsif( $t > 60*60 ) { $t/=60*60; $f="h" }
	elsif( $t > 60 ) { $t/=60; $f="m" }
	elsif( $t > 0 ) { $f="s" }
	#elsif( int($t*10)>0 ) { $t*=10; $f="ds" }
	#elsif( int($t*100)>0 ) { $t*=100; $f="cs" }
	elsif( int($t*1000)>0 ) { $t*=1000; $f="ms" }	# милли-
	elsif( int($t*1000_000)>0 ) { $t*=1000_000; $f="µs" }	# микро-
	elsif( int($t*1000_000_000)>0 ) { $t*=1000_000_000; $f="ns" }	# нано- η
	elsif( int($t*1000_000_000_000)>0 ) { $t*=1000_000_000_000; $f="ps" }		# пико- π
	else { $f = "s" }
	
	$t = 0 + sprintf("%.${precision}f", $t);
	
	"${t}${f}"
}

# переводит из времени в единицах CI в секунды и выдаёт объект R::Magnitude::Date
sub from {
	my ($self, $v) = @_;
	
	$v =~ s/_//g;
	
	if(R::Num $v) {	# без изменений
	}
	elsif($v =~ s/(Gyr|billions?\s+of\s+years)$//i) {	# миллиарды лет
		$v *= 60*60*24*$len_year*1000_000;
	}
	elsif($v =~ s/(Myr|millions?\s+of\s+years)$//i) {	# миллионы лет
		$v *= 60*60*24*$len_year*1000_000;
	}
	elsif($v =~ s/my(riads?)?$//i) {	# мириады лет
		$v *= 60*60*24*$len_year*10_000;
	}
	elsif($v =~ s/mi(lleniums?)?$//i) {	# тысячи лет
		$v *= 60*60*24*$len_year*1000;
	}
	elsif($v =~ s/c(entur(y|ies))?$//i) {	# века
		$v *= 60*60*24*$len_year*100;
	}
	elsif($v =~ s/y(ears?)?$//i) {	# годы
		$v *= 60*60*24*$len_year;
	}
	elsif($v =~ s/mo(n(ths?)?)?$//i) {	# месяцы
		$v *= 60*60*24*$len_month;
	}
	elsif($v =~ s/d(ays?)?$//i) {	# дни
		$v *= 60*60*24;
	}
	elsif($v =~ s/h(ours?)?$//i) {	# часы
		$v *= 60*60;
	}
	elsif($v =~ s/m(inutes?)$//i) {	# минуты
		$v *= 60;
	}
	elsif($v =~ s/s(econds?)?$//i) {	# секунды
	}
	elsif($v =~ s/ds$//i) {	# децисекунды
		$v /= 10;
	}
	elsif($v =~ s/ds$//i) {	# сантисекунды
		$v /= 100;
	}
	elsif($v =~ s/ms$//i) {	# милисекунды
		$v /= 1000;
	}
	elsif($v =~ s/(µ|u)s$//i) {	# уносекунды
		$v /= 1000_000;
	}
	elsif($v =~ s/ns$//i) {	# наносекунды
		$v /= 1000_000_000;
	}
	else {
		die "распознать время не удалось: `$v`"
	}
	
	$self->new($v)
}



# возващает в формате http
sub http {
	require 'HTTP/Date.pm';
	*http = \&_http;
	goto &_http;
}
sub _http {
	my ($self) = @_;
	HTTP::Date::time2str($self->{epoch})
}

# парсит http-дату и возвращает
sub parse_http {
	require 'HTTP/Date.pm';
	*parse_http = \&_parse_http;
	goto &_parse_http;
}
sub _parse_http {
	my ($self, $date) = @_;
	$self->new(HTTP::Date::str2time($date))
}

1;