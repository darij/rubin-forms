package R::Magnitude::Byte;
# представление байтовых величин

use common::sense;
use R::App;

# свойства
has qw//;

# конструктор
sub new {
	my $cls = shift;
	bless {@_}, ref $cls || $cls;
}

# парсит значение и выдаёт в байтах
sub from {
	my ($self, $v) = @_;
	
	if(Num $v) {	# без изменений
	}
	elsif($v =~ /[\+\-\*\/]/) {	# выражение
		my ($a, $op);
		for my $x (split /([\+\-\*\/])/, $v) {
			if( $x=~/^[\+\-\*\/]/ ) {
				$op = $x;
			}
			elsif($op eq "+") { $a += $x }
			elsif($op eq "-") { $a -= $x }
			elsif($op eq "*") { $a *= $x }
			elsif($op eq "/") { $a /= $x }
		}
		$a
	}
	elsif($v =~ s/t$//i) {	# терабайты
		$v *= 1024*1024*1024*1024;
	}
	elsif($v =~ s/g$//i) {	# гигабайты
		$v *= 1024*1024*1024;
	}
	elsif($v =~ s/m$//i) {	# мегабайты
		$v *= 1024*1024;
	}
	elsif($v =~ s/k$//i) {	# килобайты
		$v *= 1024;
	}
	elsif($v =~ s/b$//i) {	# байты
	}
	else {
		die "распознать размер не удалось"
	}
	
	$v
}

# выдаёт в подходящих единицах. $zn - знаков после запятой
sub to {
	my ($self, $s, $zn) = @_;
	
	$zn //= 0;
	my $t;
	if(int($s/1024**4)>0) { $s /= 1024**4; $t = "t" }
	elsif(int($s/1024**3)>0) { $s /= 1024**3; $t = "g" }
	elsif(int($s/1024**2)>0) { $s /= 1024**2; $t = "m" }
	elsif(int($s/1024)>0) { $s /= 1024; $t = "k" }
	else { $t = "b" }
	
	my $x = sprintf "%.${zn}f", $s;
	$x =~ s!\.?0+$!!;
	$x . $t
}

1;