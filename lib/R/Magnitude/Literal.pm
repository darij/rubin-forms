package R::Magnitude::Literal;
# работает с строками

use common::sense;
use R::App;

# конструктор
sub new {
	my $cls = shift;
	unshift @_, "s" if @_==1;
	bless {@_}, ref $cls || $cls;
}

# возвращает строку
sub s {
	my ($self) = @_;
	$self->{s}
}

#@@category цитаты

# оборачивает текст в кавычки «...»
sub q {
	my ($self) = @_;
	"«$self->{s}»"
}

# оборачивает текст в кавычки “...”
sub qq {
	my ($self) = @_;
	"“$self->{s}”"
}

# оборачивает текст в кавычки ‘...’
sub mq {
	my ($self) = @_;
	"‘$self->{s}’"
}


#@@category пробелы

# обрезает пробелы с двух сторон
sub trim {
	my ($self, $s) = @_;
	$s //= $self->{s};
	$s =~ s/^\s*(.*?)\s*$/$1/s;
	$s
}

# обрезает пробелы справа
sub rtrim {
	my ($self, $s) = @_;
	$s //= $self->{s};
	$s =~ s/^(.*?)\s*$/$1/s;
	$s
}

# обрезает пробелы слева
sub ltrim {
	my ($self, $s) = @_;
	$s //= $self->{s};
	$s =~ s/^\s*(.*)$/$1/s;
	$s
}


#@@category эскейпинг

# для вставки в строки
sub esc {
	my ($self, $s) = @_;
	$s //= $self->{s};
	$s =~ s![\\'"]!\\$&!g;
	$s
}

# для вставки в строки js
sub escjs {
	my ($self, $s, $q) = @_;
	$s //= $self->{s};
	$q //= "\"'";
	$s =~ s![\\$q]!\\$&!g;
	$s =~ s!\n!\\n!g;
	$s =~ s!\r!\\r!g;
	$s
}

# ескейпит "
sub escqq {
	my ($self, $s) = @_;
	$s //= $self->{s};
	$s =~ s![\\"]!\\$&!g;
	$s
}

# ескейпит '
sub escq {
	my ($self, $s) = @_;
	$s =~ s![\\']!\\$&!g;
	$s
}

# парсит из строки
sub unstring {
	my ($self, $x) = @_;
	$x //= $self->{s};
	if($x=~/^["']/) { $x = substr $x, 1, -1; $x=~s/\\([\\'"nrt])/my $x=$1; $x=~tr!nrtv!\n\r\t!; $x/ge; } 
	$x
}

# заворачивает в строку
sub asstring {
	my ($self, $x, $q) = @_;
	$x //= $self->{s};
	$q //= $self->{q} // "\"";
	join "", $q, $self->escjs($x, $q), $q
}

*to = \&asstring;
*from = \&unstring;

# заворачивает в строку perl
sub str {
	my ($self, $x, $q) = @_;
	$x //= $self->{s};
	$q //= $self->{q} // "\"";
	join "", $q, $self->esc($x), $q
}

# нормализует строку
sub normalize {
	my ($self, $s) = @_;
	$s //= $self->{s};
	$self->to($self->from($s))
}

# для вставки в строки
sub escperl {
	my ($self, $s) = @_;
	$s //= $self->{s};
	use bytes;
	no utf8;
	utf8::encode($s) if utf8::is_utf8($s);
	$s =~ s![\\'"]!\\$&!g;
	$s =~ s![\x00-\x08\x0b\x0c\x0e-\x1f\x7f-\xff]!sprintf "\\x%02x", $&!ge;
	$s
}

# для вставки в строки
sub perl {
	my ($self, $x, $q) = @_;
	$x //= $self->{s};
	$q //= $self->{q} // "\"";
	join "", $q, $self->escperl($x), $q
}


#@@category преобразования

# перекодирует строку из любых символов в идентификатор perl
sub wordname {
	my ($self, $word) = @_;
	$word //= $self->{s};
	$word =~ s!(\W)!"_".$app->magnitudeNumber->to(ord $1)."_"!ge;
	$word = "_$word" if $word =~ /^\d/;
	$word
}

# перекодирует путь к классу. Т.е. / превращается в ::, а остальное - wordname
sub classname {
	my ($self, $word) = @_;
	$word //= $self->{s};
	join "::", map { $self->wordname($_) } split /\//, $word;
}


#@@category перекодировки

# перекодирует из $from в $to
# если $to опущен, то перекодирует из utf8 в указанную кодировку ($from)
sub iconv {
	my ($self, $from, $to) = @_;
	
	if(!defined $to) {
		$to = $from;
		$from = "utf8";
	}
	
	require Encode;
	
	my $s = $self->{s};
	
	utf8::encode($s) if utf8::is_utf8($s);
	
	Encode::from_to($s, $from, $to, 1);
	
	$s
}

sub to_cp1251 {
	my ($self) = @_;
	$self->iconv("utf8", "cp1251")
}

sub from_cp1251 {
	my ($self) = @_;
	$self->iconv("cp1251", "utf8")
}

# включён ли флаг utf8
sub is {
	my ($self, $s) = @_;
	utf8::is_utf8($s // $self->{s})
}

# включить флаг utf8
sub on {
	my ($self) = @_;
	if(@_ == 2) { utf8::decode($_[1]); }
	else { utf8::decode($self->{s}); }
	$self
}

# выключить флаг utf8
sub off {
	my ($self) = @_;
	if(@_ == 2) { utf8::encode($_[1]); }
	else { utf8::encode($self->{s}); }
	$self
}

# 
sub toCamelCase {
	my ($self, $s) = @_;
	my $x = lc($s // $self->{s});
	$x =~ s![\W_](\w)!uc $1!ge;
	ucfirst $x
}

# 
sub to_snake_case {
	my ($self, $s) = @_;
	my $x = lcfirst($s // $self->{s});
	$x =~ s!\W!_!g;
	$x =~ s![A-Z]!"_".lc $&!ge;
	$x
}

# 
sub to_UPPER_CASE {
	my ($self, $s) = @_;
	my $x = $s // $self->{s};
	$x =~ s!\W!_!g;
	uc $x
}


1;
