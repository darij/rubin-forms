package R::Magnitude::Number;

# утилиты для обработки и отображения чисел

use common::sense;
use R::App;

has qw/number unit/;

# конструктор
sub new {
    my ($cls, $number) = @_;
	
	#$unit = $1 if !defined $unit and $number =~ s/([a-z]+)$//i;
	
    bless {
		number => $number,
	}, ref $cls || $cls;
}

# клонирует
sub clone {
	my ($self) = @_;
	bless {%$self}, ref $self;
}

# проверка
sub eqv {
	my ($a, $b) = @_;
	$a
}

use overload
	fallback => 1,
	'""' => \&stringify,
	'.' => sub { my ($a, $b, $c) = @_; "$a$b" },
	'bool' => sub { !!shift->{number} },
	"0+" => sub { $_[0]->{msg}? 1: 0 },
	"qr" => \&stringify,
	"++" => sub { ++shift->{number} },
	"--" => sub { --shift->{number} },
	neg => sub { my $x=shift->clone; $x->{number} = -$x->{number}; $x },
	"!" => sub { !shift->{number} },
	"<" => sub { my ($a, $b, $c) = @_; $a->{number} < $b->{number} },
;

# форматирует число
sub stringify {
	my ($self) = @_;
	$self->format($self->{number});
}

# форматирует число
sub format {
    my ( $self, $number, $precision, $comma, $space, $range ) = @_;

    $space //= " ";
    $range //= 3;

    #sign
    my $sign = "";
    $number = -$number, $sign = "-" if $number < 0;

    # round
    my $accuracy = 10**$precision;
    $number = int( $number * $accuracy + 0.5 ) / $accuracy;

    # split on integer and decimal
    my $integer = int $number;
    my $decimal = int( ( $number - $integer ) * $accuracy );

    # spaces
    $integer = reverse $integer;
    $integer =~ s!(\d{$range})!$1$space!g;
    $integer =~ s! $!!;
    $integer = reverse $integer;

    # decimal
    if ( $precision == 0 ) {
        $decimal = "";
    }
    else {
        $decimal = sprintf( "%0${precision}s", $decimal );
        $comma //= ",";
        $decimal = $comma . $decimal;
    }

    return $sign . $integer . $decimal;
}

our @RIM_CIF = (
    [ '', 'I', 'II', 'III', 'IV', 'V', 'VI', 'VII', 'VIII', 'IX' ],
    [ '', 'X', 'XX', 'XXX', 'XL', 'L', 'LX', 'LXX', 'LXXX', 'XC' ],
    [ '', 'C', 'CC', 'CCC', 'CD', 'D', 'DC', 'DCC', 'DCCC', 'CM' ]
);

# переводит в римскую систему счисления
sub rim {
    my ($self, $a) = @_;
    my $s;
	$a //= $self->{number};
	for ( ; $a != 0 ; $a = int( $a / 1000 ) ) {
        my $v = $a % 1000;
        if ( $v == 0 ) {
            $s = "M $s";
        }
        else {
			my $d;
            for ( my $i = 0, $d = "" ; $v != 0 ; $v = int( $v / 10 ), $i++ ) {
                my $x = $v % 10;
                $d = $RIM_CIF[$i][$x] . $d;
            }
            $s = "$d $s";
        }
    }
	
    $s // "N";
}

# переводит в шумерскую систему счисления
our @SHUMER = qw/ ' " /;
sub shumer {
	my ($self, $n) = @_;
	
	my $s;
	$n //= $self->{number};
	
	for(;;) {
		$s = ($n % 60) . " " . $s;
		$n /= 60;
		last if $n < 1 / (60 ** 3);
	}
	
	#`'"
	$s
}

# переводит натуральное число в заданную систему счисления
sub to {
	my ($self, $n, $radix) = @_;
	$radix //= 62;
	my ($x, $y) = "";
	for(;;) {
		$y = $n % $radix;
		$x = ($y < 10? $y:  chr($y + ($y<36? ord("A") - 10: $y<62? ord("a")-36 : 128-62))).$x;
		last unless $n = int $n / $radix;
	}
	return $x;
}

# парсит число в указанной системе счисления
sub from {
	my ($self, $s, $radix) = @_;
	$radix //= 62;
	my $x = 0;
	my $a;
	for my $ch (split "", $s) {
		$a = ord $ch;
		$x = $x*$radix + $a - ($a <= ord("9")? ord("0"): $a <= ord("Z")? ord('A')-10: $a <= ord('z')? ord('a')-36: 128-62);
	}
	return $x;
}


1;
