package R::Process;
# коллекция процессов

use base R::Collection;

use common::sense;
use R::App qw/$app msg1 msg TODO closure Num Isa Can waits guard sleep/;

use POSIX qw/setsid :sys_wait_h LONG_MAX/;
use Proc::FastSpawn;

use List::Util qw/max/;


our $SELF_PS;	# синглетон для текущего процесса

# $SIG{'CHLD'} = sub {
	# kill_zombie();
# };

# вернуть себя
sub DEFAULT_new {
	my ($self) = @_;
	$self->current
}

# конструктор
sub new {
	my ($cls) = @_;
	
	my $self = bless {
		bg => 0,					 # демонизировать (отключить от терминала и удалить ppid)
		fd => [0, 1, 2],
		#done => 1,
		parts => [],				# коллекция процессов { pid=>..., name=>... }
	}, ref $cls || $cls;
	
	my $parts = $self->{parts};
	
	for(my $i=1; $i<@_; $i++) {
		my $name = @_[$i];
		if(ref $name or $name !~ /^[-\w]+$/) {
			$parts->[$#$parts<0? 0: $#$parts]{exec} = $name;
		}
		else {
			push @$parts, +{name => $name};
		}
	}
	
	$self->loadpid
}

# возвращает name первого процесса или устанавливает для всех
sub name {
	die "лишний параметр" if @_>1;
	my ($self) = @_;
	!!@{$self->{parts}} && $self->{parts}[0]{name}
}

# возвращает exec первого процесса или устанавливает для всех
sub exec {
	my ($self, $exec) = @_;
	return !!@{$self->{parts}} && $self->{parts}[0]{exec} if @_ == 1;
	for my $ps ($self->parts) {
		$ps->{exec} = $exec;
	}
	$self
}

# возвращает init первого процесса или устанавливает для всех
sub init {
	my ($self, $init) = @_;
	return !!@{$self->{parts}} && $self->{parts}[0]{init} if @_ == 1;
	for my $ps ($self->parts) {
		$ps->{init} = $init;
	}
	$self
}

# возвращает dir первого процесса или устанавливает для всех
sub dir {
	my ($self, $dir) = @_;
	return !!@{$self->{parts}} && $self->{parts}[0]{dir} if @_ == 1;
	for my $ps ($self->parts) {
		$ps->{dir} = $dir;
	}
	$self
}


# возвращает pid первого процесса
sub pid {
	my ($self) = @_;
	return if !@{$self->{parts}};
	my $ps = $self->{parts}[0];
	$self->one($ps)->loadpid if !defined $ps->{pid};
	return $ps->{pid} if Num $ps->{pid} and kill 0, $ps->{pid};
	undef $ps->{pid};
	my $p = $self->one($ps);
	$p->runfile->rm;
	$p->pidfile->rm;
	return;
}

# возвращает pid парента
sub ppid {
	my ($self) = @_;
	!!@{$self->{parts}} && $self->{parts}[0]{ppid}
}

# группа первого процесса в коллекции
sub pgrp {
	my ($self, $pgrp) = @_;
	my $pid;
	return defined($pid = $self->pid) && getpgrp($pid) if @_ == 1;
	for my $ps ($self->parts) {
		setpgrp $ps->{pid}, $pgrp;
	}
	$self
}

# uid пользователя первого процесса в коллекции
sub uid {
	my ($self) = @_;
	!!@{$self->{parts}} && $self->{parts}[0]{uid}
}

# vsize пользователя первого процесса в коллекции
sub vsize {
	my ($self, $i) = @_;
	!!@{$self->{parts}} && $self->{parts}[$i]{vsize}
}

# rss пользователя первого процесса в коллекции
sub rss {
	my ($self, $i) = @_;
	!!@{$self->{parts}} && $self->{parts}[$i]{rss}
}


# cmdline
sub cmdline {
	my ($self, $i) = @_;
	!!@{$self->{parts}} && $self->{parts}[$i]{cmdline}
}

# путь к первому процессу в коллекции
sub path {
	my ($self, $i) = @_;
	!!@{$self->{parts}} && $self->{parts}[$i]{path}
}

# аргументы первого процесса в коллекции или установить для всех
sub argv {
	my ($self, @argv) = @_;
	
	if(@argv) {
		for my $ps ($self->parts) {
			$ps->{argv} = [@argv];
		}
		return $self;
	}
	
	return if !@{$self->{parts}};
	my $argv = $self->{parts}[0]{argv};
	return wantarray? @$argv: $argv
}

# статус процесса
sub status {
	my ($self) = @_;
	return if !@{$self->{parts}};
	return $self->{parts}[0]{status}
}

# возвращает текущий процесс
sub current {
	my ($self) = @_;
	
	return $SELF_PS if $SELF_PS;
	
	$SELF_PS = $self->new;
	push @{$SELF_PS->{parts}}, +{
		name => ($0 =~ m!([^/]+)$!? $1: undef),
		pid => $$,
		ppid => getppid(),
		uid => $<,
		path => "$^X",			# "" - для перевода в utf-8
		argv => ["$0", @R::App::ARGS],
		cmdline => join(" ", $^X, $0, @R::App::ARGS),
		status => "R"
	};
	
	$SELF_PS->procinfo;
	
	$SELF_PS->{parts}[0]{name} = $ARGV[0] if $SELF_PS->{parts}[0]{name} eq "al" && length $ARGV[0];
	
	$SELF_PS
}

# возвращает информацию по имени
sub info {
	my ($self) = @_;
	$self->loadpid->procinfo;
}

# возвращает информацию для процессов по их pid

sub procinfo {
	my ($self) = @_;

	
	for my $ps ($self->parts) {
		
		#%$ps = (%$ps, %{$SELF_PS->{parts}[0]}), next if defined $ps->{pid} && $ps->{pid} == $SELF_PS->{pid};
		
		my $pid = $ps->{pid};
		my $name = $ps->{name};
		my $exec = $ps->{exec};
		
		%$ps = (name => $name, exec => $exec), next if !$ps->{pid};
		my $f = $^O eq "freebsd"? $app->file("/compat/linux/proc/$pid"): $app->file("/proc/$pid");
		%$ps = (name => $name, exec => $exec, pid=>$pid, status=>""), next if !$f->isdir;
		
		my $status = eval { $f->sub("stat")->cat };
		
		if(defined $status) {
		
			$status =~ s!^
				(?<pid> -?\d+) [\ ]
				\(  (?<ps_name>.*)  \) [\ ]
				(?<status> \w) [\ ]
			!!x or die "не распознан /proc/$pid/stat";

			$ps->{ps_name} = $+{ps_name};
			$ps->{status} = $+{status};
		
			@$ps{qw/ppid pgid sid tty_nr tpgid flags minflt cminflt majflt cmajflt utime stime cutime cstime priority nice num_threads itrealvalue starttime vsize rss rsslim startcode endcode startstack kstkesp kstkeip signal blocked sigignore sigcatch wchan nswap cnswap exit_signal processor rt_priority policy delayacct_blkio_ticks guest_time cguest_time start_data end_data start_brk are arg_end env_start env_end exit_code/} = split / /, $status;
		}
		# else {
			## for freebsd procfs
			# my $status = eval { $f->sub("status")->cat };
			# if(defined $status and $status =~ /^.*$/) {
				# @$ps{qw/ps_name pid ppid pgid sid tty_nr flags/} = split / /, $status;
			# }
		# }
		
		%$ps = (%$ps,
			pid => $pid,
			name => $name // $ps->{ps_name},
			exec => $exec,
			path => eval { $f->sub("cmdline", "exename")->existing->cat } // "???",
			#sid => $f->sub("sessionid", "sid")->existing->cat,
			start => scalar eval { $f->ctime },
		);
		
# 		$ps->{pid} //= $pid;
# 		$ps->{uid} //= $f->sub("uid")->cat;
# 		$ps->{gid} //= $f->sub("gid")->cat;
# 		$ps->{pgid} //= $f->sub("pgid")->cat;
# 		$ps->{ppid} //= $f->sub("ppid")->cat;
		$ps->{status} //= (kill(0, $pid)? "R": "Z");

		$ps->{cmdline} = $ps->{path};
	}
	
	
	return $self;
}

# возвращает все существующие процессы
sub ps {
	my ($self) = @_;
	local $_;
	my $sta = $app->file("/proc/*")->glob(qr/\/\d+$/, "-d");
	$self->clone(parts => [ $sta->map(sub {+{ pid=>$_->name }}) ])->procinfo->loadname;
	
	
	# require 'Proc/ProcessTable.pm';
	# my $p = Proc::ProcessTable->new;	# 'cache_ttys' => 1
	# my $parts = [];
	# #msg1 $p->fields;
	# my $t = $p->table;
	# for my $ps (@$t) {
		# my $cmdline = $ps->{cmdline} // $ps->{fname};
		# utf8::decode($cmdline);
		# my $name;
		# my $path;
		# my $argv;
		# $path = $1, $argv = $' if $cmdline =~ m!((?:\\\s|\S)+)(?:\s+|$)!;
		# $name = $1 if $path =~ m!([^/]+)?(?:\.|\s|$)!;
		# $argv = [ split /\s+/, $argv ] if defined $argv;
		# push @$parts, {
			# name => $name,
			# path => $path,
			# cmdline => $cmdline,
			# argv => $argv // [],
			# pid => $ps->{pid},
			# ppid => $ps->{ppid},
			# uid => $ps->{uid},
			# status => $ps->{state},
			# ttydev => $ps->{ttydev},
			# ttynum => $ps->{ttynum},
			# start => $ps->{start},
		# }
	# }
	
	# $self->clone(parts => $parts)
}

# процессы проекта
sub psapp {
	my ($self) = @_;
	my %ps = $app->project->file("var/process/*")->glob("-f")->map(sub { $_->name => 1 });
	$self->clone(parts => [ map { +{ name => $_ } } keys %ps ])->info;
}

# форматирует процессы и возвращает строку
sub format {
	my ($self, $fmt, $sep) = @_;
	
	$fmt //= "%status %name %pid %ppid %uid %path %argv %ttydev %ttynum %start\n";
	
	local $_;
	
	my @col;
	push @col, $1 while $fmt =~ /%(\w+)/g;
	
	my @parts;
	for my $e ( $self->parts ) {
		push @parts, { map { $_ => ($_ eq "argv"? join(" ", @{$e->{$_}}): $e->{$_}) } @col };
	}
	
	my %col;
	for my $e ( @parts ) {
		$col{$_} = max length($e->{$_}), $col{$_} for @col;
	}
	
	$col{$_} = max length($_), $col{$_} for @col;
	
	$fmt =~ s/%(\w+)/%$col{$1}s/g;
	
	my @ret = sprintf $fmt, map {uc $_} @col;
	for my $e ( @parts ) {
		push @ret, sprintf($fmt, map { $e->{$_} } @col);
	}
	
	join $sep, @ret;
}

# распечатывает таблицу процессов
sub say {
	my ($self) = @_;
	$app->log->info( $self->psapp->format );
}

# возщвращает парента для всех процессов в коллекции
sub parent {
	my ($self) = @_;
	my %ppid = map {$_->{ppid} => 1} @{$self->{parts}};
	$self->ps->grep(sub { exists $ppid{$_->{pid}} })
}

# возщвращает дочерние процессы для всех процессов в коллекции
sub child {
	my ($self) = @_;
	my %pid = map {$_->{pid} => 1} @{$self->{parts}};
	$self->ps->grep(sub { exists $pid{$_->{ppid}} })
}

# находит генеалогическое дерево процессов в коллекции
sub find {
	my $self = shift;
	my $x = my $y = $self;
	while(($x = $x->child)->length) { $y = $y->add( $x ) }
	$y->grep(@_)
}

# активные процессы
sub active {
	my ($self) = @_;
	$self->filter(sub { $_->exists });
}

# завершившиеся процессы
sub passive {
	my ($self) = @_;
	$self->filter(sub { !$_->exists });
}

# # приостановленные процессы
# sub suspended {
	# my ($self) = @_;
	# $self->filter(sub { $_->exists });
# }


# # первый процесс в коллекции выполняется
# sub is_active {
	# my ($self) = @_;
	# $self
# }

# # первый процесс в коллекции выполняется
# sub is_running {
	# goto &is_active;
# }

# # для переопределения
# sub _filters {
	# my $self = shift;
	# my $filters;
	
	# for(my $i=0; $i<@_; $i++) {
		# my $filter = $_[0];
		# my $fn;
		# if(ref $filter eq "CODE") {
			# $fn = closure $self, $filter, sub { my ($self, $filter) = @_; my $x=$self->one($_); do { local $_ = $x; $filter->() } };
		# }
		# elsif($filter =~ /^(uid|gid|ppid|pid|name|path|cmdline)$/) {
			# my $val = $_[$i+1];
			# if(ref $val eq "RegExp") {
				# $fn = closure $self, $filter, $val, sub { my ($self, $filter, $val) = @_; $self->one($_)->$filter =~ $val; };
			# } else {
				# $fn = closure $self, $filter, $val, sub { my ($self, $filter, $val) = @_; $self->one($_)->$filter == $val; };
			# }
		# }
		# else {
			# die "неизвестный фильтр: $filter " . ref $filter;
		# }
		
		# push @$filters, $fn;
	# }	
	
	# $filters;
# }


# вешается на конец выполнения программы, используется в тестах
sub done {
	my ($self, $done) = @_;
	$self->{done} = $done // 1;
	$self
}

# деструктор - прибрать за собой
sub DESTROY {
	my ($self) = @_;
	$self->stop if $self->{done};
}


# END {
	# for my $process (@PROCESSES) {
		# $process->stop if $process->exists;
	# }
# }


# свойство bg - запускать процесс в фоне
sub daemonize {goto &bg}
sub bg {
	my $self = shift;
	die "ненужные параметры метода" if @_>0;
	$self->{bg} = 1;
	$self
}

# свойство fg - запускать процесс в текущем окружении
sub fg {
	my $self = shift;
	die "ненужные параметры метода" if @_>0;
	$self->{bg} = 0;
	$self
}

# добавляет файловые дескрипторы, которые нужно передать дочернему процессу
sub fd {
	my $self = shift;
	push @{$self->{fd}}, map { ref($_)? $_->fileno: $_ } @_;
	$self
}

# # создаёт процесс
# sub fork {
	# my ($self) = @_;
	
	# #die "Инициализирована AnyEvent. Нельзя после её инициализации создавать процессы" if $AnyEvent::MODEL;
	
	# my $pid = fork;
	# die "не могу создать процесс: $!" if !defined $pid or $pid < 0;
	# if(!$pid) {
		# if($self->{bg}) {	# фоновый процесс
			# $self->newsid;	# новая сессия
			# $pid = fork;
			# die "не могу создать процесс: $!" if !defined $pid or $pid < 0;
			# exit if $pid;	# завершаем промежуточный процесс
		# }
		# $self->run;
		# exit
	# }
	# $self->{pid} = $pid;
	# $self->waitkid, undef $self->{pid} if $self->{bg};
	# $self
# }

# дублирует первый процесс $n раз, при этом добавляет к имени процесса -n
sub dup {
	my ($self, $n) = @_;
	$n = 1 if @_ == 1;
	die "используйте: app->process->dup(n)" if !Num $n;
	die "в коллекции должен быть хоть один процесс" if !$self->length;
	my $x = $self->{parts}[0];
	my $name = $x->{name};
	my $exec = $x->{exec};
	my $init = $x->{init};
	local $_;
	$self->clone(parts => [map({ { name => "$name-$_", exec => $exec, init => $init } } 1..$n)]);
}

# запускает и ожидает запуска
sub waitrun {
	my ($self, $interval) = @_;
	
	$self->run;
	$interval //= 1;
	my $i=0;
	
	# ожидаем пока не исчезнут все runfile
	until($self->all(sub { !$_->runfile->exists })) {
		sleep $interval;
		
		msg1 ":magenta", "waitrun", ":cyan", $self->map(sub { $_->name }), ":blue", $i;
		
		$self->kill_zombie;
		
		# если процесс ждёт, что у него считают данные
		$self->read_from_pipe("out") if $self->{stdout};
		$self->read_from_pipe("err") if $self->{stderr};
		
		# если процесс ещё не запустился, то значит, что произошла ошибка
		for my $ps ($self->slices) {
			$ps->runfile->rm if !$ps->pid;
		}
	}
	continue { $i++ }
	
	$self
}

# stdout+stderr
sub std {
	my ($self, $std) = @_;
	$self->{stderr} = $self->{stdout} = $std // 1;
	$self
}

# включает pipe-stderr. Должен быть вызван до run
sub stderr {
	my ($self) = @_;
	$self->{stderr} = 1;
	$self
}

# включает pipe-stdout. Должен быть вызван до run
sub stdout {
	my ($self) = @_;
	$self->{stdout} = 1;
	$self
}

# считывает данные из pipe-stderr
sub err {
	my ($self) = @_;
	$self->read_from_pipe("err");
	my $data = $self->{"errdata"};
	my $res = join "", @$data;
	@$data = ();
	$res
}

# считывает данные из pipe-stdout
sub out {
	my ($self) = @_;
	$self->read_from_pipe("out");
	my $data = $self->{"outdata"};
	my $res = join "", @$data;
	@$data = ();
	$res
}

# считывает из pipe
sub read_from_pipe {
	my ($self, $name) = @_;
	
	my $RH = $self->{"${name}pipe"};
	read $RH, my $res, 1024*1024;
	utf8::decode($res);
	push @{$self->{"${name}data"}}, $res; 

	#msg1 ":empty", "считано из ", $self->name, " $name ", length($res), " символов", $app->raise->trace;

	$self
}

# # считывает данные из pipe-stderr
# sub err {
	# my ($self) = @_;
	# my $RH = $self->{"errpipe"};
	# read $RH, my $res, 1024*1024;
	# utf8::decode($res);
	# $res
# }

# # считывает данные из pipe-stdout
# sub out {
	# my ($self) = @_;
	# my $RH = $self->{"outpipe"};
	# read $RH, my $res, 1024*1024;
	# utf8::decode($res);
	# $res
# }

# out+err
sub output {
	my ($self) = @_;
	$self->out . $self->err
}

# создаёт pipe
sub _pipe {
	my ($self, $STD, $name) = @_;
	
	die "pipe не может быть создан для 2-х процессов в очереди" if @{$self->{parts}} != 1;
	
	my $fileno = fileno $STD;
	my $SAVESTD;
	die "с std$name должен быть указан $fileno-й fd" if ! grep { $_ == $fileno } @{$self->{fd}};
	pipe my $RH, my $WH or die $!;
	select $RH; $|=1; select STDOUT;
	
	# binmode $RH, ":utf8"; - не использовать: включает буффер!
	
	require "AnyEvent.pm";
	AnyEvent::fh_unblock($RH);
	
	# $self->{"${name}io"} = AE::io($RH, 0, sub {
		# $self->read_from_pipe($name);
	# });
	
	$self->{"${name}pipe"} = $RH;
	
	open $SAVESTD, ">&", $STD or die $!;
	close $STD or die $!;
	open $STD, ">&", $WH or die $!;
	
	guard {
		close $STD or die $!;
		open $STD, ">&", $SAVESTD or die $!;
	}
}

# запускает процессы
sub run {
	my ($self) = @_;

	
	for my $fd (@{$self->{fd}}) {
		fd_inherit $fd;
	}
	
	my $i = 0;
	for my $ps ($self->slices) {
		die "нет app->process->name" if !length $ps->name;
		die "app->process->run: процесс № " .$ps->pid. " ".$ps->name." уже запущен, вначале остановите его" if $ps->pid;
		my $execfile = $ps->execfile;
		my $exec = $ps->exec;
		my $init = $ps->init; $init = join "", "\n$init;\n";
		my $X = $^X;
		my $xpath = $execfile->path;
		
		if(length $exec) {
			my $interpreter = $exec =~ s!^#\!(\S+)\s*!!? $1: "perl";

			if($interpreter eq "perl") {
				$X = $^X;
			} else {
				my $f = $app->file($interpreter);
				# if($ENV{PATHEXT}) {
					# $f = $f->add(map { s/^\.//; $f->ext($_) } split /;/, lc $ENV{PATHEXT});
				# }

				$f = $f->whereis->filter(sub { $_->mod & 0b100100100 });

				die "не найден #!$1" if !$f->length;
				
				$X = $f->path;
			}
			
			my $qqname = $app->magnitudeLiteral->escjs( $ps->name );

			if($interpreter eq "perl") {
				my $dir = $ps->dir;
				my $cd = "";
				if(length $dir) {
					$dir = $app->file($dir)->abs->path;
					die "нет директории `$dir`" if !-d $dir;
					$cd = "chdir \"".$app->magnitudeLiteral->escjs($dir)."\" or die \$!; ";
				}
				
				my @lib = $app->framework->path("lib");
				push @lib, length($dir)? "$dir/lib": $app->project->path . "/lib";
				
				my $lib = join ", ", map {'"'.$app->magnitudeLiteral->escjs($_).'"'} @lib;
				
				my $pidfile = $app->magnitudeLiteral->escjs($ps->pidfile->abs->path);
				my $runfile = $app->magnitudeLiteral->escjs($ps->runfile->abs->path);
				
				$exec = "BEGIN { ${cd}push \@INC, $lib }
					use common::sense;
					use R::App;
					BEGIN {
						\$app->process->part->{name} = \"$qqname\";
					}
					
					$init
					
					my \$PIDFILE = \"$pidfile\";
					\$app->file(\$PIDFILE)->write(\$\$);
					\$app->file(\"$runfile\")->rm;

					END { unlink \$PIDFILE or warn \"not unlink pidfile:\$! (\$PIDFILE)\" }
					
					msg1 'запустился', ':red', \$\$, ':black bold', \$app->process->name;
					
					$exec
				";
			}
			elsif($interpreter eq "node") {
				my $pidfile = $app->magnitudeLiteral->escjs( $ps->pidfile->cygwin->path );
				my $runfile = $app->magnitudeLiteral->escjs( $ps->runfile->cygwin->path );
				$exec = "
					$init
					(function() {
						var fs = require('fs')
						var pidfile = '$pidfile'
						fs.writeFileSync(pidfile, process.pid)
						fs.unlinkSync('$runfile')
						process.on('exit', function() {
							fs.unlinkSync(pidfile)
						});
					})();
					
					console.log('запустился', process.pid, '$qqname')
					
					$exec
				";
				$xpath = $app->file($xpath)->cygwin->path;
				
				$ENV{NODE_PATH} = $app->framework->file("share/node_modules")->cygwin->path;
			}
			else {
				die "неизвестный $interpreter";
			}
			
			$execfile->mkpath->write($exec);
			
			#msg1 "exec: $exec", $X;
			#exit;
		}
		elsif(!$execfile->exists) {
			die "app->process->run: нет ни exec, ни " . $execfile->path;
		}
	
	
		#my $path = $file->path;
		#link $^X, $path or $app->file($^X)->cp($path) if !-e $path;
		
		my ($STDERR, $STDOUT);
		$STDOUT = $self->_pipe(\*STDOUT, "out") if $self->{stdout};
		$STDERR = $self->_pipe(\*STDERR, "err") if $self->{stderr};
		
		utf8::encode($X);
		my $xname = $ps->name;
		my @argv = $self->argv;

		my $pid = spawn $X, [map { utf8::encode($_); $_ } $xname, $xpath, @argv];

		# выполяняем гуарды
		undef $STDOUT;
		undef $STDERR;
		
		msg1 "процесс ${\$ps->name} не смог запуститься: $!" if !defined $pid;

		$ps->runfile->write($pid);
		
		$self->{parts}[$i++]{pid} = $pid;
	}
	
	$self
}

# стартует процессы
sub start {
	my ($self) = @_;
	if($self->{bg}) {
		for my $ps ($self->slices) {
			die "Process.start: процесс № " .$ps->pid. " уже запущен, вначале остановите его" if $ps->exists;
			my $pid = fork;
			die "не могу создать процесс для демонизации: $!" if !defined $pid or $pid < 0;
			if($pid == 0) {
				@{ $ps->{fd} } = grep { $_ > 2 } @{ $ps->{fd} };	# 0,1,2 - удаляем
				$ps->newsid;	# новая сессия
				$ps->run;		# запускаем
				exit;			# завершаем промежуточный процесс
			}
		}		
	}
	else {
		$self->run
	}
}

# рестартует процессы. Если процесс не был запущен, то он стартует
sub restart {
	my ($self) = @_;
	for my $ps ($self->slices) {
		$ps->stop if $ps->exists;
		$ps->start;
	}
	$self
}

# отключает процесс от терминала
sub newsid {
	my ($self) = @_;
	die "Не удалось отсоединится от терминала\n" if setsid() < 0;
	my $maxfd = POSIX::sysconf(&POSIX::_SC_OPEN_MAX) || 1024;
	my %openfd = $app->perl->set( @{$self->{fd}} );
	for(my $i=0; $i<$maxfd; $i++) {
		close $i if !exists $openfd{$i};
	}
	open STDIN, "</dev/null";
	open STDOUT, ">/dev/null";
	open STDERR, ">&STDOUT";
	$self
}

# посылает сигнал INT всем, если не указан другой
sub kill {
	my ($self, $sig) = @_;
	$sig //= "INT";
	for my $ps ($self->slices) {
		my $pid = $ps->pid;
		die "kill $sig: ".($ps->name || "<unnamed>")." не имеет pid" if !Num $pid;
		$ps->{'kill-res'} = kill $sig, $pid;
	}
	
	$self
}

# процесс имеет pid-файл и существует указанный в нём процесс
sub exists {
	my ($self) = @_;
	my $pid = $self->pid;
	return if !Num $pid;
	$self->check_pid;		# удаляем зомби, если это дочерний процесс
	kill 0, $pid;
}

# останавливает процессы
sub stop {
	my ($self, $sig, $timeout, $interval) = @_;
	$self->active->kill($sig // "INT")->wait($timeout // 5, $interval);
	$self
}

# перезагружает процесс
sub reload {
	my ($self) = @_;
	my $path = $app->perl->dump($self->path);
	my $argv = join ", ", map { $app->perl->dump($_) } $self->argv;
	eval "END { CORE::exec $path, $argv }";
	exit;
}

# перезагружает процесс при изменении пакетов perl
sub wait_autoreload {
	my ($self) = @_;	
	waits until $app->file(values %INC)->maxmtime > $^T;
	$app->log->info( ":red space", $self->name, ":green", "RELOAD " );
	$self->reload;
}

# перезагружает процесс если пакеты perl изменились
sub autoreload {
	my ($self) = @_;	
	return until $app->file(values %INC)->maxmtime > $^T;
	$app->log->info( ":red space", $self->name, ":green", "RELOAD " );
	$self->reload;
}

# ожидает завершения процессов
sub wait {
	my ($self, $times, $interval) = @_;
	$times //= LONG_MAX;
	$interval //= 1;
	my $sec = $times;
	
	while((my $active = $self->active)->length) {
		if(0 >= $sec--) {
			if($active->length == 1) {
				msg("закончилось время ожидания завершения процесса № ".$self->pid." ".$self->name.". Убиваю его 9-м сигналом");
			
			} else {
				msg("закончилось время ожидания завершения процессов ".$active->map(sub { "№ ".$_->pid." ".$_->name }).". Убиваю их 9-м сигналом");
			}
			$active->kill("KILL")->kill_zombie;
		}
		sleep $interval;
	}
		
	$self
}

# НЕ ДОЛЖЕН РАБОТАТЬ С CORO
# # ожидает завершения процессов, если они дочерние
# sub waitkid {
	# my ($self, $i) = @_;
	# my $pid = $self->eq($i)->pid;
	# return $self if !Num $pid;
	# waitpid $pid, 0;	#, WNOHANG;
	# $self
# }

# удаляет зомби
sub check_pid {
	my ($self, $i) = @_;
	my $pid = $self->eq($i)->pid;
	return 0, -1, "нет pid" if !Num $pid;
	local ($!, $?);
	my $kid = waitpid $pid, WNOHANG;
	return $kid, $?, $!
}


# удаляет всех дочерних зомби
sub kill_zombie {
	my ($self) = @_;
	
	local ($!, $?);
	
	while((my $kid = waitpid -1, WNOHANG)>0) {
		my $ps = $self->one({pid => $kid})->loadname;
		$ps->runfile->rm;
		$ps->pidfile->rm;
	}
	
	$self
}

# бесконечный цикл ожидания и восстановления потомков
sub immortal {
	my ($self, $interval) = @_;
	
	$interval //= 1;
	
	while() {
		sleep $interval;
		$self->passive->then(sub {
			msg1 "завершился процесс ${\$_->name} #$_->{parts}[0]{pid}. Восстанавливаю";
			$_->run;
		})->kill_zombie;
	}
	
}

# процесс запуска: выполняет процедуру запуска процесса
sub running {
	my ($self, $fn) = @_;
	my $runfile = $self->runfile->mkpath->write($$);
	$fn->();
	$self->pidfile->write($$);
	$runfile->rm;
	$self
}

# возвращает файл процесса
sub file {
	my ($self, $i) = @_;
	return if $self->length <= $i;
	$app->project->file("var/process/$self->{parts}[$i]{name}")
}

# возвращает файл процесса
sub execfile {
	my ($self, $i) = @_;
	return if $self->length <= $i;
	$app->project->file("var/process/$self->{parts}[$i]{name}.script")
}

# возвращает pid-файл для первого процесса
sub pidfile {
	my ($self, $i) = @_;
	return if $self->length <= $i;
	$app->project->file("var/process/$self->{parts}[$i]{name}.pid")
}

# возвращает run-файл для первого процесса (run-файл - то же что pid-файл, но он создаётся при запуске, а потом - удаляется)
sub runfile {
	my ($self, $i) = @_;
	return if $self->length <= $i;
	$app->project->file("var/process/$self->{parts}[$i]{name}.run")
}

# загружает pid из pid-файлов
sub loadpid {
	my ($self) = @_;
	for my $ps ( $self->slices ) {
		$ps->part->{pid} = eval { $ps->pidfile->read } // eval { $ps->runfile->read };
	}
	$self
}

# удаляет pid-файл
sub erasepid {
	my ($self) = @_;
	for my $ps ( $self->slices ) {
		eval { $ps->pidfile->rm };
	}
	$self
}

# загружает имена по pid
sub loadname {
	my ($self) = @_;
	local $_;
	my %pid;
	$app->project->file("var/process/*.{run,pid}")->glob->then(sub {
		my $pid = eval { $_->read } or return;
		$pid{$pid} = $_->name;
	});
	
	$_->{name} = $pid{$_->{pid}} for $self->parts;
	
	$self
}

# удаление файлов процессов
sub erase {
	my ($self, $sig, $sec, $int) = @_;
	$self->stop($sig, $sec, $int)->then(sub {
		$app->log->error("процесс ".$_->name." не уничтожен"), return if $_->exists;
		$_->execfile->rm;
		$_->runfile->rm;
		$_->pidfile->rm;
	})
}

# возвращает максимальное количество файловых дескрипторов для процесса в системе
sub maxfd {
	require POSIX;
    POSIX::sysconf( &POSIX::_SC_OPEN_MAX )
}

1;
