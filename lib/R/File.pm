package R::File;
# синхронный файловый интерфейс
# файловые пути всегда в utf8

use base R::Collection;

use common::sense;
use R::App;

use Time::HiRes qw/stat/;
use Symbol qw/gensym/;

my $DEFAULT_BUFSIZE = 1024*1024*8;

# конструктор
sub new {
	my $cls = shift;
	$cls = ref $cls || $cls;
	bless {
		parts => [map {
			Isa($_, $cls)? $_->parts:
			Can($_, "sysread") || ref($_) =~ /^(SCALAR|GLOB)\z/? $_:
			ref $_? die("добавляется в файловую коллекцию ".ref($_).", а не путь и не файл"): 
			$_
		} @_],
	}, $cls;
}

# перекрывает исходный клон, чтобы было скорее
sub clone {
	my $self = shift;
	my $new = bless {%$self, @_}, ref $self || $self;
	wantarray? @{$new->{parts}}: $new;
}

# превращает в картинку
sub image {
	my ($self) = @_;
	$app->image->load($self)
}

# устанавливает/возвращает "удалённый" файл
# это имя для http-параметра multipart-form/data
sub remote {
	my $self = shift;
	if(@_) {
		$self->clone(remote => shift());
	} else {
		$self->{remote}
	}
}

# устанавливает/возвращает кодировку файла
sub encode {
	my $self = shift;
	if(@_) {
		$self->clone(encode => shift());
	} else {
		$self->{encode}
	}
}

# возвращает layer для binmode
sub encode_layer {
	my ($self) = @_;
	!exists $self->{encode}? ":utf8": !$self->{encode}? ":raw": ":encoding($self->{encode})"
}

# устанавливает/возвращает майм-тип файла, который используется для mime
sub type {
	my $self = shift;
	if(@_) {
		$self->clone(type => shift());
	} else {
		$self->{type}
	}
}

my $MAGIC;
sub _magic {
	my ($self) = @_;
	return $MAGIC->info_from_filename($self->{path}[0])->{mime_with_encoding};
}

# возвращает mime-тип на основе магического числа в файле
sub magic {
	require "File/LibMagic.pm";
    $MAGIC = File::LibMagic->new;
    no warnings 'redefine';
    *magic = *_magic;
	goto &magic;
}

# возвращает объект mime на основе типа файла
sub mime {
	my ($self) = @_;
	$app->mime->Get($self->type // $self->ext)
}

# возвращает файл в base64
# background-image: url("data:image/ТИП;base64,КОД");
# <img src="data:image/ТИП;base64,КОД">
sub img {
	my ($self) = @_;
	my $mime = $app->mime->Type($self->type // $self->ext);
	require "MIME/Base64.pm";
	local $self->{encode} = undef;
	my $data = MIME::Base64::encode_base64($self->read, "");
	"data:$mime;base64,$data"
}

# дополняет url('...')
sub css_img {
	my ($self) = @_;
	my $data = $self->img;
	"url(\"$data\")"
}


# устанавливает/возвращает режим чтения: >> или >
sub mode {
	my $self = shift;
	if(@_) {
		$self->clone(mode => shift());
	} else {
		$self->{mode}
	}
}

# устанавливает/возвращает для cp размер буфера
sub bufsize {
	my $self = shift;
	if(@_) {
		$self->clone(bufsize => shift());
	} else {
		$self->{bufsize}
	}
}


# создаёт файлы из строчных путей и из файлов
sub from {
	my $self = shift;
	$self->clone(parts => [ map { ref $_? $_->parts: $_ } @_ ]);
}

# преобразовывает пути файлов
sub filemap {
	my ($self, $sub) = @_;
	local $_;
	$self->clone(parts => [map { my $save=$_; $_ = $self->one($_); my $ret=$sub->(); $_=$save; $ret } $self->parts]);
}

# для сортировки
sub _cmp {
	$_[0] cmp $_[1]
}

# возвращает функцию для сравнения файлов. Она используется в find для отсечения ненужных файлов
sub _filters {
	my $self = shift;
	my @filters;
	
	for my $filter (@_) {
		if(!ref $filter and $filter =~ /^-\w$/) {
			push @filters, eval "sub { $filter }";
		}
		elsif(!ref $filter) {
			push @filters, eval(join "", "sub { scalar m(", $app->perl->likes($filter), ") }") // die;
		}
		elsif(ref $filter eq "Regexp") {
			push @filters, eval "sub{ scalar m{$filter} }" // die;
		}
		elsif(ref $filter eq "ARRAY") {
			push @filters, eval join("", "sub { !m{", join("|", 
				map { ref $_ eq "Regexp"? $_: 
					ref $_? die "В фильтре что-то странное `$_` ???": 
					$app->perl->likes($_, "P")
				} @$filter), "} }") // die;
		}
		elsif(ref $filter eq "CODE") {
			push @filters, $filter;
		}
		else {
			die "параметр - не функция и не Regexp: $filter " . ref $filter;
		}
	}

	R::reduce { (sub { my ($x, $y)=@_; sub { $x->() && $y->() } })->($a, $b) } @filters
}

# ищет файлы и применяет к ним функцию
sub find {
	my $self = shift;
	
	my $parts = [];
	my $p = $self->{parts};
	
	if(@$p) {
	
		my $filters = $self->_filters(@_);

		require File::Find;

		File::Find::find({
			no_chdir => 1,
			wanted => ($filters? sub {
				utf8::decode($File::Find::name);
				local $_ = $File::Find::name;
				return if !$filters->();
				push @$parts, $File::Find::name;
			}: sub {
				utf8::decode($File::Find::name);
				push @$parts, $File::Find::name;
			}),
		}, @$p);

	}
	
	@$parts = sort @$parts;
	$self->clone(parts => $parts)
}

# пути файлов через разделитель
# разделитель по умолчанию - запятая
sub join {
	my ($self, $sep) = @_;
	$sep //= ",";
	join $sep, @{$self->{parts}}
}

# пропускает имена файлов маской - не модифицирует изначальный массив
sub glob {
	my $self = shift;
	
	my $parts = [];
	
	for my $file (@{$self->{parts}}) {
		push @$parts, map { utf8::decode($_); $_ } glob $file;
	}

	$self->clone(parts => $parts)->grep(@_)
}

# список файлов из директорий или эти файлы
sub ls {
	my $self = shift;
	my $parts = [];
	for my $path ($self->parts) {
		push(@$parts, $path), next if !-d $path;
		$path =~ s!/$!!;
		my $dir = gensym();
		opendir $dir, $path or die "не могу открыть каталог `$path`: $!";
		my $close_defer = guard { closedir $dir };
		while(my $file = readdir $dir) {
			utf8::decode($file);
			next if $file =~ /^\.{1,2}\z/;
			push @$parts, $path eq "."? $file: "$path/$file";
		}
		@$parts = sort @$parts;
	}
	$self->clone(parts => $parts)->grep(@_)
}

# a моложе b
# чем моложе файл - тем время модификации больше
# все файлы a должны быть младше всех файлов b
sub youngest {
	my $self = shift;
	$self->minmtime > $self->from(@_)->maxmtime 
}

# a старше b
# a обновлялся до того как обновлялся b
# чем старше файл - тем время модификации меньше
# значит a имеет меньшее mtime
# все файлы a должны быть старше всех файлов b
sub oldest {
	my $self = shift;
	$self->maxmtime < $self->from(@_)->minmtime
}

# youngling - детёныш, незрелый, юнец, новичёк
# хотя бы один файл из a младше 
sub youngling {
	my ($self) = @_;
	$self
}

# минимальное mtime
sub minmtime {
	my $self = shift;
	$self->reduce(0+"inf", sub { my $y=$b->mtime; $a < $y? $a: $y })
}

# максимальное mtime
sub maxmtime {
	my $self = shift;
	$self->reduce(0, sub { my $y=$b->mtime; $a > $y? $a: $y })
}

# возвращает время модификации
# чем моложе файл - тем время модификации больше
sub mtime {
	my $self = shift;
	#my ($dev,$ino,$mode,$nlink,$uid,$gid,$rdev,$size,$atime,$mtime,$ctime,$blksize,$blocks) = stat $self->{parts}[0];
	my $mtime = (stat $self->{parts}[0])[9];
	#$! = undef;
	$mtime
}

# возвращает время создания
sub ctime {
	my $self = shift;
	#my ($dev,$ino,$mode,$nlink,$uid,$gid,$rdev,$size,$atime,$mtime,$ctime,$blksize,$blocks) = stat $self->{parts}[0];
	my $ctime = (stat $self->{parts}[0])[10];
	#$! = undef;
	$ctime
}

# возвращает время доступа
sub atime {
	my $self = shift;
	#my ($dev,$ino,$mode,$nlink,$uid,$gid,$rdev,$size,$atime,$mtime,$ctime,$blksize,$blocks) = stat $self->{parts}[0];
	my $atime = (stat $self->{parts}[0])[8];
	#$! = undef;
	$atime
}

sub _mtime { (stat $_[0])[9] }

# номер файловой системы
sub device {
	my $self = shift;
	my $dev = (stat $self->{parts}[0])[0];
	#$! = undef;
	$dev
}

# номер в таблице inode
sub inode {
	my $self = shift;
	my $inode = (stat $self->{parts}[0])[1];
	#$! = undef;
	$inode
}

# возвращает и устанавливает тип и права
sub mod {
	my $self = shift;
	if(@_) {
		my ($mod) = @_;
        $mod = oct($mod) if !Num $mod;
		for my $path (@{$self->{parts}}) {
			chmod $mod, $path;
		}
		#$! = undef;
		$self
	}
	else {
		my $mod = (stat $self->{parts}[0])[2];
		#$! = undef;
		$mod
	}
}

# возвращает первую строку
sub firstline {
	my ($self) = @_;
	my $f = $self->open("<");
	my $line = <$f>;
	close $f;
	$line
}

# проверяет, что 1-й файл является скриптом
sub script {
	my ($self, $interpreter) = @_;
	my $path = $self->path;
	return unless -e $path && -r $path && -T $path;
	$self->firstline =~ /^#!.*\b$interpreter\b/
}

# файл можно модифицировать
sub writable {
	my ($self) = @_;
	-w $self->{parts}[0]
}

# файл можно читать
sub readable {
	my ($self) = @_;
	-r $self->{parts}[0]
}

# файл можно выполнять
sub executable {
	my ($self) = @_;
	-x $self->{parts}[0]
}


# файл остаётся в памяти после выгрузки
sub sticky {
	my ($self) = @_;
	#http://search.cpan.org/~bdfoy/PerlPowerTools-1.011/bin/chmod
	-k $self->{parts}[0]
}

# это переменная в памяти
sub ismem {
	my ($self) = @_;
	ref $self->{parts}[0] eq "SCALAR"
}

# это директория
sub isdir {
	my ($self) = @_;
	-d $self->{parts}[0]
}

# это регулярный файл
sub isfile {
	my ($self) = @_;
	-f $self->{parts}[0]
}

# это символьная ссылка
*issymlink = \&islink;
sub islink {
	my ($self) = @_;
	-l $self->{parts}[0]
}

# это именованная очередь
sub isfifo {
	my ($self) = @_;
	-p $self->{parts}[0]
}

# это именованный сокет
sub issock {
	my ($self) = @_;
	-S $self->{parts}[0]
}

# это устройство
*isdev = \&isdevice;
sub isdevice {
	my ($self) = @_;
	my $path = $self->{parts}[0];
	(-c $path) || -b $path
}

# это символьное устройство
sub ischar {
	my ($self) = @_;
	-c $self->{parts}[0];
}

# это блочное устройство
sub isblock {
	my ($self) = @_;
	-b $self->{parts}[0];
}

# файл связан с терминалом
sub istty {
	my ($self) = @_;
	-t $self->{parts}[0]
}

# это текстовый файл
sub istext {
	my ($self) = @_;
	-T $self->{parts}[0]
}

# это бинарный файл
*isbin = \&isbinary;
sub isbinary {
	my ($self) = @_;
	-B $self->{parts}[0]
}

# файл существует
sub exists {
	my ($self) = @_;
	-e $self->{parts}[0]
}

# все существующие файлы в коллекции
sub existing {
	my ($self) = @_;
	$self->clone(parts => [grep {-e $_} $self->parts])
}

# размер файла в байтах
sub size {
	my ($self) = @_;
	-s $self->{parts}[0]
}


# это свой файл
sub owned {
	my ($self) = @_;
	-O $self->{parts}[0]
}


# -R  File is readable by real uid/gid.
# -W  File is writable by real uid/gid.
# -X  File is executable by real uid/gid.
# -O  File is owned by real uid.
# -z  File has zero size (is empty).

# -u  File has setuid bit set.
# -g  File has setgid bit set.
# -k  File has sticky bit set.
# -B  File is a "binary" file (opposite of -T).
# -M  Script start time minus file modification time, in days.
# -A  Same for access time.
# -C  Same for inode change time (Unix, may differ for other platforms)


# -r  File is readable by effective uid/gid.
# -w  File is writable by effective uid/gid.
# -x  File is executable by effective uid/gid.
# -o  File is owned by effective uid.
# -R  File is readable by real uid/gid.
# -W  File is writable by real uid/gid.
# -X  File is executable by real uid/gid.
# -O  File is owned by real uid.
# -e  File exists.
# -z  File has zero size (is empty).
# -s  File has nonzero size (returns size in bytes).
# -f  File is a plain file.
# -d  File is a directory.
# -l  File is a symbolic link (false if symlinks aren't supported by the file system).
# -p  File is a named pipe (FIFO), or Filehandle is a pipe.
# -S  File is a socket.
# -b  File is a block special file.
# -c  File is a character special file.
# -t  Filehandle is opened to a tty.
# -u  File has setuid bit set.
# -g  File has setgid bit set.
# -k  File has sticky bit set.
# -T  File is an ASCII or UTF-8 text file (heuristic guess).
# -B  File is a "binary" file (opposite of -T).
# -M  Script start time minus file modification time, in days.
# -A  Same for access time.
# -C  Same for inode change time (Unix, may differ for other platforms)


# подсчитывает размер всех файлов и папок
sub space {
	my ($self) = @_;
	my $space = 0;
	$self->find(sub {
		$space += -s $_;
		0;
	});
	$space
}

# размер одних файлов
sub space_usage {
	my ($self) = @_;
	my $space = 0;
	$self->find(sub {
		$space += -s $_ if -f $_;
		0;
	});
	$space
}

# удаляет файлы и директории с файлами
sub rm {
	my ($self) = @_;
	my @dir;
	$self->find(sub {
		if(-d $_) {unshift @dir, $_} else { undef $!; msg("rm: не могу удалить файл `$_`: $!") unless unlink $_}
		0
	});
	do { undef $!; msg("rm: не могу удалить каталог `$_`: $!") unless rmdir $_ } for @dir;
	$self
}

# удаляет только директории
sub rmdir {
	my ($self) = @_;
	for my $f ($self->parts) {
		rmdir $f or die "не могу удалить: $!";
	}
	return $self;
}

# удаляет только файлы
sub rmfile {
	my ($self) = @_;
	for my $f ($self->parts) {
		unlink $f or die "не могу удалить: $!";
	}
	return $self;
}

# удаляет всё в указанной директории
*rmcontent = \&rmdown;
sub rmdown {
	my ($self) = @_;
	for my $dir (@{$self->{parts}}) {
		$self->new( <$dir/*> )->rm;
	}
	$self
}

# читает несыкающиеся файлы сплошняком
sub cat {
	my ($self, $re) = @_;
	my $file = join "\n", $self->readlines("\n", 1);
	($file) = $file =~ $re if defined $re;
	$file
}

# читает первый файл целиком или указанный размер
sub read {
	my ($self, $size) = @_;
	return ${$self->{parts}[0]} if ref $self->{parts}[0] eq "SCALAR";
	my $f = $self->open;
	$f->read(my($body), $size // -s $f);
	$f->close;
	return $body;
}

# считывает все файлы через разделитель, а если разделитель не указан и wantarray, то выдаёт в виде массива
sub reads {
	my ($self, $delim) = @_;
	!defined($delim) && wantarray? (map { $self->one($_)->read } $self->parts):
		(join $delim, map { $self->one($_)->read } $self->parts );
}

# устанавливает межстрочный сепаратор
sub sep {
	my $self = shift;
	if(@_) {
		$self->clone(sep => shift());
	} else {
		$self->{sep}
	}
}

# устанавливает обрезать ли разделитель строки
sub chop {
	my $self = shift;
	if(@_) {
		$self->clone(chop => shift());
	} else {
		$self->{chop}+0
	}
}

# считывает строки в массив через разделитель из первого файла
sub readln {
	my ($self, $sep, $chop) = @_;
	$sep //= $self->{sep} // "\n";
	$chop //= $self->{chop} // 1;
	
	my $f = $self->open("<");
	local $_;
	local $/ = $sep;
	my $out = [];
	if($chop) {
		chomp($_), push @$out, $_ while <$f>;
	} else {
		push @$out, $_ while <$f>;
	}
	wantarray? @$out: $out
}

# считывает строки в массив через разделитель из всех файлов
sub readlines {
	my ($self, $sep, $chop) = @_;
	
	$self->map(sub { $_->readln($sep, $chop) })
}

# записывает в 1-й файл
sub write {
	my $self = shift;
	my $f = $self->open($self->{mode} // ">");
	local $_;
	local $\ = "";
	$f->print($_) for @_;
	$f->close;
	$self
}

# дописывает в конец файла
sub append {
	my $self = shift;
	$self->clone(mode => '>>')->write(@_);
	$self
}

# записывает во все файлы
sub writes {
	my $self = shift;
	for my $file ($self->parts) {
		$self->one($file)->write(@_);
	}
	$self
}


# перезаписывает файлы через функцию
# если контент не изменился - не записывает и время модификации не меняется
sub replace {
	my ($self, $block) = splice @_, 0, 2;
	local $_;
	
	my $i = 0;
	for my $path (@{$self->{parts}}) {
		my $f = $self->one($path);
		my $buf = $_ = $f->read;
		$block->($f, $i, @_);
		$f->write($_) if $_ ne $buf;
		$i++;
	}
	$self
}

# путь файла с индексом idx в коллекции
sub path {
	my ($self, $idx) = @_;
	$self->{parts}[$idx]
}

# переводит файл в путь
sub topath {
	my ($self, $to) = @_;
	Isa($to, "R::File")? $to->{parts}[0]: $to;
}

# переводит путь в файл
sub tofile {
	my ($self, $to) = @_;
	Isa($to, "R::File")? $to: $self->new($to);
}


# копирует файлы
# чтобы скопировать все файлы в директорию используйте слеш на конце "dir/", иначе - скопируются только первый
sub cp {
	my ($self, $to) = @_;
	
	$to = $self->topath($to);
	
	my $bufsize = $self->{bufsize} // $DEFAULT_BUFSIZE;
	
	my $dir = $to =~ m!/$!;
	
	my $self = $self->encode(undef);
	
	for my $path ($dir? @{$self->{parts}}: $self->{parts}[0]) {
	
		my $f = $self->one($path);
		my $k = $self->one($dir? $to . $f->file: $to);
		
		my $from = $f->open;
		my $file_to = $k->open(">");
		
		my $buf;
		
		for(;;) {
			last unless $from->read($buf, $bufsize);
			$file_to->print( $buf );
		}
	}
	
	$self
}

# переносит файлы
# используйте / в конце пути, чтобы перенести все файлы в одну директорию, иначе перенесётся только первый
sub mv {
	my ($self, $to) = @_;
	
	$to = $self->topath($to);
	
	if($to =~ m!/$!) {
		for my $from (@{$self->{parts}}) {
			my $file = $self->one($from);
			my $to_path = $to . $file->file;
			$file->cp($to_path), unlink $from unless rename $from, $to_path;
		}
	} else {
		my $from = $self->{parts}[0];
		$self->cp($to), unlink $from unless rename $from, $to;
	}
	$! = undef;
	$self
}

# открывает первый файл и возвращает описатель
sub open {
	my ($self, $mode) = @_;
	my $path = $self->{parts}[0];
	
	my $encode = $self->encode_layer;
	
	if(ref $path eq "SCALAR") {
		R::msg("снят флаг utf8!"), utf8::encode($$path) if utf8::is_utf8($$path);
	}
	elsif(ref $path) {	# хандлер
		binmode $path, $encode;
		return $path;
	}
	
	my $mode = ($mode // $self->{mode} // "<") . $encode;
	
	undef $!;
	my $f = gensym();
	open $f, $mode, $path or die "file($path)->open($mode): Не могу открыть: $!\n";
	
	# для Coro
	#$f = Coro::Handle->new_from_fh($f) if $Coro::main;
	
	$f
}

# возвращает файловый хандлер + защищённую переменную его закрывающую
sub open_cv {
	my $self = shift;
	my $f = $self->open(@_);
	my $cv;
	$cv = R::guard { $f->close } if ref $self->{parts}[0] ne "GLOB" and !Can $self->{parts}[0], "sysread";
	return $f, $cv;
}

# возвращает ссылку на хэш связанный с файлом в формате dbm
sub dbm {
	my ($self) = @_;
	my $hash = {};
	undef $!;
	dbmopen %$hash, $self->path, 0642 or die "dbm(" . $self->path . "): Не могу открыть: $!\n";
	$hash
}

# меняет начальную директорию на dir2 у всех файлов. Ошибка, если хоть у одного нет такой директории
sub subdir {
	my ($self, $dir, $dir2) = @_;
	local ($', $');
	$dir .= "/" if $dir ne "" and $dir !~ m!/$!;
	$dir2 .= "/" if $dir2 ne "" and $dir2 !~ m!/$!;
	my $parts = [];
	for my $path (@{$self->{parts}}) {
		die "$path не начинается с $dir" if $dir ne substr $path, 0, length $dir;
		push @$parts, $dir2 . substr $path, length $dir;
	}
	$self->clone(parts => $parts);
}

# root/mmd/dir1/dir2/name.ext1.ext2.ext3
# ext = ext3
# exts = ext1.ext2.ext3
# name = name.ext1.ext2
# nik = name
# file = name.ext1.ext2.ext3
# dir = root/mmd/dir1/dir2
# subdir(root/mmd) => dir1/dir2/name.ext1.ext2.ext3
# subdir(root/mmd => rss/mmx) => rss/mmx/dir1/dir2/name.ext1.ext2.ext3

# возвращает каталог первого файла или заменяет у всех и возвращает клон
sub dir {
	my $self = shift;
	local ($`, $', $_);
	if(@_) {
		my $dir = shift;
		$dir .= "/" if $dir ne "" and $dir !~ m!/$!;
		my $parts = [];
		for my $path (@{$self->{parts}}) {
			my $file = $path; # $path в for является ссылкой - не копируется
			$file =~ s/^/$dir/ unless $file =~ s!.*/!$dir!s;
			push @$parts, $file;
		}
		$self->clone(parts => $parts);
	} else {
		$self->{parts}[0] =~ /\/(?:[^\/]*)$/? $`: "";
	}
}

# возвращает название первого файла nik+все расширения, кроме последнего
sub name {
	my $self = shift;
	local ($`, $', $1);
	if(@_) {
		my $name = shift;
		my $parts = [];
		for my $path (@{$self->{parts}}) {
			my $file = $path; # $path в for является ссылкой - не копируется
			$file =~ s!(?:[^/]+?)(\.[^\./]+)?$!$name$1!;
			push @$parts, $file;
		}
		$self->clone(parts => $parts);
	} else {
		$self->{parts}[0] =~ m!([^/]+?)(?:\.[^\./]+)?$!? $1: "";
	}
}

# возвращает имя без расширений
sub nik {
	my ($self) = @_;
	my $self = shift;
	local ($', $', $1);
	if(@_) {
		my $name = shift;
		my $parts = [];
		for my $path (@{$self->{parts}}) {
			my $file = $path; # $path в for является ссылкой - не копируется
			$file =~ s!(?:[^/\.]+)(\.[^/]+)?$!$name$1!;
			push @$parts, $file;
		}
		$self->clone(parts => $parts);
	} else {
		$self->{parts}[0] =~ m!([^/\.]+)(?:\.[^/]+)?$!? $1: "";
	}
}

# возвращает расширение первого файла
sub ext {
	my $self = shift;
	local ($', $', $1);
	if(@_) {
		my $ext = shift;
		$ext = ".$ext" if $ext ne "" and $ext !~ /^\./;
		my $parts = [];
		for my $path (@{$self->{parts}}) {
			my $file = $path; # $path в for является ссылкой - не копируется
			$file =~ s!$!$ext! unless $file =~ s!\.[^\./]+$!$ext!;
			push @$parts, $file;
		}
		$self->clone(parts => $parts);
	} else {
		$self->{parts}[0] =~ m!\.([^\./]+)$!? $1: "";
	}
}

# возвращает расширения первого файла
sub exts {
	my $self = shift;
	local ($', $', $1, $_);
	if(@_) {
		my $ext = shift;
		$ext = ".$ext" if $ext ne "" and $ext !~ /^\./;
		my $parts = [];
		for my $path (@{$self->{parts}}) {
			my $file = $path; # $path в for является ссылкой - не копируется
			$file =~ s!$!$ext! unless $file =~ s!\.[^/]+$!$ext!;
			push @$parts, $file;
		}
		$self->clone(parts => $parts);
	} else {
		$self->{parts}[0] =~ m!\.([^/]+)$!? $1: "";
	}
}

# имя + расширение первого файла
sub file {
	my $self = shift;
	local ($', $', $1, $_);
	if(@_) {
		my $main = shift;
		my $parts = [];
		for my $path (@{$self->{parts}}) {
			my $file = $path; # $path в for является ссылкой - не копируется
			$file =~ s!([^/]+?)$!$main!;
			push @$parts, $file;
		}
		$self->clone(parts => $parts);
	} else {
		$self->{parts}[0] =~ m!([^/]+?)$!? $1: "";
	}
}

# добавляет к файлу дату. Если есть такой же файл ...
sub adddate {
	my ($self, $ext) = @_;
	my $parts = [];
	my @ext = defined($ext)? ".$ext": ();
	my $time = $app->perl->strftime("%Y%m%d_%H%M%S", localtime);
	for my $path ($self->parts) {
		push @$parts, join "", $path, ".", $time, @ext;
	}
	$self->clone(parts => $parts);
}

# добавляет в конец пути файлов 
sub back {
	my ($self, $back) = @_;
	my $parts = [];
	for my $path ($self->parts) {
		push @$parts, $path . $back;
	}
	$self->clone(parts => $parts);
}

# добавляет в начало имени файлов
sub front {
	my ($self, $front) = @_;
	$front = $self->topath($front);
	my $parts = [];
	for my $path ($self->parts) {
		push @$parts, $front . $path;
	}
	$self->clone(parts => $parts);
}

# добавляет в начало имени файлов директорию
sub frontdir {
	my ($self, $front) = @_;
	
	$front = $self->topath($front);
	
	my $parts = [];
	
	$front =~ s!/?$!/!;
	
	for my $path ($self->parts) {
		my $file = $path;
		$file =~ s!^/?!$front!;
		push @$parts, $file;
	}
	$self->clone(parts => $parts);
}

# добавляет в конец файлов путь или пути, файлы считаются директориями
sub sub {
	my $self = shift;
	local $_;
	
	my $parts = [];
	
	for my $dir (map { Isa($_, "R::File")? $_->parts: $_ } @_) {
		$dir =~ s!^/?!/!;
	
		for my $file (@{$self->{parts}}) {
			my $path = $file;	# в for $file является ссылкой - не копируется
			$path =~ s!/?$!$dir!;
			push @$parts, $path;
		}
	}
	
	$self->clone(parts => $parts);
}

# возвращает текущий каталог
*cwd = \&pwd;
sub pwd {
	#return $ENV{PWD} if [(stat ".")[0..7]] ~~ [(stat $ENV{PWD})[0..7]];
	chop($ENV{PWD} = `pwd`) || die "pwd exit status: $?";
	return $ENV{PWD};
    #my $path = "/proc/$$/cwd";
    #readlink $path or do { my $pwd = `pwd`; chomp $pwd; $pwd } #die "not read link $path: $!";
}

# # возвращает текущий каталог
# sub cwd {
	# require "Cwd.pm";
	# Cwd::getcwd();
# }

# возвращает текущий каталог из окружения, а если нет - с помощью функции
# *cwd = \&pwd;
# sub pwd {
# 	my $tek = ".";
# 	my $tek_stat = [stat($tek)[0..7]];
# 	
# 	return $ENV{PWD} if $tek_stat ~~ [stat($ENV{PWD})[0..7]];
# 	
# 	my $parent = "..";
# 	my $parent_stat = [stat($parent)[0..7]];
# 	
# 	
# 	my @path;
# 	until($tek_stat ~~ $parent_stat) {
# 		my $found = 0;
# 		
# 		opendir my $dir, $parent or die $!;
# 		
# 		while(readdir $dir) {
# 			next if /^\.{1,2}\z/;
# 			
# 			my $path = "$parent/$_";
# 			my $stat = [stat($path)[0..7]];
# 			
# 			if($tek_stat ~~ $stat) {
# 				unshift @path, $_;
# 				$found = 1;
# 				last;
# 			}
# 		}
# 		
# 		die "not found!!!" if !$found;
# 	
# 		$tek = $parent;
# 		$tek_stat = $parent_stat;
# 		$parent .= "/..";
# 		$parent_stat = [stat($parent)[0..7]];
# 	}
# 	
# 	return $ENV{PWD} = "/" . join "/", @path;
# }

# переводит все пути в относительные
sub relative {
	my ($self, $pwd) = @_;
	$pwd //= $self->pwd;
	(undef, my @pwd) = split m!/!, $self->new($pwd)->abs->path;
	
	$self->clone(parts=>[map {
		(undef, my @x) = split m!/!, $_;
		
		my $i=0;
		while($i<@pwd && $i<@x && $pwd[$i] eq $x[$i]) {} continue { $i++ }
		
		my $r = join "/", ("..") x (@pwd - $i), @x[$i..$#x];
		$r = "." if !length $r;
		$r
	} $self->abs ])
}

# переводит в абсолютные все пути
sub abs {
	my ($self, $pwd, $empty) = @_;
		
	$pwd //= $self->pwd;
	$empty //= "/";
	
	# $pwd всегда заканчивается на / если не пусто
	$pwd =~ s!/?$!/! if $pwd ne "";
	
	my $parts = [];
	for my $xpath (@{$self->{parts}}) {
		my $path = $xpath;
		my $is_root = $path =~ m!^/!;
		$path = "$pwd$path" if !$is_root;
		my @path = split m!/+!, $path;
		for(my $i=0; $i<@path; $i++) {
			if($path[$i] eq "..") {
				die "выход за пределы рутовой директории (/) для `$path`" if $i <= 0;
				splice @path, $i-1, 2;
				$i-=2;
			} elsif($path[$i] eq ".") {
				splice @path, $i, 1;
				$i--;
			}
		}
		$path = @path == 0 || @path == 1 && $path[0] eq ""? ($is_root? "/": $empty): join "/", @path;
		push @$parts, $path;
	}
	
	$self->clone(parts => $parts);
}

# нормализует пути
sub norm {
	my ($self) = @_;
	$self->abs("", ".");
}

# удаляет эту директорию из всех путей
sub root {
	my ($self, $path) = @_;
	
	my $root = $self->new($path)->abs->path;
	$root =~ s!/?$!/!;
	
	$self->clone(parts=>[map { index($_, $root) == 0? substr($_, length($root)): $_ } $self->abs ])
}

# создаёт каталоги в пути для всех файлов
sub mkpath {
	my ($self) = @_;
	local ($_, $`, $');
	for(@{$self->{parts}}) {
		mkdir $` while /\//g;
	}
	$! = undef;
	$self
}

# возвращает коллекцию путей 0-го файла
sub paths {
	my ($self) = @_;
	
	my $parts = [];
	
	if(defined( local $_ = $self->{parts}[0] )) {
		push @$parts, $` while /\//g;
	}
	
	$self->clone(parts => $parts);
}

# создаёт директорию для всех файлов
sub mkdir {
	my ($self, $mode) = @_;
	
	$mode //= 0744;
	
	CORE::mkdir($_, $mode) for $self->parts;
	
	$self
}

# создаёт символьную ссылку c файлов коллекции на соответствующие в переданной коллекции
sub symlink {
	my ($self, $file) = @_;
	$file = $self->new($file);
	
	for my $pairs (R::zip $self->{parts}, $file->{parts}) {
		my ($f, $t) = @$pairs;
		last if !defined $f or !defined $t;
		symlink $f, $t or die "not create symlink $t -> $f: $!";
	}
	
	$self
}

# считывает символьные ссылки для всей коллекции. Не символьные ссылки - оставляет
sub readlink {
	my ($self) = @_;
	$self->clone(parts=>[ map { readlink($_) // $_ } @{$self->{parts}} ])
}

# удаляет все пустые директории вверх для всех файлов
sub rmpath {
	my ($self) = @_;
	local ($_, $`, $');
	for(@{$self->{parts}}) {
		my @path = ();
		push @path, $` while /\//g;
		rmdir $_ or last for reverse @path;
	}
	undef $!;
	$self
}

# переходит в 1-й каталог
*chdir = \&cd;
sub cd {
	my ($self) = @_;
	undef $!;
	chdir $self->{parts}[0];
	die "cd " . $self->{parts}[0] . " -> " . ($!+0) . ": $!" if $!;# && $! != 17;	# 17 - file exists
	# $ENV{OLDPWD} = $ENV{PWD};
	# $ENV{PWD} = $self->eq(0)->abs->path;
	$self
}

# возвращает гуард для перехода в директорию
sub withdir {
	my ($self) = @_;
	my $dir = $app->file($self->pwd);
	$self->cd;
	Guard::guard { $dir->cd if $dir && $dir->{parts} && $dir->{parts}[0] };
	
	# my $dir = $self->new(".")->abs;
	# $self->cd;
	# Guard::guard { $dir->cd if $dir && $dir->{parts} && $dir->{parts}[0] };
}

# возвращает $!
sub err {
	$!
}

# пакует файлы в bz2 и записывает в файлы c расширением bz2 или 1-й с указанным имененем
sub bzip2 {
	my ($self, $name, $bksize, $wf, $append) = @_;
	require IO::Compress::Bzip2;
	
	$name = $name->path if Isa $name, "R::File";
	
	$bksize //= 9;
	$wf //= 30;
	$append //= 0;
	
	for my $path (defined($name)? $self->{parts}[0]: $self->parts) {
		my $out = $name // "$path.bz2";
		IO::Compress::Bzip2::bzip2($path, $out, Append=>$append, BlockSize100K=>$bksize, WorkFactor=>$wf) or die "bzip2 failed: $IO::Compress::Bzip2::Bzip2Error";
	}
	$self
}

# пакует файлы в gzip и записывает в файлы c расширением gz или 1-й с указанным имененем
sub gzip {
	my ($self, $name, $compression, $strategy, $append) = @_;
	require IO::Compress::Gzip;
	
	$compression //= 6;
	$strategy //= "";
	$append //= 0;
	
	$name = $name->path if Isa $name, "R::File";
	
	for my $path (defined($name)? $self->{parts}[0]: $self->parts) {
		my $out = $name // "$path.gz";
		IO::Compress::Gzip::gzip($path, $out, Append=>$append, -Level=>$compression, Minimal=>1, AutoClose=>0) or die "gzip failed: $IO::Compress::Gzip::GzipError";
	}
	$self
}

# пакует файлы в Боротли и записывает в файлы c расширением br или 1-й с указанным имененем
# в качестве имени можно указыввать ссылку на скаляр и файл-хандлер
sub brzip {
	my ($self, $name) = @_;
	require IO::Compress::Brotli;
	
	$name = $name->path if Isa $name, "R::File";
	
	for my $path (defined($name)? $self->{parts}[0]: $self->parts) {
		my $out = $name // "$path.br";
		
		my $bro = IO::Compress::Brotli->create;
		my ($f, $f_cv) = $self->one($path)->encode(undef)->open_cv("<");
		my ($q, $q_cv) = $self->one($out)->encode(undef)->open_cv(">");
		my $len;
		my $bufsize = $self->{bufsize} // $DEFAULT_BUFSIZE;
		do {
			$len = $f->sysread(my $block, $bufsize);
			my $encoded_block = $bro->compress($block);
			$q->write($encoded_block);
		} while($len == $bufsize);
		
		$q->write($bro->finish());
		undef $f_cv;
		undef $q_cv;
	}
	$self
}

# выбирает по наименованию
sub anyzip {
	my ($self, $type, @args) = @_;
	
	if($type =~ /^(br|brzip|brotli)\z/n) { $self->brzip(@args) }
	elsif($type =~ /^(gzip|gz)\z/n) { $self->gzip(@args) }
	elsif($type eq "bzip2") { $self->bzip2(@args) }
	else { die "нет такого zip / $type" }
}

# https://metacpan.org/pod/IO::Compress::Brotli

# # 
# sub unzip {
	# my ($self) = @_;
	# $self
# }


# процесс наблюдения за файлами
sub watching {
	my ($self, $cb, $interval) = @_;
	$self->watch;
	
	$interval //= 1;
	
	while() {
		sleep $interval;
		my $w = $self->watch;
		$cb->($w) if $w->modify;
	}
	
	$self
}

# стартует watch
sub watchify {
	my ($self) = @_;
	$self->watch;
	$self
}

# Запоминает время файлов при первом вызове и сравнивает - не изменились ли при втором.
# Возвращает 3 файловых объекта: изменившиеся, появившиеся и исчезнувшие файлы.
sub watch {
	my ($self) = @_;
	
	my ($changed, $new, $deleted) = ([], [], []);
	
	my $watch = $self->{watch} //= {};
	my %watched;
	
	$self->find(sub {
		my $today = _mtime($_);
		my $before = $watch->{$_};

		#msg1 $before, $today, $_ if /var\/man-/;
		
		if(!defined $before) {
			push @$new, $_;
		}
		elsif($before < $today) {
			push @$changed, $_;
		}
		
		$watched{$_} = 1;
		$watch->{$_} = $today;
		0;
	});
	
	my @watch = keys %$watch;
	for my $path (@watch) {
		delete($watch->{$path}), push @$deleted, $path if !exists $watched{$path};
	}
	
	$changed = $self->clone(parts => $changed);
	$new = $self->clone(parts => $new);
	$deleted = $self->clone(parts => $deleted);
	
	$! = undef;
	
	wantarray? ($changed, $new, $deleted):
		$app->fileWatch->new(changed => $changed, created => $new, deleted => $deleted);
}


# как и watch, но сравнивает не с прежним значением, а с другим файловым массивом
sub cmpmtime {
	my ($self, $deps) = @_;

	my $watch = $deps->{watch};
	
	die "cmpmtime: файловые коллекции не одинаковы по размеру" if $self->length != $deps->length;
	
	my $i = 0;
	for my $root ($self->parts) {
		
		my $froot = $self->one($root);
		
		my $deproot = $deps->{parts}[$i];
		#my $fdeproot = $self->one($deproot);
		
		$root = quotemeta $root;
		$deproot = $deproot;
		
		for my $file ($froot->find->parts) {
			my $mtime = _mtime $file;
			$file =~ s!$root!$deproot!;
			$deps->{watch}{$file} = $mtime;
		}
		
	}

	my $res = $deps->watch($self);
	
	if(defined $watch) { $deps->{watch} = $watch } else { delete $deps->{watch} }
	
	wantarray? ($res->changed, $res->created, $res->deleted): $res;
}

# сравнивает один файл со всеми
sub cmp1mtime {
	my ($self, $deps) = @_;
	
	my $watch = $deps->{watch};
	
	my $mtime = $self->mtime;

	for my $dep ($deps->parts) {
		$deps->{watch}{$dep} = $mtime;
	}
	
	my ($changed, $new, $deleted) = $deps->watch($self);
	
	if(defined $watch) { $deps->{watch} = $watch } else { delete $deps->{watch} }
	
	return $changed->length || $new->length || $deleted->length;
}

# создаёт файловую коллекцию из %ENV - переменной
sub env {
	my ($self, $name, $sep) = @_;
	
	$name //= "PATH";
	
	$sep //= qr/[:;]/;
	
	$self->clone(parts=>[@{$self->{parts}}, split $sep, $ENV{$name}]);
}

# поиск по переменным
sub whereis {
	my ($self, $env) = @_;
	$self->env($env)->sub($self)->grep("-f")->unique
}

# возвращает виндовс пути, если OS - cygwin
sub cygwin {
	my ($self) = @_;
	$^O eq "cygwin"? $self->win: $self->clone
}

# возвращает windows пути
sub win {
	my ($self) = @_;
	$self->clone( parts => [map { 
		my $x=$_; 
		$x=~s!^/cygdrive/(\w+)!$1:! || $x=~s!^/!c:/cygwin64/!; 
		$x=~s!/!\\!g; 
	$x } @{$self->{parts}}] );
}

# подставляет значения из @INC
sub lib {
	my ($self) = @_;
	$self->new(@INC)->sub($self)->existing
}

# прибавляет всем путям / в конце
sub as_dir {
	my ($self) = @_;
	
	$self->clone(parts=>[map { my $x=$_; $x=~s!/?$!/!; $x } $self->parts])
}

# удаляет / в конце. Если путь "/", то заменяет на $root
sub as_file {
	my ($self, $root) = @_;
	
	$root //= "";
	
	$self->clone(parts=>[map { my $x=$_; $x=~s!/?$!!; length($x)? $x: $root } $self->parts])
}

# добавляет точку всем путям, которые не начинаются на / или ..
sub dot {
	my ($self) = @_;
	$self->clone(parts=>[map { m!^(/|\.{1,2}(/|$))!n? $_: "./$_" } $self->parts])
}

# если файла нет - создаёт. Иначе - меняет время модификации
sub touch {
	my ($self) = @_;
	my $f = $self->open(">>");
	print $f "";
	close $f;
	$self->clone
}

# возвращает первый путь в виде класса perl 
sub class {
	my ($self) = @_;
	my $class = $self->path;
	$class =~ s!\.(\w+)$!!;
	$class =~ s!/!::!g;	
	$class
}

# превращает пути из классов в файлы
sub fromclass {
	my ($self) = @_;
	$self->clone(parts => [map { my $f=$_; $f=~s!::!/!g; "$f.pm" } $self->parts])	
}

# считывает 1-й файл и возвращает пакет
sub get_class {
	my ($self) = @_;
	$self->read =~ /^package\s+([\w:]+)/m? $1: ""
}

# создаёт бэкап из файлов
sub bk {
	my ($self, $ext) = @_;
	
	for my $e ( $self->slices ) {
		$e->ext($ext // $e->ext . "~")->write($e->read) if $e->isfile;
	}
	
	$self
}

# восстанавливает из бекапа
sub unbk {
	my ($self, $ext) = @_;
	
	for my $e ( $self->slices ) {
		$e->ext($ext // substr($e->ext, 0, -1))->write($e->read) if $e->isfile;
	}
	
	$self
}

# запускает 0-й путь, возвращает stdout запущенной программы
sub run {
	my $self = shift;
	undef $!;
	my $path = $self->path;
	my @args = map { $app->magnitudeLiteral->to($_) } $path, @_;
	my $s = join " ", @args;
	my $res = `$s`;
	die "$path: $!" if $!;
	utf8::decode($res);
	return $res;
}

# grep, но в $_ - текст файла, а в первом параметре - файл
sub egrep {
	my ($self, $sub) = @_;
	$self->grep(sub { my $f=$self->one($_); local $_ = $f->read; $sub->($f) })
}

# файлы c тру-регуляркой
sub with {
	my ($self, $re) = @_;
	$self->egrep(sub { /$re/i })
}

# файлы c текстом
sub with_text {
	my ($self, $re) = @_;
	$self->egrep(sub { index($_, $re) > -1 })
}

# файлы c словом
sub with_word {
	my ($self, $re) = @_;
	$re = quotemeta $re;
	$self->egrep(sub { /\b$re\b/i })
}

# возвращает кодировку файла
sub codepage {
	my ($self, $size) = @_;
	
	require Encode::Detect::Detector;
	
	my $f = $self->encode(undef)->open;
	$size //= -s $f;
	
	my $detector = Encode::Detect::Detector->new;
	my $rd = 0;
	while($rd < $size) {
		$rd += read $f, my $x, 1024*4;
		$detector->handle($x);
	}
	close $f;
	$detector->eof;

	$detector->getresult
}

1;
