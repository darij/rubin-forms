package R::Hotswap;
# горячая замена пакетов perl

use common::sense;
use R::App;
use R::Coro;

# устанавливаем следилку за файлами

my $HOT = __PACKAGE__->new->startscan;
# push @INC, \&_inc_mtime;
# sub _inc_mtime {
	# my ($fn, $path) = @_;
	
	# msg1 my $f = $app->file(grep {!ref $_} @INC)->sub($path)->existing;
	
	
	# msg1 $f->path, $HOT->{scan}->{watch}->{$f->path} = $f->mtime if $f->length && $HOT->permission($f->path);
	# 0
# }


# свойства
has qw/pkg file debug/;

# конструктор
sub new {
	my $cls = shift;
	my $self = bless {
		pkg => undef, 	# пакет
		file => undef,	# файл
		scan_html => ["html"], 	# каталоги для компилляции кофескрипта
		scan => $HOT->{scan},
		@_
	}, ref $cls || $cls;

	$self->{pkg} = $self->by_file if !defined $self->{pkg} and defined $self->{file};
	$self->{file} = $self->by_pkg if defined $self->{pkg} and !defined $self->{file};
	
	$self->{scan_html} = $app->file(@{$self->{scan_html}});
	
	$self
}

# возвращает файл пакета
sub by_pkg {
	my ($self) = @_;
	
	my $pkg = $self->{pkg};
	$pkg =~ s!::!/!g;
	
	$app->file($pkg)->ext(".pm")
}

# определяет пакет по файлу
sub by_file {
	my ($self) = @_;
	
	$self->{file} = $app->file($self->file);
	my $ext = $self->file->ext;
	
	my $pkg = do {
		if( $ext eq "xf" ) {
			$app->xf->package_by($self->file->path);
		}
		elsif( $ext !~ /^(pm|pl)\z/ ) {
			msg1 $app->view->{INC}, $self->file->path;
			$app->view->{INC}{$self->file->path}{class}
		}
		else {
			$app->file($INC{$self->file->path})->read =~ /^package\s+([\w:]+)/m && $1;
		}
	};
	
	$pkg or die "пакет по пути " . $app->perl->qq($self->file->path) . " не распознан";
	
	$pkg
}

# проверяет, что модуль есть в %INC
sub loaded {
	my ($self) = @_;
	my $path = $self->file->path;

	return 1 if exists $INC{$path};
	
	my $re = "(^|/)" . quotemeta($path) . "\\z";

	for my $k (keys %INC) {
		return 1 if $k =~ $re;
	}
	
	return 0;
}

# удаляет из %INC
sub remove_from_inc {
	my ($self) = @_;
	
	my $path = $self->file->path;

	print(STDERR "delete INC{$path}\n"), 
		return $self if delete $INC{$path};
	
	my $re = "(^|/)" . quotemeta($path) . "\\z";
	
	for my $k (keys %INC) {
		if($k =~ $re) {
			#if($app->file($INC{$k})->read =~ /package\s+$pkg/)
			#msg(":red", "delete", ":reset", "INC{$k} = $INC{$k}"), 
			delete $INC{$k};
		}
	}
	
	$self
}

# перезагружает пакет
sub reload {
	my ($self) = @_;
	my $service = $self->remove(1);
	if($self->{pkg} eq ref $self) {
		require $self->{file}->{parts}[0];
	}
	$self->load($service);
	
}

# загружает пакет
sub load {
	my ($self, $service) = @_;
	
	my $pkg = $self->pkg;
	my $path = $self->file->{parts}[0];
	
	if($path =~ /\.xf$/) {
		$app->xf->load($path);
		msg ":green", "load", ":reset", $path;
	}
	elsif($path !~ /\.(pl|pm)\z/) {
		$app->view->load($path);
		msg ":green", "load", ":reset", $path;
	}
	else {
		require $path;
		msg ":space green", "require", ":reset", $path, $pkg;
		
		if($pkg =~ /::Row::/) {
			for my $meta (grep { Isa $_, "R::Model::Metafieldset" } values %$app) {
				if($pkg =~ /^$meta->{name}::Row::/) {
					my $cls = $';
					my $name = lcfirst $cls;
					$name =~ s!::(\w)!ucfirst $1!ge;
					my $fieldset = $meta->fieldset($name, $cls);
					$_->add_method for values %{$fieldset->{field}};
					$meta->setup([$fieldset]);
					msg ":green", "fieldset setuped", ":yellow", $name, $cls;
				}
			}
		}
	}

	if($pkg->can("HOTSWAP_LOAD")) {
		$pkg->HOTSWAP_LOAD($self, $service);
	}
	
	if($service && $pkg->can("HOTSWAP_APP_LOAD")) {
		for my $key (keys %$service) {
			$service->{$key}->HOTSWAP_APP_LOAD($key);
		}
	}
	
	return;
}


# удаляет пакет
sub remove {
	my ($self, $from_reload) = @_;

	if(!$self->permission) {
		msg ":red", $self->file->path, ":reset", "i do'nt remove system package!";
		return $self;
	}

	my $pkg = $self->pkg;
	msg ":space blue", "remove", ":reset", $self->file->path, $self->pkg;
	
	if($pkg->can("HOTSWAP_REMOVE")) {
		$pkg->HOTSWAP_REMOVE($self, $from_reload);
	}

	my $ext = $self->file->ext;
	if($ext eq "xf") {
		$app->xf->remove($self->file->path);
	}
	elsif($ext !~ /^(pm|pl)\z/) {
		my $view = delete $app->view->{INC}{$self->file->path};
		die "app->view->{INC}{ ".$self->file->path." } не найден" if !$view;
		@R::View::Perl::Classes = grep { !($_ ~~ $view->{classes}) } @R::View::Perl::Classes;
		%R::View::Perl::Classes = map { ($_ => $R::View::Perl::Classes{$_}) }
			grep { !($_ ~~ $view->{classes}) }
			keys %R::View::Perl::Classes;
		$self->delete_package($_) for @{$view->{classes}};
	}
	
	# удаление модели
	for my $meta (grep { Isa $_, "R::Model::Metafieldset" } values %$app) {
	
		if($pkg =~ /^$meta->{name}::Row::/) {
			my $name = lcfirst $';
			$name =~ s!::(\w)!ucfirst $1!ge;
			my $fieldset = $app->{meta}->exists($name);
			if($fieldset) {
				$fieldset->delete;
				msg ":red", "delete fieldset", ":yellow", $name;
				$self->delete_package($fieldset->{cls_rowset});
			}
			else {
				msg ":red", "fieldset for delete not found:", ":yellow", $name;
			}
		}
	}
	
	# удаление пакета мейка
	my @tasks = values %R::Make::TASK;
	my $path = $self->file->path;
	for my $task (@tasks) {
		delete $R::Make::TASK{$task->{name}} if $task->{file} eq $path;
	}
	
	# удаляет связанный с пакетом объект в $app
	my $service = {};
	for my $k (keys %$app) {
		my $v = $app->{$k};

		if(Isa $v, $pkg) {
			msg ":space red", "app", ":blue", "remove", ":green", $k, ":reset", $pkg;
			$v->HOTSWAP_APP_REMOVE($k, $from_reload, $self) if $v->can("HOTSWAP_APP_REMOVE");
			$self->delete_sub("R::App::$k");
			$service->{$k} = delete $app->{$k};
		}
	}

	# удаляет пакет в %INC
	$self->remove_from_inc;
	
	# удаляет все символы пакета
	$self->delete_package($pkg);
	
	return $service;
}

# удаляет символы в пакете
sub delete_package {
    my ($self, $pkg) = @_;
	
    unless ($pkg =~ /^main::.*::$/) {
        $pkg = "main$pkg"	if	$pkg =~ /^::/;
        $pkg = "main::$pkg"	unless	$pkg =~ /^main::/;
        $pkg .= '::'		unless	$pkg =~ /::$/;
    }
	
    my $leaf_symtab = *{$pkg}{HASH} or return;
	print STDERR "remove $pkg\n";
	
    foreach my $name (keys %$leaf_symtab) {
		next if $name =~ /::$/;	# отбрасываем субпакеты
		
		#print STDERR "delsym $pkg$name\n" if $name eq "TASK";
		
		undef *{$pkg . $name};
		
		# в слоте не глоб! Такое используется для констант
		#msg1 ref $leaf_symtab->{$name}, $name, $leaf_symtab->{$name};
		# if(ref $leaf_symtab->{$name}) {
			# undef *{$pkg . $name};
			# #delete $leaf_symtab->{$name};
		# }
		# else {
			# undef *{$pkg . $name};
		# }
		# given(ref $leaf_symtab->{$name}) {
			# undef *{$pkg . $name} when ""; # валим глоб
			# undef ${$pkg . $name} when "SCALAR";
			# undef ${$pkg . $name} when "SUB";
			# default { die "$name: ".ref($leaf_symtab->{$name})."?" }
		# }
    }
	
	$self
}

# удаляет из таблицы символов только подпрограмму
sub delete_sub {
	my ($self, $sym) = @_;
	
	my %backup;
	for my $slot (qw/FORMAT IO HASH ARRAY SCALAR/) {
		$backup{$slot} = *{$sym}{$slot} if defined *{$sym}{$slot};
	}
	
	undef *{$sym};
	
	for my $slot (keys %backup) {
		*{$sym} = $backup{$slot};
	}
	
	$self
}


# распечатывет содержимое пакета
sub show {
	my ($self, $pkg) = @_;
	
	$pkg //= $self->pkg;
	
	for my $key (keys %{"${pkg}::"}) {
		my $val = \*{"${pkg}::$key"};
		#msg1 ":sep( -> )", ;
		$app->log->info(":sep(: )", $key, map { *{$val}{$_}? (":sep( -> )", $_, ":sep(, )", *{$val}{$_}): () } qw/PACKAGE NAME SCALAR HASH ARRAY CODE Regexp IO GLOB FORMAT/);
	}
	
	$self
}

my $EXTS_COFFEE = "**.<litcoffee,coffee,iced>";

# перекомпилирует coffee, если нужно
sub scan_html {
	my $self = shift;
	
	if(@_) {
		$self->{scan_html} = $app->file(@_);
		return $self;
	}
	
	$self->{scan_html}->find($EXTS_COFFEE)->then(sub {
		if(!$_->ext("js")->exists || $_->mtime > $_->ext("js")->mtime) {
			$app->coffee->compile($_);
			$app->log->info(":space blue", $_->path, ":magenta", "compile");
		}
	});
	
	$self
}

# # данные для сканирования
# my $SCAN = $app->file(values %INC)->watchify;
# my $REQUIRE = \&CORE::require;
# *CORE::GLOBAL::require = sub { 
	# my ($path) = @_;
	# my $inc = $INC{$path};
	# my $exists = $SCAN->{watch}->{$inc};
	# my $res = $REQUIRE->(@_);
	# $SCAN->{watch}->{$inc} = $app->file($inc)->mtime if !$exists;
	# $res
# };

# инициализация сканирования
sub startscan {
	my $self = shift;
	
	$self->{scan} = $app->file->new;
	@{$self->{scan}->{parts}} = $self->longpaths;
	$self->{scan}->watch;
	@{$self->{scan}->{parts}} = ();

	msg1 ":red on_yellow", "startscan"; #, $app->raise->trace;
	
	$self
}

# сканирует. При изменении файла - перезагружает. При удалении - удаляет
sub scan {
	my ($self) = @_;
	
	$self->scan_html;
	
	msg1 ":black on_cyan", "scan";
	
	# $self->startscan if !$self->{scan};
	
	@{$self->{scan}->{parts}} = $self->longpaths;
	# for my $path (@{$self->{scan}->{parts}}) {
		# msg1 $path, $self->{scan}->{watch}->{$path}, $app->file($path)->mtime if $path =~ m!\.xf$!;
		# #$self->{scan}->{watch}->{$path} //= $MTIME{$path} if exists $MTIME{$path};
	# }
	my $watch = $self->{scan}->watch;
	@{$self->{scan}->{parts}} = ();
	
	#msg1 keys(%{$self->{scan}{watch}})+0, grep { m!c_scenario! } keys %{$self->{scan}{watch}};
	#msg1 ":sep( -> )", "created", $watch->created->length? [$watch->created->parts]: ();
	#msg1 ":sep( -> )", "changed", $watch->changed->length? [$watch->changed->parts]: ();
	#msg1 ":sep( -> )", "deleted", $watch->deleted->length? [$watch->deleted->parts]: ();

	# новый файл, зачем его перезагружать?
	# # # возможно файл изменился - перезагружаем
	# for my $f ($watch->created->slices) {
		# $f = _from_inc($f) or next;
		# $app->log->info(":space blue", $f->path, ":magenta", "reload, created and maybe changed");
		# #$self->new(file => $f)->reload;
	# }

	
	# файл изменился - перезагружаем
	for my $f ($watch->changed->slices) {
		my @p = $f->parts;
		$f = _from_inc($f) or do { msg1 "no _from_inc!!!", @p; next };
		$app->log->info(":space blue", $f->path, ":magenta", "reload");
		$self->new(file => $f)->reload;
	}

	# удалился? удаляем
	for my $f ($watch->deleted->slices) {
		my @p = $f->parts;
		$f = _from_inc($f) or do { msg1 "no _from_inc!!!", @p; next };
		$app->log->info(":space blue", $f->path, ":magenta", "removed");
		$self->new(file => $f)->remove;
	}
		
	$self
}

# цикл бесконечного сканирования
sub scaninf {
	my $self = shift;
	
	$self->startscan(@_);
	
	for(;;) {
		sleep $self->{sleep} // 1;
		$self->scan;
	}
	
	$self
}

#@@category Пути из %INC

# обратный
sub _from_inc {
	my ($path) = @_;
	$path = $app->file($path)->path;
	
	for my $key (keys %INC) {
		my $val = $INC{$key};
		return $app->file($key) if $val eq $path;
	}
	#die "value `$path` not in %INC";
	return 0;
}

# определяет пакет который можно перезагружать
sub permission {
	my ($self, $longpath) = @_;

	$longpath = $INC{$self->file->path} if @_==1;
	
	$longpath !~ m!^/! 
		or ($app->framework->path eq substr $longpath, 0, length $app->framework->path)
		or $app->project->path eq substr $longpath, 0, length $app->project->path
}

# возвращает длинные пути из %INC
sub longpaths {
	my ($self) = @_;
	%INC = grep2 { defined $b } %INC;
	$app->project->files("view")->find("**.html")->parts, grep { $self->permission($_) } values %INC
}



1;