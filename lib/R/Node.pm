package R::Node;
# выполняет js посредством node-сервера

use common::sense;
use R::App;

has qw/ps/;

# конструктор
sub new {
	my $cls = shift;
	my $self = bless {
		name => "node",
		port => 9132,
		@_
	}, ref $cls || $cls;
	
	$self->{ps} //= $app->process($self->{name} => $self->node);
	
	$self
}

# инициализирует процесс ноды
sub init {
	my ($self) = @_;
	$self->{ps}->waitrun if !$self->{ps}->exists;
	die "node не запустилась" if !$self->{ps}->exists;
	$self
}

# подключает файл
sub require {
	my ($self, $path) = @_;
	
	# поднимаем ноду, если нужно
	$self->init;
	
	# делаем к ноде запрос
	my $xpath = $app->file($path)->cygwin->path;
	my $res = $app->url(":$self->{port}")->param(path => $xpath)->res;
	die "node error! " . $res->code .": ". $res->content if $res->code != 200;
	
	# получаем json
	my $content = $res->decoded_content;
	$app->json->from($content)
}

# вычислить код js
sub eval {
	my ($self, $code) = @_;
	
	# поднимаем ноду, если нужно
	$self->init;
	
	# делаем к ноде запрос
	#msg1 ":blue", "node-request", length $code;
	my $res = $app->url(":$self->{port}")->format("json")->post(code => $code)->res;
	die "node error! " . $res->code .": ". $res->decoded_content if $res->code != 200;
	
	# получаем json
	my $content = $res->decoded_content;
	my $res = $app->json->from($content);
	#msg1 ":yellow on_blue", "node-response", ":reset", $res;
	
	if(exists $res->{error}) {
		my $e = $res->{error} . ($res->{stack}? "\n" . $res->{stack}: "");
		#msg1 ":red", $e;
		$e =~ s!\\!/!g;
		$e =~ s!(\s)(\w):/!$1/cygdrive/$2/!g;
		die R::colored($e, "red");
	}
	#die "неверный ответ: $content" if !exists $res->{res};
	$res->{res}
}

# возвращает код для процесса node
sub node {
	my ($self) = @_;
	"#!node var __port__ = $self->{port}; " . $app->framework->file("share/dev/eval.js")->read
}

# деструктор
sub DESTROY {
	my ($self) = @_;
	$self->{ps}->kill("KILL") if $self->{ps} && $self->{ps}->exists;
}

1;