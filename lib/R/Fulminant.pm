package R::Fulminant;
# база для создания серверов
#
# make - создаёт подключение
# accept - бесконечный цикл обработки запросов, вызывающий ritter
# impulse - обрабатывает запрос (считывает request и формирует response)
# ritter - обработчик запроса. Получает request в виде объекта и выдаёт ответ в виде строки или какой-то хуманной структуры
# shutdown - останавливает бесконечный цикл сервера
# close - закрывает сокет
# run - запускает указанное число волокон

use common::sense;
use R::App;
use R::Coro;

use Coro::Socket qw//;
use Scalar::Util qw//;
use Symbol qw/ungensym/;

has qw/name port log max_queriers max_websockets busy_websockets busy_queriers keep_alive/;

# конструктор
# @param int port
sub new {
	my $cls = shift;
	my $self = bless {@_}, ref $cls || $cls;

	$self->{name} //= $app->project->name;
	$self->{port} //= 9000;
	$self->{request} //= $app->rsgiRequest;
	$self->{response} //= $app->rsgiResponse;
	$self->{timeout_recv} //= 1;	# макс.время получения запроса
	$self->{timeout} //= 30;		# макс.время за которое выполняется запрос (без считывания и отправки)
	$self->{timeout_send} //= 1;	# макс.время отправки запроса
	$self->{timeout_websocket} //= 60;	# если в течение минуты не было движения по сокету, то отправляем ping
	$self->{timeout_websocket_pong} //= 3;	# если в течение 3 сек не пришёл pong, то закрываем веб-сокет
	$self->{ritter} //= sub {
		"Hi! I am $self->{name}";
	};
	$self->{middleware} //= sub {
		my ($request, $response) = @_;
		$response
	};
	$self->{fibers} = undef;
	$self->{max_queriers} = 1024;
	$self->{max_websockets} = 1024;
	$self->{busy_websockets} = 0;
	$self->{busy_queriers} = 0;
	
	$self->{keep_alive} //= 0;			# разрешить keep-alive
	
	$self->{gzip} //= [qw/ gzip /];		# устанавливает разрешённые архиваторы и их порядок. TODO: br
	$self->{gzip_min_length} //= 1024;
	$self->{gzip_comp_level} //= 6;
	$self->{file_modified} //= 1;		# использовать If-Modified-Since на файлах при отправке
	$self->{max_age} //= 0;				# время в секундах для кеширования статики
	
	die "некорректный request" if !$self->{request} || !$self->{request}->can("new");
	die "некорректный response" if !$self->{response} || !$self->{response}->can("new");
	
	$self
}

# загрузка сервиса в app
sub HOTSWAP_APP_LOAD {
	my ($self, $key) = @_;

	my $new = $app->$key;
	for my $k (qw/sd real_sd fibers/) {
		# delete для того, чтобы не сработал дестройер на new и не сделал shutdown
		$new->{$k} = delete $self->{$k};	
	}
	
	%$self = %$new;
	%$new = ();
	$app->{$key} = $self;
}

# деструктор
sub DESTROY {
	my ($self) = @_;
	$self->shutdown if $self->{sd};
}

# открывает сокет
sub open {
	my ($self) = @_;
	
	die "open без параметров" if @_>1;
	
	my $_port = $self->{port};
	
	die "не указан порт" if !defined $_port;
	
	my $sd = $app->socket->open($_port);
	
	$self->{real_sd} = $sd;
	$self->{sd} = Coro::Socket->new_from_fh($sd);
	
	if(my $log = $self->{log}) {
		ref($self) =~ /(\w+)$/;
		$app->log->info(":empty green", "fulminant", ":black bold", "/", lcfirst($1), ":magenta", "/", $self->name, ":reset", ":", ":red", $_port );
	}
	
	$self
}

# открывает сокет из файлового номера
sub from_fileno {
	my ($self, $fileno) = @_;
	$self->{sd} = Coro::Socket->new_from_fd($fileno, "r");
	$self
}

# бесконечный цикл ожидания и выполнения запросов
sub accept {
	my ($self) = @_;
	
	while( $self->{sd} ) {
		my ($ns, $paddr) = $self->{sd}->accept;
		
		die "accept не сработал: $!" if !$ns;

		#msg ":white on_cyan", "accept ns", fileno($ns), $Coro::current->{desc};

		# гарантированно закрывает сокет, когда происходит keep-alive по таймауту
		my $close_ns = guard {
			$self->{busy_queriers}--;
			#msg ":white on_magenta", "close ns", ($ns? fileno($ns): $ns), $Coro::current->{desc};
			return if !$ns;
			$ns->shutdown(2);
			$ns->close;
			undef $ns;
		};
		$self->{busy_queriers}++;
		
		# если подключений больше чем лимит
		if($self->{busy_queriers} >= $self->{max_queriers}) {
			local $Coro::current->{request} = my $request = $self->{request}->new(ns => $ns, paddr => $paddr, fulminant => $self);
			$self->{response}->from([503])->send;
			Coro::cede();
			next;
		}
	
		#msg1 ":blue", "accept", ":reset", ":magenta", $self->{port};

		#$ns->timeout(0.2);
	
		# порождаем волокно
		my $name = "$self->{name}-querier-${\fileno($ns)}";
		$app->coro($name => sub {
			
			Coro::cede() while $self->impulse($ns, $paddr);
			
			undef $close_ns;
		})->ready;
		
	}
	
	$self
}

# парсит запрос и сендит ответ
sub impulse {
	my ($self, $ns, $paddr) = @_;

	my $telmetry_impulse_defer = $app->telemetry->defer("fulminant/impulse");

	# формирование запроса
	local $Coro::current->{request} = my $request = $self->{request}->new(ns => $ns, paddr => $paddr, fulminant => $self);
	
	local $Coro::current->{response} = my $response;

	# любое исключение, exit и core dumped успеют выбросить 500
	my $response_guard = R::guard {
		msg1(":red", "500!!!"), $self->{response}->new(res => [500])->send($request) if !$request->was_sending && !$request->disconnected;
	};
	
	my $keep_alive = 0;
	eval {
		# считывание запроса
		my $timer_recv = $self->set_timer($self->{timeout_recv}, [408, [], "Recive Timeout $self->{timeout_recv}s"]);
		
		$request->recv;
		
		undef $timer_recv;
		$request->was_sending(1), return 0 if $request->disconnected;

		# исполнение запроса

		# веб-сокет сможет отключить таймер
		$request->{timer} = $self->set_timer($self->{timeout}, [504, [], "Gateway Timeout $self->{timeout}s"]);
		my $gateway_timer = guard { undef $request->{timer} };

		if($request->bad) {
			$response = [400];
		}
		else {
			# мы должны обработать промежуточной подпрограммой
			$response = eval { $self->ritter($request) };
			# сохраняем стек-трейс
			$response = $self->{response}->fail($@, $request) if $@;
		}

		# обработка промежуточной подпрограммой
		eval {
			$Coro::current->{response} = $response = $self->{response}->from($response);
			$response = $self->middleware($request, $response);
			$response = $self->{response}->from($response);
		};
		$response = $self->{response}->fail($@, $request) if $@;

		undef $gateway_timer;

		# отправка ответа
		if($request->was_sending) {
			if($response->status != 200) {
				msg ":sep( -> )", "Ответ уже отправлен, однако фиксирую ответ со статусом ", $response->status;
				msg ":sep( -> )", "Запрос ", $request->via, $request->location;
				msg ":sep( -> )", "Ответ ", $response;
			}
		} else {
			my $timer_send = $self->set_timer($self->{timeout_send}, [522, [], "Send Timeout $self->{timeout_send}s"]);

			$keep_alive = $response->send($request, $self->{timeout_recv}>1? $self->{timeout_recv}-1: $self->{timeout_recv});
		}
	};
	# обрабытываем исключения таймеров
	$self->{response}->fail($@, $request)->send($request) if $@;
	
	return $keep_alive;
}

# устанавливает таймер
sub set_timer {
	my ($self, $timeout, $response) = @_;
	
	return if $timeout == 0;
	
	my $me = $Coro::current;
	AE::timer $timeout, $timeout, sub {
		msg1 ":inline", "timeout throw ", $response, " -> $me->{desc}";
		$me->throw($response);
	};
}

# обрабатывает запрос
sub ritter {
	my $self = shift;
	$self->{ritter}->(@_);
}

# может изменить ответ
sub middleware {
	my $self = shift;
	$self->{middleware}->(@_);
}

# запускает в отдельном волокне ожидание и обработку подключений - accept
sub run {
	my ($self) = @_;

	my $name = $self->{name};
	
	$self->open;

	
	$self->{fiber} = $app->coro("$name-querier" => sub {
		$self->accept;
	})->ready;
	
	$self
}

# # останавливает
# sub halt {
	# my ($self) = @_;
	# $self->{fibers}->mortal->exme->kill, Coro::cede() if $self->{fibers};
	# $self->shutdown;
# }

# своеобразный деструктор
sub sv {
	my ($self) = @_;
	guard { $self && $self->shutdown };
}

# возвращает волокна запросов
sub get_queriers {
	my ($self) = @_;
	my $re = "^" . quotemeta($self->name) . "-querier-(\d+)\z";
	$app->coro->psapp->grep(sub { $_ and $_->{desc} =~ $re  })
}

# разрушает главный сокет
sub shutdown {
	my ($self) = @_;
	
	#msg1 "shutdown-sd", $self->{name}, $Coro::current->{desc};
	#undef $self->{sem};
	
	$self->{sd}->shutdown(2) if $self->{sd} && tied(*${$self->{sd}});

	$self->get_queriers->add($self->{fiber})->mortal->exme->catch(sub {
		die if $@ ~~ [503];
	})->throw([503])->wait;
	
	$self->close;
}

# закрывает сокет
sub close {
	my ($self) = @_;
	
	#msg1 "close-sd";
	$self->{sd}->close if $self->{sd};

	undef $self->{sd};
	
	ungensym $self->{real_sd} if $self->{real_sd};
	undef $self->{real_sd};
	undef $self->{ritter};
	undef $self->{middleware};
	
	$self
}


1;