package R::Connect;
# подключение к базе и простенькие функции для запросов

use common::sense;
use R::App;
use R::Coro;

use DBI;
use POSIX qw/strftime/;
use Time::HiRes qw//;


has qw/user password basename charset collate log_info host port/;

# конструктор
sub new {
	my $cls = shift;
	bless {
		host => "127.0.0.1",
		port => 3306,
		@_
	}, ref $cls || $cls;
}


sub ON_UPDATE {"RESTRICT"}
sub ON_DELETE {"RESTRICT"}

# зарезервированные слова sql
my $SQL_WORD = $app->perl->setref(qw/ACCESSIBLE	 ADD	 ALL	 ALTER	 ANALYZE	 AND	 AS	 ASC	 ASENSITIVE	 BEFORE	 BETWEEN	 BIGINT	 BINARY	 BLOB	 BOTH	 BY	 CALL	 CASCADE	 CASE	 CHANGE	 CHAR	 CHARACTER	 CHECK	 COLLATE	 COLUMN	 CONDITION	 CONSTRAINT	 CONTINUE	 CONVERT	 CREATE	 CROSS	 CURRENT_DATE	 CURRENT_TIME	 CURRENT_TIMESTAMP	 CURRENT_USER	 CURSOR	 DATABASE	 DATABASES	 DAY_HOUR	 DAY_MICROSECOND	 DAY_MINUTE	 DAY_SECOND	 DEC	 DECIMAL	 DECLARE	 DEFAULT	 DELAYED	 DELETE	 DESC	 DESCRIBE	 DETERMINISTIC	 DISTINCT	 DISTINCTROW	 DIV	 DOUBLE	 DROP	 DUAL	 EACH	 ELSE	 ELSEIF	 ENCLOSED ESCAPED	 EXISTS	 EXIT	 EXPLAIN	 FALSE	 FETCH	 FLOAT	 FLOAT4	 FLOAT8	 FOR	 FORCE	 FOREIGN	 FROM FULLTEXT	 GENERAL GRANT	 GROUP	 HAVING	 HIGH_PRIORITY	 HOUR_MICROSECOND	 HOUR_MINUTE	 HOUR_SECOND	 IF	 IGNORE	 IGNORE_SERVER_IDS IN	 INDEX	 INFILE	 INNER	 INOUT	 INSENSITIVE	 INSERT	 INT	 INT1	 INT2	 INT3	 INT4	 INT8	 INTEGER	 INTERVAL	 INTO	 IS	 ITERATE	 JOIN	 KEY	 KEYS	 KILL	 LEADING	 LEAVE	 LEFT	 LIKE	 LIMIT	 LINEAR	 LINES	 LOAD	 LOCALTIME	 LOCALTIMESTAMP	 LOCK	 LONG	 LONGBLOB	 LONGTEXT	 LOOP	 LOW_PRIORITY	 MASTER_HEARTBEAT_PERIOD MASTER_SSL_VERIFY_SERVER_CERT	 MATCH	 MAXVALUE MEDIUMBLOB	 MEDIUMINT	 MEDIUMTEXT	 MIDDLEINT	 MINUTE_MICROSECOND	 MINUTE_SECOND	 MOD	 MODIFIES	 NATURAL	 NOT	 NO_WRITE_TO_BINLOG	 NULL	 NUMERIC	 ON	 OPTIMIZE	 OPTION	 OPTIONALLY	 OR	 ORDER	 OUT	 OUTER	 OUTFILE	 PARTITION PRECISION	 PRIMARY	 PROCEDURE	 PURGE	 RANGE	 READ	 READS	 READ_WRITE	 REAL REFERENCES	 REGEXP	 RELEASE	 RENAME	 REPEAT	 REPLACE	 REQUIRE	 RESIGNAL RESTRICT	 RETURN	 REVOKE	 RIGHT	 RLIKE	 SCHEMA	 SCHEMAS	 SECOND_MICROSECOND	 SELECT	 SENSITIVE	 SEPARATOR	 SET	 SHOW	 SIGNAL SLOW SMALLINT	 SPATIAL	 SPECIFIC	 SQL	 SQLEXCEPTION	 SQLSTATE	 SQLWARNING	 SQL_BIG_RESULT	 SQL_CALC_FOUND_ROWS	 SQL_SMALL_RESULT	 SSL	 STARTING	 STRAIGHT_JOIN	 TABLE	 TERMINATED	 THEN	 TINYBLOB	 TINYINT	 TINYTEXT	 TO	 TRAILING	 TRIGGER	 TRUE	 UNDO	 UNION	 UNIQUE	 UNLOCK	 UNSIGNED	 UPDATE	 USAGE	 USE	 USING	 UTC_DATE	 UTC_TIME	 UTC_TIMESTAMP	 VALUES	 VARBINARY	 VARCHAR	 VARCHARACTER	 VARYING	 WHEN	 WHERE	 WHILE	 WITH	 WRITE	 XOR	 YEAR_MONTH	 ZEROFILL
GENERATED	GET	IO_AFTER_GTIDS	IO_BEFORE_GTIDS MASTER_BIND OPTIMIZER_COSTS PARSE_GCOL_EXPR STORED VIRTUAL/);

sub SQL_WORD {
	my ($self, $word) = @_;
	exists $SQL_WORD->{uc $word}
}

sub AUTO_INCREMENT { "AUTO_INCREMENT" }
sub COMMENT { 
	my ($self, $comment, $type) = @_;
	my $sep = $type eq "tab"? "=": " ";
	join $sep, "COMMENT", $self->quote($comment)
}

# типы столбцов для alter table
my $COLUMN_TYPE = {
	"tinyint(4)" => "tinyint",
	"tinyint(3) unsigned" => "tinyint unsigned",
	"smallint(5) unsigned" => "smallint unsigned",
	"smallint(6)" => "smallint",
	"int(11)" => "int",
	"int(10) unsigned" => "int unsigned",
	"bigint(21)" => "bigint",
	"bigint(20) unsigned" => "bigint unsigned",
	"string" => "varchar(255)",
};

sub COLUMN_TYPE {
	my ($self, $type) = @_;
	$COLUMN_TYPE->{$type} // $type
}

# формирует DSN
sub DSN {
	my ($self) = @_;
	
	#mysql_compression =>
	$self->{sock}? "DBI:mysql:database=$self->{basename};host=localhost;mysql_socket=" . ($self->{sock} eq 1? "/tmp/mysql.sock": $self->{sock}):
		"dbi:mysql:$self->{basename}:$self->{host}:$self->{port}"
}

# дополнительные опции для DBD
sub default_options {
	return mysql_enable_utf8 => 1;
		#mysql_auto_reconnect => 1;
}

# sub new {
	# my $cls = shift;
	# my %ini = @_? @_: ref $cls? (%$cls, database=>$cls->{basename}): ();

	# bless {
		# user => delete($ini{user}),
		# password => delete($ini{password}),
		# basename => delete($ini{database}),
		# charset => delete($ini{charset}),
		# collate => delete($ini{collate}),
		# log => delete($ini{'log'}),
		# log_prefix => '',
		# sql_save => undef,			# собирает sql, вместо выполнения, если []
		# options => {%ini},
	# }, ref $cls || $cls;
# }


# коннект
sub connect {
	my ($self) = @_;

	my $dbh = DBI->connect($self->DSN, $self->user, $self->password,
		{RaiseError => 1, PrintError => 1, PrintWarn => 1, $self->default_options, 
			%{$self->{options}}});
			
	$self->_dbh($dbh);
	
	$self->after_connect;
	
	$self
}

# сразу после подключения
sub after_connect {
	my ($self) = @_;

	# режимы: https://mariadb.com/kb/en/library/sql-mode/
	$self->{sql_mode} //= [qw/TRADITIONAL NO_AUTO_VALUE_ON_ZERO/];
	
	$self->do("SET sql_mode=" . $self->quote( join ",", @{$self->{sql_mode}} ));
	
	my $charset = $self->{charset};
	if($charset) {
		my $collate = $self->{collate};

		$self->do("SET NAMES " . $self->quote($charset) . ($collate? " COLLATE " . $self->quote($collate): ""));
	}
	
	$self
}

# переконнекчивается, если связь утрачена
sub reconnect {
	my ($self) = @_;
	return $self if $self->_dbh->ping;
	$self->_dbh->disconnect;
	$self->connect;
}

# переключиться на БД
sub use {
	my ($self, $basename) = @_;
	$self->do($self->show_use($basename));
	$self->clean;
	$self->basename($basename);
	$self
}

# sql use
sub show_use {
	my ($self, $basename) = @_;
	"USE " . $self->word($basename)
}

# # удаляет базу
# sub create_database {
	# my ($self, $base) = @_;
	# "CREATE DATABASE ". $c->word($self->{database}) . $self->sql;
	# $self->do($self->show_create_database($base // $self->base($self->basename)));
# }

# # удаляет базу
# sub drop_database {
	# my ($self, $base) = @_;
	# $self->do($self->show_drop_database($base // $self->base($self->basename)));
# }


#@@category информация о таблицах

our %BASE_INFO;
our %INFO;
our %TAB_INFO;
our %INDEX_INFO;
our %FK_INFO;
our %BK_INFO;

# кеширует инф. о базах
sub base_info {
	my ($self) = @_;
	$BASE_INFO{ref $self} //= $self->get_base_info;
}

# возвращает инф. о базе
sub get_base_info {
	my ($self) = @_;
	my $info = $self->nolog(sub { 
		$self->query_all("information_schema.schemata", [qw/
			schema_name
			default_character_set_name
			default_collation_name
		/]);
	});
	
	my $ret = {};
	for my $row (@$info) {
		$ret->{$row->{schema_name}} = {
			name => $row->{schema_name},
			charset => join(":", $row->{default_character_set_name}, $row->{default_collation_name}),
		};
	}
	$ret
}

# кеширует инф. о таблицах
sub tab_info {
	my ($self) = @_;
	$TAB_INFO{ref $self}{$self->{basename}} //= $self->get_tab_info;
}

# возвращает информацию о таблицах
sub get_tab_info {
	my ($self) = @_;
	
	my $sql = "SELECT 
		T.table_name as name, 
		T.engine, 
		concat(C.character_set_name, ':', C.collation_name) as charset,
		T.table_comment as remark, 
		T.create_options as options, 
		T.table_type as type,
		T.auto_increment as autoincrement,
		T.create_time as ctime,
		T.update_time as mtime,
		T.check_time as atime
	FROM information_schema.`tables` T 
	INNER JOIN information_schema.`collation_character_set_applicability` C
		ON C.collation_name = T.table_collation
	WHERE T.table_schema=".$self->quote($self->basename);
	my $rows = $self->nolog(sub { $self->query_all($sql); });
	
	my $info = {};
	for my $row (@$rows) {	# создаём info
		$info->{$row->{name}} = $row;
	}
	return $info;
}


# кеширует инф. о столбцах таблиц
sub info {
	my ($self) = @_;
	$INFO{ref $self}{$self->{basename}} //= $self->get_info;
}

# возвращает информацию о столбцах таблиц
sub get_info {
	my ($self) = @_;
	my $sql = "select table_name, column_name, data_type, column_type, column_default, is_nullable, character_maximum_length, extra, column_key, ordinal_position, column_comment, character_set_name, collation_name
		from information_schema.columns
		where table_schema=".$self->quote($self->basename);
	my $rows = $self->nolog(sub { $self->query_all($sql); });
	my $info = {};
	
	for my $row (@$rows) {	# создаём info
		
		my $extra = $row->{extra};
		my $autoincrement = $extra =~ s/\bAUTO_INCREMENT\b//i;
		
		$info->{$row->{table_name}}{$row->{column_name}} = {
			name => $row->{column_name},
			type => $self->COLUMN_TYPE($row->{column_type}),
			null => $row->{is_nullable} eq "YES",
			default => $row->{column_default},
			pk => scalar $row->{column_key} =~ /PRI/i,
			autoincrement => $autoincrement,
			remark => $row->{column_comment},
			charset => join(":", $row->{character_set_name}, $row->{collation_name}),
			pos => $row->{ordinal_position},
			key => $row->{column_key},
			extra => $extra,
			maxlen => $row->{character_maximum_length},
		};

	}

	return $info;
}

# кеширует информацию о ключах таблиц
sub index_info {
	my ($self) = @_;
	$INDEX_INFO{ref $self}{$self->{basename}} //= $self->get_index_info
}

# без кеширования
sub get_index_info {
	my ($self) = @_;
	my %rename = (
		Table => "tab",
		Non_unique => "non_uniq",
		Key_name => "name",
		Seq_in_index => "pos",
		Column_name => "col",
		Comment => "remark",
		Index_comment => "index_remark",
		Null => "null",
		Index_type => "type",
		Packed => "packed",
		Cardinality => "cardinality",
		Sub_part => "part",
		Collation => "charset",
	);
	
	my $tab_info = $self->tab_info;
	my $fk_info = $self->fk_info;
	my $info = {};
	while(my ($tab, $in) = each %$tab_info) {
		my $sql = "SHOW KEYS FROM " . $self->word($tab);
		my $rows = $self->nolog(sub { $self->query_all($sql) });

		for my $row (@$rows) {
			next if exists $fk_info->{$tab}{$row->{Key_name}};

			%$row = map {(($rename{$_} // $_) => $row->{$_})} keys %$row;
			
			$row->{unique} = !delete $row->{non_uniq};
			
			my $idx = $info->{$row->{tab}}{$row->{name}} //= [];
			push @$idx, $row;
		}
	}
	return $info;
}

# кеширует информацию о внешних ключах таблиц
sub fk_info {
	my ($self) = @_;
	my $info = $FK_INFO{ref $self}{$self->{basename}} //= $self->get_fk_info;
	
	$BK_INFO{ref $self}{$self->{basename}} //= do {
		my $bk = {};
		for my $row (values %$info) {
			$bk->{$row->{ref_tab}}{$row->{name}} = $row;
		}
		$bk
	};
	
	$info
}

# возвращает от каких ключей зависит какая таблица
sub fk_info_backward {
	my ($self) = @_;
	$self->fk_info;
	$BK_INFO{ref $self}{$self->{basename}}
}

# возвращает от каких ключей зависит какая таблица
sub get_fk_info_backward {
	my ($self) = @_;
	$self->get_fk_info;
	$BK_INFO{ref $self}{$self->{basename}}
}

# возвращает информацию о внешних ключах таблиц
sub get_fk_info {
	my ($self) = @_;
	my $sql = "
	SELECT
		KU.table_name as tab,
		column_name as col,
		KU.constraint_name as name,
		KU.referenced_table_name as ref_tab,
		referenced_column_name as ref_col,
		ordinal_position as pos,
		position_in_unique_constraint as uniq_pos,
		delete_rule as on_delete,
		update_rule as on_update
	FROM information_schema.KEY_COLUMN_USAGE AS KU 
		INNER JOIN information_schema.REFERENTIAL_CONSTRAINTS AS RC ON RC.CONSTRAINT_NAME = KU.CONSTRAINT_NAME
	WHERE TABLE_SCHEMA=" . $self->quote($self->basename) . "
		AND referenced_column_name IS not null
	GROUP BY name, col, pos
	ORDER BY ordinal_position, position_in_unique_constraint
	";
	my $rows = $self->nolog(sub { $self->query_all($sql); });
	my $info = {};
	for my $row (@$rows) {
		if(my $inf = $info->{$row->{tab}}{$row->{name}}) {
			push @{$inf->{cols}}, $row->{col};
			push @{$inf->{refs}}, $row->{ref_col};
		} else {
			$row->{refs} = [delete $row->{ref_col}];
			$row->{cols} = [delete $row->{col}];
			$info->{$row->{tab}}{$row->{name}} = $row;
		}
	}

	return $info;
}

# очищает кеши
sub clean {
	my ($self) = @_;
	
	delete $BASE_INFO{ref $self};
	delete $INFO{ref $self}{$self->{basename}};
	delete $TAB_INFO{ref $self}{$self->{basename}};
	delete $INDEX_INFO{ref $self}{$self->{basename}};
	delete $FK_INFO{ref $self}{$self->{basename}};
	delete $BK_INFO{ref $self}{$self->{basename}};
	
	$self
}


#@@category хелперы

# разбивает чарсет на чарсет и коллатион: 
sub split_charset {
	my ($self, $charset) = @_;
	my $collation;
	if($charset =~ /:/) { $charset = $`; $collation = $' }
	elsif($charset =~ /_/) { $collation = $charset; $charset = $` }
	else { die "пришёл странный charset: $charset" }
	return $charset, $collation;
}

# возвращает текущее время в формате базы
sub now {
	strftime("%F %T", @_>1? localtime @_[1..$#_]: localtime)
}

# переводит в camelcase
sub uc {
	my ($self, $word) = @_;
	local ($`, $', $&, $1);
	$word =~ s!_([A-Z])!ucfirst $1!ige;
	ucfirst $word
}

# переводит в decamelcase
sub lc {
	my ($self, $word) = @_;
	local ($`, $', $&);
	$word = lcfirst $word;
	$word =~ s![A-Z]!"_" . lcfirst $&!ge;
	$word
}

# сокращение
sub up {
	my ($self, $col) = @_;
	$self->word($self->uc($col))
}

# сокращение
sub down {
	my ($self, $col) = @_;
	$self->word($self->lc($col))
}

# оборачивает в `` если нужно
sub word {
	my ($self, $col) = @_;
	$self->SQL_WORD($col) || $col !~ /^[a-z_][a-z0-9_]*(?:\.[a-z_][a-z0-9_]*)?$/i && $col !~ /^\(/? $self->word_quote($col): $col;
}

# оборачивает таблицу
sub tab_word {
	my ($self, $tab) = @_;
	$self->SQL_WORD($tab) || $tab !~ /^[a-z_][a-z0-9_]*(?:\.[a-z_][a-z0-9_]*)?$/i? $self->tab_word_quote($tab): $tab;
	$self
}

# кавычки для столбца
sub word_quote {
	my ($self, $col) = @_;
	"`$col`"
}

# кавычки для таблицы
sub tab_word_quote {
	my ($self, $tab) = @_;
	"`$tab`"
}

# формирует колумн
sub SQL_COL {
	my ($self, $col, $as) = @_;
	join "", ($as? ($self->word($as), "."): ()), $self->word($col);
}

# возвращает алиас таблицы. То есть бывает, что таблица содержит несколько ссылок на другую таблицу, тогда они не могут называться все как таблица на которую ссылаются. И так: author_id, soauthor_id - alias_tab = {soauthor_id => author_id}
sub TAB_ref {
	my ($self, $col) = @_;
	$col =~ s/_id$//;
	$self->{alias_tab}{$col} // $col;
}

# квотирование
sub quote {
	my ($self, $s) = @_;
	!defined($s)? "NULL":
	ref($s)? do {
		use Scalar::Util qw/blessed/;
		!blessed($s)? do {
			ref $s eq "ARRAY"? do {
				my %x = map { ($_=>$_) } @$s;
				join "", "(", join(",", map { $self->quote($x{$_}) } sort keys %x), ")"
			}:
			ref $s eq "SCALAR"? $$s:
			die("не могу преобразовать ссылку в quote: `$s`");
		}:
		$s->isa("R::Model::Row")? $s->id:	# для ORM
		$s->can("toCol")? do {
			$s = $s->toCol;		# для типов базы данных
			$s =~ /^-?(?:[1-9]\d*|0)(?:\.\d+)?$/ && $s!=0? $s: $self->string_quote($s)
		}:
		die("не могу преобразовать ссылку в quote: `$s`");
	}:
	$s =~ /^-?(?:[1-9]\d*|0)(?:\.\d+)?$/ && $s!=0? $s: $self->string_quote($s)
}

# в строку
sub string_quote {
	my ($self, $s) = @_;
	$app->magnitudeLiteral->to($s, "'")
}

# формирует ключ=значение через запятую, для UPDATE SET или REPLACE SET
sub DO_SET {
	my ($self, $p, $as) = @_;
	
	return $p if !ref $p;
	
	my @set = ();
	$p = {@$p} if ref $p eq "ARRAY";
	while(my($a, $b) = each %$p) {
		push @set, join("",
			$self->SQL_COL($a, $as), "=", (
				ref $b eq 'HASH'? scalar($self->replace($self->TAB_ref($a), $b)):
				ref $b eq "SCALAR"? $self->SQL_COL($$b, $as):
				ref $b eq "REF"? $$$b:
				$self->quote($b)
			)
		);
	}
	return wantarray? @set: join(", ", @set);
}

# формирует where
sub DO_WHERE {
	my ($self, $where, $as) = @_;
	
	if(ref $where) {
		my @SET;
		$where = [%$where] if ref $where eq "HASH";
		for(my $i = 0; $i<@$where; $i+=2) {
			my($a, $b) = ($where->[$i], $where->[$i+1]);
			my $op = $a=~s/__ne$//? "<>": $a=~s/__lt$//? "<": $a=~s/__gt$//? ">": $a=~s/__le$//? "<=": $a=~s/__ge$//? ">=": $a=~s/__like$//? " like ": $a=~s/__unlike$//? " not like ": $a=~s/__isnt$//? " is not ": $a=~s/__between//? " BETWEEN ": !defined($b)? " is ": "=";
			push @SET, join("", 
				$self->SQL_COL($a, $as), $op, (
					!defined($b)? "null":
					ref $b eq "ARRAY"? (
						$op eq " BETWEEN "? $self->quote($b->[0])." AND ".$self->quote($b->[1]): do {
							$op = " IN " if $op eq '=';
							join "", "(", join(", ", map { $self->quote($_) } @$b), ")" 
						}
					):
					ref $b eq 'HASH'? scalar($self->replace($self->TAB_ref($a), $b)):
					ref $b eq "SCALAR"? (ref $$b eq "SCALAR"? $$$b: $self->SQL_COL($$b, $as)):
					$self->quote($b)
				)
			);
		}
		return join(" AND ", @SET)." AND ".$where->[$#$where] if @$where % 2;
		return join(" AND ", @SET);
	}
	return $where =~ /^\d+$/? join("", $self->SQL_COL('id', $as), '=', $where): $where;
}

# формирует матрицу значений для INSERT
sub INS_SET {
	my ($self, $m) = @_;
	my $set = join ",", map {join "", "(", join(",", map {$self->quote($_)} @$_), ")"} @$m;
	return $set;
}

# формирует столбцы
sub FIELDS {
	my ($self, $fields) = @_;
	return map { $self->word($_) } @$fields if ref $fields eq "ARRAY";
	return map { my $val = $fields->{$_}; $_ eq $val? $_: $self->word($_) . ' As ' . $self->word($val) } keys %$fields if ref $fields eq "HASH";
	return $fields;
}

# возвращает массив названий столбцов
sub FIELDS_NAMES {
	my ($self, $fields) = @_;
	return @$fields if ref $fields eq "ARRAY";
	return keys %$fields if ref $fields eq "HASH";
	return split /,\s*/, $fields;
}

# формирует столбцы с таблицей
sub FOR_TAB_FIELDS {
	my ($self, $fields, $as) = @_;
	$as = $self->word($as);
	return map { ($as.".".$self->word($_)) } @$fields if ref $fields eq "ARRAY";
	return map { ($as.".".$fields->{$_}, ' as ', $self->word($_)) } keys %$fields if ref $fields eq "HASH";
	return map { $as.".".$self->word($_) } split /,\s*/, $fields;
}

# возвращает таблицу и её алиас, если он указан
sub TAB {
	return ($2, $1) if $_[1] =~ /^\s*(\w+)\s+(?:as\s+)?(\w+)\s*$/i;
	($_[1], $_[1]);
}


#@@category хелперы-выборки

# выборки
sub sel {
	# возвращаем sql, если не указано что преобразовывать
	return $_[1] if !defined $_[2];

	my ($self, $tab, $view, @args) = @_;
	my @view = $self->FIELDS($view);
	die "not fields in sql query for `$tab`" unless @view;
	my $sep = " ";
	if(ref $tab) {
		$sep = "\n" if @$tab > 1;
		$tab = join "\n", @$tab;
	} else {
		$tab = $self->word($tab);
	}
	
	my $sql = join "", "SELECT ", join(", ", @view), "${sep}FROM ", $tab, 
		@args==1 && ref $args[0] eq "" && $args[0] =~ /^\s*(WHERE|GROUP|HAVING|ORDER|LIMIT)\b/i? (" ", $args[0]): @args>0 && defined($args[0])? $self->query_add($sep, \@args): ();
	$sql
}

# добавляет в push arg
sub query_compile {
	my ($self, $arg, $push, $as) = @_;
	my ($op, @val) = @$arg;
	my $val = (@val==1? $val[0]: \@val);
	if($op eq "WHERE") { # WHERE
		push @{$push->{where}}, $self->DO_WHERE($val, $as);
	} elsif($op eq "GROUP") {	# GROUP
		push @{$push->{group}}, $self->FOR_TAB_FIELDS($val, $as);
	} elsif($op eq "HAVING") { # HAVING
		push @{$push->{having}}, $self->DO_WHERE($val, $as);
	} elsif($op eq "ORDER") { # ORDER
		push @{$push->{order}}, $self->FOR_TAB_FIELDS($val, $as);
	} elsif($op eq "LIMIT") { # LIMIT
		die "Один LIMIT уже есть в SQL-запросе" if $push->{limit};
		$push->{limit} = join ", ", @val;
	} else {
		die "Что-то неясное попало в выражение для select";
	}
}

# добавляет к запросу указанное в массиве
sub query_add {
	my ($self, $sep, $args) = @_;
	my $push = {};
	for my $arg (@$args) {
		if(ref $arg eq 'ARRAY') { $self->query_compile($arg, $push) }
		else { push @{$push->{where}}, $self->DO_WHERE($arg) }
	}
	$self->query_join($push, $sep);
}

# объединяет все выражения в sql-запрос
sub query_join {
	my ($self, $arg, $sep) = @_;
	my @w;
	
	($arg->{where} && (@w=@{$arg->{where}})? ("${sep}WHERE ", @w==1? @w: join " AND ", map { join "", "(", $_, ")" } @w): ()), 
	($arg->{group} && (@w=@{$arg->{group}})? ("${sep}GROUP BY ", join ", ", @w): ()), 
	($arg->{having} && (@w=@{$arg->{having}})? ("${sep}HAVING ", @w==1? @w: join " AND ", map { join "", "(", $_, ")" } @w): ()),
	($arg->{order} && (@w=@{$arg->{order}})? ("${sep}ORDER BY ", join ", ", @w): ()), 
	($arg->{limit}? ("${sep}LIMIT ", $arg->{limit}): ());
}

sub flat2volume { my ($self, $join, @args) = @_; my ($tab) = @args; my ($as); ($as, $tab) = $self->TAB($tab); return {tab => $self->word($tab), as => $self->word($as), args=>[@args], join=>$join}; }

# выборки
sub sel_join {
	my ($self, @any) = @_;
	my (@st, @where, @view, @join, $push);
	my ($as_table, $table) = $self->TAB($_[1]);
	my ($fields, $real_fields, $real_new, @cols) = ([], []);
	@st = [[], \@any, $real_fields];
	
	while(@st) {
		my ($path, $args, $real) = @{ pop @st };
		my ($tab, $view, @args) = @$args;
		my ($as); ($as, $tab) = $self->TAB($tab);
		
		push @view, $self->FOR_TAB_FIELDS($view, $as);
		push @$fields, [$path, [ @cols = $self->FIELDS_NAMES($view) ] ];
		push @$real, @cols;
		my @real = ();
		for my $arg (@args) {
			if(ref $arg eq "ARRAY") {
				my ($op) = @$arg;
				if($op =~ /^(?:LEFT|INNER)$/) {
					$arg = $self->flat2volume(@$arg);
					my ($as1, $tab1) = ($arg->{as}, $arg->{tab});
					my $is1 = $as1 eq $tab1;
					unshift @real, $real_new = [$as1];
					push @st, [[@$path, $as1], $arg->{args}, $real_new];
					push @join, join "", "\n", $op, " JOIN ", $tab1, (!$is1? (' as ', $as1): ()), " ON ", $as1, '.', ($is1? $as: $as1), "_id=", $as, ".id";
				} else {
					$self->query_compile($arg, $push, $as);
				}
			} else {
				push @{$push->{where}}, $self->DO_WHERE($arg, $as);
			}
		}
		push @$real, @real;
	}
	my $sql = join "", "SELECT ", join(", ", @view), "\nFROM ", $self->word($table), ($table ne $as_table? (' as ', $as_table): ()), @join, $self->query_join($push, "\n");
	
	wantarray? ($sql, $fields, $real_fields): $sql;
}

# пакует строки для передачи клиенту
sub pack_rows {
	my ($self, $fld, $rows_start, $add_row) = @_;
	
	my ($i, @st, $row, $rows) = 0;
	my $old_path = -1;
	
	for my $field (@$fld) {
		my ($path, $cols) = @$field;
		
		splice(@st, scalar(@$path)), $st[$#$path] ++ if $old_path == @$path;
		
		$rows = $rows_start;
		$row = $rows->[$#$rows], $rows = ($row->[$_] ||= [[]]) for @st[0..$#$path];
		if( $#$rows == -1 ) { push @$rows, $row = [] } else { $row = $rows->[$#$rows] }
		
		if(not defined $add_row->[$i]) { $i += @$cols; pop @$rows if @$row == 0; }
		elsif(@$row == 0) { push @$row, $add_row->[$i++] for @$cols; }
		elsif($add_row->[$i] == $row->[0]) {	# сравниваем id
			$i += @$cols;
		} else {
			push @$rows, $row = [];
			push @$row, $add_row->[$i++] for @$cols;
		}
		
		$st[$old_path = scalar @$path] += scalar @$cols;
	}
	$self
}


# запрашивает строки и пакует их в формат для передачи
sub query_rows {
	my ($self, $tab, $view, @args) = @_;
	my ($sql, $fields, $real_fields) = $self->sel_join(@_);
	
	push(@{$self->{sql_save}}, $sql), return if $self->{sql_save};
	
	my $query = $self->log_query($sql => sub {
		my $rows = [];
		while(my $row = $_->fetchrow_arrayref) {
			$self->pack_rows($fields, $rows, $row);
		}
		return {
			fields => $real_fields,
			rows => $rows
		};
	});	
}

# аналог query_rows: запрашивает строки отдельными запросами
# отличие в том, что LIMIT может быть проставлен для отдельных подстрок
# INNER_JOIN тут работает так же как и LEFT_JOIN
sub quick_rows {
	my ($self, $tab, $view, @args) = @_;
	
	my @join = grep { ref $_ eq 'ARRAY' and $_->[0] =~ /^LEFT|INNER$/ } @args;
	@args = grep { !(ref $_ eq 'ARRAY' and $_->[0] =~ /^LEFT|INNER$/) } @args;
	
	my (@a);
	($tab, $view, @a) = @{$_->{args}}, $_->{a} = [@a], $_->{view} = $view for @join;
	
	my $rows = $self->query_all($tab, $view, @args);
	
	for my $row (@$rows) {
		for my $join (@join) {
			$row->{$join->{as}} = $self->quick_rows(($join->{tab}, $join->{view}, $row->{id}, @{$join->{a}}));
		}
	}
	
	return $rows;
}


#@@category выборки

# запрашивает первую строку в виде массива
sub query {
	my ($self) = @_;
	my $sql = sel @_;
	
	push(@{$self->{sql_save}}, $sql), return if $self->{sql_save};
	
	$self->log_query($sql => sub { $_->fetchrow_array })
}

# первая строка в виде хеша
sub query_ref {
	my $x = query_all(@_);
	$x->[0];
}

# запрашивает строки в виде массивов
sub query_array {
	my ($self) = @_;
	my $sql = sel @_;
	
	push(@{$self->{sql_save}}, $sql), return if $self->{sql_save};
	
	my $r = $self->log_query($sql => sub { $_->fetchall_arrayref });
	wantarray? @$r: $r
}

# запрашивает строки в виде хешей
sub query_all {
	my ($self) = @_;
	my $sql = sel @_;
	
	push(@{$self->{sql_save}}, $sql), return if $self->{sql_save};
	
	$self->log_query($sql => sub {
		my $r = [];
		while(my $x=$_->fetchrow_hashref) { push @$r, $x; }
		$r
	});
}

# массив значений столбца
sub query_col {
	my ($self) = @_;
	my $sql = sel @_;
	
	push(@{$self->{sql_save}}, $sql), return if $self->{sql_save};
	
	$self->log_query($sql => sub {
		my $r = [];
		while(my ($x)=$_->fetchrow_array) { push @$r, $x; }
		$r
	});
}

#@@category dbh

# возвращает-устанавливает dbh
sub _dbh {
	my ($self, $dbh) = @_;
	
	my $old = $Coro::current->{$self};
	
	# в волокне ещё нет dbh, а значит и деструктора
	$app->coro->me->destroy(sub { $self->_close(@_) }) if !$old;
	
	if(@_ == 1) {
		$dbh = $old? $old: do { $self->connect; $Coro::current->{$self} };
		$dbh
	}
	else {
		$Coro::current->{$self} = $dbh;
		$self
	}
}

# закрывает соединение в текущем coro
sub _close {
	my ($self, $name) = @_;
	my $dbh = $app->coro($name)->info->{coro}{$self};
	$dbh->disconnect if $dbh;
	$self
}

# закрывает текущее соединение
sub close {
	my ($self) = @_;
	my $dbh = delete $Coro::current->{$self};
	$dbh->disconnect if $dbh;
	$self
}

#@@category логирование

require Term::ANSIColor;
our $COLOR_NUM = Term::ANSIColor::CYAN();
our $COLOR_WORD = Term::ANSIColor::MAGENTA();
our $COLOR_STR1 = Term::ANSIColor::GREEN();
our $COLOR_STR2 = Term::ANSIColor::GREEN();
our $COLOR_STR3 = Term::ANSIColor::RED();
our $COLOR_COMMENT = Term::ANSIColor::BLUE();
our $COLOR_DO = Term::ANSIColor::BOLD() . Term::ANSIColor::BLACK();
our $COLOR_RESET = Term::ANSIColor::RESET();

# подсветка синтаксиса
sub color {
	my ($self, $sql, $type) = @_;
	
	my $cw = !defined($type) && $sql !~ /^\s*SELECT\b/i || defined($type) &&
		$type eq "do"? $COLOR_DO: $COLOR_WORD;
	
	join "", (map { ((
		/^\d+$/? $COLOR_NUM: 
		/^'/? $COLOR_STR1:
		/^"/? $COLOR_STR2:
		/^`/? $COLOR_STR3:
		/^(?:--[ \t]|#|\/\*)/? $COLOR_COMMENT:
		/^[A-Z_\d]+$/? $cw: $COLOR_RESET
	), $_)
	} split /(\w+|'(?:\\'|[^'])*'|"(?:\\"|[^"])*"|`(?:\\`|[^`])*`|--[ \t].*|#.*|\/\*.*?\*\/)/s, $sql), $COLOR_RESET;
}


# логирует
sub log {
	my ($self, $sql, $sub, $type) = @_;
	
	my $time = Time::HiRes::time();
	my @ret;
	
	local $_ = $self->_dbh;
	
	if(wantarray) {
		@ret = eval { $sub->() };
	} else {
		$ret[0] = eval { $sub->() };
	}
	
	die join "\n", $@, $self->color($sql, $type) if $@;
	
	$app->log->info(":inline empty", $self->color($sql, $type), 
		(@ret? (":red", " -> ", ":reset sep", @ret): ()),
		":bold black", " ", $app->perl->fsec(Time::HiRes::time() - $time)
	) if $self->{log};
	
	wantarray? @ret: $ret[0]
}

# асинхронный prepare
sub _prepare {
	my ($self, $sql, $sub) = @_;
	my $dbh = $self->_dbh;
	my $sth = $dbh->prepare($sql, { async => 1 });
	$sth->execute;
	
	my $mysql_watcher = AnyEvent->io(
		fh   => $dbh->mysql_fd,
		poll => 'r',
		cb   => Coro::rouse_cb(),
	);
	
	Coro::rouse_wait();
	
	undef $mysql_watcher;
	
	my $guard = guard { $sth->finish };		
	local $_ = $sth;
	$sub->()
}

# делает ещё и prepare
sub log_query {
	my ($self, $sql, $sub, $type) = @_;
	$self->log($sql, sub { $self->_prepare($sql, $sub) }, $type)
}

# включает логирование
sub logon {
	my ($self, $log_info) = @_;
	$self->{log} = 1;
	$self->{log_info} = $log_info;
	$self
}

# отключает логирование
sub logoff {
	my ($self) = @_;
	$self->{log} = 0;
	$self->{log_info} = 0;
	$self
}


# отключает логирование в функции
sub nolog {
	my ($self, $cb) = @_;
	
	my $log = $self->{log};
	my $guard = guard { $self->{log} = $log };
	
	$self->{log} = $self->{log_info};
	$cb->();
}


#@@category изменение

# id последней добавленной записи
sub last_id {
	my ($self) = @_;
	$self->query("SELECT LAST_INSERT_ID()");
}

# количество изменённых строк последней операцией редактирования
sub last_count { $_[0]->{last_count} }
sub effected_rows { $_[0]->{last_count} }
sub effect { $_[0]->{last_count} }

# выполняет sql-запросы
sub do {
	my ($self, @sql) = @_;
	
	push(@{$self->{sql_save}}, @sql), return if $self->{sql_save};

	$self->log(join(";\n", @sql) => sub {
		my @ret;
		my $clean = 0;
		for my $sql (@sql) {
			my $ret = $self->_prepare($sql, sub { $_->mysql_async_result });
		
			push @ret, $ret+0;
			
			$clean++ if $sql =~ /^\s*(CREATE|ALTER|DROP|ATTACH|DETACH)\b/n;
		}
	
		$self->clean if $clean;	# сбрасываем индексы
	
		wantarray? @ret: R::sum @ret
	}, "do");
}

# удаляет записи из таблицы
sub erase {
	my ($self, $tab, $where) = @_;
	
	my @rmtab;
	if(ref $tab) {
		if(@$tab == 1) { $tab = $self->word(@$tab) . " " }
		else { @rmtab = $tab->[0] =~ /(\s\w+)$/; $tab = join "\n", @$tab, "" }
	} else {
		$tab = $self->word($tab) . " ";
	}
	
	my $cond = $self->DO_WHERE($where);
	
	my $sql = join "", "DELETE", @rmtab, " FROM ", $tab, (defined($cond)? ("WHERE ", $cond): ());
	$self->{last_count} = $self->do($sql) + 0;
	$self
}

# изменяет запись
sub update {
	my ($self, $tab, $param, $where, $opt) = @_;
	my $SET = $self->DO_SET($param);
	my $from;
	if(ref $tab) {
		if(@$tab == 1) { $tab = $tab->[0]; }
		else {
			$from = $tab;
			($tab) = $from->[0] =~ /\s(\w+)$/;
		}
	}
	my $sql = join "", "UPDATE ", $self->word($tab), " SET ", $SET, ($from? ("\nFROM ", join "\n", @$from, ""): " "), (defined($where)? ("WHERE ", $self->DO_WHERE($where)): ()), (defined($opt)? " $opt": ());
	$self->{last_count} = $self->do($sql)+0;
	$self
}


# добавляет одну запись в таблицу и возвращает её id
sub append {
	my ($self, $tab, $param) = @_;
	$self->add($tab, $param)->last_id;
}

# добавляет одну запись в таблицу
sub ADD_AS_INSERT {}
sub add {
	my ($self, $tab, $param, $replace, $ansi) = @_;
	my $INSERT = $replace? "REPLACE": "INSERT";
	my $sql;
	$ansi = 1 if $self->ADD_AS_INSERT;
	if(defined($param) and 0!=keys %$param) {
		if($ansi) {
			my $SET = $self->INS_SET([[values %$param]]);
			$sql = join "", "$INSERT INTO ", $self->word($tab), " (", join(", ", $self->FIELDS([keys %$param])), ") VALUES ", $SET;
		} else {
			my $SET = $self->DO_SET($param);
			$sql = join "", "$INSERT INTO ", $self->word($tab), " SET ", $SET;
		}
	} else {
		$sql = join "", "$INSERT INTO ", $self->word($tab), " () VALUES ()";
	}
	$self->{last_id} = undef;
	$self->{last_count} = $self->do($sql) + 0;
	$self
}


# добавляет много записей в таблицу
sub insert {
	my ($self, $tab, $fields, $matrix) = @_;
	my $SET = $self->INS_SET($matrix);
	my $sql = join "", "INSERT INTO ", $self->word($tab), " (", join(", ", $self->FIELDS($fields)), ") VALUES ", $SET;
	$self->{last_id} = undef;
	$self->{last_count} = $self->do($sql)+0;
	$self
}


# добавляет или изменяет запись основываясь на наличии id в параметрах
sub save {
	my ($self, $tab, $param, $id) = @_;
	if(my $id = $param->{id} // $id) {
		{
			delete local $param->{id};
			$self->update($tab, $param, $id);
		};
		if($self->{last_count} == 0) {
			local $param->{id} = $id;
			$self->add($tab, $param);
		}
		$self->{last_id} = $id;
	} else {
		$self->add($tab, $param);
	}
	return $self;
}

# добавляет или изменяет первую попавщуюся запись
# sub replace {
	# my ($self, $tab, $param) = @_;
	# my $id = $self->query($tab, "id", $param, "LIMIT 1");
	# if($id) {
		# delete $param->{id};
		# $self->update($tab, $param, $id);
		# $self->{last_id} = $id;
	# } else {
		# $self->add($tab, $param);
	# }
	# return $self;
# }

# добавляет или изменяет первую попавщуюся запись
sub replace {
	my ($self, $tab, $param) = @_;
	$self->add($tab, $param, 1);
}


# добавляет или изменяет первую попавщуюся запись
sub store {
	my ($self, $tab, $param) = @_;
	my $id;
	if(ref($param) and ($id = $param->{id}) and $self->query($tab, "1", $id)) {
		delete $param->{id};
		$self->update($tab, $param, $id);
		$self->{last_id} = $id;
	} else {
		$self->add($tab, $param);
	}
	return $self;
}

# добавляет или изменяет первую попавщуюся запись по id
sub inject {
	my ($self, $tab, $param, $inject) = @_;
	
	my $SET = $self->INS_SET([[values %$param]]);
	
	my @fields = $self->FIELDS([keys %$param]);
	
	$inject //= [ grep { $_ ne "id" } @fields ];
	$inject = join ", ", map { "$_ = VALUES($_)" } sort @$inject;
	
	
	my $sql = join "", "INSERT INTO ", $self->word($tab), " (", join(", ", @fields), ") VALUES ", $SET,
		"ON DUPLICATE KEY UPDATE $inject";
	
	$self->do($sql);
	
	$self->{last_id} = $param->{id};
	
	return $self;
}


#@@category transaction

# инкрементирует счётчик начавшихся транзакций
sub _acc_transaction {
	my ($self, $acc) = @_;
	$Coro::current->{ $self->{_acc_transaction_key} //= $self . "_acc_transaction_key" } += $acc;
}

# возвращает гвард
sub begin {
	my ($self) = @_;
	
	$self->_acc_transaction( 1 );
	$self->_dbh->{AutoCommit} = 0;
	
	guard {
		# откат транзакции
		if(0 == $self->_acc_transaction( -1 )) {
			eval { $self->_dbh->rollback };
			undef $@;
			$self->_dbh->{AutoCommit} = 1;
		}
	}
}

# коммит
sub commit {
	my ($self) = @_;
	die "commit не в транзакции" if $self->_dbh->{AutoCommit};
	return $self if $self->_acc_transaction( 0 ) != 1;
	$self->_dbh->{AutoCommit} = 1;
	$self->_dbh->{AutoCommit} = 0;
	$self
}

# транзакция
sub transaction {
	my ($self, $eval, $block);
	if(@_ == 2) { ($self, $block) = @_; }
	else { ($self, $eval, $block) = @_; }
	
	my $guard = $self->begin;

	eval {
		$block->();
		$self->commit;
	};
	
	if(my $error = $@) {
		undef $guard;
		die $error if !$eval;
	}
	
	$self
}


#@@category alter

# создаёт базу, таблицу, столбец, индекс или ключ
# например:
#	create(".test_database")
#	create("test_tab")
#	create("test_tab.col_a", "int default 0")
#	create("test_tab:idx_a", "col1", "col2")
#	create("test_tab:idx_a:unique", "col1")
#	create("test_tab:fk_a:fk:tab1.col1")
#	create("test_tab:fk_a:fk", "tab1.col1")
#	create("test_tab:fk_b:fk:tab2") - по умолчанию - колумн id
sub create {
	my $self = shift;
	$self->do( $self->show_create(@_) );
}

# удаляет таблицу, столбец, индекс или ключ
sub drop {
	my $self = shift;
	$self->do( $self->show_drop(@_) );
}

# изменяет опции таблицы, столбец, индекс или ключ
sub modify {
	my $self = shift;
	$self->do( $self->show_modify(@_) );
}

# переименовывает таблицу или столбец, если первый параметр "таблица.столбец"
# или индекс, если "tab:индекс", так же fk
sub rename {
	my $self = shift;
	$self->do( $self->show_rename(@_) );
}

# проверяет на существование:
# exists(".base")
# exists("tab")
# exists("tab.col")
# exists("tab:idx_a") - unique или index или fk
# exists("tab:idx_a:unique")
# exists("tab:idx_a:index")
# exists("tab:idx_a:fk")
# exists("tab:col:fk:ref_tab.ref_col")
# exists("tab:col:fk:ref_tab")
sub exists {
	my ($self, $from) = @_;
	
	if($from =~ /^\./) {
		$self->exists_database($');
	}
	elsif($from =~ /\./) {
		$self->tab($`)->col($')->exists;
	}
	elsif($from =~ /:/) {
		my ($tab, $idx, $type, $ref_col) = split /:/, $from;
		my $ref_tab;
		if($ref_col =~ /./) {
			($ref_tab, $ref_col) = ($`, $');
		} else {
			($ref_tab, $ref_col) = ($ref_col, $idx);
		}
		$self->exists_idx($tab, $idx, $type, $ref_tab, $ref_col);
	}
	else {
		$self->tab($from)->exists;
	}
}


# возвращает sql для создания
sub show_create {
	my ($self, $from, $info) = @_;
	
	if($from =~ /^\:/) {
		$self->show_create_database($', $info);
	}
	elsif($from =~ /\./) {
		$self->alter_create_col($`, $', $info);
	} elsif($from =~ /:/) {
		my ($tab, $idx, $type) = split /:/, $from;
		$type //= "index";
		if($type ne "fk") {
			$self->alter_create_idx($tab, $idx, $type, $info, @_[3..$#_]);
		} else {
			$self->alter_create_fk($tab, $idx, $info, @_[3..$#_]);
		}
	} else {
		$self->alter_create_tab($from, $info);
	}
}

# возвращает sql для удаления
sub show_drop {
	my ($self, $from) = @_;
	
	if($from =~ /^\:/) {
		$self->show_drop_database($');
	}
	elsif($from =~ /\./) {
		$self->show_drop_column($self->tab($`)->col($'));
	} elsif($from =~ /:/) {
		my ($tab, $idx, $type, $ref) = split /:/, $from;
		if($type eq "fk") {
			my ($ref_tab, $ref_col) = split /\./, $ref;
			$self->show_drop_fk($self->tab($tab)->fk($idx, $ref_tab, $ref_col));
		} else {
			my $idx = $self->tab($tab)->idx($idx);
			$idx->unique(1) if $type =~ /^(unq|unique)/;
			$self->show_drop_index($idx);
		}
	} else {
		$self->show_drop_table($self->tab($from));
	}
}

# возвращает sql для модификации
sub show_modify {
	my ($self, $from, @info) = @_;
	
	if($from =~ /^\:/) {
		$self->show_modify_database($');
	}
	elsif($from =~ /\./) {
		$self->show_modify_column($self->tab($`)->col($')->extend(@info));
	} elsif($from =~ /:/) {
		my ($tab, $idx, $type, $ref) = split /:/, $from;
		if($type eq "fk") {
			my ($ref_tab, $ref_col) = split /\./, $ref;
			$self->show_modify_fk($self->tab($tab)->fk($idx, $ref_tab, $ref_col)->extend(@info));
		} else {
			my $idx = $self->tab($tab)->idx($idx);
			$idx->unique(1) if $type =~ /^(unq|unique)/;
			$self->show_modify_index($idx->extend(@info));
		}
	} else {
		$self->show_modify_table($self->tab($from)->extend(@info));
	}
}

# возвращает sql для переименования
sub show_rename {
	my ($self, $from, $to) = @_;
	
	if($from =~ /\./) {
		$self->show_rename_column($self->tab($`)->col($'), $to);
	} elsif($from =~ /:/) {
		my ($tab, $idx, $type, $ref) = split /:/, $from;
		if($type eq "fk") {
			my ($ref_tab, $ref_col) = split /\./, $ref;
			$self->show_rename_fk($self->tab($tab)->fk($idx, $ref_tab, $ref_col), $to);
		} else {
			my $idx = $self->tab($tab)->idx($idx);
			$idx->unique(1) if $type =~ /^(unq|unique)/;
			$self->show_rename_index($idx, $to);
		}
	} else {
		$self->show_rename_table($self->tab($from), $to);
	}
}

#@@category DDL базы

# стандартные опции базы
sub show_definition_database {
	my ($self, $base) = @_;
	return
		defined($base->charset)? ("DEFAULT CHARACTER SET", $self->word($base->charset)): (),
		defined($base->collation)? ("DEFAULT COLLATE", $self->word($base->collation)): ();
}

# создать базу
sub show_create_database {
	my ($self, $base) = @_;
	join " ", "CREATE DATABASE", $self->word($base->name), $self->show_definition_database($base);
}

# удалить базу
sub show_drop_database {
	my ($self, $base) = @_;
	join " ", "DROP DATABASE", $self->word($base->name)
}

# изменить базу
sub show_modify_database {
	my ($self, $base) = @_;
	join " ", "ALTER DATABASE", $self->word($base->name), $self->show_definition_database($base);
}

#@@category DDL таблиц

# устанавливает OTHER
sub set_other {
	my $self = shift;
	$self->{OTHER} = [@_];
	$self
}

# опции таблицы
sub show_definition_table {
	my ($self, $tab, $create) = @_;
	
	join " ",
		defined($tab->autoincrement)? "AUTO_INCREMENT=" . $tab->autoincrement: (),
		defined($tab->charset)? 
			($create? "DEFAULT": "CONVERT TO",
			"CHARACTER SET", $self->quote($tab->charset), "COLLATE", $self->quote($tab->collation)): (),
		defined($tab->engine)? "ENGINE=" . $tab->engine: (),
		defined($tab->autoincrement)? "AUTO_INCREMENT=" . $tab->autoincrement: (),
		defined($tab->options)? $tab->options: (),
		defined($tab->comment)? $self->COMMENT($tab->comment, "tab"): (),
}

# внутренности таблицы
sub show_create_definition {
	my ($self, $tab) = @_;
	join(",\n",
		(map { $self->show_column($_, 1) } @{$tab->columns}),
		(map { $self->show_index($_) } @{$tab->indexes}),
		(map { $self->show_foreign($_) } @{$tab->foreigns}),
	);
}

# создать таблицу
sub show_create_table {
	my ($self, $tab) = @_;
	return
		join("", "CREATE TABLE ", $self->word($tab->name), " (\n", $self->show_create_definition($tab), "\n) ", $self->show_definition_table($tab, 1)),
		@{$self->{OTHER}};
}

# модифицировать опции таблицы
sub show_modify_table {
	my ($self, $tab) = @_;
	join "", "ALTER TABLE ", $self->word($tab->name), " ", $self->show_definition_table($tab);
}

# переименовать таблицу
sub show_rename_table {
	my ($self, $tab, $to) = @_;
	join "", "ALTER TABLE ", $self->word($tab->name), " RENAME TO ", $self->word($to);
}

# удалить таблицу
sub show_drop_table {
	my ($self, $tab) = @_;
	join "", "DROP TABLE ", $self->word($tab->name);
}

# удалить таблицу
sub show_truncate_table {
	my ($self, $tab) = @_;
	join "", "TRUNCATE TABLE ", $self->word($tab->name);
}


#@@category DDL столбцов


# возвращает колумн из info без названия столбца
sub show_definition_column {
	my ($self, $col, $with_keys) = @_;
	join " ", $col->type,
		$with_keys? (
			$col->null || $col->pk? (): "NOT NULL",
		): (
			$col->null? (): "NOT NULL",
		),
		defined($col->default)? ("DEFAULT ", $self->quote($col->default)): (),
		$with_keys? (
			$col->pk? "PRIMARY KEY": (),
			$col->autoincrement? $self->AUTO_INCREMENT: (),
		): (),
		#$sql->{extra} ne ""? uc " $sql->{extra}": (),
		defined($col->comment)? $self->COMMENT($col->comment, "col"): (),
	;
}

# столбец с названием
sub show_column {
	my ($self, $col, $with_keys) = @_;
	join " ", $self->word($col->name), $self->show_definition_column($col, $with_keys)
}

# создать столбец
sub show_create_column {
	my ($self, $col, $after) = @_;
	join "", "ALTER TABLE ", $self->word($col->tab), " ADD COLUMN ", 
		$self->show_column($col),
		$self->show_after_column($after)
}

# изменить столбец
sub show_modify_column {
	my ($self, $col, $after) = @_;
	join "", "ALTER TABLE ", $self->word($col->tab), " MODIFY COLUMN ", 
		$self->show_column($col),
		$self->show_after_column($after)
}

# после
sub show_after_column {
	my ($self, $after) = @_;
	return "" if !defined $after;
	return " FIRST" if $after == 1;
	join "", " AFTER ", $self->word(ref $after? $after->name: $after)
}

# переименовать столбец
sub show_rename_column {
	my ($self, $col, $to) = @_;
	join "", "ALTER TABLE ", $self->word($col->tab), " CHANGE ", $self->word($col->name), " ", $self->word($to), " ", $self->show_definition_column($col->clone->load);
}

# удалить столбец
sub show_drop_column {
	my ($self, $col) = @_;
	join "", "ALTER TABLE ", $self->word($col->tab), " DROP ", $self->word($col->name);
}


#@@category DDL индексов



# формирует индекс без его названия
sub show_definition_index {
	my ($self, $idx) = @_;
	join "", "(", join(", ", map { $self->word($_) } @{$idx->cols}), ")",
		defined($idx->comment)? (" ", $self->COMMENT($idx->comment, "idx")): ();
}

# индекс c типом и названием
sub show_index {
	my ($self, $idx) = @_;
	join "", $idx->type, ($idx->type ne "INDEX"? " KEY": ()), " ", $self->word($idx->name), " ", $self->show_definition_index($idx);
}

# формирует индекс из info
sub show_create_index {
	my ($self, $idx) = @_;
	join "", "ALTER TABLE ", $self->word($idx->tab), " ADD ", $self->show_index($idx)
}

# изменяет индекс
sub show_modify_index {
	my ($self, $idx) = @_;	
	join "", $self->show_drop_index($idx), ", ADD ", $self->show_index($idx->clone->upd);
}

# переименовывает индекс
sub show_rename_index {
	my ($self, $idx, $to) = @_;
	$to = $idx->clone->name($to) if !ref $to;
	join "", $self->show_drop_index($idx), ", ADD ", $self->show_index($to->clone->upd);
}

# удалить индекс
sub show_drop_index {
	my ($self, $idx) = @_;
	join "", "ALTER TABLE ", $self->word($idx->tab), " DROP INDEX ", $self->word($idx->name)
}


#@@category DDL ссылок

# формирует индекс без его названия
sub show_definition_foreign {
	my ($self, $fk) = @_;
	join "", "FOREIGN KEY (", join(", ", map { $self->word($_) } @{$fk->cols}), ") REFERENCES ",
		$self->word($fk->ref_tab), " (", join(", ", map { $self->word($_) } @{$fk->refs}), ")",
		defined($fk->on_update)? (" ON UPDATE ", $fk->on_update): (),
		defined($fk->on_delete)? (" ON DELETE ", $fk->on_delete): ();
}

# индекс c типом и названием
sub show_foreign {
	my ($self, $fk) = @_;
	join "", "CONSTRAINT ", $self->word($fk->name), " ", $self->show_definition_foreign($fk);
}

# создаёт ссылку
sub show_create_foreign {
	my ($self, $fk) = @_;
	join "", "ALTER TABLE ", $self->word($fk->tab), " ADD ", $self->show_foreign($fk)
}

# модифицирует
sub show_modify_foreign {
	my ($self, $fk) = @_;
	return $self->show_drop_foreign($fk), 
		$self->show_create_foreign($fk);
	#join "", "ALTER TABLE ", $self->word($fk->tab), " DROP FOREIGN KEY ", $self->word($fk->name), "; ", ", ADD ", $self->show_foreign($fk)
}

# переименовывает fk
sub show_rename_foreign {
	my ($self, $fk, $to) = @_;
	$to = $fk->clone->name($to) if !ref $to;
	join "", "ALTER TABLE ", $self->word($fk->tab), " DROP FOREIGN KEY ", $self->word($fk->name), ", ADD ", $self->show_foreign($to)
}

# удаляет ссылку
sub show_drop_foreign {
	my ($self, $fk) = @_;
	join "", "ALTER TABLE ", $self->word($fk->tab), " DROP FOREIGN KEY ", $self->word($fk->name)
}



#@@category СХЕМА

# возвращает информацию
# inform("tab")
# inform("tab.col")
# inform("tab:*")
# inform("tab:idx")
# inform("tab:col:fk")
# inform("tab:*:fk")


# бин
sub bean {
	my $self = shift;
	my $tab = shift;
	TODO;
	$app->connectSchemeBean(table => $tab, connect => $self, row => {@_})
}

# коллекция
sub acc {
	TODO;
	$app->connectSchemeAccumulation(connect => @_)
}

# Операторы SQL делятся на:

# операторы определения данных (Data Definition Language, DDL):
# CREATE создает объект БД (саму базу, таблицу, представление, пользователя и т. д.),
# ALTER изменяет объект,
# DROP удаляет объект;

# операторы манипуляции данными (Data Manipulation Language, DML):
# SELECT выбирает данные, удовлетворяющие заданным условиям,
# INSERT добавляет новые данные,
# UPDATE изменяет существующие данные,
# DELETE удаляет данные;

# операторы определения доступа к данным (Data Control Language, DCL):
# GRANT предоставляет пользователю (группе) разрешения на определенные операции с объектом,
# REVOKE отзывает ранее выданные разрешения,
# DENY задает запрет, имеющий приоритет над разрешением;

# операторы управления транзакциями (Transaction Control Language, TCL):
# COMMIT применяет транзакцию,
# ROLLBACK откатывает все изменения, сделанные в контексте текущей транзакции,
# SAVEPOINT делит транзакцию на более мелкие участки.

# отключает проверку внешних ключей
sub unfkchecks {
	my ($self) = @_;
	$self->do("SET FOREIGN_KEY_CHECKS=0");
	guard { $self->do("SET FOREIGN_KEY_CHECKS=1") }
}

1;
