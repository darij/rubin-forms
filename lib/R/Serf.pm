package R::Serf;
# коллеция для сёрфинга по окнам и эмуляции действий пользователя в ОС

use base R::Collection;

use common::sense;
use R::App;

use Win32::GuiTest qw/:all/;
use Win32::Clipboard;

# свойства
has qw//;

# конструктор
sub new {
	my $cls = shift;
	$cls = ref $cls || $cls;
	bless {
		parts => [map {Isa($_, $cls)? $_->parts: !Num($_)? die("добавляется в файловую коллекцию $_, а не дескриптор окна"): $_} (@_? @_: 0)],
	}, ref $cls || $cls;
}

# делает 0-е окно активным
*fg = \&active;
sub active {
	my ($self) = @_;
	
	SetForegraundWindow $self->part;
	
	$self
}

# ввод с клавиатуры во все окна
sub keys {
	my ($self, $keys, $delay) = @_;
	
	for my $win ($self->parts) {
		SetForegraundWindow $win;
		SendKeys $keys, $delay;
	}
	
	$self
}

# ввод с клавиатуры
# ~ means ENTER
# + means SHIFT 
# ^ means CTRL 
# % means ALT
# {BACKSPACE}   Backspace
# {BS}          Backspace
# {BKSP}        Backspace
# {BREAK}       Break
# {CAPS}        Caps Lock
# {DELETE}      Delete
# {DOWN}        Down arrow
# {END}         End
# {ENTER}       Enter (same as ~)
# {ESCAPE}      Escape
# {HELP}        Help key
# {HOME}        Home
# {INSERT}      Insert
# {LEFT}        Left arrow
# {NUMLOCK}     Num lock
# {PGDN}        Page down
# {PGUP}        Page up
# {PRTSCR}      Print screen
# {RIGHT}       Right arrow
# {SCROLL}      Scroll lock
# {TAB}         Tab
# {UP}          Up arrow
# {PAUSE}       Pause
# {F1}          Function Key 1
# ...           ...
# {F24}         Function Key 24
# {SPC}         Spacebar
# {SPACE}       Spacebar
# {SPACEBAR}    Spacebar
# {LWI}         Left Windows Key
# {RWI}         Right Windows Key 
# {APP}         Open Context Menu Key
# {LEFTDOWN}    left button down
# {LEFTUP}      left button up
# {MIDDLEDOWN}  middle button down
# {MIDDLEUP}    middle button up
# {RIGHTDOWN}   right button down
# {RIGHTUP}     right button up
# {LEFTCLICK}   left button single click
# {MIDDLECLICK} middle button single click
# {RIGHTCLICK}  right button single click
# {ABSx,y}      move to absolute coordinate ( x, y )
# {RELx,y}      move to relative coordinate ( x, y )
sub sendkeys {
	my ($self, $keys, $delay) = @_;
	SendKeys $keys, $delay;
	$self
}

sub left { SendKeys "{LEFT}"; shift }
sub right { SendKeys "{RIGHT}"; shift }
sub up { SendKeys "{UP}"; shift }
sub down { SendKeys "{DOWN}"; shift }

sub leftup { SendLButtonUp(); shift }
sub leftdown { SendLButtonDown(); shift }
sub midup { SendMButtonUp(); shift }
sub middown { SendMButtonDown(); shift }
sub rightup { SendRButtonUp(); shift }
sub rightdown { SendRButtonDown(); shift }
# 0 to 65535
sub rel { my $y = pop; my $x = pop; SendMouseMoveRel($x, $y); shift }
sub abs { my $y = pop; my $x = pop; SendMouseMoveAbs($x, $y); shift }
# in pixel
sub pix { my $y = pop; my $x = pop; SendMouseMoveAbsPix($x, $y); shift }
sub wheel { my $y = pop; MouseMoveWheel($y); shift }


sub click { leftdown(); leftup(); shift }
sub dblclick { click(); sleep 0.1; click(); shift }

sub midclick { middown(); midup(); shift }
sub rightclick { rightdown(); rightup(); shift }

sub x { my ($x, $y) = GetCursorPos(); $x }
sub y { my ($x, $y) = GetCursorPos(); $y }

# FindWindowLike($window,$titleregex,$classregex,$childid,$maxlevel)

# возвращает окна по титлу. Ищет во всех
sub find {
	my ($self, $title) = @_;
	FindWindowLike(0, $title)
}

# делает активным 0-е окно
sub focus {
	my ($self) = @_;
	SetFocus $self->part;
	$self
}

# возвращает класс 0-го окна
sub class {
	my ($self) = @_;
	GetClassName $self->part
}

# режим дебага
sub verbose {
	my ($self, $x) = @_;
	$Win32::GuiTest::debug = !defined $x or $x;
	$self
}


1;