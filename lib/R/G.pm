package R::G;
# используется для поиска ошибок вроде "Out of memory!", когда разрушается стек
# используется так:
# my $x = $app->g("key");
# ... - код где предполагается вылет
# $x->r;

use common::sense;
use R::App qw/:min/;

# конструктор
sub new {
	my ($cls, $key) = @_;
	bless {key => $key}, ref $cls || $cls;
}

sub r {
	my ($self) = @_;
	undef $self->{key};
	$self
}

sub DESTROY {
	my ($self) = @_;
	print STDERR "$$ -> $self->{key}\n" if defined $self->{key};
}


1;