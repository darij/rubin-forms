package R::Ini;
# парсит ini-файл

use common::sense;
use R::App qw/:min/;

# для $app->ini
sub new {
	my $cls = shift;
	my $self = bless {}, ref $cls || $cls;
	
	my $path = $app->project->ini;
	if(defined($path)) {
		my $f = $app->file($path);
		my $ini = $app->use("R::Mime")->Get($f->ext)->from($f->read);
		
		
		my $include = $ini->{include};
		for my $path (@$include) {
			my $f = $app->project->file("etc/$path");
			
			if($f->exists) {
				my $conf = R::Mime->Get($f->ext)->from($f->read);
				
				_merge($ini, $conf);
			} else {
				$app->log->warn("а вот нету " . $f->path);
			}
		}
		
		%$self = %$ini;
	}
	
	$self
}

# рекурсивно объединяет хеши
sub _merge {
	my ($x, $y) = @_;
	
	for my $k (keys %$y) {
		my ($xe, $ye) = ($x->{$k}, $y->{$k});
		
		if(R::Is $xe, "HASH" and R::Is $ye, "HASH") {
			_merge($xe, $ye);
		}
		else {
			$x->{$k} = $ye;
		}
	}
	
}

1;