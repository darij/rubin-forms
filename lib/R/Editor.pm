package R::Editor;
# открывает файл в редакторе

use common::sense;
use R::App;

# свойства
has qw/inc/;

# конструктор
sub new {
	my $cls = shift;
	bless {	@_ }, ref $cls || $cls;
}

sub open {
	my ($self, $path, $line, $inc) = @_;
	
	my $cmd = $self->{cmd};
	if($path !~ /^\//) {
		$inc //= $self->inc;
		$path = $inc? $app->file(@$inc)->sub($path)->existing->path: $app->file($path)->abs->path;
	}
	$path = $app->file($path)->cygwin->path if $self->{win};
	$cmd =~ s/%l/$line/g;
	$cmd =~ s/%s/$path/g;
	
	msg1 $cmd;
	`$cmd`
}

1;