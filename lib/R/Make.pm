package R::Make;
# создаёт файл зависимостей и применяет их

use common::sense;
use R::App;

our %EXPORT = map {$_ => 1} qw/category name args port spec cron rels deps test task default_task make quit run/;

# импортирует символы
sub import {
	my $self = shift;
	my $caller = caller;
	
	for my $name (@_? @_: keys %EXPORT) {
		*{"${caller}::$name"} = \&$name;
	}
	
	$self;
}

# # деимпортирует символы
# sub unimport {
	# my $self = shift;
	# my $caller = caller;
	
	# for my $name (@_? @_: keys %EXPORT) {
		# delete *{"${caller}::$name"}{CODE};
	# }
	
	# $self;
# }

# используется для тестирования
sub quit (@) {
	#$app->log->info(@_);
	#exit;
	die $app->raiseQuit(msg => join(", ", @_) . "\n");
}

# конструктор
sub new {
	my ($cls) = @_;
	bless {}, ref $cls || $cls;
}

our $CATEGORY = "";
our $NAMESPACE = "";
our @CATEGORY;
our %TASK;
our $LAST;
our $DEFAULT;		# задание по умолчанию

# категория заданий
sub category ($) {
	$CATEGORY = $_[-1];
	push @CATEGORY, {category => $CATEGORY, tasks => []};
	$LAST = undef;
    $NAMESPACE = "";
	return;
}


# пространство имён
sub namespace ($) {
	($NAMESPACE) = @_;
	category $NAMESPACE =~ /:$/? $`: $NAMESPACE;
	return;
}

# описание задания для объектного интерфейса
sub add {
	my ($self, $class, $name, $args, $port, $spec) = @_;
	
	my $code = closure $class, $name, sub {
		my ($class, $name) = splice @_, 0, 2;
		$class->new->$name(@_);
	};

	
	
	_add($name, "?", "?", $code);
	
	args( $args ) if defined $args;
	port( $port ) if defined $port;
	spec( $spec ) if defined $spec;
	
	$self
}

# добавляет задание
sub _add {
	my ($name, $file, $line, $code) = @_;

	quit "команда `$name` $file:$line уже описана в $TASK{$name}{file}:$TASK{$name}{line}" if exists $TASK{$name};



	if($LAST) {
		quit "в задаче $LAST->{name} нет task-а" if !$LAST->{code};
	}

	$LAST = $TASK{$name} = {
		category => $CATEGORY,
		name => $name,
		file => $file,
		line => $line,
		code => $code,
	};

	push @CATEGORY, {} if !@CATEGORY;

	push @{ $CATEGORY[$#CATEGORY]->{tasks} }, $LAST;

	return;
}

# имя задания
sub name ($;$$$) {
	my ($name, $args, $port, $spec) = @_;
	
	my ($package, $file, $line) = caller(0);
	my $code = *{"${package}::$name"}{CODE};
	
	_add($name, $file, $line, $code);
	
	args( $args ) if defined $args;
	port( $port ) if defined $port;
	spec( $spec ) if defined $spec;
	return;
}

# аргументы задания
sub args ($) {
	my ($args) = @_;
	
	quit "args не в задании" if !defined $LAST;
	
	$LAST->{args} = $args;
	
	return;
}

# описание задания
sub port ($) {
	my ($port) = @_;
	
	quit "port не в задании" if !defined $LAST;
	
	$LAST->{port} = $port;
	
	return;
}

# спецификация задания - появляется, если набрать myapp help mytask
sub spec ($) {
	my ($spec) = @_;
	
	quit "spec не в задании" if !defined $LAST;
	
	$LAST->{spec} = $spec;
	
	return;
}

# регистрирует задание для регулярного перезапуска на tcp-сервере shiva
# 	* - каждую секунду
# 	10s - каждую минуту на 10-й секунде
#	4/5m - через пять минут с 4-й минуты
# задание будет каждый раз запускаться в отдельном процессе. Однако если указать модификатор:
#	ps - запускается каждый раз в отдельном процессе
# 	mainps - запускается в процессе сервера
# 	taskps - запускается в своём процессе
sub cron ($) {
	my ($cron) = @_;
	
	my @times = grep { length } split /\s+/, $cron;
	
	for my $time (@times) {
		
	}
}

# зависимости задания
sub rels (@) {
	#my $rel = shift;
	
	quit "rels не в задании" if !defined $LAST;
	
	$LAST->{rels} = [@_];
	
	return;
}

# файловые зависимости задания
sub deps ($$) {
	my ($files, $deps) = @_;
	
	quit "deps не в задании" if !defined $LAST;
	
	$LAST->{files} = $files;
	$LAST->{deps} = $deps;
	
	return;
}

# тестирует до того как будут применены зависимости 
sub test (&) {
	my ($code) = @_;
	
	quit "test не в задании" if !defined $LAST;
	
	$LAST->{test} = $code;
	
	return;
}

# устанавливает дефолтное задание
sub default_task(;$) {
	my ($name) = @_;
	
	$DEFAULT = $name // ($LAST? $LAST->{name}: quit "не указан обязательный параметр для default_task");
	
	return;
}

# задание
sub task (&) {
	my ($code) = @_;
	
	quit "task не в задании" if !defined $LAST;
	
	$LAST->{code} = $code;
	
	return;
}

# выполняет задание вместе с зависимостями рекурсивно
sub make (@) {
	my $name = shift;
	
	my $task = $TASK{$name};
	if(!$task) {
		$app->make->load($name);
		$task = $TASK{$name};
		if(!$task) {
			$app->log->error(":space", "неизвестное задание", ":red", $name);
			return if !length $name;
			require "Text/Levenshtein/XS.pm";
			$app->make->load;
			my $ask = min_by { Text::Levenshtein::XS::distance($_, $name) } keys %TASK;
			my $x = $TASK{$ask};
			my @args = length($x->{args})? (":magenta", " $x->{args}", ":reset"): ();
			$app->log->info(":empty", "Может имелось в виду ", ":green", $ask, @args, ":reset", "?\n\t", ":black bold", $x->{port}, ":reset", ". Категория: ", ":blue", $x->{category}) if $ask;
			return;
		}
	}
	
	my $test = $task->{test};
	if($test) {
		$test->(@_);
	}
	
	my $deps = $task->{deps};
	if($deps) {
	
		my $files = $task->{files};
	
		$files = $app->file($files)->glob if !ref $files;
		$deps = $app->file($deps)->glob if !ref $deps;
		
		return if $files->cmpmtime($deps);
	}
	
	my $rels = $task->{rels};
	if($rels) {
		for my $rel (@$rels) {
			&make( $rel );
			#return if $@;	# если установлена 
		}
	}
	
	
	die "Нет функции для команды `$task->{name}` $task->{file}:$task->{line}" if !$task->{code};
	$task->{code}->(@_);
	
	msg ":green", $name, ":white", " вернул код ", ":red", $? if $? != 0;
	#$app->log->error("задание $name было остановлено") if $@;
	
	return;
}

# подгружает все источники или источник для указанной команды
sub load {
	my ($self, $name) = @_;
	
	my $f = $app->project->files("make");
	#$f = $f->add($app->project->file("make")) if $app->project->name ne $app->framework->name;

	$f->find(qr/\.pm$/)->then(sub {
		require $_->dot->path if !defined $name or defined $name and do { 
			my $n=quotemeta $name; 
			$_->read =~ /^name\s*['"]$n["']/m;
		}
	});
	
	# for my $dir (@INC) {
		# my $cat = $app->file("R/Make")->frontdir($dir);
		# $cat->find(qr/\.pm$/)->then(sub { require $_->path }) if $cat->exists;
	# }

	$self
}

# запускает команду оболочки
sub run (@) {
	my $output = $app->tty->run(@_);
	quit $app->log->colorized(":empty", "команда завершилась с кодом ", ":red", $?, ":reset", ":\n", ":black bold", join " ", @_) if $?;
	return $output;
}

# используется на app->make->gosub("mk", args...)
*al = \&gosub;
sub gosub {
	my $self = shift;
	make @_;
	$self
}

# горячая замена make: нужно выгрузить все make-файлы
sub HOTSWAP_REMOVE {
	my ($cls, $hotswap, $from_reload) = @_;
	$hotswap->new(file=>$_)->remove for R::uniq map { $_->{file} } values %TASK;
}

1;