package R::Image;
# рисунок может формироваться в памяти, быть загружен из файла и сохранён в файл 

use common::sense;
use R::App;

use GD;
use POSIX qw/round/;

# свойства
has qw/path canvas/;

#@@category конструкторы, клонирование, загрузка и сохранение

# конструктор
sub new {
	my $cls = shift;
	bless {
		path => undef,	# путь к файлу
		@_
	}, ref $cls || $cls;
}

# клонирует себя
sub clone {
	my $self = shift;
	$self->new(%$self, @_)
}

# создаёт пустое изображение заданного размера и заменяет им canvas
sub wh {
	my ($self, $w, $h) = @_;
	$self->clone(canvas => new GD::Image($w, $h // $w));
}

# возвращает/устанавливает изображению размер
sub size {
	my ($self, $w, $h) = @_;
	return $self->canvas->getBounds if @_==1;
	TODO;
}

# загружает картинку из файла
sub load {
	my ($self, $path) = @_;
	$self = $self->clone;
	$path //= $self->{path};
	my $file = $app->file($path)->encode(undef);
	my $f = $file->read;
	
	$self->{canvas} = 
		# 0x89 0x50 0x4E 0x47 0x0D 0x0A 0x1A 0x0A
		$f =~ /^ \x89 PNG /ax? GD::Image->newFromPngData($f):
		$f =~ /^(
			\xFF \xD8 \xFF \xDB | 
			\xFF \xD8 \xFF \xE0 . . \x4A \x46 \x49 \x46 \x00 \x01 |
			\xFF \xD8 \xFF \xE1 . . \x45 \x78 \x69 \x66 \x00 \x00
		)/asxn? GD::Image->newFromJpegData($f):
		# GIF87a GIF89a
		$f =~ /^GIF/a? GD::Image->newFromGifData($f):
		#$f =~ /^BM/a? GD::Image->newFromWBMPData($f):
		#$f =~ /^BM/a? GD::Image->newFromXbmData($f):
		die "магическое число файла изображения не распознано. Файл: $path. 6 первых символов файла: " . 
			join " ", map {sprintf "%X%s", ord($_), (/^[[:print:]]\z/? "($_)": "") } 
				$f =~ /^(.)(.)(.)(.)(.)(.)/as;
	$self->{path} = $path;
	$self
}

# сохраняет картинку в файл с указанным путём
sub save {
	my ($self, $path, $compress) = @_;
	my $file = $app->file(defined($path)? ($self->{path} = $path): $self->{path})->encode(undef)->mkpath;
	my $mime = $app->mime->Type($file->ext);
	$compress //= 1;
	$file->write(
		$mime eq "image/gif"? $self->{canvas}->gif:
		$mime eq "image/png"? $self->{canvas}->png(int($compress * 9)):
		$mime eq "image/jpeg"? $self->{canvas}->jpeg(int($compress * 100)):
		die "mime `$mime` не распознан"
	);
	
	$self
}

# сохраняет с указанным типом
sub saveAs {
	my ($self, $ext, $quality) = @_;
	$self->save($app->file($self->path)->ext($ext)->path, $quality)
}

# ширина
sub width {
	my ($self) = @_;
	$self->{canvas}->width
}

# высота
sub height {
	my ($self) = @_;
	$self->{canvas}->height
}

#@@category изменения

my $I=0;
our %FILTER = map {$_=>$I++} qw( default  bell  bessel  bilinear-fixed  bicubic  bicubic-fixed  blackman  box  bspline  catmullrom  gaussian  generalized-cubic  hermite  hamming  hanning  mitchell  nearest-neighbour  power  quadratic  sinc  triangle  weighted4  linear ); 
# делает resize c указанным фильтром
sub resize {
	my ($self, %arg) = @_;
	
	my $filter = $arg{filter};
	$self->{canvas}->interpolationMethod(Num($filter)? $filter: $FILTER{lc $filter} // 0);
	
	my $width = $arg{width} // $self->calc_resize_width(height => $arg{height});
	my $height = $arg{height} // $self->calc_resize_height(width => $arg{width});
		
	$self->clone(canvas => scalar $self->{canvas}->copyScaleInterpolated($width, $height))
}

# делает поворот c указанным фильтром
sub rotate {
	my ($self, %arg) = @_;
	
	my $filter = $arg{filter};
	$self->{canvas}->interpolationMethod(Num($filter)? $filter: $FILTER{lc $filter} // 0);
		
	my $color = $arg{color} // $self->color("fff");
		
	# угол в градусах переводится в радианы
	my $angle = $arg{angle};
	
	# my ($angle, $ed) = $arg{angle} =~ /^(-?\d+)([a-z]*)$/;
	# $ed //= "grad";
	# $ed eq "grad"? $angle*$pi/180:
	# s/rad$//? :
	$self->clone(canvas => scalar $self->{canvas}->copyRotateInterpolated($arg{angle}, $color))
}

# копирует себя и полотно
sub copy {
	my ($self, %arg) = @_;
	return $self->clone(canvas => $self->{canvas}->clone) if @_==1;
	my $w = $arg{width} // $self->width - $arg{x};
	my $h = $arg{height} // $self->height - $arg{y};
	my $dst = $arg{dst} // $self->wh($w, $h);
	$dst->{canvas}->copy($self->{canvas}, $arg{dstX}, $arg{dstY}, $arg{x}, $arg{y}, $w, $h);
	$dst
}

# рассчитывает width к ширине height относительно w и h 
sub calc_resize_width {
	my ($self, %arg) = @_;
	die "не указана height" if !defined $arg{height};
	my $w = $arg{w} // $self->width;
	my $h = $arg{h} // $self->height; 
	round($arg{height} / $h * $w)
}

# рассчитывает width к ширине height относительно w и h 
sub calc_resize_height {
	my ($self, %arg) = @_;
	die "не указана width" if !defined $arg{width};
	my $w = $arg{w} // $self->width;
	my $h = $arg{h} // $self->height;
	round($arg{width} / $w * $h)
}


# копирует область  и изменяет размер
# результирующее полотно - dst или создаётся: newW на newH
# Примеры:
#  $img->resize(width=>40);
#  $img->resize(height=>40, dst=>$img2);
# При указанном только width или только height - сжимает пропорционально
sub q_resize {
	my ($self, %arg) = @_;
	
	my $width = $arg{width};
	my $height = $arg{height};
	
	my $w = $arg{w} // $self->width;
	my $h = $arg{h} // $self->height;
	# пропорционально
	$width //= round($height / $h * $w);
	$height //= round($width / $w * $h);
	
	my $dst = $arg{dst} // $self->wh($arg{dstX}+$width, $arg{dstY}+$height);
	# $image->copyResized($sourceImage,$dstX,$dstY,$srcX,$srcY,$destW,$destH,$srcW,$srcH)
	
	my $method = $arg{smooth}? "copyResampled": "copyResized";
	$dst->{canvas}->$method($self->{canvas}, $arg{dstX}, $arg{dstY}, $arg{x}, $arg{y}, $width, $height, $w, $h);
	
	$dst
}

# обычный ресайз - формирует main-картинку
sub resizing {
	my ($self, $max_width, $max_height) = @_;
	
	my $width = $self->width;
	my $height = $self->height;
	
	$height = round($max_width / $width * $height), $width = $max_width if $max_width < $width;
	#$self = $self->resize(width => $max_width) if $max_width < $width;

	$width = round($max_height / $height * $width), $height = $max_height if $max_height < $height;
	#$self = $self->resize(height => $max_height) if $max_height < $height;
	
	return $self->clone if $max_width == $width && $max_height == $height;
	$self->resize(width => $width, height => $height, filter=>"Gaussian")
}

# формирует превью: изменяет размер картинки к width
sub preview {
	my ($self, $side) = @_;
	
	my $width = $self->width;
	my $height = $self->height;
	my $srcSide = min($width, $height);
		
	$self->copy(
		x=>round(($width-$srcSide)/2),
		y=>round(($height-$srcSide)/2),
		width=>$srcSide,
		height=>$srcSide,
	)->resize(width=>$side,	height=>$side, filter=>"Gaussian")
}


#@@category палитра и цвета

sub color {
	my ($self, $color) = @_;
	my $i = length $color;
	my ($r, $g, $b, $alpha) = 
	$i==3? (hex(substr($color, 0, 1) x 2), hex(substr($color, 1, 2) x 2), hex(substr($color, 2, 3) x 2), 0):
	$i==4? (hex(substr($color, 0, 1) x 2), hex(substr($color, 1, 2) x 2), hex(substr($color, 2, 3) x 2), hex(substr($color, 3, 4) x 2)):
	$i==6? (hex(substr $color, 0, 2), hex(substr $color, 2, 4), hex(substr $color, 4, 6)):
	$i==8? (hex(substr $color, 0, 2), hex(substr $color, 2, 4), hex(substr $color, 4, 6), hex(substr $color, 6, 8)):
	die "используйте: color(rgb), color(rgba), color(rrggbb) и color(rrggbbаа)";
	
	$alpha? $self->canvas->colorAllocateAlpha($r, $g, $b, int($alpha/255*127)): $self->canvas->colorAllocate($r, $g, $b)
}






1;