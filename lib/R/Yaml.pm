package R::Yaml;
# text/yaml

use common::sense;
use R::App;

use YAML::Syck qw//;
use Types::Serialiser;

#$YAML::XS::Boolean = "JSON::PP";

$YAML::Syck::Headless = 1;
#Defaults to false. Setting this to a true value will make Dump omit the leading ---\n marker.

$YAML::Syck::SortKeys = 1;
#Defaults to false. Setting this to a true value will make Dump sort hash keys.

$YAML::Syck::SingleQuote = 0;
#Defaults to false. Setting this to a true value will make Dump always emit single quotes instead of bare strings.

$YAML::Syck::ImplicitTyping = 1;
#Defaults to false. Setting this to a true value will make Load recognize various implicit types in YAML, such as unquoted true, false, as well as integers and floating-point numbers. Otherwise, only ~ is recognized to be undef.

$YAML::Syck::ImplicitUnicode = 1;

$YAML::Syck::ImplicitBinary = 0;

$YAML::Syck::UseCode = $YAML::Syck::LoadCode = $YAML::Syck::DumpCode = 1;
$YAML::Syck::LoadBlessed = 1;

# конструктор
sub new {
	my ($cls) = @_;
	bless {
		linenum => 0,
	}, ref $cls || $cls;
}

# в строку
*stringify = \&to;
sub to {
	my ($self, $data) = @_;
	YAML::Syck::Dump($data);
}

# парсит строку
*parse = \&from;
sub from {
	my ($self, $scalar) = @_;
	$scalar =~ s!^( *\t)+! my $x=$&; $x =~ s|\t|    |g;  $x !gme;
	scalar YAML::Syck::Load($scalar)
}

# возвращает цветной yaml
sub color {
	my ($self, $data) = @_;
	require YAML::Tiny::Color;
	$YAML::Tiny::Color::LineNumber = $self->{linenum};
	my $res = YAML::Tiny::Color::Dump($data);
	$res =~ s/^---\n//;
	$res
}

# выводить номера строк в колоризированном выводе
sub linenum {
	my $self = shift;
	if(@_) {
		$self->{linenum} = shift;
		$self
	}
	else {
		$self->{linenum}
	}
}

1;