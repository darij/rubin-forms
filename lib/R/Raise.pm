package R::Raise;
# исключение фоматирует исключение


use common::sense;

#use Carp qw/shortmess longmess/;
use Scalar::Util qw/blessed looks_like_number/;

use Term::ANSIColor qw/colorstrip color colored/;


# use overload
	# '""' => \&stringify,
	# '.' => \&concat,
	# 'bool' => sub { 1 },
	# "0+" => sub { $_[0]->{msg}? 1: 0 },
	# "qr" => \&stringify,
	# fallback => 1
# ;


# устанавливает обработчики
my $__DIE__;
my $__WARN__;
sub setdie {
	$__DIE__ = $main::SIG{ __DIE__ } if $main::SIG{ __DIE__ } != \&__ondie__;
	$__WARN__ = $main::SIG{ __WARN__ } if $main::SIG{ __WARN__ } != \&__onwarn__;
	$main::SIG{ __DIE__ } = \&__ondie__;
	$main::SIG{ __WARN__ } = \&__onwarn__;
}

# возвращает как было
sub retdie {
	$main::SIG{ __DIE__ } = $__DIE__ // "DEFAULT";
	$main::SIG{ __WARN__ } = $__WARN__  // "DEFAULT";
}

# возвращает дефолтные
sub defdie {
	$main::SIG{ __DIE__ } = "DEFAULT";
	$main::SIG{ __WARN__ } = "DEFAULT";
}

# отключает хандлеры обработчиков на время
sub soft_eval {
	wantarray? do {
		&retdie;
		my @ret = eval @_;
		&setdie;
		@ret
	}: do {
		&retdie;
		my $ret = eval @_;
		&setdie;
		$ret
	}
}

if(defined $^S) {
	setdie();
}

# конструктор. Позволяет создавать исключение, с указанием страницы, которая будет выведена с ним
sub bingo {
	my ($cls, $abbr, $msg) = @_;
	my $self = $cls->new($msg, "bingo");
	$self->{abbr} = $abbr;
	$self
}

# конструктор
sub new {
	my $cls = shift;
	
	bless {
		who => "throw",		# error, warning, trace, throw, etc...
		msg => "throw",		# сообщение об ошибке
		#trace => undef, 	#&DB::__RAISE__TRACE__($arity),		# traceback - массив [[file, lineno, object]]...
		color => 1,			# колоризировать
		to_eval => 1,		# только до eval
		show_lines => 1,	# сколько строк показывать в стек-трейсе
		pid => $R::Process::SELF_PS && exists $R::Process::SELF_PS->{name} && length($R::Process::SELF_PS->{name})? $R::Process::SELF_PS->{name}: $$,
		cid => $Coro::current? "$Coro::current->{desc}": "",
		@_
	}, ref $cls || $cls;
}

# к msg добавляет трейс
sub stringify {
	my ($self) = @_;
	return $self->{msg} if $self->{arity} == -1;
	
	my $prompt = join "", color("cyan"), $self->{pid}, color("white"), "#", color("magenta"), $self->{cid}, " ", 
		color("red"), $self->{who}, color("white"), ":",
		color("reset"), " ";
		
	my $msg = $self->{msg};
	
	join "", #$msg !~ /^\Q$prompt\E/? $prompt: (), 
		reverse($self->_color_trace),
		$prompt, $msg
}

# объединяет со строкой
sub concat {
	my ($self, $str, $wrap) = @_;
	if($wrap) { $str . $self->stringify }
	else { $self->stringify . $str }
}

# возвращает колоризированную строку трейса
# $arity - пропустить столько строк
# $to_eval - до eval
# $show_lines - захватить столько строк с одной и другой стороны
our %CACHE_LINES;
sub _color_trace {
	my ($self) = @_;
	my @trace; my $go = 1;
	for(my $i=$self->{arity} // 0; $go; $i++) {
		push(@trace, "Error Trace \$i>100"), last if $i>100;
	
		#        0         1          2      3            4
		# my ($package, $filename, $line, $subroutine, $hasargs,
		#     5          6          7            8       9         10
		# $wantarray, $evaltext, $is_require, $hints, $bitmask, $hinthash)
	
		my @s = caller($i);
		last if !@s;
		
		my ($pkg, $path, $lineno, $sub) = @s;
		
		$go = 0 if $self->{to_eval} && $sub eq "(eval)";
		
		my ($pkg1, $path1, $lineno1) = caller($i-1);
		my $key = "$path:$lineno";
		
		my $f;
		my $line = $path1 eq $path && $lineno1 == $lineno? "":
		($CACHE_LINES{$key} //= $path =~ /^\(eval \d+\)\z/? "": open($f, "<:utf8", $path)? do {
			my $from=$lineno-$self->{show_lines};
			$from = 1 if $from < 1;
			my $n = $lineno+$self->{show_lines}+1;
			my @line;
			for(my $i=1; $i<$n && defined(my $x=<$f>); $i++) { $x=~s!\t!    !g, push @line, '    ', colored($i, "red"), ' ', $x if $i>=$from }
			close $f;
			join "", @line;
		}: join "", "файл не открыт. Причина: ", colored("$!", "on_red white"), "\n");
		#$sub =~ s/^\Q${pkg}::\E/~/;
		$sub =~ s/::([^:]+)\z/.$1/;
		push @trace, "    ${\colored(\"$key    $sub\", 'bold black')}\n$line";
	}
	return @trace;
}

# обработчик события die
sub __ondie__ {
	my ($msg) = @_;
	#print STDERR "@$ S==$^S args=".@_." \@_==@_ GLOBAL_PHASE=${^GLOBAL_PHASE}\n";
	
	# ${^GLOBAL_PHASE}: CONSTRUCT, START, CHECK, INIT, RUN, END, DESTRUCT
	
	# $EXCEPTIONS_BEING_CAUGHT == $^S
	# undef - Parsing module, eval, or main program
	# 1 - in eval
	# 0 - Otherwise
	# 5 - деструктор
	
	my $in_eval = $^S == 1;
	if($in_eval) {
		$msg = __PACKAGE__->new(msg=>$msg, who=>"error", arity=>2, to_eval=>1)->stringify if !ref $msg;
		die $msg;
	}

	# исключение пойдёт на STDERR
	$msg = ref $msg? $msg: __PACKAGE__->new(msg=>$msg, who=>"error", arity=>2, to_eval=>0)->stringify;
	#print STDERR "hi!----$^S $@\n";
	die $msg if $^S <= 1;
	
	# попытка выбросить исключение из деструктора
	# if($Coro::current && ref $Coro::current ne "HASH") {
		# AE::timer(0, 0, sub { $Coro::current->throw($msg); });	# срабатывает после деструктора
		# die $msg; # прерываем деструктор
	# }
	
	#print STDERR "RAISE TO LOG\n";
	
	_log($msg);
	
	# прерываем деструктор
	die $msg;
	
	# # если это волокно - завершаем его
	# if($Coro::current && ref $Coro::current ne "HASH") {
		# print STDERR "Terminate $Coro::current->{desc}\n";
		# Coro::terminate();
		# #$Coro::current->throw($msg);
	# }
	# else {
		# exit 5;
	# }
}

# обработчик события warn
sub __onwarn__ {
	my ($msg) = @_;
	
	# unless(blessed($msg) and $msg->isa('R::Raise')) {
		# $msg = __PACKAGE__->new( $msg, "warning", 3 );
	# }
	
	warn $msg;
	_log($msg) if $^S > 1 || $^S < 0; # не в eval и не как обычно
	#exit;
	die bless {msg=>$msg}, "R::Raise::Quit" if $_[0]=~/^Deep recursion on subroutine/;
}

# логирует
sub _log {
	my ($msg) = @_;
	
	print STDERR "RAISE _LOG:\n";
	
	if($^S == 5) {
		$msg = "$msg\nRAISE IN DESTRUCT($^S)\n";
	}
	elsif($^S != 0) {
		$msg = "$msg\n\$^S == $^S";
	}
	
	#print STDERR $msg;
	
	&retdie;
	eval {
		require R::App;
		$R::App::app->log->alert("$msg");
		#print STDERR $msg if !$R::App::app->log->{logs}{std};
	};
	print STDERR $msg if $@;
	&setdie;
}

# возвращает трейсбэк
sub trace {
	my ($self) = @_;
		
	__PACKAGE__->new(msg=>"app.raise.trace", who=>"trace", to_eval=>0)->stringify;
}

# # последняя ошибка для получения стек-трейса
# sub top_last {
	# $Coro::current->{RAISE_LAST}
# }

# # удаляет последнюю ошибку для получения стек-трейса и возвращает её
# sub last {
	# delete $Coro::current->{RAISE_LAST}
# }

# # передаёт ошибку полученную в eval дальше:
# # eval "..."; или do "...";
# # $app->raise->pass if $@;
# sub pass {
	# die delete $Coro::current->{RAISE_LAST};
# }

# # выбрасывает исключение
# sub eject {
	# my ($self, $error) = @_;
	# die $error // ();
# }


# # возвращает аргументы предпредпоследнего вызова
# package DB {
	# our @args;
	# # нужно использовать с функцией 2-й вложенности или задать вложенность
	# #@deprecated
	# sub __RAISE__TRACE__ {
		# my ($arity) = @_;
		# return if $arity < 0;
		# my $trace = [];
		# for(my $i=$arity // 0;; $i++) {
		
			# #        0         1          2      3            4
			# # my ($package, $filename, $line, $subroutine, $hasargs,
			# #     5          6          7            8       9         10
			# # $wantarray, $evaltext, $is_require, $hints, $bitmask, $hinthash)
		
			# my $args = [ caller($i) ];
			# last if !@$args;
			# push(@$trace, "{err-pkg}", "{err-file}", 0, "Error Trace \$i>100"), last if $i>100;
			# #eval { push @$args, [ @args ] };
			# #printf STDERR "%s %s %s\n", $args->[6]? 1:0, $args->[0], $args->[3];#$R::app->perl->dump($args);
			# push @$args, $args->[4]? [ @args ]: [];
			# push @$trace, $args;
		# }
		# return $trace;
	# }
# }


# # возвращает пустую строку
# sub empty { "" }

# # 
# sub _tostring {
	# my ($x) = @_;
	# $x =~ s/\t/\\t/g;
	# $x =~ s/\n/\\n/g;
	# "\"$x\""
# }

# # обращает в строку _toargs
# sub _toargs {
	# my ($args) = @_;
	# my $chars = 100;
	# join(", ", map { 
		# my $x = $_;
		# $x = looks_like_number($x)? $x: !ref($x)? _tostring($x): 
			# #eval { $R::App::app->perl->dump($x) } //
			# #eval { ref($x)."<".$x->REPR.">" } //
				# sprintf("%s=%X", ref($x), int($x));
		# $x = substr($x, 0, $chars) . "…" if length($x) > $chars;
		# $x
	# } @$args)
# }

# # стрингифицирует только трейс
# sub stringify_trace {
	# my ($self) = @_;
	
	# my $winpaths = !$ENV{SHLVL};
	
	# my @res;
	
	# for my $trace (reverse @{$self->{trace}}) {
		# my ($package, $filename, $line, $subroutine, $hasargs, $wantarray, $evaltext, $is_require, $hints, $bitmask, $hinthash, $args) = @$trace;
		
		# my $arguments = $hasargs? "("._toargs($args).")": "";
		
		# $filename =~ s!^/cygdrive/(\w+)/!$1:!, $filename =~ s!/!\\!g if $winpaths;
		
		# $subroutine = "~" if $subroutine eq "__ANON__";
		
		# my $pack = quotemeta $package;
		# $subroutine =~ s/^$pack//;
		
		# $subroutine =~ s/::(\w+)$/.$1/;
		
		# push @res, "\t$filename:$line\t$subroutine$arguments\n";
	# }
	
	# join "", @res
# }

# # превращает в строку
# sub stringify {
	# my $self = shift;

	# #return $Formatter->format($self) if $Formatter;
	
	# $self->defdie;
	
	# my $msg = $self->{msg};
	# if( ref $msg ) {
		# if( Scalar::Util::blessed($msg) ) {
			# my $x = eval { "$msg" };
			# # такие как ARRAY(0x6007d3400) не пройдут, а стрингифицировавшиеся объекты - да
			# if(defined $x and $x !~ /^[A-Z_]\w*\(0x[\dA-F]+\)$/i) {
				# $msg = $x;
			# } else {
				# $msg = eval { $R::App::app->perl->inline_dump($msg, 10_000) } // eval { "$@" };
			# }
		# }
		# else {
			# $msg = eval { $R::App::app->perl->inline_dump($msg, 10_000) } // eval { "$msg" } // eval { "$@" };
		# }
	# }
	
	# my $res = $self->{who} eq "quit"? "$msg\n":
		# $self->stringify_trace . "$self->{pid}#$self->{cid} $self->{who}: $msg\n";
	
	# $self->setdie;
	
	# $res
	
	
	# # $_ = $self->{msg};
	# # $_ = ref($_) eq "SCALAR"? $$_: $_;
	# # #$_ = (utf8::is_utf8($_)?"utf8":"no")."$_\n";
	# # #s/\\x\{(\w+)\}/ chr hex $1 /ge;
	
	# # # eval {
		# # # require R::App;
		# # # $R::App::app->file("var/last.raise.trace")->write($R::App::app->perl->dumper($self));
	# # # };
	# # # return "$self->{who}: $self->{orig}\n" if !$@;
	
	# # #print STDERR "XXXXX: ".$self->{who}." $_\n\n";
	
	# # my @lines = split /\n/, $_;
	
	# # my $color = $self->{who} eq "bingo"? "cyan": $self->{who} eq "warning"? "yellow": "red";
	
    # # #R::App::msg1(-t STDERR);
    # # # -t STDERR;
    # # my $cc = 1? \&color: \&empty;
    
	# # my $who = $cc->($color) . $self->{who} . ": " . $cc->("reset");
	
	# # for(@lines) {
		# # s{^\t?(.*?)(?: called)? at (.*?) line (\d+)}{
			# # my ($x, $f, $l)=($1, $2, $3);
			
			# # $x=~s{^\w+::(?:\w+::)+}{
				# # my $y = my $tmp = $&;
				# # $y =~ s!::!/!g; $y =~ s!/$!!;
				# # $f =~ m!\b$y\.pm$! ? "": $tmp
			# # }e;
			# # $x=~s![:\(\)]+!$cc->("cyan") . $& . $cc->("reset")!ge;
            
            # # $f =~ s!/cygdrive/(\w)!$1:!, $f =~ s!/!\\!g if $cc == \&empty;
            
			# # "$f:".$cc->("green")."$l".$cc->("reset").": $who$x"
		# # }e;
		
		# # $who = "";
	# # }
	
	# # unshift @lines, $R::App::app->{connect}{CURR_SQL} if $R::App::app && $R::App::app->{connect} && $R::App::app->{connect}{CURR_SQL};
	
	# # join("\n", reverse @lines) . "\n";
# }

# # Возвращает оригинальную ошибку
# sub orig {
	# my ($self) = @_;
	# $self->{msg}
# }

# # возвращает сообщение без at line
# sub message {
	# my ($self) = @_;
	# ref $self->{msg}? $self->{msg}{message}: do { $self->{msg}=~ / at .* line \d+.*$/? $`: $self->{msg}  }
# }

# # # добавляет в начало сообщения
# # sub messageAdd {
	# # my ($self, $add) = @_;
	# # $self->{orig} = join "", $add, "\n", $self->{orig};
	# # $self
# # }

# # сообщает, что аббревиатура совпадает
# sub is {
	# my ($self, $abbr) = @_;
	# $self->{abbr} && $self->{abbr} eq $abbr
# }

# # возвращает строку
# sub tracex {
	# my ($self, $count) = @_;
	# $self->trace($count)->stringify
# }

# # очищает трейсбак
# sub clear {
	# my ($self) = @_;
	
	# @{$self->{trace}} = ();
	
	# $self
# }

1;