package R::Model;
# менеджер моделей
# app->model->mymodel

use common::sense;
use R::App qw//;

# конструктор
sub new {
	my ($cls, %param) = @_;
	my $new_cls = "R::Model::" . $param{meta}->name;
	@{"${new_cls}::ISA"} = ref $cls || $cls;
	bless {%param}, $new_cls;
}


# возвращает модель
sub AUTOLOAD {
	my ($self) = @_;
	
	our $AUTOLOAD =~ /([^:]+)$/;
	my $prop = $1;

	my $meta = $self->{meta};
		
	my $fieldset = $meta->exists($prop);
	die "нет модели `$prop`" if !$fieldset;
	my $cls_row = $fieldset->{cls_row};
	my $cls_rowset = $fieldset->{cls_rowset};

	my $eval = "sub $AUTOLOAD { \@_>1? $cls_row->new(\@_[1..\$#_]): wantarray? $cls_rowset->new->_rows: $cls_rowset->new }";
	eval $eval;
	die "model autoload `$eval`: $@\ncls_row=$cls_row\ncls_rowset=$cls_rowset" if $@;
	my $sub = *{$AUTOLOAD}{CODE};
	
	goto &$sub;
}

sub DESTROY {}


1;