package R::Coro;
# коллекции волокон

use base R::Collection;

use common::sense;
use R::App;

BEGIN {
	die "Coro отключён в этом процессе" if $R::Coro::DISABLED;

	# устанавливаем модель в AnyEvent
	if(my $model = $app->ini->{coro}{AnyEventModel}) {
		my $f=$app->file(@INC)->sub("AnyEvent.pm")->existing;
		$f->read =~ m!^our\s*\@models\s*=\s*\( (.*?) ^\); !smx;
		our @models = eval $1;
		die if $@;
		@AnyEvent::REGISTRY = grep $_->[0] eq $model, @models; 
		@AnyEvent::REGISTRY or die "$model not found in \@AnyEvent::models";
	}
	#@AnyEvent::REGISTRY = [AnyEvent::Loop:: => AnyEvent::Impl::Perl::];
	#@AnyEvent::REGISTRY = [Event:: => AnyEvent::Impl::Event::];
	
	# sub coro_handle_open {
		
	# }
	
	# *CORE::GLOBAL::open = \&coro_handle_open;
	
	# перекрыть STDIN, STDOUT
};

use Coro qw//;
use Coro::AnyEvent qw//;

BEGIN {
	my $x = AnyEvent::detect();
	$x =~ s!.*:!!;
	msg ":cyan space", "Coro", ":magenta", "start", ":cyan space", "AnyEvent model is", ":magenta", $x;
}

#use Coro::EV qw//;
use Coro::State qw//;
#use Coro::Timer qw//;
use Coro::Handle qw//;
use Coro::Signal qw//;
use Coro::Semaphore qw//;
#use Coro::Specific qw//;
use Coro::LWP qw//; #- не нужен
# делает LWP::UserAgent коронным: нужно выбирать тот, который не глючит. Кроме того он ускоряет выполнение запросов
if(!exists $app->ini->{coro}{patch}) {
	require LWP::Protocol::Coro::http;
}
elsif($app->ini->{coro}{patch} eq "http") {
	require LWP::Protocol::Coro::http;
}
elsif($app->ini->{coro}{patch} eq "all") {
	require LWP::Protocol::Coro::http;
	require Coro::PatchSet;
}
elsif($app->ini->{coro}{patch} eq "set") {
	require Coro::PatchSet;
}
elsif($app->ini->{coro}{patch} eq "none") {
}
else {
	die "unresolved option coro.patch=$app->ini->{coro}{patch}";
}

# переопределяем sleep
$R::__sleep = sub {
	my ($sleep) = @_;
	#return Coro::Timer::sleep($sleep);
	my $time = Time::HiRes::time();
	my $timer = AE::timer( $sleep, 0, Coro::rouse_cb() );
	Coro::rouse_wait();
	Time::HiRes::time() - $time
};


# дополнительная информация к волокнам
our %CORO;

# при перезагрузке - сохраняем
sub HOTSWAP_REMOVE {
	my ($cls, $hotswap, $from_reload) = @_;
	$hotswap->{STASH}{$cls} = {%CORO};
}

# при перезагрузке - восстанавливаем
sub HOTSWAP_LOAD {
	my ($cls, $hotswap, $service) = @_;
	%CORO = %{$hotswap->{STASH}{$cls}};
}

$app->raise->setdie;	# восстанавливаем хэндлер в главном coro-волокне

$Coro::idle->{desc} //= "[idle]";

# AnyEvent перезаписывает %SIG, поэтому - перезапишем обработчики сигналов повторно:

$SIG{PIPE} = "IGNORE";
$SIG{INT} = sub {
	$app->coro->psapp->kill;
	
	$R::App::sigint->(@_);
};

# при выходе джойним
END {
	
	my $coro = $Coro::current->{desc} || "$Coro::current";
	my $ps = $app->process->name || $app->process->pid;
	print STDERR "$ps#[$coro] CORO END!!!\n";
		
	#$app->coro->lis;
	#$app->coro->ps->say;
	if($Coro::current != $Coro::idle) {
	
		for(;;) {
			my $dynamic = $app->coro->psapp->exme->dynamic;
			last if $dynamic->length==0;
			msg1 "ожидаем:", $dynamic->map(sub { $_->name });
			$dynamic->wait;
			Coro::cede();
		}
		
	}
}


# my $siint;
# my $exit;
# $siint = AE::signal INT => sub {
	# $exit = 1;
	# my $coro = $Coro::current->{desc} || "$Coro::current";
	# my $ps = $app->process->name || $app->process->pid;
	# print STDERR "$ps#[$coro] ae INT!!!\n";
	# #exit;
	# $app->coro->psapp->exme->kill;
	# #$app->coro->ps->exme->safekill;
	# #exit;
	# #__coro("[exit]" => sub {
		# #$app->coro->psapp->exme->safekill;
	# exit;
	# #});
	# #Coro::cede();
# };

# my $sipipe;
# $sipipe = AE::signal PIPE => sub {};


# # при выходе джойним
# END {
	
	# my $coro = $Coro::current->{desc} || "$Coro::current";
	# my $ps = $app->process->name || $app->process->pid;
	
	# if(!$exit && $Coro::current != $Coro::idle) {
		# Coro::cede();
		
		# print STDERR "$ps#[$coro] ae END!!!\n";
		
		# #$app->coro->lis;
		# #$app->coro->ps->say;
		
		# msg("ожидаем:", $app->coro->psapp->dynamic->map(sub { $_->name })), Coro::cede(), sleep 1 while $app->coro->psapp->exme->dynamic->wait->psapp->exme->dynamic->length; 
	# }
	# else {
		# print STDERR "$ps#[$coro] ae EXIT!!!\n";
	# }
	
	# undef $siint;
	# undef $sipipe;
# }

# асинхронная очередь
my $__async_number = 0;
sub __async (&;@) {
	unshift @_, "[async-" . ($__async_number++) . "]";
	goto &__coro;
}

# именованный коро
sub __coro ($&;@) {
	my ($name, $sub, @args) = @_;
	$app->coro($name => $sub)->args([@args])->ready
}

# возвращает все асинхронные
sub psasync {
	my ($self) = @_;
	$self->ps->grep(sub { $_->{desc} =~ /^\[async-\d+\]$/ })
}

our %EXPORT = (
	async => \&__async,
	coro => \&__coro,
	enter => \&Coro::on_enter,
	leave => \&Coro::on_leave,
	cede => \&Coro::cede,
	me => sub () { $app->coro->me },
);

# импортируем
sub import {
	my $self = shift;
	my $caller = caller;
	
	for my $name ( @_ ? @_ : keys %EXPORT ) {
        my $v = $EXPORT{$name};
        die "нет такого имени `$name`" if !defined $v;
        *{"${caller}::$name"} = $v;
    }
	$self
}

# запуск процесса
sub _handle {
	#msg1 "coro name: ".$Coro::current->{desc}." start";
	#msg1 ":black bold", "DETECT", ":red", "AnyEvent!!!" if $AnyEvent::MODEL; 
	
	$app->raise->setdie;
	
	my $desc = $Coro::current->{desc};
	
	my $info = $CORO{$desc};
	my $enter = $info->{enter};
	my $leave = $info->{leave};
	
	# могут устанавливаться сколько угодно раз
	Coro::on_enter { $enter->() } if $enter;
	Coro::on_leave { $leave->() } if $leave;
	
	#Coro::on_enter { msg ":red", "enter", ":green", $desc };
	#Coro::on_leave { msg ":magenta", "leave", ":cyan", $desc };
	
	my @result;
	
	do {
		@result = eval {
			#msg1 "init!";
			if($Coro::current->{return_to}) {
				$Coro::current->{return_to}->cede_to;
			} elsif($info->{immortal}) {
				Coro::cede();
			}
			else {
				die "Хорошая попытка инициализировать волокно $Coro::current->{desc} без ready"
			}
			#Coro::cede_notself();	# после инициализации перезапускаем
			#msg1 "in!";
			$info->{sub}->(@{$info->{args}}) 
		};

		if($@) {
			my $error = $@;
			return @{$error->{args}} if Isa $error, "R::Raise::Quit";
			
			my $catched;
			if(my $catch = $info->{catch}) {
				my $count = my $true = 0;
				for my $cat ( @$catch ) {
					if(!defined $cat->[0] or Isa $error, $cat->[0]) {
						$@ = $error;
						my $ret = eval { $cat->[1]->($error); 1 };
						$true++ if $ret;
						$count++;
					}
				}
				
				$catched = 1 if $true && $true == $count;
			}
			
			if(!$catched) {
				msg $app->raise(msg=>$error, who=>"coro::error")->stringify;
			}			
		}

	} while( $info->{immortal} );

	#msg1 "out!", @result;

	@result
}

# деструктор app-волокна
sub _destroy {
	my ($name) = @_;
	my $c = $CORO{$name};
	#msg1 "coro name: $name stop";
	$c->{destroy}->send($name) if $c->{destroy};
	%{$c->{coro}} = (desc => $name);	# очищаем память
	#delete $CORO{$name}; - такого не делать, т.к. исчезает
	#msg1 "coro name: $name stopped";
}

#@@category создание

# устанавливает Coro
sub new {
	my $cls = shift;
	
	my $parts = [];
	
	for(my $i=0; $i<@_; $i++) {
		my $name = $_[$i];
		die "app->coro->new(name=>sub...): нет name" if ref $name;
			
		my $sub = $_[$i+1];
		my $info = $CORO{$name};
		
		push(@$parts, $info->{coro}), next if $info && !ref $sub;
		
		die "Обработчик волокна $name уже установлен" if $info && !$info->{coro}->is_zombie;
		
		die "Требуется обработчик для волокна $name" if !ref $sub;
		
		my $c = Coro->new(\&_handle);
		$c->{desc} = $name;
		$c->on_destroy(closure $name, \&_destroy);
		
		%{$CORO{$name}} = (
			coro => $c,
			sub => $sub,
		);
		
		$i++;

		push @$parts, $CORO{$name}->{coro};
	}
	
	bless {
		parts => $parts,
	}, ref $cls || $cls;
}

# возвращает информационный объект к i-му
sub info {
	my ($self, $i) = @_;
	my $parts = $self->{parts};
	return undef if $i >= @$parts;
	$CORO{$parts->[$i]{desc}} //= {}
}

# возвращает имя
sub name {
	my ($self) = @_;
	$self->part->{desc}
}

# возвращает id
sub id {
	my ($self) = @_;
	int $self->part
}

# дублирует 1-й элемент n раз от 1..$n
sub dup {
	my ($self, $n) = @_;
	return $self->clone(parts=>[]) if !$self->length;
	my $name = $self->name;
	my $info = $self->info;
	
	my $r = $self->new(map { ("$name-$_" => $info->{sub}) } 1..$n)->then(sub {
		my $x = $_->info;
		%$x = (%$info, %$x);
	});
	$self->clone(parts => $r->{parts});
}

# устанавливает aргументы, возвращает аргументы для первого
sub args {
	my ($self, $args) = @_;
	return $self->info->{args} if @_ == 1;
	die "аргументы должны быть [...]" if ref $args ne "ARRAY";
	for my $e ( $self->slices ) { $e->info->{args} = $args }
	$self
}

# устанавливает/возвращает функцию, которая установится на гибель волокна в результате падения
sub destroy {
	my ($self, $code) = @_;
	return $self->info->{destroy} if @_ == 1;
	for my $e ( $self->parts ) {
		($CORO{$e->{desc}}{destroy} //= $app->signal->new)->cb( $code );
	}
	$self
}

# устанавливает/возвращает функцию, которая установится на вход в фибер
sub enter {
	my ($self, $code) = @_;
	return $self->info->{enter} if @_ == 1;
	my $i=0;
	for my $e ( $self->parts ) {
		$CORO{$e->{desc}}{enter} = $code;
	}
	$self
}

# устанавливает/возвращает функцию, которая установится на вход в фибер
sub leave {
	my ($self, $code) = @_;
	return $self->info->{leave} if @_ == 1;
	for my $e ( $self->parts ) {
		$CORO{$e->{desc}}{leave} = $code;
	}
	$self
}

# PRIO_MAX > PRIO_HIGH > PRIO_NORMAL > PRIO_LOW > PRIO_IDLE > PRIO_MIN
#    3    >     1     >      0      >    -1    >    -3     >    -4

# устанавливает/возвращает приоритет. Чем меньше - тем выше
sub nice {
	my $self = shift;
	
	return $self->length && $self->{parts}[0]->nice if !@_;
	
	for my $e ( $self->parts ) {
		$e->nice($_[0])
	}
	
	$self
}

# мощность процесса
sub power {
	my ($self, $power) = @_;
	$self->nice(-$power)
}

# устанавливает приоритет PRIO_MAX
sub finepower {
	my ($self) = @_;
	$self->nice(-3)
}

# устанавливает приоритет PRIO_HIGH
sub highpower {
	my ($self) = @_;
	$self->nice(-1)
}

# устанавливает нормальный приоритет
sub normpower {
	my ($self) = @_;
	$self->nice(0)
}

# устанавливает приоритет PRIO_LOW
sub lowpower {
	my ($self) = @_;
	$self->nice(1)
}

# устанавливает приоритет PRIO_IDLE
sub poorpower {
	my ($self) = @_;
	$self->nice(1)
}

# устанавливает приоритет PRIO_MIN
sub leastpower {
	my ($self) = @_;
	$self->nice(1)
}


# устанавливает код на ошибку
sub catch {
	my ($self, $type, $code) = @_;
	$code = $type, $type = undef if @_==2;
	
	for my $e ( $self->parts ) {
		push @{ $CORO{$e->{desc}}{catch} }, [$type, $code];
	}
	
	$self
}


#@@category управление


# переключает на каждый управление
sub cede {
	my ($self) = @_;
	
	for my $e ( $self->parts ) { Coro::cede_notself() }
	
	$self
}

# ожидает завершения всех волокон со статусом `ready` в очереди
sub wait {
	my ($self) = @_;
	
	# msg1 "wait step";
	# $self->say;
	# $app->coro->ps->say;
	# $app->coro->lis;
	# msg1 "wait step end";
	
	# пока все не завершаться
	while($self->zombie->length != $self->length) {
		Coro::cede_notself();
	}
		
	$self
}

# джойнит процессы в очереди и выдаёт их ответы
sub join {
	my ($self) = @_;
	local $_;
	map { $_->join } $self->parts
}

# примыкает волокна к текущему
sub adjoin {
	my ($self) = @_;
	$self->exme->join
}

# запускает волокна
*run = \&ready;
sub ready {
	my ($self) = @_;
	
	for my $e ( $self->parts ) {
		#$e->ready;
		#msg1 $e->{desc}, "cede_to";
		local $e->{return_to} = $Coro::current;
		$e->cede_to;
		#msg1 $e->{desc}, "cede_to-end";
	}
	
	$self
}

# перезапускает волокна
sub restart {
	my ($self) = @_;
	
	my $new = $app->coro(map { $_->{desc} => $CORO{$_->{desc}}->{sub} } $self->parts);
	%$self = %$new;
	
	$self->ready
}

# запускает и отключает от join-ов
sub detach {
	...
}


# делает бессмертными волокна коллекции
sub immortal {
	my ($self) = @_;
	
	for my $e ( $self->parts ) {
		my $info = $CORO{$e->{desc}};
		$info->{immortal} = 1;
	}
	
	$self
}

# делает смертными волокна коллекции
sub mortal {
	my ($self) = @_;
	
	for my $e ( $self->parts ) {
		my $info = $CORO{$e->{desc}};
		$info->{immortal} = 0;
	}
	
	$self
}

# проверяет, что 1-й в коллекции - бесмерный
sub deathless {
	my ($self) = @_;
	$self->info->{immortal}
}


# приостанавливает волокна
sub suspend {
	my ($self) = @_;
	$_->suspend for $self->parts;
	$self	
}

# запускает приостановленные
sub resume {
	my ($self) = @_;
	$_->resume for $self->parts;
	$self	
}

# вызывает в каждом волокне процедуру
sub call {
	my ($self, $code) = @_;
	$_->call($code) for $self->parts;
	$self
}

# выполняет в каждом волокне eval
sub eval {
	my ($self, $code) = @_;
	$_->eval($code) for $self->parts;
	$self
}

# убивает все волокна
sub kill {
	my ($self, @args) = @_;
	
	$self->throw( $app->raiseQuit(args=>[@args]) );

	$self
}

# выбрасывает эксепшн
sub throw {
	my $self = shift;
	
	for my $e ($self->parts) {
		$e->throw(@_);		
	}
	
	# throw отложенный, поэтому:
	Coro::cede() for 1..0+$self->parts;

	$self
}


#@@category группы

# возвращает активные в очереди
my $_is_active = sub { 
	$_->is_ready && !$_->is_zombie && !$_->is_suspended || $_->is_running 
};
sub active {
	my ($self) = @_;
	$self->grep($_is_active)
}

# возвращает пассивные в очереди
sub passive {
	my ($self) = @_;
	$self->grep(sub { !$_is_active->() })
}

# возвращает те которые запущены, спят или в очереди
my $_is_dynamic = sub {
	!$_->is_zombie && !($_->is_new && !$_->is_ready)
};
sub dynamic {
	my ($self) = @_;
	$self->grep($_is_dynamic)
}

sub static {
	my ($self) = @_;
	$self->grep(sub { !$_is_dynamic->() })
}

# завершившиеся процессы
sub zombie {
	my ($self) = @_;
	$self->grep(sub { $_->is_zombie })
}


# возвращает все, кроме себя
sub exme {
	my ($self) = @_;
	$self->grep(sub { $_ != $Coro::current })
}

# возвращает текущий
sub me {
	my ($self) = @_;
	$self->one($Coro::current)
}

# возвращает главный
sub main {
	my ($self) = @_;
	$self->one($Coro::main)
}

# возвращает idle
sub idle {
	my ($self) = @_;
	$self->one($Coro::idle)
}

# все волокна
sub ps {
	my ($self) = @_;
	local $_;
	$self->clone(parts => [Coro::State::list()]);
}

# все волокна созданные через app->coro
sub psapp {
	my ($self) = @_;
	local $_;
	$self->clone(parts => [map {$_->{coro}} grep {$_->{sub}} values %CORO]);
}


#@@category статусы

# возвращает статус для первого в очереди
sub status {
	my ($self) = @_;
	return "!" if !$self->length;
	my $o = $self->{parts}[0];
	my @x;
	push @x, "N" if $o->is_new;
	push @x, "Q" if $o->is_ready;
	push @x, "R" if $o->is_running;
	push @x, "S" if $o->is_suspended;
	push @x, "T" if $o->is_traced;
	push @x, "C" if $o->has_cctx;
	push @x, "Z" if $o->is_zombie;
	join "", @x? @x: "-";
}

# возвращает информацию по всем короутинам
sub pstab {
	my ($self) = @_;
	join "\n", "\tSTATUS\tNAME", $self->map(sub { "\t".$_->status."\t".$_->name }), ""
}

# печатает информацию по всем короутинам
sub say {
	my ($self) = @_;
	print $self->pstab;
	$self
}

# возвращает листинг дебага
sub listing {
	require Coro::Debug;
	Coro::Debug::ps_listing();
}

# распечатывает листинг
sub lis {
	my ($self) = @_;
	print STDERR $self->listing;
	$self
}



#@@category сигналы и семафоры


# блокировка
# 	$sem->down
# 	my $x = guard { $sem->up }
# 	...
# 	undef $x
# --> 
#	my $x = $app->coro->lock;
#	undef $x
sub lock {
	my ($self, $sem) = @_;
	$sem //= Coro::Semaphore->new;
	$sem->down;
	Guard::guard { $sem->up if $sem };
}

# порождает семафор down-up
sub sem {
	Coro::Semaphore->new
}

# порождает сигнал wait-send
sub sig {
	Coro::Signal->new
}

# создаёт канал ввода-вывода put/get/size
sub fifo {
	my $self = shift;
	Coro::Channel->new(@_)
}

# порождает калбак
sub rouse {
	Coro::rouse_cb()
}

# "поспи": ожидает калбак
sub nap {
	my $self = shift;
	Coro::rouse_wait(@_)
}

# создаёт связанную с текущим потоком переменную
sub var {
	Coro::Specific->new
}

1;