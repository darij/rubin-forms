package R::Telemetry;
# телеметрия привязана к Coro-волокну. Установите метки и в конце запроса выведите report

use common::sense;
use R::App qw/:min/;
use Time::HiRes qw//;

# конструктор
sub new {
	my $cls = shift;
	bless {@_}, ref $cls || $cls;
}

# база телеметрии: "$mark -> $mark" => interval
my $TELEMETRY = {};

# метка
sub mark {
	my ($self, $mark) = @_;
	
	if(!$Coro::current->{telemetry}) {
	
		my $start = [Time::HiRes::gettimeofday()];
		my $telemetry = $Coro::current->{telemetry} = {
			mark => $mark,
			time => $start,
		};
				
		return $self;
	}
	
	$self->_collapse($Coro::current->{telemetry}, $mark)
}

# если мы не предполагаем использовать finish без error
# my $telemetry_point_defer = app->telemetry->defer("point");
sub defer {
	my ($self, $mark) = @_;
	$self->mark("$mark.start")->new(mark => $mark, finish => "finish");
}

# реперная точка с деструктором
# my $telemetry_point_defer = app->telemetry->start("point");
# ...
# $telemetry_point_defer->finish;
sub start {
	my ($self, $mark) = @_;
	$self->mark("$mark.start")->new(mark => $mark, finish => "error");
}

# закрывает start
sub finish {
	my ($self, $mark) = @_;
	$self->{finish} = "finish";
	$self
}

# срабатывает после
sub DESTROY {
	my ($self) = @_;
	return if !defined $self->{mark};
	
	$self->mark("$self->{mark}.$self->{finish}");
}

# прибавляет число к метке
sub _collapse {
	my ($self, $telemetry, $mark) = @_;
	
	my $old_time = $telemetry->{time};
	my $time = $telemetry->{time} = [Time::HiRes::gettimeofday()];
	
	my $old_mark = $telemetry->{mark};
	$telemetry->{mark} = $mark;
	
	my $label = "$old_mark -> $mark";
	my $x = $TELEMETRY->{$label} //= { label => $label };
	
	$x->{time} += Time::HiRes::tv_interval($old_time, $time);
	$x->{count}++;
	
	$self
}

# сводим
*collapse = \&nullify;
sub nullify {
	my ($self) = @_;
	
	my $time = 0;
	my $count = 0;
	
	my @telemetry = sort { $b->{time} <=> $a->{time} } values %$TELEMETRY;
	
	for my $x (@telemetry) {
		$time += $x->{time};
		$count += $x->{count};
	}
	
	for my $x (@telemetry) {
		$x->{percent} = sprintf "%.2f", $x->{time} / $time * 100;
	}
	
	return {telemetry => \@telemetry, time => $time, count => $count};
}

# отчёт
sub report {
	my ($self) = @_;
	
	my $x = $self->nullify;
	
	 join "",
		map({ sprintf "%8s  %.4fs  %5.2f%%  %s\n", $_->{count}, $_->{time}, $_->{percent}, $_->{label} } @{$x->{telemetry}}),
		sprintf("Total time: %.4fs\n", $x->{time});
}

1;