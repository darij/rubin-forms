package R::Framework;
# путь к фреймворку

use common::sense;
use R::App;

has_const qw/name root/;

# конструктор
sub new {
	my $cls = shift;
	bless {
		name => "rubin-forms",	# имя фреймворка
		root => $app->file(__FILE__)->file("")->sub("/../../")->abs->path,	# путь к директории фреймвока
		@_
	}, ref $cls || $cls;
}

# возвращает абсолютный путь от рута
sub path {
	my $self = shift;
	return $self->{root} if !@_;
	my $root = $self->{root};
	return "$root/$_[0]" if !wantarray;
	map { "$root/$_" } @_
}

# возвращает файлы для путей от корня проекта
sub file {
	my $self = shift;
	$app->file($self->path(@_))
}


1;