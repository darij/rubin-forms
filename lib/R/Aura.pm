package R::Aura;
# сервер виджетов

use base R::Fulminant;

use common::sense;
use R::App;

# свойства
has qw/dev base_dev/;



# конструктор
sub new {
	my $cls = shift;
	
	$cls->SUPER::new(
		dev => 0,		# если dev - то проверять время изменения
		#max_age => 60*60*24*365*20,	# 20 лет
		@_
	);
}

# вычисляет время которое прошло при выполнении блока
sub show {
	my ($self, $name => $code) = @_;
	
	$app->telemetry->mark("aura.$name");
	
	my $t = R::time();
	
	$app->log->info( ":cyan nonewline empty", $name, ":reset", " ..." );
	
	eval {
		$code->();
	};
	my $err = $@;
	
	my $interval = R::time() - $t;
	
	$self->{all_start_time} += $interval;
	
	my $dot = "." x (20 - length $name);
	$app->log->info( ":empty", $dot, ":white", " [ ", $err? (":red", " ERR "): (":green", " OK "), ":white", " ] ", ":reset", $app->perl->fsec($interval) );
	
	die $err if $err;
	
	$self
}

# запуск портала
sub run {
	my ($self) = @_;
	
	#my $js_file = "html/js/app-rubin-forms.js";
	#$self->show("js-файл" => sub { -e $js_file? $app->js->require($js_file): msg("not $js_file") });
	
	if(!defined $self->{init} or defined $self->{init} and $self->{init}) {
	
		$self->show("модели" => sub { $app->meta->load });
		$self->show("символы html" => sub { $app->sign });
		$self->show("спрайты" => sub { $app->sprite });
		my $css_file = "./html/css/style.pass";
		$self->show("стили" => sub { -e $css_file? require($css_file): msg("not $css_file") });
		$self->show("шаблоны" => sub { $app->view->load });
	
	}
			
	# пересканируем
	if($self->{dev}) {
		$self->show("scan+coffee" => sub { $app->hotswap->scan });
	}
	
	# в режиме сценариев во все волокна запросов будет устанавливаться один и тот же $dbh с включённой транзакцией
	if($self->{base_dev}) {
		my $dbh = $app->connect->_dbh;
		$app->connect->_acc_transaction( 1 );
		$dbh->{AutoCommit} = 0;
		$self->{_dbh} = $dbh;
		
		my $_dev_sem = $app->coro->sem;
		my $_prepare = $app->connect->can("_prepare");
	
		*R::Connect::_prepare = sub {
			my $sem = $app->connect->_dbh == $dbh? do { $_dev_sem->guard }: undef;
			# нельзя делать goto &$_prepare, т.к. сбросится guard
			$_prepare->(@_);
		};
		
		my $_close = $app->connect->can("_close");
		*R::Connect::_close = sub {
			my ($self, $name) = @_;
			my $me_dbh = $app->coro($name)->info->{coro}{$self};
			$_close->(@_) if $me_dbh != $dbh;
		};
	}
	
	$app->telemetry->mark("aura.run.start");
	
	# инициализируем сессию
	$self->show("сессия" => sub { $app->session });
	
	# открываем сокет
	$self->SUPER::run;
	
	$app->telemetry->mark("aura.run.finish");
	
	$self
}

# переопределяем импульс
sub impulse {
	my $self = shift;	
	
	if($self->{base_dev}) {
		$app->connect->_dbh($self->{_dbh});
		$app->connect->_acc_transaction( 256 );
	}
	
	my $keep_alive = eval { $self->SUPER::impulse(@_) };
	msg ":blue", "$@" if $@;
	
	# $Coro::current->{request} уже не существует. Он в local в impulse
		
	#msg1 $app->perl->dump($Coro::current);
	
	msg1 ":sep(: )", "остались запросы", $Coro::current->{_REQUEST} if keys %{$Coro::current->{_REQUEST}};
	msg1 ":sep(: )", "остались ответы", $Coro::current->{_RESPONSE} if keys %{$Coro::current->{_RESPONSE}};
	
	msg1 ":sep(: )", "остались шаблоны", $Coro::current->{_TEMPLATE} if keys %{$Coro::current->{_TEMPLATE}};
	
	msg1 ":sep(: )", "остались формы", $Coro::current->{_FORMS} if keys %{$Coro::current->{_FORMS}};
	msg1 ":sep(: )", "остались поля", $Coro::current->{_INPUTS} if keys %{$Coro::current->{_INPUTS}};
	
	msg1 ":sep(: )", "остались бобы", $Coro::current->{_ROW} if keys %{$Coro::current->{_ROW}};
	msg1 ":sep(: )", "остались наборы", $Coro::current->{_ROWSET} if keys %{$Coro::current->{_ROWSET}};
		
	return $keep_alive;
}

# рыцарь - обработчик запроса
sub ritter {
	my ($self, $request) = @_;

	my $path = $request->path;
	die [403, ["Content-Type" => "text/plain; charset=utf-8"], "обнаружено .. в пути $path"] if $path =~ m!/\.\.(/|$)!n;
	
	#my $host = $request->header->Host;
	#my $router = $self->{sites}{$host};
	#return [200, "<center>host: $host not found<hr>rubin</center>"] if !$router;
	
	# получаем лайоуты и ошибки, а если повезёт - то и виджет
	my ($view, $attr, $param) = $app->router->get($path);
	$request->{_attr} = $attr;
	#$request->{access} = $param;
	
	if($view) {
		$app->hotswap->scan if $self->{dev};
		
		return $view->dispatch;
	}

	# статика
	if($path =~ m!\.(\w+)\z!) {
		my $f = $app->file("html$path");
		return [404, ["Content-Type" => "text/plain; charset=utf-8"], "нет файла $path"] if !$f->exists;
		return $f;
	}
	
	return [404, ["Content-Type" => "text/plain; charset=utf-8"], "404 - нет $path"];
	
	
}

# находит и преобразует ответ
sub middleware {
	my ($self, $request, $response) = @_;
	
	my $status = $response->status;
	my $view = $app->view->{INC}{"error/$status.html"};
	$view = $app->view->{INC}{join "", "error/", substr($status, 0, 1), "xx.html"} if !defined $view;
	
	if(defined $view) {
		$app->hotswap->scan if $self->{dev};
		
		my $html = $view->{class}->dispatch(
			response => $response,
			status => $response->status,
			error => $response->data
		);
		$response = [$status, [], $html];
	}
	
	$response
}

1;