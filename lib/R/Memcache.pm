package R::Memcache;
# мемкеш на coro

use base AnyEvent::Memcached;

use common::sense;
use R::App;

use Coro;

# конструктор
sub new {
	my $cls = shift;
	
	my $self = $cls->SUPER::new(
		servers => ["127.0.0.1:11211"],
		debug   => 0,
		compress_threshold => 10000,
		namespace => $app->project->name . ":",
		@_
	);
	
	$self->{ttl} //= 60*60*24;
	
	$self
}

# если ключа нет - устанавливает
sub fetch {
	my ($self, $key, $ttl, $cb);
	if(@_ == 3) { ($self, $key, $cb) = @_; }
	else { ($self, $key, $ttl, $cb) = @_; }
	
	my $res = $self->get($key);
	return $res if defined $res;
	
	$res = $cb->($key);
	$self->set($key, $res, $ttl) if defined $res;
	
	$res
}

# геттер
sub get {
	my ($self, $key) = @_;
	$self->SUPER::get($key, cb => Coro::rouse_cb);
	my ($rc, $err) = Coro::rouse_wait;
	die "not get($key): $err" if defined $err;
	#msg1 "get", $self->{namespace}, $key, $rc;
	$rc
}

# сеттер
sub set {
	my ($self, $key, $val, $ttl) = @_;
	
	$ttl = $self->{ttl} if @_ == 3;
	
	#msg1 "set", $self->{namespace}, $key, $val;
	
	$self->SUPER::set($key, $val, defined($ttl)? (expire => $ttl): (), cb => Coro::rouse_cb);
	my ($rc, $err) = Coro::rouse_wait;
	die "not set($key): $err" if !$rc;
	
	$self
}

# удаляет ключ
sub del {
	my ($self, $key) = @_;
	$self->SUPER::del($key, cb => Coro::rouse_cb);
	my ($rc, $err) = Coro::rouse_wait;
	die "not del($key): $err" if defined $err;
	#msg1 "get", $self->{namespace}, $key, $rc;
	$self
}

# устанавливает время на ключе
sub ttl {
	my ($self, $key, $ttl) = @_;
	my $x = $self->get($key);
	$self->set($key, $x, $ttl) if defined $x;
	$self
}

1;