package R::Url;
# коллекция урлов, позволяет надёргать страниц из интернета

use base R::Collection;

use common::sense;
use R::App qw/$app msg1 msg TODO Prefix/;

use LWP::UserAgent qw//;
use LWP::ConnCache qw//;
use HTTP::Cookies qw//;

sub _uri ($);

# конструктор
sub new {
	my $cls = shift;
	
	my $self = bless {
		method => "get",
		root => ".",							# куда сохранять
		agent => 'RubinForms-Agent',
		header => {},							# заголовки
		data => undef,							# post-параметры
		parts => [map {_uri $_} @_],			# url-ы
	}, ref $cls || $cls;
	
	$self
}

#@@category атрибуты коллекции

# устанавливает/возвращает строку агента
sub agent {
	my ($self, $agent) = @_;
	return $self->{agent} if @_ == 1;
	$self->{agent} = $agent;
	$self
}

# cookie("key" => "val", ) устанавливает куку для всех урлов, даже если они ведут на разные домены
# или возвращает куки для первого домена:
# 	app->url()->get->cookie
sub cookie {
	my ($self, $key, $val, %av) = @_;
	
	return $self->jar->get_cookies($self->url) if @_ == 1;
	
	$key = $app->mime->urlencoded->escape($key);
	
	return $app->mime->urlencoded->unescape( $self->jar->get_cookies($self->url)->{$key} ) if @_ == 2;
	
	my $version = $av{version};
	my $path = $av{path};
	my $domain = $av{domain};
	my $port = $av{port};
	my $path_spec = $av{path_spec};
	my $secure = $av{secure};
	my $maxage = $av{maxage};
	my $discard = $av{discard};
	
	my $jar = $self->jar;
	
	$jar->set_cookie(
		$version,
		$key => $app->mime->urlencoded->escape($val),
		$path // "/", 
		$domain // do { my $dom = $self->host; $dom eq "localhost"? "localhost.local": $dom },
		$port,
		$path_spec,
		$secure, 
		$maxage, 
		$discard, 
		\%av
	);
	
	$self
}

# устанавливает/возвращает cookie_jar
sub jar {
	my ($self, $cookie_jar) = @_;
	return $self->{cookie_jar} //= HTTP::Cookies->new if @_ == 1;
	$self->{cookie_jar} = $cookie_jar;
	$self
}

# устанавливает/возвращает cookie_jar
sub conn_cache {
	my ($self, $conn_cache) = @_;
	return $self->{conn_cache} //= LWP::ConnCache->new if @_ == 1;
	$self->{conn_cache} = $conn_cache;
	$self
}

# тестирует метод
my $RE_METHODS = qr/^(get|post|head|delete|put|patch|options)$/n;
sub __is_method($) {
	my $method = lc shift;
	die "метод $method не поддерживается" if $method !~ $RE_METHODS;
	$method
}

# устанавливает/возвращает метод
sub method {
	my ($self, $method) = @_;
	return $self->{method} if @_ == 1;
	$self->clone(method => __is_method $method)
}

# тестирует формат
my $RE_FORMATS = qr/^(json)$/n;
sub __is_format($) {
	my $format = lc shift;
	die "формат $format не поддерживается" if $format !~ $RE_FORMATS;
	$format
}

# устанавливает/возвращает формат отправляемых данных
sub format {
	my ($self, $format) = @_;
	return $self->{format} if @_ == 1;
	$self->clone(format => __is_format $format)
}

# устанавливает заголовки или возвращает заголовок
sub header {
	my $self = shift;
	
	if(@_==1) { return $self->res->header($_[0]) }
	
	for(my $i=0; $i<@_; $i+=2) {
		my ($k, $v) = @_[$i, $i+1];
		delete($self->{header}{$k}), next if !defined $v;
		$self->{header}{$k} = $v;
	}
	$self
}

# возвращает заголовки
sub headers {
	my ($self) = @_;
	+{ $self->res->headers->flatten }
}

# устанавливает данные
sub data {
	my $self = shift;
	if(@_ == 1) {
		$self->{data} = $_[0];
	} elsif(@_ == 0) {
		delete $self->{data};
	} else {
		$self->{data} = {@_};
	}
	$self
}

# устанавливает/возвращает таймауты на запросы 
sub timeout {
	my $self = shift;
	if(@_) {
		$self->{timeout} = shift;
		$self
	}
	else {
		$self->{timeout}
	}
}

# отключает редиректы
sub noredirect {
	my $self = shift;
	$self->{max_redirect} = 0;
	$self
}

#@@category download

# создаёт пользовательского агента
sub _ua {
	my ($self, $keep_alive) = @_;
	
	my $ua = LWP::UserAgent->new(
		agent => $self->{agent}, 
		($keep_alive? (keep_alive => 1): (conn_cache => 1)),
		ssl_opts => { verify_hostname => 1 },
		timeout => $self->{timeout} // 30,
	);
	
	if(!$keep_alive) {
		my $conn_cache = $self->conn_cache;
		$ua->conn_cache($conn_cache);
		$ua->conn_cache->total_capacity(undef);
	}
	
	$ua->cookie_jar($self->jar);
	$ua->max_redirect( $self->{max_redirect} // 7 );
	
	if($self->{handler}) {
		while(my ($handler, $arr) = each %{$self->{handler}}) {
			$ua->add_handler($handler, @$_) for @$arr;
		}
	}
	
	$ua
}

# формирует запрос
sub download {
	my ($self, $as_string) = @_;
	
	my $ua = $self->{keep} // $self->_ua;
	
	# $ua->add_handler(request_prepare => sub {
		# my($request, $ua, $handler) = @_;
		# msg1 "req:", $request->as_string, "end.";
		# undef
	# });
	
	my $method = __is_method $self->{method};
	
	my %header = (
		Accept => "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8",
		'Accept-Encoding' => 'gzip, deflate',
		'Accept-Language' => 'ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7',
		'User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36 OPR/58.0.3135.65',
		%{$self->{header}}
	);
	
	my $data = $self->{data};
	if(ref $data eq "HASH") {
		for my $k ( keys %$data ) {
			my $v = $data->{$k};
			if(R::Isa $v, "R::File") {
				$data->{$k} = [$v->path, $v->file, $app->mime->Type($v->ext)];
				$header{'Content-Type'} = 'multipart/form-data';
			}
		}
	}
	
	if($self->{format}) {
		if($self->{format} eq "json") {
			$header{'Content-Type'} = 'application/json; charset=utf-8';
			$data = $app->json->to($data);
			utf8::encode($data) if utf8::is_utf8($data);
		}
	}
	
	my @data = ref $data || length($data)? (Content => $data): ();
	my $get; if($method eq "get") {
		$get = $data;
		@data = ();
	}
	
	for my $p ( $self->parts ) {
		my $s = $self->one($p);

		if($get) { $s = $s->param(%{$s->param->All}, %$get); }
		
		if($as_string) {
			my $request = HTTP::Request->new(uc $method => $s->url, [%header], @data? $data[1]: ());
			return $ua->prepare_request($request);
		}
		
		$p->{response} = $ua->$method($s->url, %header, @data);
	}
	
	$self
}

# сохраняет на диск
sub save {
	my ($self, $root) = @_;
	
	$root //= $self->{root} // ".";
	my $f = $app->file($root);
	
	for my $e ( $self->slices ) {
		$f->sub( $e->path )->write( $e->res->decoded_content );
	}
	
	$self
}

# скачивает, если перед этим не было download и возвращает страницу для первого url-а
sub read {
	my ($self) = @_;
	$self->length && do { 
		my $x=$self->res; 
		return "url(".$self->url.")->read: ".$x->status_line if !$x->is_success && $x->content eq "";
		my $r=$x->decoded_content;
		$r
	}
}

# при клонировании сбрасываем и {response}
sub clone {
	my $self = shift;
	delete $_->{response} for @{$self->{parts}};
	$self->SUPER::clone(@_);
}

# Печатать запрос
sub show_request {
	my ($self) = @_;
	push @{$self->{handler}{request_prepare}}, [sub { print $_[0]->as_string }];
	$self
}

# добавляет обработчик на запрос
sub trigger {
	my ($self, $trigger, $sub, @match) = @_;
	
	my %trigger = qw/
	preprepare	request_preprepare
	prepare 	request_prepare
	send 		request_send
	header 		response_header
	data 		response_data
	done 		response_done
	redirect 	response_redirect
	/;
	
	push @{$self->{handler}{$trigger{$trigger}}}, [$sub, @match];
	
	$self
}

# бэйсик-авторизация
sub auth {
	my ($self, $login, $pass) = @_;
	$self->trigger(preprepare => sub { $_[0]->authorization_basic($login, $pass) });
	$self
}

# метод возвращает response для n-го урла
sub res {
	my ($self) = @_;
	$self->length && ($self->{parts}[0]->{response} // do {
		$self->conn_cache;	# чтобы остался
		$self->jar;			# чтобы остался
		$self->eq(0)->download->{parts}[0]{response}
	})
}

# возвращает статус
sub status {
	my ($self) = @_;
	$self->res->code
}

# возвращает текст статуса
sub reason {
	my ($self) = @_;
	$self->res->message
}

# возвращает заголовки ответа
sub http0 {
	my ($self) = @_;
	sprintf "%s %s\n%s", $self->status, $self->reason, $self->res->headers->as_string
}

# возвращает весь ответ
sub http {
	my ($self) = @_;
	$self->res->as_string
}

# возвращает контент
sub html {
	my ($self) = @_;
	$self->res->decoded_content
}

# неперекодированный контент
sub raw {
	my ($self) = @_;
	$self->res->content
}

# переводит в структуру perl
sub json {
	my ($self) = @_;
	my $html = $self->res->decoded_content;
	$app->json->from($html)
}

# возвращает кодировку документа
sub charset {
	my ($self) = @_;
	scalar $self->res->content_type_charset
}

# возвращает тип документа без кодировки
sub content_type {
	my ($self) = @_;
	my ($type) = $self->res->content_type_charset;
	$type
}


# возвращает объект R::Htmlquery для n-го урла
sub q {
	my ($self, $find) = @_;
	my $q = $app->htmlquery->new($self->res->decoded_content);
	$q = $q->find($find) if defined $find;
	$q
}

# создаёт keep-alive - подключение
sub keep {
	my ($self) = @_;
	$self->clone(keep => $self->_ua(1))
}

# получает урлы (R::Url или scalar) и формирует очередь
sub alive {
	my $self = shift;
	$self->clone(parts=>[])->add(@_)
}

# сохраняет урлы в root, парсит html-файлы и скачивает их
sub mirror {
	my ($self, $root) = @_;
	
	$root //= $self->{root};
	
	for my $e ( $self->download->slices ) {
		$e->{ua}->mirror("$e->{uri}", $app->file($self->one($e->{url})->path)->mkpath->path);
	}
	
	$self
}

# устанавливает функцию на процесс скачивания
sub progress {
	my ($self) = @_;
	TODO;
	$self
}

# печатает на вывод, как wget
sub wget {
	my ($self) = @_;
	TODO;
	$self
}

#@@category получение ресурса методом

# считывает страницу методом GET
sub GET { shift->get(@_)->read }
sub get {
	my $self = shift;
	$self->method("GET")->data(@_)
}

# считывает страницу методом POST
sub POST { shift->post(@_)->read }
sub post {
	my $self = shift;
	$self->method("POST")->data(@_)
}

# считывает страницу методом PUT
sub PUT { shift->put(@_)->read }
sub put {
	my $self = shift;
	$self->method("PUT")->data(@_)
}

# считывает страницу методом DELETE
sub DEL { shift->del(@_)->read }
sub del {
	my $self = shift;
	$self->method("DELETE")->data(@_)
}

# считывает страницу методом PATCH
sub PATCH { shift->patch(@_)->read }
sub patch {
	my $self = shift;
	$self->method("PATCH")->data(@_)
}

# считывает страницу методом OPTIONS
sub OPTIONS { shift->options(@_)->read }
sub options {
	my $self = shift;
	$self->method("OPTIONS")->data(@_)
}

# считывает страницу методом HEAD
sub HEAD { shift->head(@_)->read }
sub head {
	my $self = shift;
	$self->method("HEAD")->data(@_)
}


#@@category части урла

our %PORT_BY_SCHEME = (
	https => 443,
	ftp => 21,
	sftp => 22,
);


# парсер урла
sub _uri ($) {
	die "урл не распознан (`$_[0]`)" if $_[0] !~ m{
		^
		( (?<scheme> [^:\@/?#]+ ) : )??
		
		( // )?
		
		( 
		
			(
				(?<login> [^:\@/?#]+ )?
				( : (?<passwd> [^:\@/?#]+) )?
				\@
			)
						
			(?<host> [^:\@/?#]+ )?
			( : (?<port> \d+) )?
		
		|
		
			(
				(?<host> [^:\@/?#]+ )
				( : (?<port> \d+) )?
			)
			
			|
			
			: (?<port> \d+)
		
		)?
	
		
		(?<path> / [^?\#]* )?
		
		( \?
			(?<query> [^\#]* )
		)?
		
		( \#
			(?<fragment> .* )
		)?
		$
	}xn;
	
	#msg ":inline", $_[0], {%+};
	
	+{%+}
}

# возвращает адрес первого url или устанавливает для всех
sub url {
	my ($self, $url) = @_;
	if(@_ == 1) {
		return join "", $self->scheme, ":", $self->opaque, Prefix "#", $self->fragment;
	}
	$self->clone(parts => $self->new(($url) x scalar @{$self->{parts}})->{parts})
}

# возвращает протокол первого url или устанавливает для всех
sub scheme {
	my ($self, $scheme) = @_;
	return $self->length && ($self->{parts}[0]{scheme} || "http") if @_ == 1;
	$self->clone(parts => {scheme => $scheme})
}

# userinfo+domain
sub authority {
	my ($self, $authority) = @_;
	return $self->length && do { my $u=$self->userinfo; length($u)? "$u\@" . $self->domain: $self->domain } if @_ == 1;
	return $self->domain($authority) if $authority !~ /@/;
	my $userinfo = $`;
	my $domain = $';
	$self->userinfo($userinfo)->domain($domain)
}

# user+passwd
sub userinfo {
	my ($self, $userinfo) = @_;
	return $self->length && do {
		my $login = $self->login;
		my $passwd = $self->passwd;
		$passwd = ":$passwd" if defined $passwd;
		defined($login // $passwd)? $login.$passwd: $login // $passwd
	} if @_ == 1;
	die "url: не userinfo `$userinfo`" if $userinfo !~ m!^([^:/?#]*)(?::([^:/?#]))$!;
	my $login = $1;
	my $passwd = $2;
	$self->clone(parts => {login => $login, passwd => $passwd})
}

# возвращает логин первого url или устанавливает для всех
sub login {
	my ($self, $login) = @_;
	return $self->length && $self->{parts}[0]{login} if @_ == 1;
	$self->clone(parts => {login => $login})
}

# возвращает пароль первого url или устанавливает для всех
sub passwd {
	my ($self, $passwd) = @_;
	return $self->length && $self->{parts}[0]{passwd} if @_ == 1;
	$self->clone(parts => {passwd => $passwd})
}

# url без схемы и фрагмента
sub opaque {
	my ($self, $opaque) = @_;
	return $self->length && "//" . $self->authority . $self->path . Prefix "?", $self->query if @_ == 1;
	my $x = _uri $opaque;
	die "opaque: set scheme (`$opaque`)" if defined $x->{scheme};
	die "opaque: set fragment (`$opaque`)" if defined $x->{fragment};
	$self->clone(parts => $x)
}

# host+port
sub domain {
	my ($self, $domain) = @_;
	return $self->length && $self->host . (length($self->{parts}[0]{port})? ":$self->{parts}[0]{port}": "") if @_ == 1;
	my ($host, $port) = $domain =~ /:/? ($` => $'): ($domain => undef);
	$self->clone(parts => {host => $host, port => $port});
}

# возвращает протокол первого url или устанавливает для всех
sub host {
	my ($self, $host) = @_;
	return $self->length && do { my $x = $self->{parts}[0]{host}; length($x)? $x: "localhost" } if @_ == 1;
	$host = "localhost" if !length $host;
	$self->clone(parts => {host => $host})
}

# возвращает протокол первого url или устанавливает для всех
sub _port {
	my ($self, $port) = @_;
	return $self->length && $self->{parts}[0]{port} if @_ == 1;
	$self->clone(parts => {port => $port})
}

# возвращает порт или undef
sub port {
	my ($self, $port) = @_;
	return $self->length && do { my $x = $self->{parts}[0]{port}; length($x)? $x: $PORT_BY_SCHEME{ lc $self->{parts}[0]{scheme} } // 80 } if @_ == 1;
	$self->clone(parts => {port => $port})
}

# устанавливает/возвращает путь и query
sub location {
	my ($self, $location) = @_;
	return $self->length && join "?", $self->path, (length($self->query)? $self->query: ())  if @_==1;
	my $x = _uri $location;
	$self->clone(parts => {path => $x->{path}, query => $x->{query}})
}

# возвращает путь первого url или устанавливает для всех
sub path {
	my ($self, $path) = @_;
	return $self->length && $self->{parts}[0]{path} // "/" if @_ == 1;
	$path =~ s!^/?!/!;
	$self->clone(parts => {path => $path})
}

# возвращает/устанавливает строку после ?
sub query {
	my ($self, $query) = @_;
	return $self->length && $self->{parts}[0]{query} if @_ == 1;
	$self->clone(parts => {query => $query})
}

# возвращает/устанавливает параметры запроса после ?
sub param {
	my $self = shift;
	return $self->length && $app->rsgiBag($app->mime->urlencoded->from($self->{parts}[0]{query})) if @_ == 0;
	$self->query( $app->mime->urlencoded->to(\@_) );
}


# возвращает протокол первого url или устанавливает для всех
sub fragment {
	my ($self, $fragment) = @_;
	return $self->length && $self->{parts}[0]{fragment} if @_ == 1;
	$self->clone(parts => {fragment => $fragment})
}


1;