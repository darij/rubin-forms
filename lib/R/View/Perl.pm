package R::View::Perl;
# драйвер для перевода языка lukull в язык perl

use common::sense;
use R::App;

has qw/name view/;

# конструктор
sub new {
	my $cls = shift;
	my $self = bless {
		name => "perl",
		@_
	}, ref $cls || $cls;

	R::weaken $self->{view};
	
	$self
}

# заменяет спецсимволы в строке
sub escape_string {
	my ($self, $string) = @_;
	
	#msg "in", $string;
	
	$string =~ s/\\"/"/g;
	#$string =~ s/\\u([a-f\d]{1,4})/chr $1/ge;
	
	$string =~ s!\\(['"nrft])?!"\\" . ($1 || "\\")!xige;
	
	$string =~ s/["\$\@]/\\$&/g;
	
	#msg "out", $string;
	
	$string
}

# заменяет выражение в строке
sub replace_dollar {
	my ($self, $masking) = @_;
	"\${\\ $masking }"
}

# оборачивает строку в кавычки
sub to_string {
	my ($self, $string) = @_;
	"\"$string\""
}

# многострочный комментарий
sub multiline_comment {
	my ($self, $rem) = @_;
	$rem =~ s!^!#!gm;
	$rem
}

# конец строки, возможно с комментарием
sub endline {
	my ($self, $space, $rem, $endline) = @_;
	defined($rem)? "$space#$rem$endline": $endline
}

# конец выражения
sub sepexpression {
	";"
}

# конец выражения после then
sub sepexpression_after_then {
	return;
}

# встретилась регулярка
sub regexp_replace {
	my ($self, $regexp, $to, $args) = @_;
	$regexp = $self->view->replace_dollar_only($regexp);
	$to = $self->view->replace_dollar_only($to);
	"s{$regexp}{$to}$args"
}


# встретилась регулярка
sub regexp {
	my ($self, $regexp, $args) = @_;
	$regexp = $self->view->replace_dollar_only($regexp);
	"qr{$regexp}$args"
}

# специальная переменная perl
sub special_var {
	my ($self, $special_var) = @_;
	"\$$special_var"
}

# вспомогательный метод для скобок
sub _sk {
	my ($name, $sk) = @_;
	return $name if !defined $sk;
	return "$name(", ")" if $sk eq "(";
	return "$name->\[", "]" if $sk eq "[";
	return "$name->\{", "}";
}

# встретилось имя класса
sub classname {
	my ($self, $name, $sk) = @_;
	_sk($name, $sk);
}

# встретилась переменная
sub var {
	my ($self, $name, $sk) = @_;
	
	return _sk("\$self", $sk) if $name =~ /^(self|this|me)\z/i;
	
	_sk("\$DATA->{'$name'}", $sk);
}

# вызов метода по ссылке
sub dotref {
	my ($self, $name, $sk) = @_;
	_sk("->\${\\\$DATA->{'$name'}}", $sk)
}

# вызов метода
sub dot {
	my ($self, $name, $sk) = @_;
	_sk("->$name", $sk)
}

# по ключу хеша
sub colon {
	my ($self, $name, $sk) = @_;
	_sk("->{'$name'}", $sk)
}

# разыменование класса
sub twocolon {
	my ($self, $name, $sk) = @_;
	_sk("::$name", $sk)
}

# доступ к реальной переменной
my %CloseTag = qw/ ( ) { } [ ] /;
sub dollar {
	my ($self, $name, $sk) = @_;
	"\$$name$sk", ($sk? $CloseTag{$sk}: ())
}

# встретился ключ
sub key {
	my ($self, $key) = @_;
	"$key => scalar "
}

# встретилась долгая запятая
sub fat_comma {
	"=> scalar "
}

# встретилось ключевое слово
sub word {
	my ($self, $word) = @_;
	lc $word
}

# встретилось ключевое слово
sub unary {
	my ($self, $word) = @_;
	$self->gosub(lc $word);
}

# встретился указатель на себя
sub self {
	'$self'
}

# встретилась специальная переменная app
sub appvar {
	'$R::App::app'
}

# встретилась специальная переменная q
sub q {
	'$Coro::current->{request}'
}

# встретилась специальная переменная user
sub user {
	'$Coro::current->{request}->user'
}

# встретился цикл while
sub while {
	return "while(", ") {", "}";
}

# встретился цикл repeat
sub repeat {
	"do {"
}

# встретился until
sub until {
	return "} until(", ");";
}

# встретился if
sub if {
	return "((", ")? do {", "}: ())";
}

# встретился elseif
sub elseif {
	return "}: (", ")? do {";
}

# встретился else
sub else {
	return "}: do {", "})";
}

# встретился try
sub start_try {
	"do { eval {"
}

# встретился catch
sub catch {
	my ($self, $isa, $var, $is_end) = @_;
	$var = $var? " \$DATA->{'$var'}=\$@;": "";
	$isa = defined($isa)? do { $isa =~ s/,/ /g; "Isa(\$@, qw/$isa/)"}: "defined \$@";
	my $pre = $is_end? "}:": "};";
	return "$pre $isa? do {$var ", "}: () }";
}

# открывающая скобка 
sub group {
	return "(", ")";
}

# открывающая скобка массива
sub array {
	return "[", "]";
}

# открывающая скобка хэша
sub hash {
	return "+{", "}";
}

# конвеер
sub pairmapconv {
	"R::map2 { \$DATA->{a} = \$a; \$DATA->{b} = \$b; "
}

# конвеер
sub map {
	"map { \$DATA->{a} = \$_; "
}

# конвеер
sub grep {
	"grep { \$DATA->{a} = \$_; "
}

# конвеер
sub reduceconv {
	"R::reduce { \$DATA->{a} = \$a; \$DATA->{b} = \$b; "
}

# конвеер
sub sort {
	"sort { \$DATA->{a} = \$a; \$DATA->{b} = \$b; "
}

# from после конвеера
sub from {
	" } ", ""
}

# блок begin
sub begin {
	return "BEGIN { my \$DATA; ", "}";
}

# роутер on
sub route {
	my ($self, $route) = @_;
	
	$route =~ s!^/?!/!;
	
	my $actionName = "Ajax" . $app->magnitudeLiteral->classname($route);
	
	return "BEGIN { \$R::App::app->router->add('$route' => '$actionName') } package $actionName { use common::sense; use R::App qw/msg1 msg/; sub dispatch { my \$DATA={};", "}}";
}

# addhandler в роутере on
sub addhandler {
	"return if \$app->{q}->isErrors"
}

# массив параметров функции
sub paramarray {
	"\@_"
}

# супервызов блока
sub template_super {
	my ($self, $block, $sk) = @_;
	"\$DATA->SUPER::$block$sk"
}

# супервызов метода
sub super {
	my ($self, $method, $sk, $is_method) = @_;
	($method eq "new"? '$self=': "") . "\$self->SUPER::$method" . (!$is_method && !$sk? '(@_)': $sk)
}

# вызов метода
sub gosub {
	my ($self, $name) = @_;
	return "$name(", ")";
}

# оператор присваивания
sub assign {
	"="
}

# new Class
sub newstmt {
	my ($self, $name) = @_;
	"$name->new"
}

# разыменивание ссылки на массив
sub unarray {
	my ($self) = @_;
	return "\@{", "}"
}

# разыменивание ссылки на хэш
sub unhash {
	my ($self, $code) = @_;
	return "%{", "}"
}

# ничего
sub nothing {
	"undef"
}

# константа true
use R::Json;
sub true {
	"\$R::Json::true"
}

# константа false
sub false {
	"\$R::Json::false"
}

# выброс исключения
sub throw {
	"die"
}

# числовая константа
sub number {
	my ($self, $num) = @_;
	$num
}

# оператор
my %replace_op = qw/^ ** mod % xor ^/;
sub operator {
	my ($self, $operator) = @_;
	$replace_op{$operator} // $operator
}

# пробелы
sub space {
	my ($self, $space) = @_;
	$space
}

# ,
sub comma {
	","
}

# формирует аргументы функции
sub _args {
	my ($args) = @_;
	local $_;
	my $AST=0;
	$args = join ", ", map { $_ eq "*"? do { $AST++; "my \$_AST$AST"}: "\$DATA->{'$_'}"} @$args;
	$args = "($args) = \@_; " if $args;
	$args .= "%\$DATA = (".join(", ", map { "\%\$_AST$_" } 1..$AST).", %\$DATA); " if $AST;
	$args
}

# формирует заголовок функции
sub _sub {
	my ($name, $args, $class) = @_;
	my $sub = $class? "my \$self=shift; ": "";
	$sub .= _args($args);
	$sub .= "\$self = bless {}, ref \$self || \$self; " if $name eq "new";
	$sub
}


# объявление функции
sub sub {
	my ($self, $name, $args, $class_in, $class, $endline) = @_;
	
	my $sub = _sub($name, $args, $class);
	
	return ($class_in? "package $class_in {": "") . "sub $name { my \$DATA = {}; $sub$endline(); ", ($class_in? "}}": "}");
}


# перегрузка оператора
sub overload {
	my ($self, $name, $args, $class_in, $class, $endline) = @_;
	
	my $sub = _sub($name, $args, $class);
	
	return ($class_in? "package $class_in {": "") . "use overload '$name' => sub { my \$DATA = {}; $sub$endline(); ", ($class_in? "}}": "};");
}


# блок do
sub do {
	my ($self, $prevop, $args, $endline) = @_;
	my $args = _args($args);
	#$endline не выводим - если там then... а у perl нет конца комментария
	($prevop? "": ", ") . "sub { $args$endline", "}"
}

# самый обычный for =
sub for {
	my ($self, $k, $i) = @_;
	my $begin = $i? "\$DATA->{'$i'}=0; ": "";
	my $end = $i? "; \$DATA->{'$i'}++; ": "";
	return "${begin}for my \$I_$k (", ") { \$DATA->{'$k'}=\$I_$k;", "$end}";
}

# for с разыменованием параметра
sub for_in {
	my ($self, $k, $i) = @_;
	my ($begin, $then, $end) = $self->for($k, $i);
	return "$begin\@{", "}$then", $end;
}

# for по хэшу
sub for_of {
	my ($self, $k, $v, $i) = @_;
	my $begin = $i? "\$DATA->{'$i'}=0; ": "";
	my $end = $i? "; \$DATA->{'$i'}++; ": "";
	my $then = $v? " \$DATA->{'$v'}=\$H_${k}->{\$I_$k};": "";
	return "${begin}my \$H_$k = ", "; for my \$I_$k (keys %\$H_$k) { \$DATA->{'$k'}=\$I_$k;$then", "$end}";
}

# хелпер для расширения класса
sub _extends {
	my ($extends) = @_;
	if(defined $extends) {
		my $x = $extends =~ s/,/ /g;
		$extends = " our \@ISA = qw/$extends/;";
		$extends .= " use mro 'c3';" if $x;
	}
	$extends
}

# декларация модуля
sub module {
	my ($self, $name, $extends) = @_;
	$self->class($name, $extends)
}

# декларация класса
sub class {
	my ($self, $name, $extends) = @_;
	my $inherits = _extends($extends);
	my ($begin_js_class, $end_js_class) = ("\$R::App::app->js->start;", "\$R::App::app->js->compress(__PACKAGE__, '".$app->view->view."')");
	my ($begin_js, $end_js) = ("\$R::App::app->js->append(q{", "});");
	my ($begin, $end) = $app->viewJavascript(view=>$self->view)->class($name, $extends);
	$begin =~ s![{}]!\\$&!g;
	$end =~ s![{}]!\\$&!g;
	return "(do { package $name; BEGIN {$inherits \$R::View::Perl::Classes{'$name'}++; push \@R::View::Perl::Classes, '$name'; $begin_js_class${begin_js}$begin${end_js} } use common::sense; use R::App qw/!clean msg msg1 Isa Num/; sub __INIT__CLASS__ { my \$DATA; my \@R; my \$self = shift; ", "return bless \\\@R, 'R::Html' } BEGIN { ${begin_js}$end${end_js}$end_js_class } __PACKAGE__ })";
}

# вставка на js, которая будет записана в app->js и сохранена в файл
sub inline_code_js {
	my ($self) = @_;
	$self->view->lang("js");
	$self->view->{lang}{q_escape} = 1;
	return "BEGIN { \$R::App::app->js->append(q{", sub {
		$self->view->lang("perl");
		"}) }"
	};
}

# функция для вставки в модуль
sub modulesub {
	my ($self, $module, $sub, $cls) = @_;
	" sub ${module}::$sub { '$cls' }"
}

# добавляет new и аргументы
sub object {
	my ($self, $cls) = @_;
	return "->new(" , ")"
}

# вставка кода в темплейт
sub v_in {
	return "'; ", "; push \@R, '"
}

# вставка выражения в темплейт
sub v_raw {
	return "', (scalar do { ", " }), '"
}

# вставка выражения с экранированием
sub v_escape {
	return "', R::Html::_escape(scalar do { ",  " }), '";
}

# оборачивает в json
sub v_json {
	return "', \$R::App::app->json->to(scalar do {", "}), '"
}

# запоминание позиции кода
my $SETPOS = 0;
sub v_setpos {
	join "", 'my $SETPOS', ++$SETPOS, '=@R';
}

# возвращение позиции кода
sub v_getpos {
	"splice \@R, \$SETPOS$SETPOS"	
}

# сценарий
sub scenario {
	my ($self, $name, $masking) = @_;
	return "package R::View::Views; use common::sense; use R::App; sub __sc__$name { my \$DATA = {}; $masking;
}

1;"
}

# возвращает шаблон
sub template {
	my ($self, $class, $inherits, $begin) = @_;
	
	# если в блоке нет наследования от другого, то его тело рендерится, иначе - TODO: проверяется на пустоту
	my $block = @$inherits? "__UNUSED__": "__RENDER__";
	
	# множественное наследование
	my $mro = @$inherits>1? " use mro 'c3';": "";
	
	# преобразуем в человекопонятное имя
	$inherits = join " ", map { $app->view->get_name($_) } @$inherits;
	
	# добавляем базу шаблонов
	$inherits = "R::View::Lukull" if !length $inherits;
	
	my $extends = $inherits? " our \@ISA = qw/$inherits/;$mro": "";
	return "package $class;$extends use common::sense; use R::App qw/!clean msg msg1 closure Isa Num/; use R::Html;$begin sub $block { my \$DATA = shift; my \@R;", "return bless \\\@R, 'R::Html' } 1;"
}

# блок
sub block {
	my ($self, $name) = @_;
	return "sub $name { my \$DATA=shift; my \@R;", "return bless \\\@R, 'R::Html' } push \@R, \$DATA->$name"
}

# блок вывода джаваскрипта в app.js
sub js {
	return "BEGIN { my \$DATA; my \@R", "\$R::App::app->js->append( join '', \@R ) }"
}

# блок filter
sub filter {
	my ($self, $var) = @_;
	return v_setpos(), join "", "push \@R, R::View::Filter->", $var, "(join '', ", v_getpos(), ")"
}

# часть шаблона в переменную
sub set {
	my ($self, $var) = @_;
	return v_setpos(), join "", "\$DATA->{", $var, "} = bless [", v_getpos(), "], 'R::Html'";
}

# включение блока или шаблона
sub include {
	my ($self, $name, $block) = @_;
	$block //= "__RENDER__";
	return "{ my \$_DATA=\$DATA; my \$sv = R::guard { \$DATA=\$_DATA; }; \$DATA=bless {%\$DATA}, '$name';", "; push \@R, \$DATA->$block; }"
}

# включение блока или шаблона без сохранения переменных
sub process {
	my ($self, $name, $block) = @_;
	$block //= "__RENDER__";
	return "", "{ my \$BLESS = ref \$DATA; my \$sv = R::guard { bless \$DATA, \$BLESS; }; bless \$DATA, '$name'; push \@R, \$DATA->$block; }"
}

# экшн
sub action {
	my ($self, $key, $route, $view) = @_;
	$key =~ s!^/?!/!g;
	"BEGIN { \$R::App::app->router->add('$key' => '$route') }"
}

# блок функции в темплейте
sub subinblock {
	return " my \@R", "return bless \\\@R, 'R::Html' ";
}

# вызывается в masking для второго прохода по свормированному коду в этом языке
# должен модифицировать $_[1]
sub expirience {
	#my ($self, $expirience) = @_;
	#$_[1] = $expirience
	$_[1]
}

# рендерит
sub render {
	my ($self, $view, $data) = @_;
	
	$self->view->load if !keys %{$self->view->{INC}};
	my ($class) = $self->view->require($view);
	$data //= {};
	join "", @{ bless($data, $class)->__RENDER__ }
}

# вычисляет выражение на perl
sub eval {
	my ($self, $code, $DATA) = @_;
	eval $code;
}

our %Classes;
our @Classes;

# при перезагрузке - сохраняем
sub HOTSWAP_REMOVE {
	my ($cls, $hotswap, $from_reload) = @_;
	$hotswap->{STASH}{$cls} = {
		Classes => {%Classes},
		ArrClasses => [@Classes],
	};
}

# при перезагрузке - восстанавливаем
sub HOTSWAP_LOAD {
	my ($cls, $hotswap, $service) = @_;
	my $x = delete $hotswap->{STASH}{$cls};
	%Classes = %{$x->{Classes}};
	@Classes = @{$x->{ArrClasses}};
}

# очищает классы
sub clear_classes {
	my ($self) = @_;
	
	%Classes = ();
	@Classes = ();
	
	$self
}

# позиция в классах
sub len_classes {
	int @Classes
}

# инициализирует классы
sub init_classes {
	my ($self, $from) = @_;
	
	return $self if $from == @Classes;
	
	my @cls = @Classes[$from .. $#Classes];
	
	# проверка на вшивость
	for my $class (@cls) {
		die "дважды объявлен класс $class" if $Classes{$class} > 1;
		$Classes{$class} = 1;
	}
	
	# запускаем тело класса
	for my $class (@cls) {
		$class->__INIT__CLASS__;
	}
	
	# инициализируем инпуты
	for my $class (@cls) {
		$class->create_meta if $class->isa("R::Form::Input");
	}
	
	# инициализируем формы
	for my $class (@cls) {
		$class->create_meta if $class->isa("R::Form::Form");
	}	
		
	# запускаем конструктор класса
	for my $class (@cls) {
		$class->CLASS_INIT if $class->can("CLASS_INIT");
	}
	
	$self
}

1;