package R::View::Javascript;
# транслятор на js c языка программиррвания lukull

use common::sense;
use R::App;

has qw/name view q_escape/;

# конструктор
sub new {
	my $cls = shift;
	my $self = bless {
		name => "javascript",
		view => undef,		# устанавливается в view->lang
		tree => 0,				# построить дерево кода
		#ref => [],				# массив замен
		q_escape => 0,			# вывод в q{...}. Эскейпить \ ещё раз
		@_
	}, ref $cls || $cls;
	
	if($self->{view}) {
		R::weaken $self->{view};
		msg1("view.view=", $self->{view}->view), die "view(".$self->{view}->view.") пуст" if $self->{view}->view =~ /^\s*$/;
	}
	
	$self
}


# добавляет весь необходимый javascript
sub vitaland {
	my ($self) = @_;
	#return if $self->{vitaland};
	#$self->{vitaland} = 1;
	join "", $app->framework->file("share/dev/vitaland.js")->read, "\napp\$.ini_href = ", $app->ini->{dev}? app->json->to(+{ %{$app->ini} }): "{};\n"
}

# require R::Html;
# sub ($) { goto &R::Html::_escape }
# sub esc ($) { \R::Html::_escape(@_) }

# заменяет спецсимволы в строке
sub escape_string {
	my ($self, $string) = @_;
	
	$string =~ s/\\"/"/g;
	$string =~ s!\\(['"nrft])?!"\\" . ($1 || "\\")!ige;
	$string =~ s/"/\\$&/g;
	
	#$string =~ s/\\(['"\\])/$1/g;
	#$string =~ s/['"\\]/\\$&/g;
	
	my $replace = sub {
		my $x = $& eq "\r\n"? "\\r\\n": $& eq "\r"? "\\r": "\\n";
		"$x\"+$&\""
	};
	
	$string =~ s/\r\n|\r|\n/$replace->()/ge;
	
	if($self->{q_escape}) {
		$string =~ s/\\/\\\\/g;
	}
	
	$string
}

# заменяет выражение в строке
sub replace_dollar {
	my ($self, $masking) = @_;
	"\"+ $masking +\""
}

# оборачивает строку в кавычки
sub to_string {
	my ($self, $string) = @_;
	"\"$string\""
}

# конец строки, возможно с комментарием
sub endline {
	my ($self, $space, $rem, $endline) = @_;
	defined($rem)? "$space//$rem$endline": $endline
}

# многострочный комментарий <!-- -->
sub multiline_comment {
	my ($self, $rem) = @_;
	return " /* $rem */ ";
}

# конец выражения
sub sepexpression {
	my ($self) = @_;
	";"
}

# конец выражения после then
sub sepexpression_after_then {
	''
}


# встретилась регулярка
sub regexp {
	my ($self, $regexp, $args) = @_;
	
	if($regexp =~ /(?<! \\ ) \$ ( \{ | [a-z_] )/xi) {
		$regexp = $self->view->replace_dollar($regexp);
		return "new RegExp($regexp, '$args')";
	}
	
	$regexp =~ s!/!\\/!g;
	"/$regexp/$args"
}

# специальная переменная perl
sub special_var {
	my ($self, $special_var) = @_;
	die "Специальные переменные perl, такие как \$$special_var не поддерживаются"
}


# встретилось имя класса
sub classname {
	my ($self, $name, $sk) = @_;
	$self->_sk(_class_name($name), $sk)
}

# вспомогательный метод для скобок
sub _sk {
	my ($self, $name, $sk) = @_;
	return $name if !defined $sk;
	return "$name(", ")" if $sk eq "(";
	return "$name\[", "]" if $sk eq "[";
	return "$name\[", "]";
}

# вспомогательный метод для вызова метода
sub _call_sk {
	my ($self, $name, $sk) = @_;
	return "$name()" if !defined $sk;
	return "$name(", ")" if $sk eq "(";
	$self->_sk("$name()", $sk);
}

# встретилась переменная
sub var {
	my ($self, $name, $sk) = @_;
	return $self->_sk("this", $sk) if $name =~ /^(self|this|me)\z/i;
	$self->_sk("DATA.$name", $sk)
}

# вызов метода по ссылке .$name
sub dotref {
	my ($self, $name, $sk) = @_;
	$self->_call_sk("DATA['$name']", $sk)
}

# вызов метода
sub dot {
	my ($self, $name, $sk) = @_;
	$self->_call_sk(".$name", $sk);
}

# по ключу хеша
sub colon {
	my ($self, $name, $sk) = @_;
	$self->_sk(".$name", $sk)
}

# разыменование класса
sub twocolon {
	my ($self, $name, $sk) = @_;
	$self->_call_sk(".prototype.$name", $sk)
}

# доступ к реальной переменной
sub dollar {
	my ($self, $name, $sk) = @_;
	$self->_sk($name, $sk)
}

# встретился ключ key =>
sub key {
	my ($self, $key) = @_;
	$self->view->top->{tag} eq "}"? "$key:": $app->magnitudeLiteral->asstring($key) . ", "
}

# встретилась долгая запятая =>
sub fat_comma {
	my ($self) = @_;
	$self->view->top->{tag} eq "}"? ": ": ", "
	
}

# встретилось ключевое слово
sub word {
	my ($self, $word) = @_;
	$word = lc $word;
	#$word eq "ucfirst"? "":
	#$word eq "lcfirst"? "":
	#$word eq "uc"? "":
	#$word eq "lc"? "":
	$word eq "undef"? "void(0)":
	$word eq "next"? "continue":
	$word eq "last"? "break":
	#$word eq "redo"? "":
	$word eq "return"? "return":
	#$word eq "pairs"? "":
	#$word eq "msg"? "":
	#$word eq "msg1"? "":
	#$word eq "exists"? "":
	#$word eq "use"? "":
	$word =~ /^(?:push|pop|splice|shift|unshift|keys|values|scalar)/? do { $self->top->{word} = $word; "" }:
	$word eq "delete"? "delete":
	#$word eq "wantarray"? "":
	$self->view->error("ключевое слово $word в $self->{name} не используется")
}

# встретилось ключевое слово
sub unary {
	my ($self, $word) = @_;
	
	$word = lc $word;
	$word = $word eq "ref"? "typeof": $word;
	
	$self->gosub($word)
}

# встретился указатель на себя
sub self {
	'this'
}

# встретилась специальная переменная app
sub appvar {
	'app$'
}

# встретилась специальная переменная q
sub q {
	'app$.q'
}

# встретилась специальная переменная user
sub user {
	'app$.q.user'
}

# самый обычный for =
sub for {
	my ($self, $k, $i) = @_;
	my $I = "I_$k";
	my $K = "DATA.$k";
	my $N = "N_$k";
	my $A = "A_$k";
	return "var $A=[", "]; for(var $I=0, $N=$A.length; $I&lt;$N; $I++) { $K=${A}[$I]; " . ($i?"DATA.$i=$I; ":"") , "}";
}

# for с разыменованием параметра
sub for_in {
	my ($self, $k, $i) = @_;
	my $I = "I_$k";
	my $K = "DATA.$k";
	my $N = "N_$k";
	my $A = "A_$k";
	return "var $A=", "; for(var $I=0, $N=$A.length; $I<$N; $I++) { $K=${A}[$I]; " . ($i?"DATA.$i=$I; ":"") , "}";
}

# for по хэшу
sub for_of {
	my ($self, $k, $v, $i) = @_;
	$i = $i? "DATA.$i": undef;
	my $K = "K_$k";
	my $A = "A_$k";
	return "".($i? "$i=0; ": "")."var $A=", "; for(var $K in $A) { if(_own.call($A, $K)) { DATA.$k=$K; ".($v? "DATA.$v=${A}[$K]; ": "") ,  ($i? "; $i++; ": "")."}}";
}


# встретился цикл while
sub while {
	return "while(", ") {", "}";
}

# встретился цикл repeat
sub repeat {
	"do {"
}

# встретился until
sub until {
	return "} while(!(", "));"
}

# встретился if
sub if {
	my ($self, $if_is_stmt) = @_;
	
	$if_is_stmt && return "if(", ") {", "}";
	
	return "((", ")? (()=>{", "})(): void(0))";
}

# встретился elseif
sub elseif {
	my ($self, $if_is_stmt) = @_;
	$if_is_stmt && return "} else if(", ") {";
	
	return "})(): (", ")? (()=>{";
}

# встретился else
sub else {
	my ($self, $if_is_stmt) = @_;
	$if_is_stmt && return "} else {", "}";
	return "})(): (()=>{", "})())";
}

# встретился try
sub start_try {
	#"try {"
	"try {"
}

# встретился catch
sub catch {
	my ($self, $isa, $var, $is_end) = @_;
	
	$var = $var? " DATA.$var=e;": "";
	$isa = defined($isa)? do { "Isa(e, $isa)"}: "defined \$@";

	return "} catch(e) { $var ", "}";
}

# открывающая скобка 
sub group {
	return "(", ")"
}

# открывающая скобка массива
sub array {
	return "[", "]"
}

# открывающая скобка хэша
sub hash {
	return "{", "}";
}

# конвеер
sub pairmapconv {
	"_pairmap(this, DATA, function() { "
}

# конвеер
sub map {
	"_map(this, DATA, function() { "
}

# конвеер
sub grep {
	"_grep(this, DATA, function() { "
}

# конвеер
sub reduceconv {
	"_reduce(this, DATA, function() { "
}

# конвеер
sub sort {
	"_sort(this, DATA, function() { "
}

# from после конвеера
sub from {
	#" }, ([", "]) )"
	return " }, ([", "]) )"
}

# блок begin
sub begin {
	#return "\$(function() {", "})";
	return "});", "CODE\$.push(function() {"
}

# роутер on
sub route {
	my ($self, $route) = @_;
	
	$self->view->error("on $route - не используется в $self->{name}");
	
	# my $actionName = $route;
	
	# $route =~ s!^/?!/!;
	# $actionName =~ s!/([a-z])!ucfirst $1!ige;
	# $actionName =~ s!-!__!g;
	# $actionName = "on__$actionName";
	
	# my $path = $app->view->{file};
	
	# return "BEGIN { \$R::App::app->view->route_on('$route', \\&{'$actionName'}, '$path') } sub $actionName { my DATA={};", "}";
}

# addhandler в роутере on
sub addhandler {
	my ($self) = @_;
	$self->view->error("addhandler - не используется в $self->{name}");
	#"return if \$app->{q}->isErrors"
}

# массив параметров функции
sub paramarray {
	"arguments"
}

# супервызов блока
sub template_super {
	my ($self, $block, $sk) = @_;
	"DATA.SUPER.$block$sk"
}

# супервызов метода
sub super {
	my ($self, $method, $sk, $is_method) = @_;
	($method eq "new"? "super": "super.$method") . (!$is_method && !$sk? '(...arguments)': $sk // '()')
}

# вызов функции
sub gosub {
	my ($self, $name) = @_;
	return "(console.log.apply(console, ARG\$ = [", "]), ARG\$[ARG\$.length-1])" if  $name =~ /^msg1?$/;
	return "$name(", ")";
}

# оператор присваивания
sub assign {
	"="
}

# new Class
sub newstmt {
	my ($self, $name) = @_;
	$name=~/^[A-Z]/? "new window.$name": "new DATA.$name"
}

# разыменивание ссылки на массив
sub unarray {
	my ($self) = @_;
	#return "]).concat(", ", [";
	return "...", ""
}

# разыменивание ссылки на хэш
sub unhash {
	my ($self) = @_;	
	#return "]).concat(_hash2array(", "), [";
	return "..._hash2array(", ")"
}

# ничего
sub nothing {
	"null"
}

# константа true
sub true {
	"true"
}

# константа false
sub false {
	"false"
}

# выброс исключения
sub throw {
	"throw"
}

# числовая константа
sub number {
	my ($self, $num) = @_;
	$num =~ tr/_//;
	$num
}

# оператор
my %replace_op = qw/^ **    mod %    xor ^    <=> -    or ||   and &&    not !/;
my %op_logic = $app->perl->set(qw/== != <= >= < > + /);
my %op_any = $app->perl->set(qw/ - * \/ % << >> -= *= \/= ++ -- ! && || === !== instanceof in /);
my %op_replace = ( "." => " +''+ ", ".=" => " += ''+ ", "+=" => "+= + ",
					"eq" => "+'' == ''+",
					"ne" => "+'' != ''+",
					"lt" => "+'' < ''+",
					"gt" => "+'' > ''+",
					"le" => "+'' <= ''+",
					"ge" => "+'' >= ''+",
				);
sub operator {
	my ($self, $operator) = @_;
	
	$operator = $replace_op{$operator} // $operator;
	
	if(exists $op_logic{$operator}) {  " -0 $operator + " }
	elsif(exists $op_any{$operator}) {  $operator }
	elsif(exists $op_replace{$operator}) {  $op_replace{$operator} }
	elsif($operator eq "..") {
		".."
	}
	else {
		die $self->view->file.":\nнеизвестный науке оператор $` ---> `$operator`"
	}
	
}

# пробелы
sub space {
	my ($self, $space) = @_;
	$space;
}

# ,
sub comma {
	","
}

# формирует аргументы функции
sub _args {
	my ($args) = @_;
	local $_;
	my $i = -1;
	
	join "", map { $i++; $_ eq "*"? "\$.extend(DATA, arguments[$i]); ": "DATA.$_ = arguments[$i]; "} @$args;
}

# блок do
sub do {
	my ($self, $prevop, $args, $endline) = @_;
	$args = _args($args);
	#$endline не выводим - если там then... а у perl нет конца комментария
	($prevop? "": ", ") . "(function() {$args$endline", "}).bind(this)"
}

# объявление функции
sub sub {
	my ($self, $name, $args, $class_in, $class, $endline) = @_;
	
	$name = "constructor" if $name eq "new";
	
	$args = _args($args);
	
	my $cls = _class_name($class);
	$class_in = _class_name($class_in);
	
	# двойная ;; для того чтобы return установить в пустой функции
	return ($class_in? "$class_in.prototype.$name = function": $class? $name: "function $name") . ("() { var DATA = {};; $args$endline"), "}";
}

# заглушка
sub inline_code_js { return "", "" }

# это не перегрузка оператора
# это on ":root.class:click"
sub overload {
	my ($self, $name, $args, $class_in, $class, $endline) = @_;
	
	# local $_ = $name;
	
	# my $root;
	# $root = 1 if s!^:root!!;
	
	# die "нет действия в on \"$name\"" if /:(.*?)$/;
	# my $query = $`; my $action = $1;
	
	$args = _args($args);
	
	my $cls = _class_name($class);
	
	# двойная ;; для того чтобы return установить в пустой функции
	return "$cls.prototype[\"$name\"] = function() { var DATA = {};; $args$endline", "}";
}

# декларация модуля
sub module {
	my ($self, $name, $extends) = @_;
	my $name = _class_name($name);
	$extends = $self->_extends($name, $extends);
	
	return "($name = (function() {function $name() {}$extends", "return $name }).call(this))";
}

# декларация класса
sub class {
	my ($self, $name, $extends) = @_;
	my $name = _class_name($name);
	$extends = $self->_extends($extends);
	
	return "window.$name = class $name$extends {", "}";
}

# возвращает имя класса
sub _class_name {
	my ($name) = @_;
	$name =~ s/::/\$/g;
	$name
}

# вспомогательный метод для расширения
sub _extends {
	my ($self, $extends) = @_;
	return "" if !$extends;
	my @extends = map { _class_name($_) } split /\s*,\s*/, $extends;
	join "", " extends ", @extends>1? ("_mixin(", join(", ", @extends), ")"): $extends[0]
}

# возвращает шаблон
sub template {
	my ($self, $class, $inherits, $begin) = @_;
	
	$class = _class_name($class);
	
	my $block = @$inherits? "__UNUSED__": "__RENDER__";
	
	$self->view->error("множественное наследование в $self->{name} не реализовано") if @$inherits>1;
	
	my $extends = $self->_extends("window.$class", $inherits->[0]);
	
	return "$class = (function(){ function $class() {}  $begin $extends $class.prototype.$block=function(DATA) { var app_OUTPUT = []; ", " app_OUTPUT.join(''); }; return $class })();"
}

# блок
sub block {
	my ($self, $name) = @_;
	return "this.prototype.$name = function(DATA) {", "} this.$name(DATA);"
}


# # добавляет функцию для расширения
# sub _extends {
	# my ($self, $child, $parent) = @_;
	# return "" if !$parent;
	# $child = _class_name($child);
	# $parent = _class_name($parent);
	# "_extend($child, $parent); ";
# }

# функция для вставки в модуль
sub modulesub {
	my ($self, $module, $sub, $cls) = @_;
	" ${module}.$sub = function() { return '$cls' }"
}

# добавляет new и аргументы
sub object {
	my ($self, $with_args, $begin, $end) = @_;
	#$end = join "", $end, "->new(", $with_args , ")";
	return "new $begin($with_args)", $end;
}


# вставка кода в темплейт
sub v_in {
	return "');", "app_OUTPUT.push('";
}

# вставка выражения в темплейт
sub v_raw {
	return "', (() => {", "})(), '"
}

# вставка выражения с экранированием
sub v_escape {
	return "', (() => {", "})(), '"
}

# запоминание позиции кода
my $SETPOS = 0;
sub v_setpos {
	join "", 'var $SETPOS', ++$SETPOS, '=app_OUTPUT.length;';
}

# возвращение позиции кода
sub v_getpos {
	"app_OUTPUT.splice($SETPOS)"
	
}


# сценарий
sub scenario {
	my ($self, $name, $masking) = @_;
	return "\app$.scenario.$name = function() { var DATA = {};; $masking;
}

1;"
}

# блок вывода джаваскрипта в app.js - на самом деле - вставка js
sub js {
	TODO;
	return v_setpos(), join "", "\$R::App::app->js->append( ", v_getpos(), " );"
}

# блок filter
sub filter {
	my ($self, $var) = @_;
	TODO;
	return v_setpos(), join "", "push \@\$R::View::Views::OUTPUT, R::View::Filter->", $var, "(join '', ", v_getpos(), ")"
}

# часть шаблона в переменную
sub set {
	my ($self, $var) = @_;
	return v_setpos(), join "", "DATA.", $var, " = new \app$.html(", v_getpos(), ");";
}

# включение блока или шаблона
sub include {
	my ($self, $name, $masking, $block) = @_;
	$name = _class_name($name);
	
	$block //= "__RENDER__";
	"var data=DATA; DATA={}; $masking new $name().$block(DATA); DATA=data "
}

# включение блока или шаблона без сохранения переменных
sub process {
	my ($self, $name, $masking, $block) = @_;
	
	$name = _class_name($name);
	
	$block //= "__RENDER__";
	"$masking new $name().$block(DATA); "
}

# инициализатор экшена
sub init {
	my ($self, $masking) = @_;
	" sub init { my DATA=shift; $masking; return} DATA->init;"
}

# экшн
sub action {
	my ($self, $key, $route, $view, $masking) = @_;
	TODO;
	"BEGIN { \$R::App::app->view->action('$key', '$route', '$view') } $masking"
}

# блок функции в темплейте
sub subinblock {
	return " my \$IDX = \@\$R::View::Views::OUTPUT;", "return bless [splice \@\$R::View::Views::OUTPUT, \$IDX], 'R::Html' ";
}

# вызывается в masking для второго прохода по свормированному коду в этом языке
my $js_id = qr/[a-z_\$][\w\$]*/i;
my $re_sk = qr/\( [^\(\)]* \)/x;
my $re_function = qr/(?:function \s+)? (?: $js_id ) \s* $re_sk \s* \{ | 
	(?: $re_sk | $js_id ) \s* => \s* \{ /x;
my $re_try = qr/\b try \s* \{/x;
my $re_open = qr/\{/; #qr/[\{\[\(]/;
my $re_close = qr/\}/; #qr/[\}\]\)]/;
my $re_rem = qr! (  (\s*(//[^\n]*|/\*.*\*/))*\s+   )? !nsx;
sub expirience {
	my ($self, $expirience) = @_;

	#msg ":red", "expirience", ":reset", $expirience;
	 
	my @lex = grep length, split m{(
		$re_function |
		$re_open |
		$re_close |
		;\s* |
		(?: [\ \t]* (?: // ) [^\n\r]* (?: \r\n | \r | \n | $ ) ) |
		" (?: \\. | [^"\\]  )* " | 
		' (?: \\. | [^'\\] )* ' |
		/\* .*? \*/ |
		(?<! [\$\w\)\]\}] )  [\ \t]*  / (?: \\/ | [^/\n] )* /
	)}sxo, $expirience;
	
	# if($self->view->view eq "admin/c_scenario.html") {
		# msg ":size1000000 blue", "lex", ":reset", \@lex ;
	# }
	#return "";
	
	# в функциях проставляем return
	# для этого строим дерево по {} и перед последним выражением ставим return
	my @S = [undef, []];	# [тип, [ if() {, [] ]
	my $k = 0;
	my @lex_c;	# [[количество незакрытых скобок, лексема] ...]
	#msg1 @lex;
	for(@lex) {
		push @lex_c, [scalar @S, $_];
		/^$re_function\z/o? do {
			if(!/^(catch|for|switch|while|if|_mixin|constructor)\b/) {
				push @S, ["f", [$_]]
			}
			else {
				push @S, [$1, [$_]]
			}
		}:
		/^$re_open\z/o? do { push @S, ["open", [$_]] }:
		/^$re_close\z/o? do {
			my $x = pop @S;
			die _add_count_sk(\@lex_c, $k) . "` Лишняя закрывающая скобка. " . $self->view->view if !@S;
			# ставим return перед последним выражением, которое после начала или ;
			if($x->[0] eq "f") {
				my $s = $x->[1];
				# ищем ; или начало
				my $i = $#$s-1;
				for(; $i>0; $i--) {
					if($s->[$i] =~ /^;/) { last; }
				}
				
				$i++;
				
				# пропускаем все пустые строки с комментариями
				for(; $i<@$s-1; $i++) {
					if( !ref $s->[$i] and $s->[$i]!~m!^$re_rem$! ) { last; }
				}
				
				if( !ref($s->[$i]) && $s->[$i] !~ /^$re_rem(try|catch|while|do|for|continue|break|throw|switch|default|case|let|var|if|else|return)\b/n ) {
					$s->[$i] =~ s!^$re_rem!$&return !;
				}
			}
			push @{$x->[1]}, $_;
			push @{$S[$#S][1]}, $x;
		}:
		do { push @{$S[$#S][1]}, $_ };
	}
	continue { $k++ }
	
	#msg1 ":depth0 size1000000", \@S;
	
	#$app->file("var/1.yml")->write($app->yaml->to({ lex=>\@lex, stack => \@S})),
	
	die _add_count_sk(\@lex_c, $k)."` Нет закрывающей скобки. " . $self->view->view #. ". expirience: $expirience. S:" . $app->perl->dump(\@S) 
		if @S != 1;
	
	# if($self->{view}->{view} eq "admin/c_scenario.html") {
		# #msg ":red size1000000 depth4", "stack", ":reset", $S[0];
	# }
	join "--------------------------", map {_assemble($_)} @S;
}

# добавляет количество скобок на каждой строке со скобками
sub _add_count_sk {
	my ($lex, $k) = @_;
	
	join "", map { " ${\R::colored($_->[0], 'red')} $_->[1]" } @$lex[0..$k];
}

# собирает разобранное в expirince
sub _assemble {
	my ($x) = @_;
	join "", map { ref $_? _assemble($_): $_ } @{$x->[1]}
}

# # проход который превратит условия на основе тернарного оператора (?:)  из которых ничего не возвращается в if-stmt
# sub _if_stmt {
	# my ($x) = @_;
	# my $i = 0;
	# my $u = $x->[1];
	# for my $e ( @$u ) {
		# _if_stmt($e), next if ref $e;

		# next if $e ne ")? (";
		# # ищем начало if
		# my $is = 0; my $return = 0;
		# for(my $k=$i-1; $k>=0; $k--) {
			# $is = $k, $return = $1 if !ref $u->[$k] and $u->[$k] =~ m!^(return )?\(\( !;
		# }
		
		# die "начало if не найдено" if !defined $is;
		
		# if($return) {
			# # тут надо узнать: есть ли в if break или continue и если есть, то превратить.
			# # однако без AST это сделать трудно. Поэтому просто выходим
			# next;
		# }
		
		
	# }
	# continue { $i++ }
# }

# рендерит
sub render {
	my ($self, $path, $data) = @_;	

	my $x = $self->view->{INC}{$path};
	die "нет пути $path" if !$x;
	my $class = _class_name($x->{class});
	
	$data = $app->json->to($data // {});
	my $eval = "new $class().__RENDER__($data)";
	
	my $val = $app->node->eval($eval);
	msg(":red", "app->view->lang('js')->render", ":reset", $eval), die if $@;
	
	$val
}

# вычисляет выражение на javascript
sub eval {
	my ($self, $eval, $data) = @_;
	my $code = "var DATA = " . $app->json->to($data // {}) . "; " . $eval;
	$app->node->eval($code)
}

# очищает классы
sub clear_classes {
	my ($self) = @_;
	$self
}

# позиция в классах
sub len_classes {
	0
}

# инициализирует классы от указанного
sub init_classes {
	my ($self, $from) = @_;
	$self
}



1;
