package R::View::Lukull;
# базовый класс для шаблонов

use common::sense;
use R::App;

# конструктор
sub new {
	my $cls = shift;
	my $self = bless {@_}, ref $cls || $cls;
	$Coro::current->{_TEMPLATE}{int $self} = ref $self;
	$self
}

sub DESTROY {
	delete $Coro::current->{_TEMPLATE}{int $_[0]};
}

# для использования шаблона в качестве контроллера
sub dispatch {
	my $cls = shift;
	
	my $view = bless {@_}, $cls;
	my $q = $Coro::current->{request};
	
	# если есть специальный параметр: срабатывает форма
	if(!exists $view->{error} and my $action = $q->param('@action')) {
		my ($dispForm, $attrs, $param) = $app->router->get($action);
		msg1 ":on_blue red", '@action', ":reset", $action, $dispForm;
		die "нет диспетчера формы \@action=`$action`" if !$dispForm;
		
		die "\@action=$dispForm не является формой" if !$dispForm->isa("R::Form::Form");
		$q->{form} = my $form = $dispForm->new;
		# не заменять ref $form на $dispForm, т.к. происходит подмена в конструкторе для NativeModelForm
		$q->{NumberForms}{ref $form} = 0;
		$form->{save} = 1;	# брать параметры из q.can(lc metaform.method)

		$form->save;		# возвращает answer
	}
	# обрабатываем запрос
	my $result = $view->__RENDER__;
	
	# если есть форма и ошибки на ней неотработаны
	if($q->{form}) {
		my $form = $q->{form};
		if(!$form->{Concat} && $form->isErrors) {
			return join "", "<div class='noshow-form-errors'>ошибки на не выведенной форме $form->{Name}:", 
				"<ol class=error>",
					map({ "<li>" . $app->html->escape($_) } @{$form->{errors}}),
					map({ "<li>" . $app->html->escape( "$_:" . $form->{error}{$_} ) } keys %{$form->{error}}),
				'</ol>',
			"</div>", $result;
		}
	}
	
	return $result;
}

# возвращает request
sub q {
	$Coro::current->{request}
}

1;