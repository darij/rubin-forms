package R::View::Metafilter;
# пакет содержит суперфильтры шаблонов

use common::sense;
use R::App;

# конструктор
sub new {
	my ($cls) = @_;
	bless {}, ref $cls || $cls;
}


# форматирует html
sub htmlformatter {
	my ($view, $html, $prev) = @_;
	use bytes;
	return ($prev eq " "? "": " ") . "',$&'" if $html =~ /^\s+$/;
	$html
}


# фильтр парсит class="..." и создаёт найденные в style.css 
sub css_class {
	my ($view, $html, $prev) = @_;
	use bytes;
	
	my $haclasses = $view->{meta}{haclasses} //= {};
	my @class;
	my $flag = $view->{meta}{flagclasses};

	# если class="xxl <% ... %> m <% ... %> right"
	if(defined $flag) {
		if($html =~ /^([\-\w\s]*)['"]/) {
			push @class, $flag, $1;
			undef $view->{meta}{flagclasses};
		}
		elsif($html =~ /^[\-\w\s]*$/) {
			$view->{meta}{flagclasses} = "$flag $html";
			return $html;
		}
		else {
			undef $view->{meta}{flagclasses};
		}
	}
	
	while($html =~ /\bclass=(?:['"]([\-\w\s]*)['"]|([\-\w]+))/g) {
		push @class, $1 // $2;
	}
	
	if($html =~ /\bclass=['"]([\-\w\s]*)$/) {
		$view->{meta}{flagclasses} = $1;
	}
	
	if(scalar @class and my $class = join " ", @class) {
		$app->css->reg_class($class);
	}
	
	# if(scalar @class) {
		# my @classes = grep { my $x=!exists $haclasses->{$_}; $haclasses->{$_}=1; $x } $app->css->reg_html_classes(join " ", @class);
		# msg1 "c", \@classes;
		# $view->add_begin( "\$R::App::app->css->reg(qw/" . join(" ", @classes) . "/);" ) if @classes;
	# }
	
	$html
}

# фильтр заменяет &name; на значки из фонтов и спрайты
sub html_sign {
	my ($view, $html) = @_;

	my $sign = $app->sign->{orig};		# подгружаем знаки
	my $classes = $app->sprite->{classes};
	
	{use bytes;
		$html =~ s!&([\w-]+);?!
			{ no bytes; 
				my $x=$sign->{$1}; defined($x)? "<span class=sign>$x</span>":
				defined($x=$classes->{$1})? "<span class=$x>&nbsp;</span>":
				$&;
			}
		!ge;
	}
	$html
}

# # заменяет теги  на объекты
# sub tags {
	# my ($view, $html) = @_;
	# $html =~ s!<[]>!!g;
	# $html
# }

1;