package R::View::Lazy;
# используется для вставки значения после формирования шаблона в массив @R
# используется как:
# 	в начале лайоута:
#	 	<% GET app.coro.me.part:HEAD = app.viewLazy.new %>
# 		<% GET app.coro.me.part:HEAD_JS = app.viewLazy.new %>
#       <% CALL lazy_js = app.coro.me.part:FOOT = app.viewLazy.new %>
# 	в подключённых шаблонах:
#		<% call app.coro.me.part:HEAD.require_css("/css/style.css") %>
#		<% call app.coro.me.part:HEAD_JS.require_html("<meta name=question context=fail />") %>
#		<% call app.coro.me.part:FOOT.require_js("/js/lib.js") %>
#	в конце лайоута:
#		<% lazy_js %>

use common::sense;
use R::App;

use overload
	'""' => \&stringify,
	'.' => sub { $_[2]? $_[1] . $_[0]->stringify: $_[0]->stringify . $_[1] },
	'bool' => sub { 1 },
	"0+" => sub { 0+keys %{$_[0]} },
	"qr" => \&stringify,
	fallback => 1
;

# конструктор
sub new {
	my $cls = shift;
	bless {@_}, ref $cls || $cls;
}

# стрингифицирует объект
sub stringify {
	my ($self) = @_;
	
	my $html = $self->{html};
	my $css = $self->{css};
	my $js = $self->{js};
	
	join "\n\t\t",
		(R::map2 {"<script src=\"$a\" type=\"text/javascript\"></script>"} R::asc {$b} %$js),
		(R::map2 {"<link href=\"$a\" rel=\"stylesheet\"></script>"} R::asc {$b} %$css),
		(map { $html->{$_} } sort {$a <=> $b} keys %$html),
		@{$self->{HTML}};
}


#@@category js-require - можно запускать много шаблонов из разных частей программы, а затем слить всё в 

# добавляет html в заголовок
sub require_html {
	my ($self, $id, $text) = @_;
	$self->{html}{$id} //= $text;
	$self
}

# добавляет тег script, проверяет, что такого нет
sub require_js {
	my ($self, $link) = @_;
	$link =~ s!^/?!/!;
	
	my $js = $self->{js} //= {};
	
	$js->{$link} = keys %$js if !exists $js->{$link};
	
	$self
}

# добавляет тег link, проверяет, что такого нет
sub require_css {
	my ($self, $link) = @_;
	$link =~ s!^/?!/!;
	
	my $css = $self->{css} //= {};
	
	$css->{$link} = keys %$css if !exists $css->{$link};
	
	$self
}

# добавляет html к шаблонам
sub append {
	my ($self, $html) = @_;
	push @{$self->{HTML}}, $html;
	$self
}


1;