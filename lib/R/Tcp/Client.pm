package R::Tcp::Client;
# клиент tcp. Для одиночного использования

use common::sense;
use R::App;

use Symbol qw/ungensym/;
use R::Coro;

# конструктор
sub new {
	my $cls = shift;
	bless {	
		port => undef, # порт
		request => ref $app->tcpRequest,
		response => ref $app->tcpResponse,
		keep_alive => 0,	# указывает, что сервер работает в keep-alive режиме
		@_
	}, ref $cls || $cls;
}

# коннектится к серверу
sub connect {
	my ($self) = @_;
	
	my $ns = $app->socket->connect($self->{port});
	
	$self->{real_ns} = $ns;
	$self->{ns} = Coro::Socket->new_from_fh($ns);
	$self
}

# отправка запроса и получение ответа
sub send {
	my ($self, $param) = @_;
	
	$self->connect if !$self->{ns};
	
	my $request = $self->{request}->new(ns => $self->{ns});
	my $response = $self->{response}->new(res => $param);
	
	my $keep_alive = eval { $response->send($request) };
	$self->close, die if $@;
	
	eval { $request->recv };
	$self->close, die if $@;
	
	if($request->disconnected) { $self->close; die "disconnect" }
	if($request->bad) { $self->close; die "bad" }
	
	die $request->{res}->{err} if exists $request->{res}->{err};
	$request->{res}->{res}
}

# закрывает соединение
sub close {
	my ($self) = @_;
	close $self->{ns};
	ungensym $self->{real_ns};
	undef $self->{ns};
	undef $self->{real_ns};
	$self
}

# закрыть соединение
sub DESTROY {
	my ($self) = @_;
	$self->send("bay") if $self->{keep_alive};
	$self->close if $self->{ns};
}

1;