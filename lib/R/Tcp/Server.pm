package R::Tcp::Server;
# tcp-сервер. Используется для сервисов в отдельном процессе
# создаёт пул воркеров

use common::sense;
use R::App qw/$app msg1 msg/;
use Symbol qw//;

use POSIX qw/setsid :signal_h :errno_h :sys_wait_h/;

# конструктор
sub new {
	my $cls = shift;
	bless {
		workers => 3,
		request => $app->tcpRequest,
		response => $app->tcpResponse,
		ritter => '...',
		middleware => q{ my ($q, $r) = @_; $r },
		keep_alive => 0,
		@_
	}, ref $cls || $cls;
}

# открывает сокет из файлового дескриптора
sub from_fd {
	my ($self, $fd) = @_;
	
	open my $sd, '+<&=', $fd;  # fdopen
	$sd->autoflush(1);
	
	$self->{sd} = $sd;
	
	$self
}

# открывает сокет и создаёт пул воркеров
sub run {
	my ($self) = @_;
	
	$self->{sd} = my $sd = $app->socket->open($self->{port});
	
	my $name = $self->{name};
	die "не указано имя сервера" if !$name;
	my $workers = $self->{workers};
	die "не указано количество воркеров" if !$workers;
	my $ritter = delete $self->{ritter};
	die "ritter должен быть строкой" if !R::Is $ritter, "Str";
	my $middleware = $self->{middleware};
	die "middleware должна быть строкой" if !R::Is $middleware, "Str";
	
	my %x;
	my @keys = qw/workers port/;
	@x{@keys} = @$self{@keys};
	
	my $param = '%{'.$app->perl->inline_dump(\%x).'}, name => $app->process->name, ritter => sub {'.$ritter.'}';
	
	$param .= ", middleware => sub {$middleware}" if R::Is $middleware, "Str";
	
	# создаёт пул
	$self->{processes} = $app->process->new($name => '
		use '.ref($self).' qw//;
		'.ref($self).'->new('.$param.')->from_fd('.fileno($sd).')->accept;
	')->fd(fileno $sd)->dup($workers)->waitrun;
	
	$self
}

# бесконечный цикл, что делает бессмертными обработчики
sub loop {
	my ($self) = @_;
	$self->run if !$self->{processes};
	$self->{processes}->immortal;
}

# бесконечный цикл ожидания и выполнения запросов
sub accept {
	my ($self) = @_;
	
	while() {
		my $paddr = accept my $ns, $self->{sd};
		
		my $close_ns = R::guard {
			return if !$ns;
			shutdown $ns, 2;
			close $ns;
		};
		
		1 while $self->impulse($ns, $paddr) and $self->{keep_alive};
	}
	
	$self
}

# парсит запрос и сендит ответ
sub impulse {
	my ($self, $ns, $paddr) = @_;

	# формирование запроса
	my $request = $self->{request}->new(ns => $ns, paddr => $paddr);
	
	# считывание запроса
	eval {
		$request->recv;
	};
	return 0 if $request->disconnected;
	my $response;
	$response = $self->{response}->fail($@) if $@;
	
	# исполнение запроса
	if(!defined $response) {
		if($request->bad) {
			$response = $self->{response}->fail("bad request");
		}
		else {
			eval { $response = $self->ritter($request) };
			$response = $self->{response}->fail($@) if $@;
		}
		$response = $self->{response}->from($response);
	}
	
	# обработка промежуточной подпрограммой
	eval {
		$response = $self->middleware($request, $response);
		$response = $self->{response}->from($response);
	};
	$response = $self->{response}->fail($@) if $@;

	# отправка ответа
	my $keep_alive = eval {
		$response->send($request);
	};
	# последняя возможность отправить ответ
	if($@) {
		my $err = eval {"$@"} // "/no stringify error/";
		msg1 ":red on_blue", "errror", ":reset", 500, $err;
		$self->{response}->fail("tcpServer->response->send: $err", "")->send($request);
	}
	
	return $keep_alive;
}

# обрабатывает запрос
sub ritter {
	my $self = shift;
	$self->{ritter}->(@_);
}

# может изменить ответ
sub middleware {
	my $self = shift;
	$self->{middleware}->(@_);
}

# закрывает
sub close {
	my ($self) = @_;
	
	$self->{sd}->close;
	Symbol::ungensym $self->{sd};
	
	$self
}

1;