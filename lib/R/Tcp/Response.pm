package R::Tcp::Response;
# ответ для R::Fulminant-а и самостоятельный клиент

use common::sense;
use R::App;
use Storable qw//;


# свойства
has_const qw/res/;

# конструктор
sub new {
	my $cls = shift;
	bless {@_}, ref $cls || $cls;
}

# отправляет 
sub send {
	my ($self, $request) = @_;
	
	#msg1 ":cyan", "send", ":reset", $self->{res};
	
	my $msg = Storable::freeze $self->{res};
	$request->{ns}->syswrite( pack("NA*", length($msg), $msg) );
	!$request->bad		# keep_alive
}

# пришла ошибка
sub fail {
	my ($self, $error) = @_;
	$self->new(res => { err => $error });
}

# сохранить для отправки
sub from {
	my ($self, $res) = @_;
	Isa($res, $self)? $res: $self->new(res => {res => $res});	
}

1;