package R::Tcp::Request;
# модуль

use common::sense;
use R::App;
use Storable qw/thaw/;

# свойства
has_const qw/res disconnected bad/;

# конструктор
sub new {
	my $cls = shift;
	bless {@_}, ref $cls || $cls;
}

# считывает N байт из сокета
sub rd {
	my ($self, $n) = @_;
	my $len = $self->{ns}->sysread(my $N, $n);
	$self->{disconnected} = 1, die "recv: $!" if !defined $len;
	$self->{disconnected} = 1, die "прочитано $len байт, когда требуется $n. $!" if $len != $n;
	$N
}

# считывает запрос из сокета ns
sub recv {
	my ($self) = @_;
	$self->{res} = thaw $self->rd(unpack "N", $self->rd(4));
	#msg1 ":red", "recv", ":reset", $self->{res};
	$self->{disconnected} = 1 if $self->{res} eq "bay";
	$self
}



1;