package R::Project;
# текущий проект в котором вызвали al

use common::sense;
use R::App;

# имя проекта, путь к директории проекта, если мы не в проекте
has_const qw/name no root/;

my @INI_EXT = qw/.ini .yml .yaml .toml .json/;
my $INI_EXTS = "(".join("|", map {"\\$_"} @INI_EXT).")\$";
my %EXCLUDE = qw/selfapi 1/;

# пусть будет объектом в $app->project, а не классом
sub new {
	my $cls = shift;
	my $self = bless {}, ref $cls || $cls;

	my $f = $app->file(".")->abs;
	while($f->file) {
		$self->{name} = $f->file, $self->{root} = $f->path, last if 
            (
                $f->sub("etc/" . $f->file . ".*")->glob->grep(qr{$INI_EXTS}o)->exists || 
                $f->sub($f->file . ".*")->glob->grep(qr{$INI_EXTS}o)->exists || 
                $f->sub($f->file)->script("perl")
            ) && !exists $EXCLUDE{$f->file};
		last if $f->dir eq "";
		$f = $app->file($f->dir);
	}
	
	$self->{name} //= do { $self->{no} = 1; $self->{root} = "/tmp"; "NO_PROJECT_NAME" };
	
	$self
}

# меняет проект на текущую директорию
sub change {
	my ($self) = @_;

	my $new = $app->project->new;
	%$self = %$new;
	my $ini = $app->ini->new;
	%{$app->ini} = %$ini;
	
	unshift @INC, $self->path . "/lib";

	$self
}

# возвращает абсолютный путь от рута
sub path {
	my $self = shift;
	return $self->{root} if !@_;
	my $root = $self->{root};
	return "$root/$_[0]" if !wantarray;
	map { "$root/$_" } @_
}

# возвращает файлы для путей от корня проекта
sub file {
	my $self = shift;
	$app->file($self->path(@_))->relative
}

# возвращает файлы с проектом в качестве рута
sub base {
	my $self = shift;
	$self->file(@_)->root($app->file->pwd)
}

# если мы в проекте
sub yes {
	my ($self) = @_;
	!$self->{no}
}

# путь к ini-файлу
sub ini {
	my ($self) = @_;
	for my $i (
		sub { $app->project->root . "/" . $app->project->name },
		sub { $app->project->root . "/etc/" . $app->project->name },
		sub { $app->framework->root . "/etc/" . $app->framework->name },
	) {
		my $base = $i->();
		for my $ext (@INI_EXT) {
			my $path = $base . $ext;
			return $path if -e $path;
		}
	}
	return undef
}

# возвращает пути к директории lib в проекте и в фреймворке
sub lib {
	my ($self) = @_;
	my $lib1 = $self->path("lib");
	my $lib2 = $app->framework->path("lib");
	
	my @lib = $lib1 eq $lib2? $lib1: ($lib1, $lib2);
	wantarray? @lib: join ",", @lib;
}

# возвращает файлы по указанным путям и в фреймворке и в проекте
sub files {
	my $self = shift;
	$self->file(@_)->add( $app->framework->file(@_) )->relative->unique
}

# возвращает файл из проекта или фреймворка
sub inc {
	my $self = shift;
	$self->file(@_)->add( $app->framework->file(@_) )->grep("-e")
}

# возвращает файл из проекта или фреймворка
sub incfirst {
	my $self = shift;
	$self->inc(@_)->eq(0)
}


1;
