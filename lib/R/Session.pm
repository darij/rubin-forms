package R::Session;
# сессия на store

use common::sense;
use R::App qw/$app/;

R::has_const qw/id/;

# конструктор
sub new {
	my $cls = shift;
	my $self = bless {
		%{$app->session},	# расширяем из настроек
		@_
	}, ref $cls || $cls;
	
	$self->{id} //= $app->perl->unic_id(20);
	$self->{store} //= $app->cacheVinyl;	# memcache
	$self->{prefix} //= "";
	$self->{lifetime} //= 3600*24*3;	# время жизни сессий
	
	$self
}

# продляем время жизни сессии
sub touch {
	my ($self) = @_;
	my $key = $self->_key;
	$self->{store}->set($key, $self->{store}->get($key), $self->{lifetime});
	$self
}

# устанавливает/возвращает пользователя
sub user {
	my ($self, $user) = @_;
	package R::Session::User {
		sub id { shift->{id} }
		sub emit { shift }
	}
	return $self->{store}->get("user") // bless({id=>undef}, "R::Session::User") if @_ == 1;
	$self->{store}->set("user", $user);
	$self
}

# # клонирует себя
# sub _clone {
	# my $self = shift;
	# bless +{%$self, @_}, ref $self
# }

# возвращает ключ
sub _key {
	my ($self) = @_;
	$self->{prefix} . $self->{id}
}

# очищает сессию
sub _clear {
	my ($self) = @_;
	$self->{store}->del($self->_key);	
	$self
}

# устанавливает данные ключ => значение, ...
sub _set {
	my ($self, $k, $v) = @_;
	my $key = $self->_key;
	my $hash = $self->{store}->get($key);
	$hash->{$k} = $v;
	$self->{store}->set($key, $hash, $self->{lifetime});
	$self
}

# возвращает ключ и продляет его время жизни
sub _get {
	my ($self, $k) = @_;
	($self->{store}->get($self->_key) // return undef)->{$k};
}


# возвращает или устанавливает параметр
sub AUTOLOAD {
	our $AUTOLOAD =~ /([^:]+)\z/; my $prop = $1;
	my $code = "sub $prop { my \$self=shift; \@_? \$self->_set('$prop', \@_): \$self->_get('$prop') }";
	eval $code;
	die if $@;
	goto &$AUTOLOAD
}

sub DESTROY {}

1;