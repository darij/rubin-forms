package R::Mime;
# фабрика классов описывающих какой-то mime-тип

use common::sense;
use R::App;

my @MIME;
my %class2mime;
my %ext2mime;
my %type2mime;


# конструктор
sub new {
	my ($cls) = @_;
	bless {}, ref $cls || $cls;
}


#@@category public

# возващает все зарегистрированные классы
sub Regs {
	keys %class2mime
}

# возващает все зарегистрированные типы
sub Types {
	keys %type2mime
}

# возващает все зарегистрированные расширения
sub Exts {
	keys %ext2mime
}

# возвращает расширения по mime: app->mime->Ext("text/json" или "json") -> "json"
sub Ext {
	my ($self, $type) = @_;
	my $mime = $self->__mime($type) or return;
	wantarray? @{$mime->[1]}: $mime->[1][0]
}

# возвращает все алиасы mime: app->mime->Alias("text/json" или "json") -> "text/json", "application/json", "x-application/json"
sub Alias {
	my ($self, $type) = @_;
	my $mime = $self->__mime($type) or return;
	wantarray? @{$mime->[0]}: $mime->[0][0]
}

# возвращает Content-Type: app->mime->Type("text/json" или "json", codepage=>"windows-1251")
sub Type {
	my ($self, $ext, @args) = @_;
	my $alias = $self->Alias($ext);
	return $alias unless @args;
	join "; ", $alias, map2 { "$a=$b" } @args
}

# возвращает объект класса по mime или ext
sub Get {
	my ($self, $type) = @_;
	my $mime = $self->__mime($type) or die "нет `$type`";
	die "нет класса для `$type`" if "" eq (my $class = $mime->[2]);
	$app->use($class)->new(@_[2..$#_])
}


# автозагрузка
sub AUTOLOAD {
	my $self = shift;
	our $AUTOLOAD =~ /([^:]+)\z/;
	
	eval "sub $1 { return shift->{$1}->new(\@_) if \@_!=1; shift->{$1} }";
	die $@ if $@;
	
	$self->{$1} = $self->Get($1, @_);
}

# деструктор для AUTOLOAD
sub DESTROY {}


#@@category private

# возвращет  mime
sub __mime {
	my ($self, $type) = @_;
	$type = $` if $type =~ /;/;
	$type = lc $type;
	$type2mime{$type} // $ext2mime{$type}
}

# устанавливает глобальные массивы как ссылки на $MIME[$i]
sub __set_arrays {
	for my $mime ( @MIME ) {
		my ($types, $exts, $class) = @$mime;
		for my $type ( @$types ) {
			die "тип `$type` уже есть" if exists $type2mime{$type};
			$type2mime{$type} = $mime;
		}
		for my $ext ( @$exts ) {
			die "расширение `$ext` уже есть" if exists $ext2mime{$ext};
			$ext2mime{$ext} = $mime;
		}
		$class2mime{$class} = $mime if "" ne $class;
	}
}

#@link http://www.iana.org/assignments/media-types/media-types.xhtml

@MIME = (
	# application
	[["application/atom+xml"] => ["atom"] => ""],
	[["application/font-woff"] => ["woff"] => ""],
	[["application/java-archive"] => ["war", "ear", "jar"] => ""],
	[["application/mac-binhex40"] => ["hqx"] => ""],
	[["application/msword"] => ["doc"] => ""],
	[["application/octet-stream"] => ["msm", "dmg", "msp", "iso", "dll", "exe", "msi", "img", "deb", "bin"] => ""],
	[["application/pdf"] => ["pdf"] => ""],
	[["application/postscript"] => ["ai", "ps", "eps"] => ""],
	[["application/rss+xml"] => ["rss"] => ""],
	[["application/rtf"] => ["rtf"] => ""],
	[["application/vnd.google-earth.kml+xml"] => ["kml"] => ""],
	[["application/vnd.google-earth.kmz"] => ["kmz"] => ""],
	[["application/vnd.ms-excel"] => ["xls"] => ""],
	[["application/vnd.ms-fontobject"] => ["eot"] => ""],
	[["application/vnd.ms-powerpoint"] => ["ppt"] => ""],
	[["application/vnd.openxmlformats-officedocument.presentationml.presentation"] => ["pptx"] => ""],
	[["application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"] => ["xlsx"] => ""],
	[["application/vnd.openxmlformats-officedocument.wordprocessingml.document"] => ["docx"] => ""],
	[["application/vnd.wap.wmlc"] => ["wmlc"] => ""],
	[["application/x-7z-compressed"] => ["7z"] => ""],
	[["application/x-cocoa"] => ["cco"] => ""],
	[["application/x-java-archive-diff"] => ["jardiff"] => ""],
	[["application/x-java-jnlp-file"] => ["jnlp"] => ""],
	[["application/x-makeself"] => ["run"] => ""],
	[["application/x-perl"] => ["pl", "pm"] => ""],
	[["application/x-pilot"] => ["pdb", "prc"] => ""],
	[["application/x-rar-compressed"] => ["rar"] => ""],
	[["application/x-redhat-package-manager"] => ["rpm"] => ""],
	[["application/x-sea"] => ["sea"] => ""],
	[["application/x-shockwave-flash"] => ["swf"] => ""],
	[["application/x-stuffit"] => ["sit"] => ""],
	[["application/x-tcl"] => ["tcl", "tk"] => ""],
	[["application/x-x509-ca-cert"] => ["der", "pem", "crt"] => ""],
	[["application/x-xpinstall"] => ["xpi"] => ""],
	[["application/xhtml+xml"] => ["xhtml"] => ""],
	[["application/zip"] => ["zip"] => ""],
	[["application/x-www-form-urlencoded"] => ["urlencoded"] => "R::Mime::Application::Urlencoded"],
	
	[["application/x-swagger"] => ["swagger"] => "R::Mime::Application::Swagger"],
	
	
	# audio
	[["audio/midi"] => ["kar", "mid", "midi"] => ""],
	[["audio/mpeg"] => ["mp3"] => ""],
	[["audio/ogg"] => ["ogg"] => ""],
	[["audio/x-m4a"] => ["m4a"] => ""],
	[["audio/x-realaudio"] => ["ra"] => ""],
	
	# image
	[["image/gif"] => ["gif"] => ""],
	[["image/jpeg"] => ["jpe", "jpeg", "jpg"] => ""],
	[["image/png"] => ["png"] => ""],
	[["image/svg+xml"] => ["svg", "svgz"] => ""],
	[["image/tiff"] => ["tiff", "tif"] => ""],
	[["image/vnd.wap.wbmp"] => ["wbmp"] => ""],
	[["image/webp"] => ["webp"] => ""],
	[["image/x-icon"] => ["ico"] => ""],
	[["image/x-jng"] => ["jng"] => ""],
	[["image/x-ms-bmp"] => ["bmp"] => ""],
	
	# message
	[["message/http"] => ["http"] => ""],
	[["message/http-headers"] => ["httpheaders"] => "R::Mime::Message::Httpheaders"],
	[["message/imdn+xml"] => [] => ""],
	[["message/partial"] => [] => ""],
	[["message/rfc822"] => [] => ""],
	

	# model
	[["model/example"] => [] => ""],
	[["model/iges"] => [] => ""],
	[["model/mesh"] => [] => ""],
	[["model/vrml"] => ["wrl", "vrml"] => ""],
	[["model/x3d+binary"] => ["x3db"] => ""],
	[["model/x3d+vrml"] => ["x3dv"] => ""],
	[["model/x3d+xml"] => ["x3d"] => ""],
	
	# multipart
    [["multipart/mixed"] => [] => ""],
    [["multipart/alternative"] => [] => ""],
    [["multipart/related"] => [] => ""],
    [["multipart/form-data"] => ["form-data"] => "R::Mime::Multipart::Formdata"],
    [["multipart/signed"] => [] => ""],
    [["multipart/encrypted"] => [] => ""],

	
	# text
	[["text/css"] => ["css"] => ""],
	[["text/html"] => ["shtml", "htm", "html"] => ""],
	[["text/javascript", "application/javascript"] => ["js"] => "R::Mime::Text::Javascript"],
	[["text/mathml"] => ["mml"] => ""],
	[["text/plain"] => ["txt"] => ""],
	[["text/vnd.sun.j2me.app-descriptor"] => ["jad"] => ""],
	[["text/vnd.wap.wml"] => ["wml"] => ""],
	[["text/x-component"] => ["htc"] => ""],
	[["text/xml"] => ["xml"] => ""],
	[["text/yaml", "text/x-yaml", "application/yaml", "application/x-yaml", "text/vnd.yaml"] => ["yml", "yaml"] => "R::Yaml"],
	[["text/json", "application/json", "x-application/json"] => ["json"] => "R::Json"],
	[["text/ini"] => ["ini"] => "R::Mime::Text::Ini"],
	[["text/toml", "application/toml"] => ["toml"] => "R::Mime::Text::Toml"],
	[["text/markdown"] => ["markdown", "md"] => "R::Mime::Text::Markdown"],
	
	
	# video
	[["video/3gpp"] => ["3gpp", "3gp"] => ""],
	[["video/mp4"] => ["mp4"] => ""],
	[["video/mpeg"] => ["mpe", "mpeg", "mpg"] => ""],
	[["video/quicktime"] => ["mov"] => ""],
	[["video/webm"] => ["webm"] => ""],
	[["video/x-flv"] => ["flv"] => ""],
	[["video/x-m4v"] => ["m4v"] => ""],
	[["video/x-mng"] => ["mng"] => ""],
	[["video/x-ms-asf"] => ["asf", "asx"] => ""],
	[["video/x-ms-wmv"] => ["wmv"] => ""],
	[["video/x-msvideo"] => ["avi"] => ""]
);

&__set_arrays;

1;