package R::Js;
# менеджер управления скриптами фронт-энда

use common::sense;
use R::App;

# конструктор
sub new {
	my ($cls) = @_;
	bless {
		inc => {},		# название файла => [его позиция и текст]
		script => [],	# участки текста добавляемые append-ом
	}, ref $cls || $cls;
}

# добавляет код js c удалением <script>
sub append {
	my $self = shift;
	
	my $script = join "", @_;
	$script =~ s!^\s*<script[^<>]*>\s*(<\!--)?!!;
	$script =~ s!(-->)?\s*</script\s*>\s*$!!;
	
	push @{$self->{script}}, $script;
	
	$self
}

# добавляет файл
sub require {
	my ($self, $path) = @_;
	
	my $file = $app->file($path);
	
	return $self if $self->{inc}{$path};
	
	my $pos = $self->{inc}{$path}[0] // keys %{$self->{inc}};
	$self->{inc}{$path} = [$pos, $file->read];
	
	$self
}

# указывает точку старта в стеке
sub start {
	my ($self) = @_;
	push @{$self->{stack}}, scalar @{$self->{script}};
	$self
}

# сохраняет всё что накопилось в script в inc с указанным ключём
# $expirience - путь к файлу
sub compress {
	my ($self, $path, $expirience) = @_;
	
	my $from = pop @{$self->{stack}};
	return $self if @{$self->{script}} == $from;
	
	my $pos = $self->{inc}{$path}[0] // keys %{$self->{inc}};
	
	my $text = join "\n", splice @{$self->{script}}, $from;
	
	if($expirience) {
		my $view = $app->view->new(view =>$expirience);
		$text = $app->viewJavascript->new(view=>$view)->expirience($text) ;
	}
	
	# pos, text, 
	$self->{inc}{$path} = [$pos, $text];
	
	$self
}

# сохраняет код в файл
sub save {
	my ($self, $path) = @_;
	
	my $f = $app->file($path);
	my $s = $self->stringify;
	
	if(!$f->exists) {
		$f->mkpath->write($s);
	}
	elsif($s ne $f->read) {
		$f->mkpath->cp($f->ext($f->ext.".bk")), $f->mkpath->write($s);
	}
	
	$self->{saveTime} = $f->mtime;
	
	$self
}

# проставить позиции классам 
sub _isa_set_pos {
	my ($self, $r) = @_;
	$r or return;
	$r->[3] or return;
	$r->[0] = 1 + R::max $r->[0]-1, map { $self->_isa_set_pos($self->{inc}{$_}) } @{$r->[4]}
}

# стрингифицирует
sub stringify {
	my ($self) = @_;
	my $inc = $self->{inc};
	
	# классы должны быть отсортированы по своим зависимостям. 
	# Для этого:
	# 1. строим дерево зависимостей
	
	my %class;
	
	for my $r (values %$inc) {
		if($r->[1] =~ /^(?:window.[\w\$]+\s*=\s*)?class\s+([\w\$]+)(?:\s+extends\s+([^\{]+))?/) {
			$r->[3] = $1;
			$class{$1} = 1;
			my $x = $2;
			$x =~ s!\s+$!!;
			$r->[4] = [ grep { /^[A-Z]/ } split /[^\w\$]+/, $x ];
		}
	}
	
	# классы которые в списках наследования, но, возможно, не представлены, должны дополнятся фейковыми
	my @classes;
	for my $r (R::asc { $_->[0] } values %$inc) {
		for my $x (@{$r->[4]}) {
			$class{$x} and next;
			push @classes, "try { eval(\"$x\") } catch(e) { eval(\"function $x() {}\") }";
			$class{$x} = 1;
		}
	}
	
	# 2. проходим по зависимостям рекурсивно и на обратном проходе ставим максимальное+1
	for my $r (values %$inc) {
		$self->_isa_set_pos($r);
	}

	join "\n", @classes, (map { $_->[1] } sort { 
		$a->[0] == $b->[0]? $a->[1] cmp $b->[1]: $a->[0] <=> $b->[0]
	} values %$inc), @{$self->{script}}
}

# очищает js
sub clear {
	my ($self) = @_;
	%{$self->{inc}} = ();
	%{$self->{script}} = ();
	$self
}

1;