package R::Json;
# кодирует-раскодирует в json

use common::sense;
use R::App;

use JSON::XS;
# canonical - сортировать ключи
# allow_nonref - упаковывать также и скаляры
sub _json_xs { JSON::XS->new->canonical->allow_nonref }
my $json_xs = _json_xs();
our $true = $JSON::XS::true;
our $false = $JSON::XS::false;

# linenum и pretty - выводить номера строк в колоризированном выводе
has qw/linenum pretty/;

# конструктор
sub new {
	my $cls = shift;
	bless {
		pretty => 1,
		linenum => 0,
	}, ref $cls || $cls
}


# декодирует из json
sub from {
	my ($self, $json) = @_;
	$json_xs->decode($json);
}


# кодирует в json
sub to {
	my ($self, $val) = @_;
	$json_xs->encode($val);
}

# возвращает true
sub true {
	$true
}

# возвращает false
sub false {
	$false
}

# отформатированный json
sub fmt {
	my ($self, $val, $ident) = @_;
	_json_xs()->pretty(1)->indent($ident // 4)->space_before(0)->encode($val)
}

# колоризирует в цветную строку
sub color {
	my ($self, $data) = @_;
	require JSON::Color;
	JSON::Color::encode_json($data, {
		linum => $self->{linenum},
		pretty => $self->{pretty},
	})
}

1;
