package R::Color;
# колоризирует синтаксис

use common::sense;
use R::App;

use Syntax::Highlight::Engine::Kate;
use Term::ANSIColor ':constants';

# конструктор
sub new {
	my $cls = shift;
	bless {}, ref $cls || $cls;
}

# список поддерживаемых языков
sub list {
	my ($self) = @_;
	Syntax::Highlight::Engine::Kate->new->languageList
}

# список поддерживаемых секций
sub menu {
	my ($self) = @_;
	Syntax::Highlight::Engine::Kate->new->sections
}

# возвращает список расширений
sub extlist {
	my ($self) = @_;
	Syntax::Highlight::Engine::Kate->new->extensions
}

# определяет язык по пути
sub by {
	my ($self, $path) = @_;
	Syntax::Highlight::Engine::Kate->new->languagePropose($path)
}

# определяет название плагина по пути
sub plugin {
	my ($self, $lang) = @_;
	"Syntax::Highlight::Engine::Kate::" . Syntax::Highlight::Engine::Kate->new->languagePlug($lang, 1)
}

# подгружает плагин
sub load {
	my ($self, $lang) = @_;
	$app->use($self->plugin($lang));
	$lang
}

# возвращает объект языка
sub syntax {
	my ($self, $lang) = @_;
	$self->{syntax}{$lang} //= Syntax::Highlight::Engine::Kate->new(
        language => $self->load($lang),
	)
}

# колоризирует язык
sub color {
	my ($self, $lang, $text) = @_;
	my $syntax = $self->syntax($lang);
	
	$syntax->formatTable({
		Alert => [RED, RESET],
		BaseN => [RED, RESET],
		BString => [CYAN, RESET],
		Char => [CYAN, RESET],
		Comment => [BOLD.BLACK, RESET],
		DataType => [GREEN, RESET],
		DecVal => [RED, RESET],
		Error => [RED, RESET],
		Float => [BRIGHT_RED, RESET],
		Function => [MAGENTA, RESET],
		IString => [MAGENTA, RESET],
		Keyword => [CYAN, RESET],
		Normal => ["", ""],
		Operator => [BRIGHT_GREEN, RESET],
		Others => [MAGENTA, RESET],
		RegionMarker => [ON_GREEN, RESET],
		Reserved => [BLACK ON_BLUE, RESET],
		String => [MAGENTA, RESET],
		Variable => [BRIGHT_GREEN, RESET],
		Warning => [RED, RESET],
	});
	$syntax->highlightText($text);
}

# колоризирует язык в html
sub colored_html {
	my ($self, $lang, $text) = @_;
	my $syntax = $self->syntax($lang);
	
	$syntax->formatTable({
		Alert        => [ "<font color=\"#0000ff\">",       "</font>" ],
		BaseN        => [ "<font color=\"#007f00\">",       "</font>" ],
		BString      => [ "<font color=\"#c9a7ff\">",       "</font>" ],
		Char         => [ "<font color=\"#ff00ff\">",       "</font>" ],
		Comment      => [ "<font color=\"#7f7f7f\"><i>",    "</i></font>" ],
		DataType     => [ "<font color=\"#0000ff\">",       "</font>" ],
		DecVal       => [ "<font color=\"#00007f\">",       "</font>" ],
		Error        => [ "<font color=\"#ff0000\"><b><i>", "</i></b></font>" ],
		Float        => [ "<font color=\"#00007f\">",       "</font>" ],
		Function     => [ "<font color=\"#007f00\">",       "</font>" ],
		IString      => [ "<font color=\"#ff0000\">",       "" ],
		Keyword      => [ "<b>",                            "</b>" ],
		Normal       => [ "",                               "" ],
		Operator     => [ "<font color=\"#ffa500\">",       "</font>" ],
		Others       => [ "<font color=\"#b03060\">",       "</font>" ],
		RegionMarker => [ "<font color=\"#96b9ff\"><i>",    "</i></font>" ],
		Reserved     => [ "<font color=\"#9b30ff\"><b>",    "</b></font>" ],
		String       => [ "<font color=\"#ff0000\">",       "</font>" ],
		Variable     => [ "<font color=\"#0000ff\"><b>",    "</b></font>" ],
		Warning      => [ "<font color=\"#0000ff\"><b><i>", "</b></i></font>" ],
	});
	$syntax->highlightText($text);
}

# колоризирует perl для ansi
sub perl {
	my ($self, $text) = @_;
	$self->color("Perl" => $text)
}

# колоризирует perl для ansi
sub html {
	my ($self, $text) = @_;
	$self->color("HTML" => $text)
}

# колоризирует js для ansi
sub js {
	my ($self, $text) = @_;
	$self->color("JavaScript" => $text)
}


1;
