package R::Dev;
# сервер разработки
# получает список проектов:
#	список файлов и каталогов проекта
#	и команду запуска сервера
# 	при изменении файла в проекте перезагружает его
# проксирует запросы в проект

use base R::Fulminant;

use common::sense;
use R::App;

# конструктор
sub new {
	my $cls = shift;
	my $self = $cls->SUPER::new(
		projects => undef,	# { "host" => { dir => "директория проекта", watch => ["act", "model", "lib"], run => "команда запуска проекта", proxy => "localhost:123", subordinates => ["хосты которые зависят от этого и должны быть так же перезагружены"] } }
		ps => {},			# процессы
		watchers => {},		# следилки за изменением в файлах проектов
		std => 1,			# выводить на консоль вывод с процессов
		autoreload => 1,	# автоперезагрузка страницы
		script_autoreload => $app->framework->file("share/dev/autoreload.html")->read,	# скрипт перезагрузки страницы
		sleep => 1,			# задержка между проверками watch
		timeout_recv => 30,	# переопределяем
		timeout => 60,		# переопределяем
		timeout_get => 59,	# таймаут на запрос
		timeout_send => 30,	# переопределяем
		@_
	);
	
	$self
}

# запускает цикл слежения
sub run {
	my ($self) = @_;
	
	die "не указаны проекты!" if %{$self->{projects}} == 0;
	
	$self->SUPER::run; 
	
	my $p = $self->{projects};

	# инициализируем процессы
	%{$self->{ps}} = R::map2 {
		my $ps = $app->process($app->magnitudeLiteral->wordname($a) => $b->{run} // "")->init($b->{init})->std($self->{std})->dir($b->{dir});
		$ps->kill("KILL")->wait(1, 1) if $ps->exists;
		$ps->waitrun;
		$app->log->info(":space", $a, $ps->exists? (":green", "run"): (":red", "fail"), ":reset", "on", ":black bold", $b->{proxy});
		$a => $ps
	} R::grep2 { $b->{run} || $b->{init} } %$p;
	
	# определяем каталоги для слежения
	%{$self->{watchers}} = R::map2 { $a => $app->file(@{$b->{watch}})->abs($app->file($b->{dir})->abs)->watchify } R::grep2 { $b->{watch} && @{$b->{watch}} } %$p;
	
	# msg1 ":size10000", $self->{watchers}{'djetan.com'}->{parts}, $p;
	# $app->coro->psapp->kill;
	# exit;
	
	# семафор, чтобы случайно не начать рестарт одновременно

	# следилка за изменением файлов
	my $monitor = $app->coro("$self->{name}-monitor" => sub {
				
		my $start = time();
		my $page_size = `getconf PAGESIZE` || 4096;
		my $start_vsize = $app->magnitudeByte->to($app->process->procinfo->vsize);
		my $start_rss = $app->magnitudeByte->to($app->process->rss * $page_size);
		for(my $i=0;; $i++) {
			sleep $self->{sleep}; 
		
			msg ":empty", "monitor", ":cyan", $app->magnitudeDate->now->fmt, ":reset", " прощупываний ", ":red", $i, ":reset", "/", ":green", (time()-$start) . "s ", ":reset","вебсокетов ", ":black red", $self->busy_websockets, ":reset", 
			" запросов ", ":green", $self->busy_queriers, ":reset",
			" память ", ":red", $app->magnitudeByte->to($app->process->procinfo->vsize), ":reset",
			"/", ":green", $start_vsize, ":reset",
			" rss ", ":red", $app->magnitudeByte->to($app->process->rss * $page_size), ":reset",
			"/", ":green", $start_rss;
			print STDERR $app->tty->kk;
			print STDERR $app->tty->at(1, $app->tty->rows-1); 
		
			for my $host (keys %{$self->{watchers}}) {
				# если были изменения в файлах - перезагружаем (вдруг успеем раньше чем пользователь вызовет)
				# если упал - ждём изменения в файлах
				$self->_watch($host);
			}
		}
		
	})->ready;
	
	$self->{fiber} = $self->{fiber}->add($monitor);
	
	$app->log->info( ":space red", "SET", ":white", "watchers", ":cyan", keys(%{$self->{watchers}}), ":white", "start processes", ":cyan", keys(%{$self->{ps}}) );
	
	$self
}


# если были изменения в файлах - перезагружаем
my $_EXTS = join ",", qw/iced coffee litcoffee/;
my $_GREP_EXTS = "**.($_EXTS)";
sub _watch {
	my ($self, $host) = @_;
	
	# послкдующий код должен выполнятся один для всех волокон
	my $sem = $self->{watch_sem} //= $app->coro->sem;
	my $watch_up_defer = $sem->guard;
	
	# если нет - выходим!
	my $f = $self->{watchers}{$host} or return "";
		
	my $watch = $f->watch;
	 
	# ничего не изменилось!
	return "" if !$watch->modified->length;
	
	my $deleted_coffee = $watch->deleted->grep($_GREP_EXTS);
	if($deleted_coffee->length) {
		$deleted_coffee->ext("js")->rm;
		$deleted_coffee->ext("map")->rm;
		$app->log->info(":space blue", "rm", ":sep black bold", $deleted_coffee->ext("js")->parts);
	}
	
	my $coffee = $watch->modified->add($watch->created)->grep($_GREP_EXTS);
	if($coffee->length) {
		for($coffee->parts) {
			my $t = Time::HiRes::time();
			my $res = $app->coffee->compile($_);
			$app->log->info(":blue space", "compile", ":black bold", $_, ":reset",
				$app->perl->fsec(Time::HiRes::time() - $t),
				$res eq "ok"? ":green": ":red",
				$res);
		}
	}
	
	return "" if ($coffee->length || $deleted_coffee->length) && $watch->modified->grep($_GREP_EXTS)->length == $watch->modified->length;
	
	$self->_restart($watch, $_, $host) for @{$self->{projects}{$host}{subordinates}};
	
	$self->_restart($watch, $host);

	return 1; 
}

# рестартует хост
sub _restart {
	my ($self, $watch, $host, $by_host) = @_;
	
	my $ps = $self->{ps}{$host};
	
	$ps // return "";
	
	my $time = Time::HiRes::time();
	
	$ps->stop;
	
	msg1 $ps->exists;
	
	$ps->waitrun;

	my $active = $ps->exists;

	$app->log->warn(":space black bold", "restart", ":reset blue", $host,
		$by_host? (":black bold", "by", $by_host): (),
		":reset",
		$app->perl->fsec(Time::HiRes::time() - $time),
		$active? (":green", "ok"): (":red", "fail"),
		":reset", "by", ":green", $watch->modified->parts,
		#$watch->modified->length>1? (":reset", "and more",  ":red", $watch->modified->length-1): ()
	);
	# my $x1 = $watch->modified->path;
	# my $x2 = $watch->modified->ext($watch->modified->ext . ".bk")->path;
	# print `diff $x1 $x2`;
	
	msg1 "autoreload:", !!$self->{autoreload};
	if($self->{autoreload}) {
		# делаем авторелоад сокетов
		my $sockets = $self->{sockets}{$host};
		
		msg1 $host, scalar @$sockets;
		
		if($active) {
			$_->emit("autoreload") for @$sockets;
		} 
		else {
			my $err = $self->last_error($host);
			my $size = 1024 * 100;
			#max_payload_size
			$err = "...\n\n" . substr $err, length($err) - $size if length($err) > $size;
			$_->emit("underlie", $self->_ansi($err)) for @$sockets;
		}
	}
	 
	$active
}

# запрос
sub ritter {
	my ($self, $request) = @_;
	
	# в зависимости от хоста проксируем на сервис
	my $host = $request->host;
	my $project = $self->{projects}{$host};
	my $inherit = $self->{projects}{$project->{inherits}};
	
	return $self->projects_table($request) if !$project;
	return [404, [], "нет proxy"] if !$project->{proxy};
	
	# открыть в редакторе
	if($request->path eq "/admin/open_in_editor/") {
	
		my @dirs = map { $_->{dir} // () } ($inherit // $project), values %{$self->{projects}};
	
	# R::msg $request;
		# R::msg scalar $request->param;
		R::msg1 $request->param->path, $request->param->line, \@dirs;
	
		$app->editor->open($request->param->path, $request->param->line, \@dirs);
		return $? == 0? [200, "OK"]: [500, "ERROR"];
	}
	# вебсокет разработки
	elsif($request->path eq "/__dev__") { 
		my $sockets = $self->{sockets}{$project->{inherits} // $host} //= [];
		
		my $io = $app->rsgiIo->new(
			connect => sub { 
				# при коннекте добавляем сокет в очередь
				#$Coro::current->{request}->emit("autoreload"); - зацикливается
				msg ":on_green white", "io.connect", ":reset green", $Coro::current->{request}->path;
				push @$sockets, $Coro::current->{request};
			},
			disconnect => sub {
				# а при дисконнекте - удаляем
				msg ":on_green red", "io.disconnect", ":reset red", $Coro::current->{request}->path;
				@$sockets = grep { $Coro::current->{request} != $_ } @$sockets;
			},
		);
		
		return $io->adventure($request);
	}
	
	# статику выдаём так
	if(!$project->{static} && $request->path =~ /\.\w+$/) {
		return $app->file(join "", $project->{dir} // $self->{projects}{$project->{inherits}}{dir}, "/html", $request->path);
	}
	
	# перезагружаем сервак, если файлы в проекте изменились	
	#Coro::State::enable_times(1);
	#my ($real, $cpu) = $Coro::current->times;
	
	my $time_watch = Time::HiRes::time();
	$self->_watch($host);
	$time_watch = Time::HiRes::time() - $time_watch;
	
	my $time_ps_exists = Time::HiRes::time();
	my $ps = $self->{ps}{$host} // $self->{ps}{$project->{inherits}};
	
	# если сервер не отвечает: выводим последнюю ошибку
	if(!$ps->exists) {
		#msg1 "no dev exists";
		$app->log->info(":space red", "  ", 500, ":black bold", $host, "not ok", ":reset");
		return [500, ["Content-Type" => "text/html; charset=utf-8"], $self->_ansi($self->last_error($host, $ps)).$self->{script_autoreload}];
	}
	
	$time_ps_exists = Time::HiRes::time() - $time_ps_exists;
	my $time = Time::HiRes::time();
	
	my $result = eval {
		my $url = $request->url->
			timeout($project->{timeout_get} // $self->{timeout_get} // 60)->
			noredirect->
			domain($project->{proxy})->
			method($request->via)->
			header(%{$request->{headers}})->
			data($request->body);
		$url->res;
	};
	if($@) {
		$app->log->info(":space red", "  ", 503, "[503 Ошибка в запросе]: ", $@);
		return [503, ["Content-Type" => "text/html; charset=utf-8"], $self->_ansi("connect{${\$self->name}}: ", $app->perl->dump($@))];
	}
	
	my $content = $result->decoded_content;
	
	my $location;
	my @headers =R::grep2 { $a =~ /^Location$/i? do { $location = $b; 0 }: 1 } $result->headers->flatten;
	if($location) {
		my $port = $self->{port};
		$location =~ s!^(https?://)localhost(:\d+)?!$1$host:$port!;
		push @headers, "Location" => $location;
	}
	
	# считываем вывод, который был в серваке и прибавляем
	if( $result->content_type =~ /^text\/html$/i && !defined $location  ) {

		# html файлы должны быть всегда в utf-8!
		#utf8::decode($content) if !utf8::is_utf8($content);
	
		my $out = $ps->output;
		#if(length $out) { $self->{last_output}{$host} = $out }
		#else { $out = $self->{last_output}{$host} }
		
		if(length $out) {
			#msg1 "add out";
			$out = $self->_ansi($out);
		}
		
		# добавляем ауторелоад и $out
		#$content =~ s!(<body\b[^<>]*>)(.*)(</body>)!$1<table id=__DEV__SPACE__><thead><tr><td>$out<tbody><tr><td>$2<tfoot><tr><td>$self->{script_autoreload}</table>$3!is
		#or do {
			my $this_is_dev = "<meta name=\"rubin-forms-dev-proxy\" content=\"$project->{proxy}\">";
			$content =~ s!<head\b[^<>]*>!$&$this_is_dev!i or $content =~ s!^!$this_is_dev!i;
			$content =~ s!<body\b[^<>]*>!$&$out!i or $content =~ s!^!$out!i;
			$content =~ s!</body\s*>|\z!$&$self->{script_autoreload}!i;
		#};
	}
	
	
	
	# access.log
	
	$app->log->info(":space", ":magenta", $request->via, ":cyan", $request->url->url, ":reset", 
		$app->perl->fsec($time_watch), 
		$app->perl->fsec($time_ps_exists), 
		$app->perl->fsec(Time::HiRes::time() - $time), 
		
		do { given($result->code) {
			":blue" when [100..199];
			":green" when [200..299];
			":bold black" when [300..399];
			":magenta" when [400..499];
			":red" when [500..599];
			default { ":yellow" }
		}},
		$result->code,
		":magenta",
		$app->magnitudeByte->to(scalar $result->content_length // length $content, 2), 
		":reset",
		scalar $result->content_type,
		#$result->content_type =~ /^text/? $result->content_charset: (),
	);
	return [
		$result->code,
		[ R::grep2 { $a !~ /^Content-Length$/i } @headers ],
		$content
	];
}

# хелпер для вывода окошка ошибки
sub _ansi {
	my ($self, $err, $id) = @_;
	$id //= "__dev__-error";
	$app->html->ansi($err, "border: solid 2px #7B68EE", $id)
}

# возвращает последнюю ошибку
sub last_error {
	my ($self, $host, $ps) = @_;
	$ps //= $self->{ps}{$host};
	my $err = $ps->output;
	$self->{last_error}{$host} = $err if $err !~ /^\s*$/;
	$self->{last_error}{$host} = "-- нет сообщения об ошибке --\nУкажите app.ini:scenario:dev:std = 1 в rubin-forms/etc/rubin-forms.yml" if $self->{last_error}{$host} eq "";
	$self->{last_error}{$host}
}

# распечатывает таблицу состояния сервера
sub projects_table {
	my ($self, $request) = @_;
	
	my $p = $self->{projects};
	
	my @th = map { ("<th>", $_) } "host", "proxy", "dir", "run", "watch";
	my @tr;
	for my $host (sort keys %$p) {
		my $v = $p->{$host};
		push @tr, [$host, @$v{qw/proxy dir run/}, join ", ", @{$v->{watch}}];
	}
	
	my @head = "<title>Разработка | rubin-forms</title>";
	#"<h3>Сервер разработки rubin-forms</h3>");
	
	join "", @head, "<table><tr>", @th, (map { ("<tr>", map { ("<td>", $app->html->to($_)) } @$_) } @tr), "</table>";
}

1;