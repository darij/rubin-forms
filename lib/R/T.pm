package R::T;
# benchmark
# use:
#   my $x = app->t("my-name");
#   ...
#   undef $x;
# in END print to STDERR key and times code

use common::sense;
use R::App qw/:min/;
use Time::HiRes qw//;

my %x;

# my $x = app->t("my-name")
sub new {
	my ($cls, $key) = @_;
    
    bless {
		key => $key,
		start => Time::HiRes::time(),
	}, ref $cls || $cls;
}

sub DESTROY {
	my ($self) = @_; 
	$x{$self->{key}} += Time::HiRes::time() - $self->{start}
}

sub say {
	
	return if !keys %x;
	
    for my $k (sort keys %x) {
        $app->log->info(":sep( -> )", $k, $app->perl->fsec($x{$k}));
    }
	
	%x = ();
}

END {
    __PACKAGE__->say();
}


1;