package R;
# содержит основные функции, интегрируемые с модулями

our $VERSION = 0.0.0;

use common::sense;

require Scalar::Util;
require Time::HiRes;
#require Sub::Identify;
#require Sub::Name;
require Encode;
require List::Util;
require Scalar::Util;
require Guard;
use Term::ANSIColor qw/color colorstrip colored/;

# содержит $R::App::app, для перезагрузки этого модуля (app->hotswap(pkg=>"R::App")->reload)
our $app = $R::App::app;

# возвращает синглетон приложения
sub app () { $R::App::app }


sub Is ($;$) { require "R/Subtype.pm"; *Is = \&R::Subtype::Is; goto &Is };
sub IsErr ($;$) { require "R/Subtype.pm"; *IsErr = \&R::Subtype::IsErr; goto &IsErr };
sub subtype ($$;$) { require "R/Subtype.pm"; *subtype = \&R::Subtype::subtype; goto &subtype };
sub subenum ($$) { require "R/Subtype.pm"; *subenum = \&R::Subtype::subenum; goto &subenum };

*Num = \&Scalar::Util::looks_like_number;
*Int = sub { Scalar::Util::looks_like_number($_[0]) && $_[0]-int($_[0]) == 0 };
*weaken = \&Scalar::Util::weaken;

*map2 = \&List::Util::pairmap;
*grep2 = \&List::Util::pairgrep;
*keys2 = \&List::Util::pairkeys;
*values2 = \&List::Util::pairvalues;
*first2 = \&List::Util::pairfirst;
*first = \&List::Util::first;
*reduce = \&List::Util::reduce;
*all = \&List::Util::all;
*any = \&List::Util::any;
*max = \&List::Util::max;			# числовое сравнение
*min = \&List::Util::min;
*maximal = \&List::Util::maxstr;	# строчное сравнение
*minimal = \&List::Util::minstr;
*sum = \&List::Util::sum;
*product = \&List::Util::product;
*uniq = \&List::Util::uniq;

*guard = \&Guard::guard;

*time = \&Time::HiRes::time;


#@@category функции переводов

# возвращает текущую локаль
sub __locale() {
	$Coro::current && exists $Coro::current->{request}? $Coro::current->{request}->locale:
		$R::App::app->locale->factory("ru")
}

# по ключу 
sub __($) {
	my ($msgid) = @_;
	__locale->{msg}->{$msgid} // $msgid
}

# замена {id}
sub __x($@) {
	my ($msgid, %arg) = @_;
	$msgid = __locale->{msg}->{$msgid} // $msgid;
	$msgid =~ s!\{([a-z_]\w*)\}!$arg{$1}!gi;
	$msgid
}

# выбирает по номеру форму
sub __n($$$) {
	my ($one, $many, $n) = @_;
	my $local = __locale;
	($local? $local->{n}->{$one}->{$many}->[ $local->{plural}->($n) ]: undef) // ($n>1? $many: $one)
}

# __n и __x
sub __nx($$$@) {
	my ($one, $many, $n, %arg) = @_;
	my $s = __n($one, $many, $n);
	$s =~ s!\{([a-z_]\w*)\}!$arg{$1}!gi;
	$s
}

# по контексту
sub __p($$) {
	my ($ctx, $msgid) = @_;
	__locale->{ctx}->{$ctx}->{$msgid} // $msgid
}

# по контексту
sub __px($$@) {
	my ($ctx, $msgid, %arg) = @_;
	$msgid = __locale->{ctx}->{$ctx}->{$msgid} // $msgid;
	$msgid =~ s!\{([a-z_]\w*)\}!$arg{$1}!gi;
	$msgid
}

# выбирает по номеру форму
sub __np($$$$) {
	my ($ctx, $one, $many, $n) = @_;
	my $local = __locale;
	($local? $local->{ctxn}->{$ctx}->{$one}->{$many}->[ $local->{plural}->($n) ]: undef) // ($n>1? $many: $one)
}

sub __npx ($$$$@) {
	my ($ctx, $one, $many, $n, %arg) = @_;
	my $s = __np($ctx, $one, $many, $n);
	$s =~ s!\{([a-z_]\w*)\}!$arg{$1}!gi;
	$s
}

sub N__($) { $_[0] }
sub N__p($$) { $_[1] }
sub N__n($$$) { my ($one, $many, $n) = @_; $n>1? $many: $one }
sub N__np($$$$) { my ($ctx, $one, $many, $n) = @_; $n>1? $many: $one }



# добавляет префикс, если есть к чему
sub Prefix ($$) {
	my ($x, $y) = @_;
	length $y? "$x$y": undef
}

# добавляет суффикс
sub Suffix ($$) {
	my ($x, $y) = @_;
	length $x? "$x$y": undef
}

# добавляет приставку и суффикс
sub Infix ($$$) {
	my ($x, $y, $z) = @_;
	length $y? "$x$y$z": undef
}

# подменяет системный sleep
our $__sleep = \&Time::HiRes::sleep;
sub sleep($) {
    goto &$__sleep;
}

# ожидает N сек., если указано или 1 сек.
# использование: waits until $app->process->child->active->length == 2;
sub waits(@) {
	sleep $_[0] // 1;
}

# логирует вывод
# возвращает последний параметр
sub msg (@) {
	local ( $_, $&, $!, $@ );
    my ( $pkg, $file, $lineno ) = caller(0);


    # определяем, что файл совпадает с пакетом
    $_ = $pkg;
    s!::!/!g;
    $_ = quotemeta $_;
    $pkg = "$file !$pkg" if $file !~ m!$_\.[^/]+$!;
	
	my $ps = ($R::App::app->process->name || $$) . '#';
	
	my $coro = $Coro::current && length($Coro::current->{desc}) && "[$Coro::current->{desc}] ";

	my $log = $R::App::app && $R::App::app->log;
	my @mode;
	if(!$log) {
		require "R/Log.pm";
		$log = R::Log->new(%{$R::App::app && $R::App::app->ini && $R::App::app->ini->{service} && $R::App::app->ini->{service}{log} || {}});
		@mode = (":white", "${^GLOBAL_PHASE}:>", ":reset");
	}
	
	my $id = "$ps$coro$pkg $lineno";
	$log->can("info")? $log->info( ":space", @mode, $id, ":sep", @_ ): do {
		my $x = [$id, @_];
		eval {
			require "DDP.pm";
			DDP::p($x);
		};
		print STDERR @_, "\n" if $@;
	};
	
    return $_[$#_];
}

# добавляет =================== перед выводом
sub msg1 (@) {
    unshift @_, ":yellow on_red space", "====================", ":reset sep";
	goto \&msg
}

# функция замещения
sub TODO () {
    die "TODO";

    #die "метод ещё не написан";
}

# проверяет на соответствие isa
sub Isa (@) {
    my $val = shift;
    return "" if !defined $val;
	return "" if ref $val ne "" and !Scalar::Util::blessed($val);
	return "" if $val eq "";
    for my $cls (@_) {
        return 1 if $val->isa( ref $cls || $cls );
    }
}

sub Can ($$) {
    my ( $val, $cls ) = @_;
	return "" if !defined $val;
	return "" if ref $val ne "" and !Scalar::Util::blessed($val);
	return "" if $val eq "";
    $val->can($cls);
}


# строковая прямая сортировка
sub ascending (&@) {
    my $code = shift;
	map { $_->[1] } sort { $a->[0] cmp $b->[0] } map { [scalar $code->(), $_] } @_;
}

# строковая обратная сортировка
sub descending (&@) {
    my $code = shift;
	map { $_->[1] } sort { $b->[0] cmp $a->[0] } map { [scalar $code->(), $_] } @_;
}

# числовая прямая сортировка
sub asc (&@) {
    my $code = shift;
	map { $_->[1] } sort { $a->[0] <=> $b->[0] } map { [scalar $code->(), $_] } @_;
}

# числовая обратная сортировка
sub desc (&@) {
    my $code = shift;
	map { $_->[1] } sort { $b->[0] <=> $a->[0] } map { [scalar $code->(), $_] } @_;
}

# замыкание:
#	closure \&{$self->can("method")}, $self
#	closure {print}, 5
sub closure (@) {
    my $sub  = pop;
	die "замыкание должно быть функцией" if ref $sub ne "CODE";
    my @args = @_;
	#my ($package, $filename, $line, $subroutine, $hasargs, $wantarray, $evaltext, $is_require, $hints, $bitmask, $hinthash) = caller(0);
	#R::msg1 ":blue", "sub", ":red", "$sub", ":black bold", map {"$_"} @args, ":reset green", "$package", $line;
    sub { $sub->( @args, @_ ) }
}

# обходит рекурсивно структуру
# $a - ключ
# $b - значение, которое можно заменить $b = ...;
# $_ - HASH или ARRAY в котором они находятся
sub Tour (&@) {
	my $code = shift;
	
	my $pkg = caller;
	my $a = \${"${pkg}::a"};
	my $b = \${"${pkg}::b"};
	local $_;
	
	my @S = [@_];
	while(@S) {
		my $data = pop @S;
		
		if(ref($data) eq "ARRAY") {
			for(my $i=0; $i<@$data; $i++) {
				$$a = $i;
				$$b = $data->[$i];
				$_ = $data;
				$code->();
				$data->[$i] = $$b;
				push @S, $$b if Scalar::Util::reftype($$b) =~ /^(ARRAY|HASH)$/n;
			}
		}
		elsif(ref($data) eq "HASH") {
			for my $i (sort keys %$data) {
				next if !exists $data->{$i};
				$$a = $i;
				$$b = $data->{$i};
				$_ = $data;
				$code->();
				$data->{$i} = $$b;
				push @S, $$b if Scalar::Util::reftype($$b) =~ /^(ARRAY|HASH)$/n;
			}
		}
		
	}
	return;
}

# добавляет геттер-сеттер
sub has {
	my $pkg = caller;

	for my $name (@_) {
		die "has: метод $name уже есть в $pkg" if $pkg->can($name);
		eval "sub ${pkg}::$name { if(\@_>1) { \$_[0]{$name} = \$_[1]; \$_[0]  } else { \$_[0]{$name} } }";
		die if $@;
	}
	return;
}

# добавляет геттер
our $RO_ERR = "свойство только для чтения";
sub has_const {
	my $pkg = caller;

	for my $name (@_) {
		die "has_const: метод $name уже есть в $pkg" if $pkg->can($name);
		eval "sub ${pkg}::$name { if(\@_>1) { die \$R::RO_ERR } else { \$_[0]{$name} } }";
		die if $@;
	}
	return;
}

# создаёт http-сервер
sub server (&;@) {
	my $ritter = shift;
	$R::App::app->fulminant(ritter => sub { local $_ = $_[0]; $ritter->(@_) }, @_)->run;
}

# транспонирует списки из своих операндов
sub zip(@) {
	my $size = List::Util::max( map { scalar @$_ } @_);
	my @R;
	for(my $s=0; $s<$size; $s++) {
		for(my $i=0; $i<@_; $i++) {
			$R[$s][$i] = $_[$i][$s];
		}
	}
	return @R;
}

# нарезает массив блоками по N
sub slice($@) {
	my $n = shift;
	my @R;
	push @R, [splice @_, 0, $n] while @_;
	@R
}

# суперпозиция - каждый с каждым
sub cross(@) {
	#my $size = List::Util::max( map { scalar @$_ } @_);
	
	my @I = (0) x scalar @_;
	my @R;
	while() {
		push @R, my $r = [];
		for(my $i=0; $i<@_; $i++) {
			push @$r, $_[$i][$I[$i]];
		}
	
		my $I = $#_;
		while(++$I[$I] >= @{$_[$I]}) {
			$I[$I] = 0;
			--$I;
			return @R if $I == -1;
		}
	}
}

# пропускает через блок и возвращает уникальные
sub singlify (&@) {
	my $f = shift;
	my %map;
	grep { !$map{$f->()}++ } @_
}

# # объединение хешей вида key => [x1...], key => [x2...] -> key => [x1, x2...]
# sub merge (@) {
	# my $R = {};
	
	# for my $x (@_) {
		# push @{$R->{$_}}, ref $x->{$_}? @{$x->{$_}}: $x->{$_} for keys %$x;
	# }
	
	# $R
# }

# группировка по ключу. Превращает массив в хеш key => [x1, x2...]
# *heshify = \&indexing;
# *grip = \&indexing;
# *mkindex = \&indexing;
# *indexify = \&indexing;
sub indexify (&@) {
	my $f = shift;
	my $r = {};
	push @{$r->{$f->()}}, $_ for @_;
	$r
}

# объединяет по ключу и возвращает все существующие пары или более
sub cross_join (&@) {
	my $f = shift;
	my @indexes = map { indexify \&$f, @$_ } @_;
	my %keys = map { %$_ } @indexes;
	
	map { my $key=$_; cross map { $_->{$key} } @indexes } keys %keys
}

# минимальный по
sub min_by (&@) {
	my $f = shift;
	my ($ret) = asc \&$f, @_;
	$ret
}

# максимальный по
sub max_by (&@) {
	my $f = shift;
	my ($ret) = desc \&$f, @_;
	$ret
}

# минимальный по
sub minimal_by (&@) {
	my $f = shift;
	my ($ret) = ascending \&$f, @_;
	$ret
}

# максимальный по
sub maximal_by (&@) {
	my $f = shift;
	my ($ret) = descending \&$f, @_;
	$ret
}

# # "разыменовывает" массивы и хеши
# sub flat (@) {
	# map { map { Scalar::Util::reftype($_) eq "ARRAY"? @$_: $_ } Scalar::Util::reftype($_) eq "HASH"? values %$_: $_ } @_
# }

# # "существует только один" - нужен ли?
# sub one () {
	# my ($self) = @_;
	# ...
# }

#*uniq = \&List::Util::uniqnum;
#*unique = \&List::Util::uniq;
#*shuffle = \&List::Util::shuffle;

# перехватчик вида try { ... } R::Raise::Quit => except { ... } ...
sub try (&;@) {
	my $fn = shift;
	my @ret;
	if(wantarray) { @ret=eval $fn } else { $ret[0]=eval $fn };
	
	if($@) {
		my $error = $@;

		for(my $i=0; $i<@_; $i++) {
			my $fn = $_[$i];
			next unless ref($fn) || Isa($error, $fn) && ($fn=$_[++$i]);
			
			$@ = $error;
			if(wantarray) { @ret = eval $fn } else { $ret[0] = eval $fn };
		}
		
		die if Isa $error, "R::Raise::Quit";
	}
	
	return wantarray? @ret: $ret[0];
}

sub except (&;@) {
	@_
}

1;
